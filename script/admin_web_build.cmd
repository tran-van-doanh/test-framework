@echo off
SET scriptPath=%~dp0
SET projectRootDir=%scriptPath%..\
SET projectName=test_framework_admin
cd /D %projectRootDir%
if not exist %projectRootDir%deploy mkdir %projectRootDir%deploy
if exist %projectRootDir%deploy\test-framework-admin rmdir /s /q %projectRootDir%deploy\test-framework-admin
cd /D %projectRootDir%%projectName%
call flutter build web --release --no-tree-shake-icons  --dart-define=FLUTTER_WEB_CANVASKIT_URL=/canvaskit/ --base-href "/superadmin/"
move %projectRootDir%%projectName%\build\web %projectRootDir%deploy\test-framework-admin