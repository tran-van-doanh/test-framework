@echo off
SET scriptPath=%~dp0
SET projectRootDir=%scriptPath%..\
SET projectName=test_framework_admin
cd /D %projectRootDir%
if exist %projectRootDir%deploy\test-framework-admin-client\ rmdir /s /q %projectRootDir%deploy\test-framework-admin-client\
cd /D %projectRootDir%%projectName%
call flutter build windows --release --no-tree-shake-icons
timeout 2 > NUL
ren %projectRootDir%%projectName%\build\windows\x64\runner\Release test-framework-admin-client
timeout 2 > NUL
move %projectRootDir%%projectName%\build\windows\x64\runner\test-framework-admin-client %projectRootDir%deploy\