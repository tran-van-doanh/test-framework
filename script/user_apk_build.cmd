@echo off
SET scriptPath=%~dp0
SET projectRootDir=%scriptPath%..\
SET projectName=test_framework_view
cd /D %projectRootDir%
if not exist %projectRootDir%deploy mkdir %projectRootDir%deploy
if exist %projectRootDir%deploy\test-framework-view rmdir /s /q %projectRootDir%deploy\test-framework-view
cd /D %projectRootDir%%projectName%
call flutter build apk  --release --no-tree-shake-icons
timeout 2 > NUL
ren %projectRootDir%%projectName%\build\app\outputs\flutter-apk\app-release.apk test-framework.apk
timeout 2 > NUL
move  %projectRootDir%%projectName%\build\app\outputs\flutter-apk\test-framework.apk %projectRootDir%deploy\