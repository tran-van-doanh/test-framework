@echo off
SET scriptPath=%~dp0
SET projectRootDir=%scriptPath%..\
SET projectName=test_framework_view
cd /D %projectRootDir%
if not exist %projectRootDir%deploy\test-framework-testnode mkdir %projectRootDir%deploy\test-framework-testnode
if exist %projectRootDir%deploy\test-framework-testnode\client\ rmdir /s /q %projectRootDir%deploy\test-framework-testnode\client\
cd /D %projectRootDir%%projectName%
call flutter build windows --release --no-tree-shake-icons
timeout 2 > NUL
move %projectRootDir%%projectName%\build\windows\x64\runner\Release %projectRootDir%deploy\test-framework-testnode\client