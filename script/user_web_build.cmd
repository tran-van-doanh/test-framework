@echo off
SET scriptPath=%~dp0
SET projectRootDir=%scriptPath%..\
SET projectName=test_framework_view
cd /D %projectRootDir%
if not exist %projectRootDir%deploy mkdir %projectRootDir%deploy
if exist %projectRootDir%deploy\test-framework-view rmdir /s /q %projectRootDir%deploy\test-framework-view
cd /D %projectRootDir%%projectName%
call flutter build web  --release --no-tree-shake-icons --dart-define=FLUTTER_WEB_CANVASKIT_URL=/canvaskit/
move %projectRootDir%%projectName%\build\web %projectRootDir%deploy\test-framework-view