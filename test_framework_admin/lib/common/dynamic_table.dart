import 'package:flutter/material.dart';

class DynamicTablePagging extends StatelessWidget {
  final int rowCount;
  final int currentPage;
  final int rowPerPage;
  final Function? pageChangeHandler;
  final Function? rowPerPageChangeHandler;
  const DynamicTablePagging(this.rowCount, this.currentPage, this.rowPerPage, {Key? key, this.pageChangeHandler, this.rowPerPageChangeHandler})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (rowCount > 0) {
      var firstRow = (currentPage - 1) * rowPerPage + 1;
      var lastRow = currentPage * rowPerPage;
      if (lastRow > rowCount) {
        lastRow = rowCount;
      }
      return Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          const Text("Row per page: "),
          DropdownButton<int>(
            value: rowPerPage,
            icon: const Icon(Icons.keyboard_arrow_down),
            elevation: 16,
            style: const TextStyle(color: Colors.deepPurple),
            underline: Container(
              height: 2,
              color: Colors.deepPurpleAccent,
            ),
            onChanged: (int? newValue) {
              if (rowPerPageChangeHandler != null) {
                rowPerPageChangeHandler!(newValue);
              }
            },
            items: <int>[2, 5, 10, 25, 50, 100].map<DropdownMenuItem<int>>((int value) {
              return DropdownMenuItem<int>(
                value: value,
                child: Text("$value"),
              );
            }).toList(),
          ),
          Text("Row $firstRow - $lastRow of $rowCount"),
          IconButton(
              onPressed: (pageChangeHandler != null && firstRow != 1)
                  ? () {
                      pageChangeHandler!(currentPage - 1);
                    }
                  : null,
              icon: const Icon(Icons.chevron_left)),
          IconButton(
              onPressed: (pageChangeHandler != null && lastRow < rowCount)
                  ? () {
                      pageChangeHandler!(currentPage + 1);
                    }
                  : null,
              icon: const Icon(Icons.chevron_right)),
        ],
      );
    }
    return Container();
  }
}

class DynamicTable extends StatelessWidget {
  final List<DataColumn> columns;
  final List<DataRow> rows;
  final int minWidth;
  const DynamicTable({Key? key, required this.columns, required this.rows, this.minWidth = 600}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BorderSide styleBorderTable = const BorderSide(width: 1, color: Color.fromRGBO(216, 218, 229, 1), style: BorderStyle.solid);
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      if (constraints.maxWidth < minWidth) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: DataTable(
                      columnSpacing: 5,
                      showCheckboxColumn: false,
                      columns: columns,
                      rows: rows,
                    ),
                  ),
                ),
              ],
            ),
          ],
        );
      } else {
        return Row(
          children: [
            Expanded(
                child: DataTable(
              showCheckboxColumn: false,
              dataRowMinHeight: 60,
              dataRowMaxHeight: 60,
              border: TableBorder(
                  verticalInside: BorderSide.none,
                  horizontalInside: styleBorderTable,
                  bottom: styleBorderTable,
                  left: styleBorderTable,
                  right: styleBorderTable,
                  top: styleBorderTable),
              dataRowColor: MaterialStateProperty.resolveWith<Color?>((Set<MaterialState> states) {
                if (states.contains(MaterialState.selected)) {
                  return const Color.fromRGBO(238, 243, 255, 1);
                }
                return Colors.transparent; // Use the default value.
              }),
              columns: columns,
              rows: rows,
            ))
          ],
        );
      }
    });
  }
}
