import 'dart:convert';
import 'dart:io';

import 'package:filesystem_picker/filesystem_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:test_framework_admin/config.dart';

getRootDirectory() async {
  Directory? rootPath;
  if (Platform.isAndroid) {
    if (await Permission.manageExternalStorage.request().isGranted) {
      rootPath = await getExternalStorageDirectory();
    }
  }
  rootPath ??= await getApplicationDocumentsDirectory();
  return rootPath;
}

selectDirectory(context) async {
  return await FilesystemPicker.open(
    title: 'Save to folder',
    context: context,
    rootDirectory: await getRootDirectory(),
    fsType: FilesystemType.folder,
    pickText: 'Save file to this folder',
    contextActions: [
      FilesystemPickerNewFolderContextAction(),
    ],
  );
}

Future<File?> httpDownloadFile(BuildContext context, String url, String? authorization, String filename) async {
  String? path = await selectDirectory(context);
  if (path != null) {
    Map<String, String> headers = {};
    if (authorization != null) {
      headers["Authorization"] = authorization;
    }
    var res = await http.get(Uri.parse("$baseUrl$url"), headers: headers);
    if (res.statusCode == 200) {
      File file = File('$path/$filename');
      await file.writeAsBytes(res.bodyBytes);
      return file;
    }
  }
  return null;
}

Future<File?> httpDownloadFileWithBodyRequest(BuildContext context, String url, String? authorization, String filename, bodyRequest) async {
  String? path = await selectDirectory(context);
  if (path != null) {
    Map<String, String> headers = {'content-type': 'application/json'};
    var finalRequestBody = json.encode(bodyRequest);
    if (authorization != null) {
      headers["Authorization"] = authorization;
    }
    var res = await http.post(Uri.parse("$baseUrl$url"), headers: headers, body: finalRequestBody);
    if (res.statusCode == 200) {
      File file = File('$path/$filename');
      await file.writeAsBytes(res.bodyBytes);
      return file;
    }
  }
  return null;
}
