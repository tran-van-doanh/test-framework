import 'package:flutter/material.dart';

class DialogDelete extends StatelessWidget {
  final dynamic selectedItem;
  final String type;
  final Function callback;
  const DialogDelete({Key? key, required this.selectedItem, required this.type, required this.callback}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: const EdgeInsets.all(0),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Text(
            'DELETE',
            style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(0, 0, 0, 0.8), fontSize: 16, letterSpacing: 2),
          ),
          TextButton(
              onPressed: () => Navigator.pop(context, 'Cancel'),
              child: const Icon(
                Icons.close,
                color: Colors.black,
              ))
        ],
      ),
      content: Container(
        decoration: const BoxDecoration(
            border: Border(top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)), bottom: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)))),
        margin: const EdgeInsets.only(top: 20, bottom: 10),
        width: 500,
        padding: const EdgeInsets.all(20),
        child: IntrinsicHeight(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text.rich(
                TextSpan(text: 'Are you sure you want to delete ', children: <InlineSpan>[
                  if (type == "delete_element") const TextSpan(text: 'all element of '),
                  TextSpan(
                    text: type == "widget"
                        ? '"${selectedItem.type}"'
                        : (type == "Delete Test Case" || type == "Delete Test Suite")
                            ? ''
                            : type == "organization_user"
                                ? '"${selectedItem["fullname"]}"'
                                : type == "user"
                                    ? '"${selectedItem["sysUser"]["fullname"]}"'
                                    : (type == "project_type" ||
                                            type == "value_type" ||
                                            type == "element" ||
                                            type == "value_template" ||
                                            type == "random_type" ||
                                            type == "random_value" ||
                                            type == "organization_role" ||
                                            type == "project")
                                        ? '"${selectedItem["name"]}"'
                                        : (type == "server")
                                            ? '"$selectedItem"'
                                            : '"${selectedItem['title'] ?? "this"}"',
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                  const TextSpan(text: ' ?')
                ]),
              ),
              Container(
                margin: const EdgeInsets.only(top: 20),
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(228, 115, 87, 1),
                  borderRadius: BorderRadius.circular(5),
                ),
                padding: const EdgeInsets.only(left: 5),
                child: Container(
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.horizontal(
                      right: Radius.circular(5),
                    ),
                    color: Color.fromRGBO(255, 233, 217, 1),
                  ),
                  padding: const EdgeInsets.all(15),
                  child: const Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Icon(Icons.warning, color: Color.fromRGBO(243, 86, 40, 1)),
                      SizedBox(width: 10),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Warning",
                            style: TextStyle(color: Color.fromARGB(255, 206, 20, 20), fontWeight: FontWeight.bold),
                          ),
                          SizedBox(height: 10),
                          Text(
                            "You can't undo this action.",
                            style: TextStyle(color: Color.fromARGB(255, 187, 103, 82)),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      actionsAlignment: MainAxisAlignment.center,
      actions: [
        Container(
          margin: const EdgeInsets.only(right: 5),
          height: 40,
          width: 120,
          child: ElevatedButton(
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(
                const Color.fromRGBO(225, 46, 59, 1),
              ),
            ),
            onPressed: () {
              callback();
              Navigator.of(context).pop();
            },
            child: const Text(
              "DELETE",
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 5),
          height: 40,
          width: 120,
          child: ElevatedButton(
            onPressed: () {
              Navigator.pop(context);
            },
            style: ElevatedButton.styleFrom(
              side: const BorderSide(color: Color.fromRGBO(209, 212, 216, 1)),
              backgroundColor: Colors.white,
              foregroundColor: const Color.fromRGBO(61, 90, 254, 1),
            ),
            child: const Text(
              "CANCEL",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        )
      ],
    );
  }
}
