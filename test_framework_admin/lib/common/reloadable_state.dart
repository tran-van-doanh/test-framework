
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_admin/model/model.dart';

abstract class  ReloadableState<T extends StatefulWidget> extends State<T> {
  RouterItem? currentRouterItem;
  var disopsed = false;

  @override
  void initState() {
    super.initState();
    disopsed = false;
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    currentRouterItem=navigationModel.routerItemHistory.last;
    navigationModel.addListener(checkReload);
  }

  checkReload() {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    if (currentRouterItem?.key != null && currentRouterItem?.key == navigationModel.routerItemHistory.last.key) {
      refreshData();
    }
  }

  refreshData();

  @override
  void dispose() {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    navigationModel.removeListener(checkReload);
    disopsed = true;
    super.dispose();
  }

  @override
  setState(VoidCallback fn) {
    if (disopsed) {
      return;
    }
    super.setState(fn);
  }

}