import 'package:file_picker/file_picker.dart';
import 'dart:io';

selectFile(List<String>? allowedExtensions) async {
  FilePickerResult? result = await FilePicker.platform.pickFiles(
    type: FileType.custom,
    allowedExtensions: allowedExtensions,
  );
  if (result == null) {
    return null;
  }
  return result.files.first;
}

readFile(PlatformFile file) async {
  if (Platform.isWindows) {
    return await File(file.path!).readAsBytes();
  } else {
    return file.bytes!;
  }
}
