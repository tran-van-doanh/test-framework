import 'dart:math' as math;
import 'package:flutter/material.dart';

class FormDefaultPainter extends CustomPainter {
  String? label;
  double radius;
  FormDefaultPainter({this.label, this.radius = 3});
  @override
  void paint(Canvas canvas, Size size) {
    var strokePaint = Paint()
      ..color = Colors.black54
      ..strokeWidth = 1
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;
    if (label != null) {
      TextSpan span = TextSpan(text: label!, style: const TextStyle(fontSize: 12, color: Colors.black54));
      TextPainter tp = TextPainter(text: span, textDirection: TextDirection.ltr);
      tp.layout();
      tp.paint(canvas, const Offset(10.0, -8.0));
      var path = Path();
      path.moveTo(0, radius);
      path.arcTo(Rect.fromCircle(center: Offset(radius, radius), radius: radius), -math.pi, math.pi / 2, true);
      path.lineTo(8, 0);
      path.moveTo(12 + tp.size.width, 0);
      path.lineTo(size.width - radius, 0);
      path.arcTo(Rect.fromCircle(center: Offset(size.width - radius, radius), radius: radius), -math.pi / 2, math.pi / 2, true);
      path.lineTo(size.width, size.height - radius);
      path.arcTo(Rect.fromCircle(center: Offset(size.width - radius, size.height - radius), radius: radius), 0, math.pi / 2, true);
      path.lineTo(radius, size.height);
      path.arcTo(Rect.fromCircle(center: Offset(radius, size.height - radius), radius: radius), math.pi / 2, math.pi / 2, true);
      path.lineTo(0, radius);
      canvas.drawPath(path, strokePaint);
    } else {
      var path = Path();
      path.moveTo(0, radius);
      path.arcTo(Rect.fromCircle(center: Offset(radius, radius), radius: radius), -math.pi, math.pi / 2, true);
      path.lineTo(size.width - radius, 0);
      path.arcTo(Rect.fromCircle(center: Offset(size.width - radius, radius), radius: radius), -math.pi / 2, math.pi / 2, true);
      path.lineTo(size.width, size.height - radius);
      path.arcTo(Rect.fromCircle(center: Offset(size.width - radius, size.height - radius), radius: radius), 0, math.pi / 2, true);
      path.lineTo(radius, size.height);
      path.arcTo(Rect.fromCircle(center: Offset(radius, size.height - radius), radius: radius), math.pi / 2, math.pi / 2, true);
      path.lineTo(0, radius);
      canvas.drawPath(path, strokePaint);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
