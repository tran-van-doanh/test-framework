import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_admin/admin/report/rpt_dashboard.dart';
import 'package:test_framework_admin/admin/report/rpt_report.dart';
import 'package:test_framework_admin/admin/server/run_command.dart';
import 'package:test_framework_admin/admin/system/doc_document.dart';
import 'package:test_framework_admin/admin/system/doc_file.dart';
import 'package:test_framework_admin/admin/system/doc_instruction_manuals.dart';
import 'package:test_framework_admin/admin/system/editor_component.dart';
import 'package:test_framework_admin/admin/system/sys_multi_language.dart';
import 'package:test_framework_admin/admin/system/sys_node.dart';
import 'package:test_framework_admin/admin/system/sys_node_file.dart';
import 'package:test_framework_admin/admin/system/sys_organization.dart';
import 'package:test_framework_admin/admin/system/sys_organization_hostname.dart';
import 'package:test_framework_admin/admin/system/sys_organization_role.dart';
import 'package:test_framework_admin/admin/system/sys_perm.dart';
import 'package:test_framework_admin/admin/system/sys_role.dart';
import 'package:test_framework_admin/admin/system/sys_user.dart';
import 'package:test_framework_admin/admin/system/sys_user_project.dart';
import 'package:test_framework_admin/admin/system/task_type.dart';
import 'package:test_framework_admin/admin/system/tf_find_element_template.dart';
import 'package:test_framework_admin/admin/system/tf_value_template.dart';
import 'package:test_framework_admin/admin/system/tf_value_type.dart';
import 'package:test_framework_admin/page/loading.dart';
import 'package:test_framework_admin/model/model.dart';
import 'package:test_framework_admin/admin/dashboard.dart';
import 'page/login/login.dart';
import 'package:flutter/foundation.dart';

class AppRoutePath {
  String location;
  AppRoutePath(this.location);
}

class AppRouteInformationParser extends RouteInformationParser<AppRoutePath> {
  @override
  Future<AppRoutePath> parseRouteInformation(RouteInformation routeInformation) async {
    return AppRoutePath(routeInformation.location);
  }

  @override
  RouteInformation? restoreRouteInformation(AppRoutePath configuration) {
    if (configuration.location != "") {
      return RouteInformation(location: configuration.location);
    }
    return const RouteInformation(location: "/");
  }
}

class AppRouterDelegate extends RouterDelegate<AppRoutePath> with ChangeNotifier, PopNavigatorRouterDelegateMixin<AppRoutePath> {
  @override
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
  late RouterItem _initRouterItem;
  late RouterItem _lastRouterItem;

  AppRouterDelegate() {
    _initRouterItem = RouterItem(key: UniqueKey(), url: "/");
    _lastRouterItem = _initRouterItem;
  }

  @override
  AppRoutePath get currentConfiguration => AppRoutePath(_lastRouterItem.url);

  Widget? getUrlWidget(RouterItem routerItem) {
    Widget? currentWidget;
    final uri = Uri.parse(routerItem.url);
    if (uri.pathSegments.isNotEmpty) {
      if (uri.pathSegments[0] == "admin") {
        if (uri.pathSegments.length >= 3 && uri.pathSegments[1] == "server") {
          if (uri.pathSegments[2] == "run-command") {
            currentWidget = const AdminServerRunCommandWidget();
          }
        }
        if (uri.pathSegments.length >= 3 && uri.pathSegments[1] == "report") {
          if (uri.pathSegments[2] == "rpt-report") {
            if (uri.pathSegments.length >= 5) {
              currentWidget = AdminRptReportDetailWidget(rptReportAction: uri.pathSegments[3], rptReportId: uri.pathSegments[4]);
            } else if (uri.pathSegments.length >= 4) {
              currentWidget = AdminRptReportDetailWidget(rptReportAction: uri.pathSegments[3]);
            } else {
              currentWidget = const AdminRptReportListWidget();
            }
          }
          if (uri.pathSegments[2] == "rpt-dashboard") {
            if (uri.pathSegments.length >= 5) {
              currentWidget = AdminRptDashboardDetailWidget(rptDashboardAction: uri.pathSegments[3], rptDashboardId: uri.pathSegments[4]);
            } else if (uri.pathSegments.length >= 4) {
              currentWidget = AdminRptDashboardDetailWidget(rptDashboardAction: uri.pathSegments[3]);
            } else {
              currentWidget = const AdminRptDashboardListWidget();
            }
          }
        }
        if (uri.pathSegments.length >= 3 && uri.pathSegments[1] == "system") {
          if (uri.pathSegments[2] == "sys-user") {
            if (uri.pathSegments.length >= 5) {
              currentWidget = AdminSysUserDetailWidget(sysUserAction: uri.pathSegments[3], sysUserId: uri.pathSegments[4]);
            } else if (uri.pathSegments.length >= 4) {
              currentWidget = AdminSysUserDetailWidget(sysUserAction: uri.pathSegments[3]);
            } else {
              currentWidget = const AdminSysUserListWidget();
            }
          }
          if (uri.pathSegments[2] == "sys-user-project") {
            if (uri.pathSegments.length >= 5) {
              currentWidget = AdminSysUserProjectDetailWidget(sysUserProjectAction: uri.pathSegments[3], sysUserProjectId: uri.pathSegments[4]);
            } else if (uri.pathSegments.length >= 4) {
              currentWidget = AdminSysUserProjectDetailWidget(sysUserProjectAction: uri.pathSegments[3]);
            } else {
              currentWidget = const AdminSysUserProjectListWidget();
            }
          }
          if (uri.pathSegments[2] == "sys-role") {
            if (uri.pathSegments.length >= 5) {
              currentWidget = AdminSysRoleDetailWidget(sysRoleAction: uri.pathSegments[3], sysRoleId: uri.pathSegments[4]);
            } else if (uri.pathSegments.length >= 4) {
              currentWidget = AdminSysRoleDetailWidget(sysRoleAction: uri.pathSegments[3]);
            } else {
              currentWidget = const AdminSysRoleListWidget();
            }
          }
          if (uri.pathSegments[2] == "sys-organization") {
            if (uri.pathSegments.length >= 5) {
              currentWidget = AdminSysOrganizationDetailWidget(sysOrganizationAction: uri.pathSegments[3], sysOrganizationId: uri.pathSegments[4]);
            } else if (uri.pathSegments.length >= 4) {
              currentWidget = AdminSysOrganizationDetailWidget(sysOrganizationAction: uri.pathSegments[3]);
            } else {
              currentWidget = const AdminSysOrganizationListWidget();
            }
          }
          if (uri.pathSegments[2] == "sys-organization-hostname") {
            if (uri.pathSegments.length >= 5) {
              currentWidget = AdminSysOrganizationHostnameDetailWidget(
                  sysOrganizationHostnameAction: uri.pathSegments[3], sysOrganizationHostnameId: uri.pathSegments[4]);
            } else if (uri.pathSegments.length >= 4) {
              currentWidget = AdminSysOrganizationHostnameDetailWidget(sysOrganizationHostnameAction: uri.pathSegments[3]);
            } else {
              currentWidget = const AdminSysOrganizationHostnameListWidget();
            }
          }
          if (uri.pathSegments[2] == "sys-organization-role") {
            if (uri.pathSegments.length >= 5) {
              currentWidget =
                  AdminSysOrganizationRoleDetailWidget(sysOrganizationRoleAction: uri.pathSegments[3], sysOrganizationRoleId: uri.pathSegments[4]);
            } else if (uri.pathSegments.length >= 4) {
              currentWidget = AdminSysOrganizationRoleDetailWidget(sysOrganizationRoleAction: uri.pathSegments[3]);
            } else {
              currentWidget = const AdminSysOrganizationRoleListWidget();
            }
          }
          if (uri.pathSegments[2] == "sys-node") {
            if (uri.pathSegments.length >= 5) {
              currentWidget = AdminSysNodeDetailWidget(sysNodeAction: uri.pathSegments[3], sysNodeId: uri.pathSegments[4]);
            } else if (uri.pathSegments.length >= 4) {
              currentWidget = AdminSysNodeDetailWidget(sysNodeAction: uri.pathSegments[3]);
            } else {
              currentWidget = const AdminSysNodeListWidget();
            }
          }
          if (uri.pathSegments[2] == "sys-node-file") {
            if (uri.pathSegments.length >= 5) {
              currentWidget = AdminSysNodeFileDetailWidget(sysNodeFileAction: uri.pathSegments[3], sysNodeFileId: uri.pathSegments[4]);
            } else if (uri.pathSegments.length >= 4) {
              currentWidget = AdminSysNodeFileDetailWidget(sysNodeFileAction: uri.pathSegments[3]);
            } else {
              currentWidget = const AdminSysNodeFileListWidget();
            }
          }
          if (uri.pathSegments[2] == "sys-perm") {
            if (uri.pathSegments.length >= 5) {
              if (uri.pathSegments[3] == "create") {
                currentWidget = AdminSysPermDetailWidget(sysPermAction: uri.pathSegments[3], parentId: uri.pathSegments[4]);
              } else {
                currentWidget = AdminSysPermDetailWidget(sysPermAction: uri.pathSegments[3], sysPermId: uri.pathSegments[4]);
              }
            } else if (uri.pathSegments.length >= 4) {
              currentWidget = AdminSysPermDetailWidget(sysPermAction: uri.pathSegments[3]);
            } else {
              currentWidget = const AdminSysPermListPageWidget();
            }
          }
          if (uri.pathSegments[2] == "tf-value-type") {
            if (uri.pathSegments.length >= 5) {
              currentWidget = AdminTfValueTypeDetailWidget(tfValueTypeAction: uri.pathSegments[3], tfValueTypeId: uri.pathSegments[4]);
            } else if (uri.pathSegments.length >= 4) {
              currentWidget = AdminTfValueTypeDetailWidget(tfValueTypeAction: uri.pathSegments[3]);
            } else {
              currentWidget = const AdminTfValueTypeListWidget();
            }
          }
          if (uri.pathSegments[2] == "tf-value-template") {
            if (uri.pathSegments.length >= 5) {
              currentWidget = AdminTfValueTemplateDetailWidget(tfValueTemplateAction: uri.pathSegments[3], tfValueTemplateId: uri.pathSegments[4]);
            } else if (uri.pathSegments.length >= 4) {
              currentWidget = AdminTfValueTemplateDetailWidget(tfValueTemplateAction: uri.pathSegments[3]);
            } else {
              currentWidget = const AdminTfValueTemplateListWidget();
            }
          }
          if (uri.pathSegments[2] == "tf-find-element-template") {
            if (uri.pathSegments.length >= 5) {
              currentWidget = AdminTfFindElementTemplateDetailWidget(
                  tfFindElementTemplateAction: uri.pathSegments[3], tfFindElementTemplateId: uri.pathSegments[4]);
            } else if (uri.pathSegments.length >= 4) {
              currentWidget = AdminTfFindElementTemplateDetailWidget(tfFindElementTemplateAction: uri.pathSegments[3]);
            } else {
              currentWidget = const AdminTfFindElementTemplateListWidget();
            }
          }
          if (uri.pathSegments[2] == "doc-file") {
            if (uri.pathSegments.length >= 5) {
              currentWidget = AdminDocFileDetailWidget(docFileAction: uri.pathSegments[3], docFileId: uri.pathSegments[4]);
            } else if (uri.pathSegments.length >= 4) {
              currentWidget = AdminDocFileDetailWidget(docFileAction: uri.pathSegments[3]);
            } else {
              currentWidget = const AdminDocFileListWidget();
            }
          }
          if (uri.pathSegments[2] == "doc-document") {
            if (uri.pathSegments.length >= 5) {
              if (uri.pathSegments[3] == "create") {
                currentWidget = AdminDocDocumentDetailWidget(docDocumentAction: uri.pathSegments[3], parentId: uri.pathSegments[4]);
              } else {
                currentWidget = AdminDocDocumentDetailWidget(docDocumentAction: uri.pathSegments[3], docDocumentId: uri.pathSegments[4]);
              }
            } else if (uri.pathSegments.length >= 4) {
              currentWidget = AdminDocDocumentDetailWidget(docDocumentAction: uri.pathSegments[3]);
            } else {
              currentWidget = const AdminDocDocumentListPageWidget();
            }
          }
          if (uri.pathSegments[2] == "editor-component") {
            if (uri.pathSegments.length >= 5) {
              currentWidget = AdminEditorComponentDetailWidget(editorComponentAction: uri.pathSegments[3], editorComponentId: uri.pathSegments[4]);
            } else if (uri.pathSegments.length >= 4) {
              currentWidget = AdminEditorComponentDetailWidget(editorComponentAction: uri.pathSegments[3]);
            } else {
              currentWidget = const AdminEditorComponentListWidget();
            }
          }
          if (uri.pathSegments[2] == "sys-multi-language") {
            if (uri.pathSegments.length >= 5) {
              currentWidget = AdminSysMultiLanguageDetailWidget(sysMultiLanguageAction: uri.pathSegments[3], sysMultiLanguageId: uri.pathSegments[4]);
            } else if (uri.pathSegments.length >= 4) {
              currentWidget = AdminSysMultiLanguageDetailWidget(sysMultiLanguageAction: uri.pathSegments[3]);
            } else {
              currentWidget = const AdminSysMultiLanguageListWidget();
            }
          }
          if (uri.pathSegments[2] == "sys-organization") {
            if (uri.pathSegments.length >= 5) {
              currentWidget = AdminSysOrganizationDetailWidget(sysOrganizationAction: uri.pathSegments[3], sysOrganizationId: uri.pathSegments[4]);
            } else if (uri.pathSegments.length >= 4) {
              currentWidget = AdminSysOrganizationDetailWidget(sysOrganizationAction: uri.pathSegments[3]);
            } else {
              currentWidget = const AdminSysOrganizationListWidget();
            }
          }
          if (uri.pathSegments[2] == "task-type") {
            if (uri.pathSegments.length >= 5) {
              currentWidget = AdminTaskTypeDetailWidget(taskTypeAction: uri.pathSegments[3], taskTypeId: uri.pathSegments[4]);
            } else if (uri.pathSegments.length >= 4) {
              currentWidget = AdminTaskTypeDetailWidget(taskTypeAction: uri.pathSegments[3]);
            } else {
              currentWidget = const AdminTaskTypeListWidget();
            }
          }
          if (uri.pathSegments[2] == "doc-instruction-manuals") {
            if (uri.pathSegments.length >= 5) {
              currentWidget = AdminDocInstructionManualsDetailWidget(
                  docInstructionManualsAction: uri.pathSegments[3], docInstructionManualsId: uri.pathSegments[4]);
            } else if (uri.pathSegments.length >= 4) {
              currentWidget = AdminDocInstructionManualsDetailWidget(docInstructionManualsAction: uri.pathSegments[3]);
            } else {
              currentWidget = const AdminDocInstructionManualsListWidget();
            }
          }
        }
      }
    }
    return currentWidget;
  }

  @override
  Widget build(BuildContext context) {
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: true);
    List<Page<dynamic>> pages = [];
    if (securityModel.inited == false) {
      pages = [const MaterialPage(child: LoadingScreen())];
    } else if (securityModel.authenticated == false) {
      pages = [
        const MaterialPage(
          child: LoginScreen(),
        )
      ];
    } else {
      pages = [
        MaterialPage(
          key: UniqueKey(),
          child: const AdminDashboardWidget(),
        )
      ];

      NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
      if (navigationModel.routerItemHistory.isEmpty) {
        navigationModel.routerItemHistory.add(_initRouterItem);
      }
      for (var routerItem in navigationModel.routerItemHistory) {
        Widget? currentWidget = getUrlWidget(routerItem);
        if (currentWidget != null) {
          pages.add(MaterialPage(key: routerItem.key, child: currentWidget));
        }
      }
    }
    if (kReleaseMode) {
      return Navigator(
        key: navigatorKey,
        transitionDelegate: NoAnimationTransitionDelegate(),
        pages: pages,
        onPopPage: (route, result) {
          if (!route.didPop(result)) {
            return false;
          }
          Provider.of<NavigationModel>(context, listen: false).pop("/");
          return true;
        },
      );
    } else {
      return Navigator(
        key: navigatorKey,
        pages: pages,
        onPopPage: (route, result) {
          if (!route.didPop(result)) {
            return false;
          }
          Provider.of<NavigationModel>(context, listen: false).pop("/");
          return true;
        },
      );
    }
  }

  @override
  Future<void> setNewRoutePath(AppRoutePath configuration) async {
    if (configuration.location != "/") {
      _initRouterItem = RouterItem(key: UniqueKey(), url: configuration.location);
      _lastRouterItem = _initRouterItem;
    } else {
      _initRouterItem = RouterItem(key: UniqueKey(), url: "/");
      _lastRouterItem = _initRouterItem;
    }
  }

  void updateNavigation(NavigationModel navigationModel) {
    if (navigationModel.routerItemHistory.isNotEmpty) {
      _lastRouterItem = navigationModel.routerItemHistory.last;
      notifyListeners();
    } else {
      _lastRouterItem = _initRouterItem;
      notifyListeners();
    }
  }
}

class NoAnimationTransitionDelegate extends TransitionDelegate<void> {
  @override
  Iterable<RouteTransitionRecord> resolve({
    required List<RouteTransitionRecord> newPageRouteHistory,
    required Map<RouteTransitionRecord?, RouteTransitionRecord> locationToExitingPageRoute,
    required Map<RouteTransitionRecord?, List<RouteTransitionRecord>> pageRouteToPagelessRoutes,
  }) {
    final results = <RouteTransitionRecord>[];

    for (final pageRoute in newPageRouteHistory) {
      if (pageRoute.isWaitingForEnteringDecision) {
        pageRoute.markForAdd();
      }
      results.add(pageRoute);
    }
    for (final exitingPageRoute in locationToExitingPageRoute.values) {
      if (exitingPageRoute.isWaitingForExitingDecision) {
        exitingPageRoute.markForRemove();
        final pagelessRoutes = pageRouteToPagelessRoutes[exitingPageRoute];
        if (pagelessRoutes != null) {
          for (final pagelessRoute in pagelessRoutes) {
            pagelessRoute.markForRemove();
          }
        }
      }
      results.add(exitingPageRoute);
    }
    return results;
  }
}
