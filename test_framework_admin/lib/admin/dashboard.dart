import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_admin/admin/system/menu.dart';
import 'package:test_framework_admin/model/model.dart';

class AdminDashboardWidget extends StatefulWidget {
  const AdminDashboardWidget({Key? key}) : super(key: key);

  @override
  State<AdminDashboardWidget> createState() => _AdminDashboardWidgetState();
}

class _AdminDashboardWidgetState extends State<AdminDashboardWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Admin"),
        actions: [
          IconButton(
              onPressed: () {
                Provider.of<SecurityModel>(context, listen: false).logout();
              },
              icon: const Icon(Icons.logout))
        ],
      ),
      drawer: const Drawer(child: MenuWidget()),
      body: Container(),
    );
  }
}
