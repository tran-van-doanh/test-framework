import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_admin/admin/system/menu.dart';
import 'package:test_framework_admin/api.dart';
import 'package:test_framework_admin/common/dynamic_form.dart';
import 'package:test_framework_admin/common/dynamic_table.dart';
import 'package:test_framework_admin/common/reloadable_state.dart';
import 'package:test_framework_admin/model/model.dart';

class AdminSysOrganizationHostnameListWidget extends StatefulWidget {
  const AdminSysOrganizationHostnameListWidget({Key? key}) : super(key: key);

  @override
  State<AdminSysOrganizationHostnameListWidget> createState() => _AdminSysOrganizationHostnameListState();
}

class _AdminSysOrganizationHostnameListState extends ReloadableState<AdminSysOrganizationHostnameListWidget> {
  dynamic formConfig = {"type": "column", "children": []};
  dynamic optionListMap = {};
  var sysOrganizationHostnameSearchRequest = {};
  var sysOrganizationHostnameRowCount = 0;
  var sysOrganizationHostnameCurrentPage = 1;
  var sysOrganizationHostnameResultList = [];
  var sysOrganizationHostnameRowPerPage = 25;
  Widget paging = Container();

  @override
  void initState() {
    super.initState();
    reset();
  }

  @override
  refreshData() {
    sysOrganizationHostnamePageChange(sysOrganizationHostnameCurrentPage);
  }

  reset() async {
    await resetForm();
    await sysOrganizationHostnameSearch({});
  }

  resetForm() async {
    optionListMap = {
      "status": [
        {"value": 1, "label": "Active"},
        {"value": 0, "label": "Inactive"}
      ],
      "sysOrganizationId": []
    };
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var sysOrganizationFindResponse =
        await httpPost("/test-framework-api/admin/sys/sys-organization/search", {"status": 1}, securityModel.authorization);
    if (sysOrganizationFindResponse.containsKey("body") && sysOrganizationFindResponse["body"] is String == false) {
      optionListMap["sysOrganizationId"] = sysOrganizationFindResponse["body"]["resultList"]
          .map((sysOrganization) => {"value": sysOrganization["id"], "label": sysOrganization["name"]})
          .toList();
    }
    formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "sysOrganizationId", "label": "Organization", "hintText": "Organization input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "hostnameLike", "label": "Hostname", "hintText": "Hostname input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "action", "actionName": "search", "actionLabel": "Search"}
          ]
        }
      ]
    };
    setState(() {});
  }

  sysOrganizationHostnameSearch(sysOrganizationHostnameSearchRequest) async {
    this.sysOrganizationHostnameSearchRequest = sysOrganizationHostnameSearchRequest;
    await sysOrganizationHostnamePageChange(1);
  }

  sysOrganizationHostnameActionHandler(actionName, formData) async {
    if (actionName == "search") {
      sysOrganizationHostnameSearchRequest = formData;
      await sysOrganizationHostnamePageChange(1);
    }
  }

  sysOrganizationHostnamePageChange(page) async {
    if ((page - 1) * sysOrganizationHostnameRowPerPage > sysOrganizationHostnameRowCount) {
      page = (1.0 * sysOrganizationHostnameRowCount / sysOrganizationHostnameRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * sysOrganizationHostnameRowPerPage, "queryLimit": sysOrganizationHostnameRowPerPage};
    if (sysOrganizationHostnameSearchRequest["hostnameLike"] != null && sysOrganizationHostnameSearchRequest["hostnameLike"].isNotEmpty) {
      findRequest["hostnameLike"] = "%${sysOrganizationHostnameSearchRequest["hostnameLike"]}%";
    }
    if (sysOrganizationHostnameSearchRequest["sysOrganizationId"] != null && sysOrganizationHostnameSearchRequest["sysOrganizationId"].isNotEmpty) {
      findRequest["sysOrganizationId"] = sysOrganizationHostnameSearchRequest["sysOrganizationId"];
    }
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var response = await httpPost("/test-framework-api/admin/sys/sys-organization-hostname/search", findRequest, securityModel.authorization);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        sysOrganizationHostnameCurrentPage = page;
        sysOrganizationHostnameRowCount = response["body"]["rowCount"];
        sysOrganizationHostnameResultList = response["body"]["resultList"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var sysOrganizationHostnameStatusMap = {1: "Active", 0: "Inactive"};
    var resultColumns = [
      const DataColumn(
        label: Text(
          'Hostname',
          overflow: TextOverflow.visible,
          softWrap: true,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Status',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Organization',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      DataColumn(
          label: Row(
        children: [
          ElevatedButton(
            onPressed: () {
              Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-organization-hostname/create");
            },
            child: const Text('Add'),
          )
        ],
      )),
    ];
    var resultRows = [
      for (var sysOrganizationHostname in sysOrganizationHostnameResultList)
        DataRow(
          cells: [
            DataCell(Text(sysOrganizationHostname["hostname"])),
            DataCell(Text(sysOrganizationHostnameStatusMap[sysOrganizationHostname["status"]] ?? "Not defined")),
            DataCell(Text(sysOrganizationHostname["sysOrganization"]["name"])),
            DataCell(Row(
              children: [
                ElevatedButton(
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false)
                        .navigate("/admin/system/sys-organization-hostname/edit/${sysOrganizationHostname["id"]}");
                  },
                  child: const Text('Edit'),
                ),
                ElevatedButton(
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.red)),
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false)
                        .navigate("/admin/system/sys-organization-hostname/delete/${sysOrganizationHostname["id"]}");
                  },
                  child: const Text('Delete'),
                ),
              ],
            )),
          ],
        )
    ];

    return Scaffold(
        appBar: AppBar(
          title: const Text("Hostname list"),
          actions: [
            IconButton(
                onPressed: () {
                  Provider.of<SecurityModel>(context, listen: false).logout();
                },
                icon: const Icon(Icons.logout))
          ],
        ),
        drawer: const Drawer(child: MenuWidget()),
        body: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(mainAxisSize: MainAxisSize.min, children: [
                        DynamicForm(
                            formConfig: formConfig,
                            optionListMap: optionListMap,
                            formData: sysOrganizationHostnameSearchRequest,
                            actionHandler: sysOrganizationHostnameActionHandler),
                        Row(mainAxisSize: MainAxisSize.max, children: [
                          Expanded(
                              child: DynamicTable(
                            columns: resultColumns,
                            rows: resultRows,
                          ))
                        ]),
                        DynamicTablePagging(
                          sysOrganizationHostnameRowCount,
                          sysOrganizationHostnameCurrentPage,
                          sysOrganizationHostnameRowPerPage,
                          pageChangeHandler: (page) {
                            sysOrganizationHostnamePageChange(page);
                          },
                          rowPerPageChangeHandler: (rowPerPage) {
                            sysOrganizationHostnameRowPerPage = rowPerPage ?? 10;
                            sysOrganizationHostnamePageChange(1);
                          },
                        )
                      ])),
                )
              ],
            )));
  }
}

class AdminSysOrganizationHostnameDetailWidget extends StatefulWidget {
  final String? sysOrganizationHostnameId;
  final String sysOrganizationHostnameAction;
  const AdminSysOrganizationHostnameDetailWidget({required this.sysOrganizationHostnameAction, this.sysOrganizationHostnameId, Key? key})
      : super(key: key);

  @override
  State<AdminSysOrganizationHostnameDetailWidget> createState() => _AdminSysOrganizationHostnameDetailState();
}

class _AdminSysOrganizationHostnameDetailState extends ReloadableState<AdminSysOrganizationHostnameDetailWidget> {
  dynamic formConfig = {
    "type": "column",
    "children": [
      {
        "type": "row",
        "children": [
          {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
        ]
      }
    ]
  };
  dynamic optionListMap = {};
  Map<dynamic, dynamic>? sysOrganizationHostname;
  String? error;
  var hasEditPermission = false;

  @override
  void initState() {
    super.initState();
    var userPermList = Provider.of<SecurityModel>(context, listen: false).userPermList;
    if (userPermList != null) {
      hasEditPermission = userPermList.where((permCode) => permCode == "ADMIN").isNotEmpty;
    }
    reload();
  }

  @override
  refreshData() {}

  reload() async {
    if (widget.sysOrganizationHostnameId != null) {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      var sysOrganizationHostnameGetResponse =
          await httpGet("/test-framework-api/admin/sys/sys-organization-hostname/${widget.sysOrganizationHostnameId}", securityModel.authorization);
      if (sysOrganizationHostnameGetResponse.containsKey("body") && sysOrganizationHostnameGetResponse["body"] is String == false) {
        sysOrganizationHostname = sysOrganizationHostnameGetResponse["body"]["result"];
      }
    } else {
      sysOrganizationHostname = {"status": 0};
    }
    await resetForm();
  }

  resetForm() async {
    var submitActionLabel = "Submit";
    var actionSubmitType = "primary";
    if (widget.sysOrganizationHostnameAction == "delete") {
      submitActionLabel = "Delete";
      actionSubmitType = "danger";
    }
    optionListMap = {
      "status": [
        {"value": 1, "label": "Active"},
        {"value": 0, "label": "Inactive"}
      ],
      "sysOrganizationId": []
    };
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var sysOrganizationFindResponse =
        await httpPost("/test-framework-api/admin/sys/sys-organization/search", {"status": 1}, securityModel.authorization);
    if (sysOrganizationFindResponse.containsKey("body") && sysOrganizationFindResponse["body"] is String == false) {
      optionListMap["sysOrganizationId"] = sysOrganizationFindResponse["body"]["resultList"]
          .map((sysOrganization) => {"value": sysOrganization["id"], "label": sysOrganization["name"]})
          .toList();
    }

    formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "sysOrganizationId", "label": "Organization", "hintText": "Organization input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "hostname", "label": "Hostname", "hintText": "Hostname input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "description", "label": "description", "hintText": "description input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "status", "label": "status", "hintText": "status input"}
          ]
        },
        {
          "type": "row",
          "children": hasEditPermission
              ? [
                  {"type": "action", "actionType": actionSubmitType, "actionName": "submit", "actionLabel": submitActionLabel},
                  {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
                ]
              : [
                  {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
                ]
        }
      ]
    };
    setState(() {});
  }

  actionHandler(actionName, formData) async {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    if (actionName == "submit") {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      if (widget.sysOrganizationHostnameAction == "create") {
        var sysOrganizationHostnameCreateResponse =
            await httpPut("/test-framework-api/admin/sys/sys-organization-hostname", formData, securityModel.authorization);
        if (sysOrganizationHostnameCreateResponse.containsKey("body") && sysOrganizationHostnameCreateResponse["body"] is String == false) {
          var sysOrganizationHostnameCreateResponseBody = sysOrganizationHostnameCreateResponse["body"];
          if (sysOrganizationHostnameCreateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (sysOrganizationHostnameCreateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(sysOrganizationHostnameCreateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.sysOrganizationHostnameAction == "edit") {
        var sysOrganizationHostnameUpdateResponse = await httpPost(
            "/test-framework-api/admin/sys/sys-organization-hostname/${widget.sysOrganizationHostnameId}", formData, securityModel.authorization);
        if (sysOrganizationHostnameUpdateResponse.containsKey("body") && sysOrganizationHostnameUpdateResponse["body"] is String == false) {
          var sysOrganizationHostnameUpdateResponseBody = sysOrganizationHostnameUpdateResponse["body"];
          if (sysOrganizationHostnameUpdateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (sysOrganizationHostnameUpdateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(sysOrganizationHostnameUpdateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.sysOrganizationHostnameAction == "delete") {
        var sysOrganizationHostnameDeleteResponse = await httpDelete(
            "/test-framework-api/admin/sys/sys-organization-hostname/${widget.sysOrganizationHostnameId}", securityModel.authorization);
        if (sysOrganizationHostnameDeleteResponse.containsKey("body") && sysOrganizationHostnameDeleteResponse["body"] is String == false) {
          var sysOrganizationHostnameDeleteResponseBody = sysOrganizationHostnameDeleteResponse["body"];
          if (sysOrganizationHostnameDeleteResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (sysOrganizationHostnameDeleteResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(sysOrganizationHostnameDeleteResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
    }
    if (actionName == "back") {
      navigationModel.pop("/admin/system/sys-organization-hostname");
    }
  }

  @override
  Widget build(BuildContext context) {
    if (error != null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Error'),
          ),
          body: Center(child: Text("Error $error")));
    }
    if (sysOrganizationHostname == null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Loading'),
          ),
          body: const Center(child: CircularProgressIndicator()));
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hostname detail'),
      ),
      drawer: const Drawer(child: MenuWidget()),
      body: Padding(
          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(mainAxisSize: MainAxisSize.min, children: [
                      Card(
                          child: Column(mainAxisSize: MainAxisSize.min, children: [
                        const Row(
                          children: [
                            Text(
                              "Detail",
                              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        formConfig != null && optionListMap != null
                            ? DynamicForm(
                                formConfig: formConfig,
                                formData: sysOrganizationHostname!,
                                optionListMap: optionListMap,
                                actionHandler: actionHandler)
                            : Container()
                      ]))
                    ])),
              )
            ],
          )),
    );
  }
}
