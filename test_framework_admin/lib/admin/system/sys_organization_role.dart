import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_admin/admin/system/menu.dart';
import 'package:test_framework_admin/api.dart';
import 'package:test_framework_admin/common/dynamic_form.dart';
import 'package:test_framework_admin/common/dynamic_table.dart';
import 'package:test_framework_admin/common/reloadable_state.dart';
import 'package:test_framework_admin/common/tree.dart';
import 'package:test_framework_admin/model/model.dart';

class AdminSysOrganizationRoleListWidget extends StatefulWidget {
  const AdminSysOrganizationRoleListWidget({Key? key}) : super(key: key);

  @override
  State<AdminSysOrganizationRoleListWidget> createState() => _AdminSysOrganizationRoleListState();
}

class _AdminSysOrganizationRoleListState extends ReloadableState<AdminSysOrganizationRoleListWidget> {
  dynamic formConfig = {"type": "column", "children": []};
  var sysOrganizationRoleSearchRequest = {};
  var sysOrganizationRoleRowCount = 0;
  var sysOrganizationRoleCurrentPage = 1;
  var sysOrganizationRoleResultList = [];
  var sysOrganizationRoleRowPerPage = 25;
  dynamic optionListMap = {};
  Widget paging = Container();

  @override
  void initState() {
    super.initState();
    reset();
  }

  reset() async {
    await resetForm();
    sysOrganizationRoleSearch({});
  }

  resetForm() async {
    optionListMap = {
      "status": [
        {"value": 1, "label": "Active"},
        {"value": 0, "label": "Inactive"}
      ],
      "sysOrganizationId": []
    };
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var sysOrganizationFindResponse =
        await httpPost("/test-framework-api/admin/sys/sys-organization/search", {"status": 1}, securityModel.authorization);
    if (sysOrganizationFindResponse.containsKey("body") && sysOrganizationFindResponse["body"] is String == false) {
      optionListMap["sysOrganizationId"] = sysOrganizationFindResponse["body"]["resultList"]
          .map((sysOrganization) => {"value": sysOrganization["id"], "label": sysOrganization["name"]})
          .toList();
    }
    formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "sysOrganizationId", "label": "Organization", "hintText": "Organization input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "code", "label": "Code", "hintText": "Code input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "name", "label": "Name", "hintText": "Name input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "action", "actionName": "search", "actionLabel": "Search"}
          ]
        }
      ]
    };
    setState(() {});
  }

  @override
  refreshData() {
    sysOrganizationRolePageChange(sysOrganizationRoleCurrentPage);
  }

  sysOrganizationRoleSearch(sysOrganizationRoleSearchRequest) async {
    this.sysOrganizationRoleSearchRequest = sysOrganizationRoleSearchRequest;
    await sysOrganizationRolePageChange(1);
  }

  sysOrganizationRoleActionHandler(actionName, formData) async {
    if (actionName == "search") {
      sysOrganizationRoleSearchRequest = formData;
      await sysOrganizationRolePageChange(1);
    }
  }

  sysOrganizationRolePageChange(page) async {
    if ((page - 1) * sysOrganizationRoleRowPerPage > sysOrganizationRoleRowCount) {
      page = (1.0 * sysOrganizationRoleRowCount / sysOrganizationRoleRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * sysOrganizationRoleRowPerPage, "queryLimit": sysOrganizationRoleRowPerPage};
    if (sysOrganizationRoleSearchRequest["codeLike"] != null && sysOrganizationRoleSearchRequest["codeLike"].isNotEmpty) {
      findRequest["codeLike"] = "%${sysOrganizationRoleSearchRequest["codeLike"]}%";
    }
    if (sysOrganizationRoleSearchRequest["nameLike"] != null && sysOrganizationRoleSearchRequest["nameLike"].isNotEmpty) {
      findRequest["nameLike"] = "%${sysOrganizationRoleSearchRequest["nameLike"]}%";
    }
    if (sysOrganizationRoleSearchRequest["sysOrganizationId"] != null && sysOrganizationRoleSearchRequest["sysOrganizationId"].isNotEmpty) {
      findRequest["sysOrganizationId"] = sysOrganizationRoleSearchRequest["sysOrganizationId"];
    }
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var response = await httpPost("/test-framework-api/admin/sys/sys-item-role/search", findRequest, securityModel.authorization);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        sysOrganizationRoleCurrentPage = page;
        sysOrganizationRoleRowCount = response["body"]["rowCount"];
        sysOrganizationRoleResultList = response["body"]["resultList"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var sysOrganizationRoleStatusMap = {1: "Active", 0: "Inactive"};
    var resultColumns = [
      const DataColumn(
        label: Text(
          'Type',
          overflow: TextOverflow.visible,
          softWrap: true,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Code',
          overflow: TextOverflow.visible,
          softWrap: true,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Name',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Status',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      DataColumn(
          label: Row(
        children: [
          ElevatedButton(
            onPressed: () {
              Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-organization-role/create");
            },
            child: const Text('Add'),
          )
        ],
      )),
    ];
    var resultRows = [
      for (var sysOrganizationRole in sysOrganizationRoleResultList)
        DataRow(
          cells: [
            DataCell(Text(sysOrganizationRole["itemType"])),
            DataCell(Text(sysOrganizationRole["code"])),
            DataCell(Text(sysOrganizationRole["name"])),
            DataCell(Text(sysOrganizationRoleStatusMap[sysOrganizationRole["status"]] ?? "Not defined")),
            DataCell(Row(
              children: [
                ElevatedButton(
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false)
                        .navigate("/admin/system/sys-organization-role/edit/${sysOrganizationRole["id"]}");
                  },
                  child: const Text('Edit'),
                ),
                ElevatedButton(
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.red)),
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false)
                        .navigate("/admin/system/sys-organization-role/delete/${sysOrganizationRole["id"]}");
                  },
                  child: const Text('Delete'),
                ),
              ],
            )),
          ],
        )
    ];

    return Scaffold(
        appBar: AppBar(
          title: const Text("Role list"),
          actions: [
            IconButton(
                onPressed: () {
                  Provider.of<SecurityModel>(context, listen: false).logout();
                },
                icon: const Icon(Icons.logout))
          ],
        ),
        drawer: const Drawer(child: MenuWidget()),
        body: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(mainAxisSize: MainAxisSize.min, children: [
                        DynamicForm(
                            formConfig: formConfig,
                            optionListMap: optionListMap,
                            formData: sysOrganizationRoleSearchRequest,
                            actionHandler: sysOrganizationRoleActionHandler),
                        Row(mainAxisSize: MainAxisSize.max, children: [
                          Expanded(
                              child: DynamicTable(
                            columns: resultColumns,
                            rows: resultRows,
                          ))
                        ]),
                        DynamicTablePagging(
                          sysOrganizationRoleRowCount,
                          sysOrganizationRoleCurrentPage,
                          sysOrganizationRoleRowPerPage,
                          pageChangeHandler: (page) {
                            sysOrganizationRolePageChange(page);
                          },
                          rowPerPageChangeHandler: (rowPerPage) {
                            sysOrganizationRoleRowPerPage = rowPerPage ?? 10;
                            sysOrganizationRolePageChange(1);
                          },
                        )
                      ])),
                )
              ],
            )));
  }
}

class AdminSysOrganizationRoleDetailWidget extends StatefulWidget {
  final String? sysOrganizationRoleId;
  final String sysOrganizationRoleAction;
  const AdminSysOrganizationRoleDetailWidget({required this.sysOrganizationRoleAction, this.sysOrganizationRoleId, Key? key}) : super(key: key);

  @override
  State<AdminSysOrganizationRoleDetailWidget> createState() => _AdminSysOrganizationRoleDetailState();
}

class _AdminSysOrganizationRoleDetailState extends ReloadableState<AdminSysOrganizationRoleDetailWidget> {
  dynamic formConfig = {
    "type": "column",
    "children": [
      {
        "type": "row",
        "children": [
          {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
        ]
      }
    ]
  };
  dynamic optionListMap = {};
  Map<dynamic, dynamic>? sysOrganizationRole;
  TreeData? sysPermTreeData;
  String? error;
  var hasEditPermission = false;

  @override
  void initState() {
    super.initState();
    var userPermList = Provider.of<SecurityModel>(context, listen: false).userPermList;
    if (userPermList != null) {
      hasEditPermission = userPermList.where((permCode) => permCode == "ADMIN").isNotEmpty;
    }
    reload();
  }

  @override
  refreshData() {}

  reload() async {
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var sysPermFindRequest = {"permType": "organization", "status": 1};
    var sysPermFindResponse = await httpPost("/test-framework-api/admin/sys/sys-perm/search", sysPermFindRequest, securityModel.authorization);
    if (sysPermFindResponse.containsKey("body") && sysPermFindResponse["body"] is String == false) {
      var sysPermList = sysPermFindResponse["body"]["resultList"];
      List<TreeNode> treeNodeList = [];
      for (var sysPerm in sysPermList) {
        treeNodeList.add(TreeNode(key: sysPerm["id"], parentKey: sysPerm["parentId"], item: sysPerm));
      }
      sysPermTreeData = TreeData(treeNodeList);
    }
    if (widget.sysOrganizationRoleId != null) {
      var sysOrganizationRoleGetResponse =
          await httpGet("/test-framework-api/admin/sys/sys-item-role/${widget.sysOrganizationRoleId}", securityModel.authorization);
      if (sysOrganizationRoleGetResponse.containsKey("body") && sysOrganizationRoleGetResponse["body"] is String == false) {
        sysOrganizationRole = sysOrganizationRoleGetResponse["body"]["result"];
      }
    } else {
      sysOrganizationRole = {"status": 0};
    }
    await resetForm();
  }

  resetForm() async {
    var submitActionLabel = "Submit";
    var actionSubmitType = "primary";
    if (widget.sysOrganizationRoleAction == "delete") {
      submitActionLabel = "Delete";
      actionSubmitType = "danger";
    }
    optionListMap = {
      "status": [
        {"value": 1, "label": "Active"},
        {"value": 0, "label": "Inactive"}
      ],
      "sysOrganizationId": []
    };
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var sysOrganizationFindResponse =
        await httpPost("/test-framework-api/admin/sys/sys-organization/search", {"status": 1}, securityModel.authorization);
    if (sysOrganizationFindResponse.containsKey("body") && sysOrganizationFindResponse["body"] is String == false) {
      optionListMap["sysOrganizationId"] = sysOrganizationFindResponse["body"]["resultList"]
          .map((sysOrganization) => {"value": sysOrganization["id"], "label": sysOrganization["name"]})
          .toList();
    }
    formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "sysOrganizationId", "label": "Organization", "hintText": "Organization input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "code", "label": "Code", "hintText": "Code input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "name", "label": "Name", "hintText": "Name input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "description", "label": "description", "hintText": "description input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "status", "label": "status", "hintText": "status input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {
              "type": "treeMultipleSelect",
              "expanded": true,
              "fieldName": "sysPermIdList",
              "label": "Perm",
              "hintText": "Perm input",
              "getTreeNodeLabel": (treeNode) => "${treeNode.item["code"]} - ${treeNode.item["name"]}",
              "treeData": sysPermTreeData
            }
          ]
        },
        {
          "type": "row",
          "children": hasEditPermission
              ? [
                  {"type": "action", "actionType": actionSubmitType, "actionName": "submit", "actionLabel": submitActionLabel},
                  {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
                ]
              : [
                  {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
                ]
        }
      ]
    };
    setState(() {});
  }

  actionHandler(actionName, formData) async {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    if (actionName == "submit") {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      if (widget.sysOrganizationRoleAction == "create") {
        var sysOrganizationRoleCreateResponse = await httpPut("/test-framework-api/admin/sys/sys-item-role", formData, securityModel.authorization);
        if (sysOrganizationRoleCreateResponse.containsKey("body") && sysOrganizationRoleCreateResponse["body"] is String == false) {
          var sysOrganizationRoleCreateResponseBody = sysOrganizationRoleCreateResponse["body"];
          if (sysOrganizationRoleCreateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (sysOrganizationRoleCreateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(sysOrganizationRoleCreateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.sysOrganizationRoleAction == "edit") {
        var sysOrganizationRoleUpdateResponse =
            await httpPost("/test-framework-api/admin/sys/sys-item-role/${widget.sysOrganizationRoleId}", formData, securityModel.authorization);
        if (sysOrganizationRoleUpdateResponse.containsKey("body") && sysOrganizationRoleUpdateResponse["body"] is String == false) {
          var sysOrganizationRoleUpdateResponseBody = sysOrganizationRoleUpdateResponse["body"];
          if (sysOrganizationRoleUpdateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (sysOrganizationRoleUpdateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(sysOrganizationRoleUpdateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.sysOrganizationRoleAction == "delete") {
        var sysOrganizationRoleDeleteResponse =
            await httpDelete("/test-framework-api/admin/sys/sys-item-role/${widget.sysOrganizationRoleId}", securityModel.authorization);
        if (sysOrganizationRoleDeleteResponse.containsKey("body") && sysOrganizationRoleDeleteResponse["body"] is String == false) {
          var sysOrganizationRoleDeleteResponseBody = sysOrganizationRoleDeleteResponse["body"];
          if (sysOrganizationRoleDeleteResponseBody.containsKey("result") && sysOrganizationRoleDeleteResponseBody["result"] is String == false) {
            actionName = "back";
          } else if (sysOrganizationRoleDeleteResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(sysOrganizationRoleDeleteResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
    }
    if (actionName == "back") {
      navigationModel.pop("/admin/system/sys-organization-role");
    }
  }

  @override
  Widget build(BuildContext context) {
    if (error != null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Error'),
          ),
          body: Center(child: Text("Error $error")));
    }
    if (sysOrganizationRole == null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Loading'),
          ),
          body: const Center(child: CircularProgressIndicator()));
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text('Role detail'),
      ),
      drawer: const Drawer(child: MenuWidget()),
      body: Padding(
          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(mainAxisSize: MainAxisSize.min, children: [
                      Card(
                          child: Column(mainAxisSize: MainAxisSize.min, children: [
                        const Row(
                          children: [
                            Text(
                              "Detail",
                              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        formConfig != null && optionListMap != null
                            ? DynamicForm(
                                formConfig: formConfig, formData: sysOrganizationRole!, optionListMap: optionListMap, actionHandler: actionHandler)
                            : Container()
                      ]))
                    ])),
              )
            ],
          )),
    );
  }
}
