import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_admin/common/reloadable_state.dart';
import 'package:test_framework_admin/model/model.dart';

class MenuWidget extends StatefulWidget {
  const MenuWidget({Key? key}) : super(key: key);

  @override
  State<MenuWidget> createState() => _MenuState();
}

class _MenuState extends ReloadableState<MenuWidget> {
  @override
  refreshData() {}

  @override
  Widget build(BuildContext context) {
    var securityModel = Provider.of<SecurityModel>(context, listen: true);
    var hasAdminPerm = securityModel.hasUserPermission("ADMIN");
    return ListView(
      padding: EdgeInsets.zero,
      children: <Widget>[
        const DrawerHeader(
          decoration: BoxDecoration(
            color: Colors.blue,
          ),
          child: Text(
            'Drawer Header',
            style: TextStyle(
              color: Colors.white,
              fontSize: 24,
            ),
          ),
        ),
        hasAdminPerm && securityModel.hasUserPermission("ADMIN_SERVER")
            ? ListTile(
                title: const Text("Run command"),
                onTap: () {
                  Provider.of<NavigationModel>(context, listen: false).navigate("/admin/server/run-command");
                },
              )
            : Container(),
        hasAdminPerm && securityModel.hasUserPermission("ADMIN_PERM")
            ? ListTile(
                title: const Text("SysPerm"),
                onTap: () {
                  Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-perm");
                },
              )
            : Container(),
        hasAdminPerm && securityModel.hasUserPermission("ADMIN_ROLE")
            ? ListTile(
                title: const Text("SysRole"),
                onTap: () {
                  Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-role");
                },
              )
            : Container(),
        hasAdminPerm && securityModel.hasUserPermission("ADMIN_USER")
            ? ListTile(
                title: const Text("SysUser"),
                onTap: () {
                  Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-user");
                },
              )
            : Container(),
        hasAdminPerm
            ? ListTile(
                title: const Text("SysUserProject"),
                onTap: () {
                  Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-user-project");
                },
              )
            : Container(),
        hasAdminPerm && securityModel.hasUserPermission("ADMIN_ORGANIZATION")
            ? ListTile(
                title: const Text("SysOrganization"),
                onTap: () {
                  Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-organization");
                },
              )
            : Container(),
        hasAdminPerm && securityModel.hasUserPermission("ADMIN_ORGANIZATION")
            ? ListTile(
                title: const Text("SysOrganizationHostname"),
                onTap: () {
                  Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-organization-hostname");
                },
              )
            : Container(),
        hasAdminPerm && securityModel.hasUserPermission("ADMIN_ORGANIZATION_ROLE")
            ? ListTile(
                title: const Text("SysOrganizationRole"),
                onTap: () {
                  Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-organization-role");
                },
              )
            : Container(),
        hasAdminPerm && securityModel.hasUserPermission("ORGANIZATION_PUBLIC_NODE")
            ? ListTile(
                title: const Text("SysNode"),
                onTap: () {
                  Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-node");
                },
              )
            : Container(),
        hasAdminPerm && securityModel.hasUserPermission("ORGANIZATION_PUBLIC_NODE")
            ? ListTile(
                title: const Text("SysNodeFile"),
                onTap: () {
                  Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-node-file");
                },
              )
            : Container(),
        hasAdminPerm && securityModel.hasUserPermission("ORGANIZATION_VALUE_TYPE")
            ? ListTile(
                title: const Text("TfValueType"),
                onTap: () {
                  Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/tf-value-type");
                },
              )
            : Container(),
        hasAdminPerm && securityModel.hasUserPermission("ORGANIZATION_VALUE_TEMPLATE")
            ? ListTile(
                title: const Text("TfValueTemplate"),
                onTap: () {
                  Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/tf-value-template");
                },
              )
            : Container(),
        hasAdminPerm && securityModel.hasUserPermission("ADMIN_USER")
            ? ListTile(
                title: const Text("TfFindElementTemplate"),
                onTap: () {
                  Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/tf-find-element-template");
                },
              )
            : Container(),
        hasAdminPerm && securityModel.hasUserPermission("ADMIN_DOC")
            ? ListTile(
                title: const Text("DocFile"),
                onTap: () {
                  Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/doc-file");
                },
              )
            : Container(),
        hasAdminPerm && securityModel.hasUserPermission("ADMIN_DOC")
            ? ListTile(
                title: const Text("DocDocument"),
                onTap: () {
                  Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/doc-document");
                },
              )
            : Container(),
        hasAdminPerm && securityModel.hasUserPermission("ADMIN_REPORT")
            ? ListTile(
                title: const Text("RptReport"),
                onTap: () {
                  Provider.of<NavigationModel>(context, listen: false).navigate("/admin/report/rpt-report");
                },
              )
            : Container(),
        hasAdminPerm && securityModel.hasUserPermission("ADMIN_REPORT")
            ? ListTile(
                title: const Text("RptDashboard"),
                onTap: () {
                  Provider.of<NavigationModel>(context, listen: false).navigate("/admin/report/rpt-dashboard");
                },
              )
            : Container(),
        hasAdminPerm && securityModel.hasUserPermission("ADMIN_EDITOR_COMPONENT")
            ? ListTile(
                title: const Text("EditorComponent"),
                onTap: () {
                  Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/editor-component");
                },
              )
            : Container(),
        hasAdminPerm && securityModel.hasUserPermission("ADMIN_MULTILINGUAL")
            ? ListTile(
                title: const Text("SysMultiLanguage"),
                onTap: () {
                  Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-multi-language");
                },
              )
            : Container(),
        hasAdminPerm && securityModel.hasUserPermission("TASK_TYPE")
            ? ListTile(
                title: const Text("TaskType"),
                onTap: () {
                  Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/task-type");
                },
              )
            : Container(),
        hasAdminPerm && securityModel.hasUserPermission("ADMIN_DOC_INSTRUCTION_MANUALS")
            ? ListTile(
                title: const Text("Doc Instruction Manuals"),
                onTap: () {
                  Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/doc-instruction-manuals");
                },
              )
            : Container(),
      ],
    );
  }
}
