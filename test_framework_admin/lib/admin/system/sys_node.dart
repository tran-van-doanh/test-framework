import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_admin/admin/system/menu.dart';
import 'package:test_framework_admin/api.dart';
import 'package:test_framework_admin/common/dynamic_form.dart';
import 'package:test_framework_admin/common/dynamic_table.dart';
import 'package:test_framework_admin/common/reloadable_state.dart';
import 'package:test_framework_admin/common/tree.dart';
import 'package:test_framework_admin/model/model.dart';
import 'package:flutter/services.dart';

class AdminSysNodeListWidget extends StatefulWidget {
  const AdminSysNodeListWidget({Key? key}) : super(key: key);

  @override
  State<AdminSysNodeListWidget> createState() => _AdminSysNodeListState();
}

class _AdminSysNodeListState extends ReloadableState<AdminSysNodeListWidget> {
  var sysNodeSearchRequest = {};
  var sysNodeRowCount = 0;
  var sysNodeCurrentPage = 1;
  var sysNodeResultList = [];
  var sysNodeRowPerPage = 25;
  Widget paging = Container();

  @override
  void initState() {
    super.initState();
    sysNodeSearch({});
  }

  @override
  refreshData() {
    sysNodePageChange(sysNodeCurrentPage);
  }

  sysNodeSearch(sysNodeSearchRequest) async {
    this.sysNodeSearchRequest = sysNodeSearchRequest;
    await sysNodePageChange(1);
  }

  sysNodeActionHandler(actionName, formData) async {
    if (actionName == "search") {
      sysNodeSearchRequest = formData;
      await sysNodePageChange(1);
    }
  }

  sysNodePageChange(page) async {
    if ((page - 1) * sysNodeRowPerPage > sysNodeRowCount) {
      page = (1.0 * sysNodeRowCount / sysNodeRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * sysNodeRowPerPage, "queryLimit": sysNodeRowPerPage};
    if (sysNodeSearchRequest["nodeType"] != null && sysNodeSearchRequest["nodeType"].isNotEmpty) {
      findRequest["nodeType"] = sysNodeSearchRequest["nodeType"];
    }
    if (sysNodeSearchRequest["idLike"] != null && sysNodeSearchRequest["idLike"].isNotEmpty) {
      findRequest["idLike"] = "%${sysNodeSearchRequest["idLike"]}%";
    }
    if (sysNodeSearchRequest["nameLike"] != null && sysNodeSearchRequest["nameLike"].isNotEmpty) {
      findRequest["nameLike"] = "%${sysNodeSearchRequest["nameLike"]}%";
    }
    if (sysNodeSearchRequest["status"] != null) {
      findRequest["status"] = sysNodeSearchRequest["status"];
    }
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var response = await httpPost("/test-framework-api/user/sys/sys-node/search", findRequest, securityModel.authorization);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        sysNodeCurrentPage = page;
        sysNodeRowCount = response["body"]["rowCount"];
        sysNodeResultList = response["body"]["resultList"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var sysNodeTypeMap = {
      "private_all": "Private all in one node",
      "private_config": "Private config node",
      "private_build_run": "Private run node",
      "all": "Public all in one node",
      "config": "Public config node",
      "build": "Public build node",
      "run": "Public run node",
      "build_run": "Public build and run node",
    };
    var sysNodeStatusMap = {1: "Running", 0: "Stoped", -1: "Deleted"};
    var optionListMap = {
      "nodeType": [
        {"value": "private_all", "label": "Private all in one node"},
        {"value": "private_config", "label": "Private config node"},
        {"value": "private_build_run", "label": "Private run node"},
        {"value": "all", "label": "Public all in one node"},
        {"value": "config", "label": "Public config node"},
        {"value": "build", "label": "Public build node"},
        {"value": "run", "label": "Public run node"},
        {"value": "build_run", "label": "Public build and run node"},
      ],
      "status": [
        {"value": 1, "label": "Running"},
        {"value": 0, "label": "Stoped"},
        {"value": -1, "label": "Deleted"},
      ],
    };
    const formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "nodeType", "label": "Node type", "hintText": "Node type input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "idLike", "label": "Id", "hintText": "Id input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "nameLike", "label": "Name", "hintText": "Name input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "status", "label": "Status", "hintText": "Status input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "action", "actionName": "search", "actionLabel": "Search"}
          ]
        }
      ]
    };

    return Scaffold(
        appBar: AppBar(
          title: const Text("Node list"),
          actions: [
            IconButton(
                onPressed: () {
                  Provider.of<SecurityModel>(context, listen: false).logout();
                },
                icon: const Icon(Icons.logout))
          ],
        ),
        drawer: const Drawer(child: MenuWidget()),
        body: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(mainAxisSize: MainAxisSize.min, children: [
                        DynamicForm(
                            formConfig: formConfig,
                            optionListMap: optionListMap,
                            formData: sysNodeSearchRequest,
                            actionHandler: sysNodeActionHandler),
                        LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
                          if (constraints.maxWidth < 600) {
                            return Column(
                              children: [
                                for (var sysNode in sysNodeResultList)
                                  Column(children: [
                                    Row(children: [
                                      const Text(
                                        "ID: ",
                                        style: TextStyle(fontWeight: FontWeight.bold),
                                      ),
                                      Text(sysNode["id"])
                                    ]),
                                    Row(children: [
                                      const Text(
                                        "Node type: ",
                                        style: TextStyle(fontWeight: FontWeight.bold),
                                      ),
                                      Text(sysNodeTypeMap[sysNode["nodeType"]] ?? "Not defined")
                                    ]),
                                    Row(children: [
                                      const Text(
                                        "Name: ",
                                        style: TextStyle(fontWeight: FontWeight.bold),
                                      ),
                                      Text(sysNode["name"] ?? "")
                                    ]),
                                    Row(children: [
                                      const Text(
                                        "Status: ",
                                        style: TextStyle(fontWeight: FontWeight.bold),
                                      ),
                                      Text(sysNodeStatusMap[sysNode["status"]] ?? "Not defined")
                                    ]),
                                    Row(
                                      children: [
                                        Expanded(
                                            child: ElevatedButton(
                                          onPressed: () {
                                            Provider.of<NavigationModel>(context, listen: false)
                                                .navigate("/admin/system/sys-node/token/${sysNode["id"]}");
                                          },
                                          child: const Text('Token'),
                                        )),
                                        Expanded(
                                            child: ElevatedButton(
                                          onPressed: () {
                                            Provider.of<NavigationModel>(context, listen: false)
                                                .navigate("/admin/system/sys-node/edit/${sysNode["id"]}");
                                          },
                                          child: const Text('Edit'),
                                        )),
                                        Expanded(
                                            child: ElevatedButton(
                                          style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.red)),
                                          onPressed: () {
                                            Provider.of<NavigationModel>(context, listen: false)
                                                .navigate("/admin/system/sys-node/delete/${sysNode["id"]}");
                                          },
                                          child: const Text('Delete'),
                                        )),
                                      ],
                                    )
                                  ])
                              ],
                            );
                          } else {
                            var resultColumns = [
                              const DataColumn(
                                label: Text(
                                  'Id',
                                  overflow: TextOverflow.visible,
                                  softWrap: true,
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              const DataColumn(
                                label: Text(
                                  'Type',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              const DataColumn(
                                label: Text(
                                  'Name',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              const DataColumn(
                                label: Text(
                                  'Status',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              DataColumn(
                                  label: Row(
                                children: [
                                  ElevatedButton(
                                    onPressed: () {
                                      Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-node/create");
                                    },
                                    child: const Text('Add'),
                                  )
                                ],
                              )),
                            ];
                            var resultRows = [
                              for (var sysNode in sysNodeResultList)
                                DataRow(
                                  cells: [
                                    DataCell(Text(sysNode["id"])),
                                    DataCell(Text(sysNodeTypeMap[sysNode["nodeType"]] ?? "Not defined")),
                                    DataCell(Text(sysNode["name"] ?? "")),
                                    DataCell(Text(sysNodeStatusMap[sysNode["status"]] ?? "Not defined")),
                                    DataCell(Row(
                                      children: [
                                        ElevatedButton(
                                          onPressed: () {
                                            Provider.of<NavigationModel>(context, listen: false)
                                                .navigate("/admin/system/sys-node/token/${sysNode["id"]}");
                                          },
                                          child: const Text('Token'),
                                        ),
                                        ElevatedButton(
                                          onPressed: () {
                                            Provider.of<NavigationModel>(context, listen: false)
                                                .navigate("/admin/system/sys-node/edit/${sysNode["id"]}");
                                          },
                                          child: const Text('Edit'),
                                        ),
                                        ElevatedButton(
                                          style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.red)),
                                          onPressed: () {
                                            Provider.of<NavigationModel>(context, listen: false)
                                                .navigate("/admin/system/sys-node/delete/${sysNode["id"]}");
                                          },
                                          child: const Text('Delete'),
                                        ),
                                      ],
                                    )),
                                  ],
                                )
                            ];
                            return Row(mainAxisSize: MainAxisSize.max, children: [
                              Expanded(
                                  child: DynamicTable(
                                minWidth: 1280,
                                columns: resultColumns,
                                rows: resultRows,
                              ))
                            ]);
                          }
                        }),
                        DynamicTablePagging(
                          sysNodeRowCount,
                          sysNodeCurrentPage,
                          sysNodeRowPerPage,
                          pageChangeHandler: (page) {
                            sysNodePageChange(page);
                          },
                          rowPerPageChangeHandler: (rowPerPage) {
                            sysNodeRowPerPage = rowPerPage ?? 10;
                            sysNodePageChange(1);
                          },
                        )
                      ])),
                )
              ],
            )));
  }
}

class AdminSysNodeDetailWidget extends StatefulWidget {
  final String? sysNodeId;
  final String sysNodeAction;
  const AdminSysNodeDetailWidget({required this.sysNodeAction, this.sysNodeId, Key? key}) : super(key: key);

  @override
  State<AdminSysNodeDetailWidget> createState() => _AdminSysNodeDetailState();
}

class _AdminSysNodeDetailState extends ReloadableState<AdminSysNodeDetailWidget> {
  dynamic formConfig = {
    "type": "column",
    "children": [
      {
        "type": "row",
        "children": [
          {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
        ]
      }
    ]
  };
  dynamic optionListMap = {};
  Map<dynamic, dynamic>? sysNode;
  TreeData? sysPermTreeData;
  String? error;
  var hasEditPermission = false;

  @override
  void initState() {
    super.initState();
    var userPermList = Provider.of<SecurityModel>(context, listen: false).userPermList;
    if (userPermList != null) {
      hasEditPermission = userPermList.where((permCode) => permCode == "ADMIN").isNotEmpty;
    }
    reload();
  }

  @override
  refreshData() {}

  reload() async {
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    if (widget.sysNodeId != null) {
      var sysNodeGetResponse = await httpGet("/test-framework-api/user/sys/sys-node/${widget.sysNodeId}", securityModel.authorization);
      if (sysNodeGetResponse.containsKey("body") && sysNodeGetResponse["body"] is String == false) {
        sysNode = sysNodeGetResponse["body"]["result"];
        if (widget.sysNodeAction == "token") {
          var sysNodeTokenGetResponse = await httpGet("/test-framework-api/user/sys/sys-node/${widget.sysNodeId}/token", securityModel.authorization);
          if (sysNodeTokenGetResponse.containsKey("body") && sysNodeTokenGetResponse["body"] is String == false) {
            sysNode!["token"] = sysNodeTokenGetResponse["body"]["result"];
          }
        }
      }
    } else {
      sysNode = {"status": 0};
    }
    await resetForm();
  }

  resetForm() async {
    var submitActionLabel = "Submit";
    var actionSubmitType = "primary";
    if (widget.sysNodeAction == "delete") {
      submitActionLabel = "Delete";
      actionSubmitType = "danger";
    }
    optionListMap = {
      "prjProjectId": [],
      "nodeType": [
        {"value": "private_all", "label": "Private all in one node"},
        {"value": "private_config", "label": "Private config node"},
        {"value": "private_build_run", "label": "Private run node"},
        {"value": "all", "label": "Public all in one node"},
        {"value": "config", "label": "Public config node"},
        {"value": "build", "label": "Public build node"},
        {"value": "run", "label": "Public run node"},
        {"value": "build_run", "label": "Public build and run node"},
      ],
      "status": [
        {"value": 1, "label": "Running"},
        {"value": 0, "label": "Stoped"},
        {"value": -1, "label": "Deleted"},
      ],
    };
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var prjProjectFindResponse = await httpPost("/test-framework-api/user/project/prj-project/search", {"status": 1}, securityModel.authorization);
    if (prjProjectFindResponse.containsKey("body") && prjProjectFindResponse["body"] is String == false) {
      optionListMap["prjProjectId"] = [
        {"value": null, "label": "---Any project---"},
        ...prjProjectFindResponse["body"]["resultList"].map((tableItem) => {"value": tableItem["id"], "label": tableItem["name"]}).toList()
      ];
    }
    formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "prjProjectId", "label": "Project", "hintText": "Project input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "disabled": true, "fieldName": "id", "label": "Id", "hintText": "Id input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "nodeType", "label": "Node type", "hintText": "Node type input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "name", "label": "Name", "hintText": "Name input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {
              "type": "inputText",
              "maxLines": 5,
              "expanded": true,
              "fieldName": "description",
              "label": "description",
              "hintText": "description input"
            }
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "status", "label": "status", "hintText": "status input"}
          ]
        },
        sysNode!["token"] != null
            ? {
                "type": "row",
                "children": [
                  {"type": "inputText", "maxLines": 5, "expanded": true, "fieldName": "token", "label": "token", "hintText": "token input"}
                ]
              }
            : {},
        {
          "type": "row",
          "children": [
            hasEditPermission && widget.sysNodeAction != "token"
                ? {"type": "action", "actionType": actionSubmitType, "actionName": "submit", "actionLabel": submitActionLabel}
                : {},
            hasEditPermission && widget.sysNodeAction == "token"
                ? {"type": "action", "actionType": "primary", "actionName": "copy", "actionLabel": "Copy to clipboard"}
                : {},
            {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
          ]
        }
      ]
    };
    setState(() {});
  }

  actionHandler(actionName, formData) async {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    if (actionName == "submit") {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      if (widget.sysNodeAction == "create") {
        var sysNodeCreateResponse = await httpPut("/test-framework-api/user/sys/sys-node", formData, securityModel.authorization);
        if (sysNodeCreateResponse.containsKey("body") && sysNodeCreateResponse["body"] is String == false) {
          var sysNodeCreateResponseBody = sysNodeCreateResponse["body"];
          if (sysNodeCreateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (sysNodeCreateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(sysNodeCreateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.sysNodeAction == "edit") {
        var sysNodeUpdateResponse =
            await httpPost("/test-framework-api/user/sys/sys-node/${widget.sysNodeId}", formData, securityModel.authorization);
        if (sysNodeUpdateResponse.containsKey("body") && sysNodeUpdateResponse["body"] is String == false) {
          var sysNodeUpdateResponseBody = sysNodeUpdateResponse["body"];
          if (sysNodeUpdateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (sysNodeUpdateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(sysNodeUpdateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.sysNodeAction == "delete") {
        var sysNodeDeleteResponse = await httpDelete("/test-framework-api/user/sys/sys-node/${widget.sysNodeId}", securityModel.authorization);
        if (sysNodeDeleteResponse.containsKey("body") && sysNodeDeleteResponse["body"] is String == false) {
          var sysNodeDeleteResponseBody = sysNodeDeleteResponse["body"];
          if (sysNodeDeleteResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (sysNodeDeleteResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(sysNodeDeleteResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
    }
    if (actionName == "copy") {
      Clipboard.setData(ClipboardData(text: sysNode!["token"])).then((_) {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text("Token copied to clipboard")));
      });
    }
    if (actionName == "back") {
      navigationModel.pop("/admin/system/sys-node");
    }
  }

  @override
  Widget build(BuildContext context) {
    if (error != null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Error'),
          ),
          body: Center(child: Text("Error $error")));
    }
    if (sysNode == null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Loading'),
          ),
          body: const Center(child: CircularProgressIndicator()));
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text('Node detail'),
      ),
      drawer: const Drawer(child: MenuWidget()),
      body: Padding(
          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(mainAxisSize: MainAxisSize.min, children: [
                      Card(
                          child: Column(mainAxisSize: MainAxisSize.min, children: [
                        const Row(
                          children: [
                            Text(
                              "Detail",
                              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        formConfig != null && optionListMap != null
                            ? DynamicForm(formConfig: formConfig, formData: sysNode!, optionListMap: optionListMap, actionHandler: actionHandler)
                            : Container()
                      ]))
                    ])),
              )
            ],
          )),
    );
  }
}
