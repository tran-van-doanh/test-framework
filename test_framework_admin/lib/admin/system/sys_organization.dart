import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_admin/admin/system/menu.dart';
import 'package:test_framework_admin/api.dart';
import 'package:test_framework_admin/common/dynamic_form.dart';
import 'package:test_framework_admin/common/dynamic_table.dart';
import 'package:test_framework_admin/common/reloadable_state.dart';
import 'package:test_framework_admin/common/tree.dart';
import 'package:test_framework_admin/model/model.dart';

class AdminSysOrganizationListWidget extends StatefulWidget {
  const AdminSysOrganizationListWidget({Key? key}) : super(key: key);

  @override
  State<AdminSysOrganizationListWidget> createState() => _AdminSysOrganizationListState();
}

class _AdminSysOrganizationListState extends ReloadableState<AdminSysOrganizationListWidget> {
  var sysOrganizationSearchRequest = {};
  var sysOrganizationRowCount = 0;
  var sysOrganizationCurrentPage = 1;
  var sysOrganizationResultList = [];
  var sysOrganizationRowPerPage = 25;
  Widget paging = Container();

  @override
  void initState() {
    super.initState();
    sysOrganizationSearch({});
  }

  @override
  refreshData() {
    sysOrganizationPageChange(sysOrganizationCurrentPage);
  }

  sysOrganizationSearch(sysOrganizationSearchRequest) async {
    this.sysOrganizationSearchRequest = sysOrganizationSearchRequest;
    await sysOrganizationPageChange(1);
  }

  sysOrganizationActionHandler(actionName, formData) async {
    if (actionName == "search") {
      sysOrganizationSearchRequest = formData;
      await sysOrganizationPageChange(1);
    }
  }

  sysOrganizationPageChange(page) async {
    if ((page - 1) * sysOrganizationRowPerPage > sysOrganizationRowCount) {
      page = (1.0 * sysOrganizationRowCount / sysOrganizationRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * sysOrganizationRowPerPage, "queryLimit": sysOrganizationRowPerPage};
    if (sysOrganizationSearchRequest["nameLike"] != null && sysOrganizationSearchRequest["nameLike"].isNotEmpty) {
      findRequest["nameLike"] = "%${sysOrganizationSearchRequest["nameLike"]}%";
    }
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var response = await httpPost("/test-framework-api/admin/sys/sys-organization/search", findRequest, securityModel.authorization);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        sysOrganizationCurrentPage = page;
        sysOrganizationRowCount = response["body"]["rowCount"];
        sysOrganizationResultList = response["body"]["resultList"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var sysOrganizationStatusMap = {1: "Active", 0: "Inactive"};
    var optionListMap = {};
    const formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "nameLike", "label": "Name", "hintText": "Name input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "action", "actionName": "search", "actionLabel": "Search"}
          ]
        }
      ]
    };
    var resultColumns = [
      const DataColumn(
        label: Text(
          'Name',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Status',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      DataColumn(
          label: Row(
        children: [
          ElevatedButton(
            onPressed: () {
              Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-organization/create");
            },
            child: const Text('Add'),
          )
        ],
      )),
    ];
    var resultRows = [
      for (var sysOrganization in sysOrganizationResultList)
        DataRow(
          cells: [
            DataCell(Text(sysOrganization["name"] ?? "")),
            DataCell(Text(sysOrganizationStatusMap[sysOrganization["status"]] ?? "Not defined")),
            DataCell(Row(
              children: [
                ElevatedButton(
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-organization/edit/${sysOrganization["id"]}");
                  },
                  child: const Text('Edit'),
                ),
                ElevatedButton(
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.red)),
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-organization/delete/${sysOrganization["id"]}");
                  },
                  child: const Text('Delete'),
                ),
              ],
            )),
          ],
        )
    ];

    return Scaffold(
        appBar: AppBar(
          title: const Text("Node list"),
          actions: [
            IconButton(
                onPressed: () {
                  Provider.of<SecurityModel>(context, listen: false).logout();
                },
                icon: const Icon(Icons.logout))
          ],
        ),
        drawer: const Drawer(child: MenuWidget()),
        body: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(mainAxisSize: MainAxisSize.min, children: [
                        DynamicForm(
                            formConfig: formConfig,
                            optionListMap: optionListMap,
                            formData: sysOrganizationSearchRequest,
                            actionHandler: sysOrganizationActionHandler),
                        Row(mainAxisSize: MainAxisSize.max, children: [
                          Expanded(
                              child: DynamicTable(
                            columns: resultColumns,
                            rows: resultRows,
                          ))
                        ]),
                        DynamicTablePagging(
                          sysOrganizationRowCount,
                          sysOrganizationCurrentPage,
                          sysOrganizationRowPerPage,
                          pageChangeHandler: (page) {
                            sysOrganizationPageChange(page);
                          },
                          rowPerPageChangeHandler: (rowPerPage) {
                            sysOrganizationRowPerPage = rowPerPage ?? 10;
                            sysOrganizationPageChange(1);
                          },
                        )
                      ])),
                )
              ],
            )));
  }
}

class AdminSysOrganizationDetailWidget extends StatefulWidget {
  final String? sysOrganizationId;
  final String sysOrganizationAction;
  const AdminSysOrganizationDetailWidget({required this.sysOrganizationAction, this.sysOrganizationId, Key? key}) : super(key: key);

  @override
  State<AdminSysOrganizationDetailWidget> createState() => _AdminSysOrganizationDetailState();
}

class _AdminSysOrganizationDetailState extends ReloadableState<AdminSysOrganizationDetailWidget> {
  dynamic formConfig = {
    "type": "column",
    "children": [
      {
        "type": "row",
        "children": [
          {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
        ]
      }
    ]
  };
  dynamic optionListMap = {};
  Map<dynamic, dynamic>? sysOrganization;
  TreeData? sysPermTreeData;
  String? error;
  var hasEditPermission = false;

  @override
  void initState() {
    super.initState();
    var userPermList = Provider.of<SecurityModel>(context, listen: false).userPermList;
    if (userPermList != null) {
      hasEditPermission = userPermList.where((permCode) => permCode == "ADMIN").isNotEmpty;
    }
    reload();
  }

  @override
  refreshData() {}

  reload() async {
    if (widget.sysOrganizationId != null) {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      var sysOrganizationGetResponse =
          await httpGet("/test-framework-api/admin/sys/sys-organization/${widget.sysOrganizationId}", securityModel.authorization);
      if (sysOrganizationGetResponse.containsKey("body") && sysOrganizationGetResponse["body"] is String == false) {
        sysOrganization = sysOrganizationGetResponse["body"]["result"];
      }
    } else {
      sysOrganization = {"status": 0};
    }
    await resetForm();
  }

  resetForm() async {
    var submitActionLabel = "Submit";
    var actionSubmitType = "primary";
    if (widget.sysOrganizationAction == "delete") {
      submitActionLabel = "Delete";
      actionSubmitType = "danger";
    }
    optionListMap = {
      "status": [
        {"value": 1, "label": "Active"},
        {"value": 0, "label": "Inactive"}
      ],
      "autoCreateProfile": [
        {"value": true, "label": "Auto create profile"},
        {"value": false, "label": "Manual create profile"}
      ],
      "defaultOrganization": [
        {"value": true, "label": "Default organization"},
        {"value": false, "label": "Not default organization"}
      ]
    };
    formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "name", "label": "Name", "hintText": "Name input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {
              "type": "inputText",
              "maxLines": 5,
              "expanded": true,
              "fieldName": "description",
              "label": "description",
              "hintText": "description input"
            }
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "maxLines": 5, "expanded": true, "fieldName": "license", "label": "license", "hintText": "license input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "autoCreateProfile", "label": "create profile", "hintText": "create profile input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {
              "type": "select",
              "expanded": true,
              "fieldName": "defaultOrganization",
              "label": "default organization",
              "hintText": "default organization input"
            }
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "status", "label": "status", "hintText": "status input"}
          ]
        },
        {
          "type": "row",
          "children": [
            hasEditPermission ? {"type": "action", "actionType": actionSubmitType, "actionName": "submit", "actionLabel": submitActionLabel} : {},
            {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
          ]
        }
      ]
    };
    setState(() {});
  }

  actionHandler(actionName, formData) async {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    if (actionName == "submit") {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      if (widget.sysOrganizationAction == "create") {
        var sysOrganizationCreateResponse = await httpPut("/test-framework-api/admin/sys/sys-organization", formData, securityModel.authorization);
        if (sysOrganizationCreateResponse.containsKey("body") && sysOrganizationCreateResponse["body"] is String == false) {
          var sysOrganizationCreateResponseBody = sysOrganizationCreateResponse["body"];
          if (sysOrganizationCreateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (sysOrganizationCreateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(sysOrganizationCreateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.sysOrganizationAction == "edit") {
        var sysOrganizationUpdateResponse =
            await httpPost("/test-framework-api/admin/sys/sys-organization/${widget.sysOrganizationId}", formData, securityModel.authorization);
        if (sysOrganizationUpdateResponse.containsKey("body") && sysOrganizationUpdateResponse["body"] is String == false) {
          var sysOrganizationUpdateResponseBody = sysOrganizationUpdateResponse["body"];
          if (sysOrganizationUpdateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (sysOrganizationUpdateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(sysOrganizationUpdateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.sysOrganizationAction == "delete") {
        var sysOrganizationDeleteResponse =
            await httpDelete("/test-framework-api/admin/sys/sys-organization/${widget.sysOrganizationId}", securityModel.authorization);
        if (sysOrganizationDeleteResponse.containsKey("body") && sysOrganizationDeleteResponse["body"] is String == false) {
          var sysOrganizationDeleteResponseBody = sysOrganizationDeleteResponse["body"];
          if (sysOrganizationDeleteResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (sysOrganizationDeleteResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(sysOrganizationDeleteResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
    }
    if (actionName == "back") {
      navigationModel.pop("/admin/system/sys-organization");
    }
  }

  @override
  Widget build(BuildContext context) {
    if (error != null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Error'),
          ),
          body: Center(child: Text("Error $error")));
    }
    if (sysOrganization == null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Loading'),
          ),
          body: const Center(child: CircularProgressIndicator()));
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text('Node detail'),
      ),
      drawer: const Drawer(child: MenuWidget()),
      body: Padding(
          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(mainAxisSize: MainAxisSize.min, children: [
                      Card(
                          child: Column(mainAxisSize: MainAxisSize.min, children: [
                        const Row(
                          children: [
                            Text(
                              "Detail",
                              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        formConfig != null && optionListMap != null
                            ? DynamicForm(
                                formConfig: formConfig, formData: sysOrganization!, optionListMap: optionListMap, actionHandler: actionHandler)
                            : Container()
                      ]))
                    ])),
              )
            ],
          )),
    );
  }
}
