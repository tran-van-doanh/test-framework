import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_admin/admin/system/menu.dart';
import 'package:test_framework_admin/api.dart';
import 'package:test_framework_admin/common/dynamic_form.dart';
import 'package:test_framework_admin/common/dynamic_table.dart';
import 'package:test_framework_admin/common/reloadable_state.dart';
import 'package:test_framework_admin/common/tree.dart';
import 'package:test_framework_admin/model/model.dart';

class AdminSysRoleListWidget extends StatefulWidget {
  const AdminSysRoleListWidget({Key? key}) : super(key: key);

  @override
  State<AdminSysRoleListWidget> createState() => _AdminSysRoleListState();
}

class _AdminSysRoleListState extends ReloadableState<AdminSysRoleListWidget> {
  var sysRoleSearchRequest = {};
  var sysRoleRowCount = 0;
  var sysRoleCurrentPage = 1;
  var sysRoleResultList = [];
  var sysRoleRowPerPage = 25;
  Widget paging = Container();

  @override
  void initState() {
    super.initState();
    sysRoleSearch({});
  }

  @override
  refreshData() {
    sysRolePageChange(sysRoleCurrentPage);
  }

  sysRoleSearch(sysRoleSearchRequest) async {
    this.sysRoleSearchRequest = sysRoleSearchRequest;
    await sysRolePageChange(1);
  }

  sysRoleActionHandler(actionName, formData) async {
    if (actionName == "search") {
      sysRoleSearchRequest = formData;
      await sysRolePageChange(1);
    }
  }

  sysRolePageChange(page) async {
    if ((page - 1) * sysRoleRowPerPage > sysRoleRowCount) {
      page = (1.0 * sysRoleRowCount / sysRoleRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * sysRoleRowPerPage, "queryLimit": sysRoleRowPerPage};
    if (sysRoleSearchRequest["codeLike"] != null && sysRoleSearchRequest["codeLike"].isNotEmpty) {
      findRequest["codeLike"] = "%${sysRoleSearchRequest["codeLike"]}%";
    }
    if (sysRoleSearchRequest["nameLike"] != null && sysRoleSearchRequest["nameLike"].isNotEmpty) {
      findRequest["nameLike"] = "%${sysRoleSearchRequest["nameLike"]}%";
    }
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var response = await httpPost("/test-framework-api/admin/sys/sys-role/search", findRequest, securityModel.authorization);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        sysRoleCurrentPage = page;
        sysRoleRowCount = response["body"]["rowCount"];
        sysRoleResultList = response["body"]["resultList"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var sysRoleStatusMap = {1: "Active", 0: "Inactive"};
    var optionListMap = {};
    const formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "code", "label": "Code", "hintText": "Code input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "name", "label": "Name", "hintText": "Name input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "action", "actionName": "search", "actionLabel": "Search"}
          ]
        }
      ]
    };
    var resultColumns = [
      const DataColumn(
        label: Text(
          'Code',
          overflow: TextOverflow.visible,
          softWrap: true,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Name',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Status',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      DataColumn(
          label: Row(
        children: [
          ElevatedButton(
            onPressed: () {
              Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-role/create");
            },
            child: const Text('Add'),
          )
        ],
      )),
    ];
    var resultRows = [
      for (var sysRole in sysRoleResultList)
        DataRow(
          cells: [
            DataCell(Text(sysRole["code"])),
            DataCell(Text(sysRole["name"])),
            DataCell(Text(sysRoleStatusMap[sysRole["status"]] ?? "Not defined")),
            DataCell(Row(
              children: [
                ElevatedButton(
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-role/edit/${sysRole["id"]}");
                  },
                  child: const Text('Edit'),
                ),
                ElevatedButton(
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.red)),
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-role/delete/${sysRole["id"]}");
                  },
                  child: const Text('Delete'),
                ),
              ],
            )),
          ],
        )
    ];

    return Scaffold(
        appBar: AppBar(
          title: const Text("Role list"),
          actions: [
            IconButton(
                onPressed: () {
                  Provider.of<SecurityModel>(context, listen: false).logout();
                },
                icon: const Icon(Icons.logout))
          ],
        ),
        drawer: const Drawer(child: MenuWidget()),
        body: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(mainAxisSize: MainAxisSize.min, children: [
                        DynamicForm(
                            formConfig: formConfig,
                            optionListMap: optionListMap,
                            formData: sysRoleSearchRequest,
                            actionHandler: sysRoleActionHandler),
                        Row(mainAxisSize: MainAxisSize.max, children: [
                          Expanded(
                              child: DynamicTable(
                            columns: resultColumns,
                            rows: resultRows,
                          ))
                        ]),
                        DynamicTablePagging(
                          sysRoleRowCount,
                          sysRoleCurrentPage,
                          sysRoleRowPerPage,
                          pageChangeHandler: (page) {
                            sysRolePageChange(page);
                          },
                          rowPerPageChangeHandler: (rowPerPage) {
                            sysRoleRowPerPage = rowPerPage ?? 10;
                            sysRolePageChange(1);
                          },
                        )
                      ])),
                )
              ],
            )));
  }
}

class AdminSysRoleDetailWidget extends StatefulWidget {
  final String? sysRoleId;
  final String sysRoleAction;
  const AdminSysRoleDetailWidget({required this.sysRoleAction, this.sysRoleId, Key? key}) : super(key: key);

  @override
  State<AdminSysRoleDetailWidget> createState() => _AdminSysRoleDetailState();
}

class _AdminSysRoleDetailState extends ReloadableState<AdminSysRoleDetailWidget> {
  dynamic formConfig = {
    "type": "column",
    "children": [
      {
        "type": "row",
        "children": [
          {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
        ]
      }
    ]
  };
  dynamic optionListMap = {};
  Map<dynamic, dynamic>? sysRole;
  TreeData? sysPermTreeData;
  String? error;
  var hasEditPermission = false;

  @override
  void initState() {
    super.initState();
    var userPermList = Provider.of<SecurityModel>(context, listen: false).userPermList;
    if (userPermList != null) {
      hasEditPermission = userPermList.where((permCode) => permCode == "ADMIN").isNotEmpty;
    }
    reload();
  }

  @override
  refreshData() {}

  reload() async {
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var sysPermFindRequest = {"permType": "user", "status": 1};
    var sysPermFindResponse = await httpPost("/test-framework-api/admin/sys/sys-perm/search", sysPermFindRequest, securityModel.authorization);
    if (sysPermFindResponse.containsKey("body") && sysPermFindResponse["body"] is String == false) {
      var sysPermList = sysPermFindResponse["body"]["resultList"];
      List<TreeNode> treeNodeList = [];
      for (var sysPerm in sysPermList) {
        treeNodeList.add(TreeNode(key: sysPerm["id"], parentKey: sysPerm["parentId"], item: sysPerm));
      }
      sysPermTreeData = TreeData(treeNodeList);
    }
    if (widget.sysRoleId != null) {
      var sysRoleGetResponse = await httpGet("/test-framework-api/admin/sys/sys-role/${widget.sysRoleId}", securityModel.authorization);
      if (sysRoleGetResponse.containsKey("body") && sysRoleGetResponse["body"] is String == false) {
        sysRole = sysRoleGetResponse["body"]["result"];
      }
    } else {
      sysRole = {"status": 0};
    }
    await resetForm();
  }

  resetForm() async {
    var submitActionLabel = "Submit";
    var actionSubmitType = "primary";
    if (widget.sysRoleAction == "delete") {
      submitActionLabel = "Delete";
      actionSubmitType = "danger";
    }
    optionListMap = {
      "status": [
        {"value": 1, "label": "Active"},
        {"value": 0, "label": "Inactive"}
      ]
    };
    formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "code", "label": "Code", "hintText": "Code input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "name", "label": "Name", "hintText": "Name input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "description", "label": "description", "hintText": "description input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "status", "label": "status", "hintText": "status input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {
              "type": "treeMultipleSelect",
              "expanded": true,
              "fieldName": "sysPermIdList",
              "label": "Perm",
              "hintText": "Perm input",
              "getTreeNodeLabel": (treeNode) => "${treeNode.item["code"]} - ${treeNode.item["name"]}",
              "treeData": sysPermTreeData
            }
          ]
        },
        {
          "type": "row",
          "children": hasEditPermission
              ? [
                  {"type": "action", "actionType": actionSubmitType, "actionName": "submit", "actionLabel": submitActionLabel},
                  {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
                ]
              : [
                  {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
                ]
        }
      ]
    };
    setState(() {});
  }

  actionHandler(actionName, formData) async {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    if (actionName == "submit") {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      if (widget.sysRoleAction == "create") {
        var sysRoleCreateResponse = await httpPut("/test-framework-api/admin/sys/sys-role", formData, securityModel.authorization);
        if (sysRoleCreateResponse.containsKey("body") && sysRoleCreateResponse["body"] is String == false) {
          var sysRoleCreateResponseBody = sysRoleCreateResponse["body"];
          if (sysRoleCreateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (sysRoleCreateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(sysRoleCreateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.sysRoleAction == "edit") {
        var sysRoleUpdateResponse =
            await httpPost("/test-framework-api/admin/sys/sys-role/${widget.sysRoleId}", formData, securityModel.authorization);
        if (sysRoleUpdateResponse.containsKey("body") && sysRoleUpdateResponse["body"] is String == false) {
          var sysRoleUpdateResponseBody = sysRoleUpdateResponse["body"];
          if (sysRoleUpdateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (sysRoleUpdateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(sysRoleUpdateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.sysRoleAction == "delete") {
        var sysRoleDeleteResponse = await httpDelete("/test-framework-api/admin/sys/sys-role/${widget.sysRoleId}", securityModel.authorization);
        if (sysRoleDeleteResponse.containsKey("body") && sysRoleDeleteResponse["body"] is String == false) {
          var sysRoleDeleteResponseBody = sysRoleDeleteResponse["body"];
          if (sysRoleDeleteResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (sysRoleDeleteResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(sysRoleDeleteResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
    }
    if (actionName == "back") {
      navigationModel.pop("/admin/system/sys-role");
    }
  }

  @override
  Widget build(BuildContext context) {
    if (error != null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Error'),
          ),
          body: Center(child: Text("Error $error")));
    }
    if (sysRole == null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Loading'),
          ),
          body: const Center(child: CircularProgressIndicator()));
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text('Role detail'),
      ),
      drawer: const Drawer(child: MenuWidget()),
      body: Padding(
          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(mainAxisSize: MainAxisSize.min, children: [
                      Card(
                          child: Column(mainAxisSize: MainAxisSize.min, children: [
                        const Row(
                          children: [
                            Text(
                              "Detail",
                              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        formConfig != null && optionListMap != null
                            ? DynamicForm(formConfig: formConfig, formData: sysRole!, optionListMap: optionListMap, actionHandler: actionHandler)
                            : Container()
                      ]))
                    ])),
              )
            ],
          )),
    );
  }
}
