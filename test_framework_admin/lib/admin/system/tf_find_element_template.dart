import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_admin/admin/system/menu.dart';
import 'package:test_framework_admin/api.dart';
import 'package:test_framework_admin/common/dynamic_form.dart';
import 'package:test_framework_admin/common/dynamic_table.dart';
import 'package:test_framework_admin/common/reloadable_state.dart';
import 'package:test_framework_admin/common/tree.dart';
import 'package:test_framework_admin/model/model.dart';

class AdminTfFindElementTemplateListWidget extends StatefulWidget {
  const AdminTfFindElementTemplateListWidget({Key? key}) : super(key: key);

  @override
  State<AdminTfFindElementTemplateListWidget> createState() => _AdminTfFindElementTemplateListState();
}

class _AdminTfFindElementTemplateListState extends ReloadableState<AdminTfFindElementTemplateListWidget> {
  dynamic formConfig = {"type": "column", "children": []};
  dynamic optionListMap = {};
  var tfFindElementTemplateSearchRequest = {};
  var tfFindElementTemplateRowCount = 0;
  var tfFindElementTemplateCurrentPage = 1;
  var tfFindElementTemplateResultList = [];
  var tfFindElementTemplateRowPerPage = 25;
  Widget paging = Container();

  @override
  void initState() {
    super.initState();
    reset();
  }

  @override
  refreshData() {
    tfFindElementTemplatePageChange(tfFindElementTemplateCurrentPage);
  }

  reset() async {
    await resetForm();
    await tfFindElementTemplateSearch({});
  }

  resetForm() async {
    optionListMap = {
      "status": [
        {"value": 1, "label": "Active"},
        {"value": 0, "label": "Inactive"}
      ]
    };
    formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "nameLike", "label": "Name", "hintText": "Name input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "action", "actionName": "search", "actionLabel": "Search"}
          ]
        }
      ]
    };
  }

  tfFindElementTemplateSearch(tfFindElementTemplateSearchRequest) async {
    this.tfFindElementTemplateSearchRequest = tfFindElementTemplateSearchRequest;
    await tfFindElementTemplatePageChange(1);
  }

  tfFindElementTemplateActionHandler(actionName, formData) async {
    if (actionName == "search") {
      tfFindElementTemplateSearchRequest = formData;
      await tfFindElementTemplatePageChange(1);
    }
  }

  tfFindElementTemplatePageChange(page) async {
    if ((page - 1) * tfFindElementTemplateRowPerPage > tfFindElementTemplateRowCount) {
      page = (1.0 * tfFindElementTemplateRowCount / tfFindElementTemplateRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * tfFindElementTemplateRowPerPage, "queryLimit": tfFindElementTemplateRowPerPage};
    if (tfFindElementTemplateSearchRequest["nameLike"] != null && tfFindElementTemplateSearchRequest["nameLike"].isNotEmpty) {
      findRequest["nameLike"] = "%${tfFindElementTemplateSearchRequest["nameLike"]}%";
    }
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var response =
        await httpPost("/test-framework-api/user/test-framework/tf-find-element-template/search", findRequest, securityModel.authorization);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        tfFindElementTemplateCurrentPage = page;
        tfFindElementTemplateRowCount = response["body"]["rowCount"];
        tfFindElementTemplateResultList = response["body"]["resultList"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var tfFindElementTemplateStatusMap = {1: "Active", 0: "Inactive"};
    var resultColumns = [
      const DataColumn(
        label: Text(
          'Project type',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Name',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Title',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Status',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      DataColumn(
          label: Row(
        children: [
          ElevatedButton(
            onPressed: () {
              Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/tf-find-element-template/create");
            },
            child: const Text('Add'),
          )
        ],
      )),
    ];
    var resultRows = [
      for (var tfFindElementTemplate in tfFindElementTemplateResultList)
        DataRow(
          cells: [
            DataCell(Text(tfFindElementTemplate["prjProjectType"]["title"] ?? "")),
            DataCell(Text(tfFindElementTemplate["name"] ?? "")),
            DataCell(Text(tfFindElementTemplate["title"] ?? "")),
            DataCell(Text(tfFindElementTemplateStatusMap[tfFindElementTemplate["status"]] ?? "Not defined")),
            DataCell(Row(
              children: [
                ElevatedButton(
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false)
                        .navigate("/admin/system/tf-find-element-template/edit/${tfFindElementTemplate["id"]}");
                  },
                  child: const Text('Edit'),
                ),
                ElevatedButton(
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.red)),
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false)
                        .navigate("/admin/system/tf-find-element-template/delete/${tfFindElementTemplate["id"]}");
                  },
                  child: const Text('Delete'),
                ),
              ],
            )),
          ],
        )
    ];

    return Scaffold(
        appBar: AppBar(
          title: const Text("Find element template list"),
          actions: [
            IconButton(
                onPressed: () {
                  Provider.of<SecurityModel>(context, listen: false).logout();
                },
                icon: const Icon(Icons.logout))
          ],
        ),
        drawer: const Drawer(child: MenuWidget()),
        body: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(mainAxisSize: MainAxisSize.min, children: [
                        DynamicForm(
                            formConfig: formConfig,
                            optionListMap: optionListMap,
                            formData: tfFindElementTemplateSearchRequest,
                            actionHandler: tfFindElementTemplateActionHandler),
                        Row(mainAxisSize: MainAxisSize.max, children: [
                          Expanded(
                              child: DynamicTable(
                            columns: resultColumns,
                            rows: resultRows,
                          ))
                        ]),
                        DynamicTablePagging(
                          tfFindElementTemplateRowCount,
                          tfFindElementTemplateCurrentPage,
                          tfFindElementTemplateRowPerPage,
                          pageChangeHandler: (page) {
                            tfFindElementTemplatePageChange(page);
                          },
                          rowPerPageChangeHandler: (rowPerPage) {
                            tfFindElementTemplateRowPerPage = rowPerPage ?? 10;
                            tfFindElementTemplatePageChange(1);
                          },
                        )
                      ])),
                )
              ],
            )));
  }
}

class AdminTfFindElementTemplateDetailWidget extends StatefulWidget {
  final String? tfFindElementTemplateId;
  final String tfFindElementTemplateAction;
  const AdminTfFindElementTemplateDetailWidget({required this.tfFindElementTemplateAction, this.tfFindElementTemplateId, Key? key}) : super(key: key);

  @override
  State<AdminTfFindElementTemplateDetailWidget> createState() => _AdminTfFindElementTemplateDetailState();
}

class _AdminTfFindElementTemplateDetailState extends ReloadableState<AdminTfFindElementTemplateDetailWidget> {
  dynamic formConfig = {
    "type": "column",
    "children": [
      {
        "type": "row",
        "children": [
          {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
        ]
      }
    ]
  };
  dynamic optionListMap = {};
  Map<dynamic, dynamic>? tfFindElementTemplate;
  TreeData? sysPermTreeData;
  String? error;
  var hasEditPermission = false;

  @override
  void initState() {
    super.initState();
    var userPermList = Provider.of<SecurityModel>(context, listen: false).userPermList;
    if (userPermList != null) {
      hasEditPermission = userPermList.where((permCode) => permCode == "ADMIN").isNotEmpty;
    }
    reload();
  }

  @override
  refreshData() {}

  reload() async {
    if (widget.tfFindElementTemplateId != null) {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      var tfFindElementTemplateGetResponse = await httpGet(
          "/test-framework-api/user/test-framework/tf-find-element-template/${widget.tfFindElementTemplateId}", securityModel.authorization);
      if (tfFindElementTemplateGetResponse.containsKey("body") && tfFindElementTemplateGetResponse["body"] is String == false) {
        tfFindElementTemplate = tfFindElementTemplateGetResponse["body"]["result"];
        if (tfFindElementTemplate != null) {
          tfFindElementTemplate!["content"] = tfFindElementTemplate!["content"] ?? {};
        }
      }
    } else {
      tfFindElementTemplate = {
        "content": {"type": "findElements"},
        "status": 0
      };
    }
    await resetForm();
  }

  resetForm() async {
    var submitActionLabel = "Submit";
    var actionSubmitType = "primary";
    if (widget.tfFindElementTemplateAction == "delete") {
      submitActionLabel = "Delete";
      actionSubmitType = "danger";
    }
    optionListMap = {
      "status": [
        {"value": 1, "label": "Active"},
        {"value": 0, "label": "Inactive"}
      ],
      "prjProjectTypeId": []
    };
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var prjProjectTypeFindResponse =
        await httpPost("/test-framework-api/user/project/prj-project-type/search", {"status": 1}, securityModel.authorization);
    if (prjProjectTypeFindResponse.containsKey("body") && prjProjectTypeFindResponse["body"] is String == false) {
      optionListMap["prjProjectTypeId"] = prjProjectTypeFindResponse["body"]["resultList"]
          .map((prjProjectType) => {"value": prjProjectType["id"], "label": prjProjectType["title"]})
          .toList();
    }
    formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "prjProjectTypeId", "label": "Project type", "hintText": "Project type input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "name", "label": "name", "hintText": "Name input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "title", "label": "Title", "hintText": "Title input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {
              "type": "inputText",
              "maxLines": 5,
              "expanded": true,
              "fieldName": "description",
              "label": "description",
              "hintText": "description input"
            }
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "status", "label": "status", "hintText": "status input"}
          ]
        },
        {
          "type": "row",
          "children": [
            hasEditPermission ? {"type": "action", "actionType": actionSubmitType, "actionName": "submit", "actionLabel": submitActionLabel} : {},
            {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
          ]
        }
      ]
    };
    setState(() {});
  }

  actionHandler(actionName, formData) async {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    if (actionName == "submit") {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      if (widget.tfFindElementTemplateAction == "create") {
        var tfFindElementTemplateCreateResponse =
            await httpPut("/test-framework-api/user/test-framework/tf-find-element-template", formData, securityModel.authorization);
        if (tfFindElementTemplateCreateResponse.containsKey("body") && tfFindElementTemplateCreateResponse["body"] is String == false) {
          var tfFindElementTemplateCreateResponseBody = tfFindElementTemplateCreateResponse["body"];
          if (tfFindElementTemplateCreateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (tfFindElementTemplateCreateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(tfFindElementTemplateCreateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.tfFindElementTemplateAction == "edit") {
        var tfFindElementTemplateUpdateResponse = await httpPost(
            "/test-framework-api/user/test-framework/tf-find-element-template/${widget.tfFindElementTemplateId}",
            formData,
            securityModel.authorization);
        if (tfFindElementTemplateUpdateResponse.containsKey("body") && tfFindElementTemplateUpdateResponse["body"] is String == false) {
          var tfFindElementTemplateUpdateResponseBody = tfFindElementTemplateUpdateResponse["body"];
          if (tfFindElementTemplateUpdateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (tfFindElementTemplateUpdateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(tfFindElementTemplateUpdateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.tfFindElementTemplateAction == "delete") {
        var tfFindElementTemplateDeleteResponse = await httpDelete(
            "/test-framework-api/user/test-framework/tf-find-element-template/${widget.tfFindElementTemplateId}", securityModel.authorization);
        if (tfFindElementTemplateDeleteResponse.containsKey("body") && tfFindElementTemplateDeleteResponse["body"] is String == false) {
          var tfFindElementTemplateDeleteResponseBody = tfFindElementTemplateDeleteResponse["body"];
          if (tfFindElementTemplateDeleteResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (tfFindElementTemplateDeleteResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(tfFindElementTemplateDeleteResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
    }
    if (actionName == "back") {
      navigationModel.pop("/admin/system/tf-find-element-template");
    }
  }

  @override
  Widget build(BuildContext context) {
    if (error != null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Error'),
          ),
          body: Center(child: Text("Error $error")));
    }
    if (tfFindElementTemplate == null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Loading'),
          ),
          body: const Center(child: CircularProgressIndicator()));
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text('Find element template detail'),
      ),
      drawer: const Drawer(child: MenuWidget()),
      body: Padding(
          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(mainAxisSize: MainAxisSize.min, children: [
                      Card(
                          child: Column(mainAxisSize: MainAxisSize.min, children: [
                        const Row(
                          children: [
                            Text(
                              "Config",
                              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        TfFindElementTemplateContentWidget(
                          content: tfFindElementTemplate!["content"],
                        ),
                        const Row(
                          children: [
                            Text(
                              "Detail",
                              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        formConfig != null && optionListMap != null
                            ? DynamicForm(
                                formConfig: formConfig, formData: tfFindElementTemplate!, optionListMap: optionListMap, actionHandler: actionHandler)
                            : Container()
                      ]))
                    ])),
              )
            ],
          )),
    );
  }
}

class TfFindElementTemplateContentWidget extends StatefulWidget {
  final dynamic content;
  const TfFindElementTemplateContentWidget({required this.content, Key? key}) : super(key: key);

  @override
  State<TfFindElementTemplateContentWidget> createState() => _TfFindElementTemplateContentWidgetState();
}

class _TfFindElementTemplateContentWidgetState extends ReloadableState<TfFindElementTemplateContentWidget> {
  @override
  refreshData() {}

  showEditDialog(parentList, data, type) {
    if (type == "elementPath") {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext dialogContext) {
          return Dialog(
              child: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                    child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                            child: Padding(
                                padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                                child: TextField(
                                  controller: TextEditingController(text: data["from"] ?? ""),
                                  onChanged: (value) {
                                    data["from"] = value;
                                  },
                                  decoration: const InputDecoration(
                                    label: Text("from"),
                                    hintText: "From input",
                                    border: OutlineInputBorder(),
                                  ),
                                )))
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                            child: Padding(
                                padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                                child: TextField(
                                  controller: TextEditingController(text: data["name"] ?? ""),
                                  onChanged: (value) {
                                    data["name"] = value;
                                  },
                                  decoration: const InputDecoration(
                                    label: Text("Name"),
                                    hintText: "Name input",
                                    border: OutlineInputBorder(),
                                  ),
                                )))
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                            child: Padding(
                                padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                                child: TextField(
                                  controller: TextEditingController(text: data["xpath"] ?? ""),
                                  onChanged: (value) {
                                    data["xpath"] = value;
                                  },
                                  decoration: const InputDecoration(
                                    label: Text("Xpath"),
                                    hintText: "Xpath input",
                                    border: OutlineInputBorder(),
                                  ),
                                )))
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                          onPressed: () {
                            Navigator.pop(dialogContext, true);
                            setState(() {});
                          },
                          child: const Text("Close"),
                        )
                      ],
                    )
                  ],
                ))
              ],
            ),
          ));
        },
      );
    }
    if (type == "resultXpath") {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext dialogContext) {
          return Dialog(
              child: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                    child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                            child: Padding(
                                padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                                child: TextField(
                                  controller: TextEditingController(text: data["name"] ?? ""),
                                  onChanged: (value) {
                                    data["name"] = value;
                                  },
                                  decoration: const InputDecoration(
                                    label: Text("Name"),
                                    hintText: "Name input",
                                    border: OutlineInputBorder(),
                                  ),
                                )))
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                          onPressed: () {
                            Navigator.pop(dialogContext, true);
                            setState(() {});
                          },
                          child: const Text("Close"),
                        )
                      ],
                    )
                  ],
                ))
              ],
            ),
          ));
        },
      );
    }
    if (type == "xpath") {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext dialogContext) {
          return Dialog(
              child: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                    child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                            child: Padding(
                                padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                                child: TextField(
                                  controller: TextEditingController(text: data["path"] ?? ""),
                                  onChanged: (value) {
                                    data["path"] = value;
                                  },
                                  decoration: const InputDecoration(
                                    label: Text("Path"),
                                    hintText: "Path input",
                                    border: OutlineInputBorder(),
                                  ),
                                )))
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                          onPressed: () {
                            Navigator.pop(dialogContext, true);
                            setState(() {});
                          },
                          child: const Text("Close"),
                        )
                      ],
                    )
                  ],
                ))
              ],
            ),
          ));
        },
      );
    }
    if (type == "variable") {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext dialogContext) {
          return Dialog(
              child: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                    child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                            child: Padding(
                                padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                                child: TextField(
                                  controller: TextEditingController(text: data["name"] ?? ""),
                                  onChanged: (value) {
                                    data["name"] = value;
                                  },
                                  decoration: const InputDecoration(
                                    label: Text("Name"),
                                    hintText: "Name input",
                                    border: OutlineInputBorder(),
                                  ),
                                )))
                      ],
                    ),
                    MyDropdown(
                        label: "Type",
                        hintText: "Type input",
                        selectedValue: data["type"] ?? "value",
                        optionList: const [
                          {"value": "value", "label": "value"},
                          {"value": "contains", "label": "contains"}
                        ],
                        onChanged: (value) {
                          data["type"] = value;
                        }),
                    Row(
                      children: [
                        Expanded(
                            child: Padding(
                                padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                                child: TextField(
                                  controller: TextEditingController(text: data["elementName"] ?? ""),
                                  onChanged: (value) {
                                    data["elementName"] = value;
                                  },
                                  decoration: const InputDecoration(
                                    label: Text("Element"),
                                    hintText: "Element input",
                                    border: OutlineInputBorder(),
                                  ),
                                )))
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                            child: Padding(
                                padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                                child: TextField(
                                  controller: TextEditingController(text: data["attributeName"] ?? ""),
                                  onChanged: (value) {
                                    data["attributeName"] = value;
                                  },
                                  decoration: const InputDecoration(
                                    label: Text("Attribute"),
                                    hintText: "Attribute input",
                                    border: OutlineInputBorder(),
                                  ),
                                )))
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                          onPressed: () {
                            Navigator.pop(dialogContext, true);
                            setState(() {});
                          },
                          child: const Text("Close"),
                        )
                      ],
                    )
                  ],
                ))
              ],
            ),
          ));
        },
      );
    }
    if (type == "resultAttribute") {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext dialogContext) {
          return Dialog(
              child: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                    child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                            child: Padding(
                                padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                                child: TextField(
                                  controller: TextEditingController(text: data["name"] ?? ""),
                                  onChanged: (value) {
                                    data["name"] = value;
                                  },
                                  decoration: const InputDecoration(
                                    label: Text("Name"),
                                    hintText: "Name input",
                                    border: OutlineInputBorder(),
                                  ),
                                )))
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                            child: Padding(
                                padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                                child: TextField(
                                  controller: TextEditingController(text: data["elementName"] ?? ""),
                                  onChanged: (value) {
                                    data["elementName"] = value;
                                  },
                                  decoration: const InputDecoration(
                                    label: Text("Element"),
                                    hintText: "Element input",
                                    border: OutlineInputBorder(),
                                  ),
                                )))
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                            child: Padding(
                                padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                                child: TextField(
                                  controller: TextEditingController(text: data["attributeName"] ?? ""),
                                  onChanged: (value) {
                                    data["attributeName"] = value;
                                  },
                                  decoration: const InputDecoration(
                                    label: Text("Attribute"),
                                    hintText: "Attribute input",
                                    border: OutlineInputBorder(),
                                  ),
                                )))
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                          onPressed: () {
                            Navigator.pop(dialogContext, true);
                            setState(() {});
                          },
                          child: const Text("Close"),
                        )
                      ],
                    )
                  ],
                ))
              ],
            ),
          ));
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          RichText(
              text: TextSpan(children: [
            TextSpan(text: " " * 4),
            const TextSpan(text: "Element path list"),
            TextSpan(text: " " * 2),
            TextSpan(
                text: "Add",
                style: const TextStyle(color: Colors.green),
                recognizer: TapGestureRecognizer()
                  ..onTap = () {
                    if (widget.content["elementPathList"] == null) {
                      widget.content["elementPathList"] = [];
                    }
                    widget.content["elementPathList"].add({});
                    setState(() {});
                  })
          ])),
          for (var elementPath in widget.content["elementPathList"] ?? [])
            RichText(
                text: TextSpan(children: [
              TextSpan(text: " " * 8),
              TextSpan(text: "${elementPath["name"]} = "),
              TextSpan(
                  text: elementPath["from"] != null && elementPath["from"].isNotEmpty
                      ? "${elementPath["from"]} => ${elementPath["xpath"] ?? ""} "
                      : "${elementPath["xpath"] ?? ""}"),
              TextSpan(text: " " * 2),
              TextSpan(
                  text: "Edit",
                  style: const TextStyle(color: Colors.green),
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      showEditDialog(widget.content["elementPathList"], elementPath, "elementPath");
                    }),
              TextSpan(text: " " * 2),
              TextSpan(
                  text: "Delete",
                  style: const TextStyle(color: Colors.green),
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      widget.content["elementPathList"].remove(elementPath);
                      setState(() {});
                    })
            ])),
          RichText(
              text: TextSpan(children: [
            TextSpan(text: " " * 4),
            const TextSpan(text: "Result xpath list"),
            TextSpan(text: " " * 2),
            TextSpan(
                text: "Add",
                style: const TextStyle(color: Colors.green),
                recognizer: TapGestureRecognizer()
                  ..onTap = () {
                    if (widget.content["resultXpathList"] == null) {
                      widget.content["resultXpathList"] = [];
                    }
                    widget.content["resultXpathList"].add({});
                    setState(() {});
                  })
          ])),
          for (var resultXpath in widget.content["resultXpathList"] ?? [])
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RichText(
                    text: TextSpan(children: [
                  TextSpan(text: " " * 8),
                  TextSpan(text: "${resultXpath["name"]}"),
                  TextSpan(text: " " * 2),
                  TextSpan(
                      text: "Edit",
                      style: const TextStyle(color: Colors.green),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          showEditDialog(widget.content["resultXpathList"], resultXpath, "resultXpath");
                        }),
                  TextSpan(text: " " * 2),
                  TextSpan(
                      text: "Delete",
                      style: const TextStyle(color: Colors.green),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          widget.content["resultXpathList"].remove(resultXpath);
                          setState(() {});
                        })
                ])),
                RichText(
                    text: TextSpan(children: [
                  TextSpan(text: " " * 8),
                  const TextSpan(text: "Result xpath list"),
                  TextSpan(text: " " * 2),
                  TextSpan(
                      text: "Add",
                      style: const TextStyle(color: Colors.green),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          if (resultXpath["xpathList"] == null) {
                            resultXpath["xpathList"] = [];
                          }
                          resultXpath["xpathList"].add({});
                          setState(() {});
                        })
                ])),
                for (var xpath in resultXpath["xpathList"] ?? [])
                  Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                    RichText(
                        text: TextSpan(children: [
                      TextSpan(text: " " * 12),
                      TextSpan(text: "${xpath["path"]}"),
                      TextSpan(text: " " * 2),
                      TextSpan(
                          text: "Edit",
                          style: const TextStyle(color: Colors.green),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              showEditDialog(resultXpath["xpathList"], xpath, "xpath");
                            }),
                      TextSpan(text: " " * 2),
                      TextSpan(
                          text: "Delete",
                          style: const TextStyle(color: Colors.green),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              resultXpath["xpathList"].remove(xpath);
                              setState(() {});
                            })
                    ])),
                    RichText(
                        text: TextSpan(children: [
                      TextSpan(text: " " * 12),
                      const TextSpan(text: "Xpath variable list"),
                      TextSpan(text: " " * 2),
                      TextSpan(
                          text: "Add",
                          style: const TextStyle(color: Colors.green),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              if (xpath["variableList"] == null) {
                                xpath["variableList"] = [];
                              }
                              xpath["variableList"].add({});
                              setState(() {});
                            })
                    ])),
                    for (var variable in xpath["variableList"] ?? [])
                      RichText(
                          text: TextSpan(children: [
                        TextSpan(text: " " * 16),
                        TextSpan(text: "${variable["name"]} = ${variable["elementName"]}.${variable["attributeName"]}"),
                        TextSpan(text: " " * 2),
                        TextSpan(
                            text: "Edit",
                            style: const TextStyle(color: Colors.green),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                showEditDialog(xpath["variableList"], variable, "variable");
                              }),
                        TextSpan(text: " " * 2),
                        TextSpan(
                            text: "Delete",
                            style: const TextStyle(color: Colors.green),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                xpath["variableList"].remove(variable);
                                setState(() {});
                              })
                      ])),
                  ])
              ],
            ),
          RichText(
              text: TextSpan(children: [
            TextSpan(text: " " * 4),
            const TextSpan(text: "Result attribute list"),
            TextSpan(text: " " * 2),
            TextSpan(
                text: "Add",
                style: const TextStyle(color: Colors.green),
                recognizer: TapGestureRecognizer()
                  ..onTap = () {
                    if (widget.content["resultAttributeList"] == null) {
                      widget.content["resultAttributeList"] = [];
                    }
                    widget.content["resultAttributeList"].add({});
                    setState(() {});
                  })
          ])),
          for (var resultAttribute in widget.content["resultAttributeList"] ?? [])
            RichText(
                text: TextSpan(children: [
              TextSpan(text: " " * 8),
              TextSpan(text: "${resultAttribute["name"]} = ${resultAttribute["elementName"]}.${resultAttribute["attributeName"]}"),
              TextSpan(text: " " * 2),
              TextSpan(
                  text: "Edit",
                  style: const TextStyle(color: Colors.green),
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      showEditDialog(widget.content["resultAttributeList"], resultAttribute, "resultAttribute");
                    }),
              TextSpan(text: " " * 2),
              TextSpan(
                  text: "Delete",
                  style: const TextStyle(color: Colors.green),
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      widget.content["resultAttributeList"].remove(resultAttribute);
                      setState(() {});
                    })
            ])),
        ])
      ],
    );
  }
}
