import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_admin/admin/system/menu.dart';
import 'package:test_framework_admin/api.dart';
import 'package:test_framework_admin/common/dynamic_form.dart';
import 'package:test_framework_admin/common/dynamic_table.dart';
import 'package:test_framework_admin/common/reloadable_state.dart';
import 'package:test_framework_admin/model/model.dart';

class AdminSysPermListPageWidget extends StatelessWidget {
  final String? parentId;
  const AdminSysPermListPageWidget({this.parentId, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Perm list"),
          actions: [
            IconButton(
                onPressed: () {
                  Provider.of<SecurityModel>(context, listen: false).logout();
                },
                icon: const Icon(Icons.logout))
          ],
        ),
        drawer: const Drawer(child: MenuWidget()),
        body: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: AdminSysPermListWidget(
                        parentId: parentId,
                      )),
                )
              ],
            )));
  }
}

class AdminSysPermListWidget extends StatefulWidget {
  final String? parentId;
  const AdminSysPermListWidget({this.parentId, Key? key}) : super(key: key);

  @override
  State<AdminSysPermListWidget> createState() => _AdminSysPermListState();
}

class _AdminSysPermListState extends ReloadableState<AdminSysPermListWidget> {
  Map<dynamic, dynamic> sysPermSearchRequest = {};
  var sysPermRowCount = 0;
  var sysPermCurrentPage = 1;
  var sysPermResultList = [];
  var sysPermRowPerPage = 25;

  @override
  void initState() {
    super.initState();
    sysPermSearch({});
  }

  @override
  refreshData() {
    sysPermPageChange(sysPermCurrentPage);
  }

  sysPermSearch(sysPermSearchRequest) async {
    this.sysPermSearchRequest = sysPermSearchRequest;
    await sysPermPageChange(1);
  }

  sysPermSearchActionHandler(actionName, formData) async {
    if (actionName == "sysPermSearch") {
      sysPermSearchRequest = formData;
      await sysPermPageChange(1);
    }
  }

  sysPermPageChange(page) async {
    if ((page - 1) * sysPermRowPerPage > sysPermRowCount) {
      page = (1.0 * sysPermRowCount / sysPermRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var sysPermFindRequest = {"queryOffset": (page - 1) * sysPermRowPerPage, "queryLimit": sysPermRowPerPage};
    if (widget.parentId != null) {
      sysPermFindRequest["parentId"] = widget.parentId;
    } else {
      sysPermFindRequest["parentIdIsNull"] = true;
    }
    if (sysPermSearchRequest["nameLike"] != null && sysPermSearchRequest["nameLike"].isNotEmpty) {
      sysPermFindRequest["nameLike"] = "%${sysPermSearchRequest["nameLike"]}%";
    }
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var sysPermFindResponse = await httpPost("/test-framework-api/admin/sys/sys-perm/search", sysPermFindRequest, securityModel.authorization);
    if (sysPermFindResponse.containsKey("body") && sysPermFindResponse["body"] is String == false) {
      setState(() {
        sysPermCurrentPage = page;
        sysPermRowCount = sysPermFindResponse["body"]["rowCount"];
        sysPermResultList = sysPermFindResponse["body"]["resultList"];
      });
    }
  }

  sysPermMoveUpHandler(sysPerm) async {
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    await httpPatch("/test-framework-api/admin/sys/sys-perm/${sysPerm["id"]}/up", {}, securityModel.authorization);
    await sysPermPageChange(sysPermCurrentPage);
  }

  sysPermMoveDownHandler(sysPerm) async {
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    await httpPatch("/test-framework-api/admin/sys/sys-perm/${sysPerm["id"]}/down", {}, securityModel.authorization);
    await sysPermPageChange(sysPermCurrentPage);
  }

  getResultColumns() {
    return [
      const DataColumn(
        label: Text(
          'Name',
          overflow: TextOverflow.visible,
          softWrap: true,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Type',
          overflow: TextOverflow.visible,
          softWrap: true,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Status',
          overflow: TextOverflow.visible,
          softWrap: true,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      DataColumn(
          label: Row(children: [
        ElevatedButton(
          onPressed: () {
            if (widget.parentId != null) {
              Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-perm/create/${widget.parentId}");
            } else {
              Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-perm/create");
            }
          },
          child: const Text("Add"),
        )
      ]))
    ];
  }

  getResultRows() {
    var sysPermStatusMap = {1: "Active", 0: "Inactive"};
    return [
      for (var sysPerm in sysPermResultList)
        DataRow(
          cells: [
            DataCell(Text(sysPerm["name"])),
            DataCell(Text(sysPerm["permType"])),
            DataCell(Text(sysPermStatusMap[sysPerm["status"]] ?? "Not defined")),
            DataCell(Row(
              children: [
                ElevatedButton(
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.green)),
                  onPressed: () {
                    sysPermMoveUpHandler(sysPerm);
                  },
                  child: const Text('Up'),
                ),
                ElevatedButton(
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.green)),
                  onPressed: () {
                    sysPermMoveDownHandler(sysPerm);
                  },
                  child: const Text('Down'),
                ),
                ElevatedButton(
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-perm/edit/${sysPerm["id"]}");
                  },
                  child: const Text('Edit'),
                ),
                ElevatedButton(
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.red)),
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-perm/delete/${sysPerm["id"]}");
                  },
                  child: const Text('Delete'),
                ),
              ],
            )),
          ],
        )
    ];
  }

  @override
  Widget build(BuildContext context) {
    const formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "nameLike", "label": "Name", "hintText": "name input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "action", "actionName": "sysPermSearch", "actionLabel": "Search"}
          ]
        }
      ]
    };
    var resultColumns = getResultColumns();
    var resultRows = getResultRows();
    return Column(mainAxisSize: MainAxisSize.min, children: [
      DynamicForm(formConfig: formConfig, optionListMap: const {}, formData: sysPermSearchRequest, actionHandler: sysPermSearchActionHandler),
      Row(mainAxisSize: MainAxisSize.max, children: [
        Expanded(
            child: DynamicTable(
          columns: resultColumns,
          rows: resultRows,
        ))
      ]),
      DynamicTablePagging(
        sysPermRowCount,
        sysPermCurrentPage,
        sysPermRowPerPage,
        pageChangeHandler: (page) {
          sysPermPageChange(page);
        },
        rowPerPageChangeHandler: (rowPerPage) {
          sysPermRowPerPage = rowPerPage ?? 10;
          sysPermPageChange(1);
        },
      )
    ]);
  }
}

class AdminSysPermDetailWidget extends StatefulWidget {
  final String sysPermAction;
  final String? sysPermId;
  final String? parentId;
  const AdminSysPermDetailWidget({required this.sysPermAction, this.sysPermId, this.parentId, Key? key}) : super(key: key);

  @override
  State<AdminSysPermDetailWidget> createState() => _AdminSysPermDetailState();
}

class _AdminSysPermDetailState extends ReloadableState<AdminSysPermDetailWidget> {
  List<dynamic>? sysPermPathList;
  Map<dynamic, dynamic>? sysPerm;
  dynamic formConfig = {
    "type": "column",
    "children": [
      {
        "type": "row",
        "children": [
          {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
        ]
      }
    ]
  };
  dynamic optionListMap = {};
  String? error;

  @override
  void initState() {
    super.initState();
    reload();
  }

  @override
  refreshData() {}

  reload() async {
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    if (widget.sysPermId != null) {
      var sysPermGetResponse = await httpGet("/test-framework-api/admin/sys/sys-perm/${widget.sysPermId!}", securityModel.authorization);
      if (sysPermGetResponse.containsKey("body") && sysPermGetResponse["body"] is String == false) {
        sysPerm = sysPermGetResponse["body"]["result"];
      }
      var sysPermPathResponse = await httpGet("/test-framework-api/admin/sys/sys-perm/${widget.sysPermId!}/path", securityModel.authorization);
      if (sysPermPathResponse.containsKey("body") && sysPermPathResponse["body"] is String == false) {
        sysPermPathList = sysPermPathResponse["body"]["resultList"];
      }
    } else {
      sysPerm = {
        "status": 0,
        "parentId": widget.parentId,
      };
      if (widget.parentId != null) {
        var sysPermPathResponse = await httpGet("/test-framework-api/admin/sys/sys-perm/${widget.parentId!}/path", securityModel.authorization);
        if (sysPermPathResponse.containsKey("body") && sysPermPathResponse["body"] is String == false) {
          sysPermPathList = sysPermPathResponse["body"]["resultList"];
        }
      }
    }
    await resetForm();
  }

  resetForm() async {
    var submitActionLabel = "Submit";
    var actionSubmitType = "primary";
    if (widget.sysPermAction == "delete") {
      submitActionLabel = "Delete";
      actionSubmitType = "danger";
    }
    optionListMap = {
      "permType": [
        {"value": "user", "label": "user"},
        {"value": "project", "label": "project"},
        {"value": "organization", "label": "organization"}
      ],
      "status": [
        {"value": 1, "label": "Active"},
        {"value": 0, "label": "Inactive"}
      ]
    };
    formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "permType", "label": "permType", "hintText": "permType input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "code", "label": "Code", "hintText": "Code input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "name", "label": "Name", "hintText": "Name input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "description", "label": "description", "hintText": "description input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "status", "label": "status", "hintText": "status input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "action", "actionType": actionSubmitType, "actionName": "submit", "actionLabel": submitActionLabel},
            {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
          ]
        }
      ]
    };
    setState(() {});
  }

  sysPermDetailActionHandler(actionName, formData) async {
    if (sysPerm == null) {
      return;
    }
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    if (actionName == "submit") {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      if (widget.sysPermAction == "create") {
        var sysPermCreateResponse = await httpPut("/test-framework-api/admin/sys/sys-perm", formData, securityModel.authorization);
        if (sysPermCreateResponse.containsKey("body") && sysPermCreateResponse["body"] is String == false) {
          var sysPermCreateResponseBody = sysPermCreateResponse["body"];
          if (sysPermCreateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (sysPermCreateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(sysPermCreateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.sysPermAction == "edit") {
        var sysPermEditResponse = await httpPost("/test-framework-api/admin/sys/sys-perm/${widget.sysPermId}", formData, securityModel.authorization);
        if (sysPermEditResponse.containsKey("body") && sysPermEditResponse["body"] is String == false) {
          var sysPermEditResponseBody = sysPermEditResponse["body"];
          if (sysPermEditResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (sysPermEditResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(sysPermEditResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.sysPermAction == "delete") {
        var sysPermDeleteResponse = await httpDelete("/test-framework-api/admin/sys/sys-perm/${widget.sysPermId}", securityModel.authorization);
        if (sysPermDeleteResponse.containsKey("body") && sysPermDeleteResponse["body"] is String == false) {
          var sysPermDeleteResponseBody = sysPermDeleteResponse["body"];
          if (sysPermDeleteResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (sysPermDeleteResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(sysPermDeleteResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
    }
    if (actionName == "back") {
      if (sysPerm!["parentId"] != null) {
        navigationModel.pop("/admin/system/sys-perm/edit/${sysPerm!["parentId"]}");
      } else {
        navigationModel.pop("/admin/system/sys-perm");
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    if (error != null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Error'),
          ),
          body: Center(child: Text("Error $error")));
    }
    if (sysPerm == null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Loading'),
          ),
          body: const Center(child: CircularProgressIndicator()));
    }
    if (widget.sysPermAction == "create" || widget.sysPermAction == "delete") {
      return Scaffold(
        appBar: AppBar(
          title: const Text('Perm detail'),
        ),
        drawer: const Drawer(child: MenuWidget()),
        body: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(mainAxisSize: MainAxisSize.min, children: [
                        Row(
                          children: [
                            RichText(
                                text: TextSpan(children: [
                              TextSpan(
                                  text: "Root",
                                  style: const TextStyle(color: Colors.blue),
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () {
                                      Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-perm");
                                    }),
                            ])),
                            for (var sysPermPath in sysPermPathList ?? [])
                              RichText(
                                  text: TextSpan(children: [
                                const TextSpan(text: "/"),
                                TextSpan(
                                    text: "${sysPermPath["name"]}",
                                    style: const TextStyle(color: Colors.blue),
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        Provider.of<NavigationModel>(context, listen: false)
                                            .navigate("/admin/system/sys-perm/edit/${sysPermPath!["id"]}");
                                      }),
                              ]))
                          ],
                        ),
                        Card(
                            child: Column(mainAxisSize: MainAxisSize.min, children: [
                          const Row(
                            children: [
                              Text(
                                "Detail",
                                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          formConfig != null && optionListMap != null
                              ? DynamicForm(
                                  formConfig: formConfig, formData: sysPerm!, optionListMap: optionListMap, actionHandler: sysPermDetailActionHandler)
                              : Container()
                        ])),
                      ])),
                )
              ],
            )),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          title: const Text("Perm detail"),
        ),
        drawer: const Drawer(child: MenuWidget()),
        body: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(mainAxisSize: MainAxisSize.min, children: [
                        Row(
                          children: [
                            RichText(
                                text: TextSpan(children: [
                              TextSpan(
                                  text: "Root",
                                  style: const TextStyle(color: Colors.blue),
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () {
                                      Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-perm");
                                    }),
                            ])),
                            for (var sysPermPath in sysPermPathList ?? [])
                              RichText(
                                  text: TextSpan(children: [
                                const TextSpan(text: "/"),
                                TextSpan(
                                    text: "${sysPermPath["name"]}",
                                    style: const TextStyle(color: Colors.blue),
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        Provider.of<NavigationModel>(context, listen: false)
                                            .navigate("/admin/system/sys-perm/edit/${sysPermPath!["id"]}");
                                      }),
                              ]))
                          ],
                        ),
                        Card(
                            child: Column(mainAxisSize: MainAxisSize.min, children: [
                          const Row(
                            children: [
                              Text(
                                "Detail",
                                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          formConfig != null && optionListMap != null
                              ? DynamicForm(
                                  formConfig: formConfig, formData: sysPerm!, optionListMap: optionListMap, actionHandler: sysPermDetailActionHandler)
                              : Container()
                        ])),
                        Card(
                            child: Column(mainAxisSize: MainAxisSize.min, children: [
                          const Row(
                            children: [
                              Text(
                                "Child",
                                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          AdminSysPermListWidget(
                            parentId: sysPerm!["id"],
                          ),
                        ])),
                      ])),
                )
              ],
            )),
      );
    }
  }
}
