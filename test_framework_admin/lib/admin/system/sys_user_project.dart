import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_admin/admin/system/menu.dart';
import 'package:test_framework_admin/api.dart';
import 'package:test_framework_admin/common/dynamic_form.dart';
import 'package:test_framework_admin/common/dynamic_table.dart';
import 'package:test_framework_admin/common/reloadable_state.dart';
import 'package:test_framework_admin/common/tree.dart';
import 'package:test_framework_admin/model/model.dart';

class AdminSysUserProjectListWidget extends StatefulWidget {
  const AdminSysUserProjectListWidget({Key? key}) : super(key: key);

  @override
  State<AdminSysUserProjectListWidget> createState() => _AdminSysUserProjectListState();
}

class _AdminSysUserProjectListState extends ReloadableState<AdminSysUserProjectListWidget> {
  var sysUserProjectSearchRequest = {};
  var sysUserProjectRowCount = 0;
  var sysUserProjectCurrentPage = 1;
  var sysUserProjectResultList = [];
  var sysUserProjectRowPerPage = 25;
  Widget paging = Container();

  @override
  void initState() {
    super.initState();
    sysUserProjectSearch({});
  }

  @override
  refreshData() {
    sysUserProjectPageChange(sysUserProjectCurrentPage);
  }

  sysUserProjectSearch(sysUserProjectSearchRequest) async {
    this.sysUserProjectSearchRequest = sysUserProjectSearchRequest;
    await sysUserProjectPageChange(1);
  }

  sysUserProjectActionHandler(actionName, formData) async {
    if (actionName == "search") {
      sysUserProjectSearchRequest = formData;
      await sysUserProjectPageChange(1);
    }
  }

  sysUserProjectPageChange(page) async {
    if ((page - 1) * sysUserProjectRowPerPage > sysUserProjectRowCount) {
      page = (1.0 * sysUserProjectRowCount / sysUserProjectRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * sysUserProjectRowPerPage, "queryLimit": sysUserProjectRowPerPage};
    if (sysUserProjectSearchRequest["fullnameLike"] != null && sysUserProjectSearchRequest["fullnameLike"].isNotEmpty) {
      findRequest["fullnameLike"] = "%${sysUserProjectSearchRequest["fullnameLike"]}%";
    }
    if (sysUserProjectSearchRequest["nicknameLike"] != null && sysUserProjectSearchRequest["nicknameLike"].isNotEmpty) {
      findRequest["nicknameLike"] = "%${sysUserProjectSearchRequest["nicknameLike"]}%";
    }
    if (sysUserProjectSearchRequest["emailLike"] != null && sysUserProjectSearchRequest["emailLike"].isNotEmpty) {
      findRequest["emailLike"] = "%${sysUserProjectSearchRequest["emailLike"]}%";
    }
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var response = await httpPost("/test-framework-api/admin/sys/sys-user-project/search", findRequest, securityModel.authorization);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        sysUserProjectCurrentPage = page;
        sysUserProjectRowCount = response["body"]["rowCount"];
        sysUserProjectResultList = response["body"]["resultList"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var optionListMap = {};
    const formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "fullnameLike", "label": "Fullname", "hintText": "Fullname input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "nicknameLike", "label": "nickname", "hintText": "nickname input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "emailLike", "label": "Email", "hintText": "Email input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "action", "actionName": "search", "actionLabel": "Search"}
          ]
        }
      ]
    };
    var resultColumns = [
      const DataColumn(
        label: Text(
          'Fullname',
          overflow: TextOverflow.visible,
          softWrap: true,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Nickname',
          overflow: TextOverflow.visible,
          softWrap: true,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Email',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Project',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      DataColumn(
          label: Row(
        children: [
          ElevatedButton(
            onPressed: () {
              Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-user-project/create");
            },
            child: const Text('Add'),
          )
        ],
      )),
    ];
    var resultRows = [
      for (var sysUserProject in sysUserProjectResultList)
        DataRow(
          cells: [
            DataCell(Text(sysUserProject["sysUser"]["fullname"] ?? "")),
            DataCell(Text(sysUserProject["sysUser"]["nickname"] ?? "")),
            DataCell(Text(sysUserProject["sysUser"]["email"] ?? "")),
            DataCell(Text(sysUserProject["prjProject"]["name"] ?? "")),
            DataCell(Row(
              children: [
                ElevatedButton(
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-user-project/edit/${sysUserProject["id"]}");
                  },
                  child: const Text('Edit'),
                ),
                ElevatedButton(
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.red)),
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-user-project/delete/${sysUserProject["id"]}");
                  },
                  child: const Text('Delete'),
                ),
              ],
            )),
          ],
        )
    ];

    return Scaffold(
        appBar: AppBar(
          title: const Text("User list"),
          actions: [
            IconButton(
                onPressed: () {
                  Provider.of<SecurityModel>(context, listen: false).logout();
                },
                icon: const Icon(Icons.logout))
          ],
        ),
        drawer: const Drawer(child: MenuWidget()),
        body: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(mainAxisSize: MainAxisSize.min, children: [
                        DynamicForm(
                            formConfig: formConfig,
                            optionListMap: optionListMap,
                            formData: sysUserProjectSearchRequest,
                            actionHandler: sysUserProjectActionHandler),
                        Row(mainAxisSize: MainAxisSize.max, children: [
                          Expanded(
                              child: DynamicTable(
                            columns: resultColumns,
                            rows: resultRows,
                          ))
                        ]),
                        DynamicTablePagging(
                          sysUserProjectRowCount,
                          sysUserProjectCurrentPage,
                          sysUserProjectRowPerPage,
                          pageChangeHandler: (page) {
                            sysUserProjectPageChange(page);
                          },
                          rowPerPageChangeHandler: (rowPerPage) {
                            sysUserProjectRowPerPage = rowPerPage ?? 10;
                            sysUserProjectPageChange(1);
                          },
                        )
                      ])),
                )
              ],
            )));
  }
}

class AdminSysUserProjectDetailWidget extends StatefulWidget {
  final String? sysUserProjectId;
  final String sysUserProjectAction;
  const AdminSysUserProjectDetailWidget({required this.sysUserProjectAction, this.sysUserProjectId, Key? key}) : super(key: key);

  @override
  State<AdminSysUserProjectDetailWidget> createState() => _AdminSysUserProjectDetailState();
}

class _AdminSysUserProjectDetailState extends ReloadableState<AdminSysUserProjectDetailWidget> {
  dynamic formConfig = {
    "type": "column",
    "children": [
      {
        "type": "row",
        "children": [
          {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
        ]
      }
    ]
  };
  dynamic optionListMap = {};
  Map<dynamic, dynamic>? sysUserProject;
  TreeData? sysPermTreeData;
  String? error;
  var hasEditPermission = false;

  @override
  void initState() {
    super.initState();
    var userPermList = Provider.of<SecurityModel>(context, listen: false).userPermList;
    if (userPermList != null) {
      hasEditPermission = userPermList.where((permCode) => permCode == "ADMIN").isNotEmpty;
    }
    reload();
  }

  @override
  refreshData() {}

  reload() async {
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var sysPermFindRequest = {"permType": "project", "status": 1};
    var sysPermFindResponse = await httpPost("/test-framework-api/admin/sys/sys-perm/search", sysPermFindRequest, securityModel.authorization);
    if (sysPermFindResponse.containsKey("body") && sysPermFindResponse["body"] is String == false) {
      var sysPermList = sysPermFindResponse["body"]["resultList"];
      List<TreeNode> treeNodeList = [];
      for (var sysPerm in sysPermList) {
        treeNodeList.add(TreeNode(key: sysPerm["id"], parentKey: sysPerm["parentId"], item: sysPerm));
      }
      sysPermTreeData = TreeData(treeNodeList);
    }
    if (widget.sysUserProjectId != null) {
      var sysUserProjectGetResponse =
          await httpGet("/test-framework-api/admin/sys/sys-user-project/${widget.sysUserProjectId}", securityModel.authorization);
      if (sysUserProjectGetResponse.containsKey("body") && sysUserProjectGetResponse["body"] is String == false) {
        sysUserProject = sysUserProjectGetResponse["body"]["result"];
      }
    } else {
      sysUserProject = {"status": 0};
    }
    await resetForm();
  }

  resetForm() async {
    var submitActionLabel = "Submit";
    var actionSubmitType = "primary";
    if (widget.sysUserProjectAction == "delete") {
      submitActionLabel = "Delete";
      actionSubmitType = "danger";
    }
    optionListMap = {
      "sysUserId": [],
      "prjProjectId": [],
    };
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var sysUserFindResponse = await httpPost("/test-framework-api/admin/sys/sys-user/search", {"status": 1}, securityModel.authorization);
    if (sysUserFindResponse.containsKey("body") && sysUserFindResponse["body"] is String == false) {
      optionListMap["sysUserId"] = sysUserFindResponse["body"]["resultList"]
          .map((sysUser) => {"disabled": widget.sysUserProjectAction != "create", "value": sysUser["id"], "label": sysUser["email"]})
          .toList();
    }
    if (sysUserProject != null) {
      if (sysUserProject!["sysUserId"] != null) {
        if (sysUserFindResponse.containsKey("body") && sysUserFindResponse["body"] is String == false) {
          for (var sysUser in sysUserFindResponse["body"]["resultList"]) {
            if (sysUser["id"] == sysUserProject!["sysUserId"]) {
              var prjProjectFindResponse = await httpPost("/test-framework-api/admin/project/prj-project/search",
                  {"status": 1, "sysOrganizationId": sysUser["sysOrganizationId"]}, securityModel.authorization);
              if (prjProjectFindResponse.containsKey("body") && prjProjectFindResponse["body"] is String == false) {
                optionListMap["prjProjectId"] = prjProjectFindResponse["body"]["resultList"]
                    .map((sysRole) => {"disabled": widget.sysUserProjectAction != "create", "value": sysRole["id"], "label": sysRole["name"]})
                    .toList();
              }
              break;
            }
          }
        }
      }
    }

    formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "sysUserId", "label": "User", "hintText": "User input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "prjProjectId", "label": "Project", "hintText": "Project input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {
              "type": "treeMultipleSelect",
              "expanded": true,
              "fieldName": "sysPermIdList",
              "label": "Perm",
              "hintText": "Perm input",
              "getTreeNodeLabel": (treeNode) => "${treeNode.item["code"]} - ${treeNode.item["name"]}",
              "treeData": sysPermTreeData
            }
          ]
        },
        {
          "type": "row",
          "children": hasEditPermission
              ? [
                  {"type": "action", "actionType": actionSubmitType, "actionName": "submit", "actionLabel": submitActionLabel},
                  {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
                ]
              : [
                  {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
                ]
        }
      ]
    };
    setState(() {});
  }

  onChanged(fieldName, value) async {
    if (fieldName == "sysUserId") {
      sysUserProject!["sysUserId"] = value;
    }
    if (fieldName == "sysUserId") {
      resetForm();
    }
  }

  actionHandler(actionName, formData) async {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    if (actionName == "submit") {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      if (widget.sysUserProjectAction == "create") {
        var sysUserProjectCreateResponse = await httpPut("/test-framework-api/user/sys/sys-user", formData, securityModel.authorization);
        if (sysUserProjectCreateResponse.containsKey("body") && sysUserProjectCreateResponse["body"] is String == false) {
          var sysUserProjectCreateResponseBody = sysUserProjectCreateResponse["body"];
          if (sysUserProjectCreateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (sysUserProjectCreateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(sysUserProjectCreateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.sysUserProjectAction == "edit") {
        var sysUserProjectUpdateResponse =
            await httpPost("/test-framework-api/admin/sys/sys-user-project/${widget.sysUserProjectId}", formData, securityModel.authorization);
        if (sysUserProjectUpdateResponse.containsKey("body") && sysUserProjectUpdateResponse["body"] is String == false) {
          var sysUserProjectUpdateResponseBody = sysUserProjectUpdateResponse["body"];
          if (sysUserProjectUpdateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (sysUserProjectUpdateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(sysUserProjectUpdateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.sysUserProjectAction == "delete") {
        var sysUserProjectDeleteResponse =
            await httpDelete("/test-framework-api/admin/sys/sys-user-project/${widget.sysUserProjectId}", securityModel.authorization);
        if (sysUserProjectDeleteResponse.containsKey("body") && sysUserProjectDeleteResponse["body"] is String == false) {
          var sysUserProjectDeleteResponseBody = sysUserProjectDeleteResponse["body"];
          if (sysUserProjectDeleteResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (sysUserProjectDeleteResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(sysUserProjectDeleteResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
    }
    if (actionName == "back") {
      navigationModel.pop("/admin/system/sys-user-project");
    }
  }

  @override
  Widget build(BuildContext context) {
    if (error != null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Error'),
          ),
          body: Center(child: Text("Error $error")));
    }
    if (sysUserProject == null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Loading'),
          ),
          body: const Center(child: CircularProgressIndicator()));
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text('User detail'),
      ),
      drawer: const Drawer(child: MenuWidget()),
      body: Padding(
          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(mainAxisSize: MainAxisSize.min, children: [
                      Card(
                          child: Column(mainAxisSize: MainAxisSize.min, children: [
                        const Row(
                          children: [
                            Text(
                              "Detail",
                              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        formConfig != null && optionListMap != null
                            ? DynamicForm(
                                formConfig: formConfig,
                                formData: sysUserProject!,
                                optionListMap: optionListMap,
                                actionHandler: actionHandler,
                                onChanged: onChanged)
                            : Container()
                      ]))
                    ])),
              )
            ],
          )),
    );
  }
}
