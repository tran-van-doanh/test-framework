import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_admin/admin/system/menu.dart';
import 'package:test_framework_admin/api.dart';
import 'package:test_framework_admin/common/dynamic_form.dart';
import 'package:test_framework_admin/common/dynamic_table.dart';
import 'package:test_framework_admin/common/reloadable_state.dart';
import 'package:test_framework_admin/common/tree.dart';
import 'package:test_framework_admin/model/model.dart';

class AdminSysNodeFileListWidget extends StatefulWidget {
  const AdminSysNodeFileListWidget({Key? key}) : super(key: key);

  @override
  State<AdminSysNodeFileListWidget> createState() => _AdminSysNodeFileListState();
}

class _AdminSysNodeFileListState extends ReloadableState<AdminSysNodeFileListWidget> {
  var sysNodeFileSearchRequest = {};
  var sysNodeFileRowCount = 0;
  var sysNodeFileCurrentPage = 1;
  var sysNodeFileResultList = [];
  var sysNodeFileRowPerPage = 25;
  Widget paging = Container();

  @override
  void initState() {
    super.initState();
    sysNodeFileSearch({});
  }

  @override
  refreshData() {
    sysNodeFilePageChange(sysNodeFileCurrentPage);
  }

  sysNodeFileSearch(sysNodeFileSearchRequest) async {
    this.sysNodeFileSearchRequest = sysNodeFileSearchRequest;
    await sysNodeFilePageChange(1);
  }

  sysNodeFileActionHandler(actionName, formData) async {
    if (actionName == "search") {
      sysNodeFileSearchRequest = formData;
      await sysNodeFilePageChange(1);
    }
  }

  sysNodeFilePageChange(page) async {
    if ((page - 1) * sysNodeFileRowPerPage > sysNodeFileRowCount) {
      page = (1.0 * sysNodeFileRowCount / sysNodeFileRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * sysNodeFileRowPerPage, "queryLimit": sysNodeFileRowPerPage};
    if (sysNodeFileSearchRequest["fileType"] != null && sysNodeFileSearchRequest["fileType"].isNotEmpty) {
      findRequest["fileType"] = sysNodeFileSearchRequest["fileType"];
    }
    if (sysNodeFileSearchRequest["fileNameLike"] != null && sysNodeFileSearchRequest["fileNameLike"].isNotEmpty) {
      findRequest["fileNameLike"] = "%${sysNodeFileSearchRequest["fileNameLike"]}%";
    }
    if (sysNodeFileSearchRequest["status"] != null) {
      findRequest["status"] = sysNodeFileSearchRequest["status"];
    }
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var response = await httpPost("/test-framework-api/admin/sys/sys-node-file/search", findRequest, securityModel.authorization);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        sysNodeFileCurrentPage = page;
        sysNodeFileRowCount = response["body"]["rowCount"];
        sysNodeFileResultList = response["body"]["resultList"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var sysNodeFileStatusMap = {1: "Active", 0: "Inactive"};
    var optionListMap = {
      "status": [
        {"value": 1, "label": "Active"},
        {"value": 0, "label": "Inactive"}
      ],
    };
    const formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "fileType", "label": "File type", "hintText": "File type input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "fileNameLike", "label": "File name", "hintText": "File name input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "status", "label": "Status", "hintText": "Status input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "action", "actionName": "search", "actionLabel": "Search"}
          ]
        }
      ]
    };

    return Scaffold(
        appBar: AppBar(
          title: const Text("Node list"),
          actions: [
            IconButton(
                onPressed: () {
                  Provider.of<SecurityModel>(context, listen: false).logout();
                },
                icon: const Icon(Icons.logout))
          ],
        ),
        drawer: const Drawer(child: MenuWidget()),
        body: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(mainAxisSize: MainAxisSize.min, children: [
                        DynamicForm(
                            formConfig: formConfig,
                            optionListMap: optionListMap,
                            formData: sysNodeFileSearchRequest,
                            actionHandler: sysNodeFileActionHandler),
                        LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
                          if (constraints.maxWidth < 600) {
                            return Column(
                              children: [
                                for (var sysNodeFile in sysNodeFileResultList)
                                  Column(children: [
                                    Row(children: [
                                      const Text(
                                        "File type: ",
                                        style: TextStyle(fontWeight: FontWeight.bold),
                                      ),
                                      Text(sysNodeFile["fileType"] ?? "")
                                    ]),
                                    Row(children: [
                                      const Text(
                                        "File name: ",
                                        style: TextStyle(fontWeight: FontWeight.bold),
                                      ),
                                      Text(sysNodeFile["fileName"] ?? "")
                                    ]),
                                    Row(children: [
                                      const Text(
                                        "Status: ",
                                        style: TextStyle(fontWeight: FontWeight.bold),
                                      ),
                                      Text(sysNodeFileStatusMap[sysNodeFile["status"]] ?? "Not defined")
                                    ]),
                                    Row(
                                      children: [
                                        Expanded(
                                            child: ElevatedButton(
                                          onPressed: () {
                                            Provider.of<NavigationModel>(context, listen: false)
                                                .navigate("/admin/system/sys-node-file/edit/${sysNodeFile["id"]}");
                                          },
                                          child: const Text('Edit'),
                                        )),
                                        Expanded(
                                            child: ElevatedButton(
                                          style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.red)),
                                          onPressed: () {
                                            Provider.of<NavigationModel>(context, listen: false)
                                                .navigate("/admin/system/sys-node-file/delete/${sysNodeFile["id"]}");
                                          },
                                          child: const Text('Delete'),
                                        )),
                                      ],
                                    )
                                  ])
                              ],
                            );
                          } else {
                            var resultColumns = [
                              const DataColumn(
                                label: Text(
                                  'Type',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              const DataColumn(
                                label: Text(
                                  'Name',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              const DataColumn(
                                label: Text(
                                  'Status',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              DataColumn(
                                  label: Row(
                                children: [
                                  ElevatedButton(
                                    onPressed: () {
                                      Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-node-file/create");
                                    },
                                    child: const Text('Add'),
                                  ),
                                  ElevatedButton(
                                    onPressed: () async {
                                      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
                                      var sysNodeFileUpdateResponse =
                                          await httpPost("/test-framework-api/admin/sys/sys-node-file/reload", {}, securityModel.authorization);
                                      if (sysNodeFileUpdateResponse.containsKey("body") && sysNodeFileUpdateResponse["body"] is String == false) {
                                        var sysNodeFileUpdateResponseBody = sysNodeFileUpdateResponse["body"];
                                        if (sysNodeFileUpdateResponseBody.containsKey("result")) {
                                        } else if (sysNodeFileUpdateResponseBody.containsKey("errorMessage") && mounted) {
                                          showDialog<void>(
                                            context: this.context,
                                            builder: (BuildContext context) => AlertDialog(
                                              title: const Text('Error'),
                                              content: Text(sysNodeFileUpdateResponseBody["errorMessage"]),
                                              actions: <Widget>[
                                                TextButton(
                                                  onPressed: () => Navigator.pop(context),
                                                  child: const Text('Close'),
                                                ),
                                              ],
                                            ),
                                          );
                                        }
                                      }
                                    },
                                    child: const Text('Reload'),
                                  ),
                                  ElevatedButton(
                                    onPressed: () async {
                                      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
                                      var sysNodeFileUpdateResponse =
                                          await httpDelete("/test-framework-api/admin/sys/sys-node-file/delete-all", securityModel.authorization);
                                      if (sysNodeFileUpdateResponse.containsKey("body") && sysNodeFileUpdateResponse["body"] is String == false) {
                                        var sysNodeFileUpdateResponseBody = sysNodeFileUpdateResponse["body"];
                                        if (sysNodeFileUpdateResponseBody.containsKey("result")) {
                                        } else if (sysNodeFileUpdateResponseBody.containsKey("errorMessage") && mounted) {
                                          showDialog<void>(
                                            context: this.context,
                                            builder: (BuildContext context) => AlertDialog(
                                              title: const Text('Error'),
                                              content: Text(sysNodeFileUpdateResponseBody["errorMessage"]),
                                              actions: <Widget>[
                                                TextButton(
                                                  onPressed: () => Navigator.pop(context),
                                                  child: const Text('Close'),
                                                ),
                                              ],
                                            ),
                                          );
                                        }
                                      }
                                    },
                                    child: const Text('Delete all'),
                                  )
                                ],
                              )),
                            ];
                            var resultRows = [
                              for (var sysNodeFile in sysNodeFileResultList)
                                DataRow(
                                  cells: [
                                    DataCell(Text(sysNodeFile["fileType"] ?? "")),
                                    DataCell(Text(sysNodeFile["fileName"] ?? "")),
                                    DataCell(Text(sysNodeFileStatusMap[sysNodeFile["status"]] ?? "Not defined")),
                                    DataCell(Row(
                                      children: [
                                        ElevatedButton(
                                          onPressed: () {
                                            Provider.of<NavigationModel>(context, listen: false)
                                                .navigate("/admin/system/sys-node-file/edit/${sysNodeFile["id"]}");
                                          },
                                          child: const Text('Edit'),
                                        ),
                                        ElevatedButton(
                                          style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.red)),
                                          onPressed: () {
                                            Provider.of<NavigationModel>(context, listen: false)
                                                .navigate("/admin/system/sys-node-file/delete/${sysNodeFile["id"]}");
                                          },
                                          child: const Text('Delete'),
                                        ),
                                      ],
                                    )),
                                  ],
                                )
                            ];
                            return Row(mainAxisSize: MainAxisSize.max, children: [
                              Expanded(
                                  child: DynamicTable(
                                minWidth: 1280,
                                columns: resultColumns,
                                rows: resultRows,
                              ))
                            ]);
                          }
                        }),
                        DynamicTablePagging(
                          sysNodeFileRowCount,
                          sysNodeFileCurrentPage,
                          sysNodeFileRowPerPage,
                          pageChangeHandler: (page) {
                            sysNodeFilePageChange(page);
                          },
                          rowPerPageChangeHandler: (rowPerPage) {
                            sysNodeFileRowPerPage = rowPerPage ?? 10;
                            sysNodeFilePageChange(1);
                          },
                        )
                      ])),
                )
              ],
            )));
  }
}

class AdminSysNodeFileDetailWidget extends StatefulWidget {
  final String? sysNodeFileId;
  final String sysNodeFileAction;
  const AdminSysNodeFileDetailWidget({required this.sysNodeFileAction, this.sysNodeFileId, Key? key}) : super(key: key);

  @override
  State<AdminSysNodeFileDetailWidget> createState() => _AdminSysNodeFileDetailState();
}

class _AdminSysNodeFileDetailState extends ReloadableState<AdminSysNodeFileDetailWidget> {
  dynamic formConfig = {
    "type": "column",
    "children": [
      {
        "type": "row",
        "children": [
          {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
        ]
      }
    ]
  };
  dynamic optionListMap = {};
  Map<dynamic, dynamic>? sysNodeFile;
  TreeData? sysPermTreeData;
  String? error;
  var hasEditPermission = false;

  @override
  void initState() {
    super.initState();
    var userPermList = Provider.of<SecurityModel>(context, listen: false).userPermList;
    if (userPermList != null) {
      hasEditPermission = userPermList.where((permCode) => permCode == "ADMIN").isNotEmpty;
    }
    reload();
  }

  @override
  refreshData() {}

  reload() async {
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    if (widget.sysNodeFileId != null) {
      var sysNodeFileGetResponse = await httpGet("/test-framework-api/admin/sys/sys-node-file/${widget.sysNodeFileId}", securityModel.authorization);
      if (sysNodeFileGetResponse.containsKey("body") && sysNodeFileGetResponse["body"] is String == false) {
        sysNodeFile = sysNodeFileGetResponse["body"]["result"];
      }
    } else {
      sysNodeFile = {"status": 0};
    }
    await resetForm();
  }

  resetForm() async {
    var submitActionLabel = "Submit";
    var actionSubmitType = "primary";
    if (widget.sysNodeFileAction == "delete") {
      submitActionLabel = "Delete";
      actionSubmitType = "danger";
    }
    optionListMap = {
      "status": [
        {"value": 1, "label": "Active"},
        {"value": 0, "label": "Inactive"}
      ],
    };
    formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "fileType", "label": "File type", "hintText": "File type input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "fileName", "label": "File name", "hintText": "File name input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "filePath", "label": "File path", "hintText": "File path input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "fileHash", "label": "File hash", "hintText": "File hash input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {
              "type": "inputText",
              "maxLines": 5,
              "expanded": true,
              "fieldName": "description",
              "label": "description",
              "hintText": "description input"
            }
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "status", "label": "status", "hintText": "status input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "action", "actionType": actionSubmitType, "actionName": "submit", "actionLabel": submitActionLabel},
            {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
          ]
        }
      ]
    };
    setState(() {});
  }

  actionHandler(actionName, formData) async {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    if (actionName == "submit") {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      if (widget.sysNodeFileAction == "create") {
        var sysNodeFileCreateResponse = await httpPut("/test-framework-api/admin/sys/sys-node-file", formData, securityModel.authorization);
        if (sysNodeFileCreateResponse.containsKey("body") && sysNodeFileCreateResponse["body"] is String == false) {
          var sysNodeFileCreateResponseBody = sysNodeFileCreateResponse["body"];
          if (sysNodeFileCreateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (sysNodeFileCreateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(sysNodeFileCreateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.sysNodeFileAction == "edit") {
        var sysNodeFileUpdateResponse =
            await httpPost("/test-framework-api/admin/sys/sys-node-file/${widget.sysNodeFileId}", formData, securityModel.authorization);
        if (sysNodeFileUpdateResponse.containsKey("body") && sysNodeFileUpdateResponse["body"] is String == false) {
          var sysNodeFileUpdateResponseBody = sysNodeFileUpdateResponse["body"];
          if (sysNodeFileUpdateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (sysNodeFileUpdateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(sysNodeFileUpdateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.sysNodeFileAction == "delete") {
        var sysNodeFileDeleteResponse =
            await httpDelete("/test-framework-api/admin/sys/sys-node-file/${widget.sysNodeFileId}", securityModel.authorization);
        if (sysNodeFileDeleteResponse.containsKey("body") && sysNodeFileDeleteResponse["body"] is String == false) {
          var sysNodeFileDeleteResponseBody = sysNodeFileDeleteResponse["body"];
          if (sysNodeFileDeleteResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (sysNodeFileDeleteResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(sysNodeFileDeleteResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
    }
    if (actionName == "back") {
      navigationModel.pop("/admin/system/sys-node-file");
    }
  }

  @override
  Widget build(BuildContext context) {
    if (error != null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Error'),
          ),
          body: Center(child: Text("Error $error")));
    }
    if (sysNodeFile == null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Loading'),
          ),
          body: const Center(child: CircularProgressIndicator()));
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text('Node detail'),
      ),
      drawer: const Drawer(child: MenuWidget()),
      body: Padding(
          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(mainAxisSize: MainAxisSize.min, children: [
                      Card(
                          child: Column(mainAxisSize: MainAxisSize.min, children: [
                        const Row(
                          children: [
                            Text(
                              "Detail",
                              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        formConfig != null && optionListMap != null
                            ? DynamicForm(formConfig: formConfig, formData: sysNodeFile!, optionListMap: optionListMap, actionHandler: actionHandler)
                            : Container()
                      ]))
                    ])),
              )
            ],
          )),
    );
  }
}
