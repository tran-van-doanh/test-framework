import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_admin/admin/system/menu.dart';
import 'package:test_framework_admin/api.dart';
import 'package:test_framework_admin/common/dynamic_form.dart';
import 'package:test_framework_admin/common/dynamic_table.dart';
import 'package:test_framework_admin/common/reloadable_state.dart';
import 'package:test_framework_admin/common/tree.dart';
import 'package:test_framework_admin/model/model.dart';

class AdminTfValueTemplateListWidget extends StatefulWidget {
  const AdminTfValueTemplateListWidget({Key? key}) : super(key: key);

  @override
  State<AdminTfValueTemplateListWidget> createState() => _AdminTfValueTemplateListState();
}

class _AdminTfValueTemplateListState extends ReloadableState<AdminTfValueTemplateListWidget> {
  dynamic formConfig = {"type": "column", "children": []};
  dynamic optionListMap = {};
  var tfValueTemplateSearchRequest = {};
  var tfValueTemplateRowCount = 0;
  var tfValueTemplateCurrentPage = 1;
  var tfValueTemplateResultList = [];
  var tfValueTemplateRowPerPage = 25;
  Widget paging = Container();

  @override
  void initState() {
    super.initState();
    reset();
  }

  @override
  refreshData() {
    tfValueTemplatePageChange(tfValueTemplateCurrentPage);
  }

  reset() async {
    await resetForm();
    await tfValueTemplateSearch({});
  }

  resetForm() async {
    optionListMap = {
      "status": [
        {"value": 1, "label": "Active"},
        {"value": 0, "label": "Inactive"}
      ],
      "tfValueTypeId": []
    };
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var tfValueTypeFindResponse =
        await httpPost("/test-framework-api/user/test-framework/tf-value-type/search", {"status": 1}, securityModel.authorization);
    if (tfValueTypeFindResponse.containsKey("body") && tfValueTypeFindResponse["body"] is String == false) {
      optionListMap["tfValueTypeId"] =
          tfValueTypeFindResponse["body"]["resultList"].map((tfValueType) => {"value": tfValueType["id"], "label": tfValueType["name"]}).toList();
    }
    formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "tfValueTypeId", "label": "Value type", "hintText": "Value type input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "nameLike", "label": "Name", "hintText": "Name input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "action", "actionName": "search", "actionLabel": "Search"}
          ]
        }
      ]
    };
  }

  tfValueTemplateSearch(tfValueTemplateSearchRequest) async {
    this.tfValueTemplateSearchRequest = tfValueTemplateSearchRequest;
    await tfValueTemplatePageChange(1);
  }

  tfValueTemplateActionHandler(actionName, formData) async {
    if (actionName == "search") {
      tfValueTemplateSearchRequest = formData;
      await tfValueTemplatePageChange(1);
    }
  }

  tfValueTemplatePageChange(page) async {
    if ((page - 1) * tfValueTemplateRowPerPage > tfValueTemplateRowCount) {
      page = (1.0 * tfValueTemplateRowCount / tfValueTemplateRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * tfValueTemplateRowPerPage, "queryLimit": tfValueTemplateRowPerPage};
    if (tfValueTemplateSearchRequest["nameLike"] != null && tfValueTemplateSearchRequest["nameLike"].isNotEmpty) {
      findRequest["nameLike"] = "%${tfValueTemplateSearchRequest["nameLike"]}%";
    }
    if (tfValueTemplateSearchRequest["tfValueTypeId"] != null && tfValueTemplateSearchRequest["tfValueTypeId"].isNotEmpty) {
      findRequest["tfValueTypeId"] = tfValueTemplateSearchRequest["tfValueTypeId"];
    }
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var response = await httpPost("/test-framework-api/user/test-framework/tf-value-template/search", findRequest, securityModel.authorization);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        tfValueTemplateCurrentPage = page;
        tfValueTemplateRowCount = response["body"]["rowCount"];
        tfValueTemplateResultList = response["body"]["resultList"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var tfValueTemplateStatusMap = {1: "Active", 0: "Inactive"};
    var resultColumns = [
      const DataColumn(
        label: Text(
          'Value',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Name',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Status',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      DataColumn(
          label: Row(
        children: [
          ElevatedButton(
            onPressed: () {
              Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/tf-value-template/create");
            },
            child: const Text('Add'),
          )
        ],
      )),
    ];
    var resultRows = [
      for (var tfValueTemplate in tfValueTemplateResultList)
        DataRow(
          cells: [
            DataCell(Text(tfValueTemplate["value"] ?? "")),
            DataCell(Text(tfValueTemplate["name"] ?? "")),
            DataCell(Text(tfValueTemplateStatusMap[tfValueTemplate["status"]] ?? "Not defined")),
            DataCell(Row(
              children: [
                ElevatedButton(
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/tf-value-template/edit/${tfValueTemplate["id"]}");
                  },
                  child: const Text('Edit'),
                ),
                ElevatedButton(
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.red)),
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/tf-value-template/delete/${tfValueTemplate["id"]}");
                  },
                  child: const Text('Delete'),
                ),
              ],
            )),
          ],
        )
    ];

    return Scaffold(
        appBar: AppBar(
          title: const Text("Value template list"),
          actions: [
            IconButton(
                onPressed: () {
                  Provider.of<SecurityModel>(context, listen: false).logout();
                },
                icon: const Icon(Icons.logout))
          ],
        ),
        drawer: const Drawer(child: MenuWidget()),
        body: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(mainAxisSize: MainAxisSize.min, children: [
                        DynamicForm(
                            formConfig: formConfig,
                            optionListMap: optionListMap,
                            formData: tfValueTemplateSearchRequest,
                            actionHandler: tfValueTemplateActionHandler),
                        Row(mainAxisSize: MainAxisSize.max, children: [
                          Expanded(
                              child: DynamicTable(
                            columns: resultColumns,
                            rows: resultRows,
                          ))
                        ]),
                        DynamicTablePagging(
                          tfValueTemplateRowCount,
                          tfValueTemplateCurrentPage,
                          tfValueTemplateRowPerPage,
                          pageChangeHandler: (page) {
                            tfValueTemplatePageChange(page);
                          },
                          rowPerPageChangeHandler: (rowPerPage) {
                            tfValueTemplateRowPerPage = rowPerPage ?? 10;
                            tfValueTemplatePageChange(1);
                          },
                        )
                      ])),
                )
              ],
            )));
  }
}

class AdminTfValueTemplateDetailWidget extends StatefulWidget {
  final String? tfValueTemplateId;
  final String tfValueTemplateAction;
  const AdminTfValueTemplateDetailWidget({required this.tfValueTemplateAction, this.tfValueTemplateId, Key? key}) : super(key: key);

  @override
  State<AdminTfValueTemplateDetailWidget> createState() => _AdminTfValueTemplateDetailState();
}

class _AdminTfValueTemplateDetailState extends ReloadableState<AdminTfValueTemplateDetailWidget> {
  dynamic formConfig = {
    "type": "column",
    "children": [
      {
        "type": "row",
        "children": [
          {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
        ]
      }
    ]
  };
  dynamic optionListMap = {};
  Map<dynamic, dynamic>? tfValueTemplate;
  TreeData? sysPermTreeData;
  String? error;
  var hasEditPermission = false;

  @override
  void initState() {
    super.initState();
    var userPermList = Provider.of<SecurityModel>(context, listen: false).userPermList;
    if (userPermList != null) {
      hasEditPermission = userPermList.where((permCode) => permCode == "ADMIN").isNotEmpty;
    }
    reload();
  }

  @override
  refreshData() {}

  reload() async {
    if (widget.tfValueTemplateId != null) {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      var tfValueTemplateGetResponse =
          await httpGet("/test-framework-api/user/test-framework/tf-value-template/${widget.tfValueTemplateId}", securityModel.authorization);
      if (tfValueTemplateGetResponse.containsKey("body") && tfValueTemplateGetResponse["body"] is String == false) {
        tfValueTemplate = tfValueTemplateGetResponse["body"]["result"];
      }
    } else {
      tfValueTemplate = {"status": 0};
    }
    await resetForm();
  }

  resetForm() async {
    var submitActionLabel = "Submit";
    var actionSubmitType = "primary";
    if (widget.tfValueTemplateAction == "delete") {
      submitActionLabel = "Delete";
      actionSubmitType = "danger";
    }
    optionListMap = {
      "status": [
        {"value": 1, "label": "Active"},
        {"value": 0, "label": "Inactive"}
      ],
      "tfValueTypeId": []
    };
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var tfValueTypeFindResponse =
        await httpPost("/test-framework-api/user/test-framework/tf-value-type/search", {"status": 1}, securityModel.authorization);
    if (tfValueTypeFindResponse.containsKey("body") && tfValueTypeFindResponse["body"] is String == false) {
      optionListMap["tfValueTypeId"] =
          tfValueTypeFindResponse["body"]["resultList"].map((tfValueType) => {"value": tfValueType["id"], "label": tfValueType["name"]}).toList();
    }
    formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "tfValueTypeId", "label": "Value type", "hintText": "Value type input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "name", "label": "Name", "hintText": "Name input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {
              "type": "inputText",
              "maxLines": 5,
              "expanded": true,
              "fieldName": "description",
              "label": "description",
              "hintText": "description input"
            }
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "status", "label": "status", "hintText": "status input"}
          ]
        },
        {
          "type": "row",
          "children": [
            hasEditPermission ? {"type": "action", "actionType": actionSubmitType, "actionName": "submit", "actionLabel": submitActionLabel} : {},
            {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
          ]
        }
      ]
    };
    setState(() {});
  }

  actionHandler(actionName, formData) async {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    if (actionName == "submit") {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      if (widget.tfValueTemplateAction == "create") {
        var tfValueTemplateCreateResponse =
            await httpPut("/test-framework-api/user/test-framework/tf-value-template", formData, securityModel.authorization);
        if (tfValueTemplateCreateResponse.containsKey("body") && tfValueTemplateCreateResponse["body"] is String == false) {
          var tfValueTemplateCreateResponseBody = tfValueTemplateCreateResponse["body"];
          if (tfValueTemplateCreateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (tfValueTemplateCreateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(tfValueTemplateCreateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.tfValueTemplateAction == "edit") {
        var tfValueTemplateUpdateResponse = await httpPost(
            "/test-framework-api/user/test-framework/tf-value-template/${widget.tfValueTemplateId}", formData, securityModel.authorization);
        if (tfValueTemplateUpdateResponse.containsKey("body") && tfValueTemplateUpdateResponse["body"] is String == false) {
          var tfValueTemplateUpdateResponseBody = tfValueTemplateUpdateResponse["body"];
          if (tfValueTemplateUpdateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (tfValueTemplateUpdateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(tfValueTemplateUpdateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.tfValueTemplateAction == "delete") {
        var tfValueTemplateDeleteResponse =
            await httpDelete("/test-framework-api/user/test-framework/tf-value-template/${widget.tfValueTemplateId}", securityModel.authorization);
        if (tfValueTemplateDeleteResponse.containsKey("body") && tfValueTemplateDeleteResponse["body"] is String == false) {
          var tfValueTemplateDeleteResponseBody = tfValueTemplateDeleteResponse["body"];
          if (tfValueTemplateDeleteResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (tfValueTemplateDeleteResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(tfValueTemplateDeleteResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
    }
    if (actionName == "back") {
      navigationModel.pop("/admin/system/tf-value-template");
    }
  }

  @override
  Widget build(BuildContext context) {
    if (error != null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Error'),
          ),
          body: Center(child: Text("Error $error")));
    }
    if (tfValueTemplate == null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Loading'),
          ),
          body: const Center(child: CircularProgressIndicator()));
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text('Value template detail'),
      ),
      drawer: const Drawer(child: MenuWidget()),
      body: Padding(
          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(mainAxisSize: MainAxisSize.min, children: [
                      Card(
                          child: Column(mainAxisSize: MainAxisSize.min, children: [
                        const Row(
                          children: [
                            Text(
                              "Detail",
                              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        formConfig != null && optionListMap != null
                            ? DynamicForm(
                                formConfig: formConfig, formData: tfValueTemplate!, optionListMap: optionListMap, actionHandler: actionHandler)
                            : Container()
                      ]))
                    ])),
              )
            ],
          )),
    );
  }
}
