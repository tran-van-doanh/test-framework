import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_admin/admin/system/menu.dart';
import 'package:test_framework_admin/api.dart';
import 'package:test_framework_admin/common/dynamic_form.dart';
import 'package:test_framework_admin/common/dynamic_table.dart';
import 'package:test_framework_admin/common/reloadable_state.dart';
import 'package:test_framework_admin/config.dart';
import 'package:test_framework_admin/model/model.dart';
import 'package:http/http.dart' as http;

import '../../common/type.dart';

class AdminDocFileListWidget extends StatefulWidget {
  const AdminDocFileListWidget({Key? key}) : super(key: key);

  @override
  State<AdminDocFileListWidget> createState() => _AdminDocFileListState();
}

class _AdminDocFileListState extends ReloadableState<AdminDocFileListWidget> {
  var docFileSearchRequest = {};
  var docFileRowCount = 0;
  var docFileCurrentPage = 1;
  var docFileResultList = [];
  var docFileRowPerPage = 25;
  Widget paging = Container();

  @override
  void initState() {
    super.initState();
    docFileSearch({});
  }

  @override
  refreshData() {
    docFilePageChange(docFileCurrentPage);
  }

  docFileSearch(docFileSearchRequest) async {
    this.docFileSearchRequest = docFileSearchRequest;
    await docFilePageChange(1);
  }

  docFileActionHandler(actionName, formData) async {
    if (actionName == "search") {
      docFileSearchRequest = formData;
      await docFilePageChange(1);
    }
  }

  docFilePageChange(page) async {
    if ((page - 1) * docFileRowPerPage > docFileRowCount) {
      page = (1.0 * docFileRowCount / docFileRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * docFileRowPerPage, "queryLimit": docFileRowPerPage};
    if (docFileSearchRequest["nameLike"] != null && docFileSearchRequest["nameLike"].isNotEmpty) {
      findRequest["nameLike"] = "%${docFileSearchRequest["nameLike"]}%";
    }
    if (docFileSearchRequest["titleLike"] != null && docFileSearchRequest["titleLike"].isNotEmpty) {
      findRequest["titleLike"] = "%${docFileSearchRequest["titleLike"]}%";
    }
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var response = await httpPost("/test-framework-api/admin/doc/doc-file/search", findRequest, securityModel.authorization);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        docFileCurrentPage = page;
        docFileRowCount = response["body"]["rowCount"];
        docFileResultList = response["body"]["resultList"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var docFileStatusMap = {1: "Active", 0: "Inactive"};
    var optionListMap = {};
    const formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "nameLike", "label": "Name", "hintText": "Name input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "titleLike", "label": "Title", "hintText": "Title input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "action", "actionName": "search", "actionLabel": "Search"}
          ]
        }
      ]
    };
    var resultColumns = [
      const DataColumn(
        label: Text(
          'Name',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Title',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Description',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'File name',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Status',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      DataColumn(
          label: Row(
        children: [
          ElevatedButton(
            onPressed: () {
              Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/doc-file/create");
            },
            child: const Text('Add'),
          )
        ],
      )),
    ];
    var resultRows = [
      for (var docFile in docFileResultList)
        DataRow(
          cells: [
            DataCell(Text(docFile["name"] ?? "")),
            DataCell(Text(docFile["title"] ?? "")),
            DataCell(Text(docFile["description"] ?? "")),
            DataCell(Text(docFile["fileName"] ?? "")),
            DataCell(Text(docFileStatusMap[docFile["status"]] ?? "Not defined")),
            DataCell(Row(
              children: [
                ElevatedButton(
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/doc-file/edit/${docFile["id"]}");
                  },
                  child: const Text('Edit'),
                ),
                ElevatedButton(
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.red)),
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/doc-file/delete/${docFile["id"]}");
                  },
                  child: const Text('Delete'),
                ),
              ],
            )),
          ],
        )
    ];

    return Scaffold(
        appBar: AppBar(
          title: const Text("File document list"),
          actions: [
            IconButton(
                onPressed: () {
                  Provider.of<SecurityModel>(context, listen: false).logout();
                },
                icon: const Icon(Icons.logout))
          ],
        ),
        drawer: const Drawer(child: MenuWidget()),
        body: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(mainAxisSize: MainAxisSize.min, children: [
                        DynamicForm(
                            formConfig: formConfig,
                            optionListMap: optionListMap,
                            formData: docFileSearchRequest,
                            actionHandler: docFileActionHandler),
                        Row(mainAxisSize: MainAxisSize.max, children: [
                          Expanded(
                              child: DynamicTable(
                            columns: resultColumns,
                            rows: resultRows,
                          ))
                        ]),
                        DynamicTablePagging(
                          docFileRowCount,
                          docFileCurrentPage,
                          docFileRowPerPage,
                          pageChangeHandler: (page) {
                            docFilePageChange(page);
                          },
                          rowPerPageChangeHandler: (rowPerPage) {
                            docFileRowPerPage = rowPerPage ?? 10;
                            docFilePageChange(1);
                          },
                        )
                      ])),
                )
              ],
            )));
  }
}

class AdminDocFileDetailWidget extends StatefulWidget {
  final String? docFileId;
  final String docFileAction;
  const AdminDocFileDetailWidget({required this.docFileAction, this.docFileId, Key? key}) : super(key: key);

  @override
  State<AdminDocFileDetailWidget> createState() => _AdminDocFileDetailState();
}

class _AdminDocFileDetailState extends ReloadableState<AdminDocFileDetailWidget> {
  dynamic formConfig = {
    "type": "column",
    "children": [
      {
        "type": "row",
        "children": [
          {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
        ]
      }
    ]
  };
  dynamic optionListMap = {};
  Map<dynamic, dynamic>? docFile;
  String? error;
  var hasEditPermission = false;
  String? fileName;
  PlatformFile? objFile;

  @override
  void initState() {
    super.initState();
    var userPermList = Provider.of<SecurityModel>(context, listen: false).userPermList;
    if (userPermList != null) {
      hasEditPermission = userPermList.where((permCode) => permCode == "ADMIN").isNotEmpty;
    }
    reload();
  }

  @override
  refreshData() {}

  reload() async {
    if (widget.docFileId != null) {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      var docFileGetResponse = await httpGet("/test-framework-api/admin/doc/doc-file/${widget.docFileId}", securityModel.authorization);
      if (docFileGetResponse.containsKey("body") && docFileGetResponse["body"] is String == false) {
        docFile = docFileGetResponse["body"]["result"];
        fileName = docFileGetResponse["body"]["result"]["fileName"];
      }
    } else {
      docFile = {"status": 0};
    }
    await resetForm();
  }

  resetForm() async {
    var submitActionLabel = "Submit";
    var actionSubmitType = "primary";
    if (widget.docFileAction == "delete") {
      submitActionLabel = "Delete";
      actionSubmitType = "danger";
    }
    optionListMap = {
      "status": [
        {"value": 1, "label": "Active"},
        {"value": 0, "label": "Inactive"}
      ]
    };
    formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "name", "label": "name", "hintText": "Name input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "title", "label": "Title", "hintText": "Title input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {
              "type": "inputText",
              "maxLines": 5,
              "expanded": true,
              "fieldName": "description",
              "label": "description",
              "hintText": "description input"
            }
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "status", "label": "status", "hintText": "status input"}
          ]
        },
        {
          "type": "row",
          "children": [
            hasEditPermission ? {"type": "action", "actionType": actionSubmitType, "actionName": "submit", "actionLabel": submitActionLabel} : {},
            {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
          ]
        }
      ]
    };
    setState(() {});
  }

  actionHandler(actionName, formData) async {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    if (actionName == "submit") {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      if (widget.docFileAction == "create") {
        var docFileCreateResponse = await httpPut("/test-framework-api/admin/doc/doc-file", formData, securityModel.authorization);
        if (docFileCreateResponse.containsKey("body") && docFileCreateResponse["body"] is String == false) {
          var docFileCreateResponseBody = docFileCreateResponse["body"];
          if (docFileCreateResponseBody.containsKey("result")) {
            await uploadFile(docFileCreateResponse["body"]["result"], objFile?.bytes, objFile?.name);
            actionName = "back";
          } else if (docFileCreateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(docFileCreateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.docFileAction == "edit") {
        var docFileUpdateResponse =
            await httpPost("/test-framework-api/admin/doc/doc-file/${widget.docFileId}", formData, securityModel.authorization);
        if (docFileUpdateResponse.containsKey("body") && docFileUpdateResponse["body"] is String == false) {
          var docFileUpdateResponseBody = docFileUpdateResponse["body"];
          if (docFileUpdateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (docFileUpdateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(docFileUpdateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.docFileAction == "delete") {
        var docFileDeleteResponse = await httpDelete("/test-framework-api/admin/doc/doc-file/${widget.docFileId}", securityModel.authorization);
        if (docFileDeleteResponse.containsKey("body") && docFileDeleteResponse["body"] is String == false) {
          var docFileDeleteResponseBody = docFileDeleteResponse["body"];
          if (docFileDeleteResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (docFileDeleteResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(docFileDeleteResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
    }
    if (actionName == "back") {
      navigationModel.pop("/admin/system/doc-file");
    }
  }

  uploadFile(String fileId, value, filename) async {
    final request = http.MultipartRequest(
      "POST",
      Uri.parse(
        "$baseUrl/test-framework-api/admin/doc-file/doc-file/$fileId",
      ),
    );
    request.headers.addAll({"Authorization": Provider.of<SecurityModel>(context, listen: false).authorization!});
    request.files.add(http.MultipartFile.fromBytes('file', value, filename: filename));
    await request.send();
  }

  @override
  Widget build(BuildContext context) {
    if (error != null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Error'),
          ),
          body: Center(child: Text("Error $error")));
    }
    if (docFile == null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Loading'),
          ),
          body: const Center(child: CircularProgressIndicator()));
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text('File document detail'),
      ),
      drawer: const Drawer(child: MenuWidget()),
      body: Padding(
          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(mainAxisSize: MainAxisSize.min, children: [
                      Card(
                          child: Column(mainAxisSize: MainAxisSize.min, children: [
                        const Row(
                          children: [
                            Text(
                              "Detail",
                              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            if (fileName != null) Text(fileName!),
                            FloatingActionButton.extended(
                              heroTag: null,
                              onPressed: () async {
                                objFile = await selectFile(['xlsx']);
                                if (objFile != null) {
                                  setState(() {
                                    fileName = objFile!.name;
                                  });
                                }
                              },
                              icon: const Icon(Icons.cloud_upload, color: Colors.white),
                              label: const Text(
                                "Choose file",
                                style: TextStyle(fontSize: 13, fontWeight: FontWeight.w600, color: Colors.white),
                              ),
                              backgroundColor: Colors.redAccent,
                              elevation: 0,
                              hoverElevation: 0,
                            ),
                          ],
                        ),
                        if (widget.docFileAction == "edit")
                          Padding(
                            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                            child: TextField(
                              controller: TextEditingController(
                                  text: "/test-framework-api/guest/doc-file/doc-file/${docFile!["id"]}/${docFile!["fileName"]}"),
                              decoration: const InputDecoration(
                                label: Text("Link file"),
                                border: OutlineInputBorder(),
                              ),
                            ),
                          ),
                        formConfig != null && optionListMap != null
                            ? DynamicForm(formConfig: formConfig, formData: docFile!, optionListMap: optionListMap, actionHandler: actionHandler)
                            : Container()
                      ]))
                    ])),
              )
            ],
          )),
    );
  }
}
