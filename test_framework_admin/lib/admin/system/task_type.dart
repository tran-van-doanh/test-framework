import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_admin/admin/system/menu.dart';
import 'package:test_framework_admin/api.dart';
import 'package:test_framework_admin/common/dynamic_form.dart';
import 'package:test_framework_admin/common/dynamic_table.dart';
import 'package:test_framework_admin/common/reloadable_state.dart';
import 'package:test_framework_admin/common/tree.dart';
import 'package:test_framework_admin/model/model.dart';

class AdminTaskTypeListWidget extends StatefulWidget {
  const AdminTaskTypeListWidget({Key? key}) : super(key: key);

  @override
  State<AdminTaskTypeListWidget> createState() => _AdminTaskTypeListState();
}

class _AdminTaskTypeListState extends ReloadableState<AdminTaskTypeListWidget> {
  var taskTypeSearchRequest = {};
  var taskTypeRowCount = 0;
  var taskTypeCurrentPage = 1;
  var taskTypeResultList = [];
  var taskTypeRowPerPage = 25;
  Widget paging = Container();

  @override
  void initState() {
    super.initState();
    taskTypeSearch({});
  }

  @override
  refreshData() {
    taskTypePageChange(taskTypeCurrentPage);
  }

  taskTypeSearch(taskTypeSearchRequest) async {
    this.taskTypeSearchRequest = taskTypeSearchRequest;
    await taskTypePageChange(1);
  }

  taskTypeActionHandler(actionName, formData) async {
    if (actionName == "search") {
      taskTypeSearchRequest = formData;
      await taskTypePageChange(1);
    }
  }

  taskTypePageChange(page) async {
    if ((page - 1) * taskTypeRowPerPage > taskTypeRowCount) {
      page = (1.0 * taskTypeRowCount / taskTypeRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * taskTypeRowPerPage, "queryLimit": taskTypeRowPerPage};
    if (taskTypeSearchRequest["nameLike"] != null && taskTypeSearchRequest["nameLike"].isNotEmpty) {
      findRequest["nameLike"] = "%${taskTypeSearchRequest["nameLike"]}%";
    }
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var response = await httpPost("/test-framework-api/admin/project/prj-task-type/search", findRequest, securityModel.authorization);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        taskTypeCurrentPage = page;
        taskTypeRowCount = response["body"]["rowCount"];
        taskTypeResultList = response["body"]["resultList"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var taskTypeStatusMap = {1: "Active", 0: "Inactive"};
    var optionListMap = {};
    const formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "nameLike", "label": "Name", "hintText": "Name input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "action", "actionName": "search", "actionLabel": "Search"}
          ]
        }
      ]
    };
    var resultColumns = [
      const DataColumn(
        label: Text(
          'Name',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Status',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      DataColumn(
          label: Row(
        children: [
          ElevatedButton(
            onPressed: () {
              Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/task-type/create");
            },
            child: const Text('Add'),
          )
        ],
      )),
    ];
    var resultRows = [
      for (var taskType in taskTypeResultList)
        DataRow(
          cells: [
            DataCell(Text(taskType["name"] ?? "")),
            DataCell(Text(taskTypeStatusMap[taskType["status"]] ?? "Not defined")),
            DataCell(Row(
              children: [
                ElevatedButton(
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/task-type/edit/${taskType["id"]}");
                  },
                  child: const Text('Edit'),
                ),
                ElevatedButton(
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.red)),
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/task-type/delete/${taskType["id"]}");
                  },
                  child: const Text('Delete'),
                ),
              ],
            )),
          ],
        )
    ];

    return Scaffold(
        appBar: AppBar(
          title: const Text("Task type list"),
          actions: [
            IconButton(
                onPressed: () {
                  Provider.of<SecurityModel>(context, listen: false).logout();
                },
                icon: const Icon(Icons.logout))
          ],
        ),
        drawer: const Drawer(child: MenuWidget()),
        body: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(mainAxisSize: MainAxisSize.min, children: [
                        DynamicForm(
                            formConfig: formConfig,
                            optionListMap: optionListMap,
                            formData: taskTypeSearchRequest,
                            actionHandler: taskTypeActionHandler),
                        Row(mainAxisSize: MainAxisSize.max, children: [
                          Expanded(
                              child: DynamicTable(
                            columns: resultColumns,
                            rows: resultRows,
                          ))
                        ]),
                        DynamicTablePagging(
                          taskTypeRowCount,
                          taskTypeCurrentPage,
                          taskTypeRowPerPage,
                          pageChangeHandler: (page) {
                            taskTypePageChange(page);
                          },
                          rowPerPageChangeHandler: (rowPerPage) {
                            taskTypeRowPerPage = rowPerPage ?? 10;
                            taskTypePageChange(1);
                          },
                        )
                      ])),
                )
              ],
            )));
  }
}

class AdminTaskTypeDetailWidget extends StatefulWidget {
  final String? taskTypeId;
  final String taskTypeAction;
  const AdminTaskTypeDetailWidget({required this.taskTypeAction, this.taskTypeId, Key? key}) : super(key: key);

  @override
  State<AdminTaskTypeDetailWidget> createState() => _AdminTaskTypeDetailState();
}

class _AdminTaskTypeDetailState extends ReloadableState<AdminTaskTypeDetailWidget> {
  dynamic formConfig = {
    "type": "column",
    "children": [
      {
        "type": "row",
        "children": [
          {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
        ]
      }
    ]
  };
  dynamic optionListMap = {};
  Map<dynamic, dynamic>? taskType;
  TreeData? sysPermTreeData;
  String? error;
  var hasEditPermission = false;

  @override
  void initState() {
    super.initState();
    var userPermList = Provider.of<SecurityModel>(context, listen: false).userPermList;
    if (userPermList != null) {
      hasEditPermission = userPermList.where((permCode) => permCode == "ADMIN").isNotEmpty;
    }
    reload();
  }

  @override
  refreshData() {}

  reload() async {
    if (widget.taskTypeId != null) {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      var taskTypeGetResponse = await httpGet("/test-framework-api/admin/project/prj-task-type/${widget.taskTypeId}", securityModel.authorization);
      if (taskTypeGetResponse.containsKey("body") && taskTypeGetResponse["body"] is String == false) {
        taskType = taskTypeGetResponse["body"]["result"];
      }
    } else {
      taskType = {"status": 0};
    }
    await resetForm();
  }

  resetForm() async {
    var submitActionLabel = "Submit";
    var actionSubmitType = "primary";
    if (widget.taskTypeAction == "delete") {
      submitActionLabel = "Delete";
      actionSubmitType = "danger";
    }
    optionListMap = {
      "status": [
        {"value": 1, "label": "Active"},
        {"value": 0, "label": "Inactive"}
      ],
    };
    formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "name", "label": "Name", "hintText": "Name input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "title", "label": "Title", "hintText": "Title input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {
              "type": "inputText",
              "maxLines": 5,
              "expanded": true,
              "fieldName": "description",
              "label": "description",
              "hintText": "description input"
            }
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "status", "label": "status", "hintText": "status input"}
          ]
        },
        {
          "type": "row",
          "children": [
            hasEditPermission ? {"type": "action", "actionType": actionSubmitType, "actionName": "submit", "actionLabel": submitActionLabel} : {},
            {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
          ]
        }
      ]
    };
    setState(() {});
  }

  actionHandler(actionName, formData) async {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    if (actionName == "submit") {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      if (widget.taskTypeAction == "create") {
        var taskTypeCreateResponse = await httpPut("/test-framework-api/admin/project/prj-task-type", formData, securityModel.authorization);
        if (taskTypeCreateResponse.containsKey("body") && taskTypeCreateResponse["body"] is String == false) {
          var taskTypeCreateResponseBody = taskTypeCreateResponse["body"];
          if (taskTypeCreateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (taskTypeCreateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(taskTypeCreateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.taskTypeAction == "edit") {
        var taskTypeUpdateResponse =
            await httpPost("/test-framework-api/admin/project/prj-task-type/${widget.taskTypeId}", formData, securityModel.authorization);
        if (taskTypeUpdateResponse.containsKey("body") && taskTypeUpdateResponse["body"] is String == false) {
          var taskTypeUpdateResponseBody = taskTypeUpdateResponse["body"];
          if (taskTypeUpdateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (taskTypeUpdateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(taskTypeUpdateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.taskTypeAction == "delete") {
        var taskTypeDeleteResponse =
            await httpDelete("/test-framework-api/admin/project/prj-task-type/${widget.taskTypeId}", securityModel.authorization);
        if (taskTypeDeleteResponse.containsKey("body") && taskTypeDeleteResponse["body"] is String == false) {
          var taskTypeDeleteResponseBody = taskTypeDeleteResponse["body"];
          if (taskTypeDeleteResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (taskTypeDeleteResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(taskTypeDeleteResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
    }
    if (actionName == "back") {
      navigationModel.pop("/admin/system/task-type");
    }
  }

  @override
  Widget build(BuildContext context) {
    if (error != null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Error'),
          ),
          body: Center(child: Text("Error $error")));
    }
    if (taskType == null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Loading'),
          ),
          body: const Center(child: CircularProgressIndicator()));
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text('Task type detail'),
      ),
      drawer: const Drawer(child: MenuWidget()),
      body: Padding(
          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(mainAxisSize: MainAxisSize.min, children: [
                      Card(
                          child: Column(mainAxisSize: MainAxisSize.min, children: [
                        const Row(
                          children: [
                            Text(
                              "Detail",
                              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        formConfig != null && optionListMap != null
                            ? DynamicForm(formConfig: formConfig, formData: taskType!, optionListMap: optionListMap, actionHandler: actionHandler)
                            : Container()
                      ]))
                    ])),
              )
            ],
          )),
    );
  }
}
