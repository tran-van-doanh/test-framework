import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_admin/admin/system/menu.dart';
import 'package:test_framework_admin/api.dart';
import 'package:test_framework_admin/common/dynamic_form.dart';
import 'package:test_framework_admin/common/dynamic_table.dart';
import 'package:test_framework_admin/common/reloadable_state.dart';
import 'package:test_framework_admin/common/tree.dart';
import 'package:test_framework_admin/model/model.dart';

class AdminDocInstructionManualsListWidget extends StatefulWidget {
  const AdminDocInstructionManualsListWidget({Key? key}) : super(key: key);

  @override
  State<AdminDocInstructionManualsListWidget> createState() => _AdminDocInstructionManualsListState();
}

class _AdminDocInstructionManualsListState extends ReloadableState<AdminDocInstructionManualsListWidget> {
  var docInstructionManualsSearchRequest = {};
  var docInstructionManualsRowCount = 0;
  var docInstructionManualsCurrentPage = 1;
  var docInstructionManualsResultList = [];
  var docInstructionManualsRowPerPage = 25;
  Widget paging = Container();

  @override
  void initState() {
    super.initState();
    docInstructionManualsSearch({});
  }

  @override
  refreshData() {
    docInstructionManualsPageChange(docInstructionManualsCurrentPage);
  }

  docInstructionManualsSearch(docInstructionManualsSearchRequest) async {
    this.docInstructionManualsSearchRequest = docInstructionManualsSearchRequest;
    await docInstructionManualsPageChange(1);
  }

  docInstructionManualsActionHandler(actionName, formData) async {
    if (actionName == "search") {
      docInstructionManualsSearchRequest = formData;
      await docInstructionManualsPageChange(1);
    }
  }

  docInstructionManualsPageChange(page) async {
    if ((page - 1) * docInstructionManualsRowPerPage > docInstructionManualsRowCount) {
      page = (1.0 * docInstructionManualsRowCount / docInstructionManualsRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * docInstructionManualsRowPerPage, "queryLimit": docInstructionManualsRowPerPage};
    if (docInstructionManualsSearchRequest["fieldNameLike"] != null && docInstructionManualsSearchRequest["fieldNameLike"].isNotEmpty) {
      findRequest["fieldNameLike"] = "%${docInstructionManualsSearchRequest["fieldNameLike"]}%";
    }
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var response = await httpPost("/test-framework-api/admin/doc/doc-instruction-manuals/search", findRequest, securityModel.authorization);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        docInstructionManualsCurrentPage = page;
        docInstructionManualsRowCount = response["body"]["rowCount"];
        docInstructionManualsResultList = response["body"]["resultList"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var docInstructionManualsStatusMap = {1: "Active", 0: "Inactive"};
    var optionListMap = {};
    const formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "fieldNameLike", "label": "Name", "hintText": "Name input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "action", "actionName": "search", "actionLabel": "Search"}
          ]
        }
      ]
    };
    var resultColumns = [
      const DataColumn(
        label: Text(
          'Type',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Name',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Description',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Language',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Status',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      DataColumn(
          label: Row(
        children: [
          ElevatedButton(
            onPressed: () {
              Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/doc-instruction-manuals/create");
            },
            child: const Text('Add'),
          )
        ],
      )),
    ];
    var resultRows = [
      for (var docInstructionManuals in docInstructionManualsResultList)
        DataRow(
          cells: [
            DataCell(Text(docInstructionManuals["instructionManualsType"] ?? "")),
            DataCell(Text(docInstructionManuals["fieldName"] ?? "")),
            DataCell(Text(docInstructionManuals["description"] ?? "")),
            DataCell(Text(docInstructionManuals["language"] ?? "")),
            DataCell(Text(docInstructionManualsStatusMap[docInstructionManuals["status"]] ?? "Not defined")),
            DataCell(Row(
              children: [
                ElevatedButton(
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false)
                        .navigate("/admin/system/doc-instruction-manuals/edit/${docInstructionManuals["id"]}");
                  },
                  child: const Text('Edit'),
                ),
                ElevatedButton(
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.red)),
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false)
                        .navigate("/admin/system/doc-instruction-manuals/delete/${docInstructionManuals["id"]}");
                  },
                  child: const Text('Delete'),
                ),
              ],
            )),
          ],
        )
    ];

    return Scaffold(
        appBar: AppBar(
          title: const Text("Doc Instruction Manuals list"),
          actions: [
            IconButton(
                onPressed: () {
                  Provider.of<SecurityModel>(context, listen: false).logout();
                },
                icon: const Icon(Icons.logout))
          ],
        ),
        drawer: const Drawer(child: MenuWidget()),
        body: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(mainAxisSize: MainAxisSize.min, children: [
                        DynamicForm(
                            formConfig: formConfig,
                            optionListMap: optionListMap,
                            formData: docInstructionManualsSearchRequest,
                            actionHandler: docInstructionManualsActionHandler),
                        Row(mainAxisSize: MainAxisSize.max, children: [
                          Expanded(
                              child: DynamicTable(
                            columns: resultColumns,
                            rows: resultRows,
                          ))
                        ]),
                        DynamicTablePagging(
                          docInstructionManualsRowCount,
                          docInstructionManualsCurrentPage,
                          docInstructionManualsRowPerPage,
                          pageChangeHandler: (page) {
                            docInstructionManualsPageChange(page);
                          },
                          rowPerPageChangeHandler: (rowPerPage) {
                            docInstructionManualsRowPerPage = rowPerPage ?? 10;
                            docInstructionManualsPageChange(1);
                          },
                        )
                      ])),
                )
              ],
            )));
  }
}

class AdminDocInstructionManualsDetailWidget extends StatefulWidget {
  final String? docInstructionManualsId;
  final String docInstructionManualsAction;
  const AdminDocInstructionManualsDetailWidget({required this.docInstructionManualsAction, this.docInstructionManualsId, Key? key}) : super(key: key);

  @override
  State<AdminDocInstructionManualsDetailWidget> createState() => _AdminDocInstructionManualsDetailState();
}

class _AdminDocInstructionManualsDetailState extends ReloadableState<AdminDocInstructionManualsDetailWidget> {
  dynamic formConfig = {
    "type": "column",
    "children": [
      {
        "type": "row",
        "children": [
          {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
        ]
      }
    ]
  };
  dynamic optionListMap = {};
  Map<dynamic, dynamic>? docInstructionManuals;
  TreeData? sysPermTreeData;
  String? error;
  var hasEditPermission = false;

  @override
  void initState() {
    super.initState();
    var userPermList = Provider.of<SecurityModel>(context, listen: false).userPermList;
    if (userPermList != null) {
      hasEditPermission = userPermList.where((permCode) => permCode == "ADMIN").isNotEmpty;
    }
    reload();
  }

  @override
  refreshData() {}

  reload() async {
    if (widget.docInstructionManualsId != null) {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      var docInstructionManualsGetResponse =
          await httpGet("/test-framework-api/admin/doc/doc-instruction-manuals/${widget.docInstructionManualsId}", securityModel.authorization);
      if (docInstructionManualsGetResponse.containsKey("body") && docInstructionManualsGetResponse["body"] is String == false) {
        docInstructionManuals = docInstructionManualsGetResponse["body"]["result"];
      }
    } else {
      docInstructionManuals = {"status": 0};
    }
    await resetForm();
  }

  resetForm() async {
    var submitActionLabel = "Submit";
    var actionSubmitType = "primary";
    if (widget.docInstructionManualsAction == "delete") {
      submitActionLabel = "Delete";
      actionSubmitType = "danger";
    }
    optionListMap = {
      "status": [
        {"value": 1, "label": "Active"},
        {"value": 0, "label": "Inactive"}
      ],
      "fieldRequired": [
        {"value": true, "label": "Required"},
        {"value": false, "label": "Not Required"},
      ],
    };
    formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "instructionManualsType", "label": "Type", "hintText": "Type input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "language", "label": "Language", "hintText": "Language input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "fieldName", "label": "Name", "hintText": "Name input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "fieldRequired", "label": "Required", "hintText": "Required"}
          ]
        },
        {
          "type": "row",
          "children": [
            {
              "type": "inputText",
              "expanded": true,
              "fieldName": "description",
              "label": "Description",
              "hintText": "Description input",
              "maxLines": 3
            }
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "fieldIndex", "label": "Index", "hintText": "Index input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "status", "label": "Status", "hintText": "Status input"}
          ]
        },
        {
          "type": "row",
          "children": [
            hasEditPermission ? {"type": "action", "actionType": actionSubmitType, "actionName": "submit", "actionLabel": submitActionLabel} : {},
            {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
          ]
        }
      ]
    };
    setState(() {});
  }

  actionHandler(actionName, formData) async {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    if (actionName == "submit") {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      if (widget.docInstructionManualsAction == "create") {
        var docInstructionManualsCreateResponse =
            await httpPut("/test-framework-api/admin/doc/doc-instruction-manuals", formData, securityModel.authorization);
        if (docInstructionManualsCreateResponse.containsKey("body") && docInstructionManualsCreateResponse["body"] is String == false) {
          var docInstructionManualsCreateResponseBody = docInstructionManualsCreateResponse["body"];
          if (docInstructionManualsCreateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (docInstructionManualsCreateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(docInstructionManualsCreateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.docInstructionManualsAction == "edit") {
        var docInstructionManualsUpdateResponse = await httpPost(
            "/test-framework-api/admin/doc/doc-instruction-manuals/${widget.docInstructionManualsId}", formData, securityModel.authorization);
        if (docInstructionManualsUpdateResponse.containsKey("body") && docInstructionManualsUpdateResponse["body"] is String == false) {
          var docInstructionManualsUpdateResponseBody = docInstructionManualsUpdateResponse["body"];
          if (docInstructionManualsUpdateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (docInstructionManualsUpdateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(docInstructionManualsUpdateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.docInstructionManualsAction == "delete") {
        var docInstructionManualsDeleteResponse =
            await httpDelete("/test-framework-api/admin/doc/doc-instruction-manuals/${widget.docInstructionManualsId}", securityModel.authorization);
        if (docInstructionManualsDeleteResponse.containsKey("body") && docInstructionManualsDeleteResponse["body"] is String == false) {
          var docInstructionManualsDeleteResponseBody = docInstructionManualsDeleteResponse["body"];
          if (docInstructionManualsDeleteResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (docInstructionManualsDeleteResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(docInstructionManualsDeleteResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
    }
    if (actionName == "back") {
      navigationModel.pop("/admin/system/doc-instruction-manuals");
    }
  }

  @override
  Widget build(BuildContext context) {
    if (error != null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Error'),
          ),
          body: Center(child: Text("Error $error")));
    }
    if (docInstructionManuals == null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Loading'),
          ),
          body: const Center(child: CircularProgressIndicator()));
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text('Doc Instruction Manuals detail'),
      ),
      drawer: const Drawer(child: MenuWidget()),
      body: Padding(
          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(mainAxisSize: MainAxisSize.min, children: [
                      Card(
                          child: Column(mainAxisSize: MainAxisSize.min, children: [
                        const Row(
                          children: [
                            Text(
                              "Detail",
                              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        formConfig != null && optionListMap != null
                            ? DynamicForm(
                                formConfig: formConfig, formData: docInstructionManuals!, optionListMap: optionListMap, actionHandler: actionHandler)
                            : Container()
                      ]))
                    ])),
              )
            ],
          )),
    );
  }
}
