import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_admin/admin/system/menu.dart';
import 'package:test_framework_admin/api.dart';
import 'package:test_framework_admin/common/dynamic_form.dart';
import 'package:test_framework_admin/common/dynamic_table.dart';
import 'package:test_framework_admin/common/reloadable_state.dart';
import 'package:test_framework_admin/common/tree.dart';
import 'package:test_framework_admin/model/model.dart';

class AdminSysMultiLanguageListWidget extends StatefulWidget {
  const AdminSysMultiLanguageListWidget({Key? key}) : super(key: key);

  @override
  State<AdminSysMultiLanguageListWidget> createState() => _AdminSysMultiLanguageListState();
}

class _AdminSysMultiLanguageListState extends ReloadableState<AdminSysMultiLanguageListWidget> {
  var sysMultiLanguageSearchRequest = {};
  var sysMultiLanguageRowCount = 0;
  var sysMultiLanguageCurrentPage = 1;
  var sysMultiLanguageResultList = [];
  var sysMultiLanguageRowPerPage = 25;
  Widget paging = Container();

  @override
  void initState() {
    super.initState();
    sysMultiLanguageSearch({});
  }

  @override
  refreshData() {
    sysMultiLanguagePageChange(sysMultiLanguageCurrentPage);
  }

  sysMultiLanguageSearch(sysMultiLanguageSearchRequest) async {
    this.sysMultiLanguageSearchRequest = sysMultiLanguageSearchRequest;
    await sysMultiLanguagePageChange(1);
  }

  sysMultiLanguageActionHandler(actionName, formData) async {
    if (actionName == "search") {
      sysMultiLanguageSearchRequest = formData;
      await sysMultiLanguagePageChange(1);
    }
  }

  sysMultiLanguagePageChange(page) async {
    if ((page - 1) * sysMultiLanguageRowPerPage > sysMultiLanguageRowCount) {
      page = (1.0 * sysMultiLanguageRowCount / sysMultiLanguageRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * sysMultiLanguageRowPerPage, "queryLimit": sysMultiLanguageRowPerPage};
    if (sysMultiLanguageSearchRequest["nameLike"] != null && sysMultiLanguageSearchRequest["nameLike"].isNotEmpty) {
      findRequest["nameLike"] = "%${sysMultiLanguageSearchRequest["nameLike"]}%";
    }
    if (sysMultiLanguageSearchRequest["valueEnLike"] != null && sysMultiLanguageSearchRequest["valueEnLike"].isNotEmpty) {
      findRequest["valueEnLike"] = "%${sysMultiLanguageSearchRequest["valueEnLike"]}%";
    }
    if (sysMultiLanguageSearchRequest["valueViLike"] != null && sysMultiLanguageSearchRequest["valueViLike"].isNotEmpty) {
      findRequest["valueViLike"] = "%${sysMultiLanguageSearchRequest["valueViLike"]}%";
    }
    if (sysMultiLanguageSearchRequest["valueJpLike"] != null && sysMultiLanguageSearchRequest["valueJpLike"].isNotEmpty) {
      findRequest["valueJpLike"] = "%${sysMultiLanguageSearchRequest["valueJpLike"]}%";
    }
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var response = await httpPost("/test-framework-api/admin/sys/sys-multilingual/search", findRequest, securityModel.authorization);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        sysMultiLanguageCurrentPage = page;
        sysMultiLanguageRowCount = response["body"]["rowCount"];
        sysMultiLanguageResultList = response["body"]["resultList"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var sysMultiLanguageStatusMap = {1: "Active", 0: "Inactive"};
    var optionListMap = {};
    const formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "nameLike", "label": "Name", "hintText": "Name input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "valueEnLike", "label": "English", "hintText": "English input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "valueViLike", "label": "Vietnamese", "hintText": "Vietnamese input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "valueJpLike", "label": "Japanese", "hintText": "Japanese input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "action", "actionName": "search", "actionLabel": "Search"}
          ]
        }
      ]
    };
    var resultColumns = [
      const DataColumn(
        label: Text(
          'Name',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Default value',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'En',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Vi',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Jp',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Status',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      DataColumn(
          label: Row(
        children: [
          ElevatedButton(
            onPressed: () {
              Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-multi-language/create");
            },
            child: const Text('Add'),
          )
        ],
      )),
    ];
    var resultRows = [
      for (var sysMultiLanguage in sysMultiLanguageResultList)
        DataRow(
          cells: [
            DataCell(Text(sysMultiLanguage["name"] ?? "")),
            DataCell(Text(sysMultiLanguage["defaultValue"] ?? "")),
            DataCell(Text(sysMultiLanguage["valueEn"] ?? "")),
            DataCell(Text(sysMultiLanguage["valueVi"] ?? "")),
            DataCell(Text(sysMultiLanguage["valueJp"] ?? "")),
            DataCell(Text(sysMultiLanguageStatusMap[sysMultiLanguage["status"]] ?? "Not defined")),
            DataCell(Row(
              children: [
                ElevatedButton(
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-multi-language/edit/${sysMultiLanguage["id"]}");
                  },
                  child: const Text('Edit'),
                ),
                ElevatedButton(
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.red)),
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false)
                        .navigate("/admin/system/sys-multi-language/delete/${sysMultiLanguage["id"]}");
                  },
                  child: const Text('Delete'),
                ),
              ],
            )),
          ],
        )
    ];

    return Scaffold(
        appBar: AppBar(
          title: const Text("Multi Language list"),
          actions: [
            IconButton(
                onPressed: () {
                  Provider.of<SecurityModel>(context, listen: false).logout();
                },
                icon: const Icon(Icons.logout))
          ],
        ),
        drawer: const Drawer(child: MenuWidget()),
        body: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(mainAxisSize: MainAxisSize.min, children: [
                        DynamicForm(
                            formConfig: formConfig,
                            optionListMap: optionListMap,
                            formData: sysMultiLanguageSearchRequest,
                            actionHandler: sysMultiLanguageActionHandler),
                        Row(mainAxisSize: MainAxisSize.max, children: [
                          Expanded(
                              child: DynamicTable(
                            columns: resultColumns,
                            rows: resultRows,
                          ))
                        ]),
                        DynamicTablePagging(
                          sysMultiLanguageRowCount,
                          sysMultiLanguageCurrentPage,
                          sysMultiLanguageRowPerPage,
                          pageChangeHandler: (page) {
                            sysMultiLanguagePageChange(page);
                          },
                          rowPerPageChangeHandler: (rowPerPage) {
                            sysMultiLanguageRowPerPage = rowPerPage ?? 10;
                            sysMultiLanguagePageChange(1);
                          },
                        )
                      ])),
                )
              ],
            )));
  }
}

class AdminSysMultiLanguageDetailWidget extends StatefulWidget {
  final String? sysMultiLanguageId;
  final String sysMultiLanguageAction;
  const AdminSysMultiLanguageDetailWidget({required this.sysMultiLanguageAction, this.sysMultiLanguageId, Key? key}) : super(key: key);

  @override
  State<AdminSysMultiLanguageDetailWidget> createState() => _AdminSysMultiLanguageDetailState();
}

class _AdminSysMultiLanguageDetailState extends ReloadableState<AdminSysMultiLanguageDetailWidget> {
  dynamic formConfig = {
    "type": "column",
    "children": [
      {
        "type": "row",
        "children": [
          {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
        ]
      }
    ]
  };
  dynamic optionListMap = {};
  Map<dynamic, dynamic>? sysMultiLanguage;
  TreeData? sysPermTreeData;
  String? error;
  var hasEditPermission = false;

  @override
  void initState() {
    super.initState();
    var userPermList = Provider.of<SecurityModel>(context, listen: false).userPermList;
    if (userPermList != null) {
      hasEditPermission = userPermList.where((permCode) => permCode == "ADMIN").isNotEmpty;
    }
    reload();
  }

  @override
  refreshData() {}

  reload() async {
    if (widget.sysMultiLanguageId != null) {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      var sysMultiLanguageGetResponse =
          await httpGet("/test-framework-api/admin/sys/sys-multilingual/${widget.sysMultiLanguageId}", securityModel.authorization);
      if (sysMultiLanguageGetResponse.containsKey("body") && sysMultiLanguageGetResponse["body"] is String == false) {
        sysMultiLanguage = sysMultiLanguageGetResponse["body"]["result"];
      }
    } else {
      sysMultiLanguage = {"status": 0};
    }
    await resetForm();
  }

  resetForm() async {
    var submitActionLabel = "Submit";
    var actionSubmitType = "primary";
    if (widget.sysMultiLanguageAction == "delete") {
      submitActionLabel = "Delete";
      actionSubmitType = "danger";
    }
    optionListMap = {
      "status": [
        {"value": 1, "label": "Active"},
        {"value": 0, "label": "Inactive"}
      ]
    };
    formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "name", "label": "Name", "hintText": "Name input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {
              "type": "inputText",
              "expanded": true,
              "fieldName": "defaultValue",
              "label": "Default Value",
              "hintText": "Default Value input",
              "maxLines": 3
            }
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "valueEn", "label": "Value En", "hintText": "Value En input", "maxLines": 3}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "valueVi", "label": "Value Vi", "hintText": "Value Vi input", "maxLines": 3}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "valueJp", "label": "Value Jp", "hintText": "Value Jp input", "maxLines": 3}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "status", "label": "status", "hintText": "status input"}
          ]
        },
        {
          "type": "row",
          "children": [
            hasEditPermission ? {"type": "action", "actionType": actionSubmitType, "actionName": "submit", "actionLabel": submitActionLabel} : {},
            {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
          ]
        }
      ]
    };
    setState(() {});
  }

  actionHandler(actionName, formData) async {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    if (actionName == "submit") {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      if (widget.sysMultiLanguageAction == "create") {
        var sysMultiLanguageCreateResponse = await httpPut("/test-framework-api/admin/sys/sys-multilingual", formData, securityModel.authorization);
        if (sysMultiLanguageCreateResponse.containsKey("body") && sysMultiLanguageCreateResponse["body"] is String == false) {
          var sysMultiLanguageCreateResponseBody = sysMultiLanguageCreateResponse["body"];
          if (sysMultiLanguageCreateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (sysMultiLanguageCreateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(sysMultiLanguageCreateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.sysMultiLanguageAction == "edit") {
        var sysMultiLanguageUpdateResponse =
            await httpPost("/test-framework-api/admin/sys/sys-multilingual/${widget.sysMultiLanguageId}", formData, securityModel.authorization);
        if (sysMultiLanguageUpdateResponse.containsKey("body") && sysMultiLanguageUpdateResponse["body"] is String == false) {
          var sysMultiLanguageUpdateResponseBody = sysMultiLanguageUpdateResponse["body"];
          if (sysMultiLanguageUpdateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (sysMultiLanguageUpdateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(sysMultiLanguageUpdateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.sysMultiLanguageAction == "delete") {
        var sysMultiLanguageDeleteResponse =
            await httpDelete("/test-framework-api/admin/sys/sys-multilingual/${widget.sysMultiLanguageId}", securityModel.authorization);
        if (sysMultiLanguageDeleteResponse.containsKey("body") && sysMultiLanguageDeleteResponse["body"] is String == false) {
          var sysMultiLanguageDeleteResponseBody = sysMultiLanguageDeleteResponse["body"];
          if (sysMultiLanguageDeleteResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (sysMultiLanguageDeleteResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(sysMultiLanguageDeleteResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
    }
    if (actionName == "back") {
      navigationModel.pop("/admin/system/sys-multi-language");
    }
  }

  @override
  Widget build(BuildContext context) {
    if (error != null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Error'),
          ),
          body: Center(child: Text("Error $error")));
    }
    if (sysMultiLanguage == null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Loading'),
          ),
          body: const Center(child: CircularProgressIndicator()));
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text('Multi Language detail'),
      ),
      drawer: const Drawer(child: MenuWidget()),
      body: Padding(
          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(mainAxisSize: MainAxisSize.min, children: [
                      Card(
                          child: Column(mainAxisSize: MainAxisSize.min, children: [
                        const Row(
                          children: [
                            Text(
                              "Detail",
                              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        formConfig != null && optionListMap != null
                            ? DynamicForm(
                                formConfig: formConfig, formData: sysMultiLanguage!, optionListMap: optionListMap, actionHandler: actionHandler)
                            : Container()
                      ]))
                    ])),
              )
            ],
          )),
    );
  }
}
