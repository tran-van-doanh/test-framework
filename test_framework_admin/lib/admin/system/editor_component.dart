import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_admin/admin/system/menu.dart';
import 'package:test_framework_admin/api.dart';
import 'package:test_framework_admin/common/dynamic_form.dart';
import 'package:test_framework_admin/common/dynamic_table.dart';
import 'package:test_framework_admin/common/reloadable_state.dart';
import 'package:test_framework_admin/model/model.dart';

class AdminEditorComponentListWidget extends StatefulWidget {
  const AdminEditorComponentListWidget({Key? key}) : super(key: key);

  @override
  State<AdminEditorComponentListWidget> createState() => _AdminEditorComponentListState();
}

class _AdminEditorComponentListState extends ReloadableState<AdminEditorComponentListWidget> {
  var editorComponentSearchRequest = {};
  var editorComponentRowCount = 0;
  var editorComponentCurrentPage = 1;
  var editorComponentResultList = [];
  var editorComponentRowPerPage = 25;
  Widget paging = Container();

  @override
  void initState() {
    super.initState();
    editorComponentSearch({});
  }

  @override
  refreshData() {
    editorComponentPageChange(editorComponentCurrentPage);
  }

  editorComponentSearch(editorComponentSearchRequest) async {
    this.editorComponentSearchRequest = editorComponentSearchRequest;
    await editorComponentPageChange(1);
  }

  editorComponentActionHandler(actionName, formData) async {
    if (actionName == "search") {
      editorComponentSearchRequest = formData;
      await editorComponentPageChange(1);
    }
  }

  editorComponentPageChange(page) async {
    if ((page - 1) * editorComponentRowPerPage > editorComponentRowCount) {
      page = (1.0 * editorComponentRowCount / editorComponentRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * editorComponentRowPerPage, "queryLimit": editorComponentRowPerPage};
    if (editorComponentSearchRequest["titleLike"] != null && editorComponentSearchRequest["titleLike"].isNotEmpty) {
      findRequest["titleLike"] = "%${editorComponentSearchRequest["titleLike"]}%";
    }
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var response = await httpPost("/test-framework-api/admin/test-framework/tf-editor-component/search", findRequest, securityModel.authorization);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        editorComponentCurrentPage = page;
        editorComponentRowCount = response["body"]["rowCount"];
        editorComponentResultList = response["body"]["resultList"];
      });
    }
  }

  sysPermMoveUpHandler(editorComponent) async {
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    await httpPatch("/test-framework-api/admin/test-framework/tf-editor-component/${editorComponent["id"]}/up", {}, securityModel.authorization);
    await editorComponentPageChange(editorComponentCurrentPage);
  }

  sysPermMoveDownHandler(editorComponent) async {
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    await httpPatch("/test-framework-api/admin/test-framework/tf-editor-component/${editorComponent["id"]}/down", {}, securityModel.authorization);
    await editorComponentPageChange(editorComponentCurrentPage);
  }

  @override
  Widget build(BuildContext context) {
    var editorComponentStatusMap = {1: "Active", 0: "Inactive"};
    var optionListMap = {};
    const formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "titleLike", "label": "Title", "hintText": "Title input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "action", "actionName": "search", "actionLabel": "Search"}
          ]
        }
      ]
    };
    var resultColumns = [
      const DataColumn(
        label: Text(
          'Type',
          overflow: TextOverflow.visible,
          softWrap: true,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Category',
          overflow: TextOverflow.visible,
          softWrap: true,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Title',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Status',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      DataColumn(
          label: Row(
        children: [
          ElevatedButton(
            onPressed: () {
              Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/editor-component/create");
            },
            child: const Text('Add'),
          )
        ],
      )),
    ];
    var resultRows = [
      for (var editorComponent in editorComponentResultList)
        DataRow(
          cells: [
            DataCell(Text(editorComponent["type"] ?? "")),
            DataCell(Text(editorComponent["category"] ?? "")),
            DataCell(Text(editorComponent["title"] ?? "")),
            DataCell(Text(editorComponentStatusMap[editorComponent["status"]] ?? "Not defined")),
            DataCell(Row(
              children: [
                ElevatedButton(
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.green)),
                  onPressed: () {
                    sysPermMoveUpHandler(editorComponent);
                  },
                  child: const Text('Up'),
                ),
                ElevatedButton(
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.green)),
                  onPressed: () {
                    sysPermMoveDownHandler(editorComponent);
                  },
                  child: const Text('Down'),
                ),
                ElevatedButton(
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/editor-component/edit/${editorComponent["id"]}");
                  },
                  child: const Text('Edit'),
                ),
                ElevatedButton(
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.red)),
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/editor-component/delete/${editorComponent["id"]}");
                  },
                  child: const Text('Delete'),
                ),
              ],
            )),
          ],
        )
    ];

    return Scaffold(
        appBar: AppBar(
          title: const Text("Editor Component list"),
          actions: [
            IconButton(
                onPressed: () {
                  Provider.of<SecurityModel>(context, listen: false).logout();
                },
                icon: const Icon(Icons.logout))
          ],
        ),
        drawer: const Drawer(child: MenuWidget()),
        body: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(mainAxisSize: MainAxisSize.min, children: [
                        DynamicForm(
                            formConfig: formConfig,
                            optionListMap: optionListMap,
                            formData: editorComponentSearchRequest,
                            actionHandler: editorComponentActionHandler),
                        Row(mainAxisSize: MainAxisSize.max, children: [
                          Expanded(
                              child: DynamicTable(
                            columns: resultColumns,
                            rows: resultRows,
                          ))
                        ]),
                        DynamicTablePagging(
                          editorComponentRowCount,
                          editorComponentCurrentPage,
                          editorComponentRowPerPage,
                          pageChangeHandler: (page) {
                            editorComponentPageChange(page);
                          },
                          rowPerPageChangeHandler: (rowPerPage) {
                            editorComponentRowPerPage = rowPerPage ?? 10;
                            editorComponentPageChange(1);
                          },
                        )
                      ])),
                )
              ],
            )));
  }
}

class AdminEditorComponentDetailWidget extends StatefulWidget {
  final String? editorComponentId;
  final String editorComponentAction;
  const AdminEditorComponentDetailWidget({required this.editorComponentAction, this.editorComponentId, Key? key}) : super(key: key);

  @override
  State<AdminEditorComponentDetailWidget> createState() => _AdminEditorComponentDetailState();
}

class _AdminEditorComponentDetailState extends ReloadableState<AdminEditorComponentDetailWidget> {
  dynamic formConfig = {
    "type": "column",
    "children": [
      {
        "type": "row",
        "children": [
          {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
        ]
      }
    ]
  };
  dynamic optionListMap = {};
  Map<dynamic, dynamic>? editorComponent;
  String? error;
  var hasEditPermission = false;

  @override
  void initState() {
    super.initState();
    var userPermList = Provider.of<SecurityModel>(context, listen: false).userPermList;
    if (userPermList != null) {
      hasEditPermission = userPermList.where((permCode) => permCode == "ADMIN").isNotEmpty;
    }
    reload();
  }

  @override
  refreshData() {}

  reload() async {
    if (widget.editorComponentId != null) {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      var editorComponentGetResponse =
          await httpGet("/test-framework-api/admin/test-framework/tf-editor-component/${widget.editorComponentId}", securityModel.authorization);
      if (editorComponentGetResponse.containsKey("body") && editorComponentGetResponse["body"] is String == false) {
        editorComponent = editorComponentGetResponse["body"]["result"];
        if (editorComponent!["componentConfig"] != null && editorComponent!["componentConfig"].isNotEmpty) {
          editorComponent!["componentConfig"] = jsonEncode(editorComponent!["componentConfig"]);
        } else {
          editorComponent!["componentConfig"] = '';
        }
      }
    } else {
      editorComponent = {"status": 0};
    }
    await resetForm();
  }

  resetForm() async {
    setState(() {
      var submitActionLabel = "Submit";
      var actionSubmitType = "primary";
      if (widget.editorComponentAction == "delete") {
        submitActionLabel = "Delete";
        actionSubmitType = "danger";
      }
      optionListMap = {
        "status": [
          {"value": 1, "label": "Active"},
          {"value": 0, "label": "Inactive"}
        ],
        "platformsList": [
          {"value": "Web", "label": "Web"},
          {"value": "Android", "label": "Android"},
          {"value": "iOS", "label": "iOS"},
          {"value": "Windows", "label": "Windows"},
          {"value": "JavaSwing", "label": "Java Swing"},
          {"value": "Api", "label": "Api"},
        ],
      };

      formConfig = {
        "type": "column",
        "children": [
          {
            "type": "row",
            "children": [
              {"type": "inputText", "expanded": true, "fieldName": "type", "label": "type", "hintText": "Type input"}
            ]
          },
          {
            "type": "row",
            "children": [
              {"type": "inputText", "expanded": true, "fieldName": "category", "label": "category", "hintText": "Category input"}
            ]
          },
          {
            "type": "row",
            "children": [
              {"type": "inputText", "expanded": true, "fieldName": "title", "label": "title", "hintText": "Title input"}
            ]
          },
          {
            "type": "row",
            "children": [
              {"type": "select", "expanded": true, "fieldName": "status", "label": "status", "hintText": "status input"}
            ]
          },
          {
            "type": "row",
            "children": [
              {
                "type": "inputText",
                "maxLines": 5,
                "expanded": true,
                "fieldName": "componentConfig",
                "label": "componentConfig",
                "hintText": "Component Config input"
              }
            ]
          },
          {
            "type": "row",
            "children": [
              {"type": "inputText", "expanded": true, "fieldName": "description", "label": "description", "hintText": "Description input"}
            ]
          },
          {
            "type": "row",
            "children": [
              {"type": "multipleSelect", "expanded": true, "fieldName": "platformsList", "label": "Platform", "hintText": "Platform input"}
            ]
          },
          {
            "type": "row",
            "children": [
              hasEditPermission ? {"type": "action", "actionType": actionSubmitType, "actionName": "submit", "actionLabel": submitActionLabel} : {},
              {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
            ]
          }
        ]
      };
    });
  }

  actionHandler(actionName, formData) async {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    if (actionName == "submit") {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      if (widget.editorComponentAction == "create") {
        if (formData["componentConfig"] != null) {
          try {
            formData["componentConfig"] = jsonDecode(formData["componentConfig"]);
          } catch (e) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text("$e"),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
        var editorComponentCreateResponse =
            await httpPut("/test-framework-api/admin/test-framework/tf-editor-component", formData, securityModel.authorization);
        if (editorComponentCreateResponse.containsKey("body") && editorComponentCreateResponse["body"] is String == false) {
          var editorComponentCreateResponseBody = editorComponentCreateResponse["body"];
          if (editorComponentCreateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (editorComponentCreateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(editorComponentCreateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.editorComponentAction == "edit") {
        if (formData["componentConfig"] != null && formData["componentConfig"].isNotEmpty) {
          try {
            formData["componentConfig"] = jsonDecode(formData["componentConfig"]);
          } catch (e) {
            if (mounted) {
              showDialog<void>(
                context: context,
                builder: (BuildContext context) => AlertDialog(
                  title: const Text('Error'),
                  content: Text("$e"),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () => Navigator.pop(context),
                      child: const Text('Close'),
                    ),
                  ],
                ),
              );
            }
            return 0;
          }
        }
        var editorComponentUpdateResponse = await httpPost(
            "/test-framework-api/admin/test-framework/tf-editor-component/${widget.editorComponentId}", formData, securityModel.authorization);
        if (editorComponentUpdateResponse.containsKey("body") && editorComponentUpdateResponse["body"] is String == false) {
          var editorComponentUpdateResponseBody = editorComponentUpdateResponse["body"];
          if (editorComponentUpdateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (editorComponentUpdateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(editorComponentUpdateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.editorComponentAction == "delete") {
        var editorComponentDeleteResponse =
            await httpDelete("/test-framework-api/admin/test-framework/tf-editor-component/${widget.editorComponentId}", securityModel.authorization);
        if (editorComponentDeleteResponse.containsKey("body") && editorComponentDeleteResponse["body"] is String == false) {
          var editorComponentDeleteResponseBody = editorComponentDeleteResponse["body"];
          if (editorComponentDeleteResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (editorComponentDeleteResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(editorComponentDeleteResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
    }
    if (actionName == "back") {
      navigationModel.pop("/admin/system/editor-component");
    }
  }

  @override
  Widget build(BuildContext context) {
    if (error != null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Error'),
          ),
          body: Center(child: Text("Error $error")));
    }
    if (editorComponent == null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Loading'),
          ),
          body: const Center(child: CircularProgressIndicator()));
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text('Editor component detail'),
      ),
      drawer: const Drawer(child: MenuWidget()),
      body: Padding(
          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(mainAxisSize: MainAxisSize.min, children: [
                      Card(
                          child: Column(mainAxisSize: MainAxisSize.min, children: [
                        const Row(
                          children: [
                            Text(
                              "Detail",
                              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        formConfig != null && optionListMap != null
                            ? DynamicForm(
                                formConfig: formConfig,
                                formData: editorComponent!,
                                optionListMap: optionListMap,
                                actionHandler: actionHandler,
                              )
                            : Container()
                      ]))
                    ])),
              )
            ],
          )),
    );
  }
}
