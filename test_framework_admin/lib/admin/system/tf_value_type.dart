import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_admin/admin/system/menu.dart';
import 'package:test_framework_admin/api.dart';
import 'package:test_framework_admin/common/dynamic_form.dart';
import 'package:test_framework_admin/common/dynamic_table.dart';
import 'package:test_framework_admin/common/reloadable_state.dart';
import 'package:test_framework_admin/common/tree.dart';
import 'package:test_framework_admin/model/model.dart';

class AdminTfValueTypeListWidget extends StatefulWidget {
  const AdminTfValueTypeListWidget({Key? key}) : super(key: key);

  @override
  State<AdminTfValueTypeListWidget> createState() => _AdminTfValueTypeListState();
}

class _AdminTfValueTypeListState extends ReloadableState<AdminTfValueTypeListWidget> {
  var tfValueTypeSearchRequest = {};
  var tfValueTypeRowCount = 0;
  var tfValueTypeCurrentPage = 1;
  var tfValueTypeResultList = [];
  var tfValueTypeRowPerPage = 25;
  Widget paging = Container();

  @override
  void initState() {
    super.initState();
    tfValueTypeSearch({});
  }

  @override
  refreshData() {
    tfValueTypePageChange(tfValueTypeCurrentPage);
  }

  tfValueTypeSearch(tfValueTypeSearchRequest) async {
    this.tfValueTypeSearchRequest = tfValueTypeSearchRequest;
    await tfValueTypePageChange(1);
  }

  tfValueTypeActionHandler(actionName, formData) async {
    if (actionName == "search") {
      tfValueTypeSearchRequest = formData;
      await tfValueTypePageChange(1);
    }
  }

  tfValueTypePageChange(page) async {
    if ((page - 1) * tfValueTypeRowPerPage > tfValueTypeRowCount) {
      page = (1.0 * tfValueTypeRowCount / tfValueTypeRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * tfValueTypeRowPerPage, "queryLimit": tfValueTypeRowPerPage};
    if (tfValueTypeSearchRequest["nameLike"] != null && tfValueTypeSearchRequest["nameLike"].isNotEmpty) {
      findRequest["nameLike"] = "%${tfValueTypeSearchRequest["nameLike"]}%";
    }
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var response = await httpPost("/test-framework-api/user/test-framework/tf-value-type/search", findRequest, securityModel.authorization);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        tfValueTypeCurrentPage = page;
        tfValueTypeRowCount = response["body"]["rowCount"];
        tfValueTypeResultList = response["body"]["resultList"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var tfValueTypeStatusMap = {1: "Active", 0: "Inactive"};
    var optionListMap = {};
    const formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "nameLike", "label": "Name", "hintText": "Name input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "action", "actionName": "search", "actionLabel": "Search"}
          ]
        }
      ]
    };
    var resultColumns = [
      const DataColumn(
        label: Text(
          'Name',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Status',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      DataColumn(
          label: Row(
        children: [
          ElevatedButton(
            onPressed: () {
              Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/tf-value-type/create");
            },
            child: const Text('Add'),
          )
        ],
      )),
    ];
    var resultRows = [
      for (var tfValueType in tfValueTypeResultList)
        DataRow(
          cells: [
            DataCell(Text(tfValueType["name"] ?? "")),
            DataCell(Text(tfValueTypeStatusMap[tfValueType["status"]] ?? "Not defined")),
            DataCell(Row(
              children: [
                ElevatedButton(
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/tf-value-type/edit/${tfValueType["id"]}");
                  },
                  child: const Text('Edit'),
                ),
                ElevatedButton(
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.red)),
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/tf-value-type/delete/${tfValueType["id"]}");
                  },
                  child: const Text('Delete'),
                ),
              ],
            )),
          ],
        )
    ];

    return Scaffold(
        appBar: AppBar(
          title: const Text("Value type list"),
          actions: [
            IconButton(
                onPressed: () {
                  Provider.of<SecurityModel>(context, listen: false).logout();
                },
                icon: const Icon(Icons.logout))
          ],
        ),
        drawer: const Drawer(child: MenuWidget()),
        body: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(mainAxisSize: MainAxisSize.min, children: [
                        DynamicForm(
                            formConfig: formConfig,
                            optionListMap: optionListMap,
                            formData: tfValueTypeSearchRequest,
                            actionHandler: tfValueTypeActionHandler),
                        Row(mainAxisSize: MainAxisSize.max, children: [
                          Expanded(
                              child: DynamicTable(
                            columns: resultColumns,
                            rows: resultRows,
                          ))
                        ]),
                        DynamicTablePagging(
                          tfValueTypeRowCount,
                          tfValueTypeCurrentPage,
                          tfValueTypeRowPerPage,
                          pageChangeHandler: (page) {
                            tfValueTypePageChange(page);
                          },
                          rowPerPageChangeHandler: (rowPerPage) {
                            tfValueTypeRowPerPage = rowPerPage ?? 10;
                            tfValueTypePageChange(1);
                          },
                        )
                      ])),
                )
              ],
            )));
  }
}

class AdminTfValueTypeDetailWidget extends StatefulWidget {
  final String? tfValueTypeId;
  final String tfValueTypeAction;
  const AdminTfValueTypeDetailWidget({required this.tfValueTypeAction, this.tfValueTypeId, Key? key}) : super(key: key);

  @override
  State<AdminTfValueTypeDetailWidget> createState() => _AdminTfValueTypeDetailState();
}

class _AdminTfValueTypeDetailState extends ReloadableState<AdminTfValueTypeDetailWidget> {
  dynamic formConfig = {
    "type": "column",
    "children": [
      {
        "type": "row",
        "children": [
          {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
        ]
      }
    ]
  };
  dynamic optionListMap = {};
  Map<dynamic, dynamic>? tfValueType;
  TreeData? sysPermTreeData;
  String? error;
  var hasEditPermission = false;

  @override
  void initState() {
    super.initState();
    var userPermList = Provider.of<SecurityModel>(context, listen: false).userPermList;
    if (userPermList != null) {
      hasEditPermission = userPermList.where((permCode) => permCode == "ADMIN").isNotEmpty;
    }
    reload();
  }

  @override
  refreshData() {}

  reload() async {
    if (widget.tfValueTypeId != null) {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      var tfValueTypeGetResponse =
          await httpGet("/test-framework-api/user/test-framework/tf-value-type/${widget.tfValueTypeId}", securityModel.authorization);
      if (tfValueTypeGetResponse.containsKey("body") && tfValueTypeGetResponse["body"] is String == false) {
        tfValueType = tfValueTypeGetResponse["body"]["result"];
      }
    } else {
      tfValueType = {"status": 0};
    }
    await resetForm();
  }

  resetForm() async {
    var submitActionLabel = "Submit";
    var actionSubmitType = "primary";
    if (widget.tfValueTypeAction == "delete") {
      submitActionLabel = "Delete";
      actionSubmitType = "danger";
    }
    optionListMap = {
      "status": [
        {"value": 1, "label": "Active"},
        {"value": 0, "label": "Inactive"}
      ]
    };
    formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "name", "label": "Name", "hintText": "Name input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {
              "type": "inputText",
              "maxLines": 5,
              "expanded": true,
              "fieldName": "description",
              "label": "description",
              "hintText": "description input"
            }
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "status", "label": "status", "hintText": "status input"}
          ]
        },
        {
          "type": "row",
          "children": [
            hasEditPermission ? {"type": "action", "actionType": actionSubmitType, "actionName": "submit", "actionLabel": submitActionLabel} : {},
            {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
          ]
        }
      ]
    };
    setState(() {});
  }

  actionHandler(actionName, formData) async {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    if (actionName == "submit") {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      if (widget.tfValueTypeAction == "create") {
        var tfValueTypeCreateResponse = await httpPut("/test-framework-api/user/test-framework/tf-value-type", formData, securityModel.authorization);
        if (tfValueTypeCreateResponse.containsKey("body") && tfValueTypeCreateResponse["body"] is String == false) {
          var tfValueTypeCreateResponseBody = tfValueTypeCreateResponse["body"];
          if (tfValueTypeCreateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (tfValueTypeCreateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(tfValueTypeCreateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.tfValueTypeAction == "edit") {
        var tfValueTypeUpdateResponse =
            await httpPost("/test-framework-api/user/test-framework/tf-value-type/${widget.tfValueTypeId}", formData, securityModel.authorization);
        if (tfValueTypeUpdateResponse.containsKey("body") && tfValueTypeUpdateResponse["body"] is String == false) {
          var tfValueTypeUpdateResponseBody = tfValueTypeUpdateResponse["body"];
          if (tfValueTypeUpdateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (tfValueTypeUpdateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(tfValueTypeUpdateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.tfValueTypeAction == "delete") {
        var tfValueTypeDeleteResponse =
            await httpDelete("/test-framework-api/user/test-framework/tf-value-type/${widget.tfValueTypeId}", securityModel.authorization);
        if (tfValueTypeDeleteResponse.containsKey("body") && tfValueTypeDeleteResponse["body"] is String == false) {
          var tfValueTypeDeleteResponseBody = tfValueTypeDeleteResponse["body"];
          if (tfValueTypeDeleteResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (tfValueTypeDeleteResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(tfValueTypeDeleteResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
    }
    if (actionName == "back") {
      navigationModel.pop("/admin/system/tf-value-type");
    }
  }

  @override
  Widget build(BuildContext context) {
    if (error != null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Error'),
          ),
          body: Center(child: Text("Error $error")));
    }
    if (tfValueType == null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Loading'),
          ),
          body: const Center(child: CircularProgressIndicator()));
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text('Value type detail'),
      ),
      drawer: const Drawer(child: MenuWidget()),
      body: Padding(
          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(mainAxisSize: MainAxisSize.min, children: [
                      Card(
                          child: Column(mainAxisSize: MainAxisSize.min, children: [
                        const Row(
                          children: [
                            Text(
                              "Detail",
                              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        formConfig != null && optionListMap != null
                            ? DynamicForm(formConfig: formConfig, formData: tfValueType!, optionListMap: optionListMap, actionHandler: actionHandler)
                            : Container()
                      ]))
                    ])),
              )
            ],
          )),
    );
  }
}
