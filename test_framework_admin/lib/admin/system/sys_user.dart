import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:crypto/crypto.dart';
import 'dart:convert';
import 'package:test_framework_admin/admin/system/menu.dart';
import 'package:test_framework_admin/api.dart';
import 'package:test_framework_admin/common/dynamic_form.dart';
import 'package:test_framework_admin/common/dynamic_table.dart';
import 'package:test_framework_admin/common/reloadable_state.dart';
import 'package:test_framework_admin/model/model.dart';

class AdminSysUserListWidget extends StatefulWidget {
  const AdminSysUserListWidget({Key? key}) : super(key: key);

  @override
  State<AdminSysUserListWidget> createState() => _AdminSysUserListState();
}

class _AdminSysUserListState extends ReloadableState<AdminSysUserListWidget> {
  dynamic formConfig = {"type": "column", "children": []};
  dynamic optionListMap = {};
  var sysUserSearchRequest = {};
  var sysUserRowCount = 0;
  var sysUserCurrentPage = 1;
  var sysUserResultList = [];
  var sysUserRowPerPage = 25;
  Widget paging = Container();

  @override
  void initState() {
    super.initState();
    reset();
  }

  @override
  refreshData() {
    sysUserPageChange(sysUserCurrentPage);
  }

  reset() async {
    await resetForm();
    await sysUserSearch({});
  }

  resetForm() async {
    optionListMap = {
      "status": [
        {"value": 1, "label": "Active"},
        {"value": 0, "label": "Inactive"}
      ],
      "sysOrganizationId": []
    };
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var sysOrganizationFindResponse =
        await httpPost("/test-framework-api/admin/sys/sys-organization/search", {"status": 1}, securityModel.authorization);
    if (sysOrganizationFindResponse.containsKey("body") && sysOrganizationFindResponse["body"] is String == false) {
      optionListMap["sysOrganizationId"] = sysOrganizationFindResponse["body"]["resultList"]
          .map((sysOrganization) => {"value": sysOrganization["id"], "label": sysOrganization["name"]})
          .toList();
    }
    formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "sysOrganizationId", "label": "Organization", "hintText": "Organization input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "fullnameLike", "label": "Fullname", "hintText": "Fullname input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "nicknameLike", "label": "nickname", "hintText": "nickname input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "emailLike", "label": "Email", "hintText": "Email input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "action", "actionName": "search", "actionLabel": "Search"}
          ]
        }
      ]
    };
    setState(() {});
  }

  sysUserSearch(sysUserSearchRequest) async {
    this.sysUserSearchRequest = sysUserSearchRequest;
    await sysUserPageChange(1);
  }

  sysUserActionHandler(actionName, formData) async {
    if (actionName == "search") {
      sysUserSearchRequest = formData;
      await sysUserPageChange(1);
    }
  }

  sysUserPageChange(page) async {
    if ((page - 1) * sysUserRowPerPage > sysUserRowCount) {
      page = (1.0 * sysUserRowCount / sysUserRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * sysUserRowPerPage, "queryLimit": sysUserRowPerPage};
    if (sysUserSearchRequest["fullnameLike"] != null && sysUserSearchRequest["fullnameLike"].isNotEmpty) {
      findRequest["fullnameLike"] = "%${sysUserSearchRequest["fullnameLike"]}%";
    }
    if (sysUserSearchRequest["nicknameLike"] != null && sysUserSearchRequest["nicknameLike"].isNotEmpty) {
      findRequest["nicknameLike"] = "%${sysUserSearchRequest["nicknameLike"]}%";
    }
    if (sysUserSearchRequest["emailLike"] != null && sysUserSearchRequest["emailLike"].isNotEmpty) {
      findRequest["emailLike"] = "%${sysUserSearchRequest["emailLike"]}%";
    }
    if (sysUserSearchRequest["sysOrganizationId"] != null && sysUserSearchRequest["sysOrganizationId"].isNotEmpty) {
      findRequest["sysOrganizationId"] = sysUserSearchRequest["sysOrganizationId"];
    }
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var response = await httpPost("/test-framework-api/admin/sys/sys-user/search", findRequest, securityModel.authorization);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        sysUserCurrentPage = page;
        sysUserRowCount = response["body"]["rowCount"];
        sysUserResultList = response["body"]["resultList"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var sysUserStatusMap = {1: "Active", 0: "Inactive"};
    var resultColumns = [
      const DataColumn(
        label: Text(
          'Fullname',
          overflow: TextOverflow.visible,
          softWrap: true,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Nickname',
          overflow: TextOverflow.visible,
          softWrap: true,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Email',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Status',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Organization',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      DataColumn(
          label: Row(
        children: [
          ElevatedButton(
            onPressed: () {
              Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-user/create");
            },
            child: const Text('Add'),
          )
        ],
      )),
    ];
    var resultRows = [
      for (var sysUser in sysUserResultList)
        DataRow(
          cells: [
            DataCell(Text(sysUser["fullname"] ?? "")),
            DataCell(Text(sysUser["nickname"] ?? "")),
            DataCell(Text(sysUser["email"] ?? "")),
            DataCell(Text(sysUserStatusMap[sysUser["status"]] ?? "Not defined")),
            DataCell(Text(sysUser["sysOrganization"]["name"])),
            DataCell(Row(
              children: [
                ElevatedButton(
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-user/change-password/${sysUser["id"]}");
                  },
                  child: const Text('Change password'),
                ),
                ElevatedButton(
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/sys-user/edit/${sysUser["id"]}");
                  },
                  child: const Text('Edit'),
                ),
              ],
            )),
          ],
        )
    ];

    return Scaffold(
        appBar: AppBar(
          title: const Text("User list"),
          actions: [
            IconButton(
                onPressed: () {
                  Provider.of<SecurityModel>(context, listen: false).logout();
                },
                icon: const Icon(Icons.logout))
          ],
        ),
        drawer: const Drawer(child: MenuWidget()),
        body: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(mainAxisSize: MainAxisSize.min, children: [
                        DynamicForm(
                            formConfig: formConfig,
                            optionListMap: optionListMap,
                            formData: sysUserSearchRequest,
                            actionHandler: sysUserActionHandler),
                        Row(mainAxisSize: MainAxisSize.max, children: [
                          Expanded(
                              child: DynamicTable(
                            columns: resultColumns,
                            rows: resultRows,
                          ))
                        ]),
                        DynamicTablePagging(
                          sysUserRowCount,
                          sysUserCurrentPage,
                          sysUserRowPerPage,
                          pageChangeHandler: (page) {
                            sysUserPageChange(page);
                          },
                          rowPerPageChangeHandler: (rowPerPage) {
                            sysUserRowPerPage = rowPerPage ?? 10;
                            sysUserPageChange(1);
                          },
                        )
                      ])),
                )
              ],
            )));
  }
}

class AdminSysUserDetailWidget extends StatefulWidget {
  final String? sysUserId;
  final String sysUserAction;
  const AdminSysUserDetailWidget({required this.sysUserAction, this.sysUserId, Key? key}) : super(key: key);

  @override
  State<AdminSysUserDetailWidget> createState() => _AdminSysUserDetailState();
}

class _AdminSysUserDetailState extends ReloadableState<AdminSysUserDetailWidget> {
  dynamic formConfig = {
    "type": "column",
    "children": [
      {
        "type": "row",
        "children": [
          {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
        ]
      }
    ]
  };
  dynamic optionListMap = {};
  Map<dynamic, dynamic>? sysUser;
  String? error;
  var hasEditPermission = false;

  @override
  void initState() {
    super.initState();
    var userPermList = Provider.of<SecurityModel>(context, listen: false).userPermList;
    if (userPermList != null) {
      hasEditPermission = userPermList.where((permCode) => permCode == "ADMIN").isNotEmpty;
    }
    reload();
  }

  @override
  refreshData() {}

  reload() async {
    if (widget.sysUserId != null) {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      var sysUserGetResponse = await httpGet("/test-framework-api/admin/sys/sys-user/${widget.sysUserId}", securityModel.authorization);
      if (sysUserGetResponse.containsKey("body") && sysUserGetResponse["body"] is String == false) {
        sysUser = sysUserGetResponse["body"]["result"];
      }
    } else {
      sysUser = {"status": 0};
    }
    await resetForm();
  }

  resetForm() async {
    var submitActionLabel = "Submit";
    var actionSubmitType = "primary";
    if (widget.sysUserAction == "change-password") {
      formConfig = {
        "type": "column",
        "children": [
          {
            "type": "row",
            "children": [
              {"type": "inputText", "expanded": true, "obscureText": true, "fieldName": "password", "label": "Password", "hintText": "Password input"}
            ]
          },
          {
            "type": "row",
            "children": hasEditPermission
                ? [
                    {"type": "action", "actionType": actionSubmitType, "actionName": "submit", "actionLabel": submitActionLabel},
                    {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
                  ]
                : [
                    {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
                  ]
          }
        ]
      };
    } else {
      optionListMap = {
        "status": [
          {"value": 1, "label": "Active"},
          {"value": 0, "label": "Inactive"}
        ],
        "sysOrganizationId": [],
        "sysRoleIdList": [],
        "sysOrganizationRoleIdList": []
      };
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      var sysRoleFindResponse = await httpPost("/test-framework-api/admin/sys/sys-role/search", {"status": 1}, securityModel.authorization);
      if (sysRoleFindResponse.containsKey("body") && sysRoleFindResponse["body"] is String == false) {
        optionListMap["sysRoleIdList"] =
            sysRoleFindResponse["body"]["resultList"].map((sysRole) => {"value": sysRole["id"], "label": sysRole["name"]}).toList();
      }
      var sysOrganizationFindResponse =
          await httpPost("/test-framework-api/admin/sys/sys-organization/search", {"status": 1}, securityModel.authorization);
      if (sysOrganizationFindResponse.containsKey("body") && sysOrganizationFindResponse["body"] is String == false) {
        optionListMap["sysOrganizationId"] = sysOrganizationFindResponse["body"]["resultList"]
            .map(
                (sysOrganization) => {"disabled": widget.sysUserAction != "create", "value": sysOrganization["id"], "label": sysOrganization["name"]})
            .toList();
      }
      if (sysUser != null) {
        if (sysUser!["sysOrganizationId"] != null) {
          var sysOrganizationRoleFindResponse = await httpPost("/test-framework-api/admin/sys/sys-item-role/search",
              {"status": 1, "sysOrganizationId": sysUser!["sysOrganizationId"]}, securityModel.authorization);
          if (sysOrganizationRoleFindResponse.containsKey("body") && sysOrganizationRoleFindResponse["body"] is String == false) {
            optionListMap["sysOrganizationRoleIdList"] =
                sysOrganizationRoleFindResponse["body"]["resultList"].map((sysRole) => {"value": sysRole["id"], "label": sysRole["name"]}).toList();
          }
        }
      }

      formConfig = {
        "type": "column",
        "children": [
          {
            "type": "row",
            "children": [
              {"type": "select", "expanded": true, "fieldName": "sysOrganizationId", "label": "Organization", "hintText": "Organization input"}
            ]
          },
          {
            "type": "row",
            "children": [
              {"type": "inputText", "expanded": true, "fieldName": "fullname", "label": "Fullname", "hintText": "Fullname input"}
            ]
          },
          {
            "type": "row",
            "children": [
              {"type": "inputText", "expanded": true, "fieldName": "nickname", "label": "nickname", "hintText": "nickname input"}
            ]
          },
          {
            "type": "row",
            "children": [
              {"type": "inputText", "expanded": true, "fieldName": "email", "label": "Email", "hintText": "Email input"}
            ]
          },
          {
            "type": "row",
            "children": [
              {"type": "inputText", "expanded": true, "fieldName": "description", "label": "description", "hintText": "description input"}
            ]
          },
          {
            "type": "row",
            "children": [
              {"type": "select", "expanded": true, "fieldName": "status", "label": "status", "hintText": "status input"}
            ]
          },
          {
            "type": "row",
            "children": [
              {"type": "multipleSelect", "expanded": true, "fieldName": "sysRoleIdList", "label": "Role", "hintText": "Role input"}
            ]
          },
          {
            "type": "row",
            "children": [
              {
                "type": "multipleSelect",
                "expanded": true,
                "fieldName": "sysOrganizationRoleIdList",
                "label": "Organization Role",
                "hintText": "Organization Role input"
              }
            ]
          },
          {
            "type": "row",
            "children": hasEditPermission
                ? [
                    {"type": "action", "actionType": actionSubmitType, "actionName": "submit", "actionLabel": submitActionLabel},
                    {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
                  ]
                : [
                    {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
                  ]
          }
        ]
      };
    }
    setState(() {});
  }

  onChanged(fieldName, value) async {
    if (fieldName == "sysOrganizationId") {
      sysUser!["sysOrganizationId"] = value;
    }
    if (fieldName == "sysOrganizationId") {
      resetForm();
    }
  }

  actionHandler(actionName, formData) async {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    if (actionName == "submit") {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      if (widget.sysUserAction == "create") {
        var sysUserCreateResponse = await httpPut("/test-framework-api/admin/sys/sys-user", formData, securityModel.authorization);
        if (sysUserCreateResponse.containsKey("body") && sysUserCreateResponse["body"] is String == false) {
          var sysUserCreateResponseBody = sysUserCreateResponse["body"];
          if (sysUserCreateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (sysUserCreateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(sysUserCreateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.sysUserAction == "edit") {
        var sysUserUpdateResponse =
            await httpPost("/test-framework-api/admin/sys/sys-user/${widget.sysUserId}", formData, securityModel.authorization);
        if (sysUserUpdateResponse.containsKey("body") && sysUserUpdateResponse["body"] is String == false) {
          var sysUserUpdateResponseBody = sysUserUpdateResponse["body"];
          if (sysUserUpdateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (sysUserUpdateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(sysUserUpdateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.sysUserAction == "change-password") {
        var password = formData["password"];
        if (password != null) {
          var passwordBytes = utf8.encode(password);
          var passwordDigest = sha256.convert(passwordBytes);
          password = base64.encode(passwordDigest.bytes);
        }
        var changePasswordRequest = {
          "email": formData["email"],
          "password": password,
        };
        var sysUserUpdateResponse = await httpPost(
            "/test-framework-api/admin/sys/sys-user/${widget.sysUserId}/change-password", changePasswordRequest, securityModel.authorization);
        if (sysUserUpdateResponse.containsKey("body") && sysUserUpdateResponse["body"] is String == false) {
          var sysUserUpdateResponseBody = sysUserUpdateResponse["body"];
          if (sysUserUpdateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (sysUserUpdateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(sysUserUpdateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
    }
    if (actionName == "back") {
      navigationModel.pop("/admin/system/sys-user");
    }
  }

  @override
  Widget build(BuildContext context) {
    if (error != null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Error'),
          ),
          body: Center(child: Text("Error $error")));
    }
    if (sysUser == null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Loading'),
          ),
          body: const Center(child: CircularProgressIndicator()));
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text('User detail'),
      ),
      drawer: const Drawer(child: MenuWidget()),
      body: Padding(
          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(mainAxisSize: MainAxisSize.min, children: [
                      Card(
                          child: Column(mainAxisSize: MainAxisSize.min, children: [
                        const Row(
                          children: [
                            Text(
                              "Detail",
                              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        formConfig != null && optionListMap != null
                            ? DynamicForm(
                                formConfig: formConfig,
                                formData: sysUser!,
                                optionListMap: optionListMap,
                                actionHandler: actionHandler,
                                onChanged: onChanged)
                            : Container()
                      ]))
                    ])),
              )
            ],
          )),
    );
  }
}
