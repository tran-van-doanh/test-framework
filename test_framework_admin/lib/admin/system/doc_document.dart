import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:file_picker/file_picker.dart';
import 'package:flutter/gestures.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_admin/admin/system/menu.dart';
import 'package:test_framework_admin/api.dart';
import 'package:test_framework_admin/common/dynamic_form.dart';
import 'package:test_framework_admin/common/dynamic_table.dart';
import 'package:test_framework_admin/common/file_download.dart';
import 'package:test_framework_admin/common/reloadable_state.dart';
import 'package:test_framework_admin/config.dart';
import 'package:test_framework_admin/model/model.dart';

import '../../common/type.dart';

class AdminDocDocumentListPageWidget extends StatelessWidget {
  final String? parentId;
  const AdminDocDocumentListPageWidget({this.parentId, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Doc document list"),
          actions: [
            IconButton(
                onPressed: () {
                  Provider.of<SecurityModel>(context, listen: false).logout();
                },
                icon: const Icon(Icons.logout))
          ],
        ),
        drawer: const Drawer(child: MenuWidget()),
        body: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: AdminDocDocumentListWidget(
                        parentId: parentId,
                      )),
                )
              ],
            )));
  }
}

class AdminDocDocumentListWidget extends StatefulWidget {
  final String? parentId;
  const AdminDocDocumentListWidget({this.parentId, Key? key}) : super(key: key);

  @override
  State<AdminDocDocumentListWidget> createState() => _AdminDocDocumentListState();
}

class _AdminDocDocumentListState extends ReloadableState<AdminDocDocumentListWidget> {
  Map<dynamic, dynamic> docDocumentSearchRequest = {};
  var docDocumentRowCount = 0;
  var docDocumentCurrentPage = 1;
  var docDocumentResultList = [];
  var docDocumentRowPerPage = 25;
  PlatformFile? objFile;

  @override
  void initState() {
    super.initState();
    docDocumentSearch({});
  }

  uploadFile() async {
    final request = http.MultipartRequest(
      "POST",
      Uri.parse(
        "$baseUrl/test-framework-api/admin/doc-file/import",
      ),
    );
    request.headers.addAll({"Authorization": Provider.of<SecurityModel>(context, listen: false).authorization!});
    request.files.add(http.MultipartFile.fromBytes('file', await readFile(objFile!), filename: objFile!.name));
    var resp = await request.send();
    if (resp.statusCode == 200) {
      await docDocumentPageChange(1);
    } else if (mounted) {
      showDialog<void>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: const Text('Error'),
          content: const Text("Unsuccessfully import"),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text('Close'),
            ),
          ],
        ),
      );
    }
  }

  @override
  refreshData() {
    docDocumentPageChange(docDocumentCurrentPage);
  }

  docDocumentSearch(docDocumentSearchRequest) async {
    this.docDocumentSearchRequest = docDocumentSearchRequest;
    await docDocumentPageChange(1);
  }

  docDocumentSearchActionHandler(actionName, formData) async {
    if (actionName == "docDocumentSearch") {
      docDocumentSearchRequest = formData;
      await docDocumentPageChange(1);
    }
    if (actionName == "docDocumentImport") {
      objFile = await selectFile(['zip']);
      await uploadFile();
    }
    if (actionName == "docDocumentExport" && mounted) {
      File? downloadedFile = await httpDownloadFile(
          context, "/test-framework-api/admin/doc-file/export", Provider.of<SecurityModel>(context, listen: false).authorization, 'Output.zip');
      if (downloadedFile != null && mounted) {
        showDialog<void>(
          context: context,
          builder: (BuildContext context) => AlertDialog(
            title: const Text('Error'),
            content: const Text("Unsuccessfully import"),
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.pop(context),
                child: const Text('Close'),
              ),
            ],
          ),
        );
      }
    }
  }

  docDocumentPageChange(page) async {
    if ((page - 1) * docDocumentRowPerPage > docDocumentRowCount) {
      page = (1.0 * docDocumentRowCount / docDocumentRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var docDocumentFindRequest = {"queryOffset": (page - 1) * docDocumentRowPerPage, "queryLimit": docDocumentRowPerPage};
    if (widget.parentId != null) {
      docDocumentFindRequest["parentId"] = widget.parentId;
    } else {
      docDocumentFindRequest["parentIdIsNull"] = true;
    }
    if (docDocumentSearchRequest["nameLike"] != null && docDocumentSearchRequest["nameLike"].isNotEmpty) {
      docDocumentFindRequest["nameLike"] = "%${docDocumentSearchRequest["nameLike"]}%";
    }
    if (docDocumentSearchRequest["titleLike"] != null && docDocumentSearchRequest["titleLike"].isNotEmpty) {
      docDocumentFindRequest["titleLike"] = "%${docDocumentSearchRequest["titleLike"]}%";
    }
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var docDocumentFindResponse =
        await httpPost("/test-framework-api/admin/doc/doc-document/search", docDocumentFindRequest, securityModel.authorization);
    if (docDocumentFindResponse.containsKey("body") && docDocumentFindResponse["body"] is String == false) {
      setState(() {
        docDocumentCurrentPage = page;
        docDocumentRowCount = docDocumentFindResponse["body"]["rowCount"];
        docDocumentResultList = docDocumentFindResponse["body"]["resultList"];
      });
    }
  }

  docDocumentMoveUpHandler(docDocument) async {
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    await httpPatch("/test-framework-api/admin/doc/doc-document/${docDocument["id"]}/up", {}, securityModel.authorization);
    await docDocumentPageChange(docDocumentCurrentPage);
  }

  docDocumentMoveDownHandler(docDocument) async {
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    await httpPatch("/test-framework-api/admin/doc/doc-document/${docDocument["id"]}/down", {}, securityModel.authorization);
    await docDocumentPageChange(docDocumentCurrentPage);
  }

  getResultColumns() {
    return [
      const DataColumn(
        label: Text(
          'Name',
          overflow: TextOverflow.visible,
          softWrap: true,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Title',
          overflow: TextOverflow.visible,
          softWrap: true,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Description',
          overflow: TextOverflow.visible,
          softWrap: true,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Language',
          overflow: TextOverflow.visible,
          softWrap: true,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Type',
          overflow: TextOverflow.visible,
          softWrap: true,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Status',
          overflow: TextOverflow.visible,
          softWrap: true,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      DataColumn(
          label: Row(children: [
        ElevatedButton(
          onPressed: () {
            if (widget.parentId != null) {
              Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/doc-document/create/${widget.parentId}");
            } else {
              Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/doc-document/create");
            }
          },
          child: const Text("Add"),
        )
      ]))
    ];
  }

  getResultRows() {
    var docDocumentStatusMap = {1: "Active", 0: "Inactive"};
    return [
      for (var docDocument in docDocumentResultList)
        DataRow(
          cells: [
            DataCell(Text(docDocument["name"] ?? "")),
            DataCell(Text(docDocument["title"] ?? "")),
            DataCell(Text(docDocument["description"] ?? "")),
            DataCell(Text(docDocument["language"] ?? "")),
            DataCell(Text(docDocument["documentType"] ?? "")),
            DataCell(Text(docDocumentStatusMap[docDocument["status"]] ?? "Not defined")),
            DataCell(Row(
              children: [
                ElevatedButton(
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.green)),
                  onPressed: () {
                    docDocumentMoveUpHandler(docDocument);
                  },
                  child: const Text('Up'),
                ),
                ElevatedButton(
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.green)),
                  onPressed: () {
                    docDocumentMoveDownHandler(docDocument);
                  },
                  child: const Text('Down'),
                ),
                ElevatedButton(
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/doc-document/edit/${docDocument["id"]}");
                  },
                  child: const Text('Edit'),
                ),
                ElevatedButton(
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.red)),
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/doc-document/delete/${docDocument["id"]}");
                  },
                  child: const Text('Delete'),
                ),
              ],
            )),
          ],
        )
    ];
  }

  @override
  Widget build(BuildContext context) {
    const formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "nameLike", "label": "Name", "hintText": "name input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "titleLike", "label": "Title", "hintText": "Title input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "action", "actionName": "docDocumentSearch", "actionLabel": "Search"},
            {"type": "action", "actionName": "docDocumentExport", "actionLabel": "Export"},
            {"type": "action", "actionName": "docDocumentImport", "actionLabel": "Import"},
          ]
        }
      ]
    };
    var resultColumns = getResultColumns();
    var resultRows = getResultRows();
    return Column(mainAxisSize: MainAxisSize.min, children: [
      DynamicForm(formConfig: formConfig, optionListMap: const {}, formData: docDocumentSearchRequest, actionHandler: docDocumentSearchActionHandler),
      Row(mainAxisSize: MainAxisSize.max, children: [
        Expanded(
            child: DynamicTable(
          columns: resultColumns,
          rows: resultRows,
        ))
      ]),
      DynamicTablePagging(
        docDocumentRowCount,
        docDocumentCurrentPage,
        docDocumentRowPerPage,
        pageChangeHandler: (page) {
          docDocumentPageChange(page);
        },
        rowPerPageChangeHandler: (rowPerPage) {
          docDocumentRowPerPage = rowPerPage ?? 10;
          docDocumentPageChange(1);
        },
      )
    ]);
  }
}

class AdminDocDocumentDetailWidget extends StatefulWidget {
  final String docDocumentAction;
  final String? docDocumentId;
  final String? parentId;
  const AdminDocDocumentDetailWidget({required this.docDocumentAction, this.docDocumentId, this.parentId, Key? key}) : super(key: key);

  @override
  State<AdminDocDocumentDetailWidget> createState() => _AdminDocDocumentDetailState();
}

class _AdminDocDocumentDetailState extends ReloadableState<AdminDocDocumentDetailWidget> {
  List<dynamic>? docDocumentPathList;
  Map<dynamic, dynamic>? docDocument;
  dynamic formConfig = {
    "type": "column",
    "children": [
      {
        "type": "row",
        "children": [
          {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
        ]
      }
    ]
  };
  dynamic optionListMap = {};
  String? error;

  @override
  void initState() {
    super.initState();
    reload();
  }

  @override
  refreshData() {}

  reload() async {
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    if (widget.docDocumentId != null) {
      var docDocumentGetResponse = await httpGet("/test-framework-api/admin/doc/doc-document/${widget.docDocumentId!}", securityModel.authorization);
      if (docDocumentGetResponse.containsKey("body") && docDocumentGetResponse["body"] is String == false) {
        docDocument = docDocumentGetResponse["body"]["result"];
      }
      var docDocumentPathResponse =
          await httpGet("/test-framework-api/admin/doc/doc-document/${widget.docDocumentId!}/path", securityModel.authorization);
      if (docDocumentPathResponse.containsKey("body") && docDocumentPathResponse["body"] is String == false) {
        docDocumentPathList = docDocumentPathResponse["body"]["resultList"];
      }
    } else {
      docDocument = {
        //ctrl z
        "status": 1,
        "language": "vn",
        "parentId": widget.parentId,
      };
      if (widget.parentId != null) {
        var docDocumentPathResponse =
            await httpGet("/test-framework-api/admin/doc/doc-document/${widget.parentId!}/path", securityModel.authorization);
        if (docDocumentPathResponse.containsKey("body") && docDocumentPathResponse["body"] is String == false) {
          docDocumentPathList = docDocumentPathResponse["body"]["resultList"];
        }
      }
    }
    await resetForm();
  }

  resetForm() async {
    var submitActionLabel = "Submit";
    var actionSubmitType = "primary";
    if (widget.docDocumentAction == "delete") {
      submitActionLabel = "Delete";
      actionSubmitType = "danger";
    }
    optionListMap = {
      "status": [
        {"value": 1, "label": "Active"},
        {"value": 0, "label": "Inactive"}
      ],
      "language": [
        {"value": "vn", "label": "Việt Nam"},
        {"value": "en", "label": "English"}
      ],
      "documentType": [
        {"value": "document", "label": "Document"},
        {"value": "guide", "label": "Guide"}
      ]
    };
    formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "name", "label": "Name", "hintText": "Name input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "title", "label": "Title", "hintText": "Title input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "description", "label": "description", "hintText": "description input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "language", "label": "language", "hintText": "language input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "documentType", "label": "documentType", "hintText": "Type input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "select", "expanded": true, "fieldName": "status", "label": "status", "hintText": "status input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {
              "type": "inputText",
              "expanded": true,
              "fieldName": "content",
              "label": "content",
              "hintText": "content input",
              "maxLines": 10,
            }
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "action", "actionType": actionSubmitType, "actionName": "submit", "actionLabel": submitActionLabel},
            {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
          ]
        }
      ]
    };
    setState(() {});
  }

  docDocumentDetailActionHandler(actionName, formData) async {
    if (docDocument == null) {
      return;
    }
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    if (actionName == "submit") {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      if (widget.docDocumentAction == "create") {
        var docDocumentCreateResponse = await httpPut("/test-framework-api/admin/doc/doc-document", formData, securityModel.authorization);
        if (docDocumentCreateResponse.containsKey("body") && docDocumentCreateResponse["body"] is String == false) {
          var docDocumentCreateResponseBody = docDocumentCreateResponse["body"];
          if (docDocumentCreateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (docDocumentCreateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(docDocumentCreateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.docDocumentAction == "edit") {
        var docDocumentEditResponse =
            await httpPost("/test-framework-api/admin/doc/doc-document/${widget.docDocumentId}", formData, securityModel.authorization);
        if (docDocumentEditResponse.containsKey("body") && docDocumentEditResponse["body"] is String == false) {
          var docDocumentEditResponseBody = docDocumentEditResponse["body"];
          if (docDocumentEditResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (docDocumentEditResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(docDocumentEditResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.docDocumentAction == "delete") {
        var docDocumentDeleteResponse =
            await httpDelete("/test-framework-api/admin/doc/doc-document/${widget.docDocumentId}", securityModel.authorization);
        if (docDocumentDeleteResponse.containsKey("body") && docDocumentDeleteResponse["body"] is String == false) {
          var docDocumentDeleteResponseBody = docDocumentDeleteResponse["body"];
          if (docDocumentDeleteResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (docDocumentDeleteResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(docDocumentDeleteResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
    }
    if (actionName == "back") {
      if (docDocument!["parentId"] != null) {
        navigationModel.pop("/admin/system/doc-document/edit/${docDocument!["parentId"]}");
      } else {
        navigationModel.pop("/admin/system/doc-document");
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    if (error != null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Error'),
          ),
          body: Center(child: Text("Error $error")));
    }
    if (docDocument == null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Loading'),
          ),
          body: const Center(child: CircularProgressIndicator()));
    }
    if (widget.docDocumentAction == "create" || widget.docDocumentAction == "delete") {
      return Scaffold(
        appBar: AppBar(
          title: const Text('Perm detail'),
        ),
        drawer: const Drawer(child: MenuWidget()),
        body: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(mainAxisSize: MainAxisSize.min, children: [
                        Row(
                          children: [
                            RichText(
                                text: TextSpan(children: [
                              TextSpan(
                                  text: "Root",
                                  style: const TextStyle(color: Colors.blue),
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () {
                                      Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/doc-document");
                                    }),
                            ])),
                            for (var docDocumentPath in docDocumentPathList ?? [])
                              RichText(
                                  text: TextSpan(children: [
                                const TextSpan(text: "/"),
                                TextSpan(
                                    text: "${docDocumentPath["name"]}",
                                    style: const TextStyle(color: Colors.blue),
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        Provider.of<NavigationModel>(context, listen: false)
                                            .navigate("/admin/system/doc-document/edit/${docDocumentPath!["id"]}");
                                      }),
                              ]))
                          ],
                        ),
                        Card(
                            child: Column(mainAxisSize: MainAxisSize.min, children: [
                          const Row(
                            children: [
                              Text(
                                "Detail",
                                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          formConfig != null && optionListMap != null
                              ? DynamicForm(
                                  formConfig: formConfig,
                                  formData: docDocument!,
                                  optionListMap: optionListMap,
                                  actionHandler: docDocumentDetailActionHandler)
                              : Container()
                        ])),
                      ])),
                )
              ],
            )),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          title: const Text("Perm detail"),
        ),
        drawer: const Drawer(child: MenuWidget()),
        body: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(mainAxisSize: MainAxisSize.min, children: [
                        Row(
                          children: [
                            RichText(
                                text: TextSpan(children: [
                              TextSpan(
                                  text: "Root",
                                  style: const TextStyle(color: Colors.blue),
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () {
                                      Provider.of<NavigationModel>(context, listen: false).navigate("/admin/system/doc-document");
                                    }),
                            ])),
                            for (var docDocumentPath in docDocumentPathList ?? [])
                              RichText(
                                  text: TextSpan(children: [
                                const TextSpan(text: "/"),
                                TextSpan(
                                    text: "${docDocumentPath["name"]}",
                                    style: const TextStyle(color: Colors.blue),
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        Provider.of<NavigationModel>(context, listen: false)
                                            .navigate("/admin/system/doc-document/edit/${docDocumentPath!["id"]}");
                                      }),
                              ]))
                          ],
                        ),
                        Card(
                            child: Column(mainAxisSize: MainAxisSize.min, children: [
                          const Row(
                            children: [
                              Text(
                                "Detail",
                                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          formConfig != null && optionListMap != null
                              ? DynamicForm(
                                  formConfig: formConfig,
                                  formData: docDocument!,
                                  optionListMap: optionListMap,
                                  actionHandler: docDocumentDetailActionHandler)
                              : Container()
                        ])),
                        Card(
                            child: Column(mainAxisSize: MainAxisSize.min, children: [
                          const Row(
                            children: [
                              Text(
                                "Child",
                                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          AdminDocDocumentListWidget(
                            parentId: docDocument!["id"],
                          ),
                        ])),
                      ])),
                )
              ],
            )),
      );
    }
  }
}
