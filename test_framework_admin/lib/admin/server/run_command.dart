import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_admin/admin/system/menu.dart';
import 'package:test_framework_admin/api.dart';
import 'package:test_framework_admin/common/dynamic_form.dart';
import 'package:test_framework_admin/common/reloadable_state.dart';
import 'package:test_framework_admin/model/model.dart';

class AdminServerRunCommandWidget extends StatefulWidget {
  const AdminServerRunCommandWidget({Key? key}) : super(key: key);

  @override
  State<AdminServerRunCommandWidget> createState() => _AdminServerRunCommandWidgetState();
}

class _AdminServerRunCommandWidgetState extends ReloadableState<AdminServerRunCommandWidget> {
  var runCommandRequest = {};
  var runCommandResult = "";
  var runCommandHistory = [];

  @override
  refreshData() {}

  rptReportActionHandler(actionName, formData) async {
    if (actionName == "run") {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      runCommandRequest = formData;
      var runCommandResponse =
          await httpPost("/admin-server-api/admin/admin-server/run", {"commands": jsonDecode(formData["command"])}, securityModel.authorization);
      setState(() {
        runCommandHistory = runCommandHistory + [formData["command"]];
        runCommandResult = runCommandResponse["body"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var optionListMap = {};
    const formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "command", "label": "command", "hintText": "command input", "maxLines": 5}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "action", "actionName": "run", "actionLabel": "Run"}
          ]
        }
      ]
    };

    return Scaffold(
        appBar: AppBar(
          title: const Text("Run command"),
          actions: [
            IconButton(
                onPressed: () {
                  Provider.of<SecurityModel>(context, listen: false).logout();
                },
                icon: const Icon(Icons.logout))
          ],
        ),
        drawer: const Drawer(child: MenuWidget()),
        body: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 2,
                  child: Column(mainAxisSize: MainAxisSize.max, crossAxisAlignment: CrossAxisAlignment.stretch, children: [
                    Expanded(
                      child: SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: Text(runCommandResult),
                      ),
                    ),
                    DynamicForm(
                        formConfig: formConfig, optionListMap: optionListMap, formData: runCommandRequest, actionHandler: rptReportActionHandler)
                  ]),
                ),
                Expanded(
                  flex: 1,
                  child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                        for (var runCommand in runCommandHistory)
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                runCommandRequest["command"] = runCommand;
                              });
                            },
                            child: Row(
                              children: [
                                Expanded(
                                    child: Container(
                                  padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                                  decoration: const BoxDecoration(
                                      border: Border(
                                          top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
                                          bottom: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)))),
                                  child: Text(runCommand),
                                ))
                              ],
                            ),
                          ),
                      ])),
                )
                // Column(
                //   children: [
                //   ],
                // ),
                // Column(
                //   children: [
                //   ],
                // )
              ],
            )));
  }
}
