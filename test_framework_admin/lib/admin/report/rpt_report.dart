import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_admin/admin/system/menu.dart';
import 'package:test_framework_admin/api.dart';
import 'package:test_framework_admin/common/dynamic_form.dart';
import 'package:test_framework_admin/common/dynamic_table.dart';
import 'package:test_framework_admin/common/reloadable_state.dart';
import 'package:test_framework_admin/model/model.dart';

class AdminRptReportListWidget extends StatefulWidget {
  const AdminRptReportListWidget({Key? key}) : super(key: key);

  @override
  State<AdminRptReportListWidget> createState() => _AdminRptReportListState();
}

class _AdminRptReportListState extends ReloadableState<AdminRptReportListWidget> {
  var rptReportSearchRequest = {};
  var rptReportRowCount = 0;
  var rptReportCurrentPage = 1;
  var rptReportResultList = [];
  var rptReportRowPerPage = 25;
  Widget paging = Container();

  @override
  void initState() {
    super.initState();
    rptReportSearch({});
  }

  @override
  refreshData() {
    rptReportPageChange(rptReportCurrentPage);
  }

  rptReportSearch(rptReportSearchRequest) async {
    this.rptReportSearchRequest = rptReportSearchRequest;
    await rptReportPageChange(1);
  }

  rptReportActionHandler(actionName, formData) async {
    if (actionName == "search") {
      rptReportSearchRequest = formData;
      await rptReportPageChange(1);
    }
  }

  rptReportPageChange(page) async {
    if ((page - 1) * rptReportRowPerPage > rptReportRowCount) {
      page = (1.0 * rptReportRowCount / rptReportRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * rptReportRowPerPage, "queryLimit": rptReportRowPerPage};
    if (rptReportSearchRequest["titleLike"] != null && rptReportSearchRequest["titleLike"].isNotEmpty) {
      findRequest["titleLike"] = "%${rptReportSearchRequest["titleLike"]}%";
    }
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    var response = await httpPost("/test-framework-api/admin/report/rpt-report/search", findRequest, securityModel.authorization);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        rptReportCurrentPage = page;
        rptReportRowCount = response["body"]["rowCount"];
        rptReportResultList = response["body"]["resultList"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var rptReportStatusMap = {1: "Active", 0: "Inactive"};
    var rptReportTypeMap = {"chart": "Chart", "dataTable": "Data table", "dataSheet": "Data sheet", "data": "Data"};
    var optionListMap = {};
    const formConfig = {
      "type": "column",
      "children": [
        {
          "type": "row",
          "children": [
            {"type": "inputText", "expanded": true, "fieldName": "titleLike", "label": "Title", "hintText": "Title input"}
          ]
        },
        {
          "type": "row",
          "children": [
            {"type": "action", "actionName": "search", "actionLabel": "Search"}
          ]
        }
      ]
    };
    var resultColumns = [
      const DataColumn(
        label: Text(
          'Type',
          overflow: TextOverflow.visible,
          softWrap: true,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Category',
          overflow: TextOverflow.visible,
          softWrap: true,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Title',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      const DataColumn(
        label: Text(
          'Status',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      DataColumn(
          label: Row(
        children: [
          ElevatedButton(
            onPressed: () {
              Provider.of<NavigationModel>(context, listen: false).navigate("/admin/report/rpt-report/create");
            },
            child: const Text('Add'),
          )
        ],
      )),
    ];
    var resultRows = [
      for (var rptReport in rptReportResultList)
        DataRow(
          cells: [
            DataCell(Text(rptReportTypeMap[rptReport["reportType"]] ?? "Not defined")),
            DataCell(Text(rptReport["name"] ?? "")),
            DataCell(Text(rptReport["title"] ?? "")),
            DataCell(Text(rptReportStatusMap[rptReport["status"]] ?? "Not defined")),
            DataCell(Row(
              children: [
                ElevatedButton(
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false).navigate("/admin/report/rpt-report/duplicate/${rptReport["id"]}");
                  },
                  child: const Text('Duplicate'),
                ),
                ElevatedButton(
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false).navigate("/admin/report/rpt-report/edit/${rptReport["id"]}");
                  },
                  child: const Text('Edit'),
                ),
                ElevatedButton(
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.red)),
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false).navigate("/admin/report/rpt-report/delete/${rptReport["id"]}");
                  },
                  child: const Text('Delete'),
                ),
              ],
            )),
          ],
        )
    ];

    return Scaffold(
        appBar: AppBar(
          title: const Text("Report list"),
          actions: [
            IconButton(
                onPressed: () {
                  Provider.of<SecurityModel>(context, listen: false).logout();
                },
                icon: const Icon(Icons.logout))
          ],
        ),
        drawer: const Drawer(child: MenuWidget()),
        body: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(mainAxisSize: MainAxisSize.min, children: [
                        DynamicForm(
                            formConfig: formConfig,
                            optionListMap: optionListMap,
                            formData: rptReportSearchRequest,
                            actionHandler: rptReportActionHandler),
                        Row(mainAxisSize: MainAxisSize.max, children: [
                          Expanded(
                              child: DynamicTable(
                            columns: resultColumns,
                            rows: resultRows,
                          ))
                        ]),
                        DynamicTablePagging(
                          rptReportRowCount,
                          rptReportCurrentPage,
                          rptReportRowPerPage,
                          pageChangeHandler: (page) {
                            rptReportPageChange(page);
                          },
                          rowPerPageChangeHandler: (rowPerPage) {
                            rptReportRowPerPage = rowPerPage ?? 10;
                            rptReportPageChange(1);
                          },
                        )
                      ])),
                )
              ],
            )));
  }
}

class AdminRptReportDetailWidget extends StatefulWidget {
  final String? rptReportId;
  final String rptReportAction;
  const AdminRptReportDetailWidget({required this.rptReportAction, this.rptReportId, Key? key}) : super(key: key);

  @override
  State<AdminRptReportDetailWidget> createState() => _AdminRptReportDetailState();
}

class _AdminRptReportDetailState extends ReloadableState<AdminRptReportDetailWidget> {
  dynamic formConfig = {
    "type": "column",
    "children": [
      {
        "type": "row",
        "children": [
          {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
        ]
      }
    ]
  };
  dynamic optionListMap = {};
  Map<dynamic, dynamic>? rptReport;
  String? error;
  var hasEditPermission = false;

  @override
  void initState() {
    super.initState();
    var userPermList = Provider.of<SecurityModel>(context, listen: false).userPermList;
    if (userPermList != null) {
      hasEditPermission = userPermList.where((permCode) => permCode == "ADMIN").isNotEmpty;
    }
    reload();
  }

  @override
  refreshData() {}

  reload() async {
    if (widget.rptReportId != null) {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      var rptReportGetResponse = await httpGet("/test-framework-api/admin/report/rpt-report/${widget.rptReportId}", securityModel.authorization);
      if (rptReportGetResponse.containsKey("body") && rptReportGetResponse["body"] is String == false) {
        rptReport = rptReportGetResponse["body"]["result"];
        if (rptReport!["reportDisplayConfig"] != null && rptReport!["reportDisplayConfig"].isNotEmpty) {
          rptReport!["reportDisplayConfig"] = jsonEncode(rptReport!["reportDisplayConfig"]);
        } else {
          rptReport!["reportDisplayConfig"] = '{}';
        }
        if (rptReport!["reportQueryConfig"] != null && rptReport!["reportQueryConfig"].isNotEmpty) {
          rptReport!["reportQueryConfig"] = jsonEncode(rptReport!["reportQueryConfig"]);
        } else {
          rptReport!["reportQueryConfig"] = '{}';
        }
      }
    } else {
      rptReport = {"status": 0, "reportDisplayConfig": "{}", "reportQueryConfig": "{}"};
    }
    await resetForm();
  }

  resetForm() async {
    setState(() {
      var submitActionLabel = "Submit";
      var actionSubmitType = "primary";
      if (widget.rptReportAction == "delete") {
        submitActionLabel = "Delete";
        actionSubmitType = "danger";
      }
      optionListMap = {
        "status": [
          {"value": 1, "label": "Active"},
          {"value": 0, "label": "Inactive"}
        ],
        "reportType": [
          {"value": "chart", "label": "Chart"},
          {"value": "dataTable", "label": "Data table"},
          {"value": "dataSheet", "label": "Data sheet"},
          {"value": "data", "label": "Data"},
        ],
      };

      formConfig = {
        "type": "column",
        "children": [
          {
            "type": "row",
            "children": [
              {"type": "select", "expanded": true, "fieldName": "reportType", "label": "Type", "hintText": "Type input"}
            ]
          },
          {
            "type": "row",
            "children": [
              {"type": "inputText", "expanded": true, "fieldName": "name", "label": "Name", "hintText": "Name input"}
            ]
          },
          {
            "type": "row",
            "children": [
              {"type": "inputText", "expanded": true, "fieldName": "title", "label": "title", "hintText": "Title input"}
            ]
          },
          {
            "type": "row",
            "children": [
              {"type": "inputText", "expanded": true, "fieldName": "description", "label": "description", "hintText": "Description input"}
            ]
          },
          {
            "type": "row",
            "children": [
              {
                "type": "inputText",
                "maxLines": 5,
                "expanded": true,
                "fieldName": "reportDisplayConfig",
                "label": "reportDisplayConfig",
                "hintText": "Display Config input"
              }
            ]
          },
          {
            "type": "row",
            "children": [
              {
                "type": "inputText",
                "maxLines": 5,
                "expanded": true,
                "fieldName": "reportQueryConfig",
                "label": "reportQueryConfig",
                "hintText": "Query Config input"
              }
            ]
          },
          {
            "type": "row",
            "children": [
              {"type": "select", "expanded": true, "fieldName": "status", "label": "status", "hintText": "status input"}
            ]
          },
          {
            "type": "row",
            "children": [
              hasEditPermission ? {"type": "action", "actionType": actionSubmitType, "actionName": "submit", "actionLabel": submitActionLabel} : {},
              {"type": "action", "actionType": "secondary", "actionName": "back", "actionLabel": "Back"}
            ]
          }
        ]
      };
    });
  }

  actionHandler(actionName, formData) async {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    if (actionName == "submit") {
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      if (widget.rptReportAction == "create") {
        if (formData["reportDisplayConfig"] != null) {
          try {
            formData["reportDisplayConfig"] = jsonDecode(formData["reportDisplayConfig"]);
          } catch (e) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text("$e"),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
        if (formData["reportQueryConfig"] != null) {
          try {
            formData["reportQueryConfig"] = jsonDecode(formData["reportQueryConfig"]);
          } catch (e) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text("$e"),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
        var rptReportCreateResponse = await httpPut("/test-framework-api/admin/report/rpt-report", formData, securityModel.authorization);
        if (rptReportCreateResponse.containsKey("body") && rptReportCreateResponse["body"] is String == false) {
          var rptReportCreateResponseBody = rptReportCreateResponse["body"];
          if (rptReportCreateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (rptReportCreateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(rptReportCreateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.rptReportAction == "duplicate") {
        formData["id"] = null;
        if (formData["reportDisplayConfig"] != null) {
          try {
            formData["reportDisplayConfig"] = jsonDecode(formData["reportDisplayConfig"]);
          } catch (e) {
            if (mounted) {
              showDialog<void>(
                context: context,
                builder: (BuildContext context) => AlertDialog(
                  title: const Text('Error'),
                  content: Text("$e"),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () => Navigator.pop(context),
                      child: const Text('Close'),
                    ),
                  ],
                ),
              );
            }
          }
        }
        if (formData["reportQueryConfig"] != null) {
          try {
            formData["reportQueryConfig"] = jsonDecode(formData["reportQueryConfig"]);
          } catch (e) {
            if (mounted) {
              showDialog<void>(
                context: context,
                builder: (BuildContext context) => AlertDialog(
                  title: const Text('Error'),
                  content: Text("$e"),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () => Navigator.pop(context),
                      child: const Text('Close'),
                    ),
                  ],
                ),
              );
            }
          }
        }
        var rptReportCreateResponse = await httpPut("/test-framework-api/admin/report/rpt-report", formData, securityModel.authorization);
        if (rptReportCreateResponse.containsKey("body") && rptReportCreateResponse["body"] is String == false) {
          var rptReportCreateResponseBody = rptReportCreateResponse["body"];
          if (rptReportCreateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (rptReportCreateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(rptReportCreateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.rptReportAction == "edit") {
        if (formData["reportDisplayConfig"] != null && formData["reportDisplayConfig"].isNotEmpty) {
          try {
            formData["reportDisplayConfig"] = jsonDecode(formData["reportDisplayConfig"]);
          } catch (e) {
            if (mounted) {
              showDialog<void>(
                context: context,
                builder: (BuildContext context) => AlertDialog(
                  title: const Text('Error'),
                  content: Text("$e"),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () => Navigator.pop(context),
                      child: const Text('Close'),
                    ),
                  ],
                ),
              );
            }
            return 0;
          }
        }
        if (formData["reportQueryConfig"] != null && formData["reportQueryConfig"].isNotEmpty) {
          try {
            formData["reportQueryConfig"] = jsonDecode(formData["reportQueryConfig"]);
          } catch (e) {
            if (mounted) {
              showDialog<void>(
                context: context,
                builder: (BuildContext context) => AlertDialog(
                  title: const Text('Error'),
                  content: Text("$e"),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () => Navigator.pop(context),
                      child: const Text('Close'),
                    ),
                  ],
                ),
              );
            }
            return 0;
          }
        }
        var rptReportUpdateResponse =
            await httpPost("/test-framework-api/admin/report/rpt-report/${widget.rptReportId}", formData, securityModel.authorization);
        if (rptReportUpdateResponse.containsKey("body") && rptReportUpdateResponse["body"] is String == false) {
          var rptReportUpdateResponseBody = rptReportUpdateResponse["body"];
          if (rptReportUpdateResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (rptReportUpdateResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(rptReportUpdateResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
      if (widget.rptReportAction == "delete") {
        var rptReportDeleteResponse =
            await httpDelete("/test-framework-api/admin/report/rpt-report/${widget.rptReportId}", securityModel.authorization);
        if (rptReportDeleteResponse.containsKey("body") && rptReportDeleteResponse["body"] is String == false) {
          var rptReportDeleteResponseBody = rptReportDeleteResponse["body"];
          if (rptReportDeleteResponseBody.containsKey("result")) {
            actionName = "back";
          } else if (rptReportDeleteResponseBody.containsKey("errorMessage") && mounted) {
            showDialog<void>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: const Text('Error'),
                content: Text(rptReportDeleteResponseBody["errorMessage"]),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                ],
              ),
            );
          }
        }
      }
    }
    if (actionName == "back") {
      navigationModel.pop("/admin/report/rpt-report");
    }
  }

  @override
  Widget build(BuildContext context) {
    if (error != null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Error'),
          ),
          body: Center(child: Text("Error $error")));
    }
    if (rptReport == null) {
      return Scaffold(
          appBar: AppBar(
            title: const Text('Loading'),
          ),
          body: const Center(child: CircularProgressIndicator()));
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text('Report detail'),
      ),
      drawer: const Drawer(child: MenuWidget()),
      body: Padding(
          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(mainAxisSize: MainAxisSize.min, children: [
                      Card(
                          child: Column(mainAxisSize: MainAxisSize.min, children: [
                        const Row(
                          children: [
                            Text(
                              "Detail",
                              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        formConfig != null && optionListMap != null
                            ? DynamicForm(
                                formConfig: formConfig,
                                formData: rptReport!,
                                optionListMap: optionListMap,
                                actionHandler: actionHandler,
                              )
                            : Container()
                      ]))
                    ])),
              )
            ],
          )),
    );
  }
}
