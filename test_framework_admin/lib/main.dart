import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_admin/model/model.dart';
import 'package:test_framework_admin/router.dart';
import 'package:localstorage/localstorage.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() {
  GoogleFonts.config.allowRuntimeFetching = false;
  runApp(
    const TestFrameworkApp(),
  );
}

class MyCustomScrollBehavior extends MaterialScrollBehavior {
  // Override behavior methods and getters like dragDevices
  @override
  Set<PointerDeviceKind> get dragDevices => {
        PointerDeviceKind.touch,
        PointerDeviceKind.mouse,
        // etc.
      };
}

class TestFrameworkApp extends StatefulWidget {
  const TestFrameworkApp({Key? key}) : super(key: key);
  @override
  State<StatefulWidget> createState() => _TestFrameworkAppState();
}

class _TestFrameworkAppState extends State<TestFrameworkApp> {
  final AppRouterDelegate _routerDelegate = AppRouterDelegate();
  final AppRouteInformationParser _routeInformationParser = AppRouteInformationParser();
  late LocalStorage localStorage;
  late SecurityModel securityModel;
  late NavigationModel navigationModel;

  _TestFrameworkAppState() {
    localStorage = LocalStorage('storage');
    securityModel = SecurityModel(localStorage);
    navigationModel = NavigationModel(localStorage);
  }

  @override
  void initState() {
    super.initState();
    navigationModel.addListener(() async {
      _routerDelegate.updateNavigation(navigationModel);
    });
    securityModel.storage.ready.then((value) {
      securityModel.reload();
      navigationModel.reload();
    });
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => securityModel),
        ChangeNotifierProvider(create: (context) => navigationModel),
        ChangeNotifierProvider(create: (context) => Counter()),
        ChangeNotifierProvider(create: (context) => WebElementModel()),
      ],
      // child: MaterialApp(
      //   title: 'Test Framework',
      //   localizationsDelegates: const [
      //     AppLocalizations.delegate,
      //     GlobalMaterialLocalizations.delegate,
      //     GlobalWidgetsLocalizations.delegate,
      //     GlobalCupertinoLocalizations.delegate,
      //   ],
      //   locale: Provider.of<LocaleProvider>(context, listen: false).locale,
      //   supportedLocales: L10n.all,
      //   scrollBehavior: MyCustomScrollBehavior(),
      //   debugShowCheckedModeBanner: false,
      //   theme: ThemeData(
      //     textTheme: GoogleFonts.poppinsTextTheme(
      //       Theme.of(context).textTheme,
      //     ),
      //   ),
      //   home: const MyHomePageTest(),
      // ),
      child: FutureBuilder(
        future: securityModel.storage.ready,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return MaterialApp.router(
            title: 'Test Framework',
            localizationsDelegates: const [
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
            ],
            scrollBehavior: MyCustomScrollBehavior(),
            routerDelegate: _routerDelegate,
            routeInformationParser: _routeInformationParser,
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
              fontFamily: 'Poppins',
              fontFamilyFallback: const ["Poppins"],
              textTheme: GoogleFonts.poppinsTextTheme(
                Theme.of(context).textTheme,
              ),
            ),
          );
        },
      ),
    );
  }
}
