import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:http/http.dart' as http;
import 'package:test_framework_admin/config.dart';

class SecurityModel extends ChangeNotifier {
  LocalStorage storage;
  bool authenticated = false;
  bool inited = false;
  String? authorization;
  dynamic userLoginCurren;
  List? userPermList;
  dynamic projectPermMap;

  SecurityModel(this.storage);

  reload() async {
    authorization = storage.getItem("authorization");
    if (storage.getItem('server') != null) {
      var server = storage.getItem('server');
      setBaseUrl(server);
    }
    await reloadUserInfo();
    inited = true;
    notifyListeners();
  }

  reloadUserInfo() async {
    if (storage.getItem('server') != null) {
      var server = storage.getItem('server');
      setBaseUrl(server);
    }
    if (authorization != null) {
      authenticated = true;
      try{
      Map<String, String> headers = {
        'content-type': 'application/json',
        'Authorization': authorization!
      };
      var getUserPerm = await http.get(
          Uri.parse('$baseUrl/test-framework-api/auth/user-perm'),
          headers: headers);
      if (getUserPerm.statusCode == 200 &&
          getUserPerm.headers["content-type"] == 'application/json') {
        var userPermGetResponse = json.decode(utf8.decode(getUserPerm.bodyBytes));
        userPermList = userPermGetResponse["resultList"];
      }
      var getProjectPerm = await http.get(
          Uri.parse('$baseUrl/test-framework-api/auth/project-perm'),
          headers: headers);
      if (getProjectPerm.statusCode == 200 &&
          getProjectPerm.headers["content-type"] == 'application/json') {
        var projectPermGetResponse = json.decode(utf8.decode(getProjectPerm.bodyBytes));
        projectPermMap = projectPermGetResponse["result"];
      }

      var response = await http.get(
          Uri.parse('$baseUrl/test-framework-api/auth/user-detail'),
          headers: headers);
      if (response.statusCode == 200 &&
          response.headers["content-type"] == 'application/json') {
        var userLoginGetResponse = json.decode(utf8.decode(response.bodyBytes));
        userLoginCurren = userLoginGetResponse["result"];
      }
      } catch (error) {
        authorization = null;
        authenticated = false;
        userPermList = null;
        projectPermMap = null;
      }
    }
  }

  bool hasUserPermission(String permCode) {
    return (userPermList ?? []).any((userPerm) => userPerm == permCode);
  }

  bool hasProjectPermission(String prjProjectId, String permCode) {
    return ((projectPermMap ?? {})[prjProjectId] ?? [])
        .any((projectPerm) => projectPerm == permCode);
  }

  void logout() async {
    authorization = null;
    authenticated = false;
    userPermList = null;
    projectPermMap = null;
    await storage.setItem("authorization", null);
    notifyListeners();
  }

  void setAuthorization({String? authorization}) async {
    this.authorization = authorization;
    authenticated = authorization != null;
    await storage.setItem("authorization", authorization);
    await reloadUserInfo();
    notifyListeners();
  }
}

class NavigationModel extends ChangeNotifier {
  LocalStorage storage;
  final List<RouterItem> routerItemHistory = [];
  String? prjProjectId;

  NavigationModel(this.storage);

  reload() async {
    prjProjectId = storage.getItem("prjProjectId");
    notifyListeners();
  }

  void navigate(String url) {
    add(RouterItem(key: UniqueKey(), url: url));
  }

  void add(RouterItem routerItem) async {
    final uri = Uri.parse(routerItem.url);
    if (uri.pathSegments.isNotEmpty) {
      if (uri.pathSegments[0] == "project") {
        if (prjProjectId != null && prjProjectId != uri.pathSegments[1]) {
          prjProjectId = uri.pathSegments[1];
          await storage.setItem("prjProjectId", prjProjectId);
        }
      }
    }
    routerItemHistory.add(routerItem);
    notifyListeners();
  }

  void pop(defaultUrl) {
    if (routerItemHistory.isNotEmpty) {
      routerItemHistory.removeLast();
    }
    if (routerItemHistory.isEmpty) {
      navigate(defaultUrl);
    } else {
      routerItemHistory.last.shouldRefresh = true;
      notifyListeners();
    }
  }

  void clear() {
    routerItemHistory.clear();
    notifyListeners();
  }

  void setPrjProject(prjProjectId) async {
    this.prjProjectId = prjProjectId;
    await storage.setItem("prjProjectId", prjProjectId);
    notifyListeners();
  }

  void setPrjProjectWithoutNotify(prjProjectId) async{
    this.prjProjectId = prjProjectId;
    await storage.setItem("prjProjectId", prjProjectId);
  }
}

class WebElementModel extends ChangeNotifier {
  String? selectElementId;
  String? selectElementName;
  var resultAttributeList = [];
  var listFindElementsResult = [];
}

class RouterItem {
  final LocalKey key;
  final String url;
  bool shouldRefresh = false;
  RouterItem({required this.key, required this.url});
}

class Counter extends ChangeNotifier {
  int _counter = 0;
  int get counter => _counter;
  void add(count) {
    _counter = count;
    notifyListeners();
  }
}

class CheckBoxPrivider extends ChangeNotifier {
  int _counterRe = 0;
  int get counterRe => _counterRe;
  void addRe(count) {
    _counterRe = count;
    notifyListeners();
  }

  int _counterOs = 0;
  int get counterOs => _counterOs;
  void addOs(count) {
    _counterOs = count;
    notifyListeners();
  }

  int _counter = 0;
  int get counter => _counter;
  void add(count) {
    _counter = count;
    notifyListeners();
  }

  int _counterBr = 0;
  int get counterBr => _counterBr;
  void addBr(count) {
    _counterBr = count;
    notifyListeners();
  }

  int _total = 0;
  int get total => _total;
  void totalCb(addBr, addRe, addOs) {
    _total = addOs + addRe + addBr;
  }

  bool _valueReset = true;
  bool get valueReset => _valueReset;

  final int _count = 0;
  int get count => _count;

  void reset(bool value, count) {
    _valueReset = true;
    _total = 0;
  }

  final List<String> _listFilterBrowser = [];
  List<String> get listFilterBrowser => _listFilterBrowser;

  void lstFilterBr(String list) {
    _listFilterBrowser.add(list);
  }

  void lstFilterRemoveBr(String list) {
    _listFilterBrowser.remove(list);
  }

  final List<String> _listFilterOs = [];
  List<String> get listFilterOs => _listFilterOs;

  void lstFilterOs(String list) {
    _listFilterOs.add(list);
  }

  void lstFilterRemoveOs(String list) {
    _listFilterOs.remove(list);
  }

  final List<String> _listFilterSolution = [];
  List<String> get listFilterSolution => _listFilterSolution;

  void lstFilterSolution(String list) {
    _listFilterSolution.add(list);
  }

  void lstFilterRemoveSolution(String list) {
    _listFilterSolution.remove(list);
  }

  int _counterBranch = 0;
  int get counterBranch => _counterBranch;
  void addBranch(count) {
    _counterBranch = count;
    notifyListeners();
  }

  int _counterTest = 0;
  int get counterTest => _counterTest;
  void addTest(count) {
    _counterTest = count;
    notifyListeners();
  }

  int _counterStatus = 0;
  int get counterStatus => _counterStatus;
  void addStatus(count) {
    _counterStatus = count;
    notifyListeners();
  }

  int _counterFailure = 0;
  int get counterFailure => _counterFailure;
  void addFailure(count) {
    _counterFailure = count;
    notifyListeners();
  }

  int _counterOwner = 0;
  int get counterOwner => _counterOwner;
  void addOwner(count) {
    _counterOwner = count;
    notifyListeners();
  }
}
