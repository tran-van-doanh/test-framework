import 'dart:convert';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_admin/common/dynamic_form.dart';
import 'package:test_framework_admin/model/model.dart';
import 'package:test_framework_admin/api.dart';
import 'package:crypto/crypto.dart';

import '../../common/dialog_delete.dart';
import '../../config.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  String? server;
  List servers = [];
  var loginInfo = {};
  @override
  void initState() {
    super.initState();
    setInitPage();
  }

  setInitPage() async {
    var storage = Provider.of<SecurityModel>(context, listen: false).storage;
    if (storage.getItem('server') != null && storage.getItem("servers") != null && storage.getItem("servers") is List) {
      server = storage.getItem('server');
    } else {
      server = baseUrl;
      await storage.setItem("servers", [baseUrl]);
    }
    servers = [];
    servers.addAll(storage.getItem("servers"));
    return 0;
  }

  actionHandler(actionName, formData) async {
    if (actionName == "login") {
      setBaseUrl(server!);
      var password = formData["password"];
      if (password != null && password.isNotEmpty) {
        var passwordBytes = utf8.encode(password);
        var passwordDigest = sha256.convert(passwordBytes);
        password = base64.encode(passwordDigest.bytes);
      }
      var loginRequest = {
        "authType": "local",
        "email": formData["email"],
        "password": password,
      };
      SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
      NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
      await securityModel.storage.setItem("server", server);
      var loginResponse = await httpPost("/test-framework-api/auth/login", loginRequest, null);
      if (loginResponse.containsKey("body") && loginResponse["body"] is String == false) {
        var loginResponseBody = loginResponse["body"];
        if (loginResponseBody.containsKey("result")) {
          securityModel.setAuthorization(authorization: loginResponse["body"]["result"]);
          navigationModel.clear();
          navigationModel.navigate("/admin");
        } else if (loginResponseBody.containsKey("errorMessage") && mounted) {
          showDialog<void>(
            context: context,
            builder: (BuildContext context) => AlertDialog(
              title: const Text('Error'),
              content: Text(loginResponseBody["errorMessage"]),
              actions: <Widget>[
                TextButton(
                  onPressed: () => Navigator.pop(context),
                  child: const Text('Close'),
                ),
              ],
            ),
          );
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    var optionListMap = {
      // "server": [
      //   {"https://test-framework.llq.vn"},
      //   {"https://vietin-qa.llq.vn"}
      // ],
    };
    var formConfig = {
      "type": "column",
      "children": [
        // (kIsWeb)
        //     ? {
        //         "type": null,
        //       }
        //     :
        // {
        //   "type": "row",
        //   "children": [
        //     {"type": "select", "expanded": true, "fieldName": "server", "label": "server", "hintText": "Choose a server"}
        //   ]
        // },
        {
          "type": "row",
          "children": [
            {
              "type": "inputText",
              "expanded": true,
              "fieldName": "email",
              "label": "Email",
              "hintText": "Email input",
              "submitActionName": "login",
            }
          ]
        },
        {
          "type": "row",
          "children": [
            {
              "type": "inputText",
              "expanded": true,
              "obscureText": true,
              "fieldName": "password",
              "label": "Password",
              "hintText": "Password input",
              "submitActionName": "login",
            }
          ]
        },
        {"type": "action", "actionType": "primary", "actionName": "login", "actionLabel": "Login"}
      ]
    };
    return Scaffold(
      body: Padding(
          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(mainAxisSize: MainAxisSize.min, children: [
                      Card(
                          child: Column(mainAxisSize: MainAxisSize.min, children: [
                        const Row(
                          children: [
                            Text(
                              "Detail",
                              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        (kIsWeb)
                            ? Container()
                            : Container(
                                padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: DropdownButtonFormField2<dynamic>(
                                        isExpanded: true,
                                        decoration: InputDecoration(
                                          border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(5),
                                          ),
                                          contentPadding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                                          labelStyle: const TextStyle(
                                              fontWeight: FontWeight.bold, color: Color.fromRGBO(0, 0, 0, 0.8), fontSize: 16, letterSpacing: 2),
                                        ),
                                        value: server,
                                        focusColor: Colors.transparent,
                                        icon: const Icon(Icons.keyboard_arrow_down),
                                        iconOnClick: const Icon(Icons.keyboard_arrow_up),
                                        onChanged: (value) {
                                          setState(() {
                                            server = value;
                                          });
                                        },
                                        items: servers.map<DropdownMenuItem<String>>((result) {
                                          return DropdownMenuItem(
                                            value: result,
                                            child: Text(
                                              result,
                                              style: const TextStyle(overflow: TextOverflow.ellipsis),
                                            ),
                                          );
                                        }).toList(),
                                        hint: Text(
                                          server != null ? server.toString() : "Choose a server",
                                          style: const TextStyle(fontWeight: FontWeight.bold, overflow: TextOverflow.ellipsis),
                                        ),
                                        buttonPadding: const EdgeInsets.only(left: 0, right: 10),
                                        buttonHeight: 45,
                                      ),
                                    ),
                                    if (servers.length > 1)
                                      Container(
                                        margin: const EdgeInsets.only(left: 5),
                                        height: 46,
                                        width: 50,
                                        child: ElevatedButton(
                                          style: ElevatedButton.styleFrom(padding: const EdgeInsets.all(5), backgroundColor: Colors.red),
                                          onPressed: () {
                                            showDialog<String>(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return DialogDelete(
                                                  selectedItem: server,
                                                  callback: () async {
                                                    var storage = Provider.of<SecurityModel>(context, listen: false).storage;
                                                    servers.remove(server);
                                                    await storage.setItem("servers", servers);
                                                    setState(() {
                                                      server = servers[0];
                                                    });
                                                  },
                                                  type: 'server',
                                                );
                                              },
                                            );
                                          },
                                          child: const Icon(Icons.delete),
                                        ),
                                      ),
                                    Container(
                                      margin: const EdgeInsets.only(left: 5),
                                      height: 46,
                                      width: 50,
                                      child: ElevatedButton(
                                        style: ElevatedButton.styleFrom(padding: const EdgeInsets.all(5)),
                                        onPressed: () {
                                          showDialog<String>(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return AddServerWidget(
                                                  onClickAdd: () {
                                                    var storage = Provider.of<SecurityModel>(context, listen: false).storage;
                                                    setState(() {
                                                      servers = [];
                                                      servers.addAll(storage.getItem("servers"));
                                                    });
                                                  },
                                                );
                                              });
                                        },
                                        child: const Icon(Icons.add),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                        DynamicForm(formConfig: formConfig, formData: loginInfo, optionListMap: optionListMap, actionHandler: actionHandler)
                      ]))
                    ])),
              )
            ],
          )),
    );
  }
}

class AddServerWidget extends StatefulWidget {
  final VoidCallback onClickAdd;
  const AddServerWidget({Key? key, required this.onClickAdd}) : super(key: key);

  @override
  State<AddServerWidget> createState() => _AddServerWidgetState();
}

class _AddServerWidgetState extends State<AddServerWidget> {
  TextEditingController serverController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    var securityModel = Provider.of<SecurityModel>(context, listen: false);
    return AlertDialog(
      contentPadding: const EdgeInsets.all(0),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Text(
            "Add new serve",
            style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(0, 0, 0, 0.8), fontSize: 16, letterSpacing: 2),
          ),
          TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Icon(
                Icons.close,
                color: Colors.black,
              ))
        ],
      ),
      content: Container(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
        decoration: const BoxDecoration(
          border: Border(
            top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
            bottom: BorderSide(
              color: Color.fromRGBO(216, 218, 229, 1),
            ),
          ),
        ),
        margin: const EdgeInsets.only(top: 20, bottom: 10),
        width: 600,
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    const Expanded(
                      flex: 3,
                      child: Text("Server",
                          style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(0, 0, 0, 0.8), fontSize: 16, letterSpacing: 2)),
                    ),
                    Expanded(
                        flex: 7,
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                          child: TextField(
                            controller: serverController,
                            decoration: const InputDecoration(
                              hintText: "Enter a server",
                              border: OutlineInputBorder(),
                            ),
                          ),
                        )),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
      actionsAlignment: MainAxisAlignment.center,
      actions: [
        SizedBox(
          height: 40,
          width: 120,
          child: ElevatedButton(
            onPressed: () async {
              if (_formKey.currentState!.validate()) {
                List servers = securityModel.storage.getItem("servers");
                if (!servers.contains(serverController.text)) {
                  servers.add(serverController.text);
                  await securityModel.storage.setItem("servers", servers);
                  widget.onClickAdd();
                  if (mounted) {
                    Navigator.of(this.context).pop();
                  }
                } else {
                  showDialog<void>(
                    context: context,
                    builder: (BuildContext context) => AlertDialog(
                      title: const Text('Error'),
                      content: const Text("Server already exists"),
                      actions: <Widget>[
                        TextButton(
                          onPressed: () => Navigator.pop(context),
                          child: const Text('Close'),
                        ),
                      ],
                    ),
                  );
                }
              }
            },
            child: const Text(
              "OK",
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
      ],
    );
  }
}
