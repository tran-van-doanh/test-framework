library api;

import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:test_framework_admin/config.dart';

httpGet(url, String? authorization) async {
  Map<String, String> headers = {'content-type': 'application/json'};
  if (authorization != null) {
    headers["Authorization"] = authorization;
  }
  var response = await http.get(Uri.parse('$baseUrl$url'), headers: headers);
  if (response.headers["content-type"] == 'application/json') {
    try {
      return {"headers": response.headers, "body": json.decode(utf8.decode(response.bodyBytes))};
    } on FormatException {
      //bypass
    }
  }
  return {"headers": response.headers, "body": utf8.decode(response.bodyBytes)};
}

httpPostText(url, requestBody, String? authorization) async {
  Map<String, String> headers = {'content-type': 'text/plain'};
  if (authorization != null) {
    headers["Authorization"] = authorization;
  }
  var response = await http.post(Uri.parse('$baseUrl$url'), headers: headers, body: requestBody);
  if (response.headers["content-type"] == 'application/json') {
    try {
      return {"headers": response.headers, "body": json.decode(utf8.decode(response.bodyBytes))};
    } on FormatException {
      //bypass
    }
  }
  return {"headers": response.headers, "body": utf8.decode(response.bodyBytes)};
}

httpPost(url, requestBody, String? authorization) async {
  Map<String, String> headers = {'content-type': 'application/json'};
  if (authorization != null) {
    headers["Authorization"] = authorization;
  }
  var finalRequestBody = json.encode(requestBody);
  var response = await http.post(Uri.parse('$baseUrl$url'), headers: headers, body: finalRequestBody);
  if (response.headers["content-type"] == 'application/json') {
    try {
      return {"headers": response.headers, "body": json.decode(utf8.decode(response.bodyBytes))};
    } on FormatException {
      //bypass
    }
  }
  return {"headers": response.headers, "body": utf8.decode(response.bodyBytes)};
}

httpPatch(url, requestBody, String? authorization) async {
  Map<String, String> headers = {'content-type': 'application/json'};
  if (authorization != null) {
    headers["Authorization"] = authorization;
  }
  var finalRequestBody = json.encode(requestBody);
  var response = await http.patch(Uri.parse('$baseUrl$url'), headers: headers, body: finalRequestBody);
  if (response.headers["content-type"] == 'application/json') {
    try {
      return {"headers": response.headers, "body": json.decode(utf8.decode(response.bodyBytes))};
    } on FormatException {
      //bypass
    }
  }
  return {"headers": response.headers, "body": utf8.decode(response.bodyBytes)};
}

httpDelete(url, String? authorization) async {
  Map<String, String> headers = {'content-type': 'application/json'};
  if (authorization != null) {
    headers["Authorization"] = authorization;
  }
  try {
    var response = await http.delete(Uri.parse('$baseUrl$url'), headers: headers);
    if (response.headers["content-type"] == 'application/json') {
      try {
        return {"headers": response.headers, "body": json.decode(utf8.decode(response.bodyBytes))};
      } on FormatException {
        //bypass
      }
    }
    return {"headers": response.headers, "body": utf8.decode(response.bodyBytes)};
  } catch (error) {
    return {
      "headers": {},
      "body": {"errorMessage": "Connection error!"}
    };
  }
}

httpPut(url, requestBody, String? authorization) async {
  Map<String, String> headers = {'content-type': 'application/json'};
  if (authorization != null) {
    headers["Authorization"] = authorization;
  }
  var finalRequestBody = json.encode(requestBody);
  var response = await http.put(Uri.parse('$baseUrl$url'), headers: headers, body: finalRequestBody);
  if (response.headers["content-type"] == 'application/json') {
    try {
      return {"headers": response.headers, "body": json.decode(utf8.decode(response.bodyBytes))};
    } on FormatException {
      //bypass
    }
  }
  return {"headers": response.headers, "body": utf8.decode(response.bodyBytes)};
}
