@echo off
cd /d "%~dp0"
if exist "node" rmdir /s /q "node"
for /D %%f in (profile-*) do (
	if exist "%%f\runtime" rmdir /s /q "%%f\runtime"
	copy /y profile-temp\start.cmd "%%f"
	copy /y profile-temp\start-sillent.cmd "%%f"
)