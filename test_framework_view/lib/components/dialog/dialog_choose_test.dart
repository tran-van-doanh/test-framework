import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/custom_state.dart';
import 'package:test_framework_view/components/badge/badge_icon.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';
import 'package:test_framework_view/page/list_test/tests/directory/directory_item_widget.dart';
import 'package:test_framework_view/page/list_test/tests/header/header_list_test_widget.dart';

import '../../common/dynamic_table.dart';
import '../style.dart';

class DialogChooseTest extends StatefulWidget {
  final Function callbackSelectedTestCase;
  final String prjProjectId;
  final String testCaseType;
  const DialogChooseTest({
    Key? key,
    required this.callbackSelectedTestCase,
    required this.prjProjectId,
    required this.testCaseType,
  }) : super(key: key);
  @override
  State<DialogChooseTest> createState() => _DialogChooseTestState();
}

class _DialogChooseTestState extends CustomState<DialogChooseTest> {
  late Future futureListTestCase;
  var listTestCase = [];
  var rowCountListTestCase = 0;
  var currentPageTestCase = 1;
  var rowPerPageTestCase = 10;
  var listDirectory = [];
  dynamic selectedTestCase;
  var findRequestTestCase = {};
  var findRequestDirectory = {};
  bool isViewTypeDirectory = true;
  bool isExpandDirectory = true;
  String? parentDirectoryId;
  List? directoryPathList;
  @override
  void initState() {
    futureListTestCase = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    findRequestDirectory["parentIdIsNull"] = true;
    findRequestTestCase["tfTestDirectoryIdIsNull"] = true;
    await getListTfDirectory(findRequestDirectory);
    await getListTfTestCase(currentPageTestCase, findRequestTestCase);
    return 0;
  }

  getListTfDirectory(findRequest) async {
    var findRequestDirectory = findRequest;
    if (findRequest["titleLike"] != null && findRequest["titleLike"].isNotEmpty) {
      findRequestDirectory["titleLike"] = "%${findRequest["titleLike"]}%";
    }
    findRequestDirectory["directoryType"] = widget.testCaseType;
    findRequestDirectory["prjProjectId"] = widget.prjProjectId;
    var responseDirectory = await httpPost("/test-framework-api/user/test-framework/tf-test-directory/search", findRequestDirectory, context);
    if (responseDirectory.containsKey("body") && responseDirectory["body"] is String == false) {
      setState(() {
        listDirectory = responseDirectory["body"]["resultList"];
      });
    }
    return 0;
  }

  getListTfTestCase(page, findRequest) async {
    if ((page - 1) * rowPerPageTestCase > rowCountListTestCase) {
      page = (1.0 * rowCountListTestCase / rowPerPageTestCase).ceil();
    }
    if (page < 1) {
      page = 1;
    }

    var findRequestTestCase = findRequest;
    if (findRequest["titleLike"] != null && findRequest["titleLike"].isNotEmpty) {
      findRequestTestCase["titleLike"] = "%${findRequest["titleLike"]}%";
    }
    if (findRequest["nameLike"] != null && findRequest["nameLike"].isNotEmpty) {
      findRequestTestCase["nameLike"] = "%${findRequest["nameLike"]}%";
    }
    findRequestTestCase["queryOffset"] = (page - 1) * rowPerPageTestCase;
    findRequestTestCase["queryLimit"] = rowPerPageTestCase;
    findRequestTestCase["prjProjectId"] = widget.prjProjectId;
    findRequestTestCase["testCaseType"] = widget.testCaseType;
    var responseTestCase = await httpPost("/test-framework-api/user/test-framework/tf-test-case/search", findRequestTestCase, context);
    if (responseTestCase.containsKey("body") && responseTestCase["body"] is String == false) {
      setState(() {
        currentPageTestCase = page;
        rowCountListTestCase = responseTestCase["body"]["rowCount"];
        listTestCase = responseTestCase["body"]["resultList"];
      });
    }
    return 0;
  }

  getElementRepositoryPathList() async {
    var directoryPathResponse = await httpGet("/test-framework-api/user/test-framework/tf-test-directory/$parentDirectoryId/path", context);
    if (directoryPathResponse.containsKey("body") && directoryPathResponse["body"] is String == false) {
      directoryPathList = directoryPathResponse["body"]["resultList"];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return TestFrameworkRootPageWidget(
          child: FutureBuilder(
        future: futureListTestCase,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: ListView(children: [
                    headerWidget(context),
                    Container(
                      margin: const EdgeInsets.fromLTRB(50, 0, 50, 20),
                      child: SelectionArea(
                        child: Container(
                          alignment: Alignment.topLeft,
                          child: LayoutBuilder(
                            builder: (BuildContext context, BoxConstraints constraints) {
                              return Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  (isViewTypeDirectory && isExpandDirectory)
                                      ? Padding(padding: const EdgeInsets.only(right: 10), child: tableDirectoryWidget())
                                      : const SizedBox.shrink(),
                                  Expanded(
                                    child: tableTestCaseWidget(),
                                  )
                                ],
                              );
                            },
                          ),
                        ),
                      ),
                    ),
                  ]),
                ),
                btnWidget(context),
              ],
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        },
      ));
    });
  }

  Padding btnWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: 40,
            width: 80,
            child: ElevatedButton(
              onPressed: (selectedTestCase != null)
                  ? () {
                      Navigator.pop(context);
                      widget.callbackSelectedTestCase(selectedTestCase);
                    }
                  : null,
              child: const MultiLanguageText(
                name: "ok",
                defaultValue: "Ok",
                isLowerCase: false,
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 10),
            height: 40,
            width: 120,
            child: ElevatedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              style: ElevatedButton.styleFrom(
                side: const BorderSide(color: Color.fromRGBO(209, 212, 216, 1)),
                backgroundColor: Colors.white,
                foregroundColor: const Color.fromRGBO(61, 90, 254, 1),
              ),
              child: const MultiLanguageText(
                name: "cancel",
                defaultValue: "Cancel",
                isLowerCase: false,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          )
        ],
      ),
    );
  }

  Column tableTestCaseWidget() {
    return Column(
      children: [
        Container(
          constraints: const BoxConstraints(minHeight: 48),
          alignment: Alignment.centerLeft,
          margin: const EdgeInsets.only(bottom: 10),
          decoration: BoxDecoration(
            border: Border.all(
              color: const Color.fromRGBO(212, 212, 212, 1),
            ),
            color: Colors.grey[100],
          ),
          padding: (directoryPathList != null && directoryPathList!.isNotEmpty) ? const EdgeInsets.fromLTRB(3, 3, 12, 3) : const EdgeInsets.all(9),
          child: (directoryPathList != null && directoryPathList!.isNotEmpty)
              ? breadCrumbWidget()
              : Row(
                  children: [
                    if (!isExpandDirectory)
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            isExpandDirectory = true;
                          });
                        },
                        child: const Icon(Icons.arrow_forward),
                      ),
                    MultiLanguageText(
                      name: "test",
                      defaultValue: "Test",
                      style: Style(context).styleTextDataColumn,
                    ),
                  ],
                ),
        ),
        if (listTestCase.isNotEmpty)
          CustomDataTableWidget(
            columns: [
              Expanded(
                child: MultiLanguageText(
                  name: "code",
                  defaultValue: "Code",
                  isLowerCase: false,
                  style: Style(context).styleTextDataColumn,
                ),
              ),
              Expanded(
                flex: 3,
                child: MultiLanguageText(
                  name: "name",
                  defaultValue: "Name",
                  isLowerCase: false,
                  style: Style(context).styleTextDataColumn,
                ),
              ),
              Expanded(
                child: MultiLanguageText(
                  name: "owner",
                  defaultValue: "Owner",
                  isLowerCase: false,
                  style: Style(context).styleTextDataColumn,
                ),
              ),
              Expanded(
                child: Center(
                  child: MultiLanguageText(
                    name: "create_date",
                    defaultValue: "Create date",
                    isLowerCase: false,
                    style: Style(context).styleTextDataColumn,
                  ),
                ),
              ),
              Expanded(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    MultiLanguageText(
                      name: "status",
                      defaultValue: "Status",
                      isLowerCase: false,
                      style: Style(context).styleTextDataColumn,
                    ),
                    Tooltip(
                      message: multiLanguageString(
                          name: "active_draft_review", defaultValue: "Green = Active\nGrey = Draft\nRed = Review ", context: context),
                      child: const Icon(
                        Icons.info,
                        color: Colors.grey,
                        size: 16,
                      ),
                    ),
                  ],
                ),
              ),
            ],
            rows: [
              for (var resultTestCase in listTestCase)
                CustomDataRow(
                  cells: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Row(
                          children: [
                            Radio(
                              value: resultTestCase["id"],
                              groupValue: selectedTestCase,
                              onChanged: (dynamic value) {
                                setState(() {
                                  selectedTestCase = value;
                                });
                              },
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: Text(
                                resultTestCase["name"],
                                style: Style(context).styleTextDataCell,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Text(
                          resultTestCase["title"] ?? "",
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: Style(context).styleTextDataCell,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Text(
                          resultTestCase["sysUser"]["fullname"],
                          style: Style(context).styleTextDataCell,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Center(
                          child: Text(
                            DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultTestCase["createDate"]).toLocal()),
                            style: Style(context).styleTextDataCell,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Center(
                          child: Badge3StatusWidget(
                            status: resultTestCase["status"],
                          ),
                        ),
                      ),
                    ),
                  ],
                  onSelectChanged: () async {
                    setState(() {
                      selectedTestCase = resultTestCase["id"];
                    });
                  },
                ),
            ],
          ),
        DynamicTablePagging(
          rowCountListTestCase,
          currentPageTestCase,
          rowPerPageTestCase,
          pageChangeHandler: (page) {
            setState(() {
              futureListTestCase = getListTfTestCase(page, findRequestTestCase);
            });
          },
          rowPerPageChangeHandler: (rowPerPage) {
            setState(() {
              rowPerPageTestCase = rowPerPage!;
              futureListTestCase = getListTfTestCase(1, findRequestTestCase);
            });
          },
        ),
      ],
    );
  }

  Container tableDirectoryWidget() {
    return Container(
      constraints: const BoxConstraints(maxWidth: 350, maxHeight: 560),
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: const Color.fromRGBO(212, 212, 212, 1),
              ),
              color: Colors.grey[100],
            ),
            padding: const EdgeInsets.all(11),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                MultiLanguageText(
                  name: "directory",
                  defaultValue: "Directory",
                  style: Style(context).styleTextDataColumn,
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      isExpandDirectory = false;
                    });
                  },
                  child: const Icon(Icons.arrow_back),
                ),
              ],
            ),
          ),
          Expanded(
            child: Container(
              color: Colors.grey[100],
              margin: const EdgeInsets.only(top: 10),
              child: ListView(
                padding: const EdgeInsets.only(
                  left: 13,
                ),
                children: [
                  for (var resultDirectory in listDirectory)
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 5),
                      child: DirectoryItemWidget(
                        key: Key(resultDirectory["id"]),
                        testCaseType: widget.testCaseType,
                        prjProjectId: widget.prjProjectId,
                        directory: resultDirectory,
                        parentDirectoryId: parentDirectoryId,
                        onClickDirectory: (id) {
                          setState(() {
                            parentDirectoryId = id;
                          });
                          getElementRepositoryPathList();
                          if (findRequestTestCase.containsKey("tfTestDirectoryIdIsNull")) {
                            findRequestTestCase.remove("tfTestDirectoryIdIsNull");
                          }
                          findRequestTestCase["tfTestDirectoryId"] = parentDirectoryId;
                          getListTfTestCase(currentPageTestCase, findRequestTestCase);
                        },
                      ),
                    ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Row breadCrumbWidget() {
    return Row(
      children: [
        IconButton(
          onPressed: () {
            setState(() {
              parentDirectoryId = null;
              directoryPathList = null;
            });
            if (findRequestTestCase.containsKey("tfTestDirectoryId")) {
              findRequestTestCase.remove("tfTestDirectoryId");
            }
            findRequestTestCase["tfTestDirectoryIdIsNull"] = true;
            getListTfTestCase(currentPageTestCase, findRequestTestCase);
          },
          icon: const FaIcon(FontAwesomeIcons.house, size: 16),
          color: Colors.grey,
          splashRadius: 10,
          tooltip: multiLanguageString(name: "root", defaultValue: "Root", context: context),
        ),
        for (var directoryPath in directoryPathList!)
          RichText(
              text: TextSpan(children: [
            const TextSpan(
              text: " / ",
              style: TextStyle(color: Colors.grey),
            ),
            TextSpan(
                text: "${directoryPath["title"]}",
                style: TextStyle(
                    color: directoryPathList!.last == directoryPath ? Colors.blue : Colors.grey,
                    fontSize: context.fontSizeManager.fontSizeText16,
                    fontWeight: FontWeight.bold),
                recognizer: TapGestureRecognizer()
                  ..onTap = () {
                    setState(() {
                      parentDirectoryId = directoryPath["id"];
                    });
                    getElementRepositoryPathList();
                    if (findRequestTestCase.containsKey("tfTestDirectoryIdIsNull")) {
                      findRequestTestCase.remove("tfTestDirectoryIdIsNull");
                    }
                    findRequestTestCase["tfTestDirectoryId"] = parentDirectoryId;
                    getListTfTestCase(currentPageTestCase, findRequestTestCase);
                  }),
          ]))
      ],
    );
  }

  Padding headerWidget(BuildContext context) {
    Map<String, String> mapTestCaseType = {
      "test_case": multiLanguageString(name: "map_test_case", defaultValue: "Test case", context: context),
      "test_scenario": multiLanguageString(name: "map_test_scenario", defaultValue: "Test scenario", context: context),
      "manual_test": multiLanguageString(name: "map_test_manual", defaultValue: "Test manual", context: context),
      "test_suite": multiLanguageString(name: "map_test_suite", defaultValue: "Test suite", context: context),
      "test_api": multiLanguageString(name: "map_api", defaultValue: "Api", context: context),
      "test_jdbc": multiLanguageString(name: "map_jdbc", defaultValue: "JDBC", context: context),
      "action": multiLanguageString(name: "map_shared_steps", defaultValue: "Shared steps", context: context),
    };
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: HeaderListTestWidget(
              type: isViewTypeDirectory ? "Directory" : mapTestCaseType[widget.testCaseType] ?? "",
              title: mapTestCaseType[widget.testCaseType] ?? "",
              isDirectory: true,
              onChangedDirectory: () {
                onLoading(context);
                setState(() {
                  isViewTypeDirectory = !isViewTypeDirectory;
                });
                if (isViewTypeDirectory) {
                  if (parentDirectoryId != null) {
                    findRequestTestCase["tfTestDirectoryId"] = parentDirectoryId;
                  } else {
                    if (findRequestTestCase.containsKey("tfTestDirectoryId")) {
                      findRequestTestCase.remove("tfTestDirectoryId");
                    }
                    findRequestTestCase["tfTestDirectoryIdIsNull"] = true;
                  }
                } else {
                  if (findRequestTestCase.containsKey("tfTestDirectoryIdIsNull")) {
                    findRequestTestCase.remove("tfTestDirectoryIdIsNull");
                  }
                  if (findRequestTestCase.containsKey("tfTestDirectoryId")) {
                    findRequestTestCase.remove("tfTestDirectoryId");
                  }
                }
                getListTfTestCase(currentPageTestCase, findRequestTestCase);
                if (mounted) {
                  Navigator.pop(this.context);
                }
              },
              callbackFilter: (text) {
                findRequestDirectory.addAll({"titleLike": text["titleLike"]});
                findRequestTestCase.addAll(text);
                getListTfDirectory(findRequestDirectory);
                getListTfTestCase(currentPageTestCase, findRequestTestCase);
              },
            ),
          ),
        ],
      ),
    );
  }
}
