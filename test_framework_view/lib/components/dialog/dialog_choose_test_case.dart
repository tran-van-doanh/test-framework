import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/custom_state.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/components/badge/badge_icon.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';
import 'package:test_framework_view/page/list_test/tests/header/header_list_test_widget.dart';

import '../../common/dynamic_table.dart';
import '../style.dart';

class DialogChooseTestCase extends StatefulWidget {
  final String prjProjectId;
  final Function onCreate;
  const DialogChooseTestCase({
    Key? key,
    required this.prjProjectId,
    required this.onCreate,
  }) : super(key: key);
  @override
  State<DialogChooseTestCase> createState() => _DialogChooseTestCaseState();
}

class _DialogChooseTestCaseState extends CustomState<DialogChooseTestCase> {
  late Future futureListTestCase;
  var listTestCase = [];
  var rowCountListTestCase = 0;
  var currentPageTestCase = 1;
  var rowPerPageTestCase = 10;
  List selectedTestCaseList = [];
  var findRequestTestCase = {};
  List platformList = [];
  @override
  void initState() {
    futureListTestCase = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    await getListPlatform();
    await getListTfTestCase(currentPageTestCase, findRequestTestCase);
    return 0;
  }

  getListPlatform() async {
    var responseDirectory = await httpGet("/test-framework-api/user/sys/sys-organization/platforms", context);
    if (responseDirectory.containsKey("body") && responseDirectory["body"] is String == false) {
      setState(() {
        for (var element in responseDirectory["body"]["resultList"]) {
          Map<String, dynamic> newMap = {"name": element};
          platformList.add(newMap);
        }
      });
    }
  }

  getListTfTestCase(page, findRequest) async {
    if ((page - 1) * rowPerPageTestCase > rowCountListTestCase) {
      page = (1.0 * rowCountListTestCase / rowPerPageTestCase).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequestTestCase = Map.from(findRequest);
    if (findRequest["titleLike"] != null && findRequest["titleLike"].isNotEmpty) {
      findRequestTestCase["titleLike"] = "%${findRequest["titleLike"]}%";
    }
    if (findRequest["nameLike"] != null && findRequest["nameLike"].isNotEmpty) {
      findRequestTestCase["nameLike"] = "%${findRequest["nameLike"]}%";
    }
    findRequestTestCase["queryOffset"] = (page - 1) * rowPerPageTestCase;
    findRequestTestCase["queryLimit"] = rowPerPageTestCase;
    findRequestTestCase["prjProjectId"] = widget.prjProjectId;
    findRequestTestCase["testCaseType"] = "test_case";
    findRequestTestCase["status"] = 1;
    var responseTestCase = await httpPost("/test-framework-api/user/test-framework/tf-test-case/search", findRequestTestCase, context);
    if (responseTestCase.containsKey("body") && responseTestCase["body"] is String == false) {
      setState(() {
        currentPageTestCase = page;
        rowCountListTestCase = responseTestCase["body"]["rowCount"];
        listTestCase = responseTestCase["body"]["resultList"];
      });
    }
    return 0;
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return TestFrameworkRootPageWidget(
          child: FutureBuilder(
        future: futureListTestCase,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: ListView(
                    padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
                    children: [
                      Theme(
                        data: ThemeData().copyWith(dividerColor: Colors.transparent),
                        child: ExpansionTile(
                          initiallyExpanded: true,
                          tilePadding: const EdgeInsets.all(0),
                          childrenPadding: const EdgeInsets.all(10),
                          title: MultiLanguageText(
                            name: "choose_profile_for_the_platform",
                            defaultValue: "Choose profile for the platform",
                            style: TextStyle(fontSize: context.fontSizeManager.fontSizeText24, fontWeight: FontWeight.w500),
                          ),
                          children: <Widget>[
                            for (int i = 0; i < (platformList.length / 2).ceil(); i++)
                              Row(
                                children: [
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        SelectProfileByPlatform(platform: platformList[i * 2]),
                                        const SizedBox(
                                          height: 10,
                                        )
                                      ],
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  Expanded(
                                    child: (i * 2 + 1 < platformList.length)
                                        ? Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              SelectProfileByPlatform(platform: platformList[i * 2 + 1]),
                                              const SizedBox(
                                                height: 10,
                                              )
                                            ],
                                          )
                                        : const SizedBox.shrink(),
                                  ),
                                ],
                              ),
                          ],
                        ),
                      ),
                      headerWidget(context),
                      Container(
                        margin: const EdgeInsets.only(top: 20),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                                child: tableTestCaseWidget(
                                    multiLanguageString(name: "list_test_case_search", defaultValue: "List Test Case Search", context: context),
                                    "List Test Case Search",
                                    listTestCase,
                                    selectedTestCaseList)),
                            const SizedBox(
                              width: 10,
                            ),
                            Expanded(
                                child: tableTestCaseWidget(
                                    multiLanguageString(name: "list_test_case", defaultValue: "List Test Case", context: context),
                                    "List Test Case",
                                    selectedTestCaseList,
                                    listTestCase))
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                btnWidget(context),
              ],
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        },
      ));
    });
  }

  Padding btnWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: 40,
            width: 80,
            child: ElevatedButton(
              onPressed: () {
                var requestBody = {
                  "platformProfileMap": {for (var platform in platformList) platform["name"]: platform["tfTestProfileId"]},
                  "tfTestCaseIdList": [for (var testcase in selectedTestCaseList) testcase["id"]]
                };
                widget.onCreate(requestBody);
              },
              child: const MultiLanguageText(
                name: "ok",
                defaultValue: "Ok",
                isLowerCase: false,
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 10),
            height: 40,
            width: 120,
            child: ElevatedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              style: ElevatedButton.styleFrom(
                side: const BorderSide(color: Color.fromRGBO(209, 212, 216, 1)),
                backgroundColor: Colors.white,
                foregroundColor: const Color.fromRGBO(61, 90, 254, 1),
              ),
              child: const MultiLanguageText(
                name: "cancel",
                defaultValue: "Cancel",
                isLowerCase: false,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          )
        ],
      ),
    );
  }

  Column tableTestCaseWidget(String title, String label, List list, List list1) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title,
              style: Style(context).styleTitleDialog,
            ),
            (label == "List Test Case Search")
                ? ElevatedButton(
                    onPressed: () async {
                      var requestBody = Map.from(findRequestTestCase);
                      if (findRequestTestCase["titleLike"] != null && findRequestTestCase["titleLike"].isNotEmpty) {
                        requestBody["titleLike"] = "%${findRequestTestCase["titleLike"]}%";
                      }
                      if (findRequestTestCase["nameLike"] != null && findRequestTestCase["nameLike"].isNotEmpty) {
                        requestBody["nameLike"] = "%${findRequestTestCase["nameLike"]}%";
                      }
                      requestBody["prjProjectId"] = widget.prjProjectId;
                      requestBody["testCaseType"] = "test_case";
                      requestBody["status"] = 1;
                      var responseTestCase = await httpPost("/test-framework-api/user/test-framework/tf-test-case/search", requestBody, context);
                      if (responseTestCase.containsKey("body") && responseTestCase["body"] is String == false) {
                        setState(() {
                          selectedTestCaseList = responseTestCase["body"]["resultList"];
                        });
                      }
                    },
                    child: const MultiLanguageText(name: "add_all", defaultValue: "Add all"),
                  )
                : ElevatedButton(
                    onPressed: (selectedTestCaseList.isNotEmpty)
                        ? () {
                            setState(() {
                              selectedTestCaseList = [];
                            });
                          }
                        : null,
                    child: const MultiLanguageText(name: "remove_all", defaultValue: "Remove all"),
                  ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        CustomDataTableWidget(
          minWidth: 500,
          columns: [
            Expanded(
              child: MultiLanguageText(
                name: "code",
                defaultValue: "Code",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "name",
                defaultValue: "Name",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: Center(
                child: MultiLanguageText(
                  name: "status",
                  defaultValue: "Status",
                  isLowerCase: false,
                  style: Style(context).styleTextDataColumn,
                ),
              ),
            ),
            Container(
              width: 56,
            ),
          ],
          rows: [
            for (var item in list)
              CustomDataRow(
                cells: [
                  Expanded(
                    child: Text(
                      item["name"],
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                  Expanded(
                    child: Text(
                      item["title"],
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                  Expanded(
                    child: Center(
                      child: Badge3StatusWidget(
                        status: item["status"],
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 56,
                    height: 35,
                    child: (label == "List Test Case Search" &&
                            !list1.any(
                              (element) => element["id"] == item["id"],
                            ))
                        ? ElevatedButton(
                            onPressed: () {
                              setState(() {
                                list1.add(item);
                              });
                            },
                            child: const Icon(Icons.add),
                          )
                        : (label == "List Test Case")
                            ? ElevatedButton(
                                onPressed: () {
                                  setState(() {
                                    list.remove(item);
                                  });
                                },
                                child: const Icon(Icons.remove),
                              )
                            : const SizedBox.shrink(),
                  ),
                ],
              ),
          ],
        ),
        if (label == "List Test Case Search")
          DynamicTablePagging(
            rowCountListTestCase,
            currentPageTestCase,
            rowPerPageTestCase,
            pageChangeHandler: (page) {
              getListTfTestCase(page, findRequestTestCase);
            },
            rowPerPageChangeHandler: (rowPerPage) {
              setState(() {
                rowPerPageTestCase = rowPerPage!;
                getListTfTestCase(1, findRequestTestCase);
              });
            },
          ),
      ],
    );
  }

  Row headerWidget(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: HeaderListTestWidget(
            type: multiLanguageString(name: "map_test_case_batch_step", defaultValue: "Test case batch step", context: context),
            title: multiLanguageString(name: "map_test_case", defaultValue: "Test case", context: context),
            callbackFilter: (text) {
              findRequestTestCase = text;
              getListTfTestCase(currentPageTestCase, findRequestTestCase);
            },
          ),
        ),
      ],
    );
  }
}

class SelectProfileByPlatform extends StatefulWidget {
  final dynamic platform;
  const SelectProfileByPlatform({Key? key, this.platform}) : super(key: key);

  @override
  State<SelectProfileByPlatform> createState() => _SelectProfileByPlatformState();
}

class _SelectProfileByPlatformState extends CustomState<SelectProfileByPlatform> {
  List profileList = [];
  @override
  void initState() {
    super.initState();
    getInitPage();
  }

  getInitPage() async {
    getProfileList(widget.platform);
    getProfileDefault(widget.platform);
  }

  getProfileList(Map<String, dynamic> platform) async {
    var findRequest = {"prjProjectId": Provider.of<NavigationModel>(context, listen: false).prjProjectId, "status": 1, "platform": platform["name"]};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-profile/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        profileList = response["body"]["resultList"];
      });
    }
  }

  getProfileDefault(Map<String, dynamic> platform) async {
    var response = await httpGet(
        "/test-framework-api/user/project/prj-project/${Provider.of<NavigationModel>(context, listen: false).prjProjectId}/tf-test-profile/${platform["name"]}/default",
        context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        platform["tfTestProfileId"] = response["body"]["result"]["id"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return DynamicDropdownButton(
      value: widget.platform["tfTestProfileId"],
      hint: multiLanguageString(name: "choose_a_profile", defaultValue: "Choose a profile", context: context),
      items: profileList.map<DropdownMenuItem<String>>((dynamic result) {
        return DropdownMenuItem(
          value: result["id"],
          child: Text(result["title"]),
        );
      }).toList(),
      onChanged: (newValue) {
        setState(() {
          widget.platform["tfTestProfileId"] = newValue!;
        });
      },
      labelText:
          multiLanguageString(name: "platform_name", defaultValue: "Platform \$0", variables: ["${widget.platform["name"]}"], context: context),
      borderRadius: 5,
    );
  }
}
