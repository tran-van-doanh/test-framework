import 'package:flutter/material.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';

class DialogConfirm extends StatelessWidget {
  final Function callback;
  const DialogConfirm({Key? key, required this.callback}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: const EdgeInsets.all(0),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "warning",
            defaultValue: "Warning",
            isLowerCase: false,
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
              onPressed: () => Navigator.pop(context, 'Cancel'),
              child: const Icon(
                Icons.close,
                color: Colors.black,
              ))
        ],
      ),
      content: Container(
        decoration: const BoxDecoration(
            border: Border(top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)), bottom: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)))),
        margin: const EdgeInsets.only(top: 20, bottom: 10),
        width: 500,
        padding: const EdgeInsets.all(20),
        child: IntrinsicHeight(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const MultiLanguageText(name: "are_you_sure", defaultValue: "Are you sure ?"),
              Container(
                margin: const EdgeInsets.only(top: 20),
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(228, 115, 87, 1),
                  borderRadius: BorderRadius.circular(5),
                ),
                padding: const EdgeInsets.only(left: 5),
                child: Container(
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.horizontal(
                      right: Radius.circular(5),
                    ),
                    color: Color.fromRGBO(255, 233, 217, 1),
                  ),
                  padding: const EdgeInsets.all(15),
                  child: const Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Icon(Icons.warning, color: Color.fromRGBO(243, 86, 40, 1)),
                      SizedBox(width: 10),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          MultiLanguageText(
                            name: "warning",
                            defaultValue: "Warning",
                            style: TextStyle(color: Color.fromARGB(255, 206, 20, 20), fontWeight: FontWeight.bold),
                          ),
                          SizedBox(height: 10),
                          MultiLanguageText(
                            name: "cant_undo_action",
                            defaultValue: "You can't undo this action.",
                            style: TextStyle(color: Color.fromARGB(255, 187, 103, 82)),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      actionsAlignment: MainAxisAlignment.center,
      actions: [
        Container(
          margin: const EdgeInsets.only(right: 5),
          height: 40,
          width: 120,
          child: ElevatedButton(
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(
                const Color.fromRGBO(225, 46, 59, 1),
              ),
            ),
            onPressed: () {
              callback();
              Navigator.of(context).pop();
            },
            child: const MultiLanguageText(
              name: "ok",
              defaultValue: "OK",
              isLowerCase: false,
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
        const ButtonCancel()
      ],
    );
  }
}
