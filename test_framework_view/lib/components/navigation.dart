import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/model/model.dart';

import '../common/custom_state.dart';

class NavigationWidget extends StatefulWidget {
  const NavigationWidget({Key? key}) : super(key: key);

  @override
  State<NavigationWidget> createState() => _NavigationWidgetState();
}

class _NavigationWidgetState extends CustomState<NavigationWidget> {
  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(
      builder: (context, languageModel, child) {
        SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: true);
        NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: true);
        List menuItemList = [];
        List listCheck = [];
        if (navigationModel.product == "qa-platform") {
          listCheck = [
            {
              "icon": Icons.insights,
              "title": multiLanguageString(name: "nav_insights", defaultValue: "Insights", context: context),
              "url": "/qa-platform/project/${navigationModel.prjProjectId}/insights/dashboard",
              "urlPrefix": ["/qa-platform/project/${navigationModel.prjProjectId}/insights"],
            },
            {
              "icon": Icons.folder_open,
              "title": multiLanguageString(name: "nav_element_repository", defaultValue: "Element Repository", context: context),
              "url": "/qa-platform/project/${navigationModel.prjProjectId}/element-repository",
              "urlPrefix": ["/qa-platform/project/${navigationModel.prjProjectId}/element-repository"],
              "projectPermission": "PROJECT_TEST_ELEMENT_REPOSITORY",
            },
            {
              "icon": Icons.list_alt,
              "title": multiLanguageString(name: "nav_test_lib", defaultValue: "Test Lib", context: context),
              "url": "/qa-platform/project/${navigationModel.prjProjectId}/test-list/dashboard",
              "urlPrefix": ["/qa-platform/project/${navigationModel.prjProjectId}/test-list"],
              "projectPermission": "PROJECT_TEST_CASE",
            },
            {
              "icon": Icons.smart_display,
              "title": multiLanguageString(name: "nav_runs", defaultValue: "Runs", context: context),
              "url": "/qa-platform/project/${navigationModel.prjProjectId}/runs/dashboard",
              "urlPrefix": ["/qa-platform/project/${navigationModel.prjProjectId}/runs"],
              "projectPermission": "PROJECT_TEST_CASE",
            },
            {
              "icon": Icons.delete,
              "title": multiLanguageString(name: "nav_delete_data", defaultValue: "Delete Data", context: context),
              "url": "/qa-platform/project/${navigationModel.prjProjectId}/delete-data/test_case",
              "urlPrefix": ["/qa-platform/project/${navigationModel.prjProjectId}/delete-data"],
              "projectPermission": "PROJECT_TEST_RUN_DELETE",
            },
            {
              "icon": Icons.file_copy,
              "title": multiLanguageString(name: "nav_file", defaultValue: "File", context: context),
              "url": "/qa-platform/project/${navigationModel.prjProjectId}/file",
              "urlPrefix": ["/qa-platform/project/${navigationModel.prjProjectId}/file"],
              "projectPermission": "PROJECT_FILE",
            },
            {
              "icon": Icons.description,
              "title": multiLanguageString(name: "nav_data_directory", defaultValue: "Data Directory", context: context),
              "url": "/qa-platform/project/${navigationModel.prjProjectId}/data-directory",
              "urlPrefix": ["/qa-platform/project/${navigationModel.prjProjectId}/data-directory"],
              "projectPermission": "PROJECT_DATA_DIRECTORY",
              // "module": "DATA_DIRECTORY",
            },
            {
              "icon": Icons.settings,
              "title": multiLanguageString(name: "nav_setting", defaultValue: "Setting", context: context),
              "url": "/qa-platform/project/${navigationModel.prjProjectId}/setting/teammate",
              "urlPrefix": ["/qa-platform/project/${navigationModel.prjProjectId}/setting"],
              "projectPermission": "PROJECT_SETTINGS",
            },
          ];
        } else if (navigationModel.product == "work-mgt") {
          listCheck = [
            {
              "icon": Icons.summarize,
              "title": multiLanguageString(name: "nav_summary", defaultValue: "Summary", context: context),
              "url": "/work-mgt/project/${navigationModel.prjProjectId}/summary",
              "urlPrefix": ["/work-mgt/project/${navigationModel.prjProjectId}/summary"]
            },
            {
              "icon": Icons.dashboard,
              "title": multiLanguageString(name: "nav_board", defaultValue: "Board", context: context),
              "url": "/work-mgt/project/${navigationModel.prjProjectId}/board",
              "urlPrefix": ["/work-mgt/project/${navigationModel.prjProjectId}/board"]
            },
            {
              "icon": Icons.list,
              "title": multiLanguageString(name: "nav_task", defaultValue: "Task", context: context),
              "url": "/work-mgt/project/${navigationModel.prjProjectId}/task-list",
              "urlPrefix": ["/work-mgt/project/${navigationModel.prjProjectId}/task-list"]
            },
            {
              "icon": Icons.sort,
              "title": multiLanguageString(name: "nav_timeline", defaultValue: "Timeline", context: context),
              "url": "/work-mgt/project/${navigationModel.prjProjectId}/timeline",
              "urlPrefix": ["/work-mgt/project/${navigationModel.prjProjectId}/timeline"]
            },
            {
              "icon": Icons.check_circle,
              "title": multiLanguageString(name: "nav_active_sprint", defaultValue: "Active Sprint", context: context),
              "url": "/work-mgt/project/${navigationModel.prjProjectId}/active-sprint",
              "urlPrefix": ["/work-mgt/project/${navigationModel.prjProjectId}/active-sprint"]
            },
            {
              "icon": Icons.settings,
              "title": multiLanguageString(name: "nav_settings", defaultValue: "Settings", context: context),
              "url": "/work-mgt/project/${navigationModel.prjProjectId}/settings/tag",
              "urlPrefix": ["/work-mgt/project/${navigationModel.prjProjectId}/settings"]
            },
          ];
        } else if (navigationModel.product == "resource-pool") {
          listCheck = [
            {
              "icon": Icons.insights,
              "title": multiLanguageString(name: "nav_insights", defaultValue: "Insights", context: context),
              "url": "/resource-pool/insights",
              "urlPrefix": ["/resource-pool/insights"],
            },
            {
              "icon": Icons.account_circle,
              "title": multiLanguageString(name: "nav_user", defaultValue: "User", context: context),
              "url": "/resource-pool/user/user",
              "urlPrefix": ["/resource-pool/user"],
            },
            {
              "icon": Icons.devices,
              "title": multiLanguageString(name: "nav_device", defaultValue: "Device", context: context),
              "url": "/resource-pool/device/device",
              "urlPrefix": ["/resource-pool/device"],
            },
            {
              "icon": Icons.settings,
              "title": multiLanguageString(name: "nav_project", defaultValue: "Project", context: context),
              "url": "/resource-pool/project",
              "urlPrefix": ["/resource-pool/project"],
            },
          ];
        } else if (navigationModel.product == "admin") {
          listCheck = [
            {
              "icon": Icons.dashboard,
              "title": multiLanguageString(name: "nav_dashboard", defaultValue: "Dashboard", context: context),
              "url": "/admin/dashboard",
              "urlPrefix": ["/admin/dashboard"],
            },
            {
              "icon": Icons.query_stats,
              "title": multiLanguageString(name: "nav_chart", defaultValue: "Chart", context: context),
              "url": "/admin/chart",
              "urlPrefix": ["/admin/chart"],
            },
            {
              "icon": Icons.settings,
              "title": multiLanguageString(name: "nav_config", defaultValue: "Config", context: context),
              "url": "/admin/config",
              "urlPrefix": ["/admin/config"],
              "permission": "ADMIN_CONFIG"
            },
          ];
        } else if (navigationModel.product == "workflow") {
          listCheck = [
            {
              "icon": Icons.workspace_premium,
              "title": multiLanguageString(name: "nav_wf_status", defaultValue: "Status", context: context),
              "url": "/workflow/status",
              "urlPrefix": ["/workflow/status"],
            },
            {
              "icon": Icons.rebase_edit,
              "title": multiLanguageString(name: "nav_flow", defaultValue: "Flow", context: context),
              "url": "/workflow/flow",
              "urlPrefix": ["/workflow/flow"],
            },
          ];
        } else if (navigationModel.product == "prj-360") {
          listCheck = [
            {
              "icon": Icons.dashboard,
              "title": multiLanguageString(name: "nav_dashboard", defaultValue: "Dashboard", context: context),
              "url": "/prj-360/dashboard",
              "urlPrefix": ["/prj-360/dashboard"],
            },
            {
              "icon": Icons.account_tree,
              "title": multiLanguageString(name: "nav_resource_pool", defaultValue: "Resource pool", context: context),
              "url": "/prj-360/resource-pool",
              "urlPrefix": ["/prj-360/resource-pool"],
            },
            {
              "icon": Icons.hdr_auto,
              "title": multiLanguageString(name: "nav_qa_platform", defaultValue: "QA Platform", context: context),
              "url": "/prj-360/qa-platform",
              "urlPrefix": ["/prj-360/qa-platform"],
            },
            {
              "icon": Icons.engineering,
              "title": multiLanguageString(name: "nav_work_management", defaultValue: "Work management", context: context),
              "url": "/prj-360/work-mgt",
              "urlPrefix": ["/prj-360/work-mgt"],
            },
          ];
        }

        for (var navigation in listCheck) {
          if (securityModel.hasProjectPermission(navigationModel.prjProjectId ?? "", navigation["projectPermission"]) &&
              securityModel.hasUserPermission(navigation["permission"]) &&
              securityModel.hasSysOrganizationModules(navigation["module"])) menuItemList.add(navigation);
        }
        if (menuItemList.length < 2) {
          return Container();
        }
        var newSelectedItem = 0;
        if (navigationModel.routerItemHistory.isNotEmpty) {
          String url = navigationModel.routerItemHistory.last.url;
          var currentIndex = 0;
          for (var menuItem in menuItemList) {
            for (var urlPrefix in (menuItem["urlPrefix"]) as Iterable) {
              if (url.contains(urlPrefix)) {
                newSelectedItem = currentIndex;
              }
              currentIndex++;
            }
          }
        }

        return LayoutBuilder(
          builder: (context, constraint) {
            return SingleChildScrollView(
              child: IntrinsicHeight(
                  child: Container(
                width: 75,
                constraints: BoxConstraints(minHeight: constraint.maxHeight),
                child: NavigationRail(
                  selectedIndex: newSelectedItem,
                  onDestinationSelected: (value) {
                    if (value > 0 && value < menuItemList.length) {
                      navigationModel.navigate("${menuItemList[value]["url"]}");
                    } else {
                      if (navigationModel.product == "qa-platform") {
                        navigationModel.navigate("/qa-platform/project/${navigationModel.prjProjectId}/insights/dashboard");
                      } else if (navigationModel.product == "resource-pool") {
                        navigationModel.navigate("/resource-pool/insights");
                      } else if (navigationModel.product == "admin") {
                        navigationModel.navigate("/admin/dashboard");
                      } else if (navigationModel.product == "workflow") {
                        navigationModel.navigate("/workflow/status");
                      } else if (navigationModel.product == "prj-360") {
                        navigationModel.navigate("/prj-360/dashboard");
                      } else {
                        navigationModel.navigate("/${navigationModel.product}/project/${navigationModel.prjProjectId}/summary");
                      }
                    }
                  },
                  backgroundColor: const Color.fromRGBO(43, 54, 73, 1),
                  selectedIconTheme: const IconThemeData(color: Color.fromRGBO(74, 119, 255, 1), opacity: 1),
                  unselectedIconTheme: const IconThemeData(color: Colors.white, opacity: 1),
                  labelType: NavigationRailLabelType.none,
                  destinations: [
                    for (var menuItem in menuItemList)
                      if (securityModel.hasProjectPermission(navigationModel.prjProjectId ?? "", menuItem["projectPermission"]) &&
                          securityModel.hasSysOrganizationModules(menuItem["module"]))
                        NavigationRailDestination(
                          icon: Container(
                            margin: const EdgeInsets.all(15),
                            child: Tooltip(
                              message: "${menuItem["title"] ?? ""}",
                              child: Icon(menuItem["icon"] as IconData),
                            ),
                          ),
                          selectedIcon: !kIsWeb && Platform.isIOS
                              ? IconButton(
                                  icon: Icon(menuItem["icon"] as IconData),
                                  onPressed: () {},
                                  tooltip: "${menuItem["title"] ?? ""}",
                                )
                              : Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                    gradient: const LinearGradient(
                                        begin: Alignment.topLeft,
                                        end: Alignment.bottomRight,
                                        colors: [Color.fromRGBO(112, 147, 255, 1), Color.fromRGBO(61, 90, 254, 1)]),
                                  ),
                                  child: IconButton(
                                    icon: Icon(menuItem["icon"] as IconData, color: Colors.white),
                                    onPressed: () {},
                                    tooltip: "${menuItem["title"] ?? ""}",
                                  ),
                                ),
                          label: Text("${menuItem["title"] ?? ""}"),
                        ),
                  ],
                ),
              )),
            );
          },
        );
      },
    );
  }
}
