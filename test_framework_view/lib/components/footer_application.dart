import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class FooterApplicationWidget extends StatelessWidget {
  final String productName;
  const FooterApplicationWidget({Key? key, required this.productName}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 20),
      color: const Color(0xffD5DDEB),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Text(
            "2023 @ LLQ",
            style: TextStyle(color: Color(0xff98a6ad)),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                "Read our ",
                style: TextStyle(color: Color(0xff98a6ad)),
              ),
              InkWell(
                onTap: () {
                  launchUrl(
                    Uri.parse("/#/qa-platform/docs/login_platform"),
                    webOnlyWindowName: kIsWeb ? '_blank' : '_self',
                  );
                },
                child: const Text(
                  "docs",
                  style: TextStyle(color: Colors.blue),
                ),
              ),
              Text(' to learn about $productName', style: const TextStyle(color: Color(0xff98a6ad))),
            ],
          ),
        ],
      ),
    );
  }
}
