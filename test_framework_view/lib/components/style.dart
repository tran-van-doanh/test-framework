import 'package:flutter/material.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';

class Style {
  final BuildContext context;
  Style(this.context);
  BorderSide get styleBorderTable {
    return const BorderSide(width: 1, color: Color.fromRGBO(216, 218, 229, 1), style: BorderStyle.solid);
  }

  TextStyle get styleTextDataColumn {
    return TextStyle(
      color: const Color.fromRGBO(133, 135, 145, 1),
      fontSize: context.fontSizeManager.fontSizeText14,
      fontWeight: FontWeight.bold,
      letterSpacing: 2,
    );
  }

  TextStyle get styleTextDataCell {
    return TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1), decoration: TextDecoration.none);
  }

  TextStyle get styleTitleDialog {
    return TextStyle(
        fontWeight: FontWeight.bold, color: const Color.fromRGBO(0, 0, 0, 0.8), fontSize: context.fontSizeManager.fontSizeText16, letterSpacing: 2);
  }

  TextStyle get textDragAndDropWidget {
    return TextStyle(
        color: Colors.white,
        fontSize: context.fontSizeManager.fontSizeText14,
        fontWeight: FontWeight.bold,
        letterSpacing: 1.5,
        decoration: TextDecoration.none);
  }

  TextStyle get textDescriptionWidget {
    return TextStyle(color: Colors.white, fontSize: context.fontSizeManager.fontSizeText12, decoration: TextDecoration.none);
  }
}
