import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';

import 'dynamic_button.dart';

class ToastWidget extends StatelessWidget {
  final Color color;
  final Icon icon;
  final String msg;
  const ToastWidget({Key? key, required this.msg, required this.color, required this.icon}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(maxWidth: MediaQuery.of(context).size.width - 40),
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        border: Border.all(color: const Color.fromRGBO(255, 129, 130, 0.4)),
        color: color,
      ),
      child: IntrinsicWidth(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            icon,
            const SizedBox(
              width: 12.0,
            ),
            Expanded(child: Text(msg)),
          ],
        ),
      ),
    );
  }
}

void onLoading(context) {
  showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Center(child: CircularProgressIndicator()),
          Container(
            margin: const EdgeInsets.only(top: 10),
            child: Center(
                child: Column(
              children: [
                MultiLanguageText(
                  name: "processing",
                  defaultValue: "Processing",
                  style: TextStyle(color: Colors.white, fontSize: context.fontSizeManager.fontSizeText16, decoration: TextDecoration.underline),
                ),
                const SizedBox(
                  height: 20,
                ),
                const ButtonCancel()
              ],
            )),
          )
        ],
      );
    },
  );
}

void showToast({required BuildContext context, required String msg, required Color color, required Icon icon}) {
  FToast fToast = FToast();

  fToast.init(context);
  return fToast.showToast(
    child: ToastWidget(msg: msg, color: color, icon: icon),
    gravity: ToastGravity.BOTTOM,
    toastDuration: const Duration(seconds: 3),
    positionedToastBuilder: (context, child) {
      return Positioned(
        top: 60.0,
        right: 20.0,
        child: child,
      );
    },
  );
}
