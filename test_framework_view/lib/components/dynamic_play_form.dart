import 'dart:convert';
import 'dart:io';
import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/badge/badge_icon.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/config.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:http/http.dart' as http;
import 'package:test_framework_view/type.dart';
import 'package:test_framework_view/common/file_download.dart';
import '../common/custom_state.dart';

class DynamicPlayFormBody extends StatefulWidget {
  final String testCaseId;
  final String type;
  final String? testRunid;
  final String? parentId;
  const DynamicPlayFormBody({Key? key, required this.type, this.testRunid, required this.testCaseId, this.parentId}) : super(key: key);

  @override
  State<DynamicPlayFormBody> createState() => _DynamicPlayFormBodyState();
}

class _DynamicPlayFormBodyState extends CustomState<DynamicPlayFormBody> {
  late Future futureParameterListPlay;
  List resultparameterFieldList = [];
  String testName = "";
  List resultClientList = [];
  List<Map<dynamic, dynamic>> listValueParameter = [];
  bool isViewColumn = true;
  PlatformFile? objFile;
  TextEditingController scheduleDate = TextEditingController();
  TextEditingController startDate = TextEditingController();
  TextEditingController endDate = TextEditingController();
  TextEditingController desController = TextEditingController();
  dynamic _testsVersion;
  dynamic _testsEnvironment;
  String? testRunType = "parallel";
  int? statisticsStatus = 0;
  bool? usePublicNode;
  var _listVersions = [];
  var _listEnvironment = [];
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    futureParameterListPlay = getInfoPage();
    scheduleDate.text = DateFormat('dd-MM-yyyy HH:mm:ss').format(DateTime.now());
    startDate.text = DateFormat('dd-MM-yyyy HH:mm:ss').format(DateTime.now());
    endDate.text = DateFormat('dd-MM-yyyy HH:mm:ss').format(DateTime.now());
    super.initState();
  }

  getInfoPage() async {
    await getParameterFieldListAndClientList();
    await getVersionList();
    await getEnvironmentList();
    if (widget.type != "manual_test") {
      for (var client in resultClientList) {
        await getProfileList(client);
        await getProfileDefault(client);
      }
    }
    if (widget.testRunid != null) {
      getReRun();
    } else if (widget.type == "test_case") {
      getTestCase(widget.testCaseId);
    } else {
      listValueParameter.add({
        "testCaseName": "",
        "testRunExpectedResult": 1,
        "priority": "3",
        "severity": "1",
      });
    }
    return 0;
  }

  getReRun() async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-run/${widget.testRunid}", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        listValueParameter = <Map<dynamic, dynamic>>[...response["body"]["result"]["testRunData"] ?? []];
        testRunType = response["body"]["result"]["testRunType"];
        statisticsStatus = response["body"]["result"]["statisticsStatus"];
        usePublicNode = response["body"]["result"]["usePublicNode"];
        if (_listVersions.any((element) => element["id"] == response["body"]["result"]["tfTestVersionId"])) {
          _testsVersion = response["body"]["result"]["tfTestVersion"];
        }
        if (_listEnvironment.any((element) => element["id"] == response["body"]["result"]["tfTestEnvironmentId"])) {
          _testsEnvironment = response["body"]["result"]["tfTestEnvironment"];
        }
      });
    }
  }

  Future getTestCase(String id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-case/$id", context);
    if (response.containsKey("body") && response["body"] is String == false && response["body"]["result"]["content"] != null) {
      setState(() {
        listValueParameter = <Map<dynamic, dynamic>>[...response["body"]["result"]["content"]["testRunData"] ?? []];
      });
    }
  }

  Future getParameterFieldListAndClientList() async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-case/${widget.parentId ?? widget.testCaseId}", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        testName = response["body"]["result"]["title"];
        if (response["body"]["result"]["content"] != null) {
          resultparameterFieldList = response["body"]["result"]["content"]["parameterFieldList"] ?? [];
          resultClientList = response["body"]["result"]["content"]["clientList"] ?? [];
        }
      });
    }
  }

  getVersionList() async {
    var findRequest = {"prjProjectId": Provider.of<NavigationModel>(context, listen: false).prjProjectId, "orderBy": "startDate DESC", "status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-version/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listVersions = response["body"]["resultList"];
        if (_listVersions.isNotEmpty) {
          var versionItem = _listVersions.firstWhere(
            (element) => element["defaultVersion"] == 1,
            orElse: () => null,
          );
          _testsVersion = versionItem ?? _listVersions[0];
        }
      });
    }
  }

  getEnvironmentList() async {
    var findRequest = {"prjProjectId": Provider.of<NavigationModel>(context, listen: false).prjProjectId, "orderBy": "startDate DESC", "status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-environment/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listEnvironment = response["body"]["resultList"];
        if (_listEnvironment.isNotEmpty) {
          var environmentItem = _listEnvironment.firstWhere(
            (element) => element["defaultEnvironment"] == 1,
            orElse: () => null,
          );
          _testsEnvironment = environmentItem ?? _listEnvironment[0];
        }
      });
    }
  }

  getProfileList(client) async {
    var findRequest = {
      "prjProjectId": Provider.of<NavigationModel>(context, listen: false).prjProjectId,
      "status": 1,
      "platform": client["platform"]
    };
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-profile/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        client["profileList"] = response["body"]["resultList"];
      });
    }
  }

  getProfileDefault(client) async {
    var response = await httpGet(
        "/test-framework-api/user/project/prj-project/${Provider.of<NavigationModel>(context, listen: false).prjProjectId}/tf-test-profile/${client["platform"]}/default",
        context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        client["tfTestProfileId"] = response["body"]["result"]["id"];
      });
    }
  }

  _selectDate(TextEditingController controller) async {
    final DateTime? datePicked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (datePicked != null && mounted) {
      final TimeOfDay? timePicked = await showTimePicker(
        context: context,
        initialTime: TimeOfDay.now(),
      );
      if (timePicked != null) {
        setState(() {
          controller.text =
              "${DateFormat('dd-MM-yyyy').format(datePicked)} ${timePicked.hour.toString().padLeft(2, '0')}:${timePicked.minute.toString().padLeft(2, '0')}";
        });
      }
    }
  }

  uploadFile() async {
    final request = http.MultipartRequest(
      "POST",
      Uri.parse(
        "$baseUrl/test-framework-api/user/test-framework-file/read-file",
      ),
    );
    request.headers.addAll({"Authorization": Provider.of<SecurityModel>(context, listen: false).authorization!});
    request.files.add(http.MultipartFile.fromBytes('file', await readFile(objFile!), filename: objFile!.name));
    var resp = await request.send();
    var responseDecode = jsonDecode(await resp.stream.bytesToString());
    if (responseDecode.containsKey("errorMessage") && mounted) {
      showToast(context: context, msg: responseDecode["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
    } else if (mounted) {
      showToast(
          context: context,
          msg: multiLanguageString(name: "upload_success", defaultValue: "Upload success", context: context),
          color: Colors.greenAccent,
          icon: const Icon(Icons.done));
      setState(() {
        listValueParameter = responseDecode["resultList"];
      });
    }
  }

  downloadFile() async {
    File? downloadedFile = await httpDownloadFile(
        context,
        "/test-framework-api/user/test-framework-file/tf-test-case/${widget.testCaseId}/generate-file",
        Provider.of<SecurityModel>(context, listen: false).authorization,
        'Output.xlsx');
    if (downloadedFile != null && mounted) {
      var snackBar = SnackBar(
        content: MultiLanguageText(name: "file_downloaded_to", defaultValue: "File downloaded to \$0 .", variables: ["$downloadedFile"]),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  handlePlayButton(navigationModel) async {
    for (var valueParameter in listValueParameter) {
      if (widget.type == "manual_test") {
        valueParameter["status"] ??= 0;
      }
      for (var parameterField in resultparameterFieldList) {
        if (valueParameter[parameterField["name"]] == null) {
          valueParameter[parameterField["name"]] = "";
        }
      }
    }
    var clientListBodyRequest = [];
    if (widget.type != "manual_test") {
      for (var element in resultClientList) {
        var clientNew = Map.from(element);
        clientNew.remove("profileList");
        clientListBodyRequest.add(clientNew);
      }
    }
    var requestBody = {
      "scheduledDate": "${DateFormat('dd-MM-yyyy HH:mm').parse(scheduleDate.text).toIso8601String()}+07:00",
      "startDate": "${DateFormat('dd-MM-yyyy HH:mm').parse(startDate.text).toIso8601String()}+07:00",
      "endDate": "${DateFormat('dd-MM-yyyy HH:mm').parse(endDate.text).toIso8601String()}+07:00",
      "createDate": "${DateTime.now().toIso8601String()}+07:00",
      "testRunData": listValueParameter,
      "status": 0,
      "prjProjectId": navigationModel.prjProjectId,
      "tfTestVersionId": _testsVersion["id"],
      "tfTestEnvironmentId": _testsEnvironment["id"],
      "testRunType": testRunType,
      "statisticsStatus": statisticsStatus,
      if (widget.type != "manual_test") "usePublicNode": usePublicNode,
      "description": desController.text,
      if (widget.type != "manual_test") "testRunConfig": {"clientList": clientListBodyRequest},
      "tfTestCaseId": widget.testCaseId,
    };
    var response = await httpPut("/test-framework-api/user/test-framework/tf-test-run", requestBody, context);
    if (response.containsKey("body") && response["body"] is String == false && response["body"].containsKey("errorMessage") && mounted) {
      showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      return false;
    } else {
      return true;
    }
  }

  @override
  void dispose() {
    scheduleDate.dispose();
    startDate.dispose();
    endDate.dispose();
    desController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      return Consumer2<NavigationModel, LanguageModel>(builder: (context, navigationModel, languageModel, child) {
        return FutureBuilder(
          future: futureParameterListPlay,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  NavMenuTabV1(
                    currentUrl: "/test-list/${widget.type}",
                    margin: const EdgeInsets.fromLTRB(50, 20, 50, 0),
                    navigationList: context.testMenuList,
                  ),
                  Expanded(
                    child: SingleChildScrollView(
                      child: IntrinsicHeight(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(50, 30, 50, 10),
                          child: Column(
                            children: [
                              headerWidget(navigationModel),
                              Expanded(
                                child: Form(
                                  key: _formKey,
                                  child: Container(
                                    margin: const EdgeInsets.only(top: 20),
                                    child: Column(
                                      children: [
                                        testRunInformation(),
                                        if (objFile != null)
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: MultiLanguageText(
                                                name: "file_name_obj",
                                                defaultValue: "File name : \$0",
                                                variables: [objFile!.name],
                                                style: const TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1))),
                                          ),
                                        if (objFile != null)
                                          const SizedBox(
                                            height: 20,
                                          ),
                                        RunBodyParameterWidget(
                                          isViewColumn: isViewColumn,
                                          listValueParameter: listValueParameter,
                                          resultparameterFieldList: resultparameterFieldList,
                                          type: widget.type,
                                          key: UniqueKey(),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  testRunBtn(navigationModel),
                ],
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      });
    });
  }

  Widget testRunBtn(NavigationModel navigationModel) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: 40,
            width: 80,
            child: ElevatedButton(
              onPressed: () async {
                if (_formKey.currentState!.validate()) {
                  var bool = await handlePlayButton(navigationModel);
                  if (bool) {
                    navigationModel.navigate("/qa-platform/project/${navigationModel.prjProjectId}/runs/${widget.type}");
                  }
                }
              },
              child: const MultiLanguageText(
                name: "run",
                defaultValue: "Run",
                isLowerCase: false,
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 5),
            height: 40,
            width: 120,
            child: ElevatedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              style: ElevatedButton.styleFrom(
                side: const BorderSide(color: Color.fromRGBO(209, 212, 216, 1)),
                backgroundColor: Colors.white,
                foregroundColor: const Color.fromRGBO(61, 90, 254, 1),
              ),
              child: const MultiLanguageText(
                name: "cancel",
                defaultValue: "Cancel",
                isLowerCase: false,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget testRunInformation() {
    return Column(
      children: [
        if (widget.type != "manual_test")
          Column(
            children: [
              Theme(
                data: ThemeData().copyWith(dividerColor: Colors.transparent),
                child: ExpansionTile(
                  initiallyExpanded: true,
                  tilePadding: const EdgeInsets.all(0),
                  childrenPadding: const EdgeInsets.symmetric(vertical: 10),
                  title: MultiLanguageText(
                    name: "choose_profile_for_the_clients",
                    defaultValue: "Choose profile for the client",
                    style: TextStyle(fontSize: context.fontSizeManager.fontSizeText20, fontWeight: FontWeight.w500),
                  ),
                  children: <Widget>[
                    for (var client in resultClientList)
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Expanded(
                                child: DynamicDropdownButton(
                                  value: client["tfTestProfileId"],
                                  hint: multiLanguageString(name: "choose_a_profile", defaultValue: "Choose a profile", context: context),
                                  items: (client["profileList"] as List).map<DropdownMenuItem<String>>((dynamic result) {
                                    return DropdownMenuItem(
                                      value: result["id"],
                                      child: Text(result["title"]),
                                    );
                                  }).toList(),
                                  onChanged: (newValue) {
                                    setState(() {
                                      client["tfTestProfileId"] = newValue!;
                                    });
                                  },
                                  labelText: multiLanguageString(
                                      name: "client_client_name", defaultValue: "Client \$0", variables: ["${client["name"]}"], context: context),
                                  borderRadius: 5,
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          )
                        ],
                      ),
                  ],
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                      child: DynamicDropdownButton(
                        value: testRunType,
                        hint: multiLanguageString(name: "choose_a_test_run_type", defaultValue: "Choose a test run type", context: context),
                        items: [
                          {"label": multiLanguageString(name: "sequence", defaultValue: "Sequence", context: context), "value": "sequence"},
                          {"label": multiLanguageString(name: "parallel", defaultValue: "Parallel", context: context), "value": "parallel"}
                        ].map<DropdownMenuItem<String>>((dynamic result) {
                          return DropdownMenuItem(
                            value: result["value"],
                            child: Text(result["label"]),
                          );
                        }).toList(),
                        onChanged: (newValue) {
                          setState(() {
                            _formKey.currentState!.validate();
                            testRunType = newValue!;
                          });
                        },
                        labelText: multiLanguageString(name: "test_run_type", defaultValue: "Test run type", context: context),
                        borderRadius: 5,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 8, 0, 8),
                      child: DynamicDropdownButton(
                        value: usePublicNode,
                        hint: multiLanguageString(name: "use_agent_question_mark", defaultValue: "Use Agent ?", context: context),
                        items: const [
                          {"name": "Public", "value": true},
                          {"name": "Private", "value": false},
                          {"name": "Any Agent", "value": null},
                        ].map<DropdownMenuItem<bool>>((dynamic result) {
                          return DropdownMenuItem(
                            value: result["value"],
                            child: Text(result["name"]),
                          );
                        }).toList(),
                        onChanged: (newValue) {
                          setState(() {
                            _formKey.currentState!.validate();
                            usePublicNode = newValue!;
                          });
                        },
                        labelText: multiLanguageString(name: "agent_type", defaultValue: "Agent Type", context: context),
                        borderRadius: 5,
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
            ],
          ),
        Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                child: DynamicDropdownSearchClearOption(
                  hint: multiLanguageString(name: "choose_a_version", defaultValue: "Choose a version", context: context),
                  labelText: multiLanguageString(name: "version", defaultValue: "Version", context: context),
                  controller: SingleValueDropDownController(data: DropDownValueModel(name: _testsVersion["name"], value: _testsVersion["id"])),
                  items: _listVersions.map<DropDownValueModel>((dynamic result) {
                    return DropDownValueModel(
                      value: result["id"],
                      name: result["name"],
                    );
                  }).toList(),
                  maxHeight: 200,
                  onChanged: (dynamic newValue) {
                    _formKey.currentState!.validate();
                    setState(() {
                      _testsVersion = _listVersions.firstWhere((element) => element["id"] == newValue.value);
                    });
                  },
                  isRequiredNotEmpty: true,
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 0, 8),
                child: DynamicDropdownSearchClearOption(
                  hint: multiLanguageString(name: "choose_an_environment", defaultValue: "Choose an environment", context: context),
                  labelText: multiLanguageString(name: "environment", defaultValue: "Environment", context: context),
                  controller:
                      SingleValueDropDownController(data: DropDownValueModel(name: _testsEnvironment["name"], value: _testsEnvironment["id"])),
                  items: _listEnvironment.map<DropDownValueModel>((dynamic result) {
                    return DropDownValueModel(
                      value: result["id"],
                      name: result["name"],
                    );
                  }).toList(),
                  maxHeight: 200,
                  onChanged: (dynamic newValue) {
                    _formKey.currentState!.validate();
                    setState(() {
                      _testsEnvironment = _listEnvironment.firstWhere((element) => element["id"] == newValue.value);
                    });
                  },
                  isRequiredNotEmpty: true,
                ),
              ),
            ),
          ],
        ),
        Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                child: DynamicDropdownButton(
                  value: statisticsStatus,
                  hint: multiLanguageString(name: "choose_a_test_run", defaultValue: "Choose a Test Run", context: context),
                  items: [
                    {"name": multiLanguageString(name: "draft", defaultValue: "Draft", context: context), "value": 0},
                    {"name": multiLanguageString(name: "complete", defaultValue: "Complete", context: context), "value": 1},
                  ].map<DropdownMenuItem<int>>((dynamic result) {
                    return DropdownMenuItem(
                      value: result["value"],
                      child: Text(result["name"]),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      _formKey.currentState!.validate();
                      statisticsStatus = newValue!;
                    });
                  },
                  labelText: multiLanguageString(name: "test_run", defaultValue: "Test Run", context: context),
                  borderRadius: 5,
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 0, 8),
                child: DynamicTextField(
                  controller: scheduleDate,
                  hintText: multiLanguageString(name: "select_schedule_time", defaultValue: "Select schedule time", context: context),
                  labelText: multiLanguageString(name: "schedule_time", defaultValue: "Schedule time", context: context),
                  suffixIcon: const Icon(Icons.calendar_today),
                  readOnly: true,
                  onTap: () {
                    _selectDate(scheduleDate);
                  },
                ),
              ),
            ),
          ],
        ),
        if (widget.type == "manual_test")
          Row(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: DynamicTextField(
                    controller: startDate,
                    suffixIcon: const Icon(Icons.calendar_today),
                    hintText: multiLanguageString(name: "select_start_date", defaultValue: "Select start date", context: context),
                    labelText: multiLanguageString(name: "start_date", defaultValue: "Start date", context: context),
                    readOnly: true,
                    onTap: () {
                      _selectDate(startDate);
                    },
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0, 8, 0, 8),
                  child: DynamicTextField(
                    controller: endDate,
                    suffixIcon: const Icon(Icons.calendar_today),
                    hintText: multiLanguageString(name: "select_end_date", defaultValue: "Select end date", context: context),
                    labelText: multiLanguageString(name: "end_date", defaultValue: "End date", context: context),
                    readOnly: true,
                    onTap: () {
                      _selectDate(endDate);
                    },
                  ),
                ),
              ),
            ],
          ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: DynamicTextField(
            controller: desController,
            labelText: multiLanguageString(name: "test_run_description", defaultValue: "Test run description", context: context),
            hintText: multiLanguageString(name: "enter_a_test_run_description", defaultValue: "Enter a Test run description", context: context),
            maxline: 3,
            minline: 1,
          ),
        ),
      ],
    );
  }

  Widget headerWidget(NavigationModel navigationModel) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: const Icon(Icons.arrow_back),
              padding: EdgeInsets.zero,
              splashRadius: 16,
            ),
            Text(testName, style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500)),
          ],
        ),
        Row(
          children: [
            Container(
              margin: const EdgeInsets.only(right: 10),
              child: SizedBox(
                height: 40,
                child: FloatingActionButton.extended(
                  heroTag: null,
                  onPressed: () {
                    showDialog<String>(
                        context: context,
                        builder: (BuildContext context) {
                          return DialogRerunWidget(
                              testId: widget.testCaseId,
                              callback: (result) {
                                setState(() {
                                  listValueParameter = <Map<dynamic, dynamic>>[...result["testRunData"] ?? []];
                                });
                              });
                        });
                  },
                  icon: const Icon(Icons.refresh, color: Color.fromRGBO(49, 49, 49, 1)),
                  label: MultiLanguageText(
                    name: "rerun",
                    defaultValue: "Rerun",
                    style: TextStyle(
                        fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
                  ),
                  backgroundColor: const Color.fromRGBO(216, 218, 229, .7),
                  elevation: 0,
                  hoverElevation: 0,
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(right: 10),
              child: SizedBox(
                height: 40,
                child: FloatingActionButton.extended(
                  heroTag: null,
                  onPressed: () async {
                    objFile = await selectFile(['xlsx']);
                    if (objFile != null) {
                      await uploadFile();
                    }
                  },
                  icon: const FaIcon(FontAwesomeIcons.fileArrowUp, color: Color.fromRGBO(49, 49, 49, 1)),
                  label: MultiLanguageText(
                    name: "upload_excel",
                    defaultValue: "Upload Excel",
                    style: TextStyle(
                        fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
                  ),
                  backgroundColor: const Color.fromRGBO(216, 218, 229, .7),
                  elevation: 0,
                  hoverElevation: 0,
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(right: 10),
              child: SizedBox(
                height: 40,
                child: FloatingActionButton.extended(
                  heroTag: null,
                  onPressed: () async {
                    downloadFile();
                  },
                  icon: const FaIcon(FontAwesomeIcons.fileArrowDown, color: Color.fromRGBO(49, 49, 49, 1)),
                  label: MultiLanguageText(
                    name: "download_excel",
                    defaultValue: "Download Excel",
                    style: TextStyle(
                        fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
                  ),
                  backgroundColor: const Color.fromRGBO(216, 218, 229, .7),
                  elevation: 0,
                  hoverElevation: 0,
                ),
              ),
            ),
            isViewColumn
                ? SizedBox(
                    height: 40,
                    child: FloatingActionButton(
                      heroTag: null,
                      onPressed: () async {
                        setState(() {
                          isViewColumn = false;
                        });
                      },
                      backgroundColor: const Color.fromARGB(176, 125, 141, 231),
                      elevation: 0,
                      hoverElevation: 0,
                      child: const Icon(Icons.view_column, color: Color.fromRGBO(49, 49, 49, 1)),
                    ),
                  )
                : SizedBox(
                    height: 40,
                    child: FloatingActionButton(
                      heroTag: null,
                      onPressed: () async {
                        setState(() {
                          isViewColumn = true;
                        });
                      },
                      backgroundColor: const Color.fromARGB(176, 125, 141, 231),
                      elevation: 0,
                      hoverElevation: 0,
                      child: const Icon(Icons.view_stream, color: Color.fromRGBO(49, 49, 49, 1)),
                    ),
                  ),
          ],
        )
      ],
    );
  }
}

class FindFileWidget extends StatefulWidget {
  final Function callback;
  const FindFileWidget(this.callback, {Key? key}) : super(key: key);

  @override
  State<FindFileWidget> createState() => _FindFileWidgetState();
}

class _FindFileWidgetState extends CustomState<FindFileWidget> {
  late Future futureListFile;
  bool isChangeWebElement = false;
  var listSearchResponse = [];
  String typeController = "";
  TextEditingController nameController = TextEditingController();
  var searchRequest = {};
  var rowCount = 0;
  var currentPage = 1;
  var rowPerPage = 5;

  @override
  void initState() {
    search({});
    super.initState();
  }

  search(searchRequest) {
    this.searchRequest = searchRequest;
    futureListFile = getListFile(currentPage);
  }

  getListFile(page) async {
    if ((page - 1) * rowPerPage > rowCount) {
      page = (1.0 * rowCount / rowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {
      "queryOffset": (page - 1) * rowPerPage,
      "queryLimit": rowPerPage,
      "prjProjectId": Provider.of<NavigationModel>(context, listen: false).prjProjectId,
      "status": 1
    };
    if (searchRequest["titleLike"] != null && searchRequest["titleLike"].isNotEmpty) {
      findRequest["titleLike"] = "%${searchRequest["titleLike"]}%";
    }
    var response = await httpPost("/test-framework-api/user/test-framework/tf-file/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPage = page;
        rowCount = response["body"]["rowCount"];
        listSearchResponse = response["body"]["resultList"];
      });
    }
    return 0;
  }

  @override
  void dispose() {
    nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: futureListFile,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return ListView(
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Center(
                            child: MultiLanguageText(
                              name: "choose_a_file",
                              defaultValue: "Choose a file",
                              style: TextStyle(fontWeight: FontWeight.bold, fontSize: context.fontSizeManager.fontSizeText20, letterSpacing: 1.5),
                            ),
                          ),
                        ),
                        TextButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: const Icon(
                              Icons.close,
                              color: Colors.black,
                            ))
                      ],
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    TextField(
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.only(bottom: 20, left: 10, right: 10),
                        border: const OutlineInputBorder(),
                        hintText: multiLanguageString(
                            name: "enter_a_title_file_for_search", defaultValue: "Enter a title file for search", context: context),
                        prefixIcon: const Icon(Icons.search),
                      ),
                      onChanged: (text) {
                        search({"titleLike": text.trim()});
                      },
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    if (listSearchResponse.isNotEmpty) tableWidget(),
                    if (listSearchResponse.isEmpty)
                      Container(
                        margin: const EdgeInsets.only(top: 20),
                        child: Align(
                          alignment: Alignment.center,
                          child: MultiLanguageText(
                            name: "not_found_element",
                            defaultValue: "Not found element",
                            style: TextStyle(
                              fontSize: context.fontSizeManager.fontSizeText16,
                              fontWeight: FontWeight.bold,
                              letterSpacing: 1.5,
                            ),
                          ),
                        ),
                      ),
                    const SizedBox(height: 20),
                    chooseFileBtn()
                  ],
                ),
              ),
            ],
          );
        } else if (snapshot.hasError) {
          return Text('${snapshot.error}');
        }
        return const Center(child: CircularProgressIndicator());
      },
    );
  }

  Widget chooseFileBtn() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          margin: const EdgeInsets.only(right: 5),
          height: 40,
          width: 120,
          child: ElevatedButton(
            onPressed: () async {
              Navigator.pop(context);
            },
            child: const MultiLanguageText(
              name: "ok",
              defaultValue: "Ok",
              isLowerCase: false,
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 5),
          height: 40,
          width: 120,
          child: ElevatedButton(
            onPressed: () {
              Navigator.pop(context);
            },
            style: ElevatedButton.styleFrom(
              side: const BorderSide(color: Color.fromRGBO(209, 212, 216, 1)),
              backgroundColor: Colors.white,
              foregroundColor: const Color.fromRGBO(61, 90, 254, 1),
            ),
            child: const MultiLanguageText(
              name: "cancel",
              defaultValue: "Cancel",
              isLowerCase: false,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        )
      ],
    );
  }

  Widget tableWidget() {
    return Column(
      children: [
        const SizedBox(
          height: 20,
        ),
        Container(
          constraints: BoxConstraints(maxHeight: MediaQuery.of(context).size.height * 0.3),
          child: SingleChildScrollView(
            child: Row(
              children: [
                Expanded(
                  child: DataTable(
                    showCheckboxColumn: false,
                    border: TableBorder(
                        verticalInside: Style(context).styleBorderTable,
                        horizontalInside: Style(context).styleBorderTable,
                        bottom: Style(context).styleBorderTable,
                        left: Style(context).styleBorderTable,
                        right: Style(context).styleBorderTable,
                        top: Style(context).styleBorderTable),
                    columns: [
                      DataColumn(
                        label: MultiLanguageText(
                          name: "title",
                          defaultValue: "Title",
                          isLowerCase: false,
                          style: Style(context).styleTextDataColumn,
                          align: TextAlign.center,
                        ),
                      ),
                      DataColumn(
                        label: MultiLanguageText(
                          name: "description",
                          defaultValue: "Description",
                          isLowerCase: false,
                          style: Style(context).styleTextDataColumn,
                          align: TextAlign.center,
                        ),
                      ),
                      DataColumn(
                        label: MultiLanguageText(
                          name: "file_name",
                          defaultValue: "File name",
                          isLowerCase: false,
                          style: Style(context).styleTextDataColumn,
                          align: TextAlign.center,
                        ),
                      ),
                    ],
                    rows: [
                      for (var searchResponseItem in listSearchResponse)
                        DataRow(
                          onSelectChanged: (value) {
                            Navigator.pop(context);
                            widget.callback("${searchResponseItem["id"]}/${searchResponseItem["fileName"] ?? ""}");
                          },
                          cells: [
                            DataCell(
                              Text(searchResponseItem["title"] ?? ""),
                            ),
                            DataCell(
                              Text(searchResponseItem["description"] ?? ""),
                            ),
                            DataCell(
                              Text(searchResponseItem["fileName"] ?? ""),
                            ),
                          ],
                        ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class DialogRerunWidget extends StatefulWidget {
  final String testId;
  final Function? callback;
  const DialogRerunWidget({
    Key? key,
    this.callback,
    required this.testId,
  }) : super(key: key);

  @override
  State<DialogRerunWidget> createState() => _DialogRerunWidgetState();
}

class _DialogRerunWidgetState extends CustomState<DialogRerunWidget> {
  SingleValueDropDownController versionController = SingleValueDropDownController();
  var prjProjectId = "";
  var listTestRun = [];
  var rowCount = 0;
  var currentPage = 1;
  var rowPerPage = 5;
  var searchRequest = {};
  List _listVersions = [];
  bool isSearched = false;

  @override
  void initState() {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    prjProjectId = navigationModel.prjProjectId ?? "";
    search({});
    super.initState();
  }

  search(searchRequest) {
    this.searchRequest = searchRequest;
    getListTfTestCaseRun(currentPage);
    getVersionList();
  }

  getListTfTestCaseRun(page) async {
    if ((page - 1) * rowPerPage > rowCount) {
      page = (1.0 * rowCount / rowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * rowPerPage, "queryLimit": rowPerPage, "prjProjectId": prjProjectId};
    if (searchRequest["tfTestVersionId"] != null && searchRequest["tfTestVersionId"].isNotEmpty) {
      findRequest["tfTestVersionId"] = searchRequest["tfTestVersionId"];
    }
    findRequest["tfTestCaseIdNotNull"] = true;
    findRequest["tfTestCaseId"] = widget.testId;
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-run/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPage = page;
        rowCount = response["body"]["rowCount"];
        listTestRun = response["body"]["resultList"];
      });
      return 0;
    }
  }

  getVersionList() async {
    var findRequest = {"prjProjectId": prjProjectId, "status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-version/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listVersions = response["body"]["resultList"];
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
    versionController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: const EdgeInsets.all(0),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "select_last_run_data",
            defaultValue: "Select last run data",
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Icon(
                Icons.close,
                color: Colors.black,
              ))
        ],
      ),
      content: Container(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
        decoration: const BoxDecoration(
          border: Border(
            top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
            bottom: BorderSide(
              color: Color.fromRGBO(216, 218, 229, 1),
            ),
          ),
        ),
        margin: const EdgeInsets.only(top: 20, bottom: 10),
        width: 1000,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                      child: DynamicDropdownSearchClearOption(
                        labelText: multiLanguageString(name: "test_run_version", defaultValue: "Test Run Version", context: context),
                        controller: versionController,
                        hint: multiLanguageString(name: "up_search_version", defaultValue: "Search Version", context: context),
                        items: _listVersions.map<DropDownValueModel>((dynamic result) {
                          return DropDownValueModel(
                            value: result["id"],
                            name: result["title"],
                          );
                        }).toList(),
                        maxHeight: 200,
                        onChanged: (dynamic newValue) {
                          if (isSearched && newValue == "") {
                            search({
                              "tfTestVersionId": versionController.dropDownValue?.value,
                            });
                          }
                        },
                      ),
                    ),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 20),
                    child: ElevatedButton(
                        onPressed: () {
                          isSearched = true;
                          search({
                            "tfTestVersionId": versionController.dropDownValue?.value,
                          });
                        },
                        child: const MultiLanguageText(name: "search", defaultValue: "Search", isLowerCase: false)),
                  ),
                  ElevatedButton(
                      onPressed: () {
                        setState(() {
                          versionController.clearDropDown();
                          search({});
                        });
                      },
                      child: const MultiLanguageText(name: "reset", defaultValue: "Reset", isLowerCase: false))
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              (listTestRun.isNotEmpty)
                  ? Column(
                      children: [
                        Container(
                          padding: const EdgeInsets.all(15),
                          decoration: const BoxDecoration(
                            border: Border(
                              top: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
                              bottom: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
                              left: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
                              right: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
                            ),
                          ),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 3,
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 5),
                                  child: MultiLanguageText(
                                      name: "title", defaultValue: "Title", isLowerCase: false, style: Style(context).styleTextDataColumn),
                                ),
                              ),
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 5),
                                  child: Center(
                                      child: MultiLanguageText(
                                          name: "run_owner",
                                          defaultValue: "Run owner",
                                          isLowerCase: false,
                                          style: Style(context).styleTextDataColumn)),
                                ),
                              ),
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 5),
                                  child: Center(
                                      child: MultiLanguageText(
                                          name: "create_date",
                                          defaultValue: "Create date",
                                          isLowerCase: false,
                                          style: Style(context).styleTextDataColumn)),
                                ),
                              ),
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 5),
                                  child: Center(
                                      child: MultiLanguageText(
                                          name: "status", defaultValue: "Status", isLowerCase: false, style: Style(context).styleTextDataColumn)),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Column(
                          children: [
                            for (var testRun in listTestRun)
                              GestureDetector(
                                onTap: () {
                                  widget.callback!(testRun);
                                  Navigator.pop(context);
                                },
                                child: Container(
                                  padding: const EdgeInsets.all(15),
                                  decoration: const BoxDecoration(
                                    border: Border(
                                      top: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
                                      bottom: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
                                      left: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
                                      right: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
                                    ),
                                  ),
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Expanded(
                                        flex: 3,
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(horizontal: 5),
                                          child: Text(
                                            testRun["tfTestCase"]["title"] ?? "",
                                            maxLines: 3,
                                            softWrap: true,
                                            style: TextStyle(
                                                fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(horizontal: 5),
                                          child: Center(
                                            child: Text(
                                              testRun["sysUser"]["fullname"] ?? "",
                                              softWrap: true,
                                              style: TextStyle(
                                                  fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(horizontal: 5),
                                          child: Center(
                                            child: (testRun["createDate"] != null)
                                                ? Column(
                                                    children: [
                                                      Text(
                                                        DateFormat('dd-MM-yyyy')
                                                            .format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(testRun["createDate"]).toLocal()),
                                                        softWrap: true,
                                                        style: TextStyle(
                                                            fontSize: context.fontSizeManager.fontSizeText16,
                                                            color: const Color.fromRGBO(49, 49, 49, 1)),
                                                      ),
                                                      Text(
                                                        DateFormat('HH:mm:ss')
                                                            .format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(testRun["createDate"]).toLocal()),
                                                        softWrap: true,
                                                        style: TextStyle(
                                                            fontSize: context.fontSizeManager.fontSizeText16,
                                                            color: const Color.fromRGBO(49, 49, 49, 1)),
                                                      ),
                                                    ],
                                                  )
                                                : const SizedBox.shrink(),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(horizontal: 5),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                              IntrinsicWidth(
                                                child: Badge6StatusWidget(
                                                  status: testRun["status"],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                          ],
                        ),
                      ],
                    )
                  : const Center(
                      child: MultiLanguageText(
                        name: "no_data_of_last_run",
                        defaultValue: "No data of last run",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
              DynamicTablePagging(
                rowCount,
                currentPage,
                rowPerPage,
                pageChangeHandler: (page) {
                  getListTfTestCaseRun(page);
                },
                rowPerPageChangeHandler: (value) {
                  setState(() {
                    rowPerPage = value;
                    getListTfTestCaseRun(1);
                  });
                },
              ),
            ],
          ),
        ),
      ),
      actionsAlignment: MainAxisAlignment.center,
      actions: [
        Container(
          margin: const EdgeInsets.only(left: 5),
          height: 40,
          width: 120,
          child: ElevatedButton(
            onPressed: () {
              Navigator.pop(context);
            },
            style: ElevatedButton.styleFrom(
                side: const BorderSide(color: Color.fromRGBO(209, 212, 216, 1)),
                backgroundColor: Colors.white,
                foregroundColor: const Color.fromRGBO(61, 90, 254, 1)),
            child: const MultiLanguageText(
              name: "cancel",
              defaultValue: "Cancel",
              isLowerCase: false,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ],
    );
  }
}

class RunBodyParameterWidget extends StatefulWidget {
  final bool isViewColumn;
  final List resultparameterFieldList;
  final List<Map<dynamic, dynamic>> listValueParameter;
  final String type;
  const RunBodyParameterWidget(
      {Key? key, required this.isViewColumn, required this.resultparameterFieldList, required this.listValueParameter, required this.type})
      : super(key: key);

  @override
  State<RunBodyParameterWidget> createState() => _RunBodyParameterWidgetState();
}

class _RunBodyParameterWidgetState extends CustomState<RunBodyParameterWidget> {
  var rowCount = 0;
  var currentPage = 1;
  var rowPerPage = 10;
  List<Map<dynamic, dynamic>> listValueParameterWidget = [];
  List<Map<String, String>> listMapPriority = [];
  List<Map<String, dynamic>> listMapTestRunExpectedResult = [];
  List<Map<String, String>> listMapSeverity = [];
  final _scrollController = ScrollController();
  @override
  void initState() {
    super.initState();
    listMapPriority = [
      {"value": "1", "name": multiLanguageString(name: "item_highest", defaultValue: "Highest", context: context)},
      {"value": "2", "name": multiLanguageString(name: "item_high", defaultValue: "High", context: context)},
      {"value": "3", "name": multiLanguageString(name: "item_medium", defaultValue: "Medium", context: context)},
      {"value": "4", "name": multiLanguageString(name: "item_low", defaultValue: "Low", context: context)},
      {"value": "5", "name": multiLanguageString(name: "item_lowest", defaultValue: "Lowest", context: context)},
    ];
    listMapTestRunExpectedResult = [
      {"name": multiLanguageString(name: "item_pass", defaultValue: "Pass", context: context), "value": 1},
      {"name": multiLanguageString(name: "item_fail", defaultValue: "Fail", context: context), "value": 0},
    ];
    listMapSeverity = [
      {"value": "1", "name": multiLanguageString(name: "item_critical", defaultValue: "Critical", context: context)},
      {"value": "2", "name": multiLanguageString(name: "item_major", defaultValue: "Major", context: context)},
      {"value": "3", "name": multiLanguageString(name: "item_average", defaultValue: "Average", context: context)},
      {"value": "4", "name": multiLanguageString(name: "item_minor", defaultValue: "Minor", context: context)},
      {"value": "5", "name": multiLanguageString(name: "item_trivial", defaultValue: "Trivial", context: context)},
    ];
    if (widget.isViewColumn) {
      rowPerPage = 5;
    } else {
      rowPerPage = 10;
    }
    searchPaging(1);
  }

  searchPaging(page) {
    if ((page - 1) * rowPerPage > rowCount) {
      page = (1.0 * rowCount / rowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    if (widget.listValueParameter.length >= page * rowPerPage) {
      listValueParameterWidget = widget.listValueParameter.sublist((page - 1) * rowPerPage, page * rowPerPage);
    } else {
      listValueParameterWidget = widget.listValueParameter.sublist((page - 1) * rowPerPage, widget.listValueParameter.length);
    }
    for (Map<dynamic, dynamic> valueParameter in listValueParameterWidget) {
      valueParameter["testRunExpectedResult"] ??= 1;
      valueParameter["severity"] ??= "1";
      valueParameter["priority"] ??= "3";
      if (widget.type == "manual_test") valueParameter["status"] ??= 0;
    }
    setState(() {
      currentPage = page;
      rowCount = widget.listValueParameter.length;
    });
  }

  checkTypeView() {
    return widget.type != "test_case" && widget.type != "batch_step";
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        (!widget.isViewColumn)
            ? Column(
                children: [
                  Scrollbar(
                    controller: _scrollController,
                    thumbVisibility: true,
                    child: SingleChildScrollView(
                      controller: _scrollController,
                      scrollDirection: Axis.horizontal,
                      child: Theme(
                        data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
                        child: DataTable(
                          columnSpacing: 5,
                          horizontalMargin: 0,
                          columns: [
                            if (widget.type != "test_case" && widget.type != "batch_step" && widget.type != "manual_test")
                              DataColumn(
                                  label: Container(
                                    width: 300,
                                    alignment: Alignment.center,
                                    child: MultiLanguageText(
                                      name: "test_case",
                                      defaultValue: "Test case",
                                      maxLines: 1,
                                      style: Style(context).styleTextDataColumn,
                                    ),
                                  ),
                                  numeric: false),
                            if (widget.type != "test_case" && widget.type != "batch_step" && widget.type != "manual_test")
                              DataColumn(
                                  label: Container(
                                    width: 300,
                                    alignment: Alignment.center,
                                    child: MultiLanguageText(
                                      name: "test_run_expected_result",
                                      defaultValue: "Test Run Expected Result",
                                      maxLines: 1,
                                      style: Style(context).styleTextDataColumn,
                                    ),
                                  ),
                                  numeric: false),
                            if (checkTypeView())
                              DataColumn(
                                  label: Container(
                                    width: 300,
                                    alignment: Alignment.center,
                                    child: MultiLanguageText(
                                      name: "severity",
                                      defaultValue: "Severity",
                                      maxLines: 1,
                                      style: Style(context).styleTextDataColumn,
                                    ),
                                  ),
                                  numeric: false),
                            if (checkTypeView())
                              DataColumn(
                                  label: Container(
                                    width: 300,
                                    alignment: Alignment.center,
                                    child: MultiLanguageText(
                                      name: "priority",
                                      defaultValue: "Priority",
                                      maxLines: 1,
                                      style: Style(context).styleTextDataColumn,
                                    ),
                                  ),
                                  numeric: false),
                            for (var paramColumn in widget.resultparameterFieldList)
                              if (paramColumn["dataType"] != "RandomValue")
                                DataColumn(
                                  label: Container(
                                    constraints: const BoxConstraints(minWidth: 300),
                                    alignment: Alignment.center,
                                    child: Text(
                                      paramColumn["name"] ?? "",
                                      maxLines: 1,
                                      style: Style(context).styleTextDataColumn,
                                    ),
                                  ),
                                ),
                            if (widget.type == "manual_test")
                              DataColumn(
                                  label: Container(
                                    width: 300,
                                    alignment: Alignment.center,
                                    child: MultiLanguageText(
                                      name: "status",
                                      defaultValue: "Status",
                                      maxLines: 1,
                                      style: Style(context).styleTextDataColumn,
                                    ),
                                  ),
                                  numeric: false),
                            DataColumn(label: Container(), numeric: true)
                          ],
                          rows: [
                            for (var valueParameter in listValueParameterWidget)
                              DataRow(
                                cells: [
                                  if (widget.type != "test_case" && widget.type != "batch_step" && widget.type != "manual_test")
                                    DataCell(
                                      SizedBox(
                                        width: 300,
                                        child: DynamicTextField(
                                          controller: TextEditingController(text: valueParameter["testCaseName"]),
                                          hintText:
                                              multiLanguageString(name: "enter_a_test_case", defaultValue: "Enter a test case", context: context),
                                          onChanged: (text) {
                                            valueParameter["testCaseName"] = text;
                                          },
                                        ),
                                      ),
                                    ),
                                  if (widget.type != "test_case" && widget.type != "batch_step" && widget.type != "manual_test")
                                    DataCell(
                                      SizedBox(
                                        width: 300,
                                        child: DynamicDropdownButton(
                                          value: valueParameter["testRunExpectedResult"] ?? 1,
                                          hint: multiLanguageString(
                                              name: "choose_a_test_run_expected_result",
                                              defaultValue: "Choose a Test Run expected result",
                                              context: context),
                                          items: listMapTestRunExpectedResult.map<DropdownMenuItem<int>>((dynamic result) {
                                            return DropdownMenuItem(
                                              value: result["value"],
                                              child: Text(result["name"]),
                                            );
                                          }).toList(),
                                          onChanged: (newValue) {
                                            setState(() {
                                              valueParameter["testRunExpectedResult"] = newValue!;
                                            });
                                          },
                                          borderRadius: 5,
                                        ),
                                      ),
                                    ),
                                  if (checkTypeView())
                                    DataCell(
                                      SizedBox(
                                        width: 300,
                                        child: DynamicDropdownButton(
                                          value: valueParameter["severity"] ?? "1",
                                          hint: multiLanguageString(name: "choose_a_severity", defaultValue: "Choose a severity", context: context),
                                          items: listMapSeverity.map<DropdownMenuItem<String>>((dynamic result) {
                                            return DropdownMenuItem(
                                              value: result["value"],
                                              child: Text(result["name"]),
                                            );
                                          }).toList(),
                                          onChanged: (newValue) {
                                            setState(() {
                                              valueParameter["severity"] = newValue!;
                                            });
                                          },
                                          borderRadius: 5,
                                        ),
                                      ),
                                    ),
                                  if (checkTypeView())
                                    DataCell(
                                      SizedBox(
                                        width: 300,
                                        child: DynamicDropdownButton(
                                          value: valueParameter["priority"] ?? "3",
                                          hint: multiLanguageString(name: "choose_a_priority", defaultValue: "Choose a priority", context: context),
                                          items: listMapPriority.map<DropdownMenuItem<String>>((dynamic result) {
                                            return DropdownMenuItem(
                                              value: result["value"],
                                              child: Text(result["name"]),
                                            );
                                          }).toList(),
                                          onChanged: (newValue) {
                                            setState(() {
                                              valueParameter["priority"] = newValue!;
                                            });
                                          },
                                          borderRadius: 5,
                                        ),
                                      ),
                                    ),
                                  for (var parameterField in widget.resultparameterFieldList)
                                    if (parameterField["dataType"] != "RandomValue")
                                      DataCell(
                                        Row(
                                          children: [
                                            Expanded(
                                              child: SizedBox(
                                                width: 300,
                                                child: DynamicTextField(
                                                  controller: TextEditingController(text: valueParameter[parameterField["name"]]),
                                                  hintText: multiLanguageString(
                                                      name: "enter_a_parameter_field",
                                                      defaultValue: "Enter a \$0",
                                                      variables: ["${parameterField["name"]}"],
                                                      context: context),
                                                  onChanged: (text) {
                                                    valueParameter[parameterField["name"]] = text;
                                                  },
                                                  obscureText: parameterField["dataType"] == "Password",
                                                ),
                                              ),
                                            ),
                                            if (parameterField["dataType"] == "File")
                                              ElevatedButton(
                                                  onPressed: () {
                                                    Navigator.of(context).push(
                                                      createRoute(
                                                        FindFileWidget((String valueCallback) {
                                                          setState(() {
                                                            valueParameter[parameterField["name"]] = valueCallback;
                                                          });
                                                        }),
                                                      ),
                                                    );
                                                  },
                                                  child: const MultiLanguageText(name: "select_file", defaultValue: "Select file")),
                                          ],
                                        ),
                                      ),
                                  if (widget.type == "manual_test")
                                    DataCell(
                                      SizedBox(
                                        width: 300,
                                        child: DynamicDropdownButton(
                                          value: valueParameter["status"] ?? 0,
                                          hint: multiLanguageString(name: "choose_a_status", defaultValue: "Choose a status", context: context),
                                          items: [
                                            {"name": multiLanguageString(name: "item_pass", defaultValue: "Pass", context: context), "value": 1},
                                            {"name": multiLanguageString(name: "item_fail", defaultValue: "Fail", context: context), "value": 0},
                                          ].map<DropdownMenuItem<int>>((dynamic result) {
                                            return DropdownMenuItem(
                                              value: result["value"],
                                              child: Text(result["name"]),
                                            );
                                          }).toList(),
                                          onChanged: (newValue) {
                                            setState(() {
                                              valueParameter["status"] = newValue!;
                                            });
                                          },
                                          borderRadius: 5,
                                        ),
                                      ),
                                    ),
                                  DataCell(
                                    SizedBox(
                                      width: 40,
                                      child: IconButton(
                                          tooltip: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                                          splashRadius: 20,
                                          onPressed: () {
                                            setState(() {
                                              widget.listValueParameter.remove(valueParameter);
                                              searchPaging(currentPage);
                                            });
                                          },
                                          icon: const Icon(Icons.delete, color: Colors.red)),
                                    ),
                                  )
                                ],
                              ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Colors.blue,
                    ),
                    child: IconButton(
                      tooltip: multiLanguageString(name: "add", defaultValue: "Add", context: context),
                      splashRadius: 20,
                      onPressed: () {
                        setState(() {
                          widget.listValueParameter.add({});
                          if (widget.listValueParameter.length - currentPage * rowPerPage == 1) {
                            searchPaging(currentPage + 1);
                          } else {
                            searchPaging(currentPage);
                          }
                        });
                      },
                      icon: const Icon(Icons.add, color: Colors.white),
                    ),
                  ),
                ],
              )
            : IntrinsicWidth(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Column(
                      children: [
                        const SizedBox(
                          height: 42,
                        ),
                        if (widget.type != "test_case" && widget.type != "batch_step" && widget.type != "manual_test")
                          Container(
                            padding: const EdgeInsets.only(left: 10, right: 10),
                            alignment: Alignment.centerLeft,
                            width: 200,
                            height: 48,
                            child: Tooltip(
                              message: multiLanguageString(name: "test_case", defaultValue: "Test case", context: context),
                              child: MultiLanguageText(
                                name: "test_case",
                                defaultValue: "Test case",
                                maxLines: 1,
                                style: Style(context).styleTextDataColumn,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                        if (widget.type != "test_case" && widget.type != "batch_step" && widget.type != "manual_test")
                          Container(
                            padding: const EdgeInsets.only(left: 10, right: 10),
                            alignment: Alignment.centerLeft,
                            width: 200,
                            height: 48,
                            child: Tooltip(
                              message:
                                  multiLanguageString(name: "test_run_expected_result", defaultValue: "Test Run Expected Result", context: context),
                              child: MultiLanguageText(
                                name: "test_run_expected_result",
                                defaultValue: "Test Run Expected Result",
                                maxLines: 1,
                                style: Style(context).styleTextDataColumn,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                        if (checkTypeView())
                          Container(
                            padding: const EdgeInsets.only(left: 10, right: 10),
                            alignment: Alignment.centerLeft,
                            width: 200,
                            height: 48,
                            child: Tooltip(
                              message: multiLanguageString(name: "severity", defaultValue: "Severity", context: context),
                              child: MultiLanguageText(
                                name: "severity",
                                defaultValue: "Severity",
                                maxLines: 1,
                                style: Style(context).styleTextDataColumn,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                        if (checkTypeView())
                          Container(
                            padding: const EdgeInsets.only(left: 10, right: 10),
                            alignment: Alignment.centerLeft,
                            width: 200,
                            height: 48,
                            child: Tooltip(
                              message: multiLanguageString(name: "priority", defaultValue: "Priority", context: context),
                              child: MultiLanguageText(
                                name: "priority",
                                defaultValue: "Priority",
                                maxLines: 1,
                                style: Style(context).styleTextDataColumn,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                        for (var paramColumn in widget.resultparameterFieldList)
                          if (paramColumn["dataType"] != "RandomValue")
                            Container(
                              padding: const EdgeInsets.only(left: 10, right: 10),
                              alignment: Alignment.centerLeft,
                              width: 200,
                              height: 48,
                              child: Tooltip(
                                message: paramColumn["name"],
                                child: Text(
                                  paramColumn["name"],
                                  maxLines: 1,
                                  style: Style(context).styleTextDataColumn,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                            ),
                        if (widget.type == "manual_test")
                          Container(
                            padding: const EdgeInsets.only(left: 10, right: 10),
                            alignment: Alignment.centerLeft,
                            width: 200,
                            height: 48,
                            child: Tooltip(
                              message: multiLanguageString(name: "status", defaultValue: "Status", context: context),
                              child: MultiLanguageText(
                                name: "status",
                                defaultValue: "Status",
                                maxLines: 1,
                                style: Style(context).styleTextDataColumn,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          )
                      ],
                    ),
                    Expanded(
                      child: Scrollbar(
                        thumbVisibility: true,
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 10),
                          child: SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            primary: true,
                            child: Row(
                              children: [
                                for (var valueParameter in listValueParameterWidget)
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      SizedBox(
                                        width: 40,
                                        child: IconButton(
                                            tooltip: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                                            splashRadius: 20,
                                            onPressed: () {
                                              setState(() {
                                                widget.listValueParameter.remove(valueParameter);
                                                searchPaging(currentPage);
                                              });
                                            },
                                            icon: const Icon(Icons.delete, color: Colors.red)),
                                      ),
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          if (widget.type != "test_case" && widget.type != "batch_step" && widget.type != "manual_test")
                                            SizedBox(
                                              width: 300,
                                              child: DynamicTextField(
                                                controller: TextEditingController(text: valueParameter["testCaseName"]),
                                                hintText: multiLanguageString(
                                                    name: "enter_a_test_case", defaultValue: "Enter a test case", context: context),
                                                onChanged: (text) {
                                                  valueParameter["testCaseName"] = text;
                                                },
                                              ),
                                            ),
                                          if (widget.type != "test_case" && widget.type != "batch_step" && widget.type != "manual_test")
                                            SizedBox(
                                              width: 300,
                                              child: DynamicDropdownButton(
                                                value: valueParameter["testRunExpectedResult"] ?? 1,
                                                hint: multiLanguageString(
                                                    name: "choose_a_test_run_expected_result",
                                                    defaultValue: "Choose a Test Run expected result",
                                                    context: context),
                                                items: listMapTestRunExpectedResult.map<DropdownMenuItem<int>>((dynamic result) {
                                                  return DropdownMenuItem(
                                                    value: result["value"],
                                                    child: Text(result["name"]),
                                                  );
                                                }).toList(),
                                                onChanged: (newValue) {
                                                  setState(() {
                                                    valueParameter["testRunExpectedResult"] = newValue!;
                                                  });
                                                },
                                                borderRadius: 5,
                                              ),
                                            ),
                                          if (checkTypeView())
                                            SizedBox(
                                              width: 300,
                                              child: DynamicDropdownButton(
                                                value: valueParameter["severity"] ?? "1",
                                                hint: multiLanguageString(
                                                    name: "choose_a_severity", defaultValue: "Choose a severity", context: context),
                                                items: listMapSeverity.map<DropdownMenuItem<String>>((dynamic result) {
                                                  return DropdownMenuItem(
                                                    value: result["value"],
                                                    child: Text(result["name"]),
                                                  );
                                                }).toList(),
                                                onChanged: (newValue) {
                                                  setState(() {
                                                    valueParameter["severity"] = newValue!;
                                                  });
                                                },
                                                borderRadius: 5,
                                              ),
                                            ),
                                          if (checkTypeView())
                                            SizedBox(
                                              width: 300,
                                              child: DynamicDropdownButton(
                                                value: valueParameter["priority"] ?? "3",
                                                hint: multiLanguageString(
                                                    name: "choose_a_priority", defaultValue: "Choose a priority", context: context),
                                                items: listMapPriority.map<DropdownMenuItem<String>>((dynamic result) {
                                                  return DropdownMenuItem(
                                                    value: result["value"],
                                                    child: Text(result["name"]),
                                                  );
                                                }).toList(),
                                                onChanged: (newValue) {
                                                  setState(() {
                                                    valueParameter["priority"] = newValue!;
                                                  });
                                                },
                                                borderRadius: 5,
                                              ),
                                            ),
                                          for (var parameterField in widget.resultparameterFieldList)
                                            if (parameterField["dataType"] != "RandomValue")
                                              IntrinsicWidth(
                                                child: Row(
                                                  children: [
                                                    Expanded(
                                                      child: SizedBox(
                                                        width: 300,
                                                        child: DynamicTextField(
                                                          controller: TextEditingController(text: valueParameter[parameterField["name"]]),
                                                          hintText: multiLanguageString(
                                                              name: "enter_a_parameter_field",
                                                              defaultValue: "Enter a \$0",
                                                              variables: ["${parameterField["name"]}"],
                                                              context: context),
                                                          onChanged: (text) {
                                                            valueParameter[parameterField["name"]] = text;
                                                          },
                                                          obscureText: parameterField["dataType"] == "Password",
                                                        ),
                                                      ),
                                                    ),
                                                    if (parameterField["dataType"] == "File")
                                                      ElevatedButton(
                                                          onPressed: () {
                                                            Navigator.of(context).push(
                                                              createRoute(
                                                                FindFileWidget((String valueCallback) {
                                                                  setState(() {
                                                                    valueParameter[parameterField["name"]] = valueCallback;
                                                                  });
                                                                }),
                                                              ),
                                                            );
                                                          },
                                                          child: const MultiLanguageText(name: "select_file", defaultValue: "Select file")),
                                                  ],
                                                ),
                                              ),
                                          if (widget.type == "manual_test")
                                            SizedBox(
                                              width: 300,
                                              child: DynamicDropdownButton(
                                                value: valueParameter["status"] ?? 0,
                                                hint: multiLanguageString(name: "choose_a_status", defaultValue: "Choose a status", context: context),
                                                items: [
                                                  {
                                                    "name": multiLanguageString(name: "item_pass", defaultValue: "Pass", context: context),
                                                    "value": 1
                                                  },
                                                  {
                                                    "name": multiLanguageString(name: "item_fail", defaultValue: "Fail", context: context),
                                                    "value": 0
                                                  },
                                                ].map<DropdownMenuItem<int>>((dynamic result) {
                                                  return DropdownMenuItem(
                                                    value: result["value"],
                                                    child: Text(result["name"]),
                                                  );
                                                }).toList(),
                                                onChanged: (newValue) {
                                                  setState(() {
                                                    valueParameter["status"] = newValue!;
                                                  });
                                                },
                                                borderRadius: 5,
                                              ),
                                            )
                                        ],
                                      )
                                    ],
                                  ),
                                const SizedBox(
                                  width: 10,
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    color: Colors.blue,
                                  ),
                                  child: IconButton(
                                    tooltip: multiLanguageString(name: "add", defaultValue: "Add", context: context),
                                    splashRadius: 20,
                                    onPressed: () {
                                      setState(() {
                                        widget.listValueParameter.add({});
                                        if (widget.listValueParameter.length - currentPage * rowPerPage == 1) {
                                          searchPaging(currentPage + 1);
                                        } else {
                                          searchPaging(currentPage);
                                        }
                                      });
                                    },
                                    icon: const Icon(Icons.add, color: Colors.white),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
        DynamicTablePagging(
          rowCount,
          currentPage,
          rowPerPage,
          pageChangeHandler: (page) {
            setState(() {
              searchPaging(page);
            });
          },
          rowPerPageChangeHandler: (rowPerPage) {
            setState(() {
              this.rowPerPage = rowPerPage!;
              searchPaging(1);
            });
          },
        ),
      ],
    );
  }
}
