import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';

import '../common/custom_state.dart';

class HeaderSettingWidget extends StatefulWidget {
  final String type;
  final String title;
  final int countList;
  final Function? callbackSearch;
  final Function? onBack;
  final Function? onDisableAll;
  const HeaderSettingWidget({
    Key? key,
    required this.type,
    required this.countList,
    this.callbackSearch,
    this.onBack,
    required this.title,
    this.onDisableAll,
  }) : super(key: key);

  @override
  State<HeaderSettingWidget> createState() => _HeaderSettingWidgetState();
}

class _HeaderSettingWidgetState extends CustomState<HeaderSettingWidget> {
  final TextEditingController nameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  SingleValueDropDownController statusController = SingleValueDropDownController();
  SingleValueDropDownController fileTypeController = SingleValueDropDownController();
  SingleValueDropDownController ownerController = SingleValueDropDownController();
  SingleValueDropDownController testPriorityController = SingleValueDropDownController();
  SingleValueDropDownController regressionCandidateController = SingleValueDropDownController();
  var prjProjectId = "";
  var _listFileType = [];
  bool isShowFilterForm = true;
  bool isSearched = false;
  getInfoFilter() async {
    if (widget.type == "File") {
      await getFileTypeList();
    }
    return 0;
  }

  getFileTypeList() async {
    var findRequest = {"prjProjectId": prjProjectId};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-file-type/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listFileType = response["body"]["resultList"];
      });
    }
  }

  handleCallBackSearchFunction({String type = "search"}) {
    var result = {};
    if (widget.type == "Teammate") {
      result = {
        "fullnameLike": nameController.text,
        "emailLike": emailController.text,
      };
    }
    if (widget.type == "File") {
      result = {
        "tfFileTypeId": fileTypeController.dropDownValue?.value,
        "titleLike": nameController.text,
        "owner": ownerController.dropDownValue?.value,
      };
    }
    if (widget.type == "File Type" ||
        widget.type == "Profile" ||
        widget.type == "Data Directory" ||
        widget.type == "Test batch" ||
        widget.type == "Label" ||
        widget.type == "Flow" ||
        widget.type == "Status") {
      result = {
        "status": statusController.dropDownValue?.value,
        "titleLike": nameController.text,
      };
    }
    if (widget.type == "Batch Step") {
      result = {
        "status": statusController.dropDownValue?.value,
        "titleLike": nameController.text,
        "tfTestCaseRegressionCandidate": regressionCandidateController.dropDownValue?.value,
        "tfTestCaseTestPriority": testPriorityController.dropDownValue?.value,
      };
    }
    if (widget.type == "Module" || widget.type == "Version") {
      result = {
        "titleLike": nameController.text,
        "owner": ownerController.dropDownValue?.value,
        "status": statusController.dropDownValue?.value,
      };
    }
    if (type == "disable") {
      widget.onDisableAll!(result);
    } else {
      widget.callbackSearch!(result);
    }
  }

  @override
  void initState() {
    prjProjectId = Provider.of<NavigationModel>(context, listen: false).prjProjectId ?? "";
    getInfoFilter();
    super.initState();
  }

  @override
  void dispose() {
    nameController.dispose();
    emailController.dispose();
    statusController.dispose();
    fileTypeController.dispose();
    ownerController.dispose();
    testPriorityController.dispose();
    regressionCandidateController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      List<Widget> widgetList = [
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
          child: SizedBox(
            height: 50,
            child: DynamicTextField(
              controller: nameController,
              onComplete: () => handleCallBackSearchFunction(),
              onChanged: (value) {
                setState(() {
                  nameController.text;
                });
              },
              labelText: multiLanguageString(name: "type_name", defaultValue: "\$0 name", variables: [widget.title], context: context),
              hintText: multiLanguageString(name: "by_name", defaultValue: "By name", context: context),
              suffixIcon: (nameController.text != "")
                  ? IconButton(
                      onPressed: () {
                        setState(() {
                          nameController.clear();
                          if (isSearched) {
                            handleCallBackSearchFunction();
                          }
                        });
                      },
                      icon: const Icon(Icons.clear),
                      splashRadius: 20,
                    )
                  : null,
            ),
          ),
        ),
        if (widget.type == "Teammate")
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
            child: SizedBox(
              height: 50,
              child: DynamicTextField(
                controller: emailController,
                onComplete: () => handleCallBackSearchFunction(),
                onChanged: (value) {
                  setState(() {
                    emailController.text;
                  });
                },
                labelText: multiLanguageString(name: "type_email", defaultValue: "\$0 email", variables: [widget.title], context: context),
                hintText: multiLanguageString(name: "by_email", defaultValue: "By email", context: context),
                suffixIcon: (emailController.text != "")
                    ? IconButton(
                        onPressed: () {
                          setState(() {
                            emailController.clear();
                            if (isSearched) {
                              handleCallBackSearchFunction();
                            }
                          });
                        },
                        icon: const Icon(Icons.clear),
                        splashRadius: 20,
                      )
                    : null,
              ),
            ),
          ),
        if (widget.type == "File")
          Padding(
              padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
              child: DynamicDropdownSearchClearOption(
                labelText: multiLanguageString(name: "file_type", defaultValue: "File type", context: context),
                controller: fileTypeController,
                hint: multiLanguageString(name: "search_file_type", defaultValue: "Search file Type", context: context),
                items: _listFileType.map<DropDownValueModel>((dynamic result) {
                  return DropDownValueModel(
                    value: result["id"],
                    name: result["title"],
                  );
                }).toList(),
                maxHeight: 200,
                onChanged: (dynamic newValue) {
                  if (isSearched && newValue == "") {
                    handleCallBackSearchFunction();
                  }
                },
              )),
        if (widget.type != "File" && widget.type != "Teammate")
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
            child: DynamicDropdownSearchClearOption(
              labelText: multiLanguageString(name: "search_status", defaultValue: "Search status", context: context),
              controller: statusController,
              hint: multiLanguageString(name: "search_status", defaultValue: "Search status", context: context),
              items: {
                multiLanguageString(name: "active", defaultValue: "Active", context: context): 1,
                multiLanguageString(name: "inactive", defaultValue: "Inactive", context: context): 0
              }.entries.map<DropDownValueModel>((result) {
                return DropDownValueModel(
                  value: result.value,
                  name: result.key,
                );
              }).toList(),
              maxHeight: 200,
              onChanged: (dynamic newValue) {
                if (isSearched && newValue == "") {
                  handleCallBackSearchFunction();
                }
              },
            ),
          ),
        if (widget.type == "Batch Step")
          Padding(
              padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
              child: DynamicDropdownSearchClearOption(
                labelText: multiLanguageString(name: "priority", defaultValue: "Priority", context: context),
                controller: testPriorityController,
                hint: multiLanguageString(name: "search_priority", defaultValue: "Search Priority", context: context),
                items: [
                  {"name": "0", "value": multiLanguageString(name: "item_high", defaultValue: "High", context: context)},
                  {"name": "1", "value": multiLanguageString(name: "item_medium", defaultValue: "Medium", context: context)},
                  {"name": "2", "value": multiLanguageString(name: "item_low", defaultValue: "Low", context: context)},
                ].map<DropDownValueModel>((dynamic result) {
                  return DropDownValueModel(
                    value: result["value"],
                    name: result["name"],
                  );
                }).toList(),
                onChanged: (dynamic newValue) {
                  if (isSearched && newValue == "") {
                    handleCallBackSearchFunction();
                  }
                },
                maxHeight: 200,
              )),
        if (widget.type == "Batch Step")
          Padding(
              padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
              child: DynamicDropdownSearchClearOption(
                labelText: multiLanguageString(name: "regression_candidate", defaultValue: "Regression candidate", context: context),
                controller: regressionCandidateController,
                hint: multiLanguageString(name: "search_regression_candidate", defaultValue: "Search Regression candidate", context: context),
                items: [
                  {"name": multiLanguageString(name: "none", defaultValue: "None", context: context), "value": 0},
                  {"name": multiLanguageString(name: "primary", defaultValue: "Primary", context: context), "value": 3},
                  {"name": multiLanguageString(name: "secondary", defaultValue: "Secondary", context: context), "value": 2},
                  {"name": multiLanguageString(name: "tertiary", defaultValue: "Tertiary", context: context), "value": 1},
                ].map<DropDownValueModel>((dynamic result) {
                  return DropDownValueModel(
                    value: result["value"],
                    name: result["name"],
                  );
                }).toList(),
                onChanged: (dynamic newValue) {
                  if (isSearched && newValue == "") {
                    handleCallBackSearchFunction();
                  }
                },
                maxHeight: 200,
              )),
      ];
      var itemPerRow = 3;
      if (constraints.maxWidth < 600) {
        itemPerRow = 1;
      } else if (constraints.maxWidth < 1200) {
        itemPerRow = 2;
      }
      var rowCount = (1.0 * widgetList.length / itemPerRow).ceil();
      var filterWidget = Column(
        children: [
          for (var rowI = 0; rowI < rowCount; rowI++)
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                for (var colI = 0; colI < itemPerRow; colI++)
                  rowI * itemPerRow + colI < widgetList.length ? Expanded(child: widgetList[rowI * itemPerRow + colI]) : Expanded(child: Container())
              ],
            )
        ],
      );
      Widget buttons = Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 20),
            child: ElevatedButton(
                onPressed: () {
                  isSearched = true;
                  handleCallBackSearchFunction();
                },
                child: const MultiLanguageText(name: "search", defaultValue: "Search", isLowerCase: false)),
          ),
          if (widgetList.length > 2)
            ElevatedButton(
                onPressed: () {
                  setState(() {
                    nameController.clear();
                    fileTypeController.clearDropDown();
                    statusController.clearDropDown();
                    ownerController.clearDropDown();
                    regressionCandidateController.clearDropDown();
                    testPriorityController.clearDropDown();
                    widget.callbackSearch!({});
                  });
                },
                child: const MultiLanguageText(name: "reset", defaultValue: "Reset", isLowerCase: false)),
          if (widget.type == "Batch Step")
            Padding(
              padding: const EdgeInsets.only(left: 20),
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color>(Colors.red),
                ),
                onPressed: () => handleCallBackSearchFunction(type: "disable"),
                child: const MultiLanguageText(name: "disable_all", defaultValue: "Disable all", isLowerCase: false),
              ),
            )
        ],
      );
      return Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              if (widget.onBack != null)
                IconButton(
                  onPressed: () => widget.onBack!(),
                  icon: const Icon(Icons.arrow_back),
                  padding: EdgeInsets.zero,
                  splashRadius: 16,
                ),
              Expanded(
                child: (widget.type == "Version" ||
                        widget.type == "Profile" ||
                        widget.type == "Teammate" ||
                        widget.type == "Module" ||
                        widget.type == "Environment" ||
                        widget.type == "Label")
                    ? MultiLanguageText(
                        name: "project_type_with_count",
                        defaultValue: "Project \$0 ( \$1 )",
                        variables: [widget.title, "${widget.countList}"],
                        style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500))
                    : (widget.type == "Test batch")
                        ? Text(widget.title, style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500))
                        : MultiLanguageText(
                            name: "type_with_count",
                            defaultValue: "\$0 ( \$1 )",
                            variables: [widget.title, "${widget.countList}"],
                            style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500)),
              ),
            ],
          ),
          Row(
            children: [
              MultiLanguageText(
                name: "filter_form",
                defaultValue: "Filter form",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: const Color.fromRGBO(0, 0, 0, 0.8),
                    fontSize: context.fontSizeManager.fontSizeText16,
                    letterSpacing: 2),
              ),
              IconButton(
                onPressed: () {
                  setState(() {
                    isShowFilterForm = !isShowFilterForm;
                  });
                },
                icon: isShowFilterForm ? const Icon(Icons.visibility) : const Icon(Icons.visibility_off),
                splashRadius: 1,
                tooltip: isShowFilterForm
                    ? multiLanguageString(name: "hide_filter_form", defaultValue: "Hide filter form", context: context)
                    : multiLanguageString(name: "show_filter_form", defaultValue: "Show filter form", context: context),
              )
            ],
          ),
          if (isShowFilterForm)
            (widgetList.length <= 2)
                ? Wrap(
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: [
                      for (var widget in widgetList) SizedBox(width: 450, child: widget),
                      IntrinsicWidth(
                        child: buttons,
                      )
                    ],
                  )
                : Column(
                    children: [
                      filterWidget,
                      buttons,
                    ],
                  ),
        ],
      );
    });
  }
}
