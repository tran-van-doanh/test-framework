import 'package:flutter/material.dart';

class DynamicFilterListWidget extends StatelessWidget {
  final List<Widget> widgetList;
  const DynamicFilterListWidget({Key? key, required this.widgetList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      var itemPerRow = 4;
      if (constraints.maxWidth < 600) {
        itemPerRow = 1;
      } else if (constraints.maxWidth < 800) {
        itemPerRow = 2;
      } else if (constraints.maxWidth < 1200) {
        itemPerRow = 3;
      }
      var rowCount = (1.0 * widgetList.length / itemPerRow).ceil();
      return Column(
        children: [
          for (var rowI = 0; rowI < rowCount; rowI++)
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                for (var colI = 0; colI < itemPerRow; colI++)
                  rowI * itemPerRow + colI < widgetList.length ? widgetList[rowI * itemPerRow + colI] : Expanded(child: Container())
              ],
            )
        ],
      );
    });
  }
}
