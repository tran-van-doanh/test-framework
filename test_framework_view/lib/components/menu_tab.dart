import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import '../common/custom_state.dart';
import '../model/model.dart';

extension TabListExtension on BuildContext {
  List<Map<String, String>> get insightsMenuList {
    return [
      {
        "name": multiLanguageString(name: "menu_testops_dashboard", defaultValue: "Testops Dashboard", isLowerCase: false, context: this),
        "url": "/insights/dashboard"
      },
      // {
      //   "name": multiLanguageString(name: "menu_dashboard", defaultValue: "Dashboard", isLowerCase: false, context: this),
      //   "url": "/insights/new-dashboard"
      // },
      // {"name": multiLanguageString(name: "menu_chart", defaultValue: "Chart", isLowerCase: false, context: this), "url": "/insights/chart"},
      {"name": multiLanguageString(name: "menu_reports", defaultValue: "Reports", isLowerCase: false, context: this), "url": "/insights/reports"},
    ];
  }

  List<Map<String, String>> get testMenuList {
    return [
      {
        "name": multiLanguageString(name: "menu_dashboard", defaultValue: "Dashboard", isLowerCase: false, context: this),
        "url": "/test-list/dashboard"
      },
      {"name": multiLanguageString(name: "menu_all", defaultValue: "All", isLowerCase: false, context: this), "url": "/test-list/all"},
      {
        "name": multiLanguageString(name: "menu_test_case", defaultValue: "Test Case", isLowerCase: false, context: this),
        "url": "/test-list/test_case"
      },
      {
        "name": multiLanguageString(name: "menu_scenario", defaultValue: "Scenario", isLowerCase: false, context: this),
        "url": "/test-list/test_scenario"
      },
      {"name": multiLanguageString(name: "menu_suite", defaultValue: "Suite", isLowerCase: false, context: this), "url": "/test-list/test_suite"},
      {"name": multiLanguageString(name: "menu_batch", defaultValue: "Batch", isLowerCase: false, context: this), "url": "/test-list/test_batch"},
      {"name": multiLanguageString(name: "menu_manual", defaultValue: "Manual", isLowerCase: false, context: this), "url": "/test-list/manual_test"},
      {
        "name": multiLanguageString(name: "menu_api", defaultValue: "API", isLowerCase: false, context: this),
        "url": "/test-list/test_api",
        "platform": "Api"
      },
      {"name": multiLanguageString(name: "menu_jdbc", defaultValue: "JDBC", isLowerCase: false, context: this), "url": "/test-list/test_jdbc"},
      {
        "name": multiLanguageString(name: "menu_shared_steps", defaultValue: "Shared Steps", isLowerCase: false, context: this),
        "url": "/test-list/action"
      },
      {
        "name": multiLanguageString(name: "menu_import_export", defaultValue: "Import/Export", isLowerCase: false, context: this),
        "url": "/test-list/import-export",
        "module": "PROJECT_IMPORT_EXPORT_WITH_CONTENT"
      },
    ];
  }

  List<Map<String, String>> get testRunsMenuList {
    return [
      {"name": multiLanguageString(name: "menu_dashboard", defaultValue: "Dashboard", isLowerCase: false, context: this), "url": "/runs/dashboard"},
      {"name": multiLanguageString(name: "menu_test_case", defaultValue: "Test case", isLowerCase: false, context: this), "url": "/runs/test_case"},
      {"name": multiLanguageString(name: "menu_scenario", defaultValue: "Scenario", isLowerCase: false, context: this), "url": "/runs/test_scenario"},
      {"name": multiLanguageString(name: "menu_suite", defaultValue: "Suite", isLowerCase: false, context: this), "url": "/runs/test_suite"},
      {"name": multiLanguageString(name: "menu_manual", defaultValue: "Manual", isLowerCase: false, context: this), "url": "/runs/manual_test"},
      {
        "name": multiLanguageString(name: "menu_batch", defaultValue: "Batch", isLowerCase: false, context: this),
        "url": "/runs/test_batch",
        "module": "TEST_BATCH"
      },
      {
        "name": multiLanguageString(name: "menu_bug_tracker", defaultValue: "Bug tracker", isLowerCase: false, context: this),
        "url": "/runs/bug-tracker"
      },
    ];
  }

  List<Map<String, String>> get deleteDataMenuList {
    return [
      {
        "name": multiLanguageString(name: "menu_test_case", defaultValue: "Test Case", isLowerCase: false, context: this),
        "url": "/delete-data/test_case"
      },
      {
        "name": multiLanguageString(name: "menu_scenario", defaultValue: "Scenario", isLowerCase: false, context: this),
        "url": "/delete-data/test_scenario"
      },
      {"name": multiLanguageString(name: "menu_suite", defaultValue: "Suite", isLowerCase: false, context: this), "url": "/delete-data/test_suite"},
      {"name": multiLanguageString(name: "menu_batch", defaultValue: "Batch", isLowerCase: false, context: this), "url": "/delete-data/test_batch"},
      {
        "name": multiLanguageString(name: "menu_manual", defaultValue: "Manual", isLowerCase: false, context: this),
        "url": "/delete-data/manual_test"
      },
    ];
  }

  List<Map<String, String>> get settingMenuList {
    return [
      {"name": multiLanguageString(name: "menu_set_up", defaultValue: "Set Up", isLowerCase: false, context: this), "url": "/setting/set-up"},
      {"name": multiLanguageString(name: "menu_teammate", defaultValue: "Teammate", isLowerCase: false, context: this), "url": "/setting/teammate"},
      {
        "name": multiLanguageString(name: "menu_parameter", defaultValue: "Parameter", isLowerCase: false, context: this),
        "url": "/setting/parameter"
      },
      {"name": multiLanguageString(name: "menu_module", defaultValue: "Module", isLowerCase: false, context: this), "url": "/setting/module"},
      {"name": multiLanguageString(name: "menu_version", defaultValue: "Version", isLowerCase: false, context: this), "url": "/setting/version"},
      {
        "name": multiLanguageString(name: "menu_environment", defaultValue: "Environment", isLowerCase: false, context: this),
        "url": "/setting/environment"
      },
      {"name": multiLanguageString(name: "menu_profile", defaultValue: "Profile", isLowerCase: false, context: this), "url": "/setting/profile"},
      {"name": multiLanguageString(name: "menu_label", defaultValue: "Label", isLowerCase: false, context: this), "url": "/setting/label"},
      {"name": multiLanguageString(name: "menu_project", defaultValue: "Project", isLowerCase: false, context: this), "url": "/setting/project-info"},
      // {"name": "TESTCASE STATUS", "url": "/setting/testcase-status"},
    ];
  }

  List<Map<String, String>> get testBatchMenuList {
    return [
      {"name": multiLanguageString(name: "menu_detail", defaultValue: "Detail", isLowerCase: false, context: this), "url": ""},
      {"name": multiLanguageString(name: "menu_batch_step", defaultValue: "Batch Step", isLowerCase: false, context: this), "url": "/batch-step"},
    ];
  }

  List<Map<String, String>> get profileMenuList {
    return [
      {"name": multiLanguageString(name: "menu_profile", defaultValue: "Profile", isLowerCase: false, context: this), "url": "/profile/view"},
      {"name": multiLanguageString(name: "menu_account", defaultValue: "Account", isLowerCase: false, context: this), "url": "/profile/password"},
      {"name": multiLanguageString(name: "menu_token", defaultValue: "Token", isLowerCase: false, context: this), "url": "/profile/token"},
    ];
  }

  List<Map<String, String>> get organizationSettingsMenuList {
    return [
      {"name": multiLanguageString(name: "menu_home", defaultValue: "Home", isLowerCase: false, context: this), "url": "/select"},
      {
        "name": multiLanguageString(name: "menu_projects", defaultValue: "Projects", isLowerCase: false, context: this),
        "url": "/qa-platform/setting/project"
      },
      {
        "name": multiLanguageString(name: "menu_project_type", defaultValue: "Project Type", isLowerCase: false, context: this),
        "url": "/qa-platform/setting/project-type"
      },
      {
        "name": multiLanguageString(name: "menu_value_type", defaultValue: "Value Type", isLowerCase: false, context: this),
        "url": "/qa-platform/setting/value-type"
      },
      {
        "name": multiLanguageString(name: "menu_random_type", defaultValue: "Random Type", isLowerCase: false, context: this),
        "url": "/qa-platform/setting/random-type"
      },
      {"name": multiLanguageString(name: "menu_agent", defaultValue: "Agent", isLowerCase: false, context: this), "url": "/qa-platform/agent"},
    ];
  }

  List<Map<String, String>> get settingsMenuList {
    return [
      {"name": multiLanguageString(name: "menu_home", defaultValue: "Home", isLowerCase: false, context: this), "url": "/select"},
      {"name": multiLanguageString(name: "menu_role", defaultValue: "Role", isLowerCase: false, context: this), "url": "/setting/role"},
      {"name": multiLanguageString(name: "menu_user", defaultValue: "User", isLowerCase: false, context: this), "url": "/setting/user"},
      {
        "name": multiLanguageString(name: "menu_organization", defaultValue: "Organization", isLowerCase: false, context: this),
        "url": "/setting/organization"
      },
      {
        "name": multiLanguageString(name: "menu_host_name", defaultValue: "Host name", isLowerCase: false, context: this),
        "url": "/setting/host-name"
      },
      {
        "name": multiLanguageString(name: "menu_notification_template", defaultValue: "Notification template", isLowerCase: false, context: this),
        "url": "/setting/notification-template"
      },
      {
        "name": multiLanguageString(name: "menu_bug_tracker_account", defaultValue: "Bug tracker account", isLowerCase: false, context: this),
        "url": "/setting/bug-tracker-account"
      },
      {
        "name": multiLanguageString(name: "menu_email_account", defaultValue: "Email account", isLowerCase: false, context: this),
        "url": "/setting/email-account"
      },
      {
        "name": multiLanguageString(name: "menu_email_template", defaultValue: "Email template", isLowerCase: false, context: this),
        "url": "/setting/email-template"
      },
      {"name": multiLanguageString(name: "menu_scale", defaultValue: "Scale", isLowerCase: false, context: this), "url": "/setting/scale"},
    ];
  }

  List<Map<String, String>> get workManagementSettingsMenuList {
    return [
      {"name": multiLanguageString(name: "menu_home", defaultValue: "Home", isLowerCase: false, context: this), "url": "/select"},
      {"name": multiLanguageString(name: "menu_summary", defaultValue: "Summary", isLowerCase: false, context: this), "url": "/work-mgt/summary"},
      {
        "name": multiLanguageString(name: "menu_projects", defaultValue: "Projects", isLowerCase: false, context: this),
        "url": "/work-mgt/setting/project"
      },
    ];
  }

  List<Map<String, String>> get settingsTaskMenuList {
    return [
      {"name": multiLanguageString(name: "menu_tag", defaultValue: "Tag", isLowerCase: false, context: this), "url": "/settings/tag"},
      {
        "name": multiLanguageString(name: "menu_sprint_list", defaultValue: "Sprint List", isLowerCase: false, context: this),
        "url": "/settings/sprint-list"
      },
      {
        "name": multiLanguageString(name: "menu_task_type", defaultValue: "Task type", isLowerCase: false, context: this),
        "url": "/settings/task-type"
      },
    ];
  }

  List<Map<String, String>> get rpUserMenuList {
    return [
      {"name": multiLanguageString(name: "menu_user", defaultValue: "User", isLowerCase: false, context: this), "url": "/user/user"},
      {"name": multiLanguageString(name: "menu_role", defaultValue: "Role", isLowerCase: false, context: this), "url": "/user/role"},
    ];
  }

  List<Map<String, String>> get rpDeviceMenuList {
    return [
      {"name": multiLanguageString(name: "menu_device", defaultValue: "Device", isLowerCase: false, context: this), "url": "/device/device"},
      {
        "name": multiLanguageString(name: "menu_device_type", defaultValue: "Device Type", isLowerCase: false, context: this),
        "url": "/device/device-type"
      },
    ];
  }
}

class NavMenuTabV1 extends StatefulWidget {
  final String currentUrl;
  final EdgeInsets? margin;
  final List navigationList;
  final String? preUrl;
  const NavMenuTabV1({Key? key, required this.currentUrl, this.margin, required this.navigationList, this.preUrl}) : super(key: key);

  @override
  State<NavMenuTabV1> createState() => _NavMenuTabV1State();
}

class _NavMenuTabV1State extends CustomState<NavMenuTabV1> {
  @override
  Widget build(BuildContext context) {
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    var menuChildren = <Widget>[];
    for (var navigation in widget.navigationList) {
      var name = navigation["name"]!;
      var url = navigation["url"]!;
      var module = navigation["module"];
      var platform = navigation["platform"];
      if (module != null && securityModel.hasSysOrganizationModules(module) == false) {
        continue;
      }
      if (platform != null && securityModel.hasSysOrganizationPlatforms(platform) == false) {
        continue;
      }
      var selectedUrl = url == widget.currentUrl;
      if (url == "/profile/view" || url == "/profile/password" || url == "/profile/token") {
        url = url;
      } else if (widget.preUrl != null && widget.preUrl!.isNotEmpty) {
        url = "/${navigationModel.product}/project/${navigationModel.prjProjectId ?? ""}${widget.preUrl}$url";
      } else if (navigationModel.product == "resource-pool" ||
          navigationModel.product == "workflow" ||
          navigationModel.product == "admin" ||
          navigationModel.product == "prj-360") {
        url = "/${navigationModel.product}$url";
      } else {
        url = "/${navigationModel.product}/project/${navigationModel.prjProjectId ?? ""}$url";
      }
      if (selectedUrl) {
        menuChildren.add(Container(
          margin: const EdgeInsets.only(right: 30),
          child: Container(
            decoration: const BoxDecoration(border: Border(bottom: BorderSide(color: Color(0xff3d5afe), width: 2))),
            child: NameMenuTab(
              color: const Color(0xff3d5afe),
              name: name,
              redirectUrl: () {
                navigationModel.navigate(url);
              },
            ),
          ),
        ));
      } else {
        menuChildren.add(Container(
          margin: const EdgeInsets.only(right: 30),
          child: Container(
            decoration: const BoxDecoration(border: Border(bottom: BorderSide(color: Color.fromARGB(255, 250, 250, 250), width: 2))),
            child: NameMenuTab(
              color: const Color(0xff9ea0ab),
              name: name,
              redirectUrl: () {
                navigationModel.navigate(url);
              },
            ),
          ),
        ));
      }
    }
    return Container(
      margin: widget.margin,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(children: menuChildren),
      ),
    );
  }
}

class OrganizationNavMenuTab extends StatefulWidget {
  final List navigationList;
  final EdgeInsets? margin;
  const OrganizationNavMenuTab({Key? key, this.margin, required this.navigationList}) : super(key: key);

  @override
  State<OrganizationNavMenuTab> createState() => _OrganizationNavMenuTabState();
}

class _OrganizationNavMenuTabState extends CustomState<OrganizationNavMenuTab> {
  List<String> listName = [];
  List<String> listUrl = [];

  @override
  Widget build(BuildContext context) {
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: true);
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: true);
    var menuList = [];
    for (var menu in widget.navigationList) {
      if (menu["url"] == "/work-mgt/summary") {
        menuList.add(menu);
      }
      if (menu["url"] == "/select") {
        menuList.add(menu);
      }
      if (menu["url"] == "/setting/role" && securityModel.hasUserPermission("ORGANIZATION_ROLE")) {
        menuList.add(menu);
      }
      if (menu["url"] == "/setting/user" && securityModel.hasUserPermission("ORGANIZATION_USER")) {
        menuList.add(menu);
      }
      if ((menu["url"] == "/setting/organization" || menu["url"] == "/setting/host-name") && securityModel.hasUserPermission("ORGANIZATION_ADMIN")) {
        menuList.add(menu);
      }
      if (menu["url"] == "/setting/bug-tracker-account" && securityModel.hasUserPermission("ORGANIZATION_BUG_TRACKER_ACCOUNT")) {
        menuList.add(menu);
      }
      if ((menu["url"] == "/setting/email-account" || menu["url"] == "/setting/email-template" || menu["url"] == "/setting/notification-template") &&
          securityModel.hasUserPermission("ORGANIZATION_TEMPLATE")) {
        menuList.add(menu);
      }
      if (menu["url"] == "/setting/scale" || menu["url"] == "/qa-platform/agent") {
        menuList.add(menu);
      }
      if (menu["url"] == "/qa-platform/setting/project" || menu["url"] == "/work-mgt/setting/project") {
        menuList.add(menu);
      }
      if (menu["url"] == "/qa-platform/setting/project-type" && securityModel.hasUserPermission("ORGANIZATION_PROJECT_TYPE")) {
        menuList.add(menu);
      }
      if (menu["url"] == "/qa-platform/setting/value-type" && securityModel.hasUserPermission("ORGANIZATION_VALUE_TYPE")) {
        menuList.add(menu);
      }
      if (menu["url"] == "/qa-platform/setting/random-type" && securityModel.hasUserPermission("ORGANIZATION_RANDOM_TYPE")) {
        menuList.add(menu);
      }
    }
    var currentUrl = "/qa-platform/setting/project";
    if (navigationModel.routerItemHistory.isNotEmpty) {
      currentUrl = navigationModel.routerItemHistory.last.url;
    }
    return Container(
      margin: widget.margin,
      child: Row(children: [
        for (var menu in menuList)
          NameMenuTab(
            color: currentUrl == menu["url"] ? const Color(0xff3d5afe) : const Color(0xff9ea0ab),
            name: menu["name"] ?? "",
            redirectUrl: () {
              navigationModel.navigate(menu["url"] ?? "");
            },
          )
      ]),
    );
  }
}

class NameMenuTab extends StatelessWidget {
  final String name;
  final Color color;
  final Function? redirectUrl;
  const NameMenuTab({Key? key, required this.name, required this.color, this.redirectUrl}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return TextButton(
      child: Text(name,
          textAlign: TextAlign.center,
          style: TextStyle(
            color: color,
            fontSize: context.fontSizeManager.fontSizeText16,
            fontWeight: FontWeight.w700,
          )),
      onPressed: () async {
        if (redirectUrl != null) {
          redirectUrl!();
        }
      },
    );
  }
}
