import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/model/model.dart';

class MultiLanguageText extends StatelessWidget {
  final String name;
  final String defaultValue;
  final List<String>? variables;
  final TextStyle? style;
  final bool isLowerCase;
  final String? suffiText;
  final TextAlign? align;
  final int? maxLines;
  final TextOverflow? overflow;
  final bool? softWrap;
  const MultiLanguageText({
    Key? key,
    required this.name,
    required this.defaultValue,
    this.style,
    this.variables,
    this.isLowerCase = true,
    this.suffiText,
    this.align,
    this.maxLines,
    this.overflow,
    this.softWrap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(
      builder: (context, languageModel, _) {
        String text = languageModel.translate(name, defaultValue, variables: variables);
        if (suffiText != null) {
          text += suffiText ?? "";
        }
        return Text(
          isLowerCase ? text : text.toUpperCase(),
          style: style,
          textAlign: align,
          maxLines: maxLines,
          overflow: overflow,
          softWrap: softWrap,
        );
      },
    );
  }
}

String multiLanguageString({
  required String name,
  required String defaultValue,
  List<String>? variables,
  bool isLowerCase = true,
  required BuildContext context,
}) {
  final languageModel = Provider.of<LanguageModel>(context, listen: false);
  String text = languageModel.translate(name, defaultValue, variables: variables);
  return isLowerCase ? text : text.toUpperCase();
}

class LanguagePicker extends StatefulWidget {
  const LanguagePicker({Key? key}) : super(key: key);

  @override
  State<LanguagePicker> createState() => _LanguagePickerState();
}

class _LanguagePickerState extends State<LanguagePicker> {
  List languages = [
    {
      "value": "en",
      "widget": Image.asset("assets/images/england.png"),
    },
    {
      "value": "vi",
      "widget": Image.asset("assets/images/vietnam.png"),
    },
    {"value": "jp", "widget": Image.asset("assets/images/japan.png")},
  ];
  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(
      builder: (context, languageModel, child) {
        return PopupMenuButton(
          tooltip: multiLanguageString(name: "language", defaultValue: "Language", context: context),
          padding: const EdgeInsets.all(0),
          constraints: const BoxConstraints(minWidth: 45, maxWidth: 45),
          color: Colors.lightGreen[100],
          itemBuilder: (context) => [
            for (var language in languages)
              PopupMenuItem(
                onTap: () {
                  languageModel.setLanguage(language["value"]);
                },
                value: language["value"],
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: language["widget"],
              ),
          ],
          initialValue: languageModel.currentLanguage,
          splashRadius: 1,
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 2),
            decoration: BoxDecoration(
              color: const Color.fromRGBO(216, 218, 229, .7),
              borderRadius: BorderRadius.circular(10),
            ),
            child: languages.firstWhere((element) => element["value"] == languageModel.currentLanguage)["widget"],
          ),
        );
      },
    );
  }
}
