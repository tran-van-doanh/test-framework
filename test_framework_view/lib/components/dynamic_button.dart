import 'package:flutter/material.dart';
import 'package:test_framework_view/components/multi_language_text.dart';

class ButtonCancel extends StatelessWidget {
  const ButtonCancel({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 5),
      height: 40,
      width: 120,
      child: ElevatedButton(
        onPressed: () {
          Navigator.pop(context);
        },
        style: ElevatedButton.styleFrom(
          side: const BorderSide(color: Color.fromRGBO(209, 212, 216, 1)),
          backgroundColor: Colors.white,
          foregroundColor: const Color.fromRGBO(61, 90, 254, 1),
        ),
        child: const MultiLanguageText(
          name: "cancel",
          defaultValue: "Cancel",
          isLowerCase: false,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}

class DynamicButton extends StatelessWidget {
  final Function? onPressed;
  final String? label;
  final String? name;
  const DynamicButton({Key? key, this.onPressed, this.label, this.name}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: const BoxConstraints(minHeight: 40, minWidth: 120),
      child: ElevatedButton(
        onPressed: () {
          onPressed!();
        },
        child: name!.isEmpty
            ? Text(label!, style: const TextStyle(fontWeight: FontWeight.bold, letterSpacing: 2))
            : MultiLanguageText(
                name: name!, defaultValue: label!, isLowerCase: false, style: const TextStyle(fontWeight: FontWeight.bold, letterSpacing: 2)),
      ),
    );
  }
}

class ButtonSave extends StatelessWidget {
  final Function onPressed;
  const ButtonSave({Key? key, required this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(right: 5),
      height: 40,
      width: 120,
      child: ElevatedButton(
        onPressed: () {
          onPressed();
        },
        child: const MultiLanguageText(
          name: "ok",
          defaultValue: "Ok",
          isLowerCase: false,
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }
}
