import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/components/dashboard/dynamic_dashboard.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/resource-pool/insights/option_dashboard_widget.dart';
import 'package:test_framework_view/type.dart';

class AdminDashBoardPageWidget extends StatefulWidget {
  const AdminDashBoardPageWidget({Key? key}) : super(key: key);

  @override
  State<AdminDashBoardPageWidget> createState() => _AdminDashBoardPageWidgetState();
}

class _AdminDashBoardPageWidgetState extends State<AdminDashBoardPageWidget> {
  List dashboardList = [];
  String? dashboardSelectedId;
  Map<dynamic, dynamic> requestBody = {};
  @override
  void initState() {
    super.initState();
    getInfoPage();
  }

  handleAddDashboard(result) async {
    var response = await httpPut("/test-framework-api/admin/report/rpt-dashboard", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        getInfoPage();
      }
    }
  }

  handleEditDashboard(result, id) async {
    var response = await httpPost("/test-framework-api/admin/report/rpt-dashboard/$id", result, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      if (response["body"].containsKey("errorMessage") && mounted) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        getDashboardList();
        return true;
      }
    }
    return false;
  }

  getInfoPage() async {
    await getDashboardList();
    if (dashboardList.isNotEmpty) {
      setState(() {
        dashboardSelectedId = dashboardList[0]["id"];
      });
    }
  }

  getDashboardList() async {
    var findRequest = {};
    for (var key in requestBody.keys) {
      if (requestBody[key] != null && requestBody[key].isNotEmpty) {
        findRequest[key] = requestBody[key];
      }
    }
    var response = await httpPost("/test-framework-api/user/report/rpt-dashboard/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false && response["body"]["resultList"].isNotEmpty) {
      setState(() {
        dashboardList = response["body"]["resultList"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return ListView(
        padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 30),
            child: MultiLanguageText(
              name: "dashboard",
              defaultValue: "Dashboard",
              style: TextStyle(
                fontSize: context.fontSizeManager.fontSizeText30,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          Row(
            children: [
              if (dashboardList.length > 1)
                Expanded(
                  child: DynamicDropdownButton(
                    value: dashboardSelectedId,
                    hint: multiLanguageString(name: "choose_a_dashboard", defaultValue: "Choose a dashboard", context: context),
                    items: dashboardList.map<DropdownMenuItem<String>>((dynamic result) {
                      return DropdownMenuItem(
                        value: result["id"],
                        child: Text(result["title"]),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        dashboardSelectedId = newValue!;
                      });
                    },
                    borderRadius: 5,
                  ),
                ),
              if (dashboardSelectedId != null)
                const SizedBox(
                  width: 10,
                ),
              if (dashboardSelectedId != null)
                ElevatedButton(
                  onPressed: () => Navigator.of(context).push(
                    createRoute(
                      OptionDashboardWidget(
                        id: dashboardSelectedId,
                        type: "edit",
                        title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                        callbackOptionTest: (result) {
                          handleEditDashboard(result, result["id"]);
                        },
                      ),
                    ),
                  ),
                  child: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                ),
              const SizedBox(
                width: 10,
              ),
              ElevatedButton(
                onPressed: () => Navigator.of(context).push(
                  createRoute(
                    OptionDashboardWidget(
                      type: "create",
                      title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                      callbackOptionTest: (result) {
                        handleAddDashboard(result);
                      },
                    ),
                  ),
                ),
                child: const MultiLanguageText(name: "add", defaultValue: "Add"),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          if (dashboardSelectedId != null)
            DynamicDashboardWidget(
                key: Key("${dashboardSelectedId!}:${jsonEncode(requestBody)}"), dashboardSelectedId: dashboardSelectedId!, requestBody: requestBody),
        ],
      );
    });
  }
}
