import 'package:flutter/material.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';

class DialogConfigTypeWidget extends StatefulWidget {
  final dynamic config;
  final Function onSave;
  final String? parentType;
  const DialogConfigTypeWidget({Key? key, this.config, required this.onSave, this.parentType}) : super(key: key);

  @override
  State<DialogConfigTypeWidget> createState() => _DialogConfigTypeWidgetState();
}

class _DialogConfigTypeWidgetState extends State<DialogConfigTypeWidget> {
  Map<dynamic, dynamic> mapConfig = {};
  List reportList = [];

  @override
  void initState() {
    super.initState();
    getInitPage();
  }

  getInitPage() {
    setState(() {
      mapConfig = Map.from(widget.config);
      if (mapConfig["type"] == "Report") {
        getDashBoardList();
      }
    });
  }

  getDashBoardList() async {
    var findRequest = {
      "dashboardType": "resource_pool",
    };
    var response = await httpPost("/test-framework-api/user/report/rpt-report/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      if (response["body"]["resultList"].isNotEmpty) {
        setState(() {
          reportList = response["body"]["resultList"];
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: const EdgeInsets.all(0),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "edit_config",
            defaultValue: "Edit config",
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Icon(
                Icons.close,
                color: Colors.black,
              ))
        ],
      ),
      content: Container(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
        decoration: const BoxDecoration(
          border: Border(
            top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
            bottom: BorderSide(
              color: Color.fromRGBO(216, 218, 229, 1),
            ),
          ),
        ),
        margin: const EdgeInsets.only(top: 20, bottom: 10),
        width: 1000,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (widget.parentType == "Row")
                Row(
                  children: [
                    const Expanded(
                        flex: 2,
                        child: MultiLanguageText(
                            name: "expand",
                            defaultValue: "Expand",
                            suffiText: " : ",
                            style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
                    Expanded(
                      flex: 8,
                      child: DynamicTextField(
                        keyboardType: TextInputType.number,
                        controller: TextEditingController(text: (mapConfig["expand"] ?? 1).toString()),
                        hintText: multiLanguageString(name: "enter_an_expand", defaultValue: "Enter an expand", context: context),
                        onChanged: (text) {
                          mapConfig["expand"] = int.parse(text);
                        },
                      ),
                    ),
                  ],
                ),
              if (widget.parentType == "Row")
                const SizedBox(
                  height: 10,
                ),
              Row(
                children: [
                  const Expanded(
                      flex: 2,
                      child: MultiLanguageText(
                          name: "type",
                          defaultValue: "Type",
                          suffiText: " : ",
                          style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
                  Expanded(
                    flex: 8,
                    child: DynamicDropdownButton(
                      value: mapConfig["type"],
                      hint: multiLanguageString(name: "choose_a_type", defaultValue: "Choose a type", context: context),
                      items: ["Row", "Column", "Report"].map<DropdownMenuItem<String>>((String result) {
                        return DropdownMenuItem(
                          value: result,
                          child: Text(result),
                        );
                      }).toList(),
                      onChanged: (newValue) {
                        setState(() {
                          mapConfig["type"] = newValue!;
                        });
                        if (mapConfig["type"] == "Report") getDashBoardList();
                      },
                      borderRadius: 5,
                    ),
                  ),
                ],
              ),
              if (mapConfig["type"] == "Report")
                Column(
                  children: [
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        const Expanded(
                            flex: 2,
                            child: MultiLanguageText(
                                name: "report",
                                defaultValue: "Report",
                                suffiText: " : ",
                                style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
                        Expanded(
                          flex: 8,
                          child: DynamicDropdownButton(
                            value: mapConfig["rptReportId"],
                            hint: multiLanguageString(name: "choose_a_report", defaultValue: "Choose a report", context: context),
                            items: reportList.map<DropdownMenuItem<String>>((dynamic result) {
                              return DropdownMenuItem(
                                value: result["id"],
                                child: Text(result["title"]),
                              );
                            }).toList(),
                            onChanged: (newValue) {
                              setState(() {
                                mapConfig["rptReportId"] = newValue!;
                              });
                            },
                            borderRadius: 5,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    CheckboxListTile(
                      contentPadding: const EdgeInsets.symmetric(horizontal: 0),
                      controlAffinity: ListTileControlAffinity.leading,
                      value: mapConfig["isShowTitle"] ?? false,
                      onChanged: (bool? value) {
                        setState(() {
                          mapConfig["isShowTitle"] = value!;
                        });
                      },
                      title: const MultiLanguageText(
                        name: 'is_show_title',
                        defaultValue: 'Is show title',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color.fromRGBO(87, 94, 117, 1),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    CheckboxListTile(
                      contentPadding: const EdgeInsets.symmetric(horizontal: 0),
                      controlAffinity: ListTileControlAffinity.leading,
                      value: mapConfig["isShowRefreshIcon"] ?? false,
                      onChanged: (bool? value) {
                        setState(() {
                          mapConfig["isShowRefreshIcon"] = value!;
                        });
                      },
                      title: const MultiLanguageText(
                        name: 'is_show_refresh_icon',
                        defaultValue: 'Is show refresh icon',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color.fromRGBO(87, 94, 117, 1),
                        ),
                      ),
                    ),
                    // const SizedBox(
                    //   height: 10,
                    // ),
                    // CheckboxListTile(
                    //   contentPadding: const EdgeInsets.symmetric(horizontal: 0),
                    //   controlAffinity: ListTileControlAffinity.leading,
                    //   value: mapConfig["isShowDownload"] ?? false,
                    //   onChanged: (bool? value) {
                    //     setState(() {
                    //       mapConfig["isShowDownload"] = value!;
                    //     });
                    //   },
                    //   title: const MultiLanguageText(
                    //     name: 'is_show_download_button',
                    //     defaultValue: 'Is show download button',
                    //     style: TextStyle(
                    //       fontWeight: FontWeight.bold,
                    //       color: Color.fromRGBO(87, 94, 117, 1),
                    //     ),
                    //   ),
                    // ),
                  ],
                ),
            ],
          ),
        ),
      ),
      actionsAlignment: MainAxisAlignment.center,
      actions: [
        ButtonSave(
          onPressed: () {
            widget.onSave(mapConfig);
            Navigator.pop(context);
          },
        ),
        Container(
          margin: const EdgeInsets.only(left: 5),
          height: 40,
          width: 120,
          child: ElevatedButton(
            onPressed: () {
              Navigator.pop(context);
            },
            style: ElevatedButton.styleFrom(
                side: const BorderSide(color: Color.fromRGBO(209, 212, 216, 1)),
                backgroundColor: Colors.white,
                foregroundColor: const Color.fromRGBO(61, 90, 254, 1)),
            child: const MultiLanguageText(
              name: "cancel",
              defaultValue: "Cancel",
              isLowerCase: false,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ],
    );
  }
}
