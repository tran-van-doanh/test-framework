import 'package:flutter/material.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';

class DialogReportParameterWidget extends StatefulWidget {
  final String rptReportId;
  final Map<dynamic, dynamic> parameters;
  final Function onSave;
  const DialogReportParameterWidget({Key? key, required this.rptReportId, required this.onSave, required this.parameters}) : super(key: key);

  @override
  State<DialogReportParameterWidget> createState() => _DialogReportParameterWidgetState();
}

class _DialogReportParameterWidgetState extends State<DialogReportParameterWidget> {
  List parametersList = [];
  @override
  void initState() {
    super.initState();
    getInitPage();
  }

  getInitPage() {
    getReportInfo();
  }

  getReportInfo() async {
    var reportResponse = await httpGet("/test-framework-api/user/report/rpt-report/${widget.rptReportId}", context);
    var report = reportResponse["body"]["result"];
    var reportDisplayConfig = report["reportDisplayConfig"];
    if (reportDisplayConfig["parametersList"] != null && reportDisplayConfig["parametersList"].isNotEmpty) {
      setState(() {
        parametersList = reportDisplayConfig["parametersList"];
        for (var parameter in parametersList) {
          if (widget.parameters.containsKey(parameter["name"])) {
            parameter["value"] = widget.parameters[parameter["name"]];
          }
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: const EdgeInsets.all(0),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "edit_parameter",
            defaultValue: "Edit parameter",
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Icon(
                Icons.close,
                color: Colors.black,
              ))
        ],
      ),
      content: Container(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
        decoration: const BoxDecoration(
          border: Border(
            top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
            bottom: BorderSide(
              color: Color.fromRGBO(216, 218, 229, 1),
            ),
          ),
        ),
        margin: const EdgeInsets.only(top: 20, bottom: 10),
        width: 1000,
        child: SingleChildScrollView(
          child: Column(
            children: [
              for (Map parameter in parametersList)
                Row(
                  children: [
                    Container(
                      margin: EdgeInsets.only(bottom: parametersList.last != parameter ? 0 : 10),
                      alignment: Alignment.centerLeft,
                      width: 200,
                      height: 48,
                      child: Text(
                        parameter["name"],
                        maxLines: 1,
                        style: Style(context).styleTextDataColumn,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(bottom: parametersList.last != parameter ? 0 : 10),
                        child: DynamicTextField(
                          controller: TextEditingController(text: parameter["value"]),
                          hintText: multiLanguageString(
                              name: "enter_a_parameter_field", defaultValue: "Enter a \$0", variables: ["${parameter["name"]}"], context: context),
                          onChanged: (text) {
                            parameter["value"] = text;
                          },
                        ),
                      ),
                    ),
                  ],
                ),
            ],
          ),
        ),
      ),
      actionsAlignment: MainAxisAlignment.center,
      actions: [
        ButtonSave(
          onPressed: () {
            var mapParametersList = {};
            for (var parameter in parametersList) {
              if (parameter["value"] != null && parameter["value"].isNotEmpty) {
                mapParametersList.addAll({parameter["name"]: parameter["value"]});
              }
            }
            widget.onSave(mapParametersList);
            Navigator.pop(context);
          },
        ),
        Container(
          margin: const EdgeInsets.only(left: 5),
          height: 40,
          width: 120,
          child: ElevatedButton(
            onPressed: () {
              Navigator.pop(context);
            },
            style: ElevatedButton.styleFrom(
                side: const BorderSide(color: Color.fromRGBO(209, 212, 216, 1)),
                backgroundColor: Colors.white,
                foregroundColor: const Color.fromRGBO(61, 90, 254, 1)),
            child: const MultiLanguageText(
              name: "cancel",
              defaultValue: "Cancel",
              isLowerCase: false,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ],
    );
  }
}
