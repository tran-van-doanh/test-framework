import 'dart:io';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/custom_state.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/common/file_download.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/extension/hex_color.dart';
import 'package:pie_chart/pie_chart.dart' as pie;
import 'package:test_framework_view/model/model.dart';

class DynamicReportWidget extends StatefulWidget {
  final String reportSelectedId;
  final Map<dynamic, dynamic> requestBody;
  final Map<dynamic, dynamic> parameters;
  final bool isShowRefreshIcon;
  final bool isShowTitle;
  final bool isShowDownload;
  const DynamicReportWidget(
      {Key? key,
      required this.reportSelectedId,
      required this.requestBody,
      this.isShowRefreshIcon = false,
      this.isShowTitle = false,
      this.isShowDownload = false,
      required this.parameters})
      : super(key: key);

  @override
  State<DynamicReportWidget> createState() => _DynamicReportWidgetState();
}

class _DynamicReportWidgetState extends CustomState<DynamicReportWidget> {
  dynamic report;
  dynamic reportDisplayConfig;
  @override
  void initState() {
    super.initState();
    getReportInfo();
  }

  getReportInfo() async {
    var reportResponse = await httpGet("/test-framework-api/user/report/rpt-report/${widget.reportSelectedId}", context);
    var report = reportResponse["body"]["result"];
    var reportDisplayConfig = report["reportDisplayConfig"];
    setState(() {
      this.report = report;
      this.reportDisplayConfig = reportDisplayConfig;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (report == null || reportDisplayConfig == null) {
      return const SizedBox.shrink();
    }
    Widget reportWidget = const SizedBox.shrink();
    if (report["reportType"] == "dataSheet") {
      reportWidget = DataSheetWidget(
        report: report,
        requestBody: widget.requestBody,
        parameters: widget.parameters,
        isShowRefreshIcon: widget.isShowRefreshIcon,
        isShowTitle: widget.isShowTitle,
      );
    }
    if (report["reportType"] == "chart") {
      reportWidget = ChartWidget(
        report: report,
        requestBody: widget.requestBody,
        parameters: widget.parameters,
        isShowRefreshIcon: widget.isShowRefreshIcon,
        isShowTitle: widget.isShowTitle,
      );
    }
    if (report["reportType"] == "dataTable") {
      reportWidget = DataTableWidget(
        report: report,
        requestBody: widget.requestBody,
        parameters: widget.parameters,
        isShowRefreshIcon: widget.isShowRefreshIcon,
        isShowTitle: widget.isShowTitle,
      );
    }
    if (report["reportType"] == "data") {
      reportWidget = DataWidget(
        report: report,
        requestBody: widget.requestBody,
        parameters: widget.parameters,
        isShowRefreshIcon: widget.isShowRefreshIcon,
        isShowTitle: widget.isShowTitle,
      );
    }
    return Column(
      children: [
        if (widget.isShowDownload && (report["reportType"] == "dataTable" || report["reportType"] == "dataSheet")) downloadBtnWidget(context),
        reportWidget,
      ],
    );
  }

  Container downloadBtnWidget(BuildContext context) {
    return Container(
      alignment: Alignment.centerRight,
      height: 40,
      child: FloatingActionButton.extended(
        heroTag: null,
        onPressed: () async {
          NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
          SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: false);
          Map<dynamic, dynamic> requestBody = Map.from(widget.requestBody);
          requestBody.addAll(widget.parameters);
          var parametersList = reportDisplayConfig["parametersList"];
          if (parametersList != null) {
            for (var parameter in parametersList) {
              if (requestBody[parameter["name"]] != null) {
                continue;
              }
              var defaultValue = parameter["defaultValue"];
              if (defaultValue != null) {
                requestBody[parameter["name"]] = defaultValue;
              }
              var valueType = parameter["valueType"];
              if ("currentDateTime" == valueType) {
                var value = DateTime.now();
                if (parameter["addMinutes"] != null) {
                  value = value.add(Duration(minutes: parameter["addMinutes"]));
                }
                if (parameter["addHours"] != null) {
                  value = value.add(Duration(hours: parameter["addHours"]));
                }
                if (parameter["addDays"] != null) {
                  value = value.add(Duration(days: parameter["addDays"]));
                }
                requestBody[parameter["name"]] = value.toUtc().toIso8601String();
              }
              if ("currentPrjProjectId" == valueType) {
                if (navigationModel.prjProjectId != null) {
                  requestBody[parameter["name"]] = navigationModel.prjProjectId;
                }
              }
            }
          }
          File? downloadedFile = await httpDownloadFileWithBodyRequest(
              context,
              "/test-framework-api/user/report-file/rpt-report/${report["id"]}/report-excel",
              securityModel.authorization,
              'Output.xlsx',
              requestBody);
          if (downloadedFile != null && mounted) {
            var snackBar = SnackBar(
              content: MultiLanguageText(name: "file_downloaded_to", defaultValue: "File downloaded to \$0 .", variables: ["$downloadedFile"]),
            );
            ScaffoldMessenger.of(this.context).showSnackBar(snackBar);
          }
        },
        icon: const FaIcon(FontAwesomeIcons.fileArrowDown, color: Color.fromRGBO(49, 49, 49, 1), size: 20),
        label: MultiLanguageText(
          name: "download",
          defaultValue: "Download",
          isLowerCase: false,
          style: TextStyle(fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
        ),
        backgroundColor: const Color.fromARGB(175, 8, 196, 80),
        elevation: 0,
        hoverElevation: 0,
        tooltip: multiLanguageString(name: "export_to_excel", defaultValue: "Export to Excel", context: context),
      ),
    );
  }
}

class ChartWidget extends StatefulWidget {
  final dynamic report;
  final Map<dynamic, dynamic> requestBody;
  final Map<dynamic, dynamic> parameters;
  final bool isShowRefreshIcon;
  final bool isShowTitle;
  const ChartWidget(
      {Key? key,
      required this.report,
      required this.requestBody,
      required this.isShowRefreshIcon,
      required this.isShowTitle,
      required this.parameters})
      : super(key: key);

  @override
  State<ChartWidget> createState() => _ChartWidgetState();
}

class _ChartWidgetState extends CustomState<ChartWidget> {
  dynamic chartData;
  dynamic report;
  dynamic reportDisplayConfig;

  @override
  void initState() {
    super.initState();
    report = widget.report;
    reportDisplayConfig = report["reportDisplayConfig"];
    reloadData();
  }

  reloadData() async {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    Map<dynamic, dynamic> requestBody = Map.from(widget.requestBody);
    requestBody.addAll(widget.parameters);
    var parametersList = reportDisplayConfig["parametersList"];
    if (parametersList != null) {
      for (var parameter in parametersList) {
        if (requestBody[parameter["name"]] != null) {
          continue;
        }
        var defaultValue = parameter["defaultValue"];
        if (defaultValue != null) {
          requestBody[parameter["name"]] = defaultValue;
        }
        var valueType = parameter["valueType"];
        if ("currentDateTime" == valueType) {
          var value = DateTime.now();
          if (parameter["addMinutes"] != null) {
            value = value.add(Duration(minutes: parameter["addMinutes"]));
          }
          if (parameter["addHours"] != null) {
            value = value.add(Duration(hours: parameter["addHours"]));
          }
          if (parameter["addDays"] != null) {
            value = value.add(Duration(days: parameter["addDays"]));
          }
          requestBody[parameter["name"]] = value.toUtc().toIso8601String();
        }
        if ("currentPrjProjectId" == valueType) {
          if (navigationModel.prjProjectId != null) {
            requestBody[parameter["name"]] = navigationModel.prjProjectId;
          }
        }
      }
    }
    var dataResponse = await httpPost("/test-framework-api/user/report/rpt-report/${report["id"]}/data", requestBody, context);
    var chartData = dataResponse["body"];
    setState(() {
      this.chartData = chartData;
    });
  }

  getAxisText(data, axis) {
    if (axis["field"] != null) {
      var value = data[axis["field"]];
      if (axis["dateFormat"] != null) {
        value = DateFormat(reportDisplayConfig["xAxis"]["dateFormat"]).format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(value).toLocal());
      }
      return value;
    } else {
      return axis["label"];
    }
  }

  getTooltip(dynamic data, int seriesIndex) {
    String text = "";
    if (reportDisplayConfig["xAxis"]["field"] != null) {
      if (reportDisplayConfig["xAxis"]["dateFormat"] != null) {
        text = DateFormat(reportDisplayConfig["xAxis"]["dateFormat"])
            .format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(data[reportDisplayConfig["xAxis"]["field"]]).toLocal());
      } else {
        text = "${data[reportDisplayConfig["xAxis"]["field"]]}";
      }
    } else {
      text = "$text${reportDisplayConfig["xAxis"]["label"]}";
    }
    if (reportDisplayConfig["yAxisList"] != null && reportDisplayConfig["yAxisList"].length > 1) {
      if (text.isNotEmpty) {
        text = "$text : ";
      }
      text = "$text${reportDisplayConfig["yAxisList"][seriesIndex]["label"]}";
    }
    if (text.isNotEmpty) {
      text = "$text : ";
    }
    text = "$text${data[reportDisplayConfig["yAxisList"][seriesIndex]["field"]]}";
    return text;
  }

  @override
  Widget build(BuildContext context) {
    if (report == null || reportDisplayConfig == null || chartData == null) {
      return const SizedBox.shrink();
    }
    if (reportDisplayConfig["displayType"] == "barChart") {
      TooltipBehavior tooltipBehavior = TooltipBehavior(
          enable: true,
          animationDuration: 500,
          duration: 1000,
          color: Colors.transparent,
          builder: (dynamic data, dynamic point, dynamic series, int pointIndex, int seriesIndex) {
            return Container(
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: HexColor.fromHex(reportDisplayConfig["yAxisList"][seriesIndex]["color"] ?? "#ff0000"),
              ),
              child: Text(
                getTooltip(data, seriesIndex),
                style: TextStyle(
                    color: HexColor.fromHex(reportDisplayConfig["yAxisList"][seriesIndex]["colorText"] ?? "#000000"),
                    fontSize: context.fontSizeManager.fontSizeText16,
                    fontWeight: FontWeight.w700),
              ),
            );
          });
      return Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                if (widget.isShowTitle)
                  Text(
                    report["title"],
                    style: Style(context).styleTitleDialog,
                  ),
                if (widget.isShowRefreshIcon)
                  GestureDetector(
                    child: const Icon(Icons.refresh),
                    onTap: () => reloadData(),
                  ),
              ],
            ),
            Container(
              padding: const EdgeInsets.only(top: 20),
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: const Color(0xffEEEFF4)),
              child: SfCartesianChart(
                  legend: const Legend(isVisible: true),
                  onLegendItemRender: (args) {
                    args.color = HexColor.fromHex(reportDisplayConfig["yAxisList"][args.seriesIndex]["color"] ?? "#ff0000");
                  },
                  tooltipBehavior: tooltipBehavior,
                  primaryXAxis: const CategoryAxis(),
                  series: [
                    for (var yAxis in reportDisplayConfig["yAxisList"] ?? [])
                      ColumnSeries<dynamic, String>(
                          name: yAxis["label"],
                          animationDuration: 2000,
                          width: 0.3,
                          enableTooltip: true,
                          borderRadius: const BorderRadius.only(topRight: Radius.circular(5.0), topLeft: Radius.circular(5.0)),
                          dataSource: chartData!,
                          xValueMapper: (dynamic data, _) => getAxisText(data, reportDisplayConfig["xAxis"]),
                          yValueMapper: (dynamic data, _) => data[yAxis["field"]],
                          pointColorMapper: (dynamic data, _) => HexColor.fromHex(yAxis["color"] ?? "#ff0000"))
                  ]),
            ),
          ],
        ),
      );
    }
    if (reportDisplayConfig["displayType"] == "lineChart") {
      TooltipBehavior tooltipBehavior = TooltipBehavior(
          enable: true,
          animationDuration: 500,
          duration: 1000,
          color: Colors.transparent,
          builder: (dynamic data, dynamic point, dynamic series, int pointIndex, int seriesIndex) {
            return Container(
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: HexColor.fromHex(reportDisplayConfig["yAxisList"][seriesIndex]["color"] ?? "#ff0000"),
              ),
              child: Text(
                getTooltip(data, seriesIndex),
                style: TextStyle(
                    color: HexColor.fromHex(reportDisplayConfig["yAxisList"][seriesIndex]["colorText"] ?? "#000000"),
                    fontSize: context.fontSizeManager.fontSizeText16,
                    fontWeight: FontWeight.w700),
              ),
            );
          });
      return Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                if (widget.isShowTitle)
                  Text(
                    report["title"],
                    style: Style(context).styleTitleDialog,
                  ),
                if (widget.isShowRefreshIcon)
                  GestureDetector(
                    child: const Icon(Icons.refresh),
                    onTap: () => reloadData(),
                  ),
              ],
            ),
            SfCartesianChart(
              legend: const Legend(isVisible: true),
              onLegendItemRender: (args) {
                args.color = HexColor.fromHex(reportDisplayConfig["yAxisList"][args.seriesIndex]["color"] ?? "#ff0000");
              },
              primaryXAxis: const CategoryAxis(),
              tooltipBehavior: tooltipBehavior,
              series: [
                for (var yAxis in reportDisplayConfig["yAxisList"])
                  LineSeries<dynamic, String>(
                      name: yAxis["label"],
                      enableTooltip: true,
                      dataSource: chartData!,
                      xValueMapper: (dynamic data, _) => data[reportDisplayConfig["xAxis"]["field"]],
                      yValueMapper: (dynamic data, _) => data[yAxis["field"]],
                      pointColorMapper: (dynamic data, _) => HexColor.fromHex(yAxis["color"] ?? "#ff0000")),
              ],
            ),
          ],
        ),
      );
    }
    if (reportDisplayConfig["displayType"] == "pieChart") {
      var colorList = const [
        Color(0xFFff7675),
        Color(0xFF74b9ff),
        Color(0xFF55efc4),
        Color(0xFFffeaa7),
        Color(0xFFa29bfe),
        Color(0xFFfd79a8),
        Color(0xFFe17055),
        Color(0xFF00b894),
      ];
      return (chartData != null && chartData.isNotEmpty)
          ? Container(
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      if (widget.isShowTitle)
                        Text(
                          report["title"],
                          style: Style(context).styleTitleDialog,
                        ),
                      if (widget.isShowRefreshIcon)
                        GestureDetector(
                          child: const Icon(Icons.refresh),
                          onTap: () => reloadData(),
                        ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  pie.PieChart(
                    dataMap: {
                      for (var data in chartData ?? [])
                        data[reportDisplayConfig["pieConfig"]["labelField"]]: data[reportDisplayConfig["pieConfig"]["dataField"]].toDouble()
                    },
                    animationDuration: const Duration(milliseconds: 1000),
                    chartRadius: 150,
                    chartType: pie.ChartType.ring,
                    ringStrokeWidth: 40,
                    legendOptions: const pie.LegendOptions(
                      showLegendsInRow: false,
                      legendPosition: pie.LegendPosition.right,
                      showLegends: true,
                      legendTextStyle: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    colorList: reportDisplayConfig["pieConfig"]["colorField"] != null
                        ? [for (var data in chartData!) HexColor.fromHex(data[reportDisplayConfig["pieConfig"]["colorField"]] ?? "#ff0000")]
                        : colorList,
                    chartValuesOptions: const pie.ChartValuesOptions(
                      showChartValueBackground: true,
                      showChartValues: true,
                      showChartValuesOutside: true,
                      decimalPlaces: 1,
                    ),
                  ),
                ],
              ),
            )
          : const SizedBox.shrink();
    }
    return const SizedBox.shrink();
  }
}

class DataWidget extends StatefulWidget {
  final dynamic report;
  final Map<dynamic, dynamic> requestBody;
  final Map<dynamic, dynamic> parameters;
  final bool isShowRefreshIcon;
  final bool isShowTitle;
  const DataWidget(
      {Key? key,
      required this.report,
      required this.requestBody,
      required this.isShowRefreshIcon,
      required this.isShowTitle,
      required this.parameters})
      : super(key: key);

  @override
  State<DataWidget> createState() => _DataWidgetState();
}

class _DataWidgetState extends CustomState<DataWidget> {
  dynamic chartData;
  dynamic report;
  dynamic reportDisplayConfig;

  @override
  void initState() {
    super.initState();
    report = widget.report;
    reportDisplayConfig = report["reportDisplayConfig"];
    reloadData();
  }

  reloadData() async {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    Map<dynamic, dynamic> requestBody = Map.from(widget.requestBody);
    requestBody.addAll(widget.parameters);
    var parametersList = reportDisplayConfig["parametersList"];
    if (parametersList != null) {
      for (var parameter in parametersList) {
        if (requestBody[parameter["name"]] != null) {
          continue;
        }
        var defaultValue = parameter["defaultValue"];
        if (defaultValue != null) {
          requestBody[parameter["name"]] = defaultValue;
        }
        var valueType = parameter["valueType"];
        if ("currentDateTime" == valueType) {
          var value = DateTime.now();
          if (parameter["addMinutes"] != null) {
            value = value.add(Duration(minutes: parameter["addMinutes"]));
          }
          if (parameter["addHours"] != null) {
            value = value.add(Duration(hours: parameter["addHours"]));
          }
          if (parameter["addDays"] != null) {
            value = value.add(Duration(days: parameter["addDays"]));
          }
          requestBody[parameter["name"]] = value.toUtc().toIso8601String();
        }
        if ("currentPrjProjectId" == valueType) {
          if (navigationModel.prjProjectId != null) {
            requestBody[parameter["name"]] = navigationModel.prjProjectId;
          }
        }
      }
    }
    var dataResponse = await httpPost("/test-framework-api/user/report/rpt-report/${report["id"]}/data", requestBody, context);
    if (dataResponse.containsKey("body") && dataResponse["body"] is String == false) {
      setState(() {
        chartData = dataResponse["body"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if (report == null || reportDisplayConfig == null || chartData == null) {
      return const SizedBox.shrink();
    }
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              if (widget.isShowTitle)
                Text(
                  report["title"],
                  style: Style(context).styleTitleDialog,
                ),
              if (widget.isShowRefreshIcon)
                GestureDetector(
                  child: const Icon(Icons.refresh),
                  onTap: () => reloadData(),
                ),
            ],
          ),
          Row(
            children: [
              if (reportDisplayConfig["iconId"] != null)
                Container(
                  margin: const EdgeInsets.only(right: 15),
                  padding: const EdgeInsets.all(8),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: HexColor.fromHex(reportDisplayConfig["iconColor"]),
                  ),
                  child: Icon(
                    IconData(
                      int.parse(reportDisplayConfig["iconId"]),
                      fontFamily: reportDisplayConfig["iconFontFamily"],
                    ),
                    size: 25,
                    color: Colors.white,
                  ),
                ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    if (reportDisplayConfig["title"] != null)
                      Text(
                        reportDisplayConfig["title"],
                        style: const TextStyle(color: Color.fromRGBO(49, 49, 49, 0.9), fontWeight: FontWeight.bold),
                      ),
                    for (var data in chartData!)
                      Text(
                        '${data[reportDisplayConfig["dataField"]]}',
                        style: TextStyle(
                            color: const Color.fromRGBO(49, 49, 49, 1),
                            fontWeight: FontWeight.bold,
                            fontSize: context.fontSizeManager.fontSizeText24),
                      )
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class DataTableWidget extends StatefulWidget {
  final dynamic report;
  final Map<dynamic, dynamic> requestBody;
  final Map<dynamic, dynamic> parameters;
  final bool isShowRefreshIcon;
  final bool isShowTitle;
  const DataTableWidget(
      {Key? key,
      required this.report,
      required this.requestBody,
      required this.isShowRefreshIcon,
      required this.isShowTitle,
      required this.parameters})
      : super(key: key);

  @override
  State<DataTableWidget> createState() => _DataTableWidgetState();
}

class _DataTableWidgetState extends CustomState<DataTableWidget> {
  dynamic chartData;
  dynamic report;
  dynamic reportDisplayConfig;
  var rowCount = 0;
  var currentPage = 1;
  var rowPerPage = 10;

  @override
  void initState() {
    super.initState();
    report = widget.report;
    reportDisplayConfig = report["reportDisplayConfig"];
    reloadData(currentPage);
  }

  reloadData(page) async {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    Map<dynamic, dynamic> requestBody = Map.from(widget.requestBody);
    requestBody.addAll(widget.parameters);
    if (reportDisplayConfig["isPaging"] == true) {
      requestBody["queryOffset"] = (page - 1) * rowPerPage;
      requestBody["queryLimit"] = rowPerPage;
      if ((page - 1) * rowPerPage > rowCount) {
        page = (1.0 * rowCount / rowPerPage).ceil();
      }
      if (page < 1) {
        page = 1;
      }
    }
    var parametersList = reportDisplayConfig["parametersList"];
    if (parametersList != null) {
      for (var parameter in parametersList) {
        if (requestBody[parameter["name"]] != null) {
          continue;
        }
        var defaultValue = parameter["defaultValue"];
        if (defaultValue != null) {
          requestBody[parameter["name"]] = defaultValue;
        }
        var valueType = parameter["valueType"];
        if ("currentDateTime" == valueType) {
          var value = DateTime.now();
          if (parameter["addMinutes"] != null) {
            value = value.add(Duration(minutes: parameter["addMinutes"]));
          }
          if (parameter["addHours"] != null) {
            value = value.add(Duration(hours: parameter["addHours"]));
          }
          if (parameter["addDays"] != null) {
            value = value.add(Duration(days: parameter["addDays"]));
          }
          requestBody[parameter["name"]] = value.toUtc().toIso8601String();
        }
        if ("currentPrjProjectId" == valueType) {
          if (navigationModel.prjProjectId != null) {
            requestBody[parameter["name"]] = navigationModel.prjProjectId;
          }
        }
      }
    }
    var dataResponse = await httpPost("/test-framework-api/user/report/rpt-report/${report["id"]}/data-paging", requestBody, context);
    if (dataResponse.containsKey("body") && dataResponse["body"] is String == false) {
      setState(() {
        currentPage = page;
        rowCount = dataResponse["body"]["rowCount"];
        chartData = dataResponse["body"]["resultList"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if (report == null || reportDisplayConfig == null || chartData == null) {
      return const SizedBox.shrink();
    }
    if (reportDisplayConfig["rowGroupList"] != null && reportDisplayConfig["columnGroupList"] != null) {
      var columnGroupList = [];
      var rowGroupList = [];
      var rowGroupMap = {};
      var columnGroupMap = {};
      var dataMap = {};
      for (var data in chartData!) {
        var rowKey = "";
        var columnKey = "";
        for (var rowGroupConfig in reportDisplayConfig["rowGroupList"] ?? []) {
          rowKey = '$rowKey${data[rowGroupConfig["keyField"]] ?? ""}';
        }
        for (var columnGroupConfig in reportDisplayConfig["columnGroupList"] ?? []) {
          columnKey = '$columnKey${data[columnGroupConfig["keyField"]] ?? ""}';
        }
        if (rowGroupList.contains(rowKey) == false) {
          rowGroupList.add(rowKey);
          rowGroupMap[rowKey] = data;
        }
        if (columnGroupList.contains(columnKey) == false) {
          columnGroupList.add(columnKey);
          columnGroupMap[columnKey] = data;
        }
        dataMap["$rowKey#$columnKey"] = data;
      }
      return Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                if (widget.isShowTitle)
                  Text(
                    report["title"],
                    style: Style(context).styleTitleDialog,
                  ),
                if (widget.isShowRefreshIcon)
                  GestureDetector(
                    child: const Icon(Icons.refresh),
                    onTap: () => reloadData(currentPage),
                  ),
              ],
            ),
            SelectionArea(
              child: CustomDataTableWidget(
                lengthScroll: 5,
                widthColumnScroll: 350,
                columns: [
                  for (var rowGroupConfig in reportDisplayConfig["rowGroupList"] ?? [])
                    Expanded(
                      child: MultiLanguageText(
                        name: rowGroupConfig["multiLanguageName"],
                        defaultValue: rowGroupConfig["title"],
                        isLowerCase: false,
                        style: Style(context).styleTextDataColumn,
                      ),
                    ),
                  for (var columnGroup in columnGroupList)
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: Column(
                          children: [
                            Container(
                              decoration: const BoxDecoration(
                                border: Border(
                                    bottom: BorderSide(
                                  color: Color.fromRGBO(185, 185, 185, 1),
                                )),
                              ),
                              alignment: Alignment.center,
                              child: Text(
                                columnGroup,
                                style: Style(context).styleTextDataColumn,
                              ),
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                for (var columnConfig in reportDisplayConfig["columnList"] ?? [])
                                  Expanded(
                                    child: Container(
                                      decoration: BoxDecoration(
                                        border: Border(
                                            left: reportDisplayConfig["columnList"].first != columnConfig
                                                ? const BorderSide(
                                                    color: Color.fromRGBO(185, 185, 185, 1),
                                                  )
                                                : BorderSide.none),
                                      ),
                                      alignment: Alignment.center,
                                      child: MultiLanguageText(
                                        name: columnConfig["multiLanguageName"],
                                        defaultValue: columnConfig["title"],
                                        isLowerCase: false,
                                        style: TextStyle(
                                          color: const Color.fromRGBO(133, 135, 145, 1),
                                          fontSize: context.fontSizeManager.fontSizeText12,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                ],
                rows: [
                  for (var rowGroup in rowGroupList)
                    CustomDataRow(
                      cells: [
                        for (var rowGroupConfig in reportDisplayConfig["rowGroupList"] ?? [])
                          Expanded(
                            child: Text(
                              '${rowGroupMap[rowGroup][rowGroupConfig["dataField"]] ?? ""}',
                              maxLines: 3,
                              overflow: TextOverflow.ellipsis,
                              style: Style(context).styleTextDataCell,
                            ),
                          ),
                        for (var columnGroup in columnGroupList)
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 5),
                              child: Row(
                                children: [
                                  for (var columnConfig in reportDisplayConfig["columnList"] ?? [])
                                    Expanded(
                                      child: Align(
                                        alignment: ["Long", "Double"].contains(columnConfig["dataType"] ?? "String")
                                            ? Alignment.center
                                            : Alignment.centerLeft,
                                        child: Text(
                                          '${dataMap["$rowGroup#$columnGroup"]?[columnConfig["dataField"]] ?? ""}',
                                          maxLines: 3,
                                          overflow: TextOverflow.ellipsis,
                                          style: Style(context).styleTextDataCell,
                                        ),
                                      ),
                                    ),
                                ],
                              ),
                            ),
                          ),
                      ],
                    ),
                ],
              ),
            ),
            if (reportDisplayConfig["isPaging"] == true)
              DynamicTablePagging(
                rowCount,
                currentPage,
                rowPerPage,
                pageChangeHandler: (page) {
                  reloadData(page);
                },
                rowPerPageChangeHandler: (rowPerPage) {
                  setState(() {
                    this.rowPerPage = rowPerPage!;
                    reloadData(currentPage);
                  });
                },
              )
          ],
        ),
      );
    } else {
      return Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                if (widget.isShowTitle)
                  Text(
                    report["title"],
                    style: Style(context).styleTitleDialog,
                  ),
                if (widget.isShowRefreshIcon)
                  GestureDetector(
                    child: const Icon(Icons.refresh),
                    onTap: () => reloadData(currentPage),
                  ),
              ],
            ),
            SelectionArea(
              child: CustomDataTableWidget(
                columns: [
                  for (var columnConfig in reportDisplayConfig["columnList"] ?? [])
                    Expanded(
                        child: Column(
                      children: [
                        MultiLanguageText(
                          name: columnConfig["multiLanguageName"],
                          defaultValue: columnConfig["title"],
                          isLowerCase: false,
                          style: Style(context).styleTextDataColumn,
                        )
                      ],
                    )),
                ],
                rows: [
                  for (var data in chartData!)
                    CustomDataRow(
                      cells: [
                        for (var columnConfig in reportDisplayConfig["columnList"] ?? [])
                          Expanded(
                            child: Align(
                              alignment: ["Long", "Double"].contains(columnConfig["dataType"] ?? "String") ? Alignment.center : Alignment.centerLeft,
                              child: Text(
                                '${data[columnConfig["dataField"]] ?? ""}',
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                                style: Style(context).styleTextDataCell,
                              ),
                            ),
                          ),
                      ],
                    ),
                ],
              ),
            ),
            if (reportDisplayConfig["isPaging"] == true)
              DynamicTablePagging(
                rowCount,
                currentPage,
                rowPerPage,
                pageChangeHandler: (page) {
                  reloadData(page);
                },
                rowPerPageChangeHandler: (rowPerPage) {
                  setState(() {
                    this.rowPerPage = rowPerPage!;
                    reloadData(currentPage);
                  });
                },
              )
          ],
        ),
      );
    }
  }
}

class DataSheetWidget extends StatefulWidget {
  final dynamic report;
  final Map<dynamic, dynamic> requestBody;
  final Map<dynamic, dynamic> parameters;
  final bool isShowRefreshIcon;
  final bool isShowTitle;
  const DataSheetWidget(
      {Key? key,
      required this.report,
      required this.requestBody,
      required this.isShowRefreshIcon,
      required this.isShowTitle,
      required this.parameters})
      : super(key: key);

  @override
  State<DataSheetWidget> createState() => _DataSheetWidgetState();
}

class _DataSheetWidgetState extends CustomState<DataSheetWidget> {
  dynamic report;
  dynamic reportDisplayConfig;
  dynamic currentSheet;

  @override
  void initState() {
    super.initState();
    report = widget.report;
    reportDisplayConfig = report["reportDisplayConfig"];
    for (var sheet in reportDisplayConfig["sheetList"] ?? []) {
      currentSheet = sheet["name"];
      break;
    }
  }

  @override
  Widget build(BuildContext context) {
    if (report == null || reportDisplayConfig == null) {
      return const SizedBox.shrink();
    }
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    Map<dynamic, dynamic> requestBody = Map.from(widget.requestBody);
    requestBody.addAll(widget.parameters);
    var parametersList = reportDisplayConfig["parametersList"];
    if (parametersList != null) {
      for (var parameter in parametersList) {
        if (requestBody[parameter["name"]] != null) {
          continue;
        }
        var defaultValue = parameter["defaultValue"];
        if (defaultValue != null) {
          requestBody[parameter["name"]] = defaultValue;
        }
        var valueType = parameter["valueType"];
        if ("currentDateTime" == valueType) {
          var value = DateTime.now();
          if (parameter["addMinutes"] != null) {
            value = value.add(Duration(minutes: parameter["addMinutes"]));
          }
          if (parameter["addHours"] != null) {
            value = value.add(Duration(hours: parameter["addHours"]));
          }
          if (parameter["addDays"] != null) {
            value = value.add(Duration(days: parameter["addDays"]));
          }
          requestBody[parameter["name"]] = value.toUtc().toIso8601String();
        }
        if ("currentPrjProjectId" == valueType) {
          if (navigationModel.prjProjectId != null) {
            requestBody[parameter["name"]] = navigationModel.prjProjectId;
          }
        }
      }
    }
    return Column(
      children: [
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              for (var sheet in reportDisplayConfig["sheetList"] ?? [])
                Container(
                  margin: const EdgeInsets.only(right: 30),
                  decoration: currentSheet == sheet["name"]
                      ? const BoxDecoration(border: Border(bottom: BorderSide(color: Color(0xff3d5afe), width: 2)))
                      : null,
                  child: TextButton(
                    child: Text(sheet["name"],
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: const Color(0xff3d5afe),
                          fontSize: context.fontSizeManager.fontSizeText16,
                          fontWeight: FontWeight.w700,
                        )),
                    onPressed: () async {
                      setState(() {
                        currentSheet = sheet["name"];
                      });
                    },
                  ),
                ),
            ],
          ),
        ),
        for (var sheet in reportDisplayConfig["sheetList"] ?? [])
          if (currentSheet == sheet["name"])
            DynamicReportWidget(
              key: Key(sheet["rptReportId"]),
              reportSelectedId: sheet["rptReportId"],
              requestBody: requestBody,
              parameters: widget.parameters,
              isShowRefreshIcon: widget.isShowRefreshIcon,
              isShowTitle: widget.isShowTitle,
              isShowDownload: false,
            ),
      ],
    );
  }
}
