import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/components/dashboard/dialog_config_type.dart';
import 'package:test_framework_view/components/dashboard/dialog_report_parameter.dart';
import 'package:test_framework_view/components/dashboard/dynamic_report.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/model/model.dart';

class DynamicDashboardWidget extends StatefulWidget {
  final String dashboardSelectedId;
  final String? dashboardType;
  final Map<dynamic, dynamic> requestBody;
  const DynamicDashboardWidget({Key? key, required this.dashboardSelectedId, this.dashboardType, required this.requestBody}) : super(key: key);

  @override
  State<DynamicDashboardWidget> createState() => _DynamicDashboardWidgetState();
}

class _DynamicDashboardWidgetState extends State<DynamicDashboardWidget> {
  dynamic dashboard;
  dynamic dashboardConfig;
  bool adminPermission = false;
  bool editMode = false;
  Map<dynamic, dynamic> requestBody = {};
  @override
  void initState() {
    super.initState();
    getInitPage();
  }

  getInitPage() {
    var userPermList = Provider.of<SecurityModel>(context, listen: false).userPermList;
    if (userPermList != null) {
      setState(() {
        adminPermission = userPermList.where((permCode) => permCode == "ADMIN").isNotEmpty;
      });
    }
    setState(() {
      requestBody = Map.from(widget.requestBody);
    });
    getDashboardData();
  }

  getDashboardData() async {
    var dashboardResponse = await httpGet("/test-framework-api/user/report/rpt-dashboard/${widget.dashboardSelectedId}", context);
    setState(() {
      dashboard = dashboardResponse["body"]["result"];
      dashboardConfig = dashboard["dashboardConfig"];
    });
  }

  handleEditDashboardData() async {
    var result = dashboard;
    result["dashboardConfig"] = dashboardConfig;
    var response = await httpPost("/test-framework-api/admin/report/rpt-dashboard/${result["id"]}", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Provider.of<NavigationModel>(context, listen: false).setIsRequiredGetListProject(true);
        getDashboardData();
      }
    }
  }

  Widget buildConfig(dynamic parentList, dynamic config, {String? parentType}) {
    Widget elementWidget = const SizedBox.shrink();
    if (config["type"] == "Row") {
      config["childrenList"] ??= [];
      elementWidget = Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          for (var child in config["childrenList"])
            Expanded(
              flex: child["expand"] ?? 1,
              child: Padding(
                padding: EdgeInsets.only(right: config["childrenList"].last != child ? 10 : 0),
                child: buildConfig(config["childrenList"], child, parentType: config["type"]),
              ),
            )
        ],
      );
    }
    if (config["type"] == "Column") {
      config["childrenList"] ??= [];
      elementWidget = Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          for (var child in config["childrenList"])
            Padding(
              padding: EdgeInsets.only(bottom: config["childrenList"].last != child ? 10 : 0),
              child: buildConfig(config["childrenList"], child),
            ),
        ],
      );
    }
    if (config["type"] == "Report") {
      elementWidget = DynamicReportWidget(
        key: Key(config["rptReportId"]),
        requestBody: requestBody,
        parameters: config["parameters"] ?? {},
        reportSelectedId: config["rptReportId"],
        isShowRefreshIcon: config["isShowRefreshIcon"] ?? false,
        isShowTitle: config["isShowTitle"] ?? false,
        // isShowDownload: config["isShowDownload"] ?? false,
      );
    }
    if (editMode) {
      return Container(
        padding: const EdgeInsets.all(5),
        decoration: BoxDecoration(
          border: Border.all(
            color: const Color.fromRGBO(216, 218, 229, 1),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Wrap(
              runSpacing: 10,
              spacing: 10,
              children: [
                Text(config["type"] ?? "Not config"),
                if (config["type"] == "Row" || config["type"] == "Column")
                  ElevatedButton(
                    onPressed: () {
                      setState(() {
                        config["childrenList"].add({});
                      });
                    },
                    child: const MultiLanguageText(name: "add", defaultValue: "Add"),
                  ),
                ElevatedButton(
                  onPressed: () => showDialog<String>(
                      context: context,
                      builder: (BuildContext context) {
                        return DialogConfigTypeWidget(
                            config: config,
                            parentType: parentType,
                            onSave: (mapConfig) {
                              setState(() {
                                config["type"] = mapConfig["type"];
                                config["expand"] = mapConfig["expand"];
                                config["rptReportId"] = mapConfig["rptReportId"];
                                config["isShowRefreshIcon"] = mapConfig["isShowRefreshIcon"];
                                config["isShowTitle"] = mapConfig["isShowTitle"];
                                // config["isShowDownload"] = mapConfig["isShowDownload"];
                              });
                            });
                      }),
                  child: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                ),
                if (parentList != null)
                  ElevatedButton(
                    onPressed: () {
                      setState(() {
                        parentList.remove(config);
                      });
                    },
                    child: const MultiLanguageText(name: "delete", defaultValue: "Delete"),
                  ),
                if (config["type"] == "Report")
                  ElevatedButton(
                    onPressed: () {
                      showDialog<String>(
                          context: context,
                          builder: (BuildContext context) {
                            return DialogReportParameterWidget(
                                parameters: config["parameters"] ?? {},
                                rptReportId: config["rptReportId"],
                                onSave: (newParametersList) {
                                  setState(() {
                                    config["parameters"] = newParametersList;
                                  });
                                });
                          });
                    },
                    child: const MultiLanguageText(name: "edit_parameter", defaultValue: "Edit parameter"),
                  )
              ],
            ),
            elementWidget
          ],
        ),
      );
    } else {
      return elementWidget;
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget dashboardWidget = const SizedBox.shrink();
    if (dashboard != null && dashboardConfig != null) {
      dashboardWidget = buildConfig(null, dashboardConfig);
    }
    if (adminPermission) {
      return Container(
        padding: const EdgeInsets.all(5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Wrap(
              runSpacing: 10,
              spacing: 10,
              children: [
                if (widget.dashboardType == null)
                  ElevatedButton(
                    onPressed: () {
                      setState(() {
                        editMode = !editMode;
                      });
                      if (!editMode) handleEditDashboardData();
                    },
                    child: editMode
                        ? const MultiLanguageText(name: "save", defaultValue: "Save")
                        : const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                  ),
                if (editMode)
                  ElevatedButton(
                    onPressed: () {
                      setState(() {
                        editMode = !editMode;
                      });
                    },
                    child: const MultiLanguageText(name: "cancel", defaultValue: "Cancel"),
                  ),
              ],
            ),
            dashboardWidget
          ],
        ),
      );
    } else {
      return dashboardWidget;
    }
  }
}
