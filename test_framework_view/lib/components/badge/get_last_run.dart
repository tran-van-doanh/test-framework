import 'package:flutter/material.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/custom_state.dart';

class GetLastTestRun extends StatefulWidget {
  final String id;
  final String idProject;
  const GetLastTestRun({Key? key, required this.id, required this.idProject}) : super(key: key);

  @override
  State<GetLastTestRun> createState() => _GetLastTestRunState();
}

class _GetLastTestRunState extends CustomState<GetLastTestRun> {
  var listLastRun = [];
  getTestCaseRun() async {
    var requestBody = {"prjProjectId": widget.idProject, "tfTestCaseId": widget.id, "queryLimit": 5, "tfTestCaseIdNotNull": true};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-run/search", requestBody, context);
    if (response.containsKey("body") && response["body"] is String == false && response["body"].containsKey("rowCount")) {
      setState(() {
        listLastRun = response['body']['resultList'];
      });
    }
  }

  @override
  void initState() {
    getTestCaseRun();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        for (var i = 4; i >= 0; i--)
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 2),
            height: 30,
            width: 6,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey, width: 0.5),
              color: i < listLastRun.length
                  ? (listLastRun[i]["status"] == 1
                      ? Colors.green
                      : listLastRun[i]["status"] == -1
                          ? Colors.red
                          : listLastRun[i]["status"] == 0
                              ? Colors.orange
                              : listLastRun[i]["status"] == -13
                                  ? const Color.fromRGBO(49, 49, 49, 1)
                                  : listLastRun[i]["status"] < -1
                                      ? Colors.grey
                                      : Colors.blueAccent)
                  : Colors.transparent,
            ),
          )
      ],
    );
  }
}

class GetLastTestBatchRun extends StatefulWidget {
  final String id;
  final String idProject;
  const GetLastTestBatchRun({Key? key, required this.id, required this.idProject}) : super(key: key);

  @override
  State<GetLastTestBatchRun> createState() => _GetLastTestBatchRunState();
}

class _GetLastTestBatchRunState extends CustomState<GetLastTestBatchRun> {
  var listLastRun = [];

  getTestBatchRun() async {
    var requestBody = {"prjProjectId": widget.idProject, "tfTestBatchId": widget.id, "queryLimit": 5};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-batch-run/search", requestBody, context);
    if (response.containsKey("body") && response["body"] is String == false && response["body"].containsKey("rowCount")) {
      setState(() {
        listLastRun = response['body']['resultList'];
      });
    }
    return listLastRun;
  }

  @override
  void initState() {
    getTestBatchRun();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        for (var i = 4; i >= 0; i--)
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 2),
            height: 30,
            width: 6,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey, width: 0.5),
              color: i < listLastRun.length
                  ? (listLastRun[i]["status"] == 0
                      ? Colors.blueAccent
                      : listLastRun[i]["status"] == 1
                          ? Colors.orange
                          : listLastRun[i]["status"] == 2
                              ? Colors.green
                              : listLastRun[i]["status"] == -3
                                  ? Colors.yellowAccent
                                  : Colors.red)
                  : Colors.transparent,
            ),
          )
      ],
    );
  }
}
