import 'package:flutter/material.dart';
import 'package:test_framework_view/components/multi_language_text.dart';

class Badge6StatusWidget extends StatelessWidget {
  final int status;
  const Badge6StatusWidget({Key? key, required this.status}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Tooltip(
      message: status == 1
          ? multiLanguageString(name: "pass", defaultValue: "Pass", context: context)
          : status == -1
              ? multiLanguageString(name: "fail", defaultValue: "Fail", context: context)
              : status == 0
                  ? multiLanguageString(name: "waiting", defaultValue: "Waiting", context: context)
                  : status == -13
                      ? multiLanguageString(name: "canceled", defaultValue: "Canceled", context: context)
                      : status < -1
                          ? multiLanguageString(name: "error", defaultValue: "Error", context: context)
                          : multiLanguageString(name: "processing", defaultValue: "Processing", context: context),
      child: Container(
        decoration: BoxDecoration(
          color: status == 1
              ? Colors.green
              : status == -1
                  ? Colors.red
                  : status == 0
                      ? Colors.orange
                      : status == -13
                          ? const Color.fromRGBO(49, 49, 49, 1)
                          : status < -1
                              ? Colors.grey
                              : Colors.blueAccent,
          borderRadius: BorderRadius.circular(10),
        ),
        padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
        child: status == 1
            ? const Icon(
                Icons.done,
                color: Colors.white,
              )
            : status == -1
                ? const Icon(Icons.close, color: Colors.white)
                : status == 0
                    ? const Icon(Icons.pending, color: Colors.white)
                    : status == -13
                        ? const Icon(Icons.stop_circle, color: Colors.white)
                        : status < -1
                            ? const Icon(Icons.error_outline, color: Colors.white)
                            : const Icon(Icons.drag_handle, color: Colors.white),
      ),
    );
  }
}

class Badge4StatusTestRunDataWidget extends StatelessWidget {
  final int status;
  const Badge4StatusTestRunDataWidget({Key? key, required this.status}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Tooltip(
      message: status == 1
          ? multiLanguageString(name: "pass", defaultValue: "Pass", context: context)
          : status == 0
              ? multiLanguageString(name: "fail", defaultValue: "Fail", context: context)
              : status == -13
                  ? multiLanguageString(name: "canceled", defaultValue: "Canceled", context: context)
                  : multiLanguageString(name: "processing", defaultValue: "Processing", context: context),
      child: Container(
        decoration: BoxDecoration(
          color: status == 1
              ? Colors.green
              : status == 0
                  ? Colors.red
                  : status == -13
                      ? const Color.fromRGBO(49, 49, 49, 1)
                      : Colors.blueAccent,
          borderRadius: BorderRadius.circular(10),
        ),
        padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
        child: status == 1
            ? const Icon(Icons.done, color: Colors.white, size: 20)
            : status == 0
                ? const Icon(Icons.close, color: Colors.white)
                : status == -13
                    ? const Icon(Icons.stop_circle, color: Colors.white)
                    : const Icon(Icons.drag_handle, color: Colors.white),
      ),
    );
  }
}

class Badge2StatusSettingWidget extends StatelessWidget {
  final int status;
  const Badge2StatusSettingWidget({Key? key, required this.status}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Tooltip(
      message: status == 1
          ? multiLanguageString(name: "active", defaultValue: "Active", context: context)
          : multiLanguageString(name: "inactive", defaultValue: "Inactive", context: context),
      child: Container(
        decoration: BoxDecoration(
          color: status == 1 ? Colors.green : Colors.red,
          borderRadius: BorderRadius.circular(10),
        ),
        padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
        child: status == 1 ? const Icon(Icons.done, color: Colors.white, size: 20) : const Icon(Icons.close, color: Colors.white, size: 20),
      ),
    );
  }
}

class Badge3StatusWidget extends StatelessWidget {
  final int status;
  const Badge3StatusWidget({Key? key, required this.status}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Tooltip(
      message: status == 0
          ? multiLanguageString(name: "draft", defaultValue: "Draft", context: context)
          : status == 2
              ? multiLanguageString(name: "review", defaultValue: "Review", context: context)
              : multiLanguageString(name: "active", defaultValue: "Active", context: context),
      child: Container(
          decoration: BoxDecoration(
            color: status == 0
                ? const Color.fromARGB(244, 142, 142, 142)
                : status == 2
                    ? const Color.fromARGB(244, 212, 15, 95)
                    : const Color.fromARGB(244, 13, 214, 108),
            borderRadius: BorderRadius.circular(10),
          ),
          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
          child: status == 0
              ? const Icon(Icons.edit_note, color: Colors.white, size: 20)
              : status == 2
                  ? const Icon(Icons.reviews, color: Colors.white, size: 20)
                  : const Icon(Icons.play_arrow_outlined, color: Colors.white, size: 20)),
    );
  }
}

class BadgeStatusSprintWidget extends StatelessWidget {
  final int status;
  const BadgeStatusSprintWidget({Key? key, required this.status}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Tooltip(
      message: status == 1
          ? multiLanguageString(name: "active", defaultValue: "Active", context: context)
          : status == 0
              ? multiLanguageString(name: "new", defaultValue: "New", context: context)
              : multiLanguageString(name: "complete", defaultValue: "Complete", context: context),
      child: Container(
        decoration: BoxDecoration(
          color: status == 1
              ? Colors.green
              : status == 0
                  ? Colors.orangeAccent
                  : Colors.blueAccent,
          borderRadius: BorderRadius.circular(10),
        ),
        padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
        child: status == 1
            ? const Icon(Icons.done, color: Colors.white)
            : status == 0
                ? const Icon(Icons.add, color: Colors.white)
                : const Icon(Icons.checklist, color: Colors.white),
      ),
    );
  }
}

class Badge5StatusDataFileWidget extends StatelessWidget {
  final int status;
  const Badge5StatusDataFileWidget({Key? key, required this.status}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Tooltip(
      message: status == 0
          ? multiLanguageString(name: "new", defaultValue: "New", context: context)
          : status == 1
              ? multiLanguageString(name: "reading", defaultValue: "Reading", context: context)
              : status == 2
                  ? multiLanguageString(name: "running", defaultValue: "Running", context: context)
                  : status == 3
                      ? multiLanguageString(name: "result", defaultValue: "Result", context: context)
                      : status == 4
                          ? multiLanguageString(name: "finished", defaultValue: "Finished", context: context)
                          : multiLanguageString(name: "error", defaultValue: "Error", context: context),
      child: Container(
        decoration: BoxDecoration(
          color: status == 0
              ? Colors.grey
              : status == 1
                  ? Colors.blueAccent
                  : status == 2
                      ? Colors.orange
                      : status == 3
                          ? const Color.fromRGBO(49, 49, 49, 1)
                          : status == 4
                              ? Colors.green
                              : Colors.red,
          borderRadius: BorderRadius.circular(10),
        ),
        padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
        child: status == 0
            ? const Icon(
                Icons.drag_handle,
                color: Colors.white,
              )
            : status == 1
                ? const Icon(Icons.drag_handle, color: Colors.white)
                : status == 2
                    ? const Icon(Icons.drag_handle, color: Colors.white)
                    : status == 3
                        ? const Icon(Icons.drag_handle, color: Colors.white)
                        : status == 4
                            ? const Icon(Icons.done, color: Colors.white)
                            : const Icon(Icons.error_outline, color: Colors.white),
      ),
    );
  }
}

class BadgeTestBatchRunStatusWidget extends StatelessWidget {
  final int status;
  const BadgeTestBatchRunStatusWidget({Key? key, required this.status}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Tooltip(
      message: status == 0
          ? multiLanguageString(name: "new", defaultValue: "New", context: context)
          : status == 1
              ? multiLanguageString(name: "running", defaultValue: "Running", context: context)
              : status == 2
                  ? multiLanguageString(name: "run_pass", defaultValue: "Run pass", context: context)
                  : multiLanguageString(name: "run_fail", defaultValue: "Run fail", context: context),
      child: Container(
        decoration: BoxDecoration(
          color: status == 0
              ? Colors.blueAccent
              : status == 1
                  ? Colors.orange
                  : status == 2
                      ? Colors.green
                      : Colors.red,
          borderRadius: BorderRadius.circular(10),
        ),
        padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
        child: status == 0
            ? const Icon(Icons.add, color: Colors.white)
            : status == 1
                ? const Icon(Icons.pending, color: Colors.white)
                : status == 2
                    ? const Icon(
                        Icons.done,
                        color: Colors.white,
                      )
                    : const Icon(Icons.close, color: Colors.white),
      ),
    );
  }
}

class BadgeTestBatchRunStepStatusWidget extends StatelessWidget {
  final int status;
  const BadgeTestBatchRunStepStatusWidget({Key? key, required this.status}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Tooltip(
      message: status == 0
          ? multiLanguageString(name: "new", defaultValue: "New", context: context)
          : status == 2
              ? multiLanguageString(name: "waiting_for_run", defaultValue: "Waiting for run", context: context)
              : status == 1
                  ? multiLanguageString(name: "running", defaultValue: "Running", context: context)
                  : status == 3
                      ? multiLanguageString(name: "run_pass", defaultValue: "Run pass", context: context)
                      : multiLanguageString(name: "run_fail", defaultValue: "Run fail", context: context),
      child: Container(
        decoration: BoxDecoration(
          color: status == 0
              ? Colors.blueAccent
              : status == 2
                  ? Colors.grey
                  : status == 1
                      ? Colors.orange
                      : status == 3
                          ? Colors.green
                          : Colors.red,
          borderRadius: BorderRadius.circular(10),
        ),
        padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
        child: status == 0
            ? const Icon(Icons.add, color: Colors.white)
            : status == 2
                ? const Icon(Icons.drag_handle, color: Colors.white)
                : status == 1
                    ? const Icon(Icons.pending, color: Colors.white)
                    : status == 3
                        ? const Icon(
                            Icons.done,
                            color: Colors.white,
                          )
                        : const Icon(Icons.close, color: Colors.white),
      ),
    );
  }
}
