import 'package:flutter/material.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';

class BadgeText6StatusWidget extends StatelessWidget {
  final int status;
  const BadgeText6StatusWidget({Key? key, required this.status}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: status == 1
            ? Colors.green
            : status == -1
                ? Colors.red
                : status == 0
                    ? Colors.orange
                    : status == -13
                        ? const Color.fromRGBO(49, 49, 49, 1)
                        : status < -1
                            ? Colors.grey
                            : Colors.blueAccent,
        borderRadius: BorderRadius.circular(10),
      ),
      padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
      child: MultiLanguageText(
        name: status == 1
            ? "pass"
            : status == -1
                ? "fail"
                : status == 0
                    ? "waiting"
                    : status == -13
                        ? "canceled"
                        : status < -1
                            ? "error"
                            : "processing",
        defaultValue: status == 1
            ? "Pass"
            : status == -1
                ? "Fail"
                : status == 0
                    ? "Waiting"
                    : status == -13
                        ? "Canceled"
                        : status < -1
                            ? "Error"
                            : "Processing",
        style: Style(context).textDragAndDropWidget,
      ),
    );
  }
}

class BadgeText4StatusTestRunDataWidget extends StatelessWidget {
  final int status;
  const BadgeText4StatusTestRunDataWidget({Key? key, required this.status}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: status == 1
            ? Colors.green
            : status == 0
                ? Colors.red
                : status == -13
                    ? const Color.fromRGBO(49, 49, 49, 1)
                    : Colors.blueAccent,
        borderRadius: BorderRadius.circular(10),
      ),
      padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
      child: MultiLanguageText(
        name: status == 1
            ? "pass"
            : status == 0
                ? "fail"
                : status == -13
                    ? "canceled"
                    : "processing",
        defaultValue: status == 1
            ? "Pass"
            : status == 0
                ? "Fail"
                : status == -13
                    ? "Canceled"
                    : "Processing",
        style: Style(context).textDragAndDropWidget,
      ),
    );
  }
}

class BadgeTextBugTrackerStatusWidget extends StatelessWidget {
  final int status;
  const BadgeTextBugTrackerStatusWidget({Key? key, required this.status}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: status == 1
            ? Colors.orange
            : status == 0
                ? Colors.red
                : status == 2
                    ? Colors.green
                    : Colors.grey,
        borderRadius: BorderRadius.circular(10),
      ),
      padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
      child: MultiLanguageText(
        name: status == 1
            ? "reporting"
            : status == 0
                ? "not_reported"
                : status == 2
                    ? "reported"
                    : "not_bug",
        defaultValue: status == 1
            ? "Reporting"
            : status == 0
                ? "Not reported"
                : status == 2
                    ? "Reported"
                    : "Not bug",
        style: Style(context).textDragAndDropWidget,
      ),
    );
  }
}

class BadgeTextBugTrackerItemStatusWidget extends StatelessWidget {
  final int status;
  const BadgeTextBugTrackerItemStatusWidget({Key? key, required this.status}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: status == 1
            ? Colors.green
            : status == 0
                ? Colors.orange
                : status == 2
                    ? Colors.red
                    : Colors.grey,
        borderRadius: BorderRadius.circular(10),
      ),
      padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
      child: MultiLanguageText(
        name: status == 1
            ? "sent"
            : status == 0
                ? "new"
                : status == 2
                    ? "missing_final_image"
                    : "error",
        defaultValue: status == 1
            ? "Sent"
            : status == 0
                ? "New"
                : status == 2
                    ? "Missing final image"
                    : "Error",
        style: Style(context).textDragAndDropWidget,
      ),
    );
  }
}

class BadgeTextTestBatchRunStatusWidget extends StatelessWidget {
  final int status;
  const BadgeTextTestBatchRunStatusWidget({Key? key, required this.status}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: status == 0
            ? Colors.blueAccent
            : status == 1
                ? Colors.orange
                : status == 2
                    ? Colors.green
                    : status == -3
                        ? Colors.yellowAccent
                        : Colors.red,
        borderRadius: BorderRadius.circular(10),
      ),
      padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
      child: MultiLanguageText(
        name: status == 0
            ? "new"
            : status == 1
                ? "running"
                : status == 2
                    ? "run_pass"
                    : status == 2
                        ? "warning"
                        : "run_fail",
        defaultValue: status == 0
            ? "New"
            : status == 1
                ? "Running"
                : status == 2
                    ? "Run pass"
                    : status == 2
                        ? "Warning"
                        : "Run fail",
        style: Style(context).textDragAndDropWidget,
      ),
    );
  }
}
