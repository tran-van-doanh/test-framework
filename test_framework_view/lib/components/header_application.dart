import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/page/editor/editor/editor_page.dart';
import 'package:test_framework_view/page/list_test/tests/test/dialog_option_test.dart';
import 'package:test_framework_view/type.dart';
import 'package:url_launcher/url_launcher.dart';
import '../common/custom_state.dart';
import '../model/model.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

class HeaderApplication extends StatefulWidget {
  final VoidCallback onClickGuide;
  const HeaderApplication({Key? key, required this.onClickGuide}) : super(key: key);

  @override
  State<HeaderApplication> createState() => _HeaderApplicationState();
}

class _HeaderApplicationState extends CustomState<HeaderApplication> {
  @override
  Widget build(BuildContext context) {
    return Consumer2<LanguageModel, NavigationModel>(builder: (context, languageModel, navigationModel, child) {
      return AppBar(
        leading: Container(
          color: const Color.fromRGBO(26, 32, 42, 1),
          child: SvgPicture.asset(
            'assets/images/logo.svg',
            colorFilter: const ColorFilter.mode(Colors.grey, BlendMode.srcIn),
          ),
        ),
        title: (navigationModel.product != "resource-pool" &&
                navigationModel.product != "workflow" &&
                navigationModel.product != "admin" &&
                navigationModel.product != "prj-360")
            ? const SizedBox(width: 300, child: ListProjectWidget())
            : null,
        leadingWidth: 75,
        toolbarHeight: 60,
        titleTextStyle:
            TextStyle(color: const Color.fromRGBO(49, 49, 49, 1), fontSize: context.fontSizeManager.fontSizeText16, fontWeight: FontWeight.w500),
        // backgroundColor: Colors.white,
        elevation: 2,
        actions: [
          const Padding(
            padding: EdgeInsets.fromLTRB(0, 10, 10, 10),
            child: ListProductWidget(),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: FloatingActionButton(
              tooltip: multiLanguageString(name: "documentation", defaultValue: "Documentation", context: context),
              heroTag: null,
              backgroundColor: const Color.fromRGBO(216, 218, 229, .7),
              elevation: 0,
              hoverElevation: 0,
              onPressed: () {
                launchUrl(
                  Uri.parse("/#/qa-platform/docs/login_platform"),
                  webOnlyWindowName: kIsWeb ? '_blank' : '_self',
                );
              },
              child: const IconTheme(
                data: IconThemeData(color: Color.fromRGBO(49, 49, 49, 1)),
                child: Icon(
                  Icons.text_snippet,
                  size: 18,
                ),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: FloatingActionButton(
              tooltip: multiLanguageString(name: "guide", defaultValue: "Guide", context: context),
              heroTag: null,
              backgroundColor: const Color.fromRGBO(216, 218, 229, .7),
              elevation: 0,
              hoverElevation: 0,
              onPressed: () => widget.onClickGuide(),
              child: const IconTheme(
                data: IconThemeData(color: Color.fromRGBO(49, 49, 49, 1)),
                child: Icon(
                  Icons.rocket_launch,
                  size: 18,
                ),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: FloatingActionButton(
              tooltip: multiLanguageString(name: "usage_examples", defaultValue: "Usage Examples", context: context),
              heroTag: null,
              backgroundColor: const Color.fromRGBO(216, 218, 229, .7),
              elevation: 0,
              hoverElevation: 0,
              onPressed: () {},
              child: const IconTheme(
                data: IconThemeData(color: Color.fromRGBO(49, 49, 49, 1)),
                child: Icon(
                  Icons.contact_page,
                  size: 18,
                ),
              ),
            ),
          ),
          const Padding(
            padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            child: NotificationWidget(),
          ),
          const Padding(
            padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            child: LanguagePicker(),
          ),
          const Padding(
            padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
            child: VerticalDivider(
              thickness: 2,
            ),
          ),
          const UserMenuButtonWidget(),
        ],
      );
    });
  }
}

class ListProjectWidget extends StatefulWidget {
  const ListProjectWidget({Key? key}) : super(key: key);

  @override
  State<ListProjectWidget> createState() => _ListProjectWidgetState();
}

class _ListProjectWidgetState extends CustomState<ListProjectWidget> {
  var resultListProject = [];
  @override
  void initState() {
    getListProject();
    super.initState();
  }

  getListProject() async {
    resultListProject = await Provider.of<NavigationModel>(context, listen: false)
        .getPrjProjectList(Provider.of<SecurityModel>(context, listen: false).authorization);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: true);
    String selectedprjProjectId = navigationModel.prjProjectId ?? "";
    String selectedNameProject = "";
    for (var project in resultListProject) {
      if (selectedprjProjectId == project["id"]) {
        selectedNameProject = project["title"];
      }
    }
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return DropdownSearch<String>(
        dropdownDecoratorProps: DropDownDecoratorProps(
          dropdownSearchDecoration: InputDecoration(
            border: const OutlineInputBorder(),
            disabledBorder: InputBorder.none,
            contentPadding: const EdgeInsets.symmetric(horizontal: 15),
            suffixIcon: const Icon(Icons.arrow_drop_down),
            hintStyle: TextStyle(
              // fontFamily: 'Ubuntu',
              color: Colors.black,
              fontSize: context.fontSizeManager.fontSizeText12,
            ),
            labelText: multiLanguageString(name: "list_project", defaultValue: "List project", context: context),
            hintText: multiLanguageString(name: "enter_a_name", defaultValue: "Enter a name", context: context),
            focusedBorder: const OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.blue,
              ),
            ),
          ),
        ),
        items: [
          {"title": "All"},
          ...resultListProject
        ].map<String>((dynamic e) => e['title']).toList(),
        popupProps: PopupProps.menu(
          showSelectedItems: true,
          emptyBuilder: (context, searchEntry) => const Padding(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Align(alignment: Alignment.center, child: MultiLanguageText(name: "no_data", defaultValue: "No data")),
          ),
          showSearchBox: true,
          constraints: const BoxConstraints(maxHeight: 300),
          searchFieldProps: TextFieldProps(
            decoration: InputDecoration(
              border: const OutlineInputBorder(),
              hintText: multiLanguageString(name: "enter_a_name", defaultValue: "Enter a name", context: context),
              focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.blue,
                ),
              ),
            ),
          ),
        ),
        dropdownBuilder: (BuildContext context, String? value) {
          return Row(
            children: [
              Container(
                  decoration: BoxDecoration(color: const Color.fromRGBO(224, 233, 255, 1), borderRadius: BorderRadius.circular(25)),
                  padding: const EdgeInsets.all(5),
                  margin: const EdgeInsets.only(right: 10),
                  child: const Icon(
                    Icons.computer,
                    color: Color.fromRGBO(49, 49, 49, 1),
                  )),
              Expanded(child: Text(selectedNameProject)),
            ],
          );
        },
        onChanged: (value) async {
          setState(() {
            Provider.of<ElementRepositoryModel>(context, listen: false).clearProvider();
            var project = resultListProject.firstWhere((product) => product["title"] == value, orElse: () => null);
            if (project != null) {
              selectedprjProjectId = project["id"];
              selectedNameProject = value!;
              navigationModel.setPrjProjectId(project["id"]);
              if (navigationModel.product == "qa-platform") {
                navigationModel.navigate("/qa-platform/project/$selectedprjProjectId/insights/dashboard");
              } else if (navigationModel.product == "work-mgt") {
                navigationModel.navigate("/work-mgt/project/$selectedprjProjectId/summary");
              }
            } else {
              navigationModel.navigate("/${navigationModel.product}/setting/project");
            }
          });
        },
        selectedItem: selectedNameProject,
      );
    });
  }
}

class UserMenuButtonWidget extends StatefulWidget {
  const UserMenuButtonWidget({Key? key}) : super(key: key);

  @override
  State<UserMenuButtonWidget> createState() => _UserMenuButtonWidgetState();
}

class _UserMenuButtonWidgetState extends CustomState<UserMenuButtonWidget> {
  String getFirstWord(String text) => text.isNotEmpty ? text.trim().split(' ').map((l) => l[0]).take(2).join() : '';

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, SecurityModel>(builder: (context, navigationModel, securityModel, child) {
      return Padding(
        padding: const EdgeInsets.all(10.0),
        child: PopupMenuButton(
          offset: const Offset(5, 50),
          splashRadius: 10,
          tooltip: '',
          itemBuilder: (context) => [
            PopupMenuItem(
              enabled: false,
              child: Container(
                decoration: const BoxDecoration(border: Border(bottom: BorderSide())),
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    children: [
                      Container(
                        width: 60,
                        height: 60,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(40), color: const Color.fromRGBO(121, 134, 203, 1)),
                        child: Text(
                          getFirstWord(securityModel.userLoginCurren['nickname']).toUpperCase(),
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(securityModel.userLoginCurren['fullname'],
                                style: TextStyle(fontSize: context.fontSizeManager.fontSizeText18, color: const Color.fromRGBO(49, 49, 49, 1))),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                              child: Text(securityModel.userLoginCurren['email'],
                                  style: TextStyle(fontSize: context.fontSizeManager.fontSizeText14, color: const Color.fromRGBO(158, 160, 171, 1))),
                            ),
                            ElevatedButton(
                              onPressed: () {
                                navigationModel.navigate("/profile/view");
                              },
                              child: const MultiLanguageText(
                                name: "view_profile",
                                defaultValue: "View profile",
                                isLowerCase: false,
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            const PopupMenuItem(
              enabled: false,
              padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
              child: MultiLanguageText(
                name: "account",
                defaultValue: "Account",
                isLowerCase: false,
                style: TextStyle(
                  color: Colors.blueGrey,
                ),
              ),
            ),
            PopupMenuItem(
              enabled: false,
              padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const MultiLanguageText(
                    name: "font_size",
                    defaultValue: "Font Size",
                    style: TextStyle(
                      color: Color.fromARGB(255, 0, 0, 0),
                    ),
                  ),
                  Row(
                    children: [
                      GestureDetector(
                          onTap: () {
                            navigationModel.setFontSizeText(1);
                          },
                          child: changeFontSize("+", "+")),
                      const SizedBox(
                        width: 5,
                      ),
                      GestureDetector(onTap: () => navigationModel.resetFontSizeText(), child: changeFontSize("reset", "Reset")),
                      const SizedBox(
                        width: 5,
                      ),
                      GestureDetector(onTap: () => navigationModel.setFontSizeText(-1), child: changeFontSize("-", "-")),
                    ],
                  ),
                ],
              ),
            ),
            PopupMenuItem(
              padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
              child: ListTile(
                contentPadding: const EdgeInsets.all(0),
                hoverColor: Colors.transparent,
                title: const MultiLanguageText(
                  name: "project_settings",
                  defaultValue: "Project Settings",
                  style: TextStyle(
                    color: Color.fromARGB(255, 0, 0, 0),
                  ),
                ),
                onTap: (() {
                  navigationModel.navigate("/${navigationModel.product}/setting/project");
                }),
              ),
            ),
            PopupMenuItem(
              padding: const EdgeInsets.fromLTRB(33, 0, 30, 0),
              child: ListTile(
                contentPadding: const EdgeInsets.all(0),
                hoverColor: Colors.transparent,
                title: const MultiLanguageText(
                  name: "sign_out",
                  defaultValue: "Sign out",
                  style: TextStyle(
                    color: Color.fromARGB(255, 0, 0, 0),
                  ),
                ),
                onTap: (() async {
                  if (mounted) {
                    var webElementModel = Provider.of<WebElementModel>(context, listen: false);
                    var elementRepositoryModel = Provider.of<ElementRepositoryModel>(context, listen: false);
                    var debugStatusModel = Provider.of<DebugStatusModel>(context, listen: false);
                    var testCaseConfigModel = Provider.of<TestCaseConfigModel>(context, listen: false);
                    var testCaseDirectoryModel = Provider.of<TestCaseDirectoryModel>(context, listen: false);
                    var sideBarDocumentModel = Provider.of<SideBarDocumentModel>(context, listen: false);
                    var playTestCaseModel = Provider.of<PlayTestCaseModel>(context, listen: false);
                    await securityModel.clearProvider();
                    await navigationModel.clearProvider();
                    await webElementModel.clearProvider();
                    await elementRepositoryModel.clearProvider();
                    await debugStatusModel.clearProvider();
                    await testCaseConfigModel.clearProvider();
                    await testCaseDirectoryModel.clearProvider();
                    await sideBarDocumentModel.clearProvider();
                    await playTestCaseModel.clearProvider();
                  }
                }),
              ),
            ),
          ],
          child: Container(
              width: 40,
              height: 40,
              alignment: Alignment.center,
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(20), color: const Color.fromRGBO(121, 134, 203, 1)),
              child: Text(
                securityModel.userLoginCurren != null ? getFirstWord(securityModel.userLoginCurren['nickname']).toUpperCase() : "",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: context.fontSizeManager.fontSizeText20, color: Colors.white, fontWeight: FontWeight.bold),
              )),
        ),
      );
    });
  }

  Container changeFontSize(String name, String text) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 9, vertical: 2),
      decoration: BoxDecoration(
        border: Border.all(),
        borderRadius: BorderRadius.circular(50),
      ),
      child: MultiLanguageText(
        name: name,
        defaultValue: text,
        style: const TextStyle(color: Color.fromARGB(255, 0, 0, 0)),
      ),
    );
  }
}

class ListProductWidget extends StatelessWidget {
  const ListProductWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: true);
    List products = [
      {
        "icon": "assets/images/project_360.svg",
        "type": "project-360",
        "name": "Project 360",
        "module": "PROJECT_360",
        "url": "/prj-360/dashboard",
        "aspectRatio": 1.5,
      },
      {
        "icon": "assets/images/resource_pool.svg",
        "type": "resource-pool",
        "name": "Resource Pool",
        "url": "/resource-pool/insights",
        "module": "RESOURCE_POOL",
      },
      {
        "icon": "assets/images/logo.svg",
        "type": "qa-platform",
        "name": "Centralize Automation Testing Suite",
        "url": "/qa-platform/project/${navigationModel.prjProjectId}/insights/dashboard",
        "module": "TEST_FRAMEWORK"
      },
      {
        "icon": "assets/images/work_mgt.svg",
        "type": "work-mgt",
        "name": "Work Management",
        "url": "/work-mgt/project/${navigationModel.prjProjectId}/summary",
        "module": "WORK_MANAGEMENT",
      },
      // {
      //   "icon": "assets/images/content-collaboration.svg",
      //   "type": "content-collaboration",
      //   "name": "Content Collaboration",
      //   "module": "CONTENT_COLLABORATION",
      // },
      {
        "icon": "assets/images/workflow.svg",
        "type": "workflow",
        "name": "Workflow",
        "url": "/workflow/status",
        "module": "WORKFLOW",
      },
      {
        "icon": "assets/images/simulation.svg",
        "type": "simulation",
        "name": "Simulation",
        "module": "SIMULATION",
      },
      // {
      //   "icon": "assets/images/rpa.svg",
      //   "type": "rpa",
      //   "name": "RPA",
      //   "module": "RPA",
      // },
      // {
      //   "icon": "assets/images/devices-farm.svg",
      //   "type": "devices-farm",
      //   "name": "Devices farm",
      //   "module": "DEVICES_FARM",
      // },
      {
        "icon": "assets/images/settings.svg",
        "name": "Settings",
        "url": "/setting/role",
      },
      if ((Provider.of<SecurityModel>(context, listen: false).userPermList ?? []).where((permCode) => permCode == "ADMIN").isNotEmpty)
        {"type": "admin", "name": "Administrator", "url": "/admin/dashboard", "icon": "assets/images/admin.svg"},
    ];
    return PopupMenuButton(
      offset: const Offset(5, 50),
      tooltip: multiLanguageString(name: "apps", defaultValue: "Apps", context: context),
      padding: const EdgeInsets.all(0),
      constraints: const BoxConstraints(minWidth: 400, maxWidth: 400),
      itemBuilder: (context) => [
        PopupMenuItem(
          enabled: false,
          padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "LLQ QA Platform",
                    style: Style(context).styleTitleDialog,
                  ),
                  ElevatedButton(
                    child: const MultiLanguageText(name: "view_all_apps", defaultValue: "View All Apps"),
                    onPressed: () {
                      navigationModel.navigate("/select");
                    },
                  ),
                ],
              ),
              const SizedBox(
                height: 30,
              ),
              Wrap(
                runSpacing: 20,
                spacing: 25,
                children: [
                  for (var product in products)
                    if (Provider.of<SecurityModel>(context, listen: false).hasSysOrganizationModules(product["module"]) == true)
                      productItemWidget(
                        SvgPicture.asset(
                          product["icon"],
                          colorFilter: const ColorFilter.mode(Colors.blueAccent, BlendMode.srcIn),
                        ),
                        product["name"],
                        () {
                          if (product["type"] != null) navigationModel.setProduct(product["type"]);
                          if (product["type"] == "resource-pool" ||
                              product["type"] == "workflow" ||
                              product["type"] == "admin" ||
                              product["type"] == "prj-360") {
                            navigationModel.setPrjProjectIdWithoutNotify(null);
                          }
                          if ((product["type"] != null &&
                                  product["type"] != "resource-pool" &&
                                  product["type"] != "workflow" &&
                                  product["type"] != "prj-360" &&
                                  product["type"] != "admin") &&
                              navigationModel.prjProjectId == null) {
                            navigationModel.navigate("/${product["type"]}/setting/project");
                          } else if (product["url"] != null) {
                            navigationModel.navigate(product["url"]);
                          }
                        },
                        aspectRatio: product["aspectRatio"],
                      )
                ],
              ),
              const SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
      ],
      splashRadius: 1,
      icon: Container(
        padding: const EdgeInsets.symmetric(horizontal: 11, vertical: 11),
        decoration: BoxDecoration(
          color: const Color.fromRGBO(216, 218, 229, .7),
          borderRadius: BorderRadius.circular(50),
        ),
        child: const Icon(
          Icons.apps,
          size: 18,
          color: Color.fromRGBO(49, 49, 49, 1),
        ),
      ),
    );
  }

  GestureDetector productItemWidget(SvgPicture svgPicture, String text, Function() onTapFunc, {double? aspectRatio}) {
    return GestureDetector(
      onTap: onTapFunc,
      child: Tooltip(
        message: text,
        child: Container(
          height: 70,
          width: 70,
          alignment: Alignment.center,
          padding: const EdgeInsets.all(5),
          decoration: BoxDecoration(
            border: Border.all(
              color: const Color.fromRGBO(216, 218, 229, 1),
            ),
            borderRadius: BorderRadius.circular(10),
          ),
          child: AspectRatio(aspectRatio: aspectRatio ?? 1.0, child: svgPicture),
        ),
      ),
    );
  }
}

class NotificationWidget extends StatefulWidget {
  const NotificationWidget({Key? key}) : super(key: key);

  @override
  State<NotificationWidget> createState() => _NotificationWidgetState();
}

class _NotificationWidgetState extends State<NotificationWidget> {
  onReadNoti(id) async {
    var response = await httpPost("/test-framework-api/user/noti/noti-notification/$id/mark-read", {}, context);
    if (response.containsKey("body") && response["body"] is String == false && response["body"].containsKey("errorMessage") && mounted) {
      showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
    }
  }

  handleEditTfTestCase(result, id) async {
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-case/$id", result, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      if (response["body"].containsKey("errorMessage") && mounted) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else if (mounted) {
        Navigator.of(context).pop();
        showToast(
            context: context,
            msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
      }
    }
  }

  onClickNotiTestCase(resultNotification) async {
    if (resultNotification["actionType"] == "comment") {
      Navigator.of(context).push(
        createRoute(
          EditorPageRootWidget(
            isOpenListComment: true,
            testCaseType: resultNotification["itemType"],
            testCaseId: resultNotification["itemId"],
            callback: () {},
          ),
        ),
      );
    } else {
      var responseGetTestCase = await httpGet("/test-framework-api/user/test-framework/tf-test-case/${resultNotification["itemId"]}", context);
      if (mounted) {
        Navigator.of(context).push(
          createRoute(
            DialogOptionTestWidget(
              testCaseType: resultNotification["itemType"],
              type: "edit",
              title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
              selectedItem: (responseGetTestCase.containsKey("body") && responseGetTestCase["body"] is String == false)
                  ? responseGetTestCase["body"]["result"] ?? {}
                  : {},
              callbackOptionTest: (result) {
                handleEditTfTestCase(result, result["id"]);
              },
              parentId: responseGetTestCase["body"]["result"]["tfTestDirectoryId"],
            ),
          ),
        );
      }
    }
  }

  onClickNotiTestRun(itemId, NavigationModel navigationModel) async {
    var responseGetTestRun = await httpGet("/test-framework-api/user/test-framework/tf-test-run/$itemId", context);
    if (responseGetTestRun.containsKey("body") && responseGetTestRun["body"] is String == false) {
      String? type =
          responseGetTestRun["body"]["result"]['tfTestCase'] != null ? responseGetTestRun["body"]["result"]['tfTestCase']['testCaseType'] : null;
      if (type != null) {
        if (responseGetTestRun["body"]["result"]["prjProjectId"] != navigationModel.prjProjectId) {
          navigationModel.setPrjProjectId(responseGetTestRun["body"]["result"]["prjProjectId"]);
        }
        navigationModel.navigate(
            "/qa-platform/project/${responseGetTestRun["body"]["result"]["prjProjectId"]}/runs/$type/test-run/${responseGetTestRun["body"]["result"]["id"]}");
      }
    }
  }

  onClickNotiTestBatchRun(itemId, navigationModel) async {
    var responseGetTestRun = await httpGet("/test-framework-api/user/test-framework/tf-test-batch-run/$itemId", context);
    if (responseGetTestRun.containsKey("body") && responseGetTestRun["body"] is String == false) {
      if (responseGetTestRun["body"]["result"]["prjProjectId"] != navigationModel.prjProjectId) {
        navigationModel.setPrjProjectId(responseGetTestRun["body"]["result"]["prjProjectId"]);
      }
      navigationModel.navigate(
          "/qa-platform/project/${responseGetTestRun["body"]["result"]["prjProjectId"]}/runs/test_batch/test-run/${responseGetTestRun["body"]["result"]["id"]}");
    }
  }

  @override
  Widget build(BuildContext context) {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    NotificationModel notificationModel = Provider.of<NotificationModel>(context, listen: true);
    return PopupMenuButton(
      offset: const Offset(5, 50),
      tooltip: multiLanguageString(name: "notification", defaultValue: "Notification", context: context),
      padding: const EdgeInsets.all(0),
      constraints: const BoxConstraints(minWidth: 400, maxWidth: 400, maxHeight: 500),
      itemBuilder: (context) => [
        PopupMenuItem(
          enabled: false,
          padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MultiLanguageText(
                name: "Notification",
                defaultValue: "Notification",
                style: Style(context).styleTitleDialog,
              ),
              ElevatedButton(
                child: const MultiLanguageText(name: "view_all_cap", defaultValue: "View All"),
                onPressed: () {
                  navigationModel.navigate("/notification");
                },
              ),
            ],
          ),
        ),
        for (var notification in notificationModel.sysNotificationList)
          PopupMenuItem(
            onTap: () async {
              if (notification["itemType"] == "test_case") onClickNotiTestCase(notification);
              if (notification["itemType"] == "test_run") onClickNotiTestRun(notification["itemId"], navigationModel);
              if (notification["itemType"] == "test_batch_run") onClickNotiTestBatchRun(notification["itemId"], navigationModel);
              if (notification["readStatus"] == 0) {
                await onReadNoti(notification["id"]);
                notificationModel.reloadData();
              }
            },
            padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
            child: Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        notification["title"],
                        style: TextStyle(
                          color: const Color.fromRGBO(0, 0, 0, 0.8),
                          fontWeight: FontWeight.bold,
                          fontSize: context.fontSizeManager.fontSizeText14,
                        ),
                      ),
                      Text(
                        notification["content"],
                        style: TextStyle(
                          color: const Color.fromRGBO(0, 0, 0, 0.8),
                          fontSize: context.fontSizeManager.fontSizeText14,
                        ),
                      ),
                      Text(
                        DateFormat('HH:mm:ss dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(notification["createDate"]).toLocal()),
                        style: TextStyle(
                          color: const Color.fromRGBO(133, 135, 145, 1),
                          fontSize: context.fontSizeManager.fontSizeText14,
                        ),
                      ),
                    ],
                  ),
                ),
                if (notification["readStatus"] == 0)
                  Icon(
                    Icons.fiber_manual_record,
                    color: Colors.blue[900],
                  )
              ],
            ),
          ),
      ],
      splashRadius: 1,
      onOpened: () {
        notificationModel.unmarkedNewNotification();
      },
      icon: Container(
          padding: const EdgeInsets.symmetric(horizontal: 11, vertical: 11),
          decoration: BoxDecoration(
            color: const Color.fromRGBO(216, 218, 229, .7),
            borderRadius: BorderRadius.circular(50),
          ),
          child: notificationModel.hasNewNotification
              ? Badge(
                  label: Text('${notificationModel.lastNotificationRowCount}'),
                  child: const Icon(
                    Icons.notifications,
                    size: 18,
                    color: Color.fromRGBO(49, 49, 49, 1),
                  ),
                )
              : const Icon(
                  Icons.notifications,
                  size: 18,
                  color: Color.fromRGBO(49, 49, 49, 1),
                )),
    );
  }
}
