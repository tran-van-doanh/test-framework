import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/config.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/api.dart';
import 'package:crypto/crypto.dart';
import 'package:carousel_slider/carousel_slider.dart';

import '../../common/custom_state.dart';
import '../../common/dynamic_dropdown_button.dart';
import '../../common/dynamic_text_field.dart';
import '../../components/dialog/dialog_delete.dart';
import '../../components/style.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends CustomState<LoginScreen> {
  final CarouselController _controller = CarouselController();
  int _current = 0;
  List<Widget> images = [];
  @override
  void initState() {
    super.initState();
    images = [
      Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          RichText(
            text: TextSpan(children: [
              TextSpan(
                  text: "THE NO-CODE QA PLATFORM\n",
                  style: GoogleFonts.montserrat(
                    textStyle: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText12,
                      color: const Color.fromRGBO(216, 223, 229, 1),
                    ),
                  )),
              TextSpan(
                  text: "\nGet up and running quickly, ",
                  style: GoogleFonts.montserrat(
                    textStyle: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText18,
                      color: const Color.fromRGBO(216, 223, 229, 1),
                    ),
                  )),
              TextSpan(
                  text: "code required",
                  style: GoogleFonts.montserrat(
                    textStyle: TextStyle(
                      decoration: TextDecoration.lineThrough,
                      fontSize: context.fontSizeManager.fontSizeText18,
                      color: const Color.fromRGBO(216, 223, 229, 1),
                    ),
                  )),
              TextSpan(
                  text: "\nNo Code Required",
                  style: GoogleFonts.yellowtail(
                    textStyle: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText24,
                      color: const Color.fromARGB(255, 236, 128, 141),
                    ),
                  )),
            ]),
          ),
        ],
      ),
      Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          RichText(
            text: TextSpan(children: [
              TextSpan(
                  text: "AUTOMATION TESTING\n",
                  style: GoogleFonts.montserrat(
                    textStyle: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText12,
                      color: const Color.fromRGBO(216, 223, 229, 1),
                    ),
                  )),
              TextSpan(
                  text: "\nThe flexibility you need for end-to-end test coverage",
                  style: GoogleFonts.montserrat(
                    textStyle: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText18,
                      color: const Color.fromRGBO(216, 223, 229, 1),
                    ),
                  )),
            ]),
          ),
        ],
      ),
      Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          RichText(
            text: TextSpan(children: [
              TextSpan(
                  text: "Move fast, without breaking things.\n".toUpperCase(),
                  style: GoogleFonts.montserrat(
                    textStyle: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText12,
                      color: const Color.fromRGBO(216, 223, 229, 1),
                    ),
                  )),
              TextSpan(
                  text: "\nGet the speed of automation and the intelligence of humans in the only unified QA platform that anyone can use",
                  style: GoogleFonts.montserrat(
                    textStyle: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText18,
                      color: const Color.fromRGBO(216, 223, 229, 1),
                    ),
                  )),
            ]),
          ),
        ],
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: LayoutBuilder(builder: (buildContext, boxConstraints) {
      return ListView(
        children: [
          Row(
            children: [
              if (boxConstraints.maxWidth > 1000)
                Container(
                  constraints: BoxConstraints(minHeight: MediaQuery.of(context).size.height),
                  width: boxConstraints.maxWidth > 1300 ? boxConstraints.maxWidth / 4 : boxConstraints.maxWidth / 3,
                  color: const Color.fromRGBO(49, 61, 80, 1),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: 65,
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            gradient: const LinearGradient(
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                                colors: [Color.fromRGBO(112, 147, 255, 1), Color.fromRGBO(61, 90, 254, 1)]),
                            borderRadius: BorderRadius.circular(50),
                          ),
                          child: SvgPicture.asset(
                            'assets/images/logo.svg',
                            colorFilter: const ColorFilter.mode(Colors.white, BlendMode.srcIn),
                          ),
                        ),
                        Image.asset('assets/images/logo1.png'),
                        Container(
                            margin: const EdgeInsets.only(top: 20),
                            color: Colors.transparent,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                CarouselSlider(
                                  items: images,
                                  options: CarouselOptions(
                                      enlargeCenterPage: false,
                                      aspectRatio: 1 / 1,
                                      viewportFraction: 1,
                                      autoPlay: true,
                                      onPageChanged: (index, reason) {
                                        setState(() {
                                          _current = index;
                                        });
                                      }),
                                  carouselController: _controller,
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    for (int i = 0; i < images.length; i++)
                                      Container(
                                        height: 8,
                                        width: 8,
                                        margin: const EdgeInsets.all(5),
                                        decoration: BoxDecoration(
                                          color: _current == i ? Colors.blue : Colors.white,
                                          shape: BoxShape.circle,
                                        ),
                                      )
                                  ],
                                ),
                              ],
                            )),
                      ],
                    ),
                  ),
                ),
              Expanded(
                child: ConstrainedBox(
                  constraints: BoxConstraints(minHeight: MediaQuery.of(context).size.height),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Container(
                            margin: const EdgeInsets.fromLTRB(0, 20, 50, 20),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                MultiLanguageText(
                                  name: "dont_have_an_account",
                                  defaultValue: "Don't have an account? ",
                                  style: TextStyle(fontSize: context.fontSizeManager.fontSizeText15),
                                ),
                                InkWell(
                                  child: MultiLanguageText(
                                    name: "get_started",
                                    defaultValue: "Get started",
                                    style: TextStyle(color: const Color.fromRGBO(61, 90, 254, 1), fontSize: context.fontSizeManager.fontSizeText15),
                                  ),
                                  onTap: () {},
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        width: boxConstraints.maxWidth > 1400
                            ? boxConstraints.maxWidth * 3 / 4
                            : boxConstraints.maxWidth > 1000
                                ? boxConstraints.maxWidth * 3 / 4
                                : boxConstraints.maxWidth,
                        child: Center(
                            child: SizedBox(
                          width: 400,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                alignment: Alignment.centerLeft,
                                margin: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                                child: MultiLanguageText(
                                    name: "sign_in_to_qa_platform",
                                    defaultValue: "SIGN IN TO QA PLATFORM",
                                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: context.fontSizeManager.fontSizeText18),
                                    align: TextAlign.left),
                              ),
                              const LoginForm(),
                            ],
                          ),
                        )),
                      ),
                      Container(
                        margin: const EdgeInsets.fromLTRB(0, 20, 50, 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              "2022 © LLQ Software Services",
                              style: TextStyle(fontSize: context.fontSizeManager.fontSizeText15, color: Colors.black, fontWeight: FontWeight.w400),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ],
      );
    }));
  }
}

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends CustomState<LoginForm> {
  late Future futureLogin;
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool isVisiblePassword = true;
  String? server;
  List servers = [];
  handleLogin() async {
    setBaseUrl(server!);
    var storage = Provider.of<SecurityModel>(context, listen: false).storage;
    await storage.setItem("server", server);
    var password = _passwordController.text;
    if (password.isNotEmpty) {
      var hashMethod = "SHA256";
      try {
        if (mounted) {
          var hashMethodResponse = await httpGet("/test-framework-api/auth/hash-method", context);
          hashMethod = hashMethodResponse["body"]["result"];
        }
      } catch (error) {
        //bypass
      }
      if ("SHA256" == hashMethod) {
        var passwordBytes = utf8.encode(password);
        var passwordDigest = sha256.convert(passwordBytes);
        password = base64.encode(passwordDigest.bytes);
      }
    }
    var loginRequest = {
      "authType": "local",
      "username": _usernameController.text,
      "password": password,
    };
    try {
      if (mounted) {
        var loginResponse = await httpPost("/test-framework-api/auth/login", loginRequest, context);
        if (mounted) {
          if (loginResponse.containsKey("body") && loginResponse["body"] is String == false && loginResponse["body"].containsKey("result")) {
            var securityModel = Provider.of<SecurityModel>(context, listen: false);
            var navigationModel = Provider.of<NavigationModel>(context, listen: false);
            var webElementModel = Provider.of<WebElementModel>(context, listen: false);
            var elementRepositoryModel = Provider.of<ElementRepositoryModel>(context, listen: false);
            var debugStatusModel = Provider.of<DebugStatusModel>(context, listen: false);
            var testCaseConfigModel = Provider.of<TestCaseConfigModel>(context, listen: false);
            var testCaseDirectoryModel = Provider.of<TestCaseDirectoryModel>(context, listen: false);
            var sideBarDocumentModel = Provider.of<SideBarDocumentModel>(context, listen: false);
            var playTestCaseModel = Provider.of<PlayTestCaseModel>(context, listen: false);
            await securityModel.clearProvider();
            await navigationModel.clearProvider();
            await webElementModel.clearProvider();
            await elementRepositoryModel.clearProvider();
            await debugStatusModel.clearProvider();
            await testCaseConfigModel.clearProvider();
            await testCaseDirectoryModel.clearProvider();
            await sideBarDocumentModel.clearProvider();
            await playTestCaseModel.clearProvider();
            await securityModel.setAuthorization(authorization: loginResponse["body"]["result"]);
            await navigationModel.navigate("");
          } else {
            showToast(
              context: context,
              msg: loginResponse["body"]["errorMessage"].toString(),
              color: Colors.red,
              icon: const Icon(Icons.error),
            );
          }
        }
      }
    } catch (error) {
      if (mounted) {
        showToast(
          context: context,
          msg: error.toString(),
          color: Colors.red,
          icon: const Icon(Icons.error),
        );
      }
    }
  }

  @override
  void initState() {
    super.initState();
    futureLogin = setInitPage();
  }

  setInitPage() async {
    var storage = Provider.of<SecurityModel>(context, listen: false).storage;
    if (storage.getItem('server') != null && storage.getItem("servers") != null && storage.getItem("servers") is List) {
      server = storage.getItem('server');
    } else {
      server = baseUrl;
      await storage.setItem("servers", [baseUrl]);
    }
    servers = [];
    servers.addAll(storage.getItem("servers"));
    return 0;
  }

  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: futureLogin,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return AutofillGroup(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                (kIsWeb)
                    ? Container()
                    : Padding(
                        padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                        child: MultiLanguageText(
                          name: "server",
                          defaultValue: "Server",
                          isLowerCase: false,
                          style: TextStyle(
                            fontSize: context.fontSizeManager.fontSizeText13,
                            color: const Color.fromRGBO(49, 49, 49, 1),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                (kIsWeb)
                    ? Container()
                    : Container(
                        margin: const EdgeInsets.fromLTRB(0, 5, 0, 15),
                        child: Row(
                          children: [
                            Expanded(
                              child: DynamicDropdownButton(
                                borderRadius: 5,
                                value: server,
                                hint: multiLanguageString(name: "choose_a_server", defaultValue: "Choose a server", context: context),
                                items: servers.map<DropdownMenuItem<String>>((result) {
                                  return DropdownMenuItem(
                                    value: result,
                                    child: Text(
                                      result,
                                      style: const TextStyle(overflow: TextOverflow.ellipsis),
                                    ),
                                  );
                                }).toList(),
                                onChanged: (newValue) {
                                  setState(() {
                                    server = newValue;
                                  });
                                },
                              ),
                            ),
                            if (servers.length > 1)
                              Container(
                                margin: const EdgeInsets.only(left: 5),
                                height: 46,
                                width: 50,
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(padding: const EdgeInsets.all(5), backgroundColor: Colors.red),
                                  onPressed: () {
                                    showDialog<String>(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return DialogDelete(
                                          selectedItem: server,
                                          callback: () async {
                                            var storage = Provider.of<SecurityModel>(context, listen: false).storage;
                                            servers.remove(server);
                                            await storage.setItem("servers", servers);
                                            setState(() {
                                              server = servers[0];
                                            });
                                          },
                                          type: 'server',
                                        );
                                      },
                                    );
                                  },
                                  child: const Icon(Icons.delete),
                                ),
                              ),
                            Container(
                              margin: const EdgeInsets.only(left: 5),
                              height: 46,
                              width: 50,
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(padding: const EdgeInsets.all(5)),
                                onPressed: () {
                                  showDialog<String>(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return AddServerWidget(
                                          onClickAdd: () async {
                                            var storage = Provider.of<SecurityModel>(context, listen: false).storage;
                                            setState(() {
                                              servers = [];
                                              servers.addAll(storage.getItem("servers"));
                                            });
                                          },
                                        );
                                      });
                                },
                                child: const Icon(Icons.add),
                              ),
                            )
                          ],
                        ),
                      ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                  child: MultiLanguageText(
                    name: "username",
                    defaultValue: "Username",
                    isLowerCase: false,
                    style: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText13,
                      color: const Color.fromRGBO(49, 49, 49, 1),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(0, 5, 0, 15),
                  child: TextField(
                    controller: _usernameController,
                    autofillHints: const [AutofillHints.username],
                    decoration: const InputDecoration(
                      hintStyle: TextStyle(color: Colors.grey),
                      hintText: "you@yourcompany.com",
                      border: OutlineInputBorder(),
                    ),
                    onChanged: (text) => setState(
                      () {},
                    ),
                    onSubmitted: (value) {
                      handleLogin();
                    },
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                      child: MultiLanguageText(
                        name: "password",
                        defaultValue: "Password",
                        isLowerCase: false,
                        style: TextStyle(
                          fontSize: context.fontSizeManager.fontSizeText13,
                          color: const Color.fromRGBO(49, 49, 49, 1),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    MouseRegion(
                      cursor: SystemMouseCursors.click,
                      child: GestureDetector(
                        child: MultiLanguageText(
                          name: "forgot_your_password",
                          defaultValue: "Forgot your password?",
                          style: TextStyle(color: Colors.grey, fontSize: context.fontSizeManager.fontSizeText13),
                        ),
                        onTap: () {},
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                  child: TextField(
                    controller: _passwordController,
                    autofillHints: const [AutofillHints.password],
                    obscureText: isVisiblePassword,
                    decoration: InputDecoration(
                      hintStyle: const TextStyle(color: Colors.grey),
                      border: const OutlineInputBorder(),
                      hintText: multiLanguageString(name: "enter_your_password", defaultValue: "Enter your password", context: context),
                      suffixIcon: IconButton(
                        splashRadius: 1,
                        onPressed: () {
                          setState(() {
                            isVisiblePassword = !isVisiblePassword;
                          });
                        },
                        icon: Icon(isVisiblePassword ? Icons.visibility_off : Icons.visibility),
                      ),
                    ),
                    onSubmitted: (value) {
                      handleLogin();
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: Center(
                      child: SizedBox(
                    width: 120,
                    height: 40,
                    child: ElevatedButton(
                      onPressed: () {
                        if (_usernameController.text.isNotEmpty && _passwordController.text.isNotEmpty) {
                          handleLogin();
                        } else {
                          showToast(
                            context: context,
                            msg: multiLanguageString(
                                name: "incorrect_account_or_password", defaultValue: "Incorrect account or password", context: context),
                            color: Colors.red,
                            icon: const Icon(Icons.error),
                          );
                        }
                      },
                      child: const MultiLanguageText(name: "sign_in", defaultValue: "Sign In", isLowerCase: false),
                    ),
                  )),
                ),
              ],
            ),
          );
        } else if (snapshot.hasError) {
          return Text('${snapshot.error}');
        }
        return const Center(child: CircularProgressIndicator());
      },
    );
  }
}

class AddServerWidget extends StatefulWidget {
  final VoidCallback onClickAdd;
  const AddServerWidget({Key? key, required this.onClickAdd}) : super(key: key);

  @override
  State<AddServerWidget> createState() => _AddServerWidgetState();
}

class _AddServerWidgetState extends State<AddServerWidget> {
  TextEditingController serverController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    var securityModel = Provider.of<SecurityModel>(context, listen: false);
    return AlertDialog(
      contentPadding: const EdgeInsets.all(0),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "add_new_server",
            defaultValue: "Add new server",
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Icon(
                Icons.close,
                color: Colors.black,
              ))
        ],
      ),
      content: Container(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
        decoration: const BoxDecoration(
          border: Border(
            top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
            bottom: BorderSide(
              color: Color.fromRGBO(216, 218, 229, 1),
            ),
          ),
        ),
        margin: const EdgeInsets.only(top: 20, bottom: 10),
        width: 600,
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "server", defaultValue: "Server", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicTextField(
                        controller: serverController,
                        hintText: multiLanguageString(name: "enter_a_server", defaultValue: "Enter a server", context: context),
                        isRequiredNotEmpty: true,
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
      actionsAlignment: MainAxisAlignment.center,
      actions: [
        SizedBox(
          height: 40,
          width: 120,
          child: ElevatedButton(
            onPressed: () async {
              if (_formKey.currentState!.validate()) {
                List servers = securityModel.storage.getItem("servers");
                if (!servers.contains(serverController.text)) {
                  servers.add(serverController.text);
                  await securityModel.storage.setItem("servers", servers);
                  widget.onClickAdd();
                  if (mounted) {
                    Navigator.of(this.context).pop();
                  }
                } else {
                  showToast(
                      context: context,
                      msg: multiLanguageString(name: "server_already_exists", defaultValue: "Server already exists", context: context),
                      color: Colors.red,
                      icon: const Icon(Icons.error));
                }
              }
            },
            child: const MultiLanguageText(
              name: "ok",
              defaultValue: "Ok",
              isLowerCase: false,
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
      ],
    );
  }
}
