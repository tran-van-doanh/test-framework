import 'package:flutter/material.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_checkbox_label.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_form.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';

import '../../../common/custom_state.dart';

class DialogOptionOrganizationUserWidget extends StatefulWidget {
  final String type;
  final String title;
  final String? id;
  final Function? callbackOptionTest;
  const DialogOptionOrganizationUserWidget({Key? key, required this.type, this.id, this.callbackOptionTest, required this.title}) : super(key: key);

  @override
  State<DialogOptionOrganizationUserWidget> createState() => _DialogOptionOrganizationUserWidgetState();
}

class _DialogOptionOrganizationUserWidgetState extends CustomState<DialogOptionOrganizationUserWidget> {
  late Future future;
  final TextEditingController _orgFullnameController = TextEditingController();
  final TextEditingController _orgNicknameController = TextEditingController();
  final TextEditingController _orgEmailController = TextEditingController();
  final TextEditingController _orgDescriptionController = TextEditingController();
  int _status = 0;
  final _formKey = GlobalKey<FormState>();
  var sysOrganizationUser = {};
  var optionList = [];
  bool isSelectAll = false;
  dynamic selectedItem;
  @override
  void initState() {
    future = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    await getListPermOrganizationUser();
    if (widget.id != null) {
      await getSelectedItem(widget.id!);
      await setInitValue();
    }
    return 0;
  }

  getSelectedItem(String id) async {
    var response = await httpGet("/test-framework-api/user/sys/sys-user/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        selectedItem = response["body"]["result"];
      });
    }
  }

  getListPermOrganizationUser() async {
    optionList = [];
    var sysOrganizationRoleFindResponse =
        await httpPost("/test-framework-api/user/sys/sys-item-role/search", {"status": 1, "itemType": "organization"}, context);
    if (sysOrganizationRoleFindResponse.containsKey("body") && sysOrganizationRoleFindResponse["body"] is String == false) {
      setState(() {
        optionList =
            sysOrganizationRoleFindResponse["body"]["resultList"].map((sysRole) => {"value": sysRole["id"], "label": sysRole["name"]}).toList();
      });
    }
    if (selectedItem != null && mounted) {
      var sysOrganizationUserGetResponse = await httpGet("/test-framework-api/user/sys/sys-user/${selectedItem["id"]}", context);
      if (sysOrganizationUserGetResponse.containsKey("body") && sysOrganizationUserGetResponse["body"] is String == false) {
        setState(() {
          sysOrganizationUser = sysOrganizationUserGetResponse["body"]["result"];
        });
      }
      if (optionList.every((element) => sysOrganizationUser["sysOrganizationRoleIdList"].contains(element["value"]))) {
        setState(() {
          isSelectAll = true;
        });
      } else {
        setState(() {
          isSelectAll = false;
        });
      }
    } else {
      setState(() {
        sysOrganizationUser = {"status": 0};
      });
    }
  }

  setInitValue() {
    setState(() {
      if (selectedItem != null) {
        _status = selectedItem["status"] ?? 0;
        _orgFullnameController.text = selectedItem["fullname"] ?? "";
        _orgNicknameController.text = selectedItem["nickname"] ?? "";
        _orgEmailController.text = selectedItem["email"] ?? "";
        _orgDescriptionController.text = selectedItem["description"] ?? "";
      }
    });
  }

  @override
  void dispose() {
    _orgFullnameController.dispose();
    _orgNicknameController.dispose();
    _orgEmailController.dispose();
    _orgDescriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: future,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Scaffold(
              body: Column(
            children: [
              headerWidget(context),
              const Divider(color: Color.fromRGBO(216, 218, 229, 1)),
              Expanded(
                child: Container(
                  margin: const EdgeInsets.only(top: 20, bottom: 10),
                  child: Form(
                    key: _formKey,
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                        child: bodyWidget(context),
                      ),
                    ),
                  ),
                ),
              ),
              btnWidget()
            ],
          ));
        } else if (snapshot.hasError) {
          return Text('${snapshot.error}');
        }
        return const Center(child: CircularProgressIndicator());
      },
    );
  }

  Padding btnWidget() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20, top: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ButtonSave(onPressed: () {
            if (_formKey.currentState!.validate()) {
              var result = {};
              if (selectedItem != null) {
                result = selectedItem;
                result["fullname"] = _orgFullnameController.text;
                result["nickname"] = _orgNicknameController.text;
                result["email"] = _orgEmailController.text;
                result["description"] = _orgDescriptionController.text;
                result["status"] = _status;
                result["sysOrganizationRoleIdList"] = sysOrganizationUser["sysOrganizationRoleIdList"];
              } else {
                result = {
                  "fullname": _orgFullnameController.text,
                  "nickname": _orgNicknameController.text,
                  "email": _orgEmailController.text,
                  "description": _orgDescriptionController.text,
                  "status": _status,
                  "sysOrganizationRoleIdList": sysOrganizationUser["sysOrganizationRoleIdList"]
                };
              }
              widget.callbackOptionTest!(result);
            }
          }),
          const ButtonCancel()
        ],
      ),
    );
  }

  Column bodyWidget(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "fullname", defaultValue: "Fullname", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _orgFullnameController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "nickname", defaultValue: "Nickname", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _orgNicknameController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "email", defaultValue: "Email", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _orgEmailController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                  enabled: widget.type != "edit",
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "description", defaultValue: "Description", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _orgDescriptionController,
                  hintText: "...",
                  maxline: 3,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "status", defaultValue: "Status", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                  value: _status,
                  hint: multiLanguageString(name: "choose_a_status", defaultValue: "Choose a status", context: context),
                  items: [
                    {"name": multiLanguageString(name: "active", defaultValue: "Active", context: context), "value": 1},
                    {"name": multiLanguageString(name: "inactive", defaultValue: "Inactive", context: context), "value": 0},
                  ].map<DropdownMenuItem<int>>((dynamic result) {
                    return DropdownMenuItem(
                      value: result["value"],
                      child: Text(result["name"]),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      _status = newValue!;
                    });
                  },
                  borderRadius: 5,
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MultiLanguageText(name: "permission", defaultValue: "Permission", style: Style(context).styleTitleDialog),
              LabeledCheckbox(
                  label: isSelectAll
                      ? multiLanguageString(name: "unselect_all", defaultValue: "Unselect all", context: context)
                      : multiLanguageString(name: "select_all", defaultValue: "Select all", context: context),
                  value: isSelectAll,
                  onChanged: (value) {
                    setState(() {
                      if (isSelectAll) {
                        sysOrganizationUser["sysOrganizationRoleIdList"] = [];
                      } else {
                        sysOrganizationUser["sysOrganizationRoleIdList"] = [];
                        for (var element in optionList) {
                          sysOrganizationUser["sysOrganizationRoleIdList"].add(element["value"]);
                        }
                      }
                      isSelectAll = value;
                    });
                  }),
            ],
          ),
        ),
        MyMultipleSelect(
            selectedValue: sysOrganizationUser["sysOrganizationRoleIdList"] ?? [],
            optionList: optionList,
            onChanged: (value) {
              sysOrganizationUser["sysOrganizationRoleIdList"] = value;
            })
      ],
    );
  }

  Padding headerWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 35, 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "type_user",
            defaultValue: "\$0 USER",
            variables: [widget.title],
            isLowerCase: false,
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
            onPressed: () => Navigator.pop(context, 'cancel'),
            child: const Icon(
              Icons.close,
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }
}
