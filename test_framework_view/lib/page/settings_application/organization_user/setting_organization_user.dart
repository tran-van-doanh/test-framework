import 'dart:convert';
import 'dart:io';

import 'package:crypto/crypto.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/common/file_download.dart';
import 'package:test_framework_view/components/dialog/dialog_delete.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/config.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/settings_application/organization_user/option_organization_user.dart';
import 'package:test_framework_view/type.dart';

import '../../../common/custom_state.dart';

class SettingOrganizationUserWidget extends StatefulWidget {
  final String typeNav;
  const SettingOrganizationUserWidget({Key? key, required this.typeNav}) : super(key: key);

  @override
  State<SettingOrganizationUserWidget> createState() => _SettingOrganizationUserWidgetState();
}

class _SettingOrganizationUserWidgetState extends CustomState<SettingOrganizationUserWidget> {
  late Future futuresysOrganizationUserList;
  var sysOrganizationUserSearchRequest = {};
  var sysOrganizationUserRowCount = 0;
  var sysOrganizationUserCurrentPage = 1;
  var sysOrganizationUserResultList = [];
  var sysOrganizationUserRowPerPage = 10;
  var sysOrganizationUserStatusMap = {1: "Active", 0: "Inactive"};
  var prjProjectId = "";
  PlatformFile? objFile;
  TextEditingController fullnameSearchController = TextEditingController();
  TextEditingController emailSearchController = TextEditingController();
  @override
  void initState() {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    prjProjectId = navigationModel.prjProjectId ?? "";
    futuresysOrganizationUserList = sysOrganizationUserPageChange(sysOrganizationUserCurrentPage);
    super.initState();
  }

  sysOrganizationUserSearch() {
    sysOrganizationUserSearchRequest = {"emailLike": emailSearchController.text, "fullnameLike": fullnameSearchController.text};
    futuresysOrganizationUserList = sysOrganizationUserPageChange(sysOrganizationUserCurrentPage);
  }

  sysOrganizationUserPageChange(page) async {
    if ((page - 1) * sysOrganizationUserRowPerPage > sysOrganizationUserRowCount) {
      page = (1.0 * sysOrganizationUserRowCount / sysOrganizationUserRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * sysOrganizationUserRowPerPage, "queryLimit": sysOrganizationUserRowPerPage};
    if (sysOrganizationUserSearchRequest["fullnameLike"] != null && sysOrganizationUserSearchRequest["fullnameLike"].isNotEmpty) {
      findRequest["fullnameLike"] = "%${sysOrganizationUserSearchRequest["fullnameLike"]}%";
    }
    if (sysOrganizationUserSearchRequest["emailLike"] != null && sysOrganizationUserSearchRequest["emailLike"].isNotEmpty) {
      findRequest["emailLike"] = "%${sysOrganizationUserSearchRequest["emailLike"]}%";
    }
    var response = await httpPost("/test-framework-api/user/sys/sys-user/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        sysOrganizationUserCurrentPage = page;
        sysOrganizationUserRowCount = response["body"]["rowCount"];
        sysOrganizationUserResultList = response["body"]["resultList"];
      });
    }
    return 0;
  }

  handleDeleteOrganizationUser(sysOrganizationUser) async {
    var response = await httpDelete("/test-framework-api/user/sys/sys-user/${sysOrganizationUser["id"]}", context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        await sysOrganizationUserPageChange(sysOrganizationUserCurrentPage);
      }
    }
  }

  handleAddOrganizationUser(result) async {
    var response = await httpPut("/test-framework-api/user/sys/sys-user", result, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      if (response.containsKey("body") && response["body"] is String == false && mounted) {
        if (response["body"].containsKey("errorMessage")) {
          showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
        } else {
          showToast(
              context: context,
              msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
              color: Colors.greenAccent,
              icon: const Icon(Icons.done));
          Navigator.of(context).pop();
          await sysOrganizationUserPageChange(sysOrganizationUserCurrentPage);
        }
      }
    }
  }

  handleEditOrganizationUser(result, sysOrganizationUser) async {
    var response = await httpPost("/test-framework-api/user/sys/sys-user/${sysOrganizationUser["id"]}", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await sysOrganizationUserPageChange(sysOrganizationUserCurrentPage);
      }
    }
  }

  @override
  void dispose() {
    fullnameSearchController.dispose();
    emailSearchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, LanguageModel>(builder: (context, navigationModel, languageModel, child) {
      return FutureBuilder(
        future: futuresysOrganizationUserList,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Scaffold(
              body: Column(
                children: [
                  Expanded(
                    child: ListView(
                      children: [
                        (widget.typeNav == "resource-pool")
                            ? NavMenuTabV1(
                                currentUrl: "/user/user",
                                margin: const EdgeInsets.fromLTRB(50, 20, 50, 0),
                                navigationList: context.rpUserMenuList,
                              )
                            : OrganizationNavMenuTab(
                                margin: const EdgeInsets.fromLTRB(45, 20, 30, 0),
                                navigationList: context.settingsMenuList,
                              ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              MultiLanguageText(
                                name: "system_organization_user_count",
                                defaultValue: "System Organization User ( \$0)",
                                variables: ["$sysOrganizationUserRowCount"],
                                style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
                              ),
                              searchForm(),
                              tableWidget(),
                              DynamicTablePagging(
                                sysOrganizationUserRowCount,
                                sysOrganizationUserCurrentPage,
                                sysOrganizationUserRowPerPage,
                                pageChangeHandler: (page) {
                                  setState(() {
                                    futuresysOrganizationUserList = sysOrganizationUserPageChange(page);
                                  });
                                },
                                rowPerPageChangeHandler: (rowPerPage) {
                                  setState(() {
                                    sysOrganizationUserRowPerPage = rowPerPage!;
                                    futuresysOrganizationUserList = sysOrganizationUserPageChange(1);
                                  });
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  btnWidget(context),
                ],
              ),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        },
      );
    });
  }

  Padding btnWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 40, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            margin: const EdgeInsets.only(right: 10),
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              icon: const IconTheme(
                data: IconThemeData(color: Color.fromRGBO(49, 49, 49, 1)),
                child: Icon(Icons.add),
              ),
              label: MultiLanguageText(
                name: "new_organization_user",
                defaultValue: "New Organization User",
                isLowerCase: false,
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
              onPressed: () {
                Navigator.of(context).push(
                  createRoute(
                    DialogOptionOrganizationUserWidget(
                        type: "create",
                        title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                        callbackOptionTest: (result) {
                          handleAddOrganizationUser(result);
                        }),
                  ),
                );
              },
            ),
          ),
          Container(
            margin: const EdgeInsets.only(right: 10),
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              onPressed: () async {
                var requestBody = {};
                File? downloadedFile = await httpDownloadFileWithBodyRequest(
                  context,
                  "/test-framework-api/user/system-file/sys-user/export-excel",
                  Provider.of<SecurityModel>(context, listen: false).authorization,
                  'Output.xlsx',
                  requestBody,
                );
                if (downloadedFile != null && mounted) {
                  var snackBar = SnackBar(
                    content: MultiLanguageText(name: "file_downloaded_to", defaultValue: "File downloaded to \$0 .", variables: ["$downloadedFile"]),
                  );
                  ScaffoldMessenger.of(this.context).showSnackBar(snackBar);
                }
              },
              icon: const FaIcon(FontAwesomeIcons.fileArrowDown, color: Color.fromRGBO(49, 49, 49, 1), size: 20),
              label: MultiLanguageText(
                name: "export",
                defaultValue: "Export",
                isLowerCase: false,
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
              tooltip: multiLanguageString(name: "export_to_excel", defaultValue: "Export to Excel", context: context),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(right: 10),
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              onPressed: () async {
                objFile = await selectFile(['xlsx']);
                uploadFile();
              },
              icon: const FaIcon(FontAwesomeIcons.fileArrowUp, color: Color.fromRGBO(49, 49, 49, 1), size: 20),
              label: MultiLanguageText(
                name: "import",
                defaultValue: "Import",
                isLowerCase: false,
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
              tooltip: multiLanguageString(name: "import_to_excel", defaultValue: "Import to Excel", context: context),
            ),
          ),
        ],
      ),
    );
  }

  Container tableWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 30),
      child: CustomDataTableWidget(
        columns: [
          Expanded(
            flex: 2,
            child: MultiLanguageText(
              name: "fullname",
              defaultValue: "Fullname",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            flex: 2,
            child: MultiLanguageText(
              name: "email",
              defaultValue: "Email",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            flex: 2,
            child: MultiLanguageText(
              name: "description",
              defaultValue: "Description",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            child: MultiLanguageText(
              name: "status",
              defaultValue: "Status",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          const SizedBox(
            width: 38,
          ),
        ],
        rows: [
          for (var sysOrganizationUser in sysOrganizationUserResultList)
            CustomDataRow(
              cells: [
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.all(5),
                    child: Row(
                      children: [
                        const Icon(Icons.description, color: Color.fromRGBO(61, 148, 254, 1)),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Text(
                            sysOrganizationUser["fullname"] ?? "",
                            overflow: TextOverflow.ellipsis,
                            style: Style(context).styleTextDataCell,
                            maxLines: 3,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text(
                    sysOrganizationUser["email"] ?? "",
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text(
                    sysOrganizationUser["description"] ?? "",
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                Expanded(
                  child: Text(
                    sysOrganizationUserStatusMap[sysOrganizationUser["status"]] ?? "Not defined",
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                PopupMenuButton(
                  offset: const Offset(5, 50),
                  tooltip: '',
                  splashRadius: 10,
                  icon: const Icon(
                    Icons.more_vert,
                  ),
                  itemBuilder: (context) => [
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.of(context).push(
                            createRoute(
                              DialogOptionOrganizationUserWidget(
                                  type: "edit",
                                  title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                  id: sysOrganizationUser["id"],
                                  callbackOptionTest: (result) {
                                    handleEditOrganizationUser(result, sysOrganizationUser);
                                  }),
                            ),
                          );
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                        leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          dialogChangePasswordOrganizationUser(sysOrganizationUser);
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "change_password", defaultValue: "Change password"),
                        leading: const Icon(Icons.settings, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          dialogDeleteOrganizationUser(sysOrganizationUser);
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "delete", defaultValue: "Delete"),
                        leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                  ],
                ),
              ],
            ),
        ],
      ),
    );
  }

  Widget searchForm() {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                child: SizedBox(
                  height: 50,
                  child: DynamicTextField(
                    controller: fullnameSearchController,
                    onComplete: () => sysOrganizationUserSearch(),
                    onChanged: (value) {
                      setState(() {
                        fullnameSearchController.text;
                      });
                    },
                    labelText: multiLanguageString(name: "fullname", defaultValue: "Fullname", context: context),
                    hintText: multiLanguageString(name: "by_fullname", defaultValue: "By fullname", context: context),
                    suffixIcon: (fullnameSearchController.text != "")
                        ? IconButton(
                            onPressed: () {
                              setState(() {
                                fullnameSearchController.clear();
                              });
                              sysOrganizationUserSearch();
                            },
                            icon: const Icon(Icons.clear),
                            splashRadius: 20,
                          )
                        : null,
                  ),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                child: SizedBox(
                  height: 50,
                  child: DynamicTextField(
                    controller: emailSearchController,
                    onComplete: () => sysOrganizationUserSearch(),
                    onChanged: (value) {
                      setState(() {
                        emailSearchController.text;
                      });
                    },
                    labelText: multiLanguageString(name: "email", defaultValue: "Email", context: context),
                    hintText: multiLanguageString(name: "by_email", defaultValue: "By email", context: context),
                    suffixIcon: (emailSearchController.text != "")
                        ? IconButton(
                            onPressed: () {
                              setState(() {
                                emailSearchController.clear();
                              });
                              sysOrganizationUserSearch();
                            },
                            icon: const Icon(Icons.clear),
                            splashRadius: 20,
                          )
                        : null,
                  ),
                ),
              ),
            ),
          ],
        ),
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 20),
              child: ElevatedButton(
                  onPressed: () {
                    sysOrganizationUserSearch();
                  },
                  child: const MultiLanguageText(name: "search", defaultValue: "Search", isLowerCase: false)),
            ),
            ElevatedButton(
                onPressed: () {
                  setState(() {
                    fullnameSearchController.clear();
                    emailSearchController.clear();
                    sysOrganizationUserSearch();
                  });
                },
                child: const MultiLanguageText(name: "reset", defaultValue: "Reset", isLowerCase: false))
          ],
        ),
      ],
    );
  }

  uploadFile() async {
    final request = http.MultipartRequest(
      "POST",
      Uri.parse(
        "$baseUrl/test-framework-api/user/system-file/sys-user/import-excel",
      ),
    );
    request.headers.addAll({"Authorization": Provider.of<SecurityModel>(context, listen: false).authorization!});
    request.files.add(http.MultipartFile.fromBytes('file', await readFile(objFile!), filename: objFile!.name));
    var resp = await request.send();
    var responseDecode = jsonDecode(await resp.stream.bytesToString());
    if (responseDecode.containsKey("errorMessage") && mounted) {
      showToast(context: context, msg: responseDecode["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
    } else if (mounted) {
      showToast(
          context: context,
          msg: multiLanguageString(name: "upload_success", defaultValue: "Upload success", context: context),
          color: Colors.greenAccent,
          icon: const Icon(Icons.done));
    }
  }

  dialogChangePasswordOrganizationUser(selectedDataTable) {
    TextEditingController passwordController = TextEditingController();
    final formKey = GlobalKey<FormState>();
    showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: const EdgeInsets.all(0),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                MultiLanguageText(
                  name: "change_password",
                  defaultValue: "Change password",
                  style: Style(context).styleTitleDialog,
                ),
                TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Icon(
                      Icons.close,
                      color: Colors.black,
                    ))
              ],
            ),
            content: Container(
              padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
              decoration: const BoxDecoration(
                border: Border(
                  top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
                  bottom: BorderSide(
                    color: Color.fromRGBO(216, 218, 229, 1),
                  ),
                ),
              ),
              margin: const EdgeInsets.only(top: 20, bottom: 10),
              constraints: const BoxConstraints(minWidth: 600),
              child: SingleChildScrollView(
                child: Form(
                  key: formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                        child: MultiLanguageText(name: "password", defaultValue: "Password", style: Style(context).styleTitleDialog),
                      ),
                      DynamicTextField(
                        controller: passwordController,
                        hintText: multiLanguageString(name: "enter_a_password", defaultValue: "Enter a password", context: context),
                        onChanged: (text) {
                          formKey.currentState!.validate();
                        },
                        isRequiredNotEmpty: true,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            actionsAlignment: MainAxisAlignment.center,
            actions: [
              Container(
                margin: const EdgeInsets.only(right: 5),
                height: 40,
                width: 120,
                child: ElevatedButton(
                  onPressed: () async {
                    if (formKey.currentState!.validate()) {
                      var hashMethod = "SHA256";
                      try {
                        var hashMethodResponse = await httpGet("/test-framework-api/auth/hash-method", context);
                        hashMethod = hashMethodResponse["body"]["result"];
                      } catch (error) {
                        //bypass
                      }
                      var newPasswordValue = passwordController.text;
                      if ("SHA256" == hashMethod) {
                        var passwordBytes = utf8.encode(passwordController.text);
                        var passwordDigest = sha256.convert(passwordBytes);
                        newPasswordValue = base64.encode(passwordDigest.bytes);
                      }
                      var changePasswordRequest = {
                        "email": selectedDataTable["email"],
                        "password": newPasswordValue,
                      };
                      if (mounted) {
                        var sysOrganizationUserUpdateResponse = await httpPost(
                            "/test-framework-api/user/sys/sys-user/${selectedDataTable["id"]}/change-password", changePasswordRequest, this.context);
                        if (sysOrganizationUserUpdateResponse.containsKey("body") &&
                            sysOrganizationUserUpdateResponse["body"] is String == false &&
                            mounted) {
                          Navigator.pop(this.context);
                        }
                      }
                    }
                  },
                  child: const MultiLanguageText(
                    name: "ok",
                    defaultValue: "Ok",
                    isLowerCase: false,
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 5),
                height: 40,
                width: 120,
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  style: ElevatedButton.styleFrom(
                      side: const BorderSide(color: Color.fromRGBO(209, 212, 216, 1)),
                      backgroundColor: Colors.white,
                      foregroundColor: const Color.fromRGBO(61, 90, 254, 1)),
                  child: const MultiLanguageText(
                    name: "cancel",
                    defaultValue: "Cancel",
                    isLowerCase: false,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ],
          );
        });
  }

  dialogDeleteOrganizationUser(sysOrganizationUser) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogDelete(
          selectedItem: sysOrganizationUser,
          callback: () {
            handleDeleteOrganizationUser(sysOrganizationUser);
          },
          type: 'organization_user',
        );
      },
    );
  }
}
