import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/type.dart';

import '../../../common/custom_state.dart';

class SettingScaleWidget extends StatefulWidget {
  const SettingScaleWidget({Key? key}) : super(key: key);

  @override
  State<SettingScaleWidget> createState() => _SettingScaleWidgetState();
}

class _SettingScaleWidgetState extends CustomState<SettingScaleWidget> {
  @override
  Widget build(BuildContext context) {
    return Consumer<ScaleModel>(builder: (context, scaleModel, child) {
      return Scaffold(
        body: ListView(
          children: [
            OrganizationNavMenuTab(
              margin: const EdgeInsets.fromLTRB(45, 20, 30, 0),
              navigationList: context.settingsMenuList,
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      MultiLanguageText(
                        name: "settings",
                        defaultValue: "Settings",
                        style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      MultiLanguageText(
                        name: "scale_and_layout",
                        defaultValue: "Scale and layout",
                        style: TextStyle(fontSize: context.fontSizeManager.fontSizeText24, fontWeight: FontWeight.w500),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      DynamicDropdownButton(
                        labelText: multiLanguageString(
                            name: "change_the_size_of_text", defaultValue: "Change the size of text, apps, and other items", context: context),
                        value: scaleModel.textScaleFactor,
                        onChanged: (newValue) {
                          scaleModel.setTextScaleFactor(newValue!);
                        },
                        items: scaleOptionList.map<DropdownMenuItem<double>>((dynamic result) {
                          return DropdownMenuItem(
                            value: double.tryParse(result["value"].toString()),
                            child: Text(result["label"]),
                          );
                        }).toList(),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    });
  }
}
