import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';

import '../../../common/custom_state.dart';

class DialogOptionEmailTemplateWidget extends StatefulWidget {
  final String type;
  final String title;
  final String? id;
  final Function? callbackOptionTest;
  const DialogOptionEmailTemplateWidget({Key? key, required this.type, this.id, this.callbackOptionTest, required this.title}) : super(key: key);

  @override
  State<DialogOptionEmailTemplateWidget> createState() => _DialogOptionEmailTemplateWidgetState();
}

class _DialogOptionEmailTemplateWidgetState extends CustomState<DialogOptionEmailTemplateWidget> {
  late Future future;
  final TextEditingController titleController = TextEditingController();
  final TextEditingController emailSubjectTemplateController = TextEditingController();
  final TextEditingController emailContentTemplateController = TextEditingController();
  final TextEditingController emailTemplateConfigController = TextEditingController();
  int _status = 0;
  String? emailAccountId;
  String itemType = 'test_case';
  String actionType = 'comment';
  final _formKey = GlobalKey<FormState>();
  dynamic selectedItem;
  List emailAccountList = [];
  @override
  void initState() {
    future = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    await getEmailAccount();
    if (widget.id != null) {
      await getSelectedItem(widget.id!);
      await setInitValue();
    }
    return 0;
  }

  getEmailAccount() async {
    var searchRequest = {"status": 1};
    var response = await httpPost("/test-framework-api/user/noti/noti-email-account/search", searchRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        emailAccountList = response["body"]["resultList"];
      });
    }
  }

  getSelectedItem(String id) async {
    var response = await httpGet("/test-framework-api/user/noti/noti-email-template/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        selectedItem = response["body"]["result"];
      });
    }
  }

  setInitValue() {
    setState(() {
      if (selectedItem != null) {
        emailAccountId = selectedItem["notiEmailAccountId"];
        _status = selectedItem["status"] ?? 0;
        itemType = selectedItem["itemType"] ?? "test_case";
        actionType = selectedItem["actionType"] ?? "comment";
        titleController.text = selectedItem["title"] ?? "";
        emailTemplateConfigController.text = jsonEncode(selectedItem["emailTemplateConfig"] ?? {});
        emailContentTemplateController.text = selectedItem["emailContentTemplate"] ?? "";
        emailSubjectTemplateController.text = selectedItem["emailSubjectTemplate"] ?? "";
      }
    });
  }

  @override
  void dispose() {
    titleController.dispose();
    emailTemplateConfigController.dispose();
    emailContentTemplateController.dispose();
    emailSubjectTemplateController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: future,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Scaffold(
              body: Column(
            children: [
              headerWidget(context),
              const Divider(color: Color.fromRGBO(216, 218, 229, 1)),
              Expanded(
                child: Container(
                  margin: const EdgeInsets.only(top: 20, bottom: 10),
                  child: Form(
                    key: _formKey,
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                        child: bodyWidget(context),
                      ),
                    ),
                  ),
                ),
              ),
              btnWidget()
            ],
          ));
        } else if (snapshot.hasError) {
          return Text('${snapshot.error}');
        }
        return const Center(child: CircularProgressIndicator());
      },
    );
  }

  Padding btnWidget() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20, top: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ButtonSave(onPressed: () {
            if (_formKey.currentState!.validate()) {
              var result = {};
              if (selectedItem != null) {
                result = selectedItem;
                result["title"] = titleController.text;
                result["emailTemplateConfig"] = jsonDecode(emailTemplateConfigController.text.isEmpty ? "{}" : emailTemplateConfigController.text);
                result["emailContentTemplate"] = emailContentTemplateController.text;
                result["emailSubjectTemplate"] = emailSubjectTemplateController.text;
                result["status"] = _status;
                result["itemType"] = itemType;
                result["actionType"] = actionType;
                result["notiEmailAccountId"] = emailAccountId;
              } else {
                result = {
                  "title": titleController.text,
                  "emailTemplateConfig": jsonDecode(emailTemplateConfigController.text.isEmpty ? "{}" : emailTemplateConfigController.text),
                  "emailContentTemplate": emailContentTemplateController.text,
                  "emailSubjectTemplate": emailSubjectTemplateController.text,
                  "status": _status,
                  "itemType": itemType,
                  "actionType": actionType,
                  "notiEmailAccountId": emailAccountId,
                };
              }
              widget.callbackOptionTest!(result);
            }
          }),
          const ButtonCancel()
        ],
      ),
    );
  }

  Column bodyWidget(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (emailAccountList.isNotEmpty)
          Container(
            margin: const EdgeInsets.only(bottom: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 3,
                  child: MultiLanguageText(name: "email_account", defaultValue: "Email account", style: Style(context).styleTitleDialog),
                ),
                Expanded(
                  flex: 7,
                  child: DynamicDropdownButton(
                    value: emailAccountId,
                    hint: multiLanguageString(name: "choose_an_email_account", defaultValue: "Choose an email account", context: context),
                    items: emailAccountList.map<DropdownMenuItem<String>>((dynamic result) {
                      return DropdownMenuItem(
                        value: result["id"],
                        child: Text(result["emailName"]),
                      );
                    }).toList(),
                    onChanged: (newValue) async {
                      setState(() {
                        emailAccountId = newValue!;
                      });
                    },
                    borderRadius: 5,
                    isRequiredNotEmpty: true,
                  ),
                ),
              ],
            ),
          ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "type", defaultValue: "Type", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                  value: itemType,
                  hint: multiLanguageString(name: "choose_a_type", defaultValue: "Choose a type", context: context),
                  items: [
                    {"name": multiLanguageString(name: "test_case", defaultValue: "Test case", context: context), "value": "test_case"},
                    {"name": multiLanguageString(name: "test_run", defaultValue: "Test run", context: context), "value": "test_run"},
                    {
                      "name": multiLanguageString(name: "test_batch_run", defaultValue: "Test batch run", context: context),
                      "value": "test_batch_run"
                    },
                  ].map<DropdownMenuItem<String>>((dynamic result) {
                    return DropdownMenuItem(
                      value: result["value"],
                      child: Text(result["name"]),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      itemType = newValue!;
                    });
                  },
                  borderRadius: 5,
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "action", defaultValue: "Action", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                  value: actionType,
                  hint: multiLanguageString(name: "choose_an_action", defaultValue: "Choose an action", context: context),
                  items: [
                    {"name": multiLanguageString(name: "comment", defaultValue: "Comment", context: context), "value": "comment"},
                    {"name": multiLanguageString(name: "mentioned", defaultValue: "Mentioned", context: context), "value": "mentioned"},
                    {"name": multiLanguageString(name: "test_finished", defaultValue: "Test finished", context: context), "value": "test_finished"},
                  ].map<DropdownMenuItem<String>>((dynamic result) {
                    return DropdownMenuItem(
                      value: result["value"],
                      child: Text(result["name"]),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      actionType = newValue!;
                    });
                  },
                  borderRadius: 5,
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "title", defaultValue: "Title", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: titleController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "subject_template", defaultValue: "Subject Template", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: emailSubjectTemplateController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "content_template", defaultValue: "Content Template", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: emailContentTemplateController,
                  hintText: "...",
                  maxline: 10,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "config", defaultValue: "Config", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: emailTemplateConfigController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "status", defaultValue: "Status", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                  value: _status,
                  hint: multiLanguageString(name: "choose_a_status", defaultValue: "Choose a status", context: context),
                  items: [
                    {"name": multiLanguageString(name: "active", defaultValue: "Active", context: context), "value": 1},
                    {"name": multiLanguageString(name: "inactive", defaultValue: "Inactive", context: context), "value": 0},
                  ].map<DropdownMenuItem<int>>((dynamic result) {
                    return DropdownMenuItem(
                      value: result["value"],
                      child: Text(result["name"]),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      _status = newValue!;
                    });
                  },
                  borderRadius: 5,
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Padding headerWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 35, 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "type_email_template",
            defaultValue: "\$0 Email template",
            variables: [widget.title],
            isLowerCase: false,
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
            onPressed: () => Navigator.pop(context, 'cancel'),
            child: const Icon(
              Icons.close,
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }
}
