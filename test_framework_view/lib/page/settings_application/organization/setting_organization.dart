import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';

import '../../../common/custom_state.dart';

class SettingOrganizationWidget extends StatefulWidget {
  const SettingOrganizationWidget({Key? key}) : super(key: key);

  @override
  State<SettingOrganizationWidget> createState() => _SettingOrganizationWidgetState();
}

class _SettingOrganizationWidgetState extends CustomState<SettingOrganizationWidget> {
  late Future futureorganizationList;
  final TextEditingController nameController = TextEditingController();
  final TextEditingController desController = TextEditingController();
  final TextEditingController licenseController = TextEditingController();
  final TextEditingController authLdapUrlController = TextEditingController();
  final TextEditingController authLdapBaseController = TextEditingController();
  final TextEditingController authLdapPrincipalController = TextEditingController();
  final TextEditingController authLdapPasswordController = TextEditingController();
  final TextEditingController authLdapAttrsUsernameController = TextEditingController();
  final TextEditingController authLdapAttrsMailController = TextEditingController();
  final TextEditingController authLdapAttrsDisplayNameController = TextEditingController();
  final TextEditingController authLdapUserFilterController = TextEditingController();
  final TextEditingController authLdapAttrsUserPasswordController = TextEditingController();
  bool autoCreateProfile = false;
  bool defaultOrganization = false;
  int _status = 0;
  String authInternalType = "database";
  String authHashMethod = "SHA256";
  var organization = {};
  final _formKey = GlobalKey<FormState>();
  var organizationStatusMap = {1: "Active", 0: "Inactive"};
  @override
  void initState() {
    futureorganizationList = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    await getOrganization();
    await setInitValue(organization);
    return 0;
  }

  getOrganization() async {
    var response = await httpGet("/test-framework-api/user/sys/sys-organization", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        organization = response["body"]["result"];
      });
    }
    return 0;
  }

  setInitValue(selectedItem) {
    setState(() {
      _status = selectedItem["status"] ?? 0;
      nameController.text = selectedItem["name"];
      desController.text = selectedItem["description"];
      licenseController.text = selectedItem["license"];
      autoCreateProfile = selectedItem["autoCreateProfile"] ?? false;
      defaultOrganization = selectedItem["defaultOrganization"] ?? false;
      if (selectedItem["organizationConfig"] != null) {
        authInternalType = selectedItem["organizationConfig"]["authInternalType"] ?? "database";
        if (authInternalType == "ldap_authenticate" || authInternalType == "ldap_search") {
          authLdapUrlController.text = selectedItem["organizationConfig"]["authLdapUrl"];
          authLdapBaseController.text = selectedItem["organizationConfig"]["authLdapBase"];
          authLdapPrincipalController.text = selectedItem["organizationConfig"]["authLdapPrincipal"];
          authLdapPasswordController.text = selectedItem["organizationConfig"]["authLdapPassword"];
          authLdapAttrsUsernameController.text = selectedItem["organizationConfig"]["authLdapAttrsUsername"];
          authLdapAttrsMailController.text = selectedItem["organizationConfig"]["authLdapAttrsMail"];
          authLdapAttrsDisplayNameController.text = selectedItem["organizationConfig"]["authLdapAttrsDisplayName"];
          authLdapUserFilterController.text = selectedItem["organizationConfig"]["authLdapUserFilter"];
          authLdapAttrsUserPasswordController.text = selectedItem["organizationConfig"]["authLdapAttrsUserPassword"];
          authHashMethod = selectedItem["organizationConfig"]["authHashMethod"] ?? "SHA256";
        }
      }
    });
  }

  handleEditOrganization() async {
    var response = await httpPost("/test-framework-api/user/sys/sys-organization", organization, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
      }
    }
  }

  @override
  void dispose() {
    nameController.dispose();
    desController.dispose();
    licenseController.dispose();
    authLdapUrlController.dispose();
    authLdapBaseController.dispose();
    authLdapPrincipalController.dispose();
    authLdapPasswordController.dispose();
    authLdapAttrsUsernameController.dispose();
    authLdapAttrsMailController.dispose();
    authLdapAttrsDisplayNameController.dispose();
    authLdapUserFilterController.dispose();
    authLdapAttrsUserPasswordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, LanguageModel>(builder: (context, navigationModel, languageModel, child) {
      return FutureBuilder(
        future: futureorganizationList,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Scaffold(
              body: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    OrganizationNavMenuTab(
                      margin: const EdgeInsets.fromLTRB(50, 20, 50, 20),
                      navigationList: context.settingsMenuList,
                    ),
                    Expanded(
                      child: ListView(
                        padding: const EdgeInsets.fromLTRB(50, 0, 50, 20),
                        children: [
                          Text(
                            "Organization : ${organization["name"]}",
                            style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          bodyWidget(context)
                        ],
                      ),
                    ),
                    btnWidget(),
                  ],
                ),
              ),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        },
      );
    });
  }

  Container btnWidget() {
    return Container(
      margin: const EdgeInsets.only(bottom: 20, top: 20),
      alignment: Alignment.center,
      child: ButtonSave(onPressed: () {
        if (_formKey.currentState!.validate()) {
          organization["name"] = nameController.text;
          organization["description"] = desController.text;
          organization["license"] = licenseController.text;
          organization["autoCreateProfile"] = autoCreateProfile;
          organization["defaultOrganization"] = defaultOrganization;
          organization["organizationConfig"] ??= {};
          organization["organizationConfig"]["authInternalType"] = authInternalType;
          if (authInternalType == "ldap_authenticate" || authInternalType == "ldap_search") {
            organization["organizationConfig"]["authLdapUrl"] = authLdapUrlController.text;
            organization["organizationConfig"]["authLdapBase"] = authLdapBaseController.text;
            organization["organizationConfig"]["authLdapPrincipal"] = authLdapPrincipalController.text;
            organization["organizationConfig"]["authLdapPassword"] = authLdapPasswordController.text;
            organization["organizationConfig"]["authLdapAttrsUsername"] = authLdapAttrsUsernameController.text;
            organization["organizationConfig"]["authLdapAttrsMail"] = authLdapAttrsMailController.text;
            organization["organizationConfig"]["authLdapAttrsDisplayName"] = authLdapAttrsDisplayNameController.text;
            organization["organizationConfig"]["authLdapUserFilter"] = authLdapUserFilterController.text;
            organization["organizationConfig"]["authLdapAttrsUserPassword"] = authLdapAttrsUserPasswordController.text;
            organization["organizationConfig"]["authHashMethod"] = authHashMethod;
          }
          handleEditOrganization();
        }
      }),
    );
  }

  Column bodyWidget(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.only(bottom: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "title", defaultValue: "Title", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: nameController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "description", defaultValue: "Description", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: desController,
                  hintText: "...",
                  maxline: 3,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "license", defaultValue: "License", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: licenseController,
                  hintText: "...",
                  maxline: 3,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "create_profile", defaultValue: "Create profile", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                  value: autoCreateProfile,
                  items: [
                    {"name": multiLanguageString(name: "auto_create_profile", defaultValue: "Auto create profile", context: context), "value": true},
                    {
                      "name": multiLanguageString(name: "manual_create_profile", defaultValue: "Manual create profile", context: context),
                      "value": false
                    },
                  ].map<DropdownMenuItem<bool>>((dynamic result) {
                    return DropdownMenuItem(
                      value: result["value"],
                      child: Text(result["name"]),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      autoCreateProfile = newValue!;
                    });
                  },
                  borderRadius: 5,
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.only(bottom: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "default_organization", defaultValue: "Default organization", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                  value: defaultOrganization,
                  items: [
                    {
                      "name": multiLanguageString(name: "default_organization", defaultValue: "Default organization", context: context),
                      "value": true
                    },
                    {
                      "name": multiLanguageString(name: "not_default_organization", defaultValue: "Not default organization", context: context),
                      "value": false
                    },
                  ].map<DropdownMenuItem<bool>>((dynamic result) {
                    return DropdownMenuItem(
                      value: result["value"],
                      child: Text(result["name"]),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      defaultOrganization = newValue!;
                    });
                  },
                  borderRadius: 5,
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "status", defaultValue: "Status", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                  value: _status,
                  items: [
                    {"name": multiLanguageString(name: "active", defaultValue: "Active", context: context), "value": 1},
                    {"name": multiLanguageString(name: "inactive", defaultValue: "Inactive", context: context), "value": 0},
                  ].map<DropdownMenuItem<int>>((dynamic result) {
                    return DropdownMenuItem(
                      value: result["value"],
                      child: Text(result["name"]),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      _status = newValue!;
                    });
                  },
                  borderRadius: 5,
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "internal_type", defaultValue: "Internal type", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                  value: authInternalType,
                  items: [
                    {"name": multiLanguageString(name: "database", defaultValue: "Database", context: context), "value": "database"},
                    {"name": multiLanguageString(name: "ldap authenticate", defaultValue: "Ldap", context: context), "value": "ldap_authenticate"},
                    {"name": multiLanguageString(name: "ldap search", defaultValue: "Ldap", context: context), "value": "ldap_search"},
                  ].map<DropdownMenuItem<String>>((dynamic result) {
                    return DropdownMenuItem(
                      value: result["value"],
                      child: Text(result["name"]),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      authInternalType = newValue!;
                    });
                  },
                  borderRadius: 5,
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        if (authInternalType == "ldap_authenticate" || authInternalType == "ldap_search")
          Column(
            children: [
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "hash_method", defaultValue: "Hash method", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicDropdownButton(
                        value: authHashMethod,
                        items: [
                          {"name": multiLanguageString(name: "SHA256", defaultValue: "SHA256", context: context), "value": "SHA256"},
                          {"name": multiLanguageString(name: "plain_text", defaultValue: "Plain text", context: context), "value": "PLAIN_TEXT"},
                        ].map<DropdownMenuItem<String>>((dynamic result) {
                          return DropdownMenuItem(
                            value: result["value"],
                            child: Text(result["name"]),
                          );
                        }).toList(),
                        onChanged: (newValue) {
                          setState(() {
                            authHashMethod = newValue!;
                          });
                        },
                        borderRadius: 5,
                        isRequiredNotEmpty: true,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "url", defaultValue: "Url", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicTextField(
                        controller: authLdapUrlController,
                        hintText: "...",
                        maxline: 3,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "base", defaultValue: "Base", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicTextField(
                        controller: authLdapBaseController,
                        hintText: "...",
                        maxline: 3,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "principal", defaultValue: "Principal", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicTextField(
                        controller: authLdapPrincipalController,
                        hintText: "...",
                        maxline: 3,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "password", defaultValue: "Password", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicTextField(
                        controller: authLdapPasswordController,
                        hintText: "...",
                        maxline: 3,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "attrs_username", defaultValue: "Attrs username", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicTextField(
                        controller: authLdapAttrsUsernameController,
                        hintText: "...",
                        maxline: 3,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "attrs_mail", defaultValue: "Attrs mail", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicTextField(
                        controller: authLdapAttrsMailController,
                        hintText: "...",
                        maxline: 3,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child:
                          MultiLanguageText(name: "attrs_display_name", defaultValue: "Attrs display name", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicTextField(
                        controller: authLdapAttrsDisplayNameController,
                        hintText: "...",
                        maxline: 3,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "user_filter", defaultValue: "User filter", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicTextField(
                        controller: authLdapUserFilterController,
                        hintText: "...",
                        maxline: 3,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child:
                          MultiLanguageText(name: "attrs_user_password", defaultValue: "Attrs user password", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicTextField(
                        controller: authLdapAttrsUserPasswordController,
                        hintText: "...",
                        maxline: 3,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
      ],
    );
  }
}
