import 'package:flutter/material.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_checkbox_label.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_form.dart';
import 'package:test_framework_view/common/row_code_with_tooltip.dart';
import 'package:test_framework_view/common/tree.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';

import '../../../common/custom_state.dart';

class DialogOptionOrganizationRoleWidget extends StatefulWidget {
  final String type;
  final String title;
  final String? id;
  final Function? callbackOptionTest;
  const DialogOptionOrganizationRoleWidget({Key? key, required this.type, this.id, this.callbackOptionTest, required this.title}) : super(key: key);

  @override
  State<DialogOptionOrganizationRoleWidget> createState() => _DialogOptionOrganizationRoleWidgetState();
}

class _DialogOptionOrganizationRoleWidgetState extends CustomState<DialogOptionOrganizationRoleWidget> {
  late Future future;
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _desController = TextEditingController();
  final TextEditingController _codeController = TextEditingController();
  int _status = 0;
  String itemType = 'organization';
  final _formKey = GlobalKey<FormState>();
  TreeData? sysPermTreeData;
  var sysOrganizationRole = {};
  bool isSelectAll = false;
  dynamic selectedItem;
  @override
  void initState() {
    future = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    if (widget.id != null) {
      await getSelectedItem(widget.id!);
      await setInitValue();
    }
    await getListPermOrganizationRole();
    return 0;
  }

  getSelectedItem(String id) async {
    var response = await httpGet("/test-framework-api/user/sys/sys-item-role/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        selectedItem = response["body"]["result"];
      });
    }
  }

  getListPermOrganizationRole() async {
    var sysPermFindRequest = {"permType": itemType, "status": 1};
    var sysPermFindResponse = await httpPost("/test-framework-api/user/sys/sys-perm/search", sysPermFindRequest, context);
    if (sysPermFindResponse.containsKey("body") && sysPermFindResponse["body"] is String == false) {
      var sysPermList = sysPermFindResponse["body"]["resultList"];
      List<TreeNode> treeNodeList = [];
      for (var sysPerm in sysPermList) {
        treeNodeList.add(TreeNode(key: sysPerm["id"], parentKey: sysPerm["parentId"], item: sysPerm));
      }
      setState(() {
        sysPermTreeData = TreeData(treeNodeList);
      });
    }
    if (selectedItem != null && mounted) {
      setState(() {
        sysOrganizationRole = selectedItem;
      });
      if (sysPermTreeData!.getAllNode().map((e) => e.key).every((element) => sysOrganizationRole["sysPermIdList"].contains(element))) {
        setState(() {
          isSelectAll = true;
        });
      } else {
        setState(() {
          isSelectAll = false;
        });
      }
    } else {
      setState(() {
        sysOrganizationRole = {"status": 0};
      });
    }
  }

  setInitValue() {
    setState(() {
      if (selectedItem != null) {
        _status = selectedItem["status"] ?? 0;
        itemType = selectedItem["itemType"] ?? "organization";
        _nameController.text = selectedItem["name"] ?? "";
        _desController.text = selectedItem["description"] ?? "";
        _codeController.text = selectedItem["code"] ?? "";
      }
    });
  }

  @override
  void dispose() {
    _nameController.dispose();
    _desController.dispose();
    _codeController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: future,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Scaffold(
              body: Column(
            children: [
              headerWidget(context),
              const Divider(color: Color.fromRGBO(216, 218, 229, 1)),
              Expanded(
                child: Container(
                  margin: const EdgeInsets.only(top: 20, bottom: 10),
                  child: Form(
                    key: _formKey,
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                        child: bodyWidget(context),
                      ),
                    ),
                  ),
                ),
              ),
              btnWidget()
            ],
          ));
        } else if (snapshot.hasError) {
          return Text('${snapshot.error}');
        }
        return const Center(child: CircularProgressIndicator());
      },
    );
  }

  Padding btnWidget() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20, top: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ButtonSave(onPressed: () {
            if (_formKey.currentState!.validate()) {
              var result = {};
              if (selectedItem != null) {
                result = selectedItem;
                result["code"] = _codeController.text;
                result["name"] = _nameController.text;
                result["description"] = _desController.text;
                result["sysPermIdList"] = sysOrganizationRole["sysPermIdList"];
                result["status"] = _status;
                result["itemType"] = itemType;
              } else {
                result = {
                  "code": _codeController.text,
                  "name": _nameController.text,
                  "sysPermIdList": sysOrganizationRole["sysPermIdList"],
                  "description": _desController.text,
                  "status": _status,
                  "itemType": itemType,
                };
              }
              widget.callbackOptionTest!(result);
            }
          }),
          const ButtonCancel()
        ],
      ),
    );
  }

  Column bodyWidget(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "type", defaultValue: "Type", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                  value: itemType,
                  items: [
                    {"name": multiLanguageString(name: "item_organization", defaultValue: "Organization", context: context), "value": "organization"},
                    {"name": multiLanguageString(name: "item_project", defaultValue: "Project", context: context), "value": "project"},
                  ].map<DropdownMenuItem<String>>((dynamic result) {
                    return DropdownMenuItem(
                      value: result["value"],
                      child: Text(result["name"]),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      itemType = newValue!;
                    });
                    getListPermOrganizationRole();
                  },
                  borderRadius: 5,
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Expanded(
                flex: 3,
                child: RowCodeWithTooltipWidget(),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _codeController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                  enabled: widget.type != "edit",
                  isRequiredRegex: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "name", defaultValue: "Name", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _nameController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "description", defaultValue: "Description", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _desController,
                  hintText: "...",
                  maxline: 3,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "status", defaultValue: "Status", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                  value: _status,
                  hint: multiLanguageString(name: "choose_a_status", defaultValue: "Choose a status", context: context),
                  items: [
                    {"name": multiLanguageString(name: "active", defaultValue: "Active", context: context), "value": 1},
                    {"name": multiLanguageString(name: "inactive", defaultValue: "Inactive", context: context), "value": 0},
                  ].map<DropdownMenuItem<int>>((dynamic result) {
                    return DropdownMenuItem(
                      value: result["value"],
                      child: Text(result["name"]),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      _status = newValue!;
                    });
                  },
                  borderRadius: 5,
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MultiLanguageText(name: "permission", defaultValue: "Permission", style: Style(context).styleTitleDialog),
              LabeledCheckbox(
                  label: isSelectAll
                      ? multiLanguageString(name: "unselect_all", defaultValue: "Unselect all", context: context)
                      : multiLanguageString(name: "select_all", defaultValue: "Select all", context: context),
                  value: isSelectAll,
                  onChanged: (value) {
                    setState(() {
                      if (isSelectAll) {
                        sysOrganizationRole["sysPermIdList"] = [];
                      } else {
                        sysOrganizationRole["sysPermIdList"] = [];
                        for (var element in sysPermTreeData!.getAllNode()) {
                          sysOrganizationRole["sysPermIdList"].add(element.key);
                        }
                      }
                      isSelectAll = value;
                    });
                  }),
            ],
          ),
        ),
        if (sysPermTreeData != null)
          MyTreeMultipleSelect(
              selectedValue: sysOrganizationRole["sysPermIdList"] ?? [],
              treeData: sysPermTreeData!,
              getTreeNodeLabel: (treeNode) => "${treeNode.item["code"]} - ${treeNode.item["name"]}",
              onChanged: (value) {
                sysOrganizationRole["sysPermIdList"] = value;
              })
      ],
    );
  }

  Padding headerWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 35, 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "type_role",
            defaultValue: "\$0 ROLE",
            variables: [widget.title],
            isLowerCase: false,
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
            onPressed: () => Navigator.pop(context, 'cancel'),
            child: const Icon(
              Icons.close,
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }
}
