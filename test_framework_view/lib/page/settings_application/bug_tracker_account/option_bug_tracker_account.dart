import 'package:flutter/material.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';

import '../../../common/custom_state.dart';

class DialogOptionBugTrackerAccountWidget extends StatefulWidget {
  final String type;
  final String title;
  final String? id;
  final Function? callbackOptionTest;
  const DialogOptionBugTrackerAccountWidget({Key? key, required this.type, this.id, this.callbackOptionTest, required this.title}) : super(key: key);

  @override
  State<DialogOptionBugTrackerAccountWidget> createState() => _DialogOptionBugTrackerAccountWidgetState();
}

class _DialogOptionBugTrackerAccountWidgetState extends CustomState<DialogOptionBugTrackerAccountWidget> {
  late Future future;
  final TextEditingController usernameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController baseUrlController = TextEditingController();
  int _status = 0;
  String bugTrackerType = "None";
  final _formKey = GlobalKey<FormState>();
  dynamic selectedItem;
  @override
  void initState() {
    future = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    if (widget.id != null) {
      await getSelectedItem(widget.id!);
      await setInitValue();
    }
    return 0;
  }

  getSelectedItem(String id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-bug-tracker-account/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        selectedItem = response["body"]["result"];
      });
    }
  }

  setInitValue() {
    setState(() {
      if (selectedItem != null) {
        _status = selectedItem["status"] ?? 0;
        bugTrackerType = selectedItem["bugTrackerType"] ?? "None";
        usernameController.text = selectedItem["username"] ?? "";
        passwordController.text = selectedItem["password"] ?? "";
        baseUrlController.text = selectedItem["baseUrl"] ?? "";
      }
    });
  }

  @override
  void dispose() {
    usernameController.dispose();
    baseUrlController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: future,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Scaffold(
              body: Column(
            children: [
              headerWidget(context),
              const Divider(color: Color.fromRGBO(216, 218, 229, 1)),
              Expanded(
                child: Container(
                  margin: const EdgeInsets.only(top: 20, bottom: 10),
                  child: Form(
                    key: _formKey,
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                        child: bodyWidget(context),
                      ),
                    ),
                  ),
                ),
              ),
              btnWidget()
            ],
          ));
        } else if (snapshot.hasError) {
          return Text('${snapshot.error}');
        }
        return const Center(child: CircularProgressIndicator());
      },
    );
  }

  Padding btnWidget() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20, top: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ButtonSave(onPressed: () {
            if (_formKey.currentState!.validate()) {
              var result = {};
              if (selectedItem != null) {
                result = selectedItem;
                result["username"] = usernameController.text;
                result["baseUrl"] = baseUrlController.text;
                result["password"] = passwordController.text;
                result["status"] = _status;
                result["bugTrackerType"] = bugTrackerType;
              } else {
                result = {
                  "username": usernameController.text,
                  "baseUrl": baseUrlController.text,
                  "password": passwordController.text,
                  "status": _status,
                  "bugTrackerType": bugTrackerType,
                };
              }
              widget.callbackOptionTest!(result);
            }
          }),
          const ButtonCancel()
        ],
      ),
    );
  }

  Column bodyWidget(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.only(bottom: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "type", defaultValue: "Type", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                  value: bugTrackerType,
                  hint: multiLanguageString(name: "choose_a_type", defaultValue: "Choose a type", context: context),
                  items: [
                    {"value": "None", "label": "None"},
                    {"value": "LLQ", "label": "LLQ Work management"},
                    {"value": "JIRA", "label": "Jira"},
                    {"value": "MANTIS", "label": "Mantis"}
                  ].map<DropdownMenuItem<String>>((dynamic result) {
                    return DropdownMenuItem(
                      value: result["value"],
                      child: Text(result["label"]),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      bugTrackerType = newValue!;
                    });
                  },
                  borderRadius: 5,
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "url", defaultValue: "Url", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: baseUrlController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "username", defaultValue: "Username", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: usernameController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "password", defaultValue: "Password", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: passwordController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "status", defaultValue: "Status", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                  value: _status,
                  hint: multiLanguageString(name: "choose_a_status", defaultValue: "Choose a status", context: context),
                  items: [
                    {"name": multiLanguageString(name: "active", defaultValue: "Active", context: context), "value": 1},
                    {"name": multiLanguageString(name: "inactive", defaultValue: "Inactive", context: context), "value": 0},
                  ].map<DropdownMenuItem<int>>((dynamic result) {
                    return DropdownMenuItem(
                      value: result["value"],
                      child: Text(result["name"]),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      _status = newValue!;
                    });
                  },
                  borderRadius: 5,
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Padding headerWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 35, 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "type_bug_tracker_account",
            defaultValue: "\$0 Bug tracker account",
            variables: [widget.title],
            isLowerCase: false,
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
            onPressed: () => Navigator.pop(context, 'cancel'),
            child: const Icon(
              Icons.close,
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }
}
