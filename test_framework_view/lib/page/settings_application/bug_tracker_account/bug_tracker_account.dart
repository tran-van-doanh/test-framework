import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dialog/dialog_delete.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/settings_application/bug_tracker_account/option_bug_tracker_account.dart';
import 'package:test_framework_view/type.dart';

import '../../../common/custom_state.dart';

class SettingBugTrackerAccountWidget extends StatefulWidget {
  const SettingBugTrackerAccountWidget({Key? key}) : super(key: key);

  @override
  State<SettingBugTrackerAccountWidget> createState() => _SettingBugTrackerAccountWidgetState();
}

class _SettingBugTrackerAccountWidgetState extends CustomState<SettingBugTrackerAccountWidget> {
  late Future futurebugTrackerAccountList;
  var bugTrackerAccountSearchRequest = {};
  var bugTrackerAccountRowCount = 0;
  var bugTrackerAccountCurrentPage = 1;
  var bugTrackerAccountResultList = [];
  var bugTrackerAccountRowPerPage = 10;
  var bugTrackerAccountStatusMap = {1: "Active", 0: "Inactive"};
  TextEditingController usernameController = TextEditingController();
  String? bugTrackerType;
  String? status;

  @override
  void initState() {
    futurebugTrackerAccountList = bugTrackerAccountPageChange(bugTrackerAccountCurrentPage);
    super.initState();
  }

  bugTrackerAccountSearch() {
    bugTrackerAccountSearchRequest = {
      "usernameLike": usernameController.text,
      "bugTrackerType": bugTrackerType,
      "status": status,
    };
    futurebugTrackerAccountList = bugTrackerAccountPageChange(bugTrackerAccountCurrentPage);
  }

  bugTrackerAccountPageChange(page) async {
    if ((page - 1) * bugTrackerAccountRowPerPage > bugTrackerAccountRowCount) {
      page = (1.0 * bugTrackerAccountRowCount / bugTrackerAccountRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * bugTrackerAccountRowPerPage, "queryLimit": bugTrackerAccountRowPerPage};
    if (bugTrackerAccountSearchRequest["usernameLike"] != null && bugTrackerAccountSearchRequest["usernameLike"].isNotEmpty) {
      findRequest["usernameLike"] = "%${bugTrackerAccountSearchRequest["usernameLike"]}%";
    }
    if (bugTrackerAccountSearchRequest["bugTrackerType"] != null && bugTrackerAccountSearchRequest["bugTrackerType"].isNotEmpty) {
      findRequest["bugTrackerType"] = bugTrackerAccountSearchRequest["bugTrackerType"];
    }
    if (bugTrackerAccountSearchRequest["status"] != null && bugTrackerAccountSearchRequest["status"].isNotEmpty) {
      findRequest["status"] = bugTrackerAccountSearchRequest["status"];
    }
    var response = await httpPost("/test-framework-api/user/test-framework/tf-bug-tracker-account/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        bugTrackerAccountCurrentPage = page;
        bugTrackerAccountRowCount = response["body"]["rowCount"];
        bugTrackerAccountResultList = response["body"]["resultList"];
      });
    }
    return 0;
  }

  handleDeleteBugTrackerAccount(bugTrackerAccount) async {
    var response = await httpDelete("/test-framework-api/user/test-framework/tf-bug-tracker-account/${bugTrackerAccount["id"]}", context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        await bugTrackerAccountPageChange(bugTrackerAccountCurrentPage);
      }
    }
  }

  handleAddBugTrackerAccount(result) async {
    var response = await httpPut("/test-framework-api/user/test-framework/tf-bug-tracker-account", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await bugTrackerAccountPageChange(bugTrackerAccountCurrentPage);
      }
    }
  }

  handleEditBugTrackerAccount(result, bugTrackerAccount) async {
    var response = await httpPost("/test-framework-api/user/test-framework/tf-bug-tracker-account/${bugTrackerAccount["id"]}", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await bugTrackerAccountPageChange(bugTrackerAccountCurrentPage);
      }
    }
  }

  @override
  void dispose() {
    usernameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, LanguageModel>(builder: (context, navigationModel, languageModel, child) {
      return FutureBuilder(
        future: futurebugTrackerAccountList,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Scaffold(
              body: Column(
                children: [
                  Expanded(
                    child: ListView(
                      children: [
                        OrganizationNavMenuTab(
                          margin: const EdgeInsets.fromLTRB(45, 20, 30, 0),
                          navigationList: context.settingsMenuList,
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              MultiLanguageText(
                                name: "bug_tracker_account_count",
                                defaultValue: "Bug tracker account (\$0)",
                                variables: ["$bugTrackerAccountRowCount"],
                                style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
                              ),
                              searchForm(),
                              tableWidget(),
                              DynamicTablePagging(
                                bugTrackerAccountRowCount,
                                bugTrackerAccountCurrentPage,
                                bugTrackerAccountRowPerPage,
                                pageChangeHandler: (page) {
                                  setState(() {
                                    futurebugTrackerAccountList = bugTrackerAccountPageChange(page);
                                  });
                                },
                                rowPerPageChangeHandler: (rowPerPage) {
                                  setState(() {
                                    bugTrackerAccountRowPerPage = rowPerPage!;
                                    futurebugTrackerAccountList = bugTrackerAccountPageChange(1);
                                  });
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  btnWidget(context),
                ],
              ),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        },
      );
    });
  }

  Widget searchForm() {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                child: SizedBox(
                  height: 50,
                  child: DynamicTextField(
                    controller: usernameController,
                    onComplete: () => bugTrackerAccountSearch(),
                    onChanged: (value) {
                      setState(() {
                        usernameController.text;
                      });
                    },
                    labelText: multiLanguageString(name: "search_user_name", defaultValue: "Search user name", context: context),
                    hintText: multiLanguageString(name: "by_user_name", defaultValue: "By user name", context: context),
                    suffixIcon: (usernameController.text != "")
                        ? IconButton(
                            onPressed: () {
                              setState(() {
                                usernameController.clear();
                              });
                              bugTrackerAccountSearch();
                            },
                            icon: const Icon(Icons.clear),
                            splashRadius: 20,
                          )
                        : null,
                  ),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                child: SizedBox(
                  height: 50,
                  child: DynamicDropdownButton(
                    value: bugTrackerType,
                    hint: multiLanguageString(name: "choose_a_type", defaultValue: "Choose a type", context: context),
                    items: [
                      {"value": "LLQ", "label": "LLQ Work management"},
                      {"value": "JIRA", "label": "Jira"},
                      {"value": "MANTIS", "label": "Mantis"}
                    ].map<DropdownMenuItem<String>>((dynamic result) {
                      return DropdownMenuItem(
                        value: result["value"],
                        child: Text(result["label"]),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        bugTrackerType = newValue!;
                      });
                    },
                    borderRadius: 5,
                    isRequiredNotEmpty: true,
                  ),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                child: SizedBox(
                  height: 50,
                  child: DynamicDropdownButton(
                    value: status,
                    hint: multiLanguageString(name: "choose_a_status", defaultValue: "Choose a status", context: context),
                    items: [
                      {"name": multiLanguageString(name: "active", defaultValue: "Active", context: context), "value": 1},
                      {"name": multiLanguageString(name: "inactive", defaultValue: "Inactive", context: context), "value": 0},
                    ].map<DropdownMenuItem<int>>((dynamic result) {
                      return DropdownMenuItem(
                        value: result["value"],
                        child: Text(result["name"]),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        status = newValue!;
                      });
                    },
                    borderRadius: 5,
                    isRequiredNotEmpty: true,
                  ),
                ),
              ),
            ),
          ],
        ),
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 20),
              child: ElevatedButton(
                  onPressed: () {
                    bugTrackerAccountSearch();
                  },
                  child: const MultiLanguageText(name: "search", defaultValue: "Search", isLowerCase: false)),
            ),
            ElevatedButton(
                onPressed: () {
                  setState(() {
                    usernameController.clear();
                    bugTrackerType = null;
                    status = null;
                    bugTrackerAccountSearch();
                  });
                },
                child: const MultiLanguageText(name: "reset", defaultValue: "Reset", isLowerCase: false))
          ],
        ),
      ],
    );
  }

  Padding btnWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 40, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            margin: const EdgeInsets.only(right: 10),
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              icon: const IconTheme(
                data: IconThemeData(color: Color.fromRGBO(49, 49, 49, 1)),
                child: Icon(Icons.add),
              ),
              label: MultiLanguageText(
                name: "new_bug_tracker_account",
                defaultValue: "New Bug Tracker Account",
                isLowerCase: false,
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
              onPressed: () {
                Navigator.of(context).push(
                  createRoute(
                    DialogOptionBugTrackerAccountWidget(
                        type: "create",
                        title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                        callbackOptionTest: (result) {
                          handleAddBugTrackerAccount(result);
                        }),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Container tableWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 30),
      child: CustomDataTableWidget(
        columns: [
          Expanded(
            child: MultiLanguageText(
              name: "type",
              defaultValue: "Type",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            child: MultiLanguageText(
              name: "url",
              defaultValue: "Url",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            child: MultiLanguageText(
              name: "username",
              defaultValue: "Username",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            child: MultiLanguageText(
              name: "status",
              defaultValue: "Status",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          const SizedBox(
            width: 38,
          ),
        ],
        rows: [
          for (var bugTrackerAccount in bugTrackerAccountResultList)
            CustomDataRow(
              cells: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        const Icon(Icons.description, color: Color.fromRGBO(61, 148, 254, 1)),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Text(
                            bugTrackerAccount["bugTrackerType"] ?? "",
                            overflow: TextOverflow.ellipsis,
                            style: Style(context).styleTextDataCell,
                            maxLines: 3,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Text(
                    bugTrackerAccount["baseUrl"] ?? "",
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                Expanded(
                  child: Text(
                    bugTrackerAccount["username"] ?? "",
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                Expanded(
                  child: Text(
                    bugTrackerAccountStatusMap[bugTrackerAccount["status"]] ?? "Not defined",
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                PopupMenuButton(
                  offset: const Offset(5, 50),
                  tooltip: '',
                  splashRadius: 10,
                  icon: const Icon(
                    Icons.more_vert,
                  ),
                  itemBuilder: (context) => [
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.of(context).push(
                            createRoute(
                              DialogOptionBugTrackerAccountWidget(
                                  type: "edit",
                                  title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                  id: bugTrackerAccount["id"],
                                  callbackOptionTest: (result) {
                                    handleEditBugTrackerAccount(result, bugTrackerAccount);
                                  }),
                            ),
                          );
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                        leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          dialogDeleteBugTrackerAccount(bugTrackerAccount);
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "delete", defaultValue: "Delete"),
                        leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                  ],
                ),
              ],
            ),
        ],
      ),
    );
  }

  dialogDeleteBugTrackerAccount(bugTrackerAccount) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogDelete(
          selectedItem: bugTrackerAccount,
          callback: () {
            handleDeleteBugTrackerAccount(bugTrackerAccount);
          },
          type: 'bug_tracker_account',
        );
      },
    );
  }
}
