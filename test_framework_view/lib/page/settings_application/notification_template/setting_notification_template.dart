import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dialog/dialog_delete.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/settings_application/notification_template/option_notification_template.dart';
import 'package:test_framework_view/type.dart';

import '../../../common/custom_state.dart';

class SettingNotificationTemplateWidget extends StatefulWidget {
  const SettingNotificationTemplateWidget({Key? key}) : super(key: key);

  @override
  State<SettingNotificationTemplateWidget> createState() => _SettingNotificationTemplateWidgetState();
}

class _SettingNotificationTemplateWidgetState extends CustomState<SettingNotificationTemplateWidget> {
  late Future futurenotificationTemplateList;
  var notificationTemplateSearchRequest = {};
  var notificationTemplateRowCount = 0;
  var notificationTemplateCurrentPage = 1;
  var notificationTemplateResultList = [];
  var notificationTemplateRowPerPage = 10;
  var notificationTemplateStatusMap = {1: "Active", 0: "Inactive"};
  TextEditingController titleController = TextEditingController();
  String? itemType;
  String? actionType;

  @override
  void initState() {
    futurenotificationTemplateList = notificationTemplatePageChange(notificationTemplateCurrentPage);
    super.initState();
  }

  notificationTemplateSearch() {
    notificationTemplateSearchRequest = {
      "titleLike": titleController.text,
      "itemType": itemType,
      "actionType": actionType,
    };
    futurenotificationTemplateList = notificationTemplatePageChange(notificationTemplateCurrentPage);
  }

  notificationTemplatePageChange(page) async {
    if ((page - 1) * notificationTemplateRowPerPage > notificationTemplateRowCount) {
      page = (1.0 * notificationTemplateRowCount / notificationTemplateRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * notificationTemplateRowPerPage, "queryLimit": notificationTemplateRowPerPage};
    if (notificationTemplateSearchRequest["titleLike"] != null && notificationTemplateSearchRequest["titleLike"].isNotEmpty) {
      findRequest["titleLike"] = "%${notificationTemplateSearchRequest["titleLike"]}%";
    }
    if (notificationTemplateSearchRequest["itemType"] != null && notificationTemplateSearchRequest["itemType"].isNotEmpty) {
      findRequest["itemType"] = notificationTemplateSearchRequest["itemType"];
    }
    if (notificationTemplateSearchRequest["actionType"] != null && notificationTemplateSearchRequest["actionType"].isNotEmpty) {
      findRequest["actionType"] = notificationTemplateSearchRequest["actionType"];
    }
    var response = await httpPost("/test-framework-api/user/noti/noti-notification-template/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        notificationTemplateCurrentPage = page;
        notificationTemplateRowCount = response["body"]["rowCount"];
        notificationTemplateResultList = response["body"]["resultList"];
      });
    }
    return 0;
  }

  handleDeleteNotificationTemplate(notificationTemplate) async {
    var response = await httpDelete("/test-framework-api/user/noti/noti-notification-template/${notificationTemplate["id"]}", context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        await notificationTemplatePageChange(notificationTemplateCurrentPage);
      }
    }
  }

  handleAddNotificationTemplate(result) async {
    var response = await httpPut("/test-framework-api/user/noti/noti-notification-template", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await notificationTemplatePageChange(notificationTemplateCurrentPage);
      }
    }
  }

  handleEditNotificationTemplate(result, notificationTemplate) async {
    var response = await httpPost("/test-framework-api/user/noti/noti-notification-template/${notificationTemplate["id"]}", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await notificationTemplatePageChange(notificationTemplateCurrentPage);
      }
    }
  }

  @override
  void dispose() {
    titleController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, LanguageModel>(builder: (context, navigationModel, languageModel, child) {
      return FutureBuilder(
        future: futurenotificationTemplateList,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Scaffold(
              body: Column(
                children: [
                  Expanded(
                    child: ListView(
                      children: [
                        OrganizationNavMenuTab(
                          margin: const EdgeInsets.fromLTRB(45, 20, 30, 0),
                          navigationList: context.settingsMenuList,
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              MultiLanguageText(
                                name: "notification_template_count",
                                defaultValue: "Notification Template (\$0)",
                                variables: ["$notificationTemplateRowCount"],
                                style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
                              ),
                              searchForm(),
                              tableWidget(),
                              DynamicTablePagging(
                                notificationTemplateRowCount,
                                notificationTemplateCurrentPage,
                                notificationTemplateRowPerPage,
                                pageChangeHandler: (page) {
                                  setState(() {
                                    futurenotificationTemplateList = notificationTemplatePageChange(page);
                                  });
                                },
                                rowPerPageChangeHandler: (rowPerPage) {
                                  setState(() {
                                    notificationTemplateRowPerPage = rowPerPage!;
                                    futurenotificationTemplateList = notificationTemplatePageChange(1);
                                  });
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  btnWidget(context),
                ],
              ),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        },
      );
    });
  }

  Widget searchForm() {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                child: SizedBox(
                  height: 50,
                  child: DynamicDropdownButton(
                    value: itemType,
                    hint: multiLanguageString(name: "choose_a_type", defaultValue: "Choose a type", context: context),
                    items: [
                      {"name": multiLanguageString(name: "test_case", defaultValue: "Test case", context: context), "value": "test_case"},
                      {"name": multiLanguageString(name: "test_run", defaultValue: "Test run", context: context), "value": "test_run"},
                      {
                        "name": multiLanguageString(name: "test_batch_run", defaultValue: "Test batch run", context: context),
                        "value": "test_batch_run"
                      },
                    ].map<DropdownMenuItem<String>>((dynamic result) {
                      return DropdownMenuItem(
                        value: result["value"],
                        child: Text(result["name"]),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        itemType = newValue!;
                      });
                    },
                    borderRadius: 5,
                    isRequiredNotEmpty: true,
                  ),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                child: SizedBox(
                  height: 50,
                  child: DynamicDropdownButton(
                    value: actionType,
                    hint: multiLanguageString(name: "choose_an_action", defaultValue: "Choose an action", context: context),
                    items: [
                      {"name": multiLanguageString(name: "comment", defaultValue: "Comment", context: context), "value": "comment"},
                      {"name": multiLanguageString(name: "mentioned", defaultValue: "Mentioned", context: context), "value": "mentioned"},
                      {"name": multiLanguageString(name: "test_finished", defaultValue: "Test finished", context: context), "value": "test_finished"},
                    ].map<DropdownMenuItem<String>>((dynamic result) {
                      return DropdownMenuItem(
                        value: result["value"],
                        child: Text(result["name"]),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        actionType = newValue!;
                      });
                    },
                    borderRadius: 5,
                    isRequiredNotEmpty: true,
                  ),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                child: SizedBox(
                  height: 50,
                  child: DynamicTextField(
                    controller: titleController,
                    onComplete: () => notificationTemplateSearch(),
                    onChanged: (value) {
                      setState(() {
                        titleController.text;
                      });
                    },
                    labelText: multiLanguageString(name: "search_title", defaultValue: "Search title", context: context),
                    hintText: multiLanguageString(name: "by_title", defaultValue: "By title", context: context),
                    suffixIcon: (titleController.text != "")
                        ? IconButton(
                            onPressed: () {
                              setState(() {
                                titleController.clear();
                              });
                              notificationTemplateSearch();
                            },
                            icon: const Icon(Icons.clear),
                            splashRadius: 20,
                          )
                        : null,
                  ),
                ),
              ),
            ),
          ],
        ),
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 20),
              child: ElevatedButton(
                  onPressed: () {
                    notificationTemplateSearch();
                  },
                  child: const MultiLanguageText(name: "search", defaultValue: "Search", isLowerCase: false)),
            ),
            ElevatedButton(
                onPressed: () {
                  setState(() {
                    titleController.clear();
                    itemType = null;
                    actionType = null;
                    notificationTemplateSearch();
                  });
                },
                child: const MultiLanguageText(name: "reset", defaultValue: "Reset", isLowerCase: false))
          ],
        ),
      ],
    );
  }

  Padding btnWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 40, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            margin: const EdgeInsets.only(right: 10),
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              icon: const IconTheme(
                data: IconThemeData(color: Color.fromRGBO(49, 49, 49, 1)),
                child: Icon(Icons.add),
              ),
              label: MultiLanguageText(
                name: "new_notification_template",
                defaultValue: "New Notification Template",
                isLowerCase: false,
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
              onPressed: () {
                Navigator.of(context).push(
                  createRoute(
                    DialogOptionNotificationTemplateWidget(
                        type: "create",
                        title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                        callbackOptionTest: (result) {
                          handleAddNotificationTemplate(result);
                        }),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Container tableWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 30),
      child: CustomDataTableWidget(
        columns: [
          Expanded(
            flex: 2,
            child: MultiLanguageText(
              name: "title",
              defaultValue: "title",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            child: MultiLanguageText(
              name: "type",
              defaultValue: "Type",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            child: MultiLanguageText(
              name: "action",
              defaultValue: "Action",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            child: MultiLanguageText(
              name: "status",
              defaultValue: "Status",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          const SizedBox(
            width: 38,
          ),
        ],
        rows: [
          for (var notificationTemplate in notificationTemplateResultList)
            CustomDataRow(
              cells: [
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        const Icon(Icons.description, color: Color.fromRGBO(61, 148, 254, 1)),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Text(
                            notificationTemplate["title"] ?? "",
                            overflow: TextOverflow.ellipsis,
                            style: Style(context).styleTextDataCell,
                            maxLines: 3,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Text(
                    notificationTemplate["itemType"] ?? "",
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                Expanded(
                  child: Text(
                    notificationTemplate["actionType"] ?? "",
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                Expanded(
                  child: Text(
                    notificationTemplateStatusMap[notificationTemplate["status"]] ?? "Not defined",
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                PopupMenuButton(
                  offset: const Offset(5, 50),
                  tooltip: '',
                  splashRadius: 10,
                  icon: const Icon(
                    Icons.more_vert,
                  ),
                  itemBuilder: (context) => [
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.of(context).push(
                            createRoute(
                              DialogOptionNotificationTemplateWidget(
                                  type: "edit",
                                  title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                  id: notificationTemplate["id"],
                                  callbackOptionTest: (result) {
                                    handleEditNotificationTemplate(result, notificationTemplate);
                                  }),
                            ),
                          );
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                        leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          dialogDeleteNotificationTemplate(notificationTemplate);
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "delete", defaultValue: "Delete"),
                        leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                  ],
                ),
              ],
            ),
        ],
      ),
    );
  }

  dialogDeleteNotificationTemplate(notificationTemplate) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogDelete(
          selectedItem: notificationTemplate,
          callback: () {
            handleDeleteNotificationTemplate(notificationTemplate);
          },
          type: 'notification_template',
        );
      },
    );
  }
}
