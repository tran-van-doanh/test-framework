import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dialog/dialog_delete.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/settings_application/host-name/option_host_name.dart';
import 'package:test_framework_view/type.dart';

import '../../../common/custom_state.dart';

class SettingHostNameWidget extends StatefulWidget {
  const SettingHostNameWidget({Key? key}) : super(key: key);

  @override
  State<SettingHostNameWidget> createState() => _SettingHostNameWidgetState();
}

class _SettingHostNameWidgetState extends CustomState<SettingHostNameWidget> {
  late Future futurehostNameList;
  var hostNameSearchRequest = {};
  var hostNameRowCount = 0;
  var hostNameCurrentPage = 1;
  var hostNameResultList = [];
  var hostNameRowPerPage = 10;
  var hostNameStatusMap = {1: "Active", 0: "Inactive"};
  TextEditingController hostnameController = TextEditingController();
  String? itemType;
  String? actionType;

  @override
  void initState() {
    futurehostNameList = hostNamePageChange(hostNameCurrentPage);
    super.initState();
  }

  hostNameSearch() {
    hostNameSearchRequest = {
      "hostnameLike": hostnameController.text,
    };
    futurehostNameList = hostNamePageChange(hostNameCurrentPage);
  }

  hostNamePageChange(page) async {
    if ((page - 1) * hostNameRowPerPage > hostNameRowCount) {
      page = (1.0 * hostNameRowCount / hostNameRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * hostNameRowPerPage, "queryLimit": hostNameRowPerPage};
    if (hostNameSearchRequest["hostnameLike"] != null && hostNameSearchRequest["hostnameLike"].isNotEmpty) {
      findRequest["hostnameLike"] = "%${hostNameSearchRequest["hostnameLike"]}%";
    }
    var response = await httpPost("/test-framework-api/user/sys/sys-organization-hostname/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        hostNameCurrentPage = page;
        hostNameRowCount = response["body"]["rowCount"];
        hostNameResultList = response["body"]["resultList"];
      });
    }
    return 0;
  }

  handleDeleteHostName(hostName) async {
    var response = await httpDelete("/test-framework-api/user/sys/sys-organization-hostname/${hostName["id"]}", context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        await hostNamePageChange(hostNameCurrentPage);
      }
    }
  }

  handleAddHostName(result) async {
    var response = await httpPut("/test-framework-api/user/sys/sys-organization-hostname", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await hostNamePageChange(hostNameCurrentPage);
      }
    }
  }

  handleEditHostName(result, hostName) async {
    var response = await httpPost("/test-framework-api/user/sys/sys-organization-hostname/${hostName["id"]}", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await hostNamePageChange(hostNameCurrentPage);
      }
    }
  }

  @override
  void dispose() {
    hostnameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, LanguageModel>(builder: (context, navigationModel, languageModel, child) {
      return FutureBuilder(
        future: futurehostNameList,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Scaffold(
              body: Column(
                children: [
                  Expanded(
                    child: ListView(
                      children: [
                        OrganizationNavMenuTab(
                          margin: const EdgeInsets.fromLTRB(45, 20, 30, 0),
                          navigationList: context.settingsMenuList,
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              MultiLanguageText(
                                name: "host_name_count",
                                defaultValue: "Host name (\$0)",
                                variables: ["$hostNameRowCount"],
                                style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
                              ),
                              searchForm(),
                              tableWidget(),
                              DynamicTablePagging(
                                hostNameRowCount,
                                hostNameCurrentPage,
                                hostNameRowPerPage,
                                pageChangeHandler: (page) {
                                  setState(() {
                                    futurehostNameList = hostNamePageChange(page);
                                  });
                                },
                                rowPerPageChangeHandler: (rowPerPage) {
                                  setState(() {
                                    hostNameRowPerPage = rowPerPage!;
                                    futurehostNameList = hostNamePageChange(1);
                                  });
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  btnWidget(context),
                ],
              ),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        },
      );
    });
  }

  Widget searchForm() {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                child: SizedBox(
                  height: 50,
                  child: DynamicTextField(
                    controller: hostnameController,
                    onComplete: () => hostNameSearch(),
                    onChanged: (value) {
                      setState(() {
                        hostnameController.text;
                      });
                    },
                    labelText: multiLanguageString(name: "search_host_name", defaultValue: "Search host name", context: context),
                    hintText: multiLanguageString(name: "by_host_name", defaultValue: "By host name", context: context),
                    suffixIcon: (hostnameController.text != "")
                        ? IconButton(
                            onPressed: () {
                              setState(() {
                                hostnameController.clear();
                              });
                              hostNameSearch();
                            },
                            icon: const Icon(Icons.clear),
                            splashRadius: 20,
                          )
                        : null,
                  ),
                ),
              ),
            ),
          ],
        ),
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 20),
              child: ElevatedButton(
                  onPressed: () {
                    hostNameSearch();
                  },
                  child: const MultiLanguageText(name: "search", defaultValue: "Search", isLowerCase: false)),
            ),
            ElevatedButton(
                onPressed: () {
                  setState(() {
                    hostnameController.clear();
                    itemType = null;
                    actionType = null;
                    hostNameSearch();
                  });
                },
                child: const MultiLanguageText(name: "reset", defaultValue: "Reset", isLowerCase: false))
          ],
        ),
      ],
    );
  }

  Padding btnWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 40, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            margin: const EdgeInsets.only(right: 10),
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              icon: const IconTheme(
                data: IconThemeData(color: Color.fromRGBO(49, 49, 49, 1)),
                child: Icon(Icons.add),
              ),
              label: MultiLanguageText(
                name: "new_host_name",
                defaultValue: "New Host Name",
                isLowerCase: false,
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
              onPressed: () {
                Navigator.of(context).push(
                  createRoute(
                    DialogOptionHostNameWidget(
                        type: "create",
                        title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                        callbackOptionTest: (result) {
                          handleAddHostName(result);
                        }),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Container tableWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 30),
      child: CustomDataTableWidget(
        columns: [
          Expanded(
            flex: 2,
            child: MultiLanguageText(
              name: "hostname",
              defaultValue: "Hostname",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            child: MultiLanguageText(
              name: "Default",
              defaultValue: "Default",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            child: MultiLanguageText(
              name: "status",
              defaultValue: "Status",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          const SizedBox(
            width: 38,
          ),
        ],
        rows: [
          for (var hostName in hostNameResultList)
            CustomDataRow(
              cells: [
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        const Icon(Icons.description, color: Color.fromRGBO(61, 148, 254, 1)),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Text(
                            hostName["hostname"] ?? "",
                            overflow: TextOverflow.ellipsis,
                            style: Style(context).styleTextDataCell,
                            maxLines: 3,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Text(
                    hostName["defaultHostname"] ? "Default" : "Not default",
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                Expanded(
                  child: Text(
                    hostNameStatusMap[hostName["status"]] ?? "Not defined",
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                PopupMenuButton(
                  offset: const Offset(5, 50),
                  tooltip: '',
                  splashRadius: 10,
                  icon: const Icon(
                    Icons.more_vert,
                  ),
                  itemBuilder: (context) => [
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.of(context).push(
                            createRoute(
                              DialogOptionHostNameWidget(
                                  type: "edit",
                                  title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                  id: hostName["id"],
                                  callbackOptionTest: (result) {
                                    handleEditHostName(result, hostName);
                                  }),
                            ),
                          );
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                        leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          dialogDeleteHostName(hostName);
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "delete", defaultValue: "Delete"),
                        leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                  ],
                ),
              ],
            ),
        ],
      ),
    );
  }

  dialogDeleteHostName(hostName) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogDelete(
          selectedItem: hostName,
          callback: () {
            handleDeleteHostName(hostName);
          },
          type: 'host_name',
        );
      },
    );
  }
}
