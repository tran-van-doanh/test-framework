import 'package:flutter/material.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';

import '../../../common/custom_state.dart';

class DialogOptionEmailAccountWidget extends StatefulWidget {
  final String type;
  final String title;
  final String? id;
  final Function? callbackOptionTest;
  const DialogOptionEmailAccountWidget({Key? key, required this.type, this.id, this.callbackOptionTest, required this.title}) : super(key: key);

  @override
  State<DialogOptionEmailAccountWidget> createState() => _DialogOptionEmailAccountWidgetState();
}

class _DialogOptionEmailAccountWidgetState extends CustomState<DialogOptionEmailAccountWidget> {
  late Future future;
  final TextEditingController emailNameController = TextEditingController();
  final TextEditingController emailAddressController = TextEditingController();
  final TextEditingController emailReplyNameController = TextEditingController();
  final TextEditingController emailReplyAddressController = TextEditingController();
  final TextEditingController azureClientIdController = TextEditingController();
  final TextEditingController azureClientSecretController = TextEditingController();
  final TextEditingController azureAuthorityController = TextEditingController();
  final TextEditingController azureRedirectUrlController = TextEditingController();
  final TextEditingController serverHostController = TextEditingController();
  final TextEditingController serverPortController = TextEditingController();
  final TextEditingController usernameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController emailPropertyController = TextEditingController();
  int _status = 0;
  String emailType = 'microsoft365';
  final _formKey = GlobalKey<FormState>();
  dynamic selectedItem;
  @override
  void initState() {
    future = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    if (widget.id != null) {
      await getSelectedItem(widget.id!);
      await setInitValue();
    }
    return 0;
  }

  getSelectedItem(String id) async {
    var response = await httpGet("/test-framework-api/user/noti/noti-email-account/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        selectedItem = response["body"]["result"];
      });
    }
  }

  setInitValue() {
    setState(() {
      if (selectedItem != null) {
        _status = selectedItem["status"] ?? 0;
        emailType = selectedItem["emailType"] ?? "microsoft365";
        emailNameController.text = selectedItem["emailName"];
        emailReplyAddressController.text = selectedItem["emailReplyAddress"];
        emailReplyNameController.text = selectedItem["emailReplyName"];
        emailAddressController.text = selectedItem["emailAddress"];
        if (selectedItem["emailConfig"] != null) {
          if (emailType == "microsoft365") {
            azureClientIdController.text = selectedItem["emailConfig"]["azureClientId"];
            azureClientSecretController.text = selectedItem["emailConfig"]["azureClientSecret"];
            azureAuthorityController.text = selectedItem["emailConfig"]["azureAuthority"];
            azureRedirectUrlController.text = selectedItem["emailConfig"]["azureRedirectUrl"];
          } else {
            serverHostController.text = selectedItem["emailConfig"]["serverHost"];
            serverPortController.text = selectedItem["emailConfig"]["serverPort"];
            usernameController.text = selectedItem["emailConfig"]["username"];
            passwordController.text = selectedItem["emailConfig"]["password"];
            emailPropertyController.text = selectedItem["emailConfig"]["emailProperty"];
          }
        }
      }
    });
  }

  @override
  void dispose() {
    emailNameController.dispose();
    emailReplyAddressController.dispose();
    emailReplyNameController.dispose();
    emailAddressController.dispose();
    azureClientIdController.dispose();
    azureClientSecretController.dispose();
    azureAuthorityController.dispose();
    azureRedirectUrlController.dispose();
    serverHostController.dispose();
    serverPortController.dispose();
    usernameController.dispose();
    passwordController.dispose();
    emailPropertyController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: future,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Scaffold(
              body: Column(
            children: [
              headerWidget(context),
              const Divider(color: Color.fromRGBO(216, 218, 229, 1)),
              Expanded(
                child: Container(
                  margin: const EdgeInsets.only(top: 20, bottom: 10),
                  child: Form(
                    key: _formKey,
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                        child: bodyWidget(context),
                      ),
                    ),
                  ),
                ),
              ),
              btnWidget()
            ],
          ));
        } else if (snapshot.hasError) {
          return Text('${snapshot.error}');
        }
        return const Center(child: CircularProgressIndicator());
      },
    );
  }

  Padding btnWidget() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20, top: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ButtonSave(onPressed: () {
            if (_formKey.currentState!.validate()) {
              var result = {};
              if (selectedItem != null) {
                result = selectedItem;
                result["emailType"] = emailType;
                result["emailName"] = emailNameController.text;
                result["emailAddress"] = emailAddressController.text;
                result["emailReplyName"] = emailReplyNameController.text;
                result["emailReplyAddress"] = emailReplyAddressController.text;
                result["status"] = _status;
                result["emailConfig"] ??= {};
                if (emailType == "microsoft365") {
                  result["emailConfig"]["azureClientId"] = azureClientIdController.text;
                  result["emailConfig"]["azureClientSecret"] = azureClientSecretController.text;
                  result["emailConfig"]["azureAuthority"] = azureAuthorityController.text;
                  result["emailConfig"]["azureRedirectUrl"] = azureRedirectUrlController.text;
                } else {
                  result["emailConfig"]["serverHost"] = serverHostController.text;
                  result["emailConfig"]["serverPort"] = serverPortController.text;
                  result["emailConfig"]["username"] = usernameController.text;
                  result["emailConfig"]["password"] = passwordController.text;
                  result["emailConfig"]["emailProperty"] = emailPropertyController.text;
                }
              } else {
                result = {
                  "emailType": emailType,
                  "emailName": emailNameController.text,
                  "emailAddress": emailAddressController.text,
                  "emailReplyName": emailReplyNameController.text,
                  "emailReplyAddress": emailReplyAddressController.text,
                  "status": _status,
                  if (emailType == "microsoft365")
                    "emailConfig": {
                      "azureClientId": azureClientIdController.text,
                      "azureClientSecret": azureClientSecretController.text,
                      "azureAuthority": azureAuthorityController.text,
                      "azureRedirectUrl": azureRedirectUrlController.text,
                    },
                  if (emailType == "smtp")
                    "emailConfig": {
                      "serverHost": serverHostController.text,
                      "serverPort": serverPortController.text,
                      "username": usernameController.text,
                      "password": passwordController.text,
                      "emailProperty": emailPropertyController.text,
                    }
                };
              }
              widget.callbackOptionTest!(result);
            }
          }),
          const ButtonCancel()
        ],
      ),
    );
  }

  Column bodyWidget(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.only(bottom: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "type", defaultValue: "Type", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                  value: emailType,
                  hint: multiLanguageString(name: "choose_a_type", defaultValue: "Choose a type", context: context),
                  items: [
                    {"name": multiLanguageString(name: "microsoft_365", defaultValue: "Microsoft 365", context: context), "value": "microsoft365"},
                    {"name": multiLanguageString(name: "smtp", defaultValue: "SMTP", context: context), "value": "smtp"},
                  ].map<DropdownMenuItem<String>>((dynamic result) {
                    return DropdownMenuItem(
                      value: result["value"],
                      child: Text(result["name"]),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      emailType = newValue!;
                    });
                  },
                  borderRadius: 5,
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "name", defaultValue: "Name", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: emailNameController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "address", defaultValue: "Address", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: emailAddressController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "reply_name", defaultValue: "Reply Name", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: emailReplyNameController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "reply_address", defaultValue: "Reply Address", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: emailReplyAddressController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "status", defaultValue: "Status", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                  value: _status,
                  hint: multiLanguageString(name: "choose_a_status", defaultValue: "Choose a status", context: context),
                  items: [
                    {"name": multiLanguageString(name: "active", defaultValue: "Active", context: context), "value": 1},
                    {"name": multiLanguageString(name: "inactive", defaultValue: "Inactive", context: context), "value": 0},
                  ].map<DropdownMenuItem<int>>((dynamic result) {
                    return DropdownMenuItem(
                      value: result["value"],
                      child: Text(result["name"]),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      _status = newValue!;
                    });
                  },
                  borderRadius: 5,
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        if (emailType == 'microsoft365')
          Column(
            children: [
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "client_id", defaultValue: "Client Id", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicTextField(
                        controller: azureClientIdController,
                        hintText: "...",
                        isRequiredNotEmpty: true,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "client_secret", defaultValue: "Client Secret", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicTextField(
                        controller: azureClientSecretController,
                        hintText: "...",
                        isRequiredNotEmpty: true,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "authority", defaultValue: "Authority", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicTextField(
                        controller: azureAuthorityController,
                        hintText: "...",
                        isRequiredNotEmpty: true,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "redirect_url", defaultValue: "Redirect Url", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicTextField(
                        controller: azureRedirectUrlController,
                        hintText: "...",
                        isRequiredNotEmpty: true,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        if (emailType == "smtp")
          Column(
            children: [
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "server_host", defaultValue: "Server Host", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicTextField(
                        controller: serverHostController,
                        hintText: "...",
                        isRequiredNotEmpty: true,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "server_port", defaultValue: "Server Port", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicTextField(
                        controller: serverPortController,
                        hintText: "...",
                        isRequiredNotEmpty: true,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "username", defaultValue: "Username", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicTextField(
                        controller: usernameController,
                        hintText: "...",
                        isRequiredNotEmpty: true,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "password", defaultValue: "Password", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicTextField(
                        controller: passwordController,
                        hintText: "...",
                        isRequiredNotEmpty: true,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "property", defaultValue: "Property", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicTextField(
                        controller: emailPropertyController,
                        hintText: "...",
                        isRequiredNotEmpty: true,
                        maxline: 10,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )
      ],
    );
  }

  Padding headerWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 35, 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "type_email_account",
            defaultValue: "\$0 Email account",
            variables: [widget.title],
            isLowerCase: false,
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
            onPressed: () => Navigator.pop(context, 'cancel'),
            child: const Icon(
              Icons.close,
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }
}
