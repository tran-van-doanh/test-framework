import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dialog/dialog_delete.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/settings_application/email_account/option_email._account.dart';
import 'package:test_framework_view/type.dart';

import '../../../common/custom_state.dart';

class SettingEmailAccountWidget extends StatefulWidget {
  const SettingEmailAccountWidget({Key? key}) : super(key: key);

  @override
  State<SettingEmailAccountWidget> createState() => _SettingEmailAccountWidgetState();
}

class _SettingEmailAccountWidgetState extends CustomState<SettingEmailAccountWidget> {
  late Future futureemailAccountList;
  var emailAccountSearchRequest = {};
  var emailAccountRowCount = 0;
  var emailAccountCurrentPage = 1;
  var emailAccountResultList = [];
  var emailAccountRowPerPage = 10;
  var emailAccountStatusMap = {1: "Active", 0: "Inactive"};
  TextEditingController emailNameController = TextEditingController();
  String? emailType;

  @override
  void initState() {
    futureemailAccountList = emailAccountPageChange(emailAccountCurrentPage);
    super.initState();
  }

  emailAccountSearch() {
    emailAccountSearchRequest = {
      "emailNameLike": emailNameController.text,
      "emailType": emailType,
    };
    futureemailAccountList = emailAccountPageChange(emailAccountCurrentPage);
  }

  emailAccountPageChange(page) async {
    if ((page - 1) * emailAccountRowPerPage > emailAccountRowCount) {
      page = (1.0 * emailAccountRowCount / emailAccountRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * emailAccountRowPerPage, "queryLimit": emailAccountRowPerPage};
    if (emailAccountSearchRequest["emailNameLike"] != null && emailAccountSearchRequest["emailNameLike"].isNotEmpty) {
      findRequest["emailNameLike"] = "%${emailAccountSearchRequest["emailNameLike"]}%";
    }
    if (emailAccountSearchRequest["emailType"] != null && emailAccountSearchRequest["emailType"].isNotEmpty) {
      findRequest["emailType"] = emailAccountSearchRequest["emailType"];
    }
    if (emailAccountSearchRequest["actionType"] != null && emailAccountSearchRequest["actionType"].isNotEmpty) {
      findRequest["actionType"] = emailAccountSearchRequest["actionType"];
    }
    var response = await httpPost("/test-framework-api/user/noti/noti-email-account/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        emailAccountCurrentPage = page;
        emailAccountRowCount = response["body"]["rowCount"];
        emailAccountResultList = response["body"]["resultList"];
      });
    }
    return 0;
  }

  handleDeleteEmailAccount(emailAccount) async {
    var response = await httpDelete("/test-framework-api/user/noti/noti-email-account/${emailAccount["id"]}", context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        await emailAccountPageChange(emailAccountCurrentPage);
      }
    }
  }

  handleAddEmailAccount(result) async {
    var response = await httpPut("/test-framework-api/user/noti/noti-email-account", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await emailAccountPageChange(emailAccountCurrentPage);
      }
    }
  }

  handleEditEmailAccount(result, emailAccount) async {
    var response = await httpPost("/test-framework-api/user/noti/noti-email-account/${emailAccount["id"]}", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await emailAccountPageChange(emailAccountCurrentPage);
      }
    }
  }

  @override
  void dispose() {
    emailNameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, LanguageModel>(builder: (context, navigationModel, languageModel, child) {
      return FutureBuilder(
        future: futureemailAccountList,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Scaffold(
              body: Column(
                children: [
                  Expanded(
                    child: ListView(
                      children: [
                        OrganizationNavMenuTab(
                          margin: const EdgeInsets.fromLTRB(45, 20, 30, 0),
                          navigationList: context.settingsMenuList,
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              MultiLanguageText(
                                name: "email_account_count",
                                defaultValue: "Email Account (\$0)",
                                variables: ["$emailAccountRowCount"],
                                style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
                              ),
                              searchForm(),
                              tableWidget(),
                              DynamicTablePagging(
                                emailAccountRowCount,
                                emailAccountCurrentPage,
                                emailAccountRowPerPage,
                                pageChangeHandler: (page) {
                                  setState(() {
                                    futureemailAccountList = emailAccountPageChange(page);
                                  });
                                },
                                rowPerPageChangeHandler: (rowPerPage) {
                                  setState(() {
                                    emailAccountRowPerPage = rowPerPage!;
                                    futureemailAccountList = emailAccountPageChange(1);
                                  });
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  btnWidget(context),
                ],
              ),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        },
      );
    });
  }

  Widget searchForm() {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                child: SizedBox(
                  height: 50,
                  child: DynamicDropdownButton(
                    value: emailType,
                    hint: multiLanguageString(name: "choose_a_type", defaultValue: "Choose a type", context: context),
                    items: [
                      {"name": multiLanguageString(name: "microsoft_365", defaultValue: "Microsoft 365", context: context), "value": "microsoft365"},
                      {"name": multiLanguageString(name: "smtp", defaultValue: "SMTP", context: context), "value": "smtp"},
                    ].map<DropdownMenuItem<String>>((dynamic result) {
                      return DropdownMenuItem(
                        value: result["value"],
                        child: Text(result["name"]),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        emailType = newValue!;
                      });
                    },
                    borderRadius: 5,
                    isRequiredNotEmpty: true,
                  ),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                child: SizedBox(
                  height: 50,
                  child: DynamicTextField(
                    controller: emailNameController,
                    onComplete: () => emailAccountSearch(),
                    onChanged: (value) {
                      setState(() {
                        emailNameController.text;
                      });
                    },
                    labelText: multiLanguageString(name: "search_title", defaultValue: "Search title", context: context),
                    hintText: multiLanguageString(name: "by_title", defaultValue: "By title", context: context),
                    suffixIcon: (emailNameController.text != "")
                        ? IconButton(
                            onPressed: () {
                              setState(() {
                                emailNameController.clear();
                              });
                              emailAccountSearch();
                            },
                            icon: const Icon(Icons.clear),
                            splashRadius: 20,
                          )
                        : null,
                  ),
                ),
              ),
            ),
          ],
        ),
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 20),
              child: ElevatedButton(
                  onPressed: () {
                    emailAccountSearch();
                  },
                  child: const MultiLanguageText(name: "search", defaultValue: "Search", isLowerCase: false)),
            ),
            ElevatedButton(
                onPressed: () {
                  setState(() {
                    emailNameController.clear();
                    emailType = null;
                    emailAccountSearch();
                  });
                },
                child: const MultiLanguageText(name: "reset", defaultValue: "Reset", isLowerCase: false))
          ],
        ),
      ],
    );
  }

  Padding btnWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 40, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            margin: const EdgeInsets.only(right: 10),
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              icon: const IconTheme(
                data: IconThemeData(color: Color.fromRGBO(49, 49, 49, 1)),
                child: Icon(Icons.add),
              ),
              label: MultiLanguageText(
                name: "new_email_account",
                defaultValue: "New Email Account",
                isLowerCase: false,
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
              onPressed: () {
                Navigator.of(context).push(
                  createRoute(
                    DialogOptionEmailAccountWidget(
                        type: "create",
                        title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                        callbackOptionTest: (result) {
                          handleAddEmailAccount(result);
                        }),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Container tableWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 30),
      child: CustomDataTableWidget(
        columns: [
          Expanded(
            flex: 2,
            child: MultiLanguageText(
              name: "name",
              defaultValue: "Name",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            child: MultiLanguageText(
              name: "type",
              defaultValue: "Type",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            child: MultiLanguageText(
              name: "address",
              defaultValue: "Address",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            child: MultiLanguageText(
              name: "status",
              defaultValue: "Status",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          const SizedBox(
            width: 38,
          ),
        ],
        rows: [
          for (var emailAccount in emailAccountResultList)
            CustomDataRow(
              cells: [
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        const Icon(Icons.description, color: Color.fromRGBO(61, 148, 254, 1)),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Text(
                            emailAccount["emailName"] ?? "",
                            overflow: TextOverflow.ellipsis,
                            style: Style(context).styleTextDataCell,
                            maxLines: 3,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Text(
                    emailAccount["emailType"] ?? "",
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                Expanded(
                  child: Text(
                    emailAccount["emailAddress"] ?? "",
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                Expanded(
                  child: Text(
                    emailAccountStatusMap[emailAccount["status"]] ?? "Not defined",
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                PopupMenuButton(
                  offset: const Offset(5, 50),
                  tooltip: '',
                  splashRadius: 10,
                  icon: const Icon(
                    Icons.more_vert,
                  ),
                  itemBuilder: (context) => [
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.of(context).push(
                            createRoute(
                              DialogOptionEmailAccountWidget(
                                  type: "edit",
                                  title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                  id: emailAccount["id"],
                                  callbackOptionTest: (result) {
                                    handleEditEmailAccount(result, emailAccount);
                                  }),
                            ),
                          );
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                        leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          dialogDeleteEmailAccount(emailAccount);
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "delete", defaultValue: "Delete"),
                        leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                  ],
                ),
              ],
            ),
        ],
      ),
    );
  }

  dialogDeleteEmailAccount(emailAccount) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogDelete(
          selectedItem: emailAccount,
          callback: () {
            handleDeleteEmailAccount(emailAccount);
          },
          type: 'email_account',
        );
      },
    );
  }
}
