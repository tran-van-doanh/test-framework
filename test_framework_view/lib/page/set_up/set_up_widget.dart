import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'dart:async';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/badge/badge_icon.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import '../../../../api.dart';
import '../../../../model/model.dart';

import '../../../common/custom_state.dart';

class SetUpWidget extends StatefulWidget {
  const SetUpWidget({Key? key}) : super(key: key);

  @override
  State<SetUpWidget> createState() => _SetUpWidgetState();
}

class _SetUpWidgetState extends CustomState<SetUpWidget> {
  late Future futureListSetUp;
  var resultListSetUp = [];
  var searchRequest = {};
  var prjProjectId = "";
  @override
  void initState() {
    prjProjectId = Provider.of<NavigationModel>(context, listen: false).prjProjectId ?? "";
    search({});
    super.initState();
  }

  search(searchRequest) {
    this.searchRequest = searchRequest;
    futureListSetUp = getListSetUp();
  }

  Future getListSetUp() async {
    var findRequest = {
      "prjProjectId": prjProjectId,
    };
    if (searchRequest["titleLike"] != null && searchRequest["titleLike"].isNotEmpty) {
      findRequest["titleLike"] = "%${searchRequest["titleLike"]}%";
    }
    if (searchRequest["owner"] != null && searchRequest["owner"].isNotEmpty) {
      findRequest["owner"] = searchRequest["owner"];
    }
    if (searchRequest["status"] != null) {
      findRequest["status"] = searchRequest["status"];
    }
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-component/search", findRequest, context);

    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        resultListSetUp = response["body"]["resultList"];
      });
    }
    return 0;
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, LanguageModel>(
      builder: (context, navigationModel, languageModel, child) {
        return FutureBuilder(
          future: futureListSetUp,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView(
                padding: const EdgeInsets.fromLTRB(50, 20, 50, 30),
                children: [
                  NavMenuTabV1(
                    currentUrl: "/setting/set-up",
                    navigationList: context.settingMenuList,
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  MultiLanguageText(
                    name: "set_up",
                    defaultValue: "Set Up",
                    style: TextStyle(fontSize: context.fontSizeManager.fontSizeText, fontWeight: FontWeight.w500),
                  ),
                  Text(
                    context.fontSizeManager.fontSizeText.toString(),
                  ),
                  tableWidget(context),
                ],
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  SelectionArea tableWidget(BuildContext context) {
    return SelectionArea(
      child: Container(
        margin: const EdgeInsets.only(top: 30),
        child: CustomDataTableWidget(
          columns: [
            Expanded(
              flex: 3,
              child: MultiLanguageText(
                name: "name",
                defaultValue: "Name",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              flex: 3,
              child: MultiLanguageText(
                name: "description",
                defaultValue: "Description",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "create_date",
                defaultValue: "Create date",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "owner",
                defaultValue: "Owner",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  MultiLanguageText(
                    name: "status",
                    defaultValue: "Status",
                    isLowerCase: false,
                    style: Style(context).styleTextDataColumn,
                  ),
                  Tooltip(
                    message: multiLanguageString(name: "active_inactive", defaultValue: "Green = Active\nRed = Inactive", context: context),
                    child: const Icon(
                      Icons.info,
                      color: Colors.grey,
                      size: 16,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: 38,
            ),
          ],
          rows: [
            for (var resultSetUp in resultListSetUp)
              CustomDataRow(
                cells: [
                  Expanded(
                    flex: 3,
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Row(
                        children: [
                          const Icon(Icons.description, color: Color.fromRGBO(61, 148, 254, 1)),
                          const SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Text(
                              resultSetUp["title"] ?? "",
                              overflow: TextOverflow.ellipsis,
                              style: Style(context).styleTextDataCell,
                              maxLines: 3,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Text(
                      resultSetUp["description"] ?? "",
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        resultSetUp["createDate"] != null
                            ? DateFormat('dd-MM-yyyy HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultSetUp["createDate"]).toLocal())
                            : "",
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Text(
                      resultSetUp["sysUser"]["fullname"],
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                  Expanded(
                    child: Center(
                      child: Badge2StatusSettingWidget(
                        status: resultSetUp["status"],
                      ),
                    ),
                  ),
                  PopupMenuButton(
                    offset: const Offset(5, 50),
                    tooltip: '',
                    splashRadius: 10,
                    itemBuilder: (context) => [
                      PopupMenuItem(
                        padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                        child: ListTile(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          contentPadding: const EdgeInsets.all(0),
                          hoverColor: Colors.transparent,
                          title: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                          leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
                        ),
                      ),
                      PopupMenuItem(
                        padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                        child: ListTile(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          contentPadding: const EdgeInsets.all(0),
                          hoverColor: Colors.transparent,
                          title: const MultiLanguageText(name: "delete", defaultValue: "Delete"),
                          leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
          ],
        ),
      ),
    );
  }
}
