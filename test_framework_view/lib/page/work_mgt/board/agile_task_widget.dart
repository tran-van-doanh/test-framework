import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';

import '../../../../../common/custom_state.dart';
import '../../../api.dart';
import '../../../components/toast.dart';
import '../../../model/model.dart';
import '../../../type.dart';
import '../item_list_option.dart';
import '../tag/option_tags_widget.dart';
import 'group_agile_task_widget.dart';

class AgileTaskWidget extends StatefulWidget {
  const AgileTaskWidget({
    Key? key,
  }) : super(key: key);

  @override
  State<AgileTaskWidget> createState() => _AgileTaskWidgetState();
}

class _AgileTaskWidgetState extends CustomState<AgileTaskWidget> {
  late Future future;
  String prjProjectId = "";
  bool isExpandAll = true;
  Sprint? sprint;
  List<Tag> tagsList = [];
  List<TaskStatus> taskStatusList = [
    TaskStatus(id: "1", name: "active", title: "Active", statusColor: "#008000", status: 1),
    TaskStatus(id: "0", name: "inactive", title: "Inactive", statusColor: "#FF0000", status: 0)
  ];
  List<SprintTask> sprintTaskList = [];
  Completer<void> completer = Completer<void>();
  TextEditingController searchController = TextEditingController();
  @override
  void initState() {
    prjProjectId = Provider.of<NavigationModel>(context, listen: false).prjProjectId ?? "";
    future = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    final futures = <Future>[
      getSprint(),
      getTaskStatus(),
      getListTags(),
    ];
    final result = await Future.wait<dynamic>(futures);
    if (result.length == futures.length) {
      return 0;
    }
  }

  getSprint() async {
    var findRequest = {"prjProjectId": prjProjectId, "status": 1};
    var response = await httpPost("/test-framework-api/user/project/prj-sprint/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false && response["body"]["resultList"].isNotEmpty) {
      setState(() {
        sprint = Sprint.fromMap(response["body"]["resultList"][0]);
      });
    }
  }

  getTaskStatus() async {
    var findRequest = {"prjProjectId": prjProjectId, "status": 1, "flowTypes": "TASK"};
    var response = await httpPost("/test-framework-api/user/workflow/wf-status/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false && response["body"]["resultList"].isNotEmpty) {
      setState(() {
        taskStatusList = List<TaskStatus>.from(response["body"]["resultList"].map((e) => TaskStatus.fromMap(e)));
      });
    }
  }

  getListTags() async {
    var findRequest = {"prjProjectId": prjProjectId, "status": 1};
    var response = await httpPost("/test-framework-api/user/project/prj-task-tag/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        tagsList = List<Tag>.from(response["body"]["resultList"].map((e) => Tag.fromMap(e)));
      });
    }
  }

  handleAddTags(result) async {
    var response = await httpPut("/test-framework-api/user/project/prj-task-tag", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await getListTags();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          return FutureBuilder(
            future: future,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
                          // decoration: const BoxDecoration(
                          //   image: DecorationImage(
                          //     image: AssetImage("assets/images/bg_agile_task.jpg"),
                          //     fit: BoxFit.cover,
                          //   ),
                          // ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              MultiLanguageText(
                                name: "kanban_board",
                                defaultValue: "Kanban Board",
                                style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
                              ),
                              Row(
                                children: [
                                  if (sprint != null)
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        MultiLanguageText(
                                          name: "sprint_name_string",
                                          defaultValue: "Sprint name: \$0",
                                          variables: [sprint!.title],
                                          style: TextStyle(fontSize: context.fontSizeManager.fontSizeText22, fontWeight: FontWeight.bold),
                                        ),
                                        Text(
                                          "${DateFormat('dd/MM/yyyy').format(DateTime.parse(sprint!.startDate))} - ${DateFormat('dd/MM/yyyy').format(DateTime.parse(sprint!.endDate))}",
                                          style: TextStyle(fontSize: context.fontSizeManager.fontSizeText18, fontWeight: FontWeight.w500),
                                        ),
                                      ],
                                    ),
                                  Expanded(child: tagsList.isNotEmpty ? headerBarWidget() : const SizedBox.shrink()),
                                ],
                              ),
                              Container(
                                alignment: Alignment.centerRight,
                                child: ConstrainedBox(
                                  constraints: const BoxConstraints(maxWidth: 300),
                                  child: DynamicTextField(
                                    prefixIcon: const Icon(Icons.search),
                                    controller: searchController,
                                    hintText: multiLanguageString(name: "search_task_title", defaultValue: "Search task title", context: context),
                                    onChanged: (value) {
                                      setState(() {
                                        searchController.text;
                                      });
                                    },
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 20,
                              ),
                              if (taskStatusList.isNotEmpty)
                                LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
                                  if (taskStatusList.length >= 4) {
                                    return SingleChildScrollView(
                                      scrollDirection: Axis.horizontal,
                                      child: SizedBox(
                                        width:
                                            (constraints.maxWidth / taskStatusList.length < 300) ? taskStatusList.length * 300 : constraints.maxWidth,
                                        child: agileTaskWidget(),
                                      ),
                                    );
                                  } else if (constraints.maxWidth < 1200) {
                                    return SingleChildScrollView(
                                      scrollDirection: Axis.horizontal,
                                      child: SizedBox(
                                        width: 1200,
                                        child: agileTaskWidget(),
                                      ),
                                    );
                                  }
                                  return agileTaskWidget();
                                }),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                );
              } else if (snapshot.hasError) {
                return Text('${snapshot.error}');
              }
              return const Center(child: CircularProgressIndicator());
            },
          );
        },
      );
    });
  }

  Wrap headerBarWidget() {
    return Wrap(
      crossAxisAlignment: WrapCrossAlignment.center,
      alignment: WrapAlignment.end,
      children: [
        const Icon(
          Icons.sell,
        ),
        const SizedBox(
          width: 10,
        ),
        for (var tag in tagsList)
          Padding(
            padding: EdgeInsets.only(right: tagsList.last != tag ? 5 : 0, bottom: 5),
            child: GestureDetector(
              onTap: () {},
              child: TagItemAgileTaskWidget(
                tag: tag,
              ),
            ),
          ),
        Padding(
          padding: const EdgeInsets.only(left: 5, bottom: 5),
          child: SizedBox(
            height: 33,
            child: ElevatedButton(
              child: const Icon(
                Icons.add,
                color: Colors.black,
                size: 16,
              ),
              onPressed: () {
                Navigator.of(context).push(
                  createRoute(
                    OptionTagsWidget(
                      type: "create",
                      title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                      callbackOptionTest: (result) {
                        handleAddTags(result);
                      },
                      prjProjectId: prjProjectId,
                    ),
                  ),
                );
              },
            ),
          ),
        )
      ],
    );
  }

  Widget agileTaskWidget() {
    return (sprint != null)
        ? GroupAgileTaskWidget(
            taskStatusList: taskStatusList,
            sprint: sprint!,
            prjProjectId: prjProjectId,
            prjTaskTitleLike: searchController.text,
          )
        : const SizedBox.shrink();
  }
}
