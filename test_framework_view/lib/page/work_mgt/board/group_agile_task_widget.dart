import 'dart:async';
import 'package:flutter/material.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import '../../../api.dart';
import '../../../common/custom_draggable.dart';
import '../../../components/toast.dart';
import '../../../extension/hex_color.dart';
import '../../../type.dart';
import '../task/option_task_widget.dart';
import '../task/task_item_widget.dart';

class GroupAgileTaskWidget extends StatefulWidget {
  const GroupAgileTaskWidget({
    Key? key,
    required this.sprint,
    required this.taskStatusList,
    required this.prjProjectId,
    required this.prjTaskTitleLike,
  }) : super(key: key);
  final String prjTaskTitleLike;
  final List<TaskStatus> taskStatusList;
  final Sprint sprint;
  final String prjProjectId;
  @override
  State<GroupAgileTaskWidget> createState() => _GroupAgileTaskWidgetState();
}

class _GroupAgileTaskWidgetState extends State<GroupAgileTaskWidget> {
  Widget widgetGhost = Container();
  int oldIndexItem = -1;
  int newIndexItem = -1;
  bool dragAccepted = false;
  int acceptedDataIndex = -1;
  int acceptedDataIndexTaskStatus = -1;
  List<SprintTask> sprintTaskList = [];
  Completer completer = Completer();
  String prjTaskTitleLike = '';
  @override
  void initState() {
    super.initState();
    getListSprintTask(widget.sprint.id);
  }

  getListSprintTask(String prjSprintId, {String? prjTaskTitleLike}) async {
    var findRequest = {"prjSprintId": prjSprintId};
    if (prjTaskTitleLike != null && prjTaskTitleLike.isNotEmpty) findRequest["prjTaskTitleLike"] = "%$prjTaskTitleLike%";
    var response = await httpPost("/test-framework-api/user/project/prj-sprint-task/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      setState(() {
        sprintTaskList = List<SprintTask>.from(response["body"]["resultList"].map((e) => SprintTask.fromMap(e)));
      });
    }
  }

  handleDeleteSprintTask(id) async {
    var response = await httpDelete("/test-framework-api/user/project/prj-sprint-task/$id", context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        await getListSprintTask(widget.sprint.id);
      }
    }
  }

  // handleAddSprintTask(result) async {
  //   var response = await httpPut("/test-framework-api/user/project/prj-sprint-task", result, context);
  //   if (response.containsKey("body") && response["body"] is String == false && mounted) {
  //     if (response["body"].containsKey("errorMessage")) {
  //       showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
  //     } else {
  //       showToast(
  //           context: context,
  //           msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
  //           color: Colors.greenAccent,
  //           icon: const Icon(Icons.done));
  //       Navigator.of(context).pop();
  //       await getListSprintTask(widget.sprint.id);
  //     }
  //   }
  // }

  handleEditSprintTask(result, id) async {
    var response = await httpPost("/test-framework-api/user/project/prj-sprint-task/$id", result, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      if (response["body"].containsKey("errorMessage") && mounted) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
        return false;
      }
      return true;
    }
    return false;
  }

  handleEditTasks(result, id) async {
    var response = await httpPost("/test-framework-api/user/project/prj-task/$id", result, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      if (response["body"].containsKey("errorMessage") && mounted) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
        return false;
      }
      return true;
    }
    return false;
  }

  // handleAddTasks(result) async {
  //   var response = await httpPut("/test-framework-api/user/project/prj-task", result, context);
  //   if (response.containsKey("body") && response["body"] is String == false) {
  //     if (response["body"].containsKey("errorMessage") && mounted) {
  //       showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
  //     } else {
  //       return response["body"];
  //     }
  //   }
  // }

  getTaskById(id) async {
    var response = await httpGet("/test-framework-api/user/project/prj-task/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      return response["body"]["result"];
    }
  }

  getSprintTaskById(id) async {
    var response = await httpGet("/test-framework-api/user/project/prj-sprint-task/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      return response["body"]["result"];
    }
  }

  @override
  Widget build(BuildContext context) {
    if (prjTaskTitleLike != widget.prjTaskTitleLike) {
      prjTaskTitleLike = widget.prjTaskTitleLike;
      getListSprintTask(widget.sprint.id, prjTaskTitleLike: prjTaskTitleLike);
    }
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        for (var j = 0; j < widget.taskStatusList.length; j++)
          Expanded(
            child: Container(
              margin: EdgeInsets.only(right: (widget.taskStatusList.length == j + 1) ? 0 : 25),
              decoration: BoxDecoration(
                color: const Color.fromARGB(255, 238, 238, 238),
                borderRadius: BorderRadius.circular(5),
              ),
              padding: const EdgeInsets.only(top: 5),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        widget.taskStatusList[j].title,
                        style: TextStyle(fontSize: context.fontSizeManager.fontSizeText18, fontWeight: FontWeight.bold),
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 6),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(color: HexColor.fromHex(widget.taskStatusList[j].statusColor), width: 2)),
                        child: Text(
                          getTaskStatusLength(j).toString(),
                          style: TextStyle(fontSize: context.fontSizeManager.fontSizeText18, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: DragTarget(
                          builder: (
                            BuildContext context,
                            List<dynamic> accepted,
                            List<dynamic> rejected,
                          ) {
                            return Container(
                              padding: const EdgeInsets.only(bottom: 20),
                              height: MediaQuery.of(context).size.height - 247,
                              child: SingleChildScrollView(
                                child: Column(
                                  children: [
                                    for (var i = 0; i < sprintTaskList.length; i++)
                                      if (sprintTaskList[i].wfStatusId == widget.taskStatusList[j].id)
                                        GestureDetector(
                                          onTap: () {
                                            Navigator.of(context).push(
                                              createRoute(
                                                OptionTaskWidget(
                                                  type: 'edit',
                                                  title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                                  callbackOptionTest: (result) async {
                                                    bool isSuccess = await handleEditTasks(result, result["id"]);
                                                    if (isSuccess && mounted) {
                                                      Navigator.of(this.context).pop();
                                                      showToast(
                                                          context: this.context,
                                                          msg: multiLanguageString(
                                                              name: "update_successful", defaultValue: "Update successful", context: this.context),
                                                          color: Colors.greenAccent,
                                                          icon: const Icon(Icons.done));
                                                      await getListSprintTask(widget.sprint.id);
                                                    }
                                                  },
                                                  id: sprintTaskList[i].prjTask.id,
                                                ),
                                              ),
                                            );
                                          },
                                          child: DragTarget(
                                            builder: (
                                              BuildContext context,
                                              List<dynamic> accepted,
                                              List<dynamic> rejected,
                                            ) {
                                              return Container(
                                                margin: const EdgeInsets.fromLTRB(10, 20, 10, 0),
                                                child: Column(
                                                  children: [
                                                    if (acceptedDataIndex == i)
                                                      Container(
                                                        margin: EdgeInsets.only(bottom: oldIndexItem == i ? 0 : 20),
                                                        child: Opacity(
                                                          opacity: 0.5,
                                                          child: widgetGhost,
                                                        ),
                                                      ),
                                                    CustomDraggable(
                                                      data: sprintTaskList[i],
                                                      feedback: SprintTaskWidget(
                                                        sprintTask: sprintTaskList[i],
                                                        onRemove: () => handleDeleteSprintTask(sprintTaskList[i].id),
                                                      ),
                                                      childWhenDragging: Container(),
                                                      onDragStarted: () {
                                                        oldIndexItem = i;
                                                        acceptedDataIndex = i;
                                                        // dragAccepted = false;
                                                      },
                                                      onDragEnd: (draggableDetails) {
                                                        if (completer.isCompleted) {
                                                          acceptedDataIndex = -1;
                                                          acceptedDataIndexTaskStatus = -1;
                                                          completer = Completer();
                                                        } else {
                                                          completer.future.then((_) {
                                                            acceptedDataIndex = -1;
                                                            acceptedDataIndexTaskStatus = -1;
                                                            completer = Completer();
                                                          });
                                                        }
                                                      },
                                                      child: SprintTaskWidget(
                                                        sprintTask: sprintTaskList[i],
                                                        onRemove: () => handleDeleteSprintTask(sprintTaskList[i].id),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              );
                                            },
                                            onAccept: (data) async {
                                              if (data is SprintTask && data.taskIndex != (sprintTaskList[i].taskIndex - 1)) {
                                                var result = await getSprintTaskById(data.id);
                                                result["wfStatusId"] = widget.taskStatusList[j].id;
                                                result["taskIndex"] = sprintTaskList[i].taskIndex - 1;
                                                bool isSuccess = await handleEditSprintTask(result, data.id);
                                                if (mounted && isSuccess) {
                                                  await getListSprintTask(widget.sprint.id);
                                                }
                                              }
                                              if (data is Tag) {
                                                var result = await getTaskById(sprintTaskList[i].prjTask.id);
                                                if (!result["tagsList"].contains(data.id)) {
                                                  result["tagsList"].add(data.id);
                                                  bool isSuccess = await handleEditTasks(result, result["id"]);
                                                  if (isSuccess && mounted) {
                                                    await getListSprintTask(widget.sprint.id);
                                                  }
                                                } else if (mounted) {
                                                  showToast(
                                                      context: this.context,
                                                      msg: multiLanguageString(
                                                          name: "duplicate_tag", defaultValue: "Duplicate tag", context: this.context),
                                                      color: Colors.red,
                                                      icon: const Icon(Icons.info));
                                                }
                                              }
                                              completer.complete();
                                            },
                                            onLeave: (data) {
                                              acceptedDataIndex = -1;
                                            },
                                            onWillAccept: (data) {
                                              if (data is SprintTask) {
                                                acceptedDataIndex = i;
                                                widgetGhost = SprintTaskWidget(
                                                  sprintTask: data,
                                                  onRemove: () => handleDeleteSprintTask(data.id),
                                                );
                                              }
                                              return true;
                                            },
                                          ),
                                        ),
                                    if (acceptedDataIndexTaskStatus == j)
                                      Padding(
                                        padding: EdgeInsets.fromLTRB(10, oldIndexItem == getTaskStatusLength(j) - 1 ? 0 : 20, 10, 0),
                                        child: Opacity(
                                          opacity: 0.5,
                                          child: Container(
                                            child: widgetGhost,
                                          ),
                                        ),
                                      ),
                                  ],
                                ),
                              ),
                            );
                          },
                          onLeave: (data) {
                            acceptedDataIndexTaskStatus = -1;
                          },
                          onWillAccept: (data) {
                            if (data is SprintTask) {
                              acceptedDataIndexTaskStatus = j;
                              widgetGhost = SprintTaskWidget(
                                sprintTask: data,
                                onRemove: () => handleDeleteSprintTask(data.id),
                              );
                            }
                            return true;
                          },
                          onAccept: (Dragable data) async {
                            if (data is SprintTask) {
                              var result = await getSprintTaskById(data.id);
                              result["wfStatusId"] = widget.taskStatusList[j].id;
                              result["taskIndex"] = null;
                              bool isSuccess = await handleEditSprintTask(result, data.id);
                              if (mounted && isSuccess) {
                                await getListSprintTask(widget.sprint.id);
                              }
                            }
                            completer.complete();
                          },
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          )
      ],
    );
  }

  int getTaskStatusLength(int j) => sprintTaskList.where((element) => element.wfStatusId == widget.taskStatusList[j].id).length;
}
