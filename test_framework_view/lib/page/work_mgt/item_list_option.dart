import 'package:flutter/material.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import '../../common/color_picker.dart';
import '../../common/custom_draggable.dart';
import '../../components/style.dart';
import '../../extension/hex_color.dart';
import '../../type.dart';

class TagItemAgileTaskWidget extends StatelessWidget {
  final Tag tag;
  final bool canDrag;
  const TagItemAgileTaskWidget({
    Key? key,
    required this.tag,
    this.canDrag = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget widget = Container(
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 6),
      decoration: BoxDecoration(
        color: HexColor.fromHex(tag.tagColor).withOpacity(0.3),
        border: Border(
          left: BorderSide(color: HexColor.fromHex(tag.tagColor), width: 4),
        ),
      ),
      child: Text(
        tag.title,
        style: TextStyle(
          fontSize: context.fontSizeManager.fontSizeText14,
          decoration: TextDecoration.none,
          fontWeight: FontWeight.w500,
          color: const Color.fromRGBO(0, 0, 0, 0.8),
        ),
      ),
    );
    return canDrag
        ? CustomDraggable(
            data: tag,
            feedback: widget,
            childWhenDragging: widget,
            child: widget,
          )
        : widget;
  }
}

class TagListAgileTaskWidget extends StatefulWidget {
  final dynamic tag;
  final VoidCallback? onRemove;
  final Function? onChangeColor;
  final EdgeInsetsGeometry padding;
  const TagListAgileTaskWidget(
      {Key? key, required this.tag, this.onRemove, this.onChangeColor, this.padding = const EdgeInsets.symmetric(horizontal: 10, vertical: 10)})
      : super(key: key);

  @override
  State<TagListAgileTaskWidget> createState() => _TagListAgileTaskWidgetState();
}

class _TagListAgileTaskWidgetState extends State<TagListAgileTaskWidget> {
  Color? pickerColor;
  TextEditingController textController = TextEditingController();
  @override
  void initState() {
    super.initState();
    pickerColor = HexColor.fromHex(widget.tag["tagColor"]);
  }

  void changeColor(Color color) {
    setState(() => pickerColor = color);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: widget.padding,
      decoration: BoxDecoration(
        color: HexColor.fromHex(widget.tag["tagColor"]).withOpacity(0.5),
        // color: Colors.grey.withOpacity(0.3),
        border: Border(
          left: BorderSide(color: HexColor.fromHex(widget.tag["tagColor"]), width: 4),
          // left: BorderSide(color: Colors.grey, width: 4),
        ),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Text(
              widget.tag["title"],
              style: Style(context).styleTextDataCell,
            ),
          ),
          Row(
            children: [
              if (widget.onChangeColor != null)
                ColorPickerWidget(
                  pickerColor: pickerColor ?? Colors.grey,
                  onColorChanged: changeColor,
                  hexInputController: textController,
                  onSubmit: () {
                    setState(() => widget.tag["tagColor"] = pickerColor!.value);
                  },
                ),
              if (widget.onRemove != null)
                GestureDetector(
                  onTap: () => widget.onRemove!(),
                  child: const Icon(Icons.close),
                ),
            ],
          )
        ],
      ),
    );
  }
}

class UserListAgileTaskWidget extends StatelessWidget {
  final dynamic user;
  final VoidCallback? onRemove;
  const UserListAgileTaskWidget({Key? key, required this.user, this.onRemove}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(5),
      decoration: const BoxDecoration(
        color: Color.fromARGB(255, 190, 193, 206),
        border: Border(
          left: BorderSide(
            color: Color.fromARGB(255, 68, 166, 97),
            width: 4,
          ),
        ),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Text(
              user["fullname"],
              style: Style(context).styleTextDataCell,
            ),
          ),
          if (onRemove != null)
            GestureDetector(
              onTap: () => onRemove!(),
              child: const Icon(Icons.close),
            ),
        ],
      ),
    );
  }
}

class TestCaseListAgileTaskWidget extends StatelessWidget {
  final dynamic testCase;
  final VoidCallback? onRemove;
  const TestCaseListAgileTaskWidget({Key? key, required this.testCase, this.onRemove}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(5),
      decoration: const BoxDecoration(
        color: Color.fromARGB(255, 190, 193, 206),
        border: Border(
          left: BorderSide(
            color: Color.fromRGBO(61, 148, 254, 1),
            width: 4,
          ),
        ),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Text(
              testCase["title"],
              style: Style(context).styleTextDataCell,
            ),
          ),
          if (onRemove != null)
            GestureDetector(
              onTap: () => onRemove!(),
              child: const Icon(Icons.close),
            ),
        ],
      ),
    );
  }
}
