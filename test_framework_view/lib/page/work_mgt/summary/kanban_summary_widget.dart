import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pie_chart/pie_chart.dart' as pie;
import 'package:test_framework_view/components/footer_application.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/extension/hex_color.dart';
import 'package:test_framework_view/page/work_mgt/summary/priority_chart_widget.dart';
import '../../../api.dart';
import '../../../components/menu_tab.dart';
import '../../../components/style.dart';
import '../../../model/model.dart';

class KanbanSummaryWidget extends StatefulWidget {
  final bool isSummaryAll;
  const KanbanSummaryWidget({Key? key, this.isSummaryAll = false}) : super(key: key);

  @override
  State<KanbanSummaryWidget> createState() => _KanbanSummaryWidgetState();
}

class _KanbanSummaryWidgetState extends State<KanbanSummaryWidget> {
  late Future future;
  var prjProjectId = "";
  List taskStatusList = [];
  @override
  void initState() {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    prjProjectId = navigationModel.prjProjectId ?? "";
    future = getInfoPage();
    super.initState();
  }

  String getGretting() {
    DateTime now = DateTime.now();
    if (now.hour < 12) {
      return multiLanguageString(name: "good_morning", defaultValue: "Good Morning", context: context);
    } else if (now.hour < 18) {
      return multiLanguageString(name: "good_afternoon", defaultValue: "Good Afternoon", context: context);
    } else {
      return multiLanguageString(name: "good_evening", defaultValue: "Good Evening", context: context);
    }
  }

  getInfoPage() async {
    // final futures = <Future>[];
    // final result = await Future.wait<dynamic>(futures);
    // if (result.length == futures.length) {
    return 0;
    // }
  }

  getTaskByTaskStatus(taskStatus) async {
    var findRequest = {"prjProjectId": prjProjectId, "prjTaskStatusId": taskStatus["id"]};
    var response = await httpPost("/test-framework-api/user/project/prj-task/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        taskStatus["taskListCount"] = response["body"]["rowCount"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, LanguageModel>(
      builder: (context, navigationModel, languageModel, child) {
        return FutureBuilder(
          future: future,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(50, 20, 50, 30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        if (widget.isSummaryAll)
                          OrganizationNavMenuTab(
                            navigationList: context.workManagementSettingsMenuList,
                          ),
                        if (widget.isSummaryAll)
                          const SizedBox(
                            height: 30,
                          ),
                        Text(
                          getGretting(),
                          style: TextStyle(
                            fontSize: context.fontSizeManager.fontSizeText30,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        overviewCards(),
                        const SizedBox(
                          height: 30,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            if (taskStatusList.isNotEmpty) Expanded(child: taskStatusOverview(navigationModel)),
                            if (taskStatusList.isNotEmpty)
                              const SizedBox(
                                width: 10,
                              ),
                            Expanded(child: recentActivity()),
                          ],
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                        priorityBreakdown(navigationModel),
                        const SizedBox(
                          height: 30,
                        ),
                        typesOfWork(navigationModel),
                        const SizedBox(
                          height: 30,
                        ),
                        teamWorkload(navigationModel),
                      ],
                    ),
                  ),
                  const FooterApplicationWidget(productName: "Work Management"),
                ],
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  Column recentActivity() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MultiLanguageText(
          name: "recent_activity",
          defaultValue: "Recent Activity",
          isLowerCase: false,
          style: Style(context).styleTitleDialog,
        ),
        const SizedBox(
          height: 5,
        ),
        MultiLanguageText(
          name: "stay_up_to_date",
          defaultValue: "Stay up to date with what's happening across the project",
          style: TextStyle(
            color: const Color.fromRGBO(0, 0, 0, 0.8),
            fontSize: context.fontSizeManager.fontSizeText14,
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        dayActivity(
          title: multiLanguageString(name: "today", defaultValue: "Today", isLowerCase: false, context: context),
          list: [
            activityItem(
                username: "Linh BM",
                type: " updated the Task Status of ",
                taskName: "Automation Testing Login",
                description: "Update the Task Name of automation_testing_login",
                time: "1 hour ago"),
            const SizedBox(
              height: 10,
            ),
            activityItem(
                username: "Quỳnh DN",
                type: " created a task ",
                taskName: "Automation Testing 'User Management' Function",
                description: "Automation Testing 'User Management' Function",
                time: "5 hours ago"),
            const SizedBox(
              height: 10,
            ),
            activityItem(
                username: "Anh NN",
                type: " updated the Actual End Date of ",
                taskName: "Manual Login",
                description: "Update the Actual End Date of manual_login",
                time: "7 hours ago"),
          ],
        ),
        const SizedBox(
          height: 20,
        ),
        dayActivity(
          title: multiLanguageString(name: "yesterday", defaultValue: "Yesterday", isLowerCase: false, context: context),
          list: [
            activityItem(
                username: "Quân NM",
                type: " updated the Involve to of ",
                description: "Update the Involve to of manual_testing",
                taskName: "Manual Testing Login",
                time: "2 days ago"),
            const SizedBox(
              height: 10,
            ),
            activityItem(
                username: "Anh NN",
                type: " created a task ",
                taskName: "Create User Management Test Scenario",
                description: "Created a task create_user",
                time: "2 days ago"),
            const SizedBox(
              height: 10,
            ),
            activityItem(
                username: "Quân NM",
                type: " updated the Schedule Start Date of ",
                taskName: "Manual Testing Login",
                description: "Update the Schedule Start Date of manual_user",
                time: "3 days ago"),
            const SizedBox(
              height: 10,
            ),
            activityItem(
                username: "Anh NN",
                type: " updated the Task Status of ",
                taskName: "Create Login Test Scenario",
                description: "Update the Task Status of create_login",
                time: "5 days ago"),
          ],
        ),
      ],
    );
  }

  dayActivity({
    required String title,
    required List<Widget> list,
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: Style(context).styleTextDataColumn,
        ),
        for (var item in list) item,
      ],
    );
  }

  activityItem({required String username, required String type, required String taskName, required String description, required String time}) {
    return Padding(
      padding: const EdgeInsets.only(left: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(
                username,
                style: TextStyle(
                  color: Colors.blue,
                  fontSize: context.fontSizeManager.fontSizeText14,
                ),
              ),
              Text(
                type,
                style: TextStyle(
                  color: const Color.fromRGBO(0, 0, 0, 0.8),
                  fontSize: context.fontSizeManager.fontSizeText14,
                ),
              ),
              Text(
                taskName,
                style: TextStyle(
                  color: Colors.blue,
                  fontSize: context.fontSizeManager.fontSizeText14,
                ),
              ),
            ],
          ),
          Text(
            time,
            style: TextStyle(
              color: const Color.fromRGBO(133, 135, 145, 1),
              fontSize: context.fontSizeManager.fontSizeText14,
            ),
          ),
          Text(
            description,
            style: TextStyle(
                color: const Color.fromRGBO(31, 31, 31, 0.8), fontSize: context.fontSizeManager.fontSizeText14, fontWeight: FontWeight.w600),
          ),
        ],
      ),
    );
  }

  priorityBreakdown(NavigationModel navigationModel) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MultiLanguageText(
          name: "priority_breakdown",
          defaultValue: "Priority Breakdown",
          isLowerCase: false,
          style: Style(context).styleTitleDialog,
        ),
        const SizedBox(
          height: 5,
        ),
        Wrap(
          children: [
            MultiLanguageText(
              name: "view_priority_of_items",
              defaultValue:
                  "View the priority of items within your project. It's helpful to prioritize work so you can ensure that your team is focusing on the most important work first. To prioritize your work,",
              style: TextStyle(
                color: const Color.fromRGBO(0, 0, 0, 0.8),
                fontSize: context.fontSizeManager.fontSizeText14,
              ),
            ),
            GestureDetector(
              onTap: () {
                navigationModel.navigate("/work-mgt/project/${navigationModel.prjProjectId}/task-list");
              },
              child: MultiLanguageText(
                name: "go_to_list_view",
                defaultValue: " go to the list view.",
                style: TextStyle(
                  color: Colors.blueAccent,
                  fontSize: context.fontSizeManager.fontSizeText14,
                ),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        const PriorityChartWidget(),
      ],
    );
  }

  Column taskStatusOverview(NavigationModel navigationModel) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MultiLanguageText(
          name: "task_status_overview",
          defaultValue: "Task Status Overview",
          isLowerCase: false,
          style: Style(context).styleTitleDialog,
        ),
        const SizedBox(
          height: 5,
        ),
        Wrap(
          children: [
            MultiLanguageText(
              name: "view_your_project_overall",
              defaultValue:
                  "View your project's overall progress based on the task status in your workflow. To see your team's progress in more detail,",
              style: TextStyle(
                color: const Color.fromRGBO(0, 0, 0, 0.8),
                fontSize: context.fontSizeManager.fontSizeText14,
              ),
            ),
            GestureDetector(
              onTap: () {
                navigationModel.navigate("/work-mgt/project/${navigationModel.prjProjectId}/board");
              },
              child: MultiLanguageText(
                name: "go_to_board_view",
                defaultValue: " go to the board view.",
                style: TextStyle(
                  color: Colors.blueAccent,
                  fontSize: context.fontSizeManager.fontSizeText14,
                ),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 20,
        ),
        Row(
          children: [
            Container(
              padding: const EdgeInsets.all(20),
              alignment: Alignment.centerLeft,
              child: pie.PieChart(
                dataMap: const {
                  // for (var taskStatus in taskStatusList) taskStatus["title"]: taskStatus["taskListCount"] * 1.0 ?? 1.0,
                  "To do": 3,
                  "Open": 1,
                  "In Progress": 2,
                  "Done": 3,
                },
                animationDuration: const Duration(milliseconds: 1000),
                chartRadius: 300,
                chartType: pie.ChartType.ring,
                ringStrokeWidth: 70,
                legendOptions: const pie.LegendOptions(
                  showLegendsInRow: false,
                  legendPosition: pie.LegendPosition.right,
                  showLegends: true,
                  legendTextStyle: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                colorList: [for (var taskStatus in taskStatusList) HexColor.fromHex(taskStatus["statusColor"])],
                chartValuesOptions: const pie.ChartValuesOptions(
                  showChartValues: false,
                ),
              ),
            ),
          ],
        )
      ],
    );
  }

  overviewCards() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MultiLanguageText(
          name: "card_overview",
          defaultValue: "Card Overview",
          isLowerCase: false,
          style: Style(context).styleTitleDialog,
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          children: [
            overviewCardItem(icon: Icons.done, text: "3 done", color: Colors.greenAccent),
            const SizedBox(
              width: 10,
            ),
            overviewCardItem(icon: Icons.edit, text: "4 updated", color: Colors.blueAccent),
            const SizedBox(
              width: 10,
            ),
            overviewCardItem(icon: Icons.add, text: "9 created", color: Colors.purpleAccent),
            const SizedBox(
              width: 10,
            ),
            overviewCardItem(icon: Icons.calendar_month, text: "1 due", color: Colors.redAccent),
          ],
        ),
      ],
    );
  }

  overviewCardItem({required IconData icon, required String text, required Color color}) {
    return Expanded(
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 20),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: const [
            BoxShadow(
              color: Colors.grey,
              offset: Offset(1, 1), // Điều chỉnh vị trí đổ bóng (x, y)
              blurRadius: 2, // Điều chỉnh độ mờ của bóng
              spreadRadius: 0, // Điều chỉnh sự lan rộng của bóng
            ),
          ],
        ),
        child: Row(
          children: [
            Container(
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                color: color.withOpacity(0.3),
              ),
              child: Icon(
                icon,
                color: color,
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  text,
                  style: TextStyle(color: color, fontWeight: FontWeight.bold, fontSize: context.fontSizeManager.fontSizeText18),
                ),
                MultiLanguageText(
                  name: "in_the_last_7_days",
                  defaultValue: "in the last 7 days",
                  style: TextStyle(
                    color: const Color.fromRGBO(133, 135, 145, 1),
                    fontSize: context.fontSizeManager.fontSizeText14,
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  teamWorkload(NavigationModel navigationModel) {
    return ConstrainedBox(
      constraints: const BoxConstraints(maxWidth: 1200),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MultiLanguageText(
            name: "team_workload",
            defaultValue: "Team Workload",
            isLowerCase: false,
            style: Style(context).styleTitleDialog,
          ),
          const SizedBox(
            height: 5,
          ),
          Wrap(
            children: [
              MultiLanguageText(
                name: "view_your_team_current",
                defaultValue: "View your team's current workload at a glance. To re-assign tasks across your team,",
                style: TextStyle(
                  color: const Color.fromRGBO(0, 0, 0, 0.8),
                  fontSize: context.fontSizeManager.fontSizeText14,
                ),
              ),
              GestureDetector(
                onTap: () {
                  navigationModel.navigate("/work-mgt/project/${navigationModel.prjProjectId}/task-list");
                },
                child: MultiLanguageText(
                  name: "go_to_list_view",
                  defaultValue: "  go to the list view.",
                  style: TextStyle(
                    color: Colors.blueAccent,
                    fontSize: context.fontSizeManager.fontSizeText14,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Wrap(
            runSpacing: 20,
            children: [
              typesOfWorkRowItem(
                MultiLanguageText(name: "assignee", defaultValue: "Assignee", style: Style(context).styleTextDataColumn),
                MultiLanguageText(name: "work_distribution", defaultValue: "Work distribution", style: Style(context).styleTextDataColumn),
                Center(child: MultiLanguageText(name: "count", defaultValue: "Count", style: Style(context).styleTextDataColumn)),
              ),
              typesOfWorkRowItem(
                typeItem(icon: Icons.person, text: "Anh NN", color: Colors.blue),
                percentageItem(percentage: 34, color: Colors.blue),
                countItem(count: 3),
              ),
              typesOfWorkRowItem(
                typeItem(icon: Icons.person, text: "Linh BM", color: Colors.green),
                percentageItem(percentage: 22, color: Colors.green),
                countItem(count: 2),
              ),
              typesOfWorkRowItem(
                typeItem(icon: Icons.person, text: "Doanh TV", color: Colors.yellowAccent),
                percentageItem(percentage: 11, color: Colors.yellowAccent),
                countItem(count: 1),
              ),
              typesOfWorkRowItem(
                typeItem(icon: Icons.person, text: "Quynh DN", color: Colors.purpleAccent),
                percentageItem(percentage: 22, color: Colors.purpleAccent),
                countItem(count: 2),
              ),
              typesOfWorkRowItem(
                typeItem(icon: Icons.person, text: "Quân NM", color: Colors.orangeAccent),
                percentageItem(percentage: 11, color: Colors.orangeAccent),
                countItem(count: 1),
              ),
            ],
          )
        ],
      ),
    );
  }

  typesOfWork(NavigationModel navigationModel) {
    return ConstrainedBox(
      constraints: const BoxConstraints(maxWidth: 1200),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MultiLanguageText(
            name: "types_of_work",
            defaultValue: "Types of work",
            isLowerCase: false,
            style: Style(context).styleTitleDialog,
          ),
          const SizedBox(
            height: 5,
          ),
          Wrap(
            children: [
              MultiLanguageText(
                name: "view_breakdown_of_items",
                defaultValue: "View the breakdown of items by their type. For more details,",
                style: TextStyle(
                  color: const Color.fromRGBO(0, 0, 0, 0.8),
                  fontSize: context.fontSizeManager.fontSizeText14,
                ),
              ),
              GestureDetector(
                onTap: () {
                  navigationModel.navigate("/work-mgt/project/${navigationModel.prjProjectId}/task-list");
                },
                child: MultiLanguageText(
                  name: "go_to_list_view",
                  defaultValue: " go to the list view.",
                  style: TextStyle(
                    color: Colors.blueAccent,
                    fontSize: context.fontSizeManager.fontSizeText14,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Wrap(
            runSpacing: 20,
            children: [
              typesOfWorkRowItem(
                MultiLanguageText(name: "type", defaultValue: "Type", style: Style(context).styleTextDataColumn),
                MultiLanguageText(name: "percentage", defaultValue: "Percentage", style: Style(context).styleTextDataColumn),
                Center(child: MultiLanguageText(name: "count", defaultValue: "Count", style: Style(context).styleTextDataColumn)),
              ),
              typesOfWorkRowItem(
                typeItem(icon: Icons.done, text: "Task", color: Colors.blue),
                percentageItem(percentage: 45),
                countItem(count: 4),
              ),
              typesOfWorkRowItem(
                typeItem(icon: Icons.attach_money, text: "Story", color: Colors.green),
                percentageItem(percentage: 11),
                countItem(count: 1),
              ),
              typesOfWorkRowItem(
                typeItem(icon: Icons.local_pizza, text: "Systems engineering", color: Colors.orangeAccent),
                percentageItem(percentage: 22),
                countItem(count: 2),
              ),
              typesOfWorkRowItem(
                typeItem(icon: Icons.bolt, text: "Discovery", color: Colors.yellowAccent),
                percentageItem(percentage: 11),
                countItem(count: 1),
              ),
              typesOfWorkRowItem(
                typeItem(icon: Icons.flash_on, text: "Epic", color: Colors.purpleAccent),
                percentageItem(percentage: 11),
                countItem(count: 1),
              ),
            ],
          )
        ],
      ),
    );
  }

  typesOfWorkRowItem(Widget type, Widget percentage, Widget count) {
    return Row(
      children: [
        Expanded(
          flex: 4,
          child: type,
        ),
        Expanded(
          flex: 4,
          child: percentage,
        ),
        Expanded(
          flex: 2,
          child: count,
        ),
      ],
    );
  }

  typeItem({required IconData icon, required String text, required Color color}) {
    return Row(
      children: [
        Container(
          margin: const EdgeInsets.only(right: 10),
          padding: const EdgeInsets.all(5),
          decoration: BoxDecoration(color: color, borderRadius: BorderRadius.circular(8)),
          child: Icon(
            icon,
            color: Colors.white,
          ),
        ),
        Text(text)
      ],
    );
  }

  percentageItem({required int percentage, Color? color}) {
    return Row(
      children: [
        Expanded(
          child: Container(
            height: 15,
            decoration: BoxDecoration(
              color: Colors.grey[300],
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: FractionallySizedBox(
              alignment: Alignment.centerLeft,
              widthFactor: percentage / 100,
              child: Container(
                decoration: BoxDecoration(
                  color: color ?? Colors.grey[600],
                  borderRadius: BorderRadius.circular(10.0),
                ),
              ),
            ),
          ),
        ),
        const SizedBox(
          width: 10,
        ),
        SizedBox(width: 40, child: Text("$percentage %"))
      ],
    );
  }

  countItem({required int count}) {
    return Center(
      child: Text(
        "$count",
        style: TextStyle(
          fontSize: context.fontSizeManager.fontSizeText14,
          color: Colors.blue,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}
