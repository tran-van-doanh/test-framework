import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';

import '../../../type.dart';

class PriorityChartWidget extends StatefulWidget {
  const PriorityChartWidget({Key? key}) : super(key: key);

  @override
  State<PriorityChartWidget> createState() => _PriorityChartWidgetState();
}

class _PriorityChartWidgetState extends State<PriorityChartWidget> {
  late List<ChartData> chartData = [];
  TooltipBehavior? _tooltipBehavior;
  @override
  void initState() {
    _tooltipBehavior = TooltipBehavior(
        enable: true,
        animationDuration: 500,
        duration: 1000,
        color: Colors.transparent,
        // canShowMarker: false,
        builder: (dynamic data, dynamic point, dynamic series, int pointIndex, int seriesIndex) {
          return Container(
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: data.color,
            ),
            child: Text(
              '${data.x} : ${data.y}',
              style:
                  TextStyle(color: const Color.fromARGB(255, 0, 0, 0), fontSize: context.fontSizeManager.fontSizeText16, fontWeight: FontWeight.w700),
            ),
          );
        });
    super.initState();
    chartData = [
      ChartData(multiLanguageString(name: "none", defaultValue: "None", context: context), 1, Colors.grey),
      ChartData(multiLanguageString(name: "lowest", defaultValue: "Lowest", context: context), 1, const Color(0xFF35B7E9)),
      ChartData(multiLanguageString(name: "low", defaultValue: "Low", context: context), 3, const Color(0xFF48DB29)),
      ChartData(multiLanguageString(name: "medium", defaultValue: "Medium", context: context), 3, const Color(0xFFEADE05)),
      ChartData(multiLanguageString(name: "high", defaultValue: "High", context: context), 2, const Color(0xFFFE9901)),
      ChartData(multiLanguageString(name: "hightest", defaultValue: "Hightest", context: context), 2, const Color(0xFFE41E1E)),
      ChartData(multiLanguageString(name: "others", defaultValue: "Others", context: context), 2, Colors.blueGrey),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: const Color(0xffEEEFF4)),
        height: 300,
        child: _buildRoundedColumnChart());
  }

  SfCartesianChart _buildRoundedColumnChart() {
    return SfCartesianChart(
      primaryXAxis: const CategoryAxis(),
      series: _getRoundedColumnSeries(),
      tooltipBehavior: _tooltipBehavior,
    );
  }

  List<ColumnSeries<ChartData, String>> _getRoundedColumnSeries() {
    return <ColumnSeries<ChartData, String>>[
      ColumnSeries<ChartData, String>(
          animationDuration: 2000,
          width: 0.5,
          enableTooltip: true,
          dataSource: chartData,
          borderRadius: const BorderRadius.only(topRight: Radius.circular(5.0), topLeft: Radius.circular(5.0)),
          xValueMapper: (ChartData sales, _) => sales.x,
          yValueMapper: (ChartData sales, _) => sales.y,
          pointColorMapper: (ChartData data, _) => data.color),
    ];
  }
}
