import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';
import '../../../api.dart';
import '../../../common/dynamic_dropdown_button.dart';
import '../../../common/dynamic_table.dart';
import '../../../components/badge/badge_icon.dart';
import '../../../components/style.dart';
import '../../../components/toast.dart';
import '../../../model/model.dart';
import '../../../type.dart';
import '../../list_test/tests/test/dialog_option_test.dart';

class ManagerTestCaseWidget extends StatefulWidget {
  final Map<dynamic, dynamic>? testCase;
  final Function(Map<dynamic, dynamic>? testCase) onSave;
  const ManagerTestCaseWidget({Key? key, required this.testCase, required this.onSave}) : super(key: key);

  @override
  State<ManagerTestCaseWidget> createState() => _ManagerTestCaseWidgetState();
}

class _ManagerTestCaseWidgetState extends State<ManagerTestCaseWidget> {
  final TextEditingController codeController = TextEditingController();
  final TextEditingController nameController = TextEditingController();
  SingleValueDropDownController statusController = SingleValueDropDownController();
  List resultListTestCase = [];
  var rowCountListTestCase = 0;
  var currentPage = 1;
  var rowPerPage = 10;
  var prjProjectId = "";
  bool isShowFilterForm = true;
  Map<dynamic, dynamic>? testCase = {};
  @override
  void initState() {
    prjProjectId = Provider.of<NavigationModel>(context, listen: false).prjProjectId ?? "";
    super.initState();
    setInitValue();
  }

  setInitValue() async {
    if (widget.testCase != null) {
      testCase = Map.from(widget.testCase!);
    }
    await handleSearchTestCase(currentPage);
  }

  handleSearchTestCase(page) async {
    var findRequest = {
      "queryOffset": (page - 1) * rowPerPage,
      "queryLimit": rowPerPage,
      "prjProjectId": prjProjectId,
      "testCaseType": "test_scenario"
    };
    findRequest["titleLike"] = nameController.text.isNotEmpty ? "%${nameController.text}%" : null;
    findRequest["nameLike"] = codeController.text.isNotEmpty ? "%${codeController.text}%" : null;
    findRequest["status"] = statusController.dropDownValue?.value;
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-case/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPage = page;
        rowCountListTestCase = response["body"]["rowCount"];
        resultListTestCase = response["body"]["resultList"];
      });
    }
    return 0;
  }

  handleAddTfDirectoryTestCase(result, String type) async {
    var response = await httpPut("/test-framework-api/user/test-framework/tf-test-case", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        Navigator.of(context).pop();
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        await handleSearchTestCase(currentPage);
      }
    }
  }

  @override
  void dispose() {
    super.dispose();
    nameController.dispose();
    codeController.dispose();
    statusController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return TestFrameworkRootPageWidget(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(50, 30, 50, 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      headerWidget(context),
                      const SizedBox(
                        height: 20,
                      ),
                      filterFormWidget(),
                      const SizedBox(
                        height: 10,
                      ),
                      tableTestCaseWidget("List Test Scenario Search", resultListTestCase),
                      const SizedBox(
                        width: 10,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            btnWidget(context),
          ],
        ),
      );
    });
  }

  Column tableTestCaseWidget(String label, List list) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: Style(context).styleTitleDialog,
        ),
        CustomDataTableWidget(
          minWidth: 500,
          columns: [
            Expanded(
              child: MultiLanguageText(
                name: "code",
                defaultValue: "Code",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "name",
                defaultValue: "Name",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: Center(
                child: MultiLanguageText(
                  name: "status",
                  defaultValue: "Status",
                  isLowerCase: false,
                  style: Style(context).styleTextDataColumn,
                ),
              ),
            ),
            Container(
              width: 56,
            ),
          ],
          rows: [
            for (var item in list)
              CustomDataRow(
                cells: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        item["name"],
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        item["title"],
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Center(
                        child: Badge3StatusWidget(
                          status: item["status"],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 56,
                    height: 35,
                    child: !(testCase?["id"] == item["id"])
                        ? ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.white,
                              foregroundColor: Colors.grey,
                              side: const BorderSide(color: Colors.grey),
                            ),
                            onPressed: () {
                              setState(() {
                                testCase = item;
                              });
                            },
                            child: const Icon(Icons.radio_button_unchecked),
                          )
                        : ElevatedButton(
                            onPressed: () {
                              setState(() {
                                testCase = null;
                              });
                            },
                            child: const Icon(Icons.radio_button_checked),
                          ),
                  ),
                ],
              ),
          ],
        ),
        if (label == "List Test Scenario Search")
          DynamicTablePagging(
            rowCountListTestCase,
            currentPage,
            rowPerPage,
            pageChangeHandler: (page) {
              handleSearchTestCase(page);
            },
            rowPerPageChangeHandler: (rowPerPage) {
              setState(() {
                rowPerPage = rowPerPage!;
                handleSearchTestCase(1);
              });
            },
          ),
      ],
    );
  }

  Column filterFormWidget() {
    return Column(
      children: [
        Row(
          children: [
            MultiLanguageText(
              name: "filter_form",
              defaultValue: "Filter form",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: const Color.fromRGBO(0, 0, 0, 0.8),
                  fontSize: context.fontSizeManager.fontSizeText16,
                  letterSpacing: 2),
            ),
            IconButton(
              onPressed: () {
                setState(() {
                  isShowFilterForm = !isShowFilterForm;
                });
              },
              icon: isShowFilterForm ? const Icon(Icons.visibility) : const Icon(Icons.visibility_off),
              splashRadius: 1,
              tooltip: isShowFilterForm
                  ? multiLanguageString(name: "hide_filter_form", defaultValue: "Hide filter form", context: context)
                  : multiLanguageString(name: "show_filter_form", defaultValue: "Show filter form", context: context),
            )
          ],
        ),
        if (isShowFilterForm)
          Row(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: SizedBox(
                    height: 50,
                    child: TextField(
                      controller: codeController,
                      decoration: InputDecoration(
                        label: const MultiLanguageText(name: "code", defaultValue: "Code"),
                        contentPadding: const EdgeInsets.fromLTRB(10, 0, 10, 20),
                        border: const OutlineInputBorder(),
                        hintText: multiLanguageString(name: "by_code", defaultValue: "By code", context: context),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: SizedBox(
                    height: 50,
                    child: TextField(
                      controller: nameController,
                      decoration: InputDecoration(
                        label: const MultiLanguageText(name: "name", defaultValue: "Name"),
                        contentPadding: const EdgeInsets.fromLTRB(10, 0, 10, 20),
                        border: const OutlineInputBorder(),
                        hintText: multiLanguageString(name: "by_name", defaultValue: "By name", context: context),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: DynamicDropdownSearchClearOption(
                    labelText: multiLanguageString(name: "status", defaultValue: "Status", context: context),
                    controller: statusController,
                    hint: multiLanguageString(name: "by_status", defaultValue: "By Status", context: context),
                    items: {
                      multiLanguageString(name: "draft", defaultValue: "Draft", context: context): 0,
                      multiLanguageString(name: "review", defaultValue: "Review", context: context): 2,
                      multiLanguageString(name: "active", defaultValue: "Active", context: context): 1
                    }.entries.map<DropDownValueModel>((result) {
                      return DropDownValueModel(
                        value: result.value,
                        name: result.key,
                      );
                    }).toList(),
                    maxHeight: 200,
                  ),
                ),
              ),
            ],
          ),
        if (isShowFilterForm)
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: ElevatedButton(
                    onPressed: () {
                      handleSearchTestCase(currentPage);
                    },
                    child: const MultiLanguageText(name: "search", defaultValue: "Search", isLowerCase: false)),
              ),
              ElevatedButton(
                  onPressed: () {
                    setState(() {
                      codeController.clear();
                      nameController.clear();
                      handleSearchTestCase(currentPage);
                    });
                  },
                  child: const MultiLanguageText(name: "reset", defaultValue: "Reset", isLowerCase: false))
            ],
          ),
      ],
    );
  }

  Row headerWidget(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        MultiLanguageText(
          name: "test_scenario",
          defaultValue: "Test Scenario",
          style: Style(context).styleTitleDialog,
        ),
        TextButton(
          onPressed: () => Navigator.pop(context, 'cancel'),
          child: const Icon(
            Icons.close,
            color: Colors.black,
          ),
        ),
      ],
    );
  }

  Padding btnWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: 40,
            width: 140,
            child: ElevatedButton(
              onPressed: () {
                Navigator.of(context).push(
                  createRoute(
                    DialogOptionTestWidget(
                      testCaseType: "test_scenario",
                      type: "create",
                      title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                      callbackOptionTest: (result) async {
                        handleAddTfDirectoryTestCase(result, "test_scenario");
                      },
                      selectedItem: const {},
                    ),
                  ),
                );
              },
              style: ElevatedButton.styleFrom(
                side: const BorderSide(color: Colors.green),
                backgroundColor: Colors.green[300],
                foregroundColor: Colors.white,
              ),
              child: const MultiLanguageText(
                name: "add_test_scenario",
                defaultValue: "Add Test Scenario",
                isLowerCase: false,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 10),
            height: 40,
            width: 80,
            child: ElevatedButton(
              onPressed: () {
                widget.onSave(testCase);
                Navigator.pop(context);
              },
              child: const MultiLanguageText(
                name: "ok",
                defaultValue: "Ok",
                isLowerCase: false,
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 10),
            height: 40,
            width: 120,
            child: ElevatedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              style: ElevatedButton.styleFrom(
                side: const BorderSide(color: Color.fromRGBO(209, 212, 216, 1)),
                backgroundColor: Colors.white,
                foregroundColor: const Color.fromRGBO(61, 90, 254, 1),
              ),
              child: const MultiLanguageText(
                name: "cancel",
                defaultValue: "Cancel",
                isLowerCase: false,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          )
        ],
      ),
    );
  }
}
