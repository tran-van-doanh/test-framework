import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';
import '../../../api.dart';
import '../../../common/dynamic_table.dart';
import '../../../components/style.dart';
import '../../../model/model.dart';

class ManageAssignToWidget extends StatefulWidget {
  final String? sysUserId;
  final Function(dynamic sysUser) onSave;
  const ManageAssignToWidget({Key? key, required this.sysUserId, required this.onSave}) : super(key: key);

  @override
  State<ManageAssignToWidget> createState() => _ManageAssignToWidgetState();
}

class _ManageAssignToWidgetState extends State<ManageAssignToWidget> {
  final TextEditingController fullnameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  late Future futureListUser;
  List resultListUser = [];
  var rowCountListUser = 0;
  var currentPage = 1;
  var rowPerPage = 10;
  var prjProjectId = "";
  bool isShowFilterForm = true;
  String? sysUserId;
  @override
  void initState() {
    prjProjectId = Provider.of<NavigationModel>(context, listen: false).prjProjectId ?? "";
    super.initState();
    setInitValue();
  }

  setInitValue() async {
    if (widget.sysUserId != null) sysUserId = widget.sysUserId;
    await handleSearchUser(currentPage);
  }

  handleSearchUser(page) async {
    var findRequest = {
      "queryOffset": (page - 1) * rowPerPage,
      "queryLimit": rowPerPage,
      "prjProjectId": prjProjectId,
    };
    findRequest["fullnameLike"] = fullnameController.text.isNotEmpty ? "%${fullnameController.text}%" : null;
    findRequest["emailLike"] = emailController.text.isNotEmpty ? "%${emailController.text}%" : null;
    var response = await httpPost("/test-framework-api/user/sys/sys-user-project/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPage = page;
        rowCountListUser = response["body"]["rowCount"];
        resultListUser = response["body"]["resultList"];
      });
    }
    return 0;
  }

  @override
  void dispose() {
    super.dispose();
    fullnameController.dispose();
    emailController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return TestFrameworkRootPageWidget(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(50, 30, 50, 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      headerWidget(context),
                      const SizedBox(
                        height: 20,
                      ),
                      filterFormWidget(),
                      const SizedBox(
                        height: 10,
                      ),
                      tableUserWidget(multiLanguageString(name: "list_user_search", defaultValue: "List User Search", context: context),
                          "List User Search", resultListUser),
                      const SizedBox(
                        width: 10,
                      ),
                      // tableUserWidget("List User Task", user, resultListUser)
                    ],
                  ),
                ),
              ),
            ),
            btnWidget(context),
          ],
        ),
      );
    });
  }

  Column tableUserWidget(String title, String label, List list) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: Style(context).styleTitleDialog,
        ),
        CustomDataTableWidget(
          minWidth: 500,
          columns: [
            Expanded(
              child: MultiLanguageText(
                name: "fullname",
                defaultValue: "Fullname",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "nick_name",
                defaultValue: "Nick Name",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "email",
                defaultValue: "Email",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Container(
              width: 56,
            ),
          ],
          rows: [
            for (var item in list)
              CustomDataRow(
                cells: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        item["sysUser"]["fullname"],
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        item["sysUser"]["nickname"],
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        item["sysUser"]["email"],
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 56,
                    height: 35,
                    child: !(sysUserId == item["sysUserId"])
                        ? ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.white,
                              foregroundColor: Colors.grey,
                              side: const BorderSide(color: Colors.grey),
                            ),
                            onPressed: () {
                              setState(() {
                                sysUserId = item["sysUserId"];
                              });
                            },
                            child: const Icon(Icons.radio_button_unchecked),
                          )
                        : ElevatedButton(
                            onPressed: () {
                              setState(() {
                                sysUserId = null;
                              });
                            },
                            child: const Icon(Icons.radio_button_checked),
                          ),
                  ),
                ],
              ),
          ],
        ),
        DynamicTablePagging(
          rowCountListUser,
          currentPage,
          rowPerPage,
          pageChangeHandler: (page) {
            handleSearchUser(page);
          },
          rowPerPageChangeHandler: (rowPerPage) {
            setState(() {
              rowPerPage = rowPerPage!;
              handleSearchUser(1);
            });
          },
        ),
      ],
    );
  }

  Column filterFormWidget() {
    return Column(
      children: [
        Row(
          children: [
            MultiLanguageText(
              name: "filter_form",
              defaultValue: "Filter form",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: const Color.fromRGBO(0, 0, 0, 0.8),
                  fontSize: context.fontSizeManager.fontSizeText16,
                  letterSpacing: 2),
            ),
            IconButton(
              onPressed: () {
                setState(() {
                  isShowFilterForm = !isShowFilterForm;
                });
              },
              icon: isShowFilterForm ? const Icon(Icons.visibility) : const Icon(Icons.visibility_off),
              splashRadius: 1,
              tooltip: isShowFilterForm
                  ? multiLanguageString(name: "hide_filter_form", defaultValue: "Hide filter form", context: context)
                  : multiLanguageString(name: "show_filter_form", defaultValue: "Show filter form", context: context),
            )
          ],
        ),
        if (isShowFilterForm)
          Row(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: SizedBox(
                    height: 50,
                    child: TextField(
                      controller: fullnameController,
                      decoration: InputDecoration(
                        label: const MultiLanguageText(name: "fullname", defaultValue: "Fullname"),
                        contentPadding: const EdgeInsets.fromLTRB(10, 0, 10, 20),
                        border: const OutlineInputBorder(),
                        hintText: multiLanguageString(name: "by_fullname", defaultValue: "By fullname", context: context),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: SizedBox(
                    height: 50,
                    child: TextField(
                      controller: emailController,
                      decoration: InputDecoration(
                        label: const MultiLanguageText(name: "email", defaultValue: "Email"),
                        contentPadding: const EdgeInsets.fromLTRB(10, 0, 10, 20),
                        border: const OutlineInputBorder(),
                        hintText: multiLanguageString(name: "by_email", defaultValue: "By email", context: context),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        if (isShowFilterForm)
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: ElevatedButton(
                    onPressed: () {
                      handleSearchUser(currentPage);
                    },
                    child: const MultiLanguageText(name: "search", defaultValue: "Search", isLowerCase: false)),
              ),
              ElevatedButton(
                  onPressed: () {
                    setState(() {
                      fullnameController.clear();
                      emailController.clear();
                      handleSearchUser(currentPage);
                    });
                  },
                  child: const MultiLanguageText(name: "reset", defaultValue: "Reset", isLowerCase: false))
            ],
          ),
      ],
    );
  }

  Row headerWidget(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        MultiLanguageText(
          name: "user",
          defaultValue: "User",
          style: Style(context).styleTitleDialog,
        ),
        TextButton(
          onPressed: () => Navigator.pop(context, 'cancel'),
          child: const Icon(
            Icons.close,
            color: Colors.black,
          ),
        ),
      ],
    );
  }

  Padding btnWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: 40,
            width: 80,
            child: ElevatedButton(
              onPressed: () {
                widget.onSave(sysUserId);
                Navigator.pop(context);
              },
              child: const MultiLanguageText(
                name: "ok",
                defaultValue: "Ok",
                isLowerCase: false,
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 10),
            height: 40,
            width: 120,
            child: ElevatedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              style: ElevatedButton.styleFrom(
                side: const BorderSide(color: Color.fromRGBO(209, 212, 216, 1)),
                backgroundColor: Colors.white,
                foregroundColor: const Color.fromRGBO(61, 90, 254, 1),
              ),
              child: const MultiLanguageText(
                name: "cancel",
                defaultValue: "Cancel",
                isLowerCase: false,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          )
        ],
      ),
    );
  }
}
