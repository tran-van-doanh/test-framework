import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'dart:async';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/components/dialog/dialog_delete.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/type.dart';
import '../../../../api.dart';
import '../../../../model/model.dart';

import '../../../common/custom_state.dart';
import '../board/header_agile_tasks_widget.dart';
import 'option_task_widget.dart';

class TaskListBodyWidget extends StatefulWidget {
  const TaskListBodyWidget({Key? key}) : super(key: key);

  @override
  State<TaskListBodyWidget> createState() => _TaskListBodyWidgetState();
}

class _TaskListBodyWidgetState extends CustomState<TaskListBodyWidget> {
  late Future futureListTaskList;
  var resultListTaskList = [];
  var rowCountListTaskList = 0;
  var currentPage = 1;
  var rowPerPage = 10;
  var searchRequest = {};
  var prjProjectId = "";

  @override
  void initState() {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    prjProjectId = navigationModel.prjProjectId ?? "";
    search({"parentIdIsNull": true});
    super.initState();
  }

  search(searchRequest) {
    this.searchRequest = searchRequest;
    futureListTaskList = getListTaskList(currentPage);
  }

  Future getListTaskList(page) async {
    if ((page - 1) * rowPerPage > rowCountListTaskList) {
      page = (1.0 * rowCountListTaskList / rowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {
      "queryOffset": (page - 1) * rowPerPage,
      "queryLimit": rowPerPage,
      "prjProjectId": prjProjectId,
    };
    if (searchRequest["parentIdIsNull"] != null) {
      findRequest["parentIdIsNull"] = searchRequest["parentIdIsNull"];
    }
    if (searchRequest["titleLike"] != null && searchRequest["titleLike"].isNotEmpty) {
      findRequest["titleLike"] = "%${searchRequest["titleLike"]}%";
    }
    if (searchRequest["status"] != null) {
      findRequest["status"] = searchRequest["status"];
    }
    var response = await httpPost("/test-framework-api/user/project/prj-task/search", findRequest, context);

    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPage = page;
        rowCountListTaskList = response["body"]["rowCount"];
        resultListTaskList = response["body"]["resultList"];
      });
    }
    return 0;
  }

  handleAddTaskList(result) async {
    var response = await httpPut("/test-framework-api/user/project/prj-task", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await getListTaskList(currentPage);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return FutureBuilder(
        future: futureListTaskList,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Column(
              children: [
                Expanded(
                  child: ListView(
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
                        child: Column(
                          children: [
                            HeaderAgileTasksWidget(
                              type: multiLanguageString(name: "task_list", defaultValue: "Task List", context: context),
                              countList: rowCountListTaskList,
                              callbackFilter: (result) {
                                if (result.isEmpty) {
                                  result = {"parentIdIsNull": true};
                                }
                                search(result);
                              },
                            ),
                            const SizedBox(
                              height: 30,
                            ),
                            if (resultListTaskList.isNotEmpty) tableWidget(context),
                            SelectionArea(
                              child: DynamicTablePagging(
                                rowCountListTaskList,
                                currentPage,
                                rowPerPage,
                                pageChangeHandler: (page) {
                                  setState(() {
                                    futureListTaskList = getListTaskList(page);
                                  });
                                },
                                rowPerPageChangeHandler: (rowPerPage) {
                                  setState(() {
                                    this.rowPerPage = rowPerPage!;
                                    futureListTaskList = getListTaskList(1);
                                  });
                                },
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                btnWidget(context),
              ],
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        },
      );
    });
  }

  Padding btnWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 40, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            margin: const EdgeInsets.only(right: 10),
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              onPressed: () {
                Navigator.of(context).push(
                  createRoute(
                    OptionTaskWidget(
                      type: "create",
                      title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                      callbackOptionTest: (result) {
                        handleAddTaskList(result);
                      },
                    ),
                  ),
                );
              },
              icon: const Icon(Icons.note_add, color: Color.fromRGBO(49, 49, 49, 1)),
              label: MultiLanguageText(
                name: "new_task",
                defaultValue: "New Task",
                isLowerCase: false,
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
            ),
          ),
        ],
      ),
    );
  }

  SelectionArea tableWidget(BuildContext context) {
    return SelectionArea(
      child: Column(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.symmetric(vertical: 17.5, horizontal: 5),
            decoration: BoxDecoration(
              border: Border.all(color: const Color.fromARGB(255, 207, 205, 205)),
            ),
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      flex: 2,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: MultiLanguageText(name: "name", defaultValue: "Name", isLowerCase: false, style: Style(context).styleTextDataColumn),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: MultiLanguageText(
                            name: "assigned_to", defaultValue: "Assigned To", isLowerCase: false, style: Style(context).styleTextDataColumn),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        alignment: Alignment.center,
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: MultiLanguageText(
                            name: "start_date", defaultValue: "Start date", isLowerCase: false, style: Style(context).styleTextDataColumn),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        alignment: Alignment.center,
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: MultiLanguageText(
                            name: "end_date", defaultValue: "End date", isLowerCase: false, style: Style(context).styleTextDataColumn),
                      ),
                    ),
                    Container(
                      width: 24,
                    ),
                  ],
                ),
              ],
            ),
          ),
          for (var task in resultListTaskList)
            RowTaskItemWidget(
              task: task,
              generation: 1,
              handleAddTaskList: handleAddTaskList,
              callback: () => getListTaskList(currentPage),
            ),
        ],
      ),
    );
  }
}

class RowTaskItemWidget extends StatefulWidget {
  final dynamic task;
  final int generation;
  final VoidCallback callback;
  final Function handleAddTaskList;
  const RowTaskItemWidget({Key? key, required this.task, required this.generation, required this.handleAddTaskList, required this.callback})
      : super(key: key);

  @override
  State<RowTaskItemWidget> createState() => _RowTaskItemWidgetState();
}

class _RowTaskItemWidgetState extends State<RowTaskItemWidget> {
  var resultListTaskList = [];
  getListTasks() async {
    var findRequest = {"parentId": widget.task["id"]};
    var response = await httpPost("/test-framework-api/user/project/prj-task/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        resultListTaskList = response["body"]["resultList"];
      });
    }
  }

  handleDeleteTaskList(id) async {
    var response = await httpDelete("/test-framework-api/user/project/prj-task/$id", context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        widget.callback();
      }
    }
  }

  handleEditTaskList(result, id) async {
    var response = await httpPost("/test-framework-api/user/project/prj-task/$id", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        widget.callback();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            Navigator.of(context).push(
              createRoute(
                OptionTaskWidget(
                  type: 'edit',
                  title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                  id: widget.task["id"],
                  callbackOptionTest: (result) => handleEditTaskList(result, widget.task["id"]),
                ),
              ),
            );
          },
          child: Container(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
            decoration: BoxDecoration(
              border: Border.all(color: const Color.fromARGB(255, 207, 205, 205)),
            ),
            child: Row(
              children: [
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.only(right: 5),
                    child: Row(
                      children: [
                        SizedBox(
                          width: (widget.generation - 1) * 20,
                        ),
                        GestureDetector(
                          onTap: () async {
                            setState(() {
                              widget.task["isSelected"] = !(widget.task["isSelected"] ?? false);
                            });
                            if (widget.task["isSelected"]) {
                              await getListTasks();
                            }
                          },
                          child: Icon(
                            (widget.task["isSelected"] ?? false) ? Icons.keyboard_arrow_down : Icons.keyboard_arrow_right,
                            color: const Color.fromRGBO(49, 49, 49, 1),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            widget.task["title"] ?? "",
                            softWrap: true,
                            style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: widget.task["assigneeSysUser"] != null
                        ? Row(
                            children: [
                              const Icon(Icons.account_circle, color: Color.fromRGBO(137, 172, 238, 1)),
                              const SizedBox(
                                width: 5,
                              ),
                              Expanded(
                                child: Text(
                                  widget.task["assigneeSysUser"]["fullname"] ?? "",
                                  style: TextStyle(
                                      fontSize: context.fontSizeManager.fontSizeText16,
                                      color: const Color.fromRGBO(90, 143, 241, 1),
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          )
                        : const SizedBox.shrink(),
                  ),
                ),
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Text(
                      widget.task["scheduledStartDate"] != null
                          ? DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(widget.task["scheduledStartDate"]).toLocal())
                          : "",
                      style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Text(
                      widget.task["scheduledEndDate"] != null
                          ? DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(widget.task["scheduledEndDate"]).toLocal())
                          : "",
                      style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                    ),
                  ),
                ),
                PopupMenuButton(
                  offset: const Offset(2, 30),
                  tooltip: '',
                  splashRadius: 10,
                  child: const Icon(
                    Icons.more_vert,
                  ),
                  itemBuilder: (context) => [
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.of(context).push(
                            createRoute(
                              OptionTaskWidget(
                                type: "create",
                                title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                                parentId: widget.task["id"],
                                callbackOptionTest: (result) {
                                  widget.handleAddTaskList(result);
                                },
                                // callbackOptionTest: (result) => handleAddTaskList(result, widget.task["id"]),
                              ),
                            ),
                          );
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "new_task", defaultValue: "New task"),
                        leading: const Icon(Icons.add, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.of(context).push(
                            createRoute(
                              OptionTaskWidget(
                                type: 'edit',
                                title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                id: widget.task["id"],
                                callbackOptionTest: (result) => handleEditTaskList(result, widget.task["id"]),
                              ),
                            ),
                          );
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                        leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          dialogDeleteTaskList(widget.task);
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "delete", defaultValue: "Delete"),
                        leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        if (widget.task["isSelected"] ?? false)
          for (var task in resultListTaskList)
            RowTaskItemWidget(
              task: task,
              generation: widget.generation + 1,
              handleAddTaskList: widget.handleAddTaskList,
              callback: () => widget.callback(),
            )
      ],
    );
  }

  dialogDeleteTaskList(selectItem) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogDelete(
          selectedItem: selectItem,
          callback: () {
            handleDeleteTaskList(selectItem["id"]);
          },
          type: 'TaskList',
        );
      },
    );
  }
}
