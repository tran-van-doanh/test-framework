import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';
import 'package:test_framework_view/page/work_mgt/task/manager_involve_to_widget.dart';
import '../../../api.dart';
import '../../../common/custom_state.dart';
import '../../../common/dynamic_dropdown_button.dart';
import '../../../components/toast.dart';
import '../../../model/model.dart';
import '../../../type.dart';
import '../item_list_option.dart';
import 'dialog_task_widget.dart';
import 'manage_assingn_to_widget.dart';
import 'manage_task_tag_widget.dart';

class OptionTaskWidget extends StatefulWidget {
  final String type;
  final String title;
  final String? id;
  final String? parentId;
  final Function? callbackOptionTest;
  const OptionTaskWidget({Key? key, required this.type, this.id, this.parentId, this.callbackOptionTest, required this.title}) : super(key: key);

  @override
  State<OptionTaskWidget> createState() => _OptionTaskWidgetState();
}

class _OptionTaskWidgetState extends CustomState<OptionTaskWidget> {
  late Future future;
  TextEditingController codeController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController desController = TextEditingController();
  TextEditingController scheduledStartDate = TextEditingController();
  TextEditingController scheduledEndDate = TextEditingController();
  TextEditingController actualStartDate = TextEditingController();
  TextEditingController actualEndDate = TextEditingController();
  String? parentId;
  int _status = 0;
  String? taskType;
  String? prjProjectId;
  final _formKey = GlobalKey<FormState>();
  // String priority = "Normal";
  String? assigneeSysUserId;
  List tagsList = [];
  List taskTypeList = [];
  List involvedList = [];
  // Map<dynamic, dynamic>? testCase = {};
  dynamic selectedItem;
  dynamic parentTask;
  dynamic assigneeSysUser;
  @override
  void initState() {
    prjProjectId = Provider.of<NavigationModel>(context, listen: false).prjProjectId;
    future = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    getTaskTypeList();
    if (widget.id != null) {
      await setInitValue();
      await getInvolvedList();
    } else {
      parentId = widget.parentId;
      if (parentId != null) {
        var result = await getSelectedItem(parentId!);
        setState(() {
          parentTask = result;
        });
      }
    }
  }

  getTaskTypeList() async {
    var response = await httpPost("/test-framework-api/user/project/prj-task-type/search", {}, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        taskTypeList = response["body"]["resultList"];
      });
    }
  }

  getInvolvedList() async {
    var findRequest = {
      "prjProjectId": prjProjectId,
      "prjTaskId": widget.id,
    };
    var response = await httpPost("/test-framework-api/user/project/prj-task-involved/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        involvedList = response["body"]["resultList"];
      });
    }
  }

  getSelectedItem(String id) async {
    var response = await httpGet("/test-framework-api/user/project/prj-task/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      return response["body"]["result"];
    }
  }

  getTag(id) async {
    var response = await httpGet("/test-framework-api/user/project/prj-task-tag/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        tagsList.add(response["body"]["result"]);
      });
    }
  }

  setInitValue() async {
    selectedItem = await getSelectedItem(widget.id!);
    if (selectedItem != null) {
      if (selectedItem["parentId"] != null) {
        var result = await getSelectedItem(selectedItem["parentId"]);
        setState(() {
          parentTask = result;
        });
      }
      setState(() {
        _status = selectedItem["status"] ?? 0;
        taskType = selectedItem["taskType"];
        codeController.text = selectedItem["name"] ?? "";
        nameController.text = selectedItem["title"] ?? "";
        desController.text = selectedItem["description"] ?? "";
        if (selectedItem["tagsList"] != null && selectedItem["tagsList"].isNotEmpty) {
          for (var tagId in selectedItem["tagsList"]) {
            getTag(tagId);
          }
        }
        if (selectedItem["scheduledStartDate"] != null) {
          scheduledStartDate.text =
              DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(selectedItem["scheduledStartDate"]).toLocal());
        }
        if (selectedItem["scheduledEndDate"] != null) {
          scheduledEndDate.text =
              DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(selectedItem["scheduledEndDate"]).toLocal());
        }
        if (selectedItem["actualStartDate"] != null) {
          actualStartDate.text = DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(selectedItem["actualStartDate"]).toLocal());
        }
        if (selectedItem["actualEndDate"] != null) {
          actualEndDate.text = DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(selectedItem["actualEndDate"]).toLocal());
        }
        assigneeSysUserId = selectedItem["assigneeSysUserId"];
        if (assigneeSysUserId != null) getAssigneeSysUser(assigneeSysUserId!);
      });
    }
  }

  getAssigneeSysUser(String id) async {
    var response = await httpGet("/test-framework-api/user/sys/sys-user/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        assigneeSysUser = response["body"]["result"];
      });
    }
  }

  onRemovedInvolved(id) async {
    var response = await httpDelete("/test-framework-api/user/project/prj-task-involved/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      if (response["body"].containsKey("errorMessage") && mounted) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      }
    }
  }

  onChangeInvolvedList(List removedInvolved, List<User> addedInvolved) async {
    final futures = <Future>[
      handleRemovedInvolved(removedInvolved),
      onAddedInvolved(addedInvolved),
    ];
    await Future.wait<dynamic>(futures);
  }

  handleRemovedInvolved(List removedInvolved) async {
    final futures = <Future>[
      for (var user in removedInvolved) onRemovedInvolved(user["id"]),
    ];
    final result = await Future.wait<dynamic>(futures);
    if (result.length == removedInvolved.length) {
      return 0;
    }
  }

  onAddedInvolved(List<User> addedInvolved) async {
    final futures = <Future>[
      for (var user in addedInvolved) onAddInvolved(user.id),
    ];
    final result = await Future.wait<dynamic>(futures);
    if (result.length == addedInvolved.length) {
      return 0;
    }
  }

  onAddInvolved(involvedSysUserId) async {
    var result = {
      "prjTaskId": widget.id,
      "involvedSysUserId": involvedSysUserId,
      "prjProjectId": prjProjectId,
    };
    var response = await httpPut("/test-framework-api/user/project/prj-task-involved", result, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      if (response["body"].containsKey("errorMessage") && mounted) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      }
    }
  }

  @override
  void dispose() {
    codeController.dispose();
    nameController.dispose();
    desController.dispose();
    scheduledStartDate.dispose();
    scheduledEndDate.dispose();
    actualStartDate.dispose();
    actualEndDate.dispose();
    super.dispose();
  }

  _selectDate(TextEditingController controller) async {
    final DateTime? datePicked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (datePicked != null) {
      setState(() {
        controller.text = DateFormat('dd-MM-yyyy').format(datePicked);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return TestFrameworkRootPageWidget(
        child: Column(
          children: [
            headerWidget(context),
            const Divider(color: Color.fromRGBO(216, 218, 229, 1)),
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(top: 20, bottom: 10),
                child: Form(
                  key: _formKey,
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                      child: FocusTraversalGroup(
                        policy: OrderedTraversalPolicy(),
                        child: bodyWidget(),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 20, top: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ButtonSave(onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      var result = {};
                      List<String> tagsListId = [];
                      for (var element in tagsList) {
                        tagsListId.add(element["id"]);
                      }
                      if (selectedItem != null) {
                        result = selectedItem;
                        result["name"] = codeController.text;
                        result["title"] = nameController.text;
                        result["description"] = desController.text;
                        if (scheduledStartDate.text.isNotEmpty) {
                          result["scheduledStartDate"] = "${DateFormat('dd-MM-yyyy').parse(scheduledStartDate.text).toIso8601String()}+07:00";
                        }
                        if (scheduledEndDate.text.isNotEmpty) {
                          result["scheduledEndDate"] = "${DateFormat('dd-MM-yyyy').parse(scheduledEndDate.text).toIso8601String()}+07:00";
                        }
                        if (actualStartDate.text.isNotEmpty) {
                          result["actualStartDate"] = "${DateFormat('dd-MM-yyyy').parse(actualStartDate.text).toIso8601String()}+07:00";
                        }
                        if (actualEndDate.text.isNotEmpty) {
                          result["actualEndDate"] = "${DateFormat('dd-MM-yyyy').parse(actualEndDate.text).toIso8601String()}+07:00";
                        }
                        result["assigneeSysUserId"] = assigneeSysUserId;
                        result["tagsList"] = tagsListId;
                        result["taskType"] = taskType;
                        result["status"] = _status;
                        if (parentTask != null) result["parentId"] = parentTask["id"];
                      } else {
                        result = {
                          "title": nameController.text,
                          "name": codeController.text,
                          "prjProjectId": prjProjectId,
                          "description": desController.text,
                          if (scheduledStartDate.text.isNotEmpty)
                            "scheduledStartDate": "${DateFormat('dd-MM-yyyy').parse(scheduledStartDate.text).toIso8601String()}+07:00",
                          if (scheduledEndDate.text.isNotEmpty)
                            "scheduledEndDate": "${DateFormat('dd-MM-yyyy').parse(scheduledEndDate.text).toIso8601String()}+07:00",
                          if (actualStartDate.text.isNotEmpty)
                            "actualStartDate": "${DateFormat('dd-MM-yyyy').parse(actualStartDate.text).toIso8601String()}+07:00",
                          if (actualEndDate.text.isNotEmpty)
                            "actualEndDate": "${DateFormat('dd-MM-yyyy').parse(actualEndDate.text).toIso8601String()}+07:00",
                          "assigneeSysUserId": assigneeSysUserId,
                          "tagsList": tagsListId,
                          "taskType": taskType,
                          if (parentTask != null) "parentId": parentTask["id"],
                          "status": _status,
                        };
                      }
                      widget.callbackOptionTest!(result);
                    }
                  }),
                  const ButtonCancel()
                ],
              ),
            )
          ],
        ),
      );
    });
  }

  Column bodyWidget() {
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 10),
                    child: Row(
                      children: [
                        Expanded(
                          flex: 3,
                          child: MultiLanguageText(name: "parent_task", defaultValue: "Parent task", style: Style(context).styleTitleDialog),
                        ),
                        Expanded(
                          flex: 7,
                          child: Row(
                            children: [
                              Expanded(
                                child: DynamicTextField(
                                  controller: TextEditingController(
                                      text: parentTask != null
                                          ? parentTask["title"]
                                          : multiLanguageString(
                                              name: "click_to_choose_a_task", defaultValue: "Click to choose a task", context: context)),
                                  readOnly: true,
                                  onTap: () {
                                    Navigator.of(context).push(
                                      createRoute(
                                        DialogTaskWidget(
                                          taskType: "group",
                                          prjProjectId: prjProjectId!,
                                          callbackSelected: (taskId) async {
                                            var result = await getSelectedItem(taskId);
                                            setState(() {
                                              parentTask = result;
                                            });
                                          },
                                        ),
                                      ),
                                    );
                                  },
                                  suffixIcon: parentTask != null
                                      ? GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              parentTask = null;
                                            });
                                          },
                                          child: const Icon(Icons.close),
                                        )
                                      : null,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      children: [
                        Expanded(
                          flex: 3,
                          child: MultiLanguageText(name: "task_code", defaultValue: "Task Code", style: Style(context).styleTitleDialog),
                        ),
                        Expanded(
                          flex: 7,
                          child: DynamicTextField(
                            controller: codeController,
                            hintText: "...",
                            isRequiredNotEmpty: true,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      children: [
                        Expanded(
                          flex: 3,
                          child: MultiLanguageText(
                              name: "schedule_start_date", defaultValue: "Schedule Start Date", style: Style(context).styleTitleDialog),
                        ),
                        Expanded(
                          flex: 7,
                          child: DynamicTextField(
                            controller: scheduledStartDate,
                            hintText: multiLanguageString(name: "start_date", defaultValue: "Start date", context: context),
                            suffixIcon: (scheduledStartDate.text != "")
                                ? IconButton(
                                    onPressed: () {
                                      scheduledStartDate.clear();
                                    },
                                    icon: const Icon(Icons.close),
                                    splashRadius: 20,
                                  )
                                : const Icon(Icons.calendar_today),
                            labelText: multiLanguageString(name: "start_date", defaultValue: "Start date", context: context),
                            readOnly: true,
                            onTap: () {
                              _selectDate(scheduledStartDate);
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      children: [
                        Expanded(
                          flex: 3,
                          child:
                              MultiLanguageText(name: "actual_start_date", defaultValue: "Actual Start Date", style: Style(context).styleTitleDialog),
                        ),
                        Expanded(
                          flex: 7,
                          child: DynamicTextField(
                            controller: actualStartDate,
                            hintText: multiLanguageString(name: "start_date", defaultValue: "Start date", context: context),
                            suffixIcon: (actualStartDate.text != "")
                                ? IconButton(
                                    onPressed: () {
                                      actualStartDate.clear();
                                    },
                                    icon: const Icon(Icons.close),
                                    splashRadius: 20,
                                  )
                                : const Icon(Icons.calendar_today),
                            labelText: multiLanguageString(name: "start_date", defaultValue: "Start date", context: context),
                            readOnly: true,
                            onTap: () {
                              _selectDate(actualStartDate);
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            MultiLanguageText(name: "tags", defaultValue: "Tags", style: Style(context).styleTitleDialog),
                            GestureDetector(
                              onTap: () {
                                Navigator.of(context).push(
                                  createRoute(
                                    ManageTaskTagWidget(
                                      tagsList: tagsList,
                                      onSave: (tagsList) {
                                        setState(() {
                                          this.tagsList = tagsList;
                                        });
                                      },
                                    ),
                                  ),
                                );
                              },
                              child: MultiLanguageText(
                                name: "click_to_manage_tags",
                                defaultValue: "Click To Manage Tags",
                                style: Style(context).styleTitleDialog,
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).push(
                              createRoute(
                                ManageTaskTagWidget(
                                  tagsList: tagsList,
                                  onSave: (tagsList) {
                                    setState(() {
                                      this.tagsList = tagsList;
                                    });
                                  },
                                ),
                              ),
                            );
                          },
                          child: Container(
                            alignment: Alignment.centerLeft,
                            constraints: const BoxConstraints(minHeight: 50),
                            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: const Color.fromRGBO(216, 218, 229, 1),
                              ),
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: Wrap(
                              children: [
                                for (var tag in tagsList)
                                  Container(
                                    margin: EdgeInsets.only(right: tagsList.last != tag ? 10 : 0, bottom: 5, top: 5),
                                    child: IntrinsicWidth(
                                      child: TagListAgileTaskWidget(
                                        tag: tag,
                                        onRemove: () {
                                          setState(() {
                                            tagsList.remove(tag);
                                          });
                                        },
                                        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                                      ),
                                    ),
                                  )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              width: 25,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 10),
                    child: Row(
                      children: [
                        Expanded(
                          flex: 3,
                          child: MultiLanguageText(name: "task_type", defaultValue: "Task type", style: Style(context).styleTitleDialog),
                        ),
                        Expanded(
                          flex: 7,
                          child: DynamicDropdownButton(
                              borderRadius: 5,
                              value: taskType,
                              hint: multiLanguageString(name: "choose_a_task_type", defaultValue: "Choose a task type", context: context),
                              items: taskTypeList.map<DropdownMenuItem<String>>((dynamic result) {
                                return DropdownMenuItem(
                                  value: result["id"],
                                  child: Text(result["title"]),
                                );
                              }).toList(),
                              onChanged: (newValue) {
                                setState(() {
                                  taskType = newValue!;
                                });
                              },
                              isRequiredNotEmpty: true),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      children: [
                        Expanded(
                          flex: 3,
                          child: MultiLanguageText(name: "task_name", defaultValue: "Task Name", style: Style(context).styleTitleDialog),
                        ),
                        Expanded(
                          flex: 7,
                          child: DynamicTextField(
                            controller: nameController,
                            hintText: "...",
                            isRequiredNotEmpty: true,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      children: [
                        Expanded(
                          flex: 3,
                          child:
                              MultiLanguageText(name: "schedule_end_date", defaultValue: "Schedule End Date", style: Style(context).styleTitleDialog),
                        ),
                        Expanded(
                          flex: 7,
                          child: DynamicTextField(
                            controller: scheduledEndDate,
                            hintText: multiLanguageString(name: "start_date", defaultValue: "Start date", context: context),
                            suffixIcon: (scheduledEndDate.text != "")
                                ? IconButton(
                                    onPressed: () {
                                      scheduledEndDate.clear();
                                    },
                                    icon: const Icon(Icons.close),
                                    splashRadius: 20,
                                  )
                                : const Icon(Icons.calendar_today),
                            labelText: multiLanguageString(name: "end_date", defaultValue: "End date", context: context),
                            readOnly: true,
                            onTap: () {
                              _selectDate(scheduledEndDate);
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      children: [
                        Expanded(
                          flex: 3,
                          child: MultiLanguageText(name: "actual_end_date", defaultValue: "Actual End Date", style: Style(context).styleTitleDialog),
                        ),
                        Expanded(
                          flex: 7,
                          child: DynamicTextField(
                            controller: actualEndDate,
                            hintText: multiLanguageString(name: "end_date", defaultValue: "End date", context: context),
                            suffixIcon: (actualEndDate.text != "")
                                ? IconButton(
                                    onPressed: () {
                                      actualEndDate.clear();
                                    },
                                    icon: const Icon(Icons.close),
                                    splashRadius: 20,
                                  )
                                : const Icon(Icons.calendar_today),
                            labelText: multiLanguageString(name: "end_date", defaultValue: "End date", context: context),
                            readOnly: true,
                            onTap: () {
                              _selectDate(actualEndDate);
                            },
                          ),
                        ),
                      ],
                    ),
                  ),

                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      children: [
                        Expanded(
                          flex: 3,
                          child: MultiLanguageText(name: "status", defaultValue: "Status", style: Style(context).styleTitleDialog),
                        ),
                        Expanded(
                          flex: 7,
                          child: DynamicDropdownButton(
                              borderRadius: 5,
                              value: _status,
                              hint: multiLanguageString(name: "choose_a_status", defaultValue: "Choose a status", context: context),
                              items: [
                                {"name": multiLanguageString(name: "active", defaultValue: "Active", context: context), "value": 1},
                                {"name": multiLanguageString(name: "inactive", defaultValue: "Inactive", context: context), "value": 0},
                              ].map<DropdownMenuItem<int>>((dynamic result) {
                                return DropdownMenuItem(
                                  value: result["value"],
                                  child: Text(result["name"]),
                                );
                              }).toList(),
                              onChanged: (newValue) {
                                setState(() {
                                  _status = newValue!;
                                });
                              },
                              isRequiredNotEmpty: true),
                        ),
                      ],
                    ),
                  ),

                  // Container(
                  //   margin: const EdgeInsets.symmetric(vertical: 10),
                  //   child: Row(
                  //     children: [
                  //       Expanded(
                  //         flex: 3,
                  //         child: Text("Priority", style: Style(context).styleTitleDialog),
                  //       ),
                  //       Expanded(
                  //         flex: 7,
                  //         child: DynamicDropdownButton(
                  //           borderRadius: 5,
                  //           value: priority,
                  //           hint: "Choose a priority",
                  //           items: const ["Critical", "High", "Normal", "Low"].map<DropdownMenuItem<String>>((String result) {
                  //             return DropdownMenuItem(
                  //               value: result,
                  //               child: Text(result),
                  //             );
                  //           }).toList(),
                  //           onChanged: (newValue) {
                  //             setState(() {
                  //               priority = newValue!;
                  //             });
                  //           },
                  //         ),
                  //       ),
                  //     ],
                  //   ),
                  // ),

                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            MultiLanguageText(name: "assign_to", defaultValue: "Assign to", style: Style(context).styleTitleDialog),
                            GestureDetector(
                              onTap: () {
                                Navigator.of(context).push(
                                  createRoute(
                                    ManageAssignToWidget(
                                      sysUserId: assigneeSysUserId,
                                      onSave: (assigneeSysUserId) async {
                                        setState(() {
                                          this.assigneeSysUserId = assigneeSysUserId;
                                        });
                                        await getAssigneeSysUser(assigneeSysUserId!);
                                      },
                                    ),
                                  ),
                                );
                              },
                              child: MultiLanguageText(
                                name: "click_to_manage_user",
                                defaultValue: "Click To Manage User",
                                style: Style(context).styleTitleDialog,
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).push(
                              createRoute(
                                ManageAssignToWidget(
                                  sysUserId: assigneeSysUserId,
                                  onSave: (assigneeSysUserId) async {
                                    setState(() {
                                      this.assigneeSysUserId = assigneeSysUserId;
                                    });
                                    await getAssigneeSysUser(assigneeSysUserId!);
                                  },
                                ),
                              ),
                            );
                          },
                          child: Container(
                            alignment: Alignment.centerLeft,
                            constraints: const BoxConstraints(minHeight: 50),
                            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: const Color.fromRGBO(216, 218, 229, 1),
                              ),
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: assigneeSysUser != null
                                ? IntrinsicWidth(
                                    child: UserListAgileTaskWidget(
                                      user: assigneeSysUser,
                                      onRemove: () {
                                        setState(() {
                                          assigneeSysUserId = null;
                                          assigneeSysUser = null;
                                        });
                                      },
                                    ),
                                  )
                                : null,
                          ),
                        )
                      ],
                    ),
                  ),
                  if (widget.type != "create")
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              MultiLanguageText(name: "involve_to", defaultValue: "Involve to", style: Style(context).styleTitleDialog),
                              GestureDetector(
                                onTap: () {
                                  Navigator.of(context).push(
                                    createRoute(
                                      ManageInvolveToWidget(
                                        involvedList: involvedList,
                                        prjTaskId: widget.id,
                                        onSave: (List removedInvolved, List<User> addedInvolved) async {
                                          await onChangeInvolvedList(removedInvolved, addedInvolved);
                                          getInvolvedList();
                                        },
                                      ),
                                    ),
                                  );
                                },
                                child: MultiLanguageText(
                                  name: "click_to_manage_involve",
                                  defaultValue: "Click To Manage Involve",
                                  style: Style(context).styleTitleDialog,
                                ),
                              )
                            ],
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.of(context).push(
                                createRoute(
                                  ManageInvolveToWidget(
                                    involvedList: involvedList,
                                    prjTaskId: widget.id,
                                    onSave: (List removedInvolved, List<User> addedInvolved) async {
                                      await onChangeInvolvedList(removedInvolved, addedInvolved);
                                      getInvolvedList();
                                    },
                                  ),
                                ),
                              );
                            },
                            child: Container(
                              alignment: Alignment.centerLeft,
                              constraints: const BoxConstraints(minHeight: 50),
                              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: const Color.fromRGBO(216, 218, 229, 1),
                                ),
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: Wrap(
                                children: [
                                  for (var involved in involvedList)
                                    Container(
                                      margin: EdgeInsets.only(right: involvedList.last != involved ? 10 : 0, bottom: 5, top: 5),
                                      child: IntrinsicWidth(
                                        child: UserListAgileTaskWidget(
                                          user: involved["involvedSysUser"],
                                          onRemove: () async {
                                            await onRemovedInvolved(involved["id"]);
                                            getInvolvedList();
                                          },
                                        ),
                                      ),
                                    )
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                ],
              ),
            ),
          ],
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            MultiLanguageText(name: "description", defaultValue: "Description", style: Style(context).styleTitleDialog),
            DynamicTextField(
              controller: desController,
              hintText: "...",
              maxline: 3,
            ),
          ],
        ),
      ],
    );
  }

  Padding headerWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 35, 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "type_task",
            defaultValue: "\$0 TASK",
            variables: [widget.title],
            isLowerCase: false,
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
            onPressed: () => Navigator.pop(context, 'cancel'),
            child: const Icon(
              Icons.close,
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }
}
