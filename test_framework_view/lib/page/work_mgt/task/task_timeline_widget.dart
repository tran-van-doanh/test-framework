import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/work_mgt/board/header_agile_tasks_widget.dart';
import '../../../api.dart';
import '../../../components/dialog/dialog_delete.dart';
import '../../../components/style.dart';
import '../../../components/toast.dart';
import '../../../type.dart';
import 'option_task_widget.dart';

class TaskTimelineBodyWidget extends StatefulWidget {
  const TaskTimelineBodyWidget({Key? key}) : super(key: key);

  @override
  State<TaskTimelineBodyWidget> createState() => _TaskTimelineBodyWidgetState();
}

class _TaskTimelineBodyWidgetState extends State<TaskTimelineBodyWidget> {
  late Future futureListTasks;
  var resultListTasks = [];
  var rowCountListTasks = 0;
  var currentPageTasks = 1;
  var rowPerPageTasks = 10;
  var searchRequest = {};
  String prjProjectId = "";
  DateTime? startDate;
  DateTime? endDate;
  @override
  void initState() {
    super.initState();
    prjProjectId = Provider.of<NavigationModel>(context, listen: false).prjProjectId ?? "";
    futureListTasks = search({"parentIdIsNull": true});
  }

  search(searchRequest) async {
    this.searchRequest = searchRequest;
    await getListTasks(currentPageTasks);
    return 0;
  }

  getListTasks(page) async {
    if ((page - 1) * rowPerPageTasks > rowCountListTasks) {
      page = (1.0 * rowCountListTasks / rowPerPageTasks).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {
      "queryOffset": (page - 1) * rowPerPageTasks,
      "queryLimit": rowPerPageTasks,
      "prjProjectId": prjProjectId,
    };
    if (searchRequest["titleLike"] != null && searchRequest["titleLike"].isNotEmpty) {
      findRequest["titleLike"] = "%${searchRequest["titleLike"]}%";
    }
    if (searchRequest["status"] != null) {
      findRequest["status"] = searchRequest["status"];
    }
    if (searchRequest["parentIdIsNull"] != null) {
      findRequest["parentIdIsNull"] = searchRequest["parentIdIsNull"];
    }
    var response = await httpPost("/test-framework-api/user/project/prj-task/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPageTasks = page;
        rowCountListTasks = response["body"]["rowCount"];
        resultListTasks = response["body"]["resultList"];
      });
      if (resultListTasks.isNotEmpty) {
        findStartDate(resultListTasks);
        findEndDate(resultListTasks);
        checkMinWeek(35);
      }
    }
    return 0;
  }

  checkMinWeek(int day) {
    if (endDate!.difference(startDate!).inDays < day) {
      setState(() {
        endDate = startDate!.add(Duration(days: day));
      });
    }
  }

  DateTime findFirstDateOfTheWeek(DateTime dateTime) {
    return dateTime.subtract(Duration(days: dateTime.weekday - 1));
  }

  DateTime findLastDateOfTheWeek(DateTime dateTime) {
    return dateTime.add(Duration(days: DateTime.daysPerWeek - dateTime.weekday));
  }

  findEndDate(List list) {
    setState(() {
      var result = list.reduce(
          (value, element) => DateTime.parse(element["scheduledEndDate"]).isAfter(DateTime.parse(value["scheduledEndDate"])) ? element : value);
      endDate = findLastDateOfTheWeek(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(result["scheduledEndDate"]).toLocal());
    });
  }

  findStartDate(List list) {
    setState(() {
      var result = list.reduce(
          (value, element) => DateTime.parse(element["scheduledStartDate"]).isBefore(DateTime.parse(value["scheduledStartDate"])) ? element : value);
      startDate = findFirstDateOfTheWeek(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(result["scheduledStartDate"]).toLocal());
    });
  }

  handleDeleteTasks(id) async {
    var response = await httpDelete("/test-framework-api/user/project/prj-task/$id", context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        await getListTasks(currentPageTasks);
      }
    }
  }

  handleAddTasks(result) async {
    var response = await httpPut("/test-framework-api/user/project/prj-task", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await getListTasks(currentPageTasks);
      }
    }
  }

  handleEditTasks(result, id) async {
    var response = await httpPost("/test-framework-api/user/project/prj-task/$id", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, LanguageModel>(
      builder: (context, navigationModel, languageModel, child) {
        return FutureBuilder(
          future: futureListTasks,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(50, 20, 50, 30),
                        child: Column(
                          children: [
                            HeaderAgileTasksWidget(
                              type: multiLanguageString(name: "timeline", defaultValue: "Timeline", context: context),
                              countList: rowCountListTasks,
                              callbackFilter: (result) {
                                if (result.isEmpty) {
                                  result = {"parentIdIsNull": true};
                                }
                                search(result);
                              },
                            ),
                            const SizedBox(
                              height: 30,
                            ),
                            if (resultListTasks.isNotEmpty) ganttChartWidget()
                          ],
                        ),
                      ),
                    ),
                  ),
                  btnWidget(context),
                ],
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  Padding btnWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 10, 50, 10),
      child: Align(
        alignment: Alignment.centerRight,
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: SizedBox(
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              onPressed: () {
                Navigator.of(context).push(
                  createRoute(
                    OptionTaskWidget(
                      type: 'create',
                      title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                      callbackOptionTest: (result) => handleAddTasks(result),
                    ),
                  ),
                );
              },
              icon: const Icon(Icons.note_add, color: Color.fromRGBO(49, 49, 49, 1)),
              label: MultiLanguageText(
                name: "new_tasks",
                defaultValue: "New Tasks",
                isLowerCase: false,
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
            ),
          ),
        ),
      ),
    );
  }

  Row ganttChartWidget() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ganttTaskName(),
        Expanded(
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: IntrinsicWidth(
              child: ganttTimeLine(),
            ),
          ),
        )
      ],
    );
  }

  Column ganttTaskName() {
    return Column(
      children: [
        Container(
          alignment: Alignment.centerLeft,
          padding: const EdgeInsets.symmetric(vertical: 17.5, horizontal: 5),
          decoration: BoxDecoration(
            border: Border.all(color: const Color.fromARGB(255, 207, 205, 205)),
          ),
          child: Column(
            children: [
              Row(
                children: [
                  Container(
                    width: 230,
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: MultiLanguageText(
                        name: "task_name", defaultValue: "Task Name", isLowerCase: false, style: Style(context).styleTextDataColumn),
                  ),
                  Container(
                    width: 110,
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Center(
                      child: MultiLanguageText(
                          name: "start_date", defaultValue: "Start date", isLowerCase: false, style: Style(context).styleTextDataColumn),
                    ),
                  ),
                  Container(
                    width: 110,
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Center(
                      child: MultiLanguageText(
                          name: "end_date", defaultValue: "End date", isLowerCase: false, style: Style(context).styleTextDataColumn),
                    ),
                  ),
                  Container(
                    width: 24,
                  ),
                ],
              ),
            ],
          ),
        ),
        for (var task in resultListTasks)
          RowGanttChartTitleWidget(
            task: task,
            generation: 1,
            callbackStartDateEndDate: (startDate, endDate) {
              setState(() {
                resultListTasks;
              });
              if (startDate.isBefore(this.startDate!) || task["isSelected"] == true) {
                setState(() {
                  this.startDate = findFirstDateOfTheWeek(startDate);
                });
              } else {
                findStartDate(resultListTasks);
              }
              if (endDate.isAfter(this.endDate!) || task["isSelected"] == true) {
                setState(() {
                  this.endDate = findLastDateOfTheWeek(endDate);
                });
              } else {
                findEndDate(resultListTasks);
              }
              checkMinWeek(35);
            },
            onEditTask: (taskCallback) async {
              await handleEditTasks(taskCallback, taskCallback["id"]);
              getListTasks(currentPageTasks);
            },
            onDeleteTask: () => dialogDeleteTasks(task),
          ),
      ],
    );
  }

  ganttTimeLine() {
    return Column(
      children: [
        if (startDate != null && endDate != null)
          Row(
            children: [
              for (DateTime i = startDate!; i.isBefore(endDate!); i = i.add(const Duration(days: 7)))
                Expanded(
                  child: Column(
                    children: [
                      Container(
                        padding: const EdgeInsets.all(3),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(border: Border.all(color: const Color.fromARGB(255, 207, 205, 205))),
                        child: Text("${DateFormat('dd/MM/yyyy').format(i)} - ${DateFormat('dd/MM/yyyy').format(i.add(const Duration(days: 6)))}",
                            style: Style(context).styleTextDataColumn),
                      ),
                      Row(
                        children: [
                          for (DateTime j = i; j.isBefore(i.add(const Duration(days: 7))); j = j.add(const Duration(days: 1)))
                            Expanded(
                              child: Container(
                                alignment: Alignment.center,
                                padding: const EdgeInsets.symmetric(vertical: 3),
                                decoration: BoxDecoration(border: Border.all(color: const Color.fromARGB(255, 207, 205, 205))),
                                child: Text(DateFormat('E').format(j),
                                    style: TextStyle(
                                        color: const Color.fromRGBO(133, 135, 145, 1),
                                        fontSize: context.fontSizeManager.fontSizeText14,
                                        letterSpacing: 2)),
                              ),
                            ),
                        ],
                      ),
                    ],
                  ),
                ),
            ],
          ),
        for (var task in resultListTasks)
          RowGanttChartTimeLineWidget(
            task: task,
            startDate: startDate,
            endDate: endDate,
          )
      ],
    );
  }

  // Column tableTaskTimelineWidget(BuildContext context, NavigationModel navigationModel) {
  //   return Column(
  //     children: [
  //       Container(
  //         margin: const EdgeInsets.only(top: 10),
  //         child: CustomDataTableWidget(
  //           columns: [
  //             Expanded(
  //               flex: 5,
  //               child: Text(
  //                 "NAME",
  //                 style: Style(context).styleTextDataColumn,
  //               ),
  //             ),
  //             Expanded(
  //               flex: 5,
  //               child: Text(
  //                 "DESCRIPTION",
  //                 style: Style(context).styleTextDataColumn,
  //               ),
  //             ),
  //             Expanded(
  //               flex: 2,
  //               child: Row(
  //                 crossAxisAlignment: CrossAxisAlignment.center,
  //                 mainAxisAlignment: MainAxisAlignment.center,
  //                 children: [
  //                   Text(
  //                     "STATUS",
  //                     style: Style(context).styleTextDataColumn,
  //                   ),
  //                   const Tooltip(
  //                     message: "Green = Active\nGrey = Draft\nRed = Review ",
  //                     child: Icon(
  //                       Icons.info,
  //                       color: Colors.grey,
  //                       size: 16,
  //                     ),
  //                   ),
  //                 ],
  //               ),
  //             ),
  //             Expanded(
  //               flex: 2,
  //               child: Center(
  //                 child: Text(
  //                   "CREATE DATE",
  //                   style: Style(context).styleTextDataColumn,
  //                 ),
  //               ),
  //             ),
  //             Container(
  //               width: 38,
  //             ),
  //           ],
  //           rows: [
  //             for (var task in resultListTasks)
  //               CustomDataRow(
  //                 cells: [
  //                   Expanded(
  //                     flex: 5,
  //                     child: Padding(
  //                       padding: const EdgeInsets.all(5),
  //                       child: Row(
  //                         crossAxisAlignment: CrossAxisAlignment.start,
  //                         children: [
  //                           const Icon(Icons.folder, color: Color.fromRGBO(61, 148, 254, 1)),
  //                           const SizedBox(
  //                             width: 10,
  //                           ),
  //                           Expanded(
  //                             child: Text(
  //                               "${task["title"] ?? ""}",
  //                               maxLines: 3,
  //                               overflow: TextOverflow.ellipsis,
  //                               style: Style(context).styleTextDataCell,
  //                             ),
  //                           ),
  //                         ],
  //                       ),
  //                     ),
  //                   ),
  //                   Expanded(
  //                     flex: 5,
  //                     child: Padding(
  //                       padding: const EdgeInsets.all(5),
  //                       child: Text(
  //                         task["description"] ?? "",
  //                         maxLines: 3,
  //                         overflow: TextOverflow.ellipsis,
  //                         style: Style(context).styleTextDataCell,
  //                       ),
  //                     ),
  //                   ),
  //                   Expanded(
  //                     flex: 2,
  //                     child: Padding(
  //                       padding: const EdgeInsets.all(5),
  //                       child: Center(
  //                         child: Badge2StatusSettingWidget(
  //                           status: task["status"],
  //                         ),
  //                       ),
  //                     ),
  //                   ),
  //                   Expanded(
  //                     flex: 2,
  //                     child: Padding(
  //                       padding: const EdgeInsets.all(5),
  //                       child: Center(
  //                         child: Text(
  //                           DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(task["createDate"]).toLocal()),
  //                           style: Style(context).styleTextDataCell,
  //                         ),
  //                       ),
  //                     ),
  //                   ),
  //                   PopupMenuButton(
  //                     offset: const Offset(5, 50),
  //                     tooltip: '',
  //                     splashRadius: 10,
  //                     icon: const Icon(
  //                       Icons.more_vert,
  //                     ),
  //                     itemBuilder: (context) => [
  //                       PopupMenuItem(
  //                         padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
  //                         child: ListTile(
  //                           onTap: () {
  //                             Navigator.pop(context);
  //                             Navigator.of(context).push(
  //                               createRoute(
  //                                 OptionTaskWidget(
  //                                   type: 'edit',
  //                                   id: task["id"],
  //                                   callbackOptionTest: (result) => handleEditTasks(result, task["id"]),
  //                                 ),
  //                               ),
  //                             );
  //                           },
  //                           contentPadding: const EdgeInsets.all(0),
  //                           hoverColor: Colors.transparent,
  //                           title: const Text("Edit"),
  //                           leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
  //                         ),
  //                       ),
  //                       PopupMenuItem(
  //                         padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
  //                         child: ListTile(
  //                           onTap: () {
  //                             Navigator.pop(context);
  //                             // dialogDeleteAll(
  //                             //     task, "elementRepository");
  //                           },
  //                           contentPadding: const EdgeInsets.all(0),
  //                           hoverColor: Colors.transparent,
  //                           title: const Text("Delete"),
  //                           leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
  //                         ),
  //                       ),
  //                     ],
  //                   ),
  //                 ],
  //                 onSelectChanged: () {
  //                   navigationModel.navigate("/project/${navigationModel.prjProjectId}/kanban/board/${task["id"]}");
  //                 },
  //               ),
  //           ],
  //         ),
  //       ),
  //       DynamicTablePagging(
  //         rowCountListTasks,
  //         currentPageTasks,
  //         rowPerPageTasks,
  //         pageChangeHandler: (page) {
  //           setState(() {
  //             futureListTasks = getListTasks(page);
  //           });
  //         },
  //         rowPerPageChangeHandler: (rowPerPage) {
  //           setState(() {
  //             rowPerPageTasks = rowPerPage!;
  //             futureListTasks = getListTasks(1);
  //           });
  //         },
  //       ),
  //     ],
  //   );
  // }

  dialogDeleteTasks(selectItem) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogDelete(
          selectedItem: selectItem,
          callback: () => handleDeleteTasks(selectItem["id"]),
          type: 'Tasks',
        );
      },
    );
  }
}

class RowGanttChartTitleWidget extends StatefulWidget {
  const RowGanttChartTitleWidget({
    Key? key,
    required this.task,
    required this.generation,
    required this.callbackStartDateEndDate,
    required this.onEditTask,
    required this.onDeleteTask,
  }) : super(key: key);
  final dynamic task;
  final int generation;
  final Function(DateTime startDate, DateTime endDate) callbackStartDateEndDate;
  final Function(dynamic task) onEditTask;
  final VoidCallback onDeleteTask;

  @override
  State<RowGanttChartTitleWidget> createState() => _RowGanttChartTitleWidgetState();
}

class _RowGanttChartTitleWidgetState extends State<RowGanttChartTitleWidget> {
  DateTime? startDate;
  DateTime? endDate;
  @override
  void initState() {
    setInitPage();
    super.initState();
  }

  setInitPage() {
    if (widget.task["children"] != null && widget.task["children"].isNotEmpty) {
      findStartDate(widget.task["children"]);
      findEndDate(widget.task["children"]);
    }
  }

  getListTasks() async {
    var findRequest = {"parentId": widget.task["id"]};
    var response = await httpPost("/test-framework-api/user/project/prj-task/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        widget.task["children"] = response["body"]["resultList"];
      });
      if (widget.task["children"].isNotEmpty) {
        findStartDate(widget.task["children"]);
        findEndDate(widget.task["children"]);
      }
    }
  }

  findEndDate(List list) {
    setState(() {
      var result = list.reduce(
          (value, element) => DateTime.parse(element["scheduledEndDate"]).isAfter(DateTime.parse(value["scheduledEndDate"])) ? element : value);
      endDate = DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(result["scheduledEndDate"]).toLocal();
    });
  }

  findStartDate(List list) {
    setState(() {
      var result = list.reduce(
          (value, element) => DateTime.parse(element["scheduledStartDate"]).isBefore(DateTime.parse(value["scheduledStartDate"])) ? element : value);
      startDate = DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(result["scheduledStartDate"]).toLocal();
    });
  }

  handleEditTasks(result, id) async {
    var response = await httpPost("/test-framework-api/user/project/prj-task/$id", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: () => Navigator.of(context).push(
            createRoute(
              OptionTaskWidget(
                type: 'edit',
                title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                id: widget.task["id"],
                callbackOptionTest: (result) => widget.onEditTask(result),
              ),
            ),
          ),
          child: Container(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
            decoration: BoxDecoration(
              border: Border.all(color: const Color.fromARGB(255, 207, 205, 205)),
            ),
            child: Row(
              children: [
                Container(
                  width: 230,
                  padding: const EdgeInsets.only(right: 5),
                  child: Row(
                    children: [
                      SizedBox(
                        width: (widget.generation - 1) * 15,
                      ),
                      GestureDetector(
                        onTap: () async {
                          setState(() {
                            widget.task["isSelected"] = !(widget.task["isSelected"] ?? false);
                          });
                          if (widget.task["isSelected"]) {
                            await getListTasks();
                          }
                          if (startDate != null && endDate != null) {
                            widget.callbackStartDateEndDate(startDate!, endDate!);
                          }
                        },
                        child: Icon(
                          (widget.task["isSelected"] ?? false) ? Icons.keyboard_arrow_down : Icons.keyboard_arrow_right,
                          color: const Color.fromRGBO(49, 49, 49, 1),
                        ),
                      ),
                      Expanded(
                        child: Text(
                          widget.task["title"] ?? "",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  width: 110,
                  alignment: Alignment.center,
                  padding: const EdgeInsets.symmetric(horizontal: 5),
                  child: Text(
                    widget.task["scheduledStartDate"] != null
                        ? DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(widget.task["scheduledStartDate"]).toLocal())
                        : "",
                    style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                  ),
                ),
                Container(
                  width: 110,
                  alignment: Alignment.center,
                  padding: const EdgeInsets.symmetric(horizontal: 5),
                  child: Text(
                    widget.task["scheduledEndDate"] != null
                        ? DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(widget.task["scheduledEndDate"]).toLocal())
                        : "",
                    style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                  ),
                ),
                PopupMenuButton(
                  offset: const Offset(2, 30),
                  tooltip: '',
                  splashRadius: 10,
                  child: const Icon(
                    Icons.more_vert,
                  ),
                  itemBuilder: (context) => [
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.of(context).push(
                            createRoute(
                              OptionTaskWidget(
                                type: 'edit',
                                title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                id: widget.task["id"],
                                callbackOptionTest: (result) => widget.onEditTask(result),
                              ),
                            ),
                          );
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                        leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          widget.onDeleteTask();
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "delete", defaultValue: "Delete"),
                        leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        if (widget.task["isSelected"] ?? false)
          for (var task in widget.task["children"] ?? [])
            RowGanttChartTitleWidget(
              task: task,
              generation: widget.generation + 1,
              callbackStartDateEndDate: (startDate, endDate) async {
                if (startDate.isBefore(this.startDate!) && task["isSelected"] == true) {
                  setState(() {
                    this.startDate = startDate;
                  });
                } else {
                  findStartDate(widget.task["children"]);
                }
                if (endDate.isAfter(this.endDate!) && task["isSelected"] == true) {
                  setState(() {
                    this.endDate = endDate;
                  });
                } else {
                  findEndDate(widget.task["children"]);
                }

                widget.callbackStartDateEndDate(this.startDate!, this.endDate!);
              },
              onEditTask: (taskCallback) async {
                await handleEditTasks(taskCallback, taskCallback["id"]);
                getListTasks();
              },
              onDeleteTask: () => widget.onDeleteTask(),
            )
      ],
    );
  }
}

class RowGanttChartTimeLineWidget extends StatefulWidget {
  const RowGanttChartTimeLineWidget({
    Key? key,
    this.task,
    required this.startDate,
    required this.endDate,
  }) : super(key: key);
  final dynamic task;
  final DateTime? startDate;
  final DateTime? endDate;
  @override
  State<RowGanttChartTimeLineWidget> createState() => _RowGanttChartTimeLineWidgetState();
}

class _RowGanttChartTimeLineWidgetState extends State<RowGanttChartTimeLineWidget> {
  bool isAfterOrEqual(DateTime dateTask, DateTime dateCheck) {
    final isAtSameMomentAs = dateTask.isAtSameMomentAs(dateCheck);
    return isAtSameMomentAs || dateCheck.isAfter(dateTask);
  }

  bool isBeforeOrEqual(DateTime dateTask, DateTime dateCheck) {
    final isAtSameMomentAs = dateTask.isAtSameMomentAs(dateCheck);
    return isAtSameMomentAs || dateCheck.isBefore(dateTask);
  }

  bool isBetween({required DateTime from, required DateTime to, required DateTime dateCheck}) {
    return isAfterOrEqual(from, dateCheck) && isBeforeOrEqual(to, dateCheck);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          decoration: BoxDecoration(
            border: Border.all(color: const Color.fromARGB(255, 207, 205, 205)),
          ),
          child: Row(
            children: [
              for (DateTime i = widget.startDate!; i.isBefore(widget.endDate!); i = i.add(const Duration(days: 7)))
                Expanded(
                  child: Row(
                    children: [
                      for (DateTime j = i; j.isBefore(i.add(const Duration(days: 7))); j = j.add(const Duration(days: 1)))
                        Expanded(
                          child: Container(
                            padding: const EdgeInsets.symmetric(vertical: 12),
                            alignment: Alignment.center,
                            child: Container(
                              height: 20,
                              color: isBetween(
                                      from: DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(widget.task["scheduledStartDate"]).toLocal(),
                                      to: DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(widget.task["scheduledEndDate"]).toLocal(),
                                      dateCheck: j)
                                  ? Colors.greenAccent
                                  : Colors.transparent,
                            ),
                          ),
                        ),
                    ],
                  ),
                ),
            ],
          ),
        ),
        if (widget.task["isSelected"] ?? false)
          for (var task in widget.task["children"] ?? [])
            RowGanttChartTimeLineWidget(
              // key: UniqueKey(),
              task: task,
              startDate: widget.startDate,
              endDate: widget.endDate,
            )
      ],
    );
  }
}
