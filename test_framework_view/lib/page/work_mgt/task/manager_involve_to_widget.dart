import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';
import 'package:test_framework_view/type.dart';
import '../../../api.dart';
import '../../../common/dynamic_table.dart';
import '../../../components/style.dart';
import '../../../model/model.dart';

class ManageInvolveToWidget extends StatefulWidget {
  final List involvedList;
  final String? prjTaskId;
  final Function onSave;
  const ManageInvolveToWidget({Key? key, required this.involvedList, required this.onSave, this.prjTaskId}) : super(key: key);

  @override
  State<ManageInvolveToWidget> createState() => _ManageInvolveToWidgetState();
}

class _ManageInvolveToWidgetState extends State<ManageInvolveToWidget> {
  final TextEditingController fullnameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  late Future futureListUser;
  List<User> resultListUser = [];
  var rowCountListUser = 0;
  var currentPage = 1;
  var rowPerPage = 10;
  var prjProjectId = "";
  bool isShowFilterForm = true;
  List<User> newInvolvedList = [];
  // List<User> oldInvolvedList = [];
  @override
  void initState() {
    prjProjectId = Provider.of<NavigationModel>(context, listen: false).prjProjectId ?? "";
    super.initState();
    setInitValue();
  }

  setInitValue() async {
    for (var involve in widget.involvedList) {
      User itemNew = User.fromMap(involve["involvedSysUser"]);
      newInvolvedList.add(itemNew);
    }
    await handleSearchUser(currentPage);
  }

  handleSearchUser(page) async {
    var findRequest = {
      "queryOffset": (page - 1) * rowPerPage,
      "queryLimit": rowPerPage,
      "prjProjectId": prjProjectId,
    };
    findRequest["fullnameLike"] = fullnameController.text.isNotEmpty ? "%${fullnameController.text}%" : null;
    findRequest["emailLike"] = emailController.text.isNotEmpty ? "%${emailController.text}%" : null;
    var response = await httpPost("/test-framework-api/user/sys/sys-user-project/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPage = page;
        rowCountListUser = response["body"]["rowCount"];
        resultListUser = [];
        for (var item in response["body"]["resultList"]) {
          User itemNew = User.fromMap(item["sysUser"]);
          resultListUser.add(itemNew);
        }
      });
    }
    return 0;
  }

  @override
  void dispose() {
    super.dispose();
    fullnameController.dispose();
    emailController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return TestFrameworkRootPageWidget(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(50, 30, 50, 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      headerWidget(context),
                      const SizedBox(
                        height: 20,
                      ),
                      filterFormWidget(),
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                              child: tableUserWidget(
                                  multiLanguageString(name: "list_user_search", defaultValue: "List User Search", context: context),
                                  "List User Search",
                                  resultListUser,
                                  newInvolvedList)),
                          const SizedBox(
                            width: 10,
                          ),
                          Expanded(
                              child: tableUserWidget(
                                  multiLanguageString(name: "list_user_Involve", defaultValue: "List User Involve", context: context),
                                  "List User Involve",
                                  newInvolvedList,
                                  resultListUser))
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
            btnWidget(context),
          ],
        ),
      );
    });
  }

  Column tableUserWidget(String title, String label, List<User> list, List<User> list1) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: Style(context).styleTitleDialog,
        ),
        CustomDataTableWidget(
          minWidth: 500,
          columns: [
            Expanded(
              child: MultiLanguageText(
                name: "fullname",
                defaultValue: "Fullname",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "nick_name",
                defaultValue: "Nick Name",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "email",
                defaultValue: "Email",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Container(
              width: 56,
            ),
          ],
          rows: [
            for (var item in list)
              CustomDataRow(
                cells: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        item.fullname,
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        item.nickname,
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        item.email,
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 56,
                    height: 35,
                    child: (label == "List User Search" &&
                            !list1.any(
                              (element) => element.id == item.id,
                            ))
                        ? ElevatedButton(
                            onPressed: () {
                              setState(() {
                                list1.add(item);
                              });
                            },
                            child: const Icon(Icons.add),
                          )
                        : (label == "List User Involve")
                            ? ElevatedButton(
                                onPressed: () {
                                  setState(() {
                                    list.remove(item);
                                  });
                                },
                                child: const Icon(Icons.remove),
                              )
                            : const SizedBox.shrink(),
                  ),
                ],
              ),
          ],
        ),
        if (label == "List User Search")
          DynamicTablePagging(
            rowCountListUser,
            currentPage,
            rowPerPage,
            pageChangeHandler: (page) {
              handleSearchUser(page);
            },
            rowPerPageChangeHandler: (rowPerPage) {
              setState(() {
                rowPerPage = rowPerPage!;
                handleSearchUser(1);
              });
            },
          ),
      ],
    );
  }

  Column filterFormWidget() {
    return Column(
      children: [
        Row(
          children: [
            MultiLanguageText(
              name: "filter_form",
              defaultValue: "Filter form",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: const Color.fromRGBO(0, 0, 0, 0.8),
                  fontSize: context.fontSizeManager.fontSizeText16,
                  letterSpacing: 2),
            ),
            IconButton(
              onPressed: () {
                setState(() {
                  isShowFilterForm = !isShowFilterForm;
                });
              },
              icon: isShowFilterForm ? const Icon(Icons.visibility) : const Icon(Icons.visibility_off),
              splashRadius: 1,
              tooltip: isShowFilterForm
                  ? multiLanguageString(name: "hide_filter_form", defaultValue: "Hide filter form", context: context)
                  : multiLanguageString(name: "show_filter_form", defaultValue: "Show filter form", context: context),
            )
          ],
        ),
        if (isShowFilterForm)
          Row(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: SizedBox(
                    height: 50,
                    child: TextField(
                      controller: fullnameController,
                      decoration: InputDecoration(
                        label: const MultiLanguageText(name: "fullname", defaultValue: "Fullname"),
                        contentPadding: const EdgeInsets.fromLTRB(10, 0, 10, 20),
                        border: const OutlineInputBorder(),
                        hintText: multiLanguageString(name: "by_fullname", defaultValue: "By fullname", context: context),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: SizedBox(
                    height: 50,
                    child: TextField(
                      controller: emailController,
                      decoration: InputDecoration(
                        label: const MultiLanguageText(name: "email", defaultValue: "Email"),
                        contentPadding: const EdgeInsets.fromLTRB(10, 0, 10, 20),
                        border: const OutlineInputBorder(),
                        hintText: multiLanguageString(name: "by_email", defaultValue: "By email", context: context),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        if (isShowFilterForm)
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: ElevatedButton(
                    onPressed: () {
                      handleSearchUser(currentPage);
                    },
                    child: const MultiLanguageText(name: "search", defaultValue: "Search", isLowerCase: false)),
              ),
              ElevatedButton(
                  onPressed: () {
                    setState(() {
                      fullnameController.clear();
                      emailController.clear();
                      handleSearchUser(currentPage);
                    });
                  },
                  child: const MultiLanguageText(name: "reset", defaultValue: "Reset", isLowerCase: false))
            ],
          ),
      ],
    );
  }

  Row headerWidget(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        MultiLanguageText(
          name: "user",
          defaultValue: "User",
          style: Style(context).styleTitleDialog,
        ),
        TextButton(
          onPressed: () => Navigator.pop(context, 'cancel'),
          child: const Icon(
            Icons.close,
            color: Colors.black,
          ),
        ),
      ],
    );
  }

  Padding btnWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: 40,
            width: 80,
            child: ElevatedButton(
              onPressed: () {
                List removedInvolved =
                    widget.involvedList.where((obj) => !newInvolvedList.any((newObj) => newObj.id == obj["involvedSysUserId"])).toList();
                List<User> addedInvolved =
                    newInvolvedList.where((newObj) => !widget.involvedList.any((obj) => obj["involvedSysUserId"] == newObj.id)).toList();
                widget.onSave(removedInvolved, addedInvolved);
                Navigator.pop(context);
              },
              child: const MultiLanguageText(
                name: "ok",
                defaultValue: "OK",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 10),
            height: 40,
            width: 120,
            child: ElevatedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              style: ElevatedButton.styleFrom(
                side: const BorderSide(color: Color.fromRGBO(209, 212, 216, 1)),
                backgroundColor: Colors.white,
                foregroundColor: const Color.fromRGBO(61, 90, 254, 1),
              ),
              child: const MultiLanguageText(
                name: "cancel",
                defaultValue: "CANCEL",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          )
        ],
      ),
    );
  }
}
