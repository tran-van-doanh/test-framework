import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/custom_state.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';
import '../../../common/dynamic_table.dart';
import '../../../components/style.dart';

class DialogTaskWidget extends StatefulWidget {
  final Function callbackSelected;
  final String prjProjectId;
  final String taskType;
  const DialogTaskWidget({Key? key, required this.callbackSelected, required this.prjProjectId, required this.taskType}) : super(key: key);
  @override
  State<DialogTaskWidget> createState() => _DialogTaskWidgetState();
}

class _DialogTaskWidgetState extends CustomState<DialogTaskWidget> {
  late Future future;
  var listTask = [];
  var rowCount = 0;
  var currentPage = 1;
  var rowPerPage = 10;
  dynamic selectedItemId;
  var searchRequest = {};
  TextEditingController nameController = TextEditingController();
  TextEditingController codeController = TextEditingController();
  @override
  void initState() {
    future = getListTask(currentPage);
    super.initState();
  }

  getListTask(page) async {
    if ((page - 1) * rowPerPage > rowCount) {
      page = (1.0 * rowCount / rowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {
      "queryOffset": (page - 1) * rowPerPage,
      "queryLimit": rowPerPage,
      "taskType": widget.taskType,
      "prjProjectId": widget.prjProjectId,
    };
    if (nameController.text.isNotEmpty) {
      findRequest["titleLike"] = "%${nameController.text}%";
    }
    if (codeController.text.isNotEmpty) {
      findRequest["nameLike"] = "%${codeController.text}%";
    }
    var response = await httpPost("/test-framework-api/user/project/prj-task/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPage = page;
        rowCount = response["body"]["rowCount"];
        listTask = response["body"]["resultList"];
      });
    }
    return 0;
  }

  @override
  void dispose() {
    nameController.dispose();
    codeController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return TestFrameworkRootPageWidget(
        child: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            return FutureBuilder(
              future: future,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: SingleChildScrollView(
                          child: IntrinsicHeight(
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(50, 30, 50, 10),
                              child: Column(
                                children: [
                                  headerWidget(context),
                                  const Divider(color: Color.fromRGBO(216, 218, 229, 1)),
                                  const SizedBox(height: 20),
                                  searchFormWidget(),
                                  tableTaskList(),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      btnWidget(context),
                    ],
                  );
                } else if (snapshot.hasError) {
                  return Text('${snapshot.error}');
                }
                return const Center(child: CircularProgressIndicator());
              },
            );
          },
        ),
      );
    });
  }

  Padding btnWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: 40,
            width: 80,
            child: ElevatedButton(
              onPressed: (selectedItemId != null)
                  ? () {
                      Navigator.pop(context);
                      widget.callbackSelected(selectedItemId);
                    }
                  : null,
              child: const MultiLanguageText(
                name: "ok",
                defaultValue: "Ok",
                isLowerCase: false,
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 10),
            height: 40,
            width: 120,
            child: ElevatedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              style: ElevatedButton.styleFrom(
                side: const BorderSide(color: Color.fromRGBO(209, 212, 216, 1)),
                backgroundColor: Colors.white,
                foregroundColor: const Color.fromRGBO(61, 90, 254, 1),
              ),
              child: const MultiLanguageText(
                name: "cancel",
                defaultValue: "Cancel",
                isLowerCase: false,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          )
        ],
      ),
    );
  }

  Column searchFormWidget() {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: DynamicTextField(
                controller: codeController,
                hintText: multiLanguageString(name: "enter_a_task_code", defaultValue: "Enter a task code", context: context),
                labelText: multiLanguageString(name: "task_code", defaultValue: "Task code", context: context),
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            Expanded(
              child: DynamicTextField(
                controller: nameController,
                hintText: multiLanguageString(name: "enter_a_task_name", defaultValue: "Enter a task name", context: context),
                labelText: multiLanguageString(name: "task_name", defaultValue: "Task name", context: context),
              ),
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                  onPressed: () {
                    future = getListTask(currentPage);
                  },
                  child: const MultiLanguageText(name: "search", defaultValue: "Search", isLowerCase: false)),
              const SizedBox(
                width: 10,
              ),
              ElevatedButton(
                  onPressed: () {
                    nameController.clear();
                    codeController.clear();
                    future = getListTask(currentPage);
                  },
                  child: const MultiLanguageText(name: "reset", defaultValue: "Reset", isLowerCase: false))
            ],
          ),
        ),
      ],
    );
  }

  Row headerWidget(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        MultiLanguageText(
          name: "task",
          defaultValue: "Task",
          isLowerCase: false,
          style: Style(context).styleTitleDialog,
        ),
        TextButton(
          onPressed: () => Navigator.pop(context, 'cancel'),
          child: const Icon(
            Icons.close,
            color: Colors.black,
          ),
        ),
      ],
    );
  }

  Widget tableTaskList() {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.all(15),
          decoration: const BoxDecoration(
            border: Border(
              top: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
              bottom: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
              left: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
              right: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
            ),
          ),
          child: Row(
            children: [
              Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: MultiLanguageText(name: "code", defaultValue: "Code", isLowerCase: false, style: Style(context).styleTextDataColumn),
                  )),
              Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: MultiLanguageText(name: "name", defaultValue: "Name", isLowerCase: false, style: Style(context).styleTextDataColumn),
                  )),
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: Center(
                    child: MultiLanguageText(name: "owner", defaultValue: "Owner", isLowerCase: false, style: Style(context).styleTextDataColumn)),
              )),
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: Center(
                    child: MultiLanguageText(
                        name: "create_date", defaultValue: "Create date", isLowerCase: false, style: Style(context).styleTextDataColumn)),
              )),
            ],
          ),
        ),
        Column(
          children: [
            for (var task in listTask)
              Container(
                padding: const EdgeInsets.all(15),
                decoration: const BoxDecoration(
                  border: Border(
                    top: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
                    bottom: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
                    left: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
                    right: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
                  ),
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      flex: 2,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: Row(
                          children: [
                            Radio(
                              value: task["id"],
                              groupValue: selectedItemId,
                              onChanged: (dynamic value) {
                                setState(() {
                                  selectedItemId = value;
                                });
                              },
                            ),
                            Text(
                              task["name"] ?? "",
                              style: Style(context).styleTextDataCell,
                            ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: Text(
                          task["title"] ?? "",
                          maxLines: 3,
                          style: Style(context).styleTextDataCell,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: Center(
                          child: Text(
                            task["sysUser"]["fullname"],
                            maxLines: 3,
                            softWrap: true,
                            style: Style(context).styleTextDataCell,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: Center(
                          child: Text(
                            DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(task["createDate"]).toLocal()),
                            maxLines: 3,
                            softWrap: true,
                            style: Style(context).styleTextDataCell,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
          ],
        ),
        DynamicTablePagging(
          rowCount,
          currentPage,
          rowPerPage,
          pageChangeHandler: (page) {
            getListTask(page);
          },
          rowPerPageChangeHandler: (value) {
            setState(() {
              rowPerPage = value;
              getListTask(1);
            });
          },
        ),
      ],
    );
  }
}
