import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import '../../../api.dart';
import '../../../common/custom_draggable.dart';
import '../../../extension/hex_color.dart';
import '../../../type.dart';

class SprintTaskWidget extends StatefulWidget {
  final SprintTask sprintTask;
  final VoidCallback onRemove;
  const SprintTaskWidget({
    Key? key,
    required this.sprintTask,
    required this.onRemove,
  }) : super(key: key);

  @override
  State<SprintTaskWidget> createState() => _SprintTaskWidgetState();
}

class _SprintTaskWidgetState extends State<SprintTaskWidget> {
  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(5),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 6),
        decoration: const BoxDecoration(
          color: Colors.white,
          border: Border(
            left: BorderSide(
              color: Colors.grey,
              width: 4,
            ),
          ),
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(56, 65, 74, 0.15),
              offset: Offset(0, 1),
              blurRadius: 2,
            ),
          ],
        ),
        constraints: const BoxConstraints(maxWidth: 400),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Material(
              color: Colors.transparent,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Text(
                      widget.sprintTask.prjTask.title,
                      style: TextStyle(
                        fontSize: context.fontSizeManager.fontSizeText14,
                        decoration: TextDecoration.none,
                        fontWeight: FontWeight.bold,
                        // color: Color.fromRGBO(0, 0, 0, 0.8),
                      ),
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.all(0),
                    alignment: Alignment.centerRight,
                    child: PopupMenuButton(
                      offset: const Offset(0, 25),
                      tooltip: '',
                      splashRadius: 1,
                      child: const Icon(
                        Icons.more_vert,
                      ),
                      itemBuilder: (context) => [
                        PopupMenuItem(
                          padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                              widget.onRemove();
                            },
                            child: const Row(
                              children: [
                                Icon(
                                  Icons.delete,
                                  color: Color.fromARGB(255, 112, 114, 119),
                                  size: 20,
                                ),
                                MultiLanguageText(name: "delete", defaultValue: "Delete"),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            (widget.sprintTask.prjTask.tagsList != null && widget.sprintTask.prjTask.tagsList!.isNotEmpty)
                ? Wrap(
                    children: [
                      for (var tagId in widget.sprintTask.prjTask.tagsList!)
                        Container(
                          margin: EdgeInsets.only(bottom: 5, right: widget.sprintTask.prjTask.tagsList!.last != tagId ? 5 : 0),
                          child: TagTitleWidget(key: Key(tagId), tagId: tagId),
                        ),
                    ],
                  )
                : const Icon(Icons.backspace),
            const SizedBox(
              height: 5,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                DueDateCalendarWidget(dueDate: widget.sprintTask.prjTask.scheduledEndDate),
                (widget.sprintTask.prjTask.assigneeSysUser != null)
                    ? UserAvatarAgileTaskWidget(
                        user: widget.sprintTask.prjTask.assigneeSysUser!,
                        isDrag: false,
                      )
                    : const SizedBox.shrink(),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class TagTitleWidget extends StatefulWidget {
  const TagTitleWidget({
    Key? key,
    required this.tagId,
  }) : super(key: key);

  final String tagId;

  @override
  State<TagTitleWidget> createState() => _TagTitleWidgetState();
}

class _TagTitleWidgetState extends State<TagTitleWidget> {
  dynamic tag;
  @override
  void initState() {
    super.initState();
    getTag(widget.tagId);
  }

  getTag(id) async {
    var response = await httpGet("/test-framework-api/user/project/prj-task-tag/$id", context);
    if (mounted && response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        tag = response["body"]["result"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return tag != null && tag.isNotEmpty
        ? Container(
            padding: const EdgeInsets.symmetric(horizontal: 5),
            decoration: BoxDecoration(
              color: HexColor.fromHex(tag["tagColor"]).withOpacity(0.3),
              borderRadius: BorderRadius.circular(6),
            ),
            child: Text(
              "${tag["title"]}",
              style: TextStyle(
                fontSize: context.fontSizeManager.fontSizeText13,
                decoration: TextDecoration.none,
                fontWeight: FontWeight.bold,
                color: HexColor.fromHex(tag["tagColor"]),
              ),
            ),
          )
        : const CircularProgressIndicator();
  }
}

class DueDateCalendarWidget extends StatelessWidget {
  const DueDateCalendarWidget({
    Key? key,
    required this.dueDate,
  }) : super(key: key);
  final String? dueDate;
  @override
  Widget build(BuildContext context) {
    DateTime? dueDateTime;
    if (dueDate != null && dueDate!.isNotEmpty) dueDateTime = DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(dueDate!).toLocal();
    DateTime threeDaysAgo = DateTime.now().subtract(const Duration(days: 3));
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        (dueDate != null && dueDate!.isNotEmpty)
            ? Icon(
                Icons.calendar_today,
                size: 20,
                color: dueDateTime!.isBefore(threeDaysAgo)
                    ? Colors.green
                    : dueDateTime.isAfter(DateTime.now())
                        ? Colors.red
                        : Colors.yellow,
              )
            : const Icon(
                Icons.event_busy,
                size: 20,
              ),
        const SizedBox(
          width: 5,
        ),
        Text(
          DateFormat("yyyy-MM-dd").format(dueDateTime!),
          style: TextStyle(
              fontSize: context.fontSizeManager.fontSizeText13,
              fontWeight: FontWeight.bold,
              color: const Color.fromRGBO(75, 75, 75, 1),
              decoration: TextDecoration.none),
        ),
      ],
    );
  }
}

class UserAvatarAgileTaskWidget extends StatelessWidget {
  final User user;
  final bool isDrag;
  const UserAvatarAgileTaskWidget({Key? key, required this.user, required this.isDrag}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    getFirstChar(String fullname) => fullname.isNotEmpty ? fullname.trim().split(' ').map((l) => l[0]).take(4).join() : '';
    Tooltip widget = Tooltip(
      message: user.fullname,
      child: Container(
        height: 25,
        width: 25,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: const Color(0xff7986cb),
        ),
        child: Center(
          child: Transform.scale(
            scale: 1,
            child: Text(
              getFirstChar(user.fullname).toUpperCase(),
              style: const TextStyle(
                color: Colors.white,
                fontSize: 11,
                decoration: TextDecoration.none,
                fontWeight: FontWeight.normal,
              ),
            ),
          ),
        ),
      ),
    );
    return isDrag
        ? CustomDraggable(
            data: user,
            feedback: widget,
            childWhenDragging: widget,
            child: widget,
          )
        : widget;
  }
}
