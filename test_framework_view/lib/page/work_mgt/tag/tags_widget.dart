import 'dart:async';

import 'package:flutter/material.dart';

import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/components/badge/badge_icon.dart';
import 'package:test_framework_view/components/dialog/dialog_delete.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/components/header_setting.dart';
import 'package:test_framework_view/type.dart';

import '../../../../api.dart';
import '../../../../model/model.dart';
import '../../../common/custom_state.dart';
import 'option_tags_widget.dart';

class TagsWidget extends StatefulWidget {
  const TagsWidget({Key? key}) : super(key: key);

  @override
  State<TagsWidget> createState() => _TagsWidgetState();
}

class _TagsWidgetState extends CustomState<TagsWidget> {
  late Future futureListTags;
  var resultListTags = [];
  var rowCountListTags = 0;
  var currentPage = 1;
  var rowPerPage = 10;
  var searchRequest = {};
  var prjProjectId = "";

  @override
  void initState() {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    prjProjectId = navigationModel.prjProjectId ?? "";
    search({});
    super.initState();
  }

  search(searchRequest) {
    this.searchRequest = searchRequest;
    futureListTags = getListTags(currentPage);
  }

  Future getListTags(page) async {
    if ((page - 1) * rowPerPage > rowCountListTags) {
      page = (1.0 * rowCountListTags / rowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {
      "queryOffset": (page - 1) * rowPerPage,
      "queryLimit": rowPerPage,
      "prjProjectId": prjProjectId,
    };
    if (searchRequest["titleLike"] != null && searchRequest["titleLike"].isNotEmpty) {
      findRequest["titleLike"] = "%${searchRequest["titleLike"]}%";
    }
    if (searchRequest["status"] != null) {
      findRequest["status"] = searchRequest["status"];
    }
    var response = await httpPost("/test-framework-api/user/project/prj-task-tag/search", findRequest, context);

    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPage = page;
        rowCountListTags = response["body"]["rowCount"];
        resultListTags = response["body"]["resultList"];
      });
    }
    return 0;
  }

  handleDeleteTags(id) async {
    var response = await httpDelete("/test-framework-api/user/project/prj-task-tag/$id", context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        await getListTags(currentPage);
      }
    }
  }

  handleAddTags(result) async {
    var response = await httpPut("/test-framework-api/user/project/prj-task-tag", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await getListTags(currentPage);
      }
    }
  }

  handleEditTags(result, id) async {
    var response = await httpPost("/test-framework-api/user/project/prj-task-tag/$id", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await getListTags(currentPage);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, LanguageModel>(
      builder: (context, navigationModel, languageModel, child) {
        return FutureBuilder(
          future: futureListTags,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Column(
                children: [
                  Expanded(
                    child: ListView(
                      children: [
                        NavMenuTabV1(
                          currentUrl: "/settings/tag",
                          margin: const EdgeInsets.fromLTRB(50, 20, 50, 0),
                          navigationList: context.settingsTaskMenuList,
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
                          child: Column(
                            children: [
                              HeaderSettingWidget(
                                type: "Tag",
                                title: multiLanguageString(name: "tag", defaultValue: "Tag", context: context),
                                countList: rowCountListTags,
                                callbackSearch: (result) {
                                  search(result);
                                },
                              ),
                              if (resultListTags.isNotEmpty) tableWidget(context),
                              SelectionArea(
                                child: DynamicTablePagging(
                                  rowCountListTags,
                                  currentPage,
                                  rowPerPage,
                                  pageChangeHandler: (page) {
                                    setState(() {
                                      futureListTags = getListTags(page);
                                    });
                                  },
                                  rowPerPageChangeHandler: (rowPerPage) {
                                    setState(() {
                                      this.rowPerPage = rowPerPage!;
                                      futureListTags = getListTags(1);
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  btnWidget(context),
                ],
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  Padding btnWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 40, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            margin: const EdgeInsets.only(right: 10),
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              onPressed: () {
                Navigator.of(context).push(
                  createRoute(
                    OptionTagsWidget(
                      type: "create",
                      title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                      callbackOptionTest: (result) {
                        handleAddTags(result);
                      },
                      prjProjectId: prjProjectId,
                    ),
                  ),
                );
              },
              icon: const Icon(Icons.note_add, color: Color.fromRGBO(49, 49, 49, 1)),
              label: MultiLanguageText(
                name: "new_tag",
                defaultValue: "New Tag",
                isLowerCase: false,
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
            ),
          ),
        ],
      ),
    );
  }

  SelectionArea tableWidget(BuildContext context) {
    return SelectionArea(
      child: Container(
        margin: const EdgeInsets.only(top: 30),
        child: CustomDataTableWidget(
          columns: [
            Expanded(
              flex: 2,
              child: MultiLanguageText(
                name: "name",
                defaultValue: "Name",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              flex: 2,
              child: MultiLanguageText(
                name: "description",
                defaultValue: "Description",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "owner",
                defaultValue: "Owner",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: Center(
                child: MultiLanguageText(
                  name: "create_date",
                  defaultValue: "Create date",
                  isLowerCase: false,
                  style: Style(context).styleTextDataColumn,
                ),
              ),
            ),
            Expanded(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  MultiLanguageText(
                    name: "status",
                    defaultValue: "Status",
                    isLowerCase: false,
                    style: Style(context).styleTextDataColumn,
                  ),
                  Tooltip(
                    message: multiLanguageString(name: "active_inactive", defaultValue: "Green = Active\nRed = Inactive ", context: context),
                    child: const Icon(
                      Icons.info,
                      color: Colors.grey,
                      size: 16,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: 38,
            ),
          ],
          rows: [
            for (var resultTags in resultListTags)
              CustomDataRow(
                cells: [
                  Expanded(
                    flex: 2,
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Row(
                        children: [
                          const Icon(Icons.description, color: Color.fromRGBO(61, 148, 254, 1)),
                          const SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Text(
                              resultTags["title"] ?? "",
                              maxLines: 3,
                              overflow: TextOverflow.ellipsis,
                              style: Style(context).styleTextDataCell,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Text(
                      resultTags["description"] ?? "",
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        resultTags["sysUser"]["fullname"],
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Center(
                        child: Text(
                          DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultTags["createDate"]).toLocal()),
                          style: Style(context).styleTextDataCell,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Center(
                      child: Badge2StatusSettingWidget(
                        status: resultTags["status"],
                      ),
                    ),
                  ),
                  PopupMenuButton(
                    offset: const Offset(5, 50),
                    tooltip: '',
                    splashRadius: 10,
                    itemBuilder: (context) => [
                      PopupMenuItem(
                        padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                        child: ListTile(
                          onTap: () {
                            Navigator.pop(context);
                            Navigator.of(context).push(
                              createRoute(
                                OptionTagsWidget(
                                  type: "edit",
                                  title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                  id: resultTags["id"],
                                  callbackOptionTest: (result) {
                                    handleEditTags(result, resultTags["id"]);
                                  },
                                  prjProjectId: prjProjectId,
                                ),
                              ),
                            );
                          },
                          contentPadding: const EdgeInsets.all(0),
                          hoverColor: Colors.transparent,
                          title: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                          leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
                        ),
                      ),
                      PopupMenuItem(
                        padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                        child: ListTile(
                          onTap: () {
                            Navigator.pop(context);
                            dialogDeleteTags(resultTags);
                          },
                          contentPadding: const EdgeInsets.all(0),
                          hoverColor: Colors.transparent,
                          title: const MultiLanguageText(name: "delete", defaultValue: "Delete"),
                          leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
          ],
        ),
      ),
    );
  }

  dialogDeleteTags(selectItem) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogDelete(
          selectedItem: selectItem,
          callback: () {
            handleDeleteTags(selectItem["id"]);
          },
          type: 'Tag',
        );
      },
    );
  }
}
