import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/custom_state.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/common/row_code_with_tooltip.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/model/model.dart';

class SettingProjectWorkMGTWidget extends StatefulWidget {
  final String type;
  final String title;
  final String? id;
  final Function? callbackOptionTest;
  const SettingProjectWorkMGTWidget({
    Key? key,
    required this.type,
    required this.title,
    this.id,
    this.callbackOptionTest,
  }) : super(key: key);

  @override
  State<SettingProjectWorkMGTWidget> createState() => _SettingProjectWorkMGTWidgetState();
}

class _SettingProjectWorkMGTWidgetState extends CustomState<SettingProjectWorkMGTWidget> {
  late Future future;
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _desController = TextEditingController();
  final TextEditingController _startDateController = TextEditingController();
  final TextEditingController _endDateController = TextEditingController();
  String? _bugTrackerType;
  String? _jiraIssueCreateType = "MANUAL";
  String? _mantisIssueCreateType = "MANUAL";
  String? _llqIssueCreateType = "MANUAL";
  int _status = 0;
  String? autoCreateProfile;
  String? projectType;
  var listProjectType = [];
  final _formKey = GlobalKey<FormState>();
  dynamic selectedItem;
  @override
  void initState() {
    future = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    getListProjectType();
    if (widget.id != null) {
      await getSelectedItem(widget.id!);
      await setInitValue();
    }

    return 0;
  }

  getListProjectType() async {
    var findRequest = {"status": 1};
    var response = await httpPost("/test-framework-api/user/project/prj-project-type/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        listProjectType = response["body"]["resultList"] ?? [];
      });
    }
  }

  getSelectedItem(String id) async {
    var response = await httpGet("/test-framework-api/user/project/prj-project/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        selectedItem = response["body"]["result"];
      });
    }
  }

  _selectDate(TextEditingController controller) async {
    final DateTime? datePicked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (datePicked != null) {
      setState(() {
        controller.text = DateFormat('dd-MM-yyyy').format(datePicked);
      });
    }
  }

  setInitValue() {
    setState(() {
      if (selectedItem != null) {
        _bugTrackerType = selectedItem["bugTrackerType"];
        _status = selectedItem["status"] ?? 0;
        autoCreateProfile = selectedItem["autoCreateProfile"].toString();
        _nameController.text = selectedItem["name"] ?? "";
        _titleController.text = selectedItem["title"] ?? "";
        _desController.text = selectedItem["description"] ?? "";
        projectType = selectedItem["prjProjectTypeId"];
        if (selectedItem["projectConfig"] != null && selectedItem["projectConfig"]["jira"] != null) {
          _jiraIssueCreateType = selectedItem["projectConfig"]["jira"]["issueCreateType"] ?? "MANUAL";
        }
        if (selectedItem["projectConfig"] != null && selectedItem["projectConfig"]["mantis"] != null) {
          _mantisIssueCreateType = selectedItem["projectConfig"]["mantis"]["issueCreateType"] ?? "MANUAL";
        }
        if (selectedItem["projectConfig"] != null && selectedItem["projectConfig"]["llq"] != null) {
          _llqIssueCreateType = selectedItem["projectConfig"]["llq"]["issueCreateType"] ?? "MANUAL";
        }
        _startDateController.text = selectedItem["startDate"] != null
            ? DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(selectedItem["startDate"]).toLocal())
            : DateFormat('dd-MM-yyyy').format(DateTime.now());
        _endDateController.text = selectedItem["endDate"] != null
            ? DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(selectedItem["endDate"]).toLocal())
            : DateFormat('dd-MM-yyyy').format(DateTime.now());
      }
    });
  }

  @override
  void dispose() {
    _nameController.dispose();
    _titleController.dispose();
    _desController.dispose();
    _startDateController.dispose();
    _endDateController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return FutureBuilder(
        future: future,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Scaffold(
                body: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                headerWidget(context),
                const Divider(color: Color.fromRGBO(216, 218, 229, 1)),
                Expanded(
                  child: Form(
                    key: _formKey,
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                        child: bodyWidget(context),
                      ),
                    ),
                  ),
                ),
                btnWidget()
              ],
            ));
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        },
      );
    });
  }

  Padding btnWidget() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20, top: 20),
      child: Center(
        child: DynamicButton(
            onPressed: () {
              if (_formKey.currentState!.validate()) {
                var result = {};
                if (selectedItem != null) {
                  result = selectedItem;
                  result["prjProjectTypeId"] = projectType;
                  result["name"] = _nameController.text;
                  result["title"] = _titleController.text;
                  result["description"] = _desController.text;
                  result["bugTrackerType"] = _bugTrackerType;
                  result["status"] = _status;
                  result["autoCreateProfile"] = (autoCreateProfile == "true");
                  result["startDate"] = "${DateFormat('dd-MM-yyyy').parse(_startDateController.text).toIso8601String()}+07:00";
                  result["endDate"] = "${DateFormat('dd-MM-yyyy').parse(_endDateController.text).toIso8601String()}+07:00";
                  result["projectConfig"] ??= {};
                  result["projectConfig"]["jira"] = {
                    "issueCreateType": _jiraIssueCreateType,
                  };
                  result["projectConfig"]["mantis"] = {
                    "issueCreateType": _mantisIssueCreateType,
                  };
                  result["projectConfig"]["llq"] = {
                    "issueCreateType": _llqIssueCreateType,
                  };
                } else {
                  result = {
                    "prjProjectTypeId": projectType,
                    "name": _nameController.text,
                    "title": _titleController.text,
                    "description": _desController.text,
                    "bugTrackerType": _bugTrackerType,
                    "status": _status,
                    "autoCreateProfile": (autoCreateProfile == "true"),
                    "startDate": "${DateFormat('dd-MM-yyyy').parse(_startDateController.text).toIso8601String()}+07:00",
                    "endDate": "${DateFormat('dd-MM-yyyy').parse(_endDateController.text).toIso8601String()}+07:00",
                    "projectConfig": {
                      "jira": {
                        "issueCreateType": _jiraIssueCreateType,
                      },
                      "mantis": {
                        "issueCreateType": _mantisIssueCreateType,
                      },
                      "llq": {
                        "issueCreateType": _llqIssueCreateType,
                      }
                    }
                  };
                }
                widget.callbackOptionTest!(result);
              }
            },
            name: "save",
            label: multiLanguageString(name: "save", defaultValue: "Save", isLowerCase: false, context: context)),
      ),
    );
  }

  Column bodyWidget(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.only(bottom: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "project_type", defaultValue: "Project type", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                  value: projectType,
                  hint: multiLanguageString(name: "choose_a_project_type", defaultValue: "Choose a project type", context: context),
                  items: listProjectType.map<DropdownMenuItem<String>>((dynamic result) {
                    return DropdownMenuItem(
                      value: result["id"],
                      child: Text(result["title"]),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      projectType = newValue!;
                    });
                  },
                  borderRadius: 5,
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Expanded(
                flex: 3,
                child: RowCodeWithTooltipWidget(),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _nameController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                  readOnly: widget.type == "edit",
                  isRequiredRegex: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "name", defaultValue: "Name", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _titleController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "description", defaultValue: "Description", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _desController,
                  hintText: "...",
                  maxline: 3,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "auto_create_profile", defaultValue: "Auto create profile", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                  value: autoCreateProfile ?? "false",
                  hint: multiLanguageString(name: "choose_a_status", defaultValue: "Choose a status", context: context),
                  items: ["false", "true"].map<DropdownMenuItem<String>>((String result) {
                    return DropdownMenuItem(
                      value: result,
                      child: Text(result),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      autoCreateProfile = newValue!;
                    });
                  },
                  borderRadius: 5,
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "status", defaultValue: "Status", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                  value: _status,
                  hint: multiLanguageString(name: "choose_a_status", defaultValue: "Choose a status", context: context),
                  items: [
                    {"name": multiLanguageString(name: "active", defaultValue: "Active", context: context), "value": 1},
                    {"name": multiLanguageString(name: "inactive", defaultValue: "Inactive", context: context), "value": 0},
                  ].map<DropdownMenuItem<int>>((dynamic result) {
                    return DropdownMenuItem(
                      value: result["value"],
                      child: Text(result["name"]),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      _status = newValue!;
                    });
                  },
                  borderRadius: 5,
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "start_date", defaultValue: "Start date", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _startDateController,
                  suffixIcon: const Icon(Icons.calendar_today),
                  hintText: multiLanguageString(name: "select_start_date", defaultValue: "Select start date", context: context),
                  labelText: multiLanguageString(name: "start_date", defaultValue: "Start date", context: context),
                  readOnly: true,
                  isRequiredNotEmpty: true,
                  onTap: () {
                    _selectDate(_startDateController);
                  },
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "end_date", defaultValue: "End date", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _endDateController,
                  suffixIcon: const Icon(Icons.calendar_today),
                  hintText: multiLanguageString(name: "select_end_date", defaultValue: "Select end date", context: context),
                  labelText: multiLanguageString(name: "end_date", defaultValue: "End date", context: context),
                  readOnly: true,
                  isRequiredNotEmpty: true,
                  onTap: () {
                    _selectDate(_endDateController);
                  },
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "bug_tracker_type", defaultValue: "Bug tracker type", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                  value: _bugTrackerType ?? "None",
                  hint: multiLanguageString(name: "bug_tracker_type", defaultValue: "Bug tracker type", context: context),
                  items: [
                    {"value": "None", "name": "None"},
                    {"value": "LLQ", "name": "LLQ Work management"},
                    {"value": "JIRA", "name": "Jira"},
                    {"value": "MANTIS", "name": "Mantis"}
                  ].map<DropdownMenuItem<String>>((dynamic result) {
                    return DropdownMenuItem(
                      value: result["value"],
                      child: Text(result["name"]),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      // _formKey.currentState!.validate();
                      _bugTrackerType = newValue!;
                    });
                  },
                  borderRadius: 5,
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        if (_bugTrackerType == "JIRA")
          Column(
            children: [
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "issue_create_type", defaultValue: "Issue create type", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicDropdownButton(
                        value: _jiraIssueCreateType,
                        hint: multiLanguageString(name: "issue_create_type", defaultValue: "issue create type", context: context),
                        items: ["AUTO", "MANUAL"].map<DropdownMenuItem<String>>((String result) {
                          return DropdownMenuItem(
                            value: result,
                            child: Text(result),
                          );
                        }).toList(),
                        onChanged: (newValue) {
                          setState(() {
                            // _formKey.currentState!.validate();
                            _jiraIssueCreateType = newValue!;
                          });
                        },
                        borderRadius: 5,
                        isRequiredNotEmpty: true,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        if (_bugTrackerType == "MANTIS")
          Column(
            children: [
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "issue_create_type", defaultValue: "Issue create type", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicDropdownButton(
                        value: _mantisIssueCreateType,
                        hint: multiLanguageString(name: "issue_create_type", defaultValue: "issue create type", context: context),
                        items: ["AUTO", "MANUAL"].map<DropdownMenuItem<String>>((String result) {
                          return DropdownMenuItem(
                            value: result,
                            child: Text(result),
                          );
                        }).toList(),
                        onChanged: (newValue) {
                          setState(() {
                            // _formKey.currentState!.validate();
                            _mantisIssueCreateType = newValue!;
                          });
                        },
                        borderRadius: 5,
                        isRequiredNotEmpty: true,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        if (_bugTrackerType == "LLQ")
          Column(
            children: [
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "issue_create_type", defaultValue: "Issue create type", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicDropdownButton(
                        value: _llqIssueCreateType,
                        hint: multiLanguageString(name: "issue_create_type", defaultValue: "issue create type", context: context),
                        items: ["AUTO", "MANUAL"].map<DropdownMenuItem<String>>((String result) {
                          return DropdownMenuItem(
                            value: result,
                            child: Text(result),
                          );
                        }).toList(),
                        onChanged: (newValue) {
                          setState(() {
                            // _formKey.currentState!.validate();
                            _llqIssueCreateType = newValue!;
                          });
                        },
                        borderRadius: 5,
                        isRequiredNotEmpty: true,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
      ],
    );
  }

  Padding headerWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "type_project",
            defaultValue: "\$0 PROJECT",
            variables: [widget.title],
            isLowerCase: false,
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
            onPressed: () => Navigator.pop(context, 'cancel'),
            child: const Icon(
              Icons.close,
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }
}
