import 'dart:async';

import 'package:flutter/material.dart';

import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/components/badge/badge_icon.dart';
import 'package:test_framework_view/components/dialog/dialog_delete.dart' show DialogDelete;
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/components/header_setting.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';
import 'package:test_framework_view/type.dart';

import '../../../../api.dart';
import '../../../../model/model.dart';
import '../../../common/custom_state.dart';
import 'manage_task_widget.dart';
import 'option_sprint_widget.dart';

class SprintWidget extends StatefulWidget {
  const SprintWidget({Key? key}) : super(key: key);

  @override
  State<SprintWidget> createState() => _SprintWidgetState();
}

class _SprintWidgetState extends CustomState<SprintWidget> {
  late Future futureListSprint;
  var resultListSprint = [];
  var rowCountListSprint = 0;
  var currentPage = 1;
  var rowPerPage = 10;
  var searchRequest = {};
  var prjProjectId = "";

  @override
  void initState() {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    prjProjectId = navigationModel.prjProjectId ?? "";
    search({});
    super.initState();
  }

  search(searchRequest) {
    this.searchRequest = searchRequest;
    futureListSprint = getListSprint(currentPage);
  }

  Future getListSprint(page) async {
    if ((page - 1) * rowPerPage > rowCountListSprint) {
      page = (1.0 * rowCountListSprint / rowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {
      "queryOffset": (page - 1) * rowPerPage,
      "queryLimit": rowPerPage,
      "prjProjectId": prjProjectId,
    };
    if (searchRequest["titleLike"] != null && searchRequest["titleLike"].isNotEmpty) {
      findRequest["titleLike"] = "%${searchRequest["titleLike"]}%";
    }
    if (searchRequest["status"] != null) {
      findRequest["status"] = searchRequest["status"];
    }
    var response = await httpPost("/test-framework-api/user/project/prj-sprint/search", findRequest, context);

    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPage = page;
        rowCountListSprint = response["body"]["rowCount"];
        resultListSprint = response["body"]["resultList"];
      });
    }
    return 0;
  }

  handleDeleteSprint(id) async {
    var response = await httpDelete("/test-framework-api/user/project/prj-sprint/$id", context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        await getListSprint(currentPage);
      }
    }
  }

  handleAddSprint(result) async {
    var response = await httpPut("/test-framework-api/user/project/prj-sprint", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await getListSprint(currentPage);
      }
    }
  }

  handleEditSprint(result, id) async {
    var response = await httpPost("/test-framework-api/user/project/prj-sprint/$id", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await getListSprint(currentPage);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, LanguageModel>(
      builder: (context, navigationModel, languageModel, child) {
        return FutureBuilder(
          future: futureListSprint,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Column(
                children: [
                  Expanded(
                    child: ListView(
                      children: [
                        NavMenuTabV1(
                          currentUrl: "/settings/sprint-list",
                          margin: const EdgeInsets.fromLTRB(50, 20, 50, 0),
                          navigationList: context.settingsTaskMenuList,
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
                          child: Column(
                            children: [
                              HeaderSettingWidget(
                                type: "Sprint",
                                title: multiLanguageString(name: "sprint", defaultValue: "Sprint", context: context),
                                countList: rowCountListSprint,
                                callbackSearch: (result) {
                                  search(result);
                                },
                              ),
                              if (resultListSprint.isNotEmpty) tableWidget(context),
                              SelectionArea(
                                child: DynamicTablePagging(
                                  rowCountListSprint,
                                  currentPage,
                                  rowPerPage,
                                  pageChangeHandler: (page) {
                                    setState(() {
                                      futureListSprint = getListSprint(page);
                                    });
                                  },
                                  rowPerPageChangeHandler: (rowPerPage) {
                                    setState(() {
                                      this.rowPerPage = rowPerPage!;
                                      futureListSprint = getListSprint(1);
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  btnWidget(context),
                ],
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  Padding btnWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 40, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            margin: const EdgeInsets.only(right: 10),
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              onPressed: () {
                Navigator.of(context).push(
                  createRoute(
                    OptionSprintWidget(
                      type: "create",
                      title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                      callbackOptionTest: (result) {
                        handleAddSprint(result);
                      },
                      prjProjectId: prjProjectId,
                    ),
                  ),
                );
              },
              icon: const Icon(Icons.note_add, color: Color.fromRGBO(49, 49, 49, 1)),
              label: MultiLanguageText(
                name: "new_sprint",
                defaultValue: "New Sprint",
                isLowerCase: false,
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
            ),
          ),
        ],
      ),
    );
  }

  SelectionArea tableWidget(BuildContext context) {
    return SelectionArea(
      child: Container(
        margin: const EdgeInsets.only(top: 30),
        child: CustomDataTableWidget(
          columns: [
            Expanded(
              flex: 2,
              child: MultiLanguageText(
                name: "name",
                defaultValue: "Name",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              flex: 2,
              child: MultiLanguageText(
                name: "description",
                defaultValue: "Description",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "owner",
                defaultValue: "Owner",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: Center(
                child: MultiLanguageText(
                  name: "create_date",
                  defaultValue: "Create date",
                  isLowerCase: false,
                  style: Style(context).styleTextDataColumn,
                ),
              ),
            ),
            Expanded(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  MultiLanguageText(
                    name: "status",
                    defaultValue: "Status",
                    isLowerCase: false,
                    style: Style(context).styleTextDataColumn,
                  ),
                  Tooltip(
                    message: multiLanguageString(name: "active_inactive", defaultValue: "Green = Active\nRed = Inactive ", context: context),
                    child: const Icon(
                      Icons.info,
                      color: Colors.grey,
                      size: 16,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: 38,
            ),
          ],
          rows: [
            for (var resultSprint in resultListSprint)
              CustomDataRow(
                cells: [
                  Expanded(
                    flex: 2,
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Row(
                        children: [
                          const Icon(Icons.description, color: Color.fromRGBO(61, 148, 254, 1)),
                          const SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Text(
                              resultSprint["title"] ?? "",
                              maxLines: 3,
                              overflow: TextOverflow.ellipsis,
                              style: Style(context).styleTextDataCell,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Text(
                      resultSprint["description"] ?? "",
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        resultSprint["sysUser"]["fullname"],
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Center(
                        child: Text(
                          DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultSprint["createDate"]).toLocal()),
                          style: Style(context).styleTextDataCell,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Center(
                      child: BadgeStatusSprintWidget(
                        status: resultSprint["status"],
                      ),
                    ),
                  ),
                  PopupMenuButton(
                    offset: const Offset(5, 50),
                    tooltip: '',
                    splashRadius: 10,
                    itemBuilder: (context) => [
                      PopupMenuItem(
                        padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                        child: ListTile(
                          onTap: () {
                            Navigator.pop(context);
                            Navigator.of(context).push(
                              createRoute(
                                OptionSprintWidget(
                                  type: "edit",
                                  title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                  id: resultSprint["id"],
                                  callbackOptionTest: (result) {
                                    handleEditSprint(result, resultSprint["id"]);
                                  },
                                  prjProjectId: prjProjectId,
                                ),
                              ),
                            );
                          },
                          contentPadding: const EdgeInsets.all(0),
                          hoverColor: Colors.transparent,
                          title: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                          leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
                        ),
                      ),
                      PopupMenuItem(
                        padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                        child: ListTile(
                          onTap: () {
                            Navigator.pop(context);
                            Navigator.of(context).push(
                              createRoute(
                                TestFrameworkRootPageWidget(
                                  child: ManageTaskWidget(
                                    prjSprintId: resultSprint["id"],
                                    prjProjectId: prjProjectId,
                                    type: 2,
                                  ),
                                ),
                              ),
                            );
                          },
                          contentPadding: const EdgeInsets.all(0),
                          hoverColor: Colors.transparent,
                          title: const MultiLanguageText(name: "manage_task", defaultValue: "Manage task"),
                          leading: const Icon(Icons.settings, color: Color.fromARGB(255, 112, 114, 119)),
                        ),
                      ),
                      PopupMenuItem(
                        padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                        child: ListTile(
                          onTap: () {
                            Navigator.pop(context);
                            dialogDeleteSprint(resultSprint);
                          },
                          contentPadding: const EdgeInsets.all(0),
                          hoverColor: Colors.transparent,
                          title: const MultiLanguageText(name: "delete", defaultValue: "Delete"),
                          leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
          ],
        ),
      ),
    );
  }

  dialogDeleteSprint(selectItem) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogDelete(
          selectedItem: selectItem,
          callback: () {
            handleDeleteSprint(selectItem["id"]);
          },
          type: 'Sprint',
        );
      },
    );
  }
}
