import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import '../../../api.dart';
import '../../../common/custom_draggable.dart';
import '../../../common/dynamic_table.dart';
import '../../../components/style.dart';
import '../../../components/toast.dart';
import '../../../model/model.dart';
import '../../../type.dart';

class ManageTaskWidget extends StatefulWidget {
  final String prjSprintId;
  final String prjProjectId;
  final int type;
  const ManageTaskWidget({
    Key? key,
    required this.prjSprintId,
    required this.prjProjectId,
    required this.type,
  }) : super(key: key);

  @override
  State<ManageTaskWidget> createState() => _ManageTaskWidgetState();
}

class _ManageTaskWidgetState extends State<ManageTaskWidget> {
  final TextEditingController nameController = TextEditingController();
  final TextEditingController titleController = TextEditingController();
  List<Task> resultListTask = [];
  var rowCountListTask = 0;
  var currentPage = 1;
  var rowPerPage = 10;
  bool isShowFilterForm = true;
  List<SprintTask> sprintTaskList = [];
  late Future future;
  Widget widgetGhost = Container();
  int oldIndexItem = -1;
  int newIndexItem = -1;
  bool dragAccepted = true;
  int acceptedDataIndex = -1;
  var sprint = {};
  Completer completer = Completer();
  @override
  void initState() {
    super.initState();
    future = setInitValue();
  }

  setInitValue() async {
    final futures = <Future>[
      getListSprintTask(widget.prjSprintId),
      handleSearchTask(currentPage),
      getSprint(widget.prjSprintId),
    ];
    final result = await Future.wait<dynamic>(futures);
    if (result.length == 3) {
      return 0;
    }
  }

  getListSprintTask(String prjSprintId) async {
    var findRequest = {
      "prjSprintId": prjSprintId,
      "prjProjectId": widget.prjProjectId,
    };
    var response = await httpPost("/test-framework-api/user/project/prj-sprint-task/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      setState(() {
        sprintTaskList = List<SprintTask>.from(response["body"]["resultList"].map((e) => SprintTask.fromMap(e)));
      });
    }
  }

  handleSearchTask(page) async {
    var findRequest = {
      "queryOffset": (page - 1) * rowPerPage,
      "queryLimit": rowPerPage,
      "prjProjectId": widget.prjProjectId,
    };
    findRequest["nameLike"] = nameController.text.isNotEmpty ? "%${nameController.text}%" : null;
    findRequest["titleLike"] = titleController.text.isNotEmpty ? "%${titleController.text}%" : null;
    var response = await httpPost("/test-framework-api/user/project/prj-task/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPage = page;
        rowCountListTask = response["body"]["rowCount"];
        resultListTask = List<Task>.from(response["body"]["resultList"].map((e) => Task.fromMap(e)));
      });
    }
  }

  onRemovedSprintTask(id) async {
    var response = await httpDelete("/test-framework-api/user/project/prj-sprint-task/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      if (response["body"].containsKey("errorMessage") && mounted) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      }
    }
  }

  onAddSprintTask(Task prjTask, {int? taskIndex}) async {
    var result = {
      "prjTaskId": prjTask.id,
      "prjSprintId": widget.prjSprintId,
      "prjProjectId": widget.prjProjectId,
      "wfStatusId": prjTask.wfStatusId,
      "taskIndex": taskIndex,
    };
    var response = await httpPut("/test-framework-api/user/project/prj-sprint-task", result, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      if (response["body"].containsKey("errorMessage") && mounted) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      }
    }
  }

  getSprint(String id) async {
    var response = await httpGet("/test-framework-api/user/project/prj-sprint/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        sprint = response["body"]["result"];
      });
    }
  }

  handleEditSprint(result, id) async {
    var response = await httpPost("/test-framework-api/user/project/prj-sprint/$id", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        Provider.of<NavigationModel>(context, listen: false).navigate("/work-mgt/project/${widget.prjProjectId}/board");
      }
    }
  }

  getSprintTaskById(id) async {
    var response = await httpGet("/test-framework-api/user/project/prj-sprint-task/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      return response["body"]["result"];
    }
  }

  handleEditSprintTask(result, id) async {
    var response = await httpPost("/test-framework-api/user/project/prj-sprint-task/$id", result, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      if (response["body"].containsKey("errorMessage") && mounted) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
        return false;
      }
      return true;
    }
    return false;
  }

  @override
  void dispose() {
    super.dispose();
    nameController.dispose();
    titleController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: future,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        headerWidget(context),
                        const SizedBox(
                          height: 10,
                        ),
                        filterFormWidget(),
                        const SizedBox(
                          height: 10,
                        ),
                        LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
                          if (constraints.maxWidth >= 1200) {
                            return Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                    child: tableTaskWidget(
                                        multiLanguageString(name: "list_task_Search", defaultValue: "List Task Search", context: context),
                                        "List Task Search",
                                        resultListTask,
                                        true)),
                                const SizedBox(
                                  width: 10,
                                ),
                                Expanded(child: tableSprintTaskWidget(true)),
                              ],
                            );
                          } else {
                            return Column(
                              children: [
                                tableTaskWidget(multiLanguageString(name: "list_task_Search", defaultValue: "List Task Search", context: context),
                                    "List Task Search", resultListTask, false),
                                const SizedBox(
                                  height: 10,
                                ),
                                tableSprintTaskWidget(false),
                              ],
                            );
                          }
                        }),
                      ],
                    ),
                  ),
                ),
              ),
              btnWidget(context),
            ],
          );
        } else if (snapshot.hasError) {
          return Text('${snapshot.error}');
        }
        return const Center(child: CircularProgressIndicator());
      },
    );
  }

  Column tableSprintTaskWidget(bool isExpand) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MultiLanguageText(
          name: "list_sprint_task",
          defaultValue: "List Sprint Task",
          style: Style(context).styleTitleDialog,
        ),
        Container(
          padding: const EdgeInsets.all(15),
          decoration: BoxDecoration(
            color: Colors.grey[200],
            border: const Border(
              top: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
              bottom: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
              left: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
              right: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
            ),
          ),
          child: Row(
            children: [
              Expanded(
                child: MultiLanguageText(
                  name: "name",
                  defaultValue: "Name",
                  isLowerCase: false,
                  style: Style(context).styleTextDataColumn,
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5),
                  child: MultiLanguageText(
                      name: "assigned_to", defaultValue: "Assigned To", isLowerCase: false, style: Style(context).styleTextDataColumn),
                ),
              ),
              Container(
                width: 56,
              ),
            ],
          ),
        ),
        for (var i = 0; i < sprintTaskList.length; i++)
          DragTarget(
            builder: (
              BuildContext context,
              List<dynamic> accepted,
              List<dynamic> rejected,
            ) {
              return Column(
                children: [
                  if (acceptedDataIndex == i)
                    Opacity(
                      opacity: 0.5,
                      child: widgetGhost,
                    ),
                  CustomDraggable(
                    data: sprintTaskList[i],
                    feedback: Container(
                        width: isExpand ? (MediaQuery.of(context).size.width - 175) / 2 - 5 : MediaQuery.of(context).size.width - 175,
                        color: Colors.white,
                        child: childTaskWidget(sprintTaskList[i].prjTask)),
                    childWhenDragging: const SizedBox.shrink(),
                    child: childTaskWidget(sprintTaskList[i].prjTask, onRemove: () async {
                      await onRemovedSprintTask(sprintTaskList[i].id);
                      getListSprintTask(widget.prjSprintId);
                    }),
                    onDragStarted: () {
                      setState(() {
                        oldIndexItem = i;
                        acceptedDataIndex = i;
                        dragAccepted = false;
                      });
                    },
                    onDragEnd: (draggableDetails) {
                      if (completer.isCompleted) {
                        acceptedDataIndex = -1;
                        completer = Completer();
                      } else {
                        completer.future.then((_) {
                          acceptedDataIndex = -1;
                          completer = Completer();
                        });
                      }
                    },
                  ),
                ],
              );
            },
            onLeave: (data) {
              acceptedDataIndex = -1;
            },
            onWillAccept: (data) {
              if (data is Task && (!dragAccepted || sprintTaskList.every((element) => element.prjTask.id != data.id))) {
                setState(() {
                  acceptedDataIndex = i;
                  widgetGhost = childTaskWidget(data);
                });
              } else if (data is SprintTask) {
                setState(() {
                  acceptedDataIndex = i;
                  widgetGhost = childTaskWidget(data.prjTask);
                });
              }
              return true;
            },
            onAccept: (Dragable data) async {
              if (data is Task && (!dragAccepted || sprintTaskList.every((element) => element.prjTask.id != data.id))) {
                await onAddSprintTask(data, taskIndex: sprintTaskList[i].taskIndex);
                getListSprintTask(widget.prjSprintId);
              }
              if (data is SprintTask && data.taskIndex != sprintTaskList[i].taskIndex) {
                var result = await getSprintTaskById(data.id);
                result["taskIndex"] = sprintTaskList[i].taskIndex - 1;
                bool isSuccess = await handleEditSprintTask(result, data.id);
                if (mounted && isSuccess) {
                  getListSprintTask(widget.prjSprintId);
                }
              }
              setState(() {
                acceptedDataIndex = -1;
                dragAccepted = true;
              });
              completer.complete();
            },
          ),
        DragTarget(
          builder: (
            BuildContext context,
            List<dynamic> accepted,
            List<dynamic> rejected,
          ) {
            return Column(
              children: [
                if (acceptedDataIndex == -2)
                  Opacity(
                    opacity: 0.5,
                    child: widgetGhost,
                  ),
                Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 13),
                    decoration: const BoxDecoration(
                      border: Border(
                        top: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
                        bottom: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
                        left: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
                        right: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
                      ),
                    ),
                    child: Column(
                      children: [
                        MultiLanguageText(
                          name: "drop_here",
                          defaultValue: "Drop here",
                          style: TextStyle(
                              color: const Color.fromRGBO(61, 90, 254, 1),
                              fontSize: context.fontSizeManager.fontSizeText20,
                              fontWeight: FontWeight.bold),
                        ),
                        if (sprintTaskList.isEmpty)
                          Image.asset(
                            "assets/images/drag-and-drop.jpg",
                            // width: 50,
                            height: 200,
                          ),
                      ],
                    )),
              ],
            );
          },
          onLeave: (data) {
            acceptedDataIndex = -1;
          },
          onWillAccept: (data) {
            if (data is Task && sprintTaskList.every((element) => element.prjTask.id != data.id)) {
              setState(() {
                acceptedDataIndex = -2;
                widgetGhost = childTaskWidget(data);
              });
            } else if (data is SprintTask) {
              setState(() {
                acceptedDataIndex = -2;
                widgetGhost = childTaskWidget(data.prjTask);
              });
            }
            return true;
          },
          onAccept: (Dragable data) async {
            if (data is Task && sprintTaskList.every((element) => element.prjTask.id != data.id)) {
              await onAddSprintTask(data);
              getListSprintTask(widget.prjSprintId);
            }
            if (data is SprintTask) {
              var result = await getSprintTaskById(data.id);
              result["taskIndex"] = null;
              bool isSuccess = await handleEditSprintTask(result, data.id);
              if (mounted && isSuccess) {
                getListSprintTask(widget.prjSprintId);
              }
            }
            setState(() {
              acceptedDataIndex = -1;
            });
            completer.complete();
          },
        )
      ],
    );
  }

  Column tableTaskWidget(String title, String label, List<Task> list, bool isExpand) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: Style(context).styleTitleDialog,
        ),
        Container(
          padding: const EdgeInsets.all(15),
          decoration: BoxDecoration(
            color: Colors.grey[200],
            border: const Border(
              top: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
              bottom: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
              left: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
              right: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
            ),
          ),
          child: Row(
            children: [
              Expanded(
                child: MultiLanguageText(
                  name: "name",
                  defaultValue: "NAME",
                  isLowerCase: false,
                  style: Style(context).styleTextDataColumn,
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5),
                  child: MultiLanguageText(
                      name: "assigned_to", defaultValue: "Assigned To", isLowerCase: false, style: Style(context).styleTextDataColumn),
                ),
              ),
            ],
          ),
        ),
        for (var item in list)
          CustomDraggable(
            data: item,
            feedback: Container(
                width: isExpand ? (MediaQuery.of(context).size.width - 175) / 2 - 5 : MediaQuery.of(context).size.width - 175,
                color: Colors.white,
                child: childTaskWidget(item)),
            childWhenDragging: childTaskWidget(item),
            child: childTaskWidget(item),
          ),
        DynamicTablePagging(
          rowCountListTask,
          currentPage,
          rowPerPage,
          pageChangeHandler: (page) {
            handleSearchTask(page);
          },
          rowPerPageChangeHandler: (rowPerPage) {
            setState(() {
              this.rowPerPage = rowPerPage!;
              handleSearchTask(1);
            });
          },
        ),
      ],
    );
  }

  Container childTaskWidget(Task item, {Function? onRemove}) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
      decoration: const BoxDecoration(
        border: Border(
          top: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
          bottom: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
          left: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
          right: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
        ),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Text(
              item.title,
              style: TextStyle(
                  fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1), decoration: TextDecoration.none),
            ),
          ),
          Expanded(
            child: item.assigneeSysUser != null
                ? Row(
                    children: [
                      const Icon(Icons.account_circle, color: Color.fromRGBO(137, 172, 238, 1)),
                      const SizedBox(
                        width: 5,
                      ),
                      Expanded(
                        child: Text(
                          item.assigneeSysUser?.fullname ?? "",
                          style: TextStyle(
                              fontSize: context.fontSizeManager.fontSizeText16,
                              color: const Color.fromRGBO(90, 143, 241, 1),
                              fontWeight: FontWeight.bold,
                              decoration: TextDecoration.none),
                        ),
                      ),
                    ],
                  )
                : const SizedBox.shrink(),
          ),
          SizedBox(
            width: 56,
            height: 35,
            child: onRemove != null
                ? ElevatedButton(
                    onPressed: () => onRemove(),
                    child: const Icon(Icons.remove),
                  )
                : const SizedBox.shrink(),
          ),
        ],
      ),
    );
  }

  Column filterFormWidget() {
    return Column(
      children: [
        Row(
          children: [
            MultiLanguageText(
              name: "filter_form",
              defaultValue: "Filter form",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: const Color.fromRGBO(0, 0, 0, 0.8),
                  fontSize: context.fontSizeManager.fontSizeText16,
                  letterSpacing: 2),
            ),
            IconButton(
              onPressed: () {
                setState(() {
                  isShowFilterForm = !isShowFilterForm;
                });
              },
              icon: isShowFilterForm ? const Icon(Icons.visibility) : const Icon(Icons.visibility_off),
              splashRadius: 1,
              tooltip: isShowFilterForm
                  ? multiLanguageString(name: "hide_filter_form", defaultValue: "Hide filter form", context: context)
                  : multiLanguageString(name: "show_filter_form", defaultValue: "Show filter form", context: context),
            )
          ],
        ),
        if (isShowFilterForm)
          Row(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: SizedBox(
                    height: 50,
                    child: TextField(
                      controller: nameController,
                      decoration: InputDecoration(
                        label: const MultiLanguageText(name: "code", defaultValue: "Code"),
                        contentPadding: const EdgeInsets.fromLTRB(10, 0, 10, 20),
                        border: const OutlineInputBorder(),
                        hintText: multiLanguageString(name: "by_code", defaultValue: "By code", context: context),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: SizedBox(
                    height: 50,
                    child: TextField(
                      controller: titleController,
                      decoration: InputDecoration(
                        label: const MultiLanguageText(name: "name", defaultValue: "Name"),
                        contentPadding: const EdgeInsets.fromLTRB(10, 0, 10, 20),
                        border: const OutlineInputBorder(),
                        hintText: multiLanguageString(name: "by_name", defaultValue: "By name", context: context),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        if (isShowFilterForm)
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: ElevatedButton(
                    onPressed: () {
                      handleSearchTask(currentPage);
                    },
                    child: const MultiLanguageText(name: "search", defaultValue: "Search", isLowerCase: false)),
              ),
              ElevatedButton(
                  onPressed: () {
                    setState(() {
                      nameController.clear();
                      titleController.clear();
                      handleSearchTask(currentPage);
                    });
                  },
                  child: const MultiLanguageText(name: "reset", defaultValue: "Reset", isLowerCase: false))
            ],
          ),
      ],
    );
  }

  Column headerWidget(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            MultiLanguageText(
              name: "active_sprint",
              defaultValue: "Active Sprint",
              style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
            ),
            if (widget.type == 2)
              TextButton(
                onPressed: () => Navigator.pop(context, 'cancel'),
                child: const Icon(
                  Icons.close,
                  color: Colors.black,
                ),
              ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        MultiLanguageText(
          name: "sprint_name_map",
          defaultValue: "Sprint name: \$0",
          variables: ["${sprint['title']}"],
          style: TextStyle(fontSize: context.fontSizeManager.fontSizeText22, fontWeight: FontWeight.bold),
        ),
        Text(
          "${DateFormat('dd/MM/yyyy').format(DateTime.parse(sprint["startDate"]))} - ${DateFormat('dd/MM/yyyy').format(DateTime.parse(sprint["endDate"]))}",
          style: TextStyle(fontSize: context.fontSizeManager.fontSizeText18, fontWeight: FontWeight.w500),
        ),
      ],
    );
  }

  Padding btnWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          // SizedBox(
          //   height: 40,
          //   width: 80,
          //   child: ElevatedButton(
          //     onPressed: () {
          //       List<SprintTask> removedSprintTask =
          //           oldSprintTaskList.where((oldObj) => !newSprintTaskList.any((newObj) => newObj.id == oldObj.prjTaskId)).toList();
          //       List<Task> addedSprintTask =
          //           newSprintTaskList.where((newObj) => !oldSprintTaskList.any((oldObj) => oldObj.prjTaskId == newObj.id)).toList();
          //       onChangeSprintTask(removedSprintTask, addedSprintTask);
          //     },
          //     child: const Text(
          //       "OK",
          //       style: TextStyle(color: Colors.white),
          //     ),
          //   ),
          // ),
          if (sprint["status"] != 2)
            SizedBox(
              height: 40,
              width: 110,
              child: ElevatedButton(
                  onPressed: () {
                    sprint["status"] = (sprint["status"] == 0) ? 1 : 2;
                    handleEditSprint(sprint, widget.prjSprintId);
                  },
                  child: (sprint["status"] == 0)
                      ? const MultiLanguageText(name: "active", defaultValue: "Active", isLowerCase: false, style: TextStyle(color: Colors.white))
                      : (sprint["status"] == 1)
                          ? const MultiLanguageText(
                              name: "complete", defaultValue: "Complete", isLowerCase: false, style: TextStyle(color: Colors.white))
                          : const Text("")),
            ),
          if (widget.type == 2)
            Container(
              margin: const EdgeInsets.only(left: 10),
              height: 40,
              width: 120,
              child: ElevatedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                style: ElevatedButton.styleFrom(
                  side: const BorderSide(color: Color.fromRGBO(209, 212, 216, 1)),
                  backgroundColor: Colors.white,
                  foregroundColor: const Color.fromRGBO(61, 90, 254, 1),
                ),
                child: const MultiLanguageText(
                  name: "cancel",
                  defaultValue: "Cancel",
                  isLowerCase: false,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
            )
        ],
      ),
    );
  }
}
