import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:test_framework_view/components/multi_language_text.dart';

import 'package:test_framework_view/page/work_mgt/sprint/manage_task_widget.dart';

import '../../../api.dart';
import '../../../components/toast.dart';
import '../../../model/model.dart';

class ActiveSprintWidget extends StatefulWidget {
  const ActiveSprintWidget({Key? key}) : super(key: key);

  @override
  State<ActiveSprintWidget> createState() => _ActiveSprintWidgetState();
}

class _ActiveSprintWidgetState extends State<ActiveSprintWidget> {
  String? prjSprintId;
  String prjProjectId = "";
  bool isLoad = true;
  @override
  void initState() {
    prjProjectId = Provider.of<NavigationModel>(context, listen: false).prjProjectId ?? "";
    super.initState();
    getInitPage();
  }

  getInitPage() async {
    await getSprint(1);
    if (prjSprintId == null) await getSprint(0);
    setState(() {
      isLoad = false;
    });
  }

  getSprint(status) async {
    var findRequest = {
      "prjProjectId": prjProjectId,
      "status": status,
    };
    var response = await httpPost("/test-framework-api/user/project/prj-sprint/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false && response["body"]["resultList"].isNotEmpty) {
      setState(() {
        prjSprintId = response["body"]["resultList"][0]["id"];
      });
    }
    return 0;
  }

  handleAddSprint(result) async {
    var response = await httpPut("/test-framework-api/user/project/prj-sprint", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        return response["body"];
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return (isLoad)
          ? const Center(child: CircularProgressIndicator())
          : (prjSprintId != null)
              ? ManageTaskWidget(
                  prjSprintId: prjSprintId!,
                  prjProjectId: prjProjectId,
                  type: 1,
                )
              : const SizedBox.shrink();
    });
  }
}
