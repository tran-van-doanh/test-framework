import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';

import '../../../common/custom_state.dart';
import '../../../common/dynamic_dropdown_button.dart';

class HeaderBugTrackerWidget extends StatefulWidget {
  final int countList;
  final Function? callbackFilter;
  final Function? callbackResend;

  const HeaderBugTrackerWidget({
    Key? key,
    required this.countList,
    this.callbackFilter,
    this.callbackResend,
  }) : super(key: key);

  @override
  State<HeaderBugTrackerWidget> createState() => _HeaderBugTrackerWidgetState();
}

class _HeaderBugTrackerWidgetState extends CustomState<HeaderBugTrackerWidget> {
  final TextEditingController descriptionController = TextEditingController();
  final TextEditingController tfTestCaseNameController = TextEditingController();
  final TextEditingController tfTestCaseCodeController = TextEditingController();
  SingleValueDropDownController bugTrackerTypeController = SingleValueDropDownController();
  bool isShowFilterForm = true;
  bool isSearched = false;

  handleCallBackSearchFunction({String type = "search"}) {
    var result = {
      "bugTrackerType": bugTrackerTypeController.dropDownValue?.value,
      "descriptionLike": descriptionController.text,
      "tfTestCaseTitleLike": tfTestCaseNameController.text,
      "tfTestCaseNameLike": tfTestCaseCodeController.text,
    };
    if (type == "resend") {
      widget.callbackResend!(result);
    } else {
      widget.callbackFilter!(result);
    }
  }

  @override
  void dispose() {
    descriptionController.dispose();
    tfTestCaseNameController.dispose();
    tfTestCaseCodeController.dispose();
    bugTrackerTypeController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      List<Widget> widgetList = [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
            child: SizedBox(
              height: 50,
              child: DynamicTextField(
                controller: tfTestCaseCodeController,
                onComplete: () => handleCallBackSearchFunction(),
                onChanged: (value) {
                  setState(() {
                    tfTestCaseCodeController.text;
                  });
                },
                labelText: multiLanguageString(name: "search_test_code", defaultValue: "Search test code", context: context),
                hintText: multiLanguageString(name: "by_test_code", defaultValue: "By test code", context: context),
                suffixIcon: (tfTestCaseCodeController.text != "")
                    ? IconButton(
                        onPressed: () {
                          setState(() {
                            tfTestCaseCodeController.clear();
                            if (isSearched) {
                              handleCallBackSearchFunction();
                            }
                          });
                        },
                        icon: const Icon(Icons.clear),
                        splashRadius: 20,
                      )
                    : null,
              ),
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
            child: SizedBox(
              height: 50,
              child: DynamicTextField(
                controller: tfTestCaseNameController,
                onComplete: () => handleCallBackSearchFunction(),
                onChanged: (value) {
                  setState(() {
                    tfTestCaseNameController.text;
                  });
                },
                labelText: multiLanguageString(name: "search_test_name", defaultValue: "Search test name", context: context),
                hintText: multiLanguageString(name: "by_test_name", defaultValue: "By test name", context: context),
                suffixIcon: (tfTestCaseNameController.text != "")
                    ? IconButton(
                        onPressed: () {
                          setState(() {
                            tfTestCaseNameController.clear();
                            if (isSearched) {
                              handleCallBackSearchFunction();
                            }
                          });
                        },
                        icon: const Icon(Icons.clear),
                        splashRadius: 20,
                      )
                    : null,
              ),
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
            child: SizedBox(
              height: 50,
              child: DynamicTextField(
                controller: descriptionController,
                onComplete: () => handleCallBackSearchFunction(),
                onChanged: (value) {
                  setState(() {
                    descriptionController.text;
                  });
                },
                labelText: multiLanguageString(name: "search_description", defaultValue: "Search description", context: context),
                hintText: multiLanguageString(name: "by_description", defaultValue: "By description", context: context),
                suffixIcon: (descriptionController.text != "")
                    ? IconButton(
                        onPressed: () {
                          setState(() {
                            descriptionController.clear();
                            if (isSearched) {
                              handleCallBackSearchFunction();
                            }
                          });
                        },
                        icon: const Icon(Icons.clear),
                        splashRadius: 20,
                      )
                    : null,
              ),
            ),
          ),
        ),
        Expanded(
          child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
              child: DynamicDropdownSearchClearOption(
                onChanged: (dynamic newValue) {
                  if (isSearched && newValue == "") {
                    handleCallBackSearchFunction();
                  }
                },
                labelText: multiLanguageString(name: "bug_tracker_type", defaultValue: "Bug Tracker Type", context: context),
                controller: bugTrackerTypeController,
                hint: multiLanguageString(name: "search_bug_tracker_type", defaultValue: "Search Bug Tracker Type", context: context),
                items: [
                  {"value": "LLQ", "name": "LLQ Work management"},
                  {"value": "JIRA", "name": "Jira"},
                  {"value": "MANTIS", "name": "Mantis"}
                ].map<DropDownValueModel>((dynamic result) {
                  return DropDownValueModel(
                    value: result["value"],
                    name: result["name"],
                  );
                }).toList(),
                maxHeight: 200,
              )),
        ),
      ];
      var itemPerRow = 4;
      if (constraints.maxWidth < 600) {
        itemPerRow = 1;
      } else if (constraints.maxWidth < 800) {
        itemPerRow = 2;
      } else if (constraints.maxWidth < 1200) {
        itemPerRow = 3;
      }
      var rowCount = (1.0 * widgetList.length / itemPerRow).ceil();
      var filterWidget = Column(
        children: [
          for (var rowI = 0; rowI < rowCount; rowI++)
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                for (var colI = 0; colI < itemPerRow; colI++)
                  rowI * itemPerRow + colI < widgetList.length ? widgetList[rowI * itemPerRow + colI] : Expanded(child: Container())
              ],
            )
        ],
      );
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MultiLanguageText(
            name: "bug_tracker_count",
            defaultValue: "Bug tracker (\$0)",
            variables: ["${widget.countList}"],
            style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
          ),
          Row(
            children: [
              MultiLanguageText(
                name: "filter_form",
                defaultValue: "Filter form",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: const Color.fromRGBO(0, 0, 0, 0.8),
                    fontSize: context.fontSizeManager.fontSizeText16,
                    letterSpacing: 2),
              ),
              IconButton(
                onPressed: () {
                  setState(() {
                    isShowFilterForm = !isShowFilterForm;
                  });
                },
                icon: isShowFilterForm ? const Icon(Icons.visibility) : const Icon(Icons.visibility_off),
                splashRadius: 1,
                tooltip: isShowFilterForm
                    ? multiLanguageString(name: "hide_filter_form", defaultValue: "Hide filter form", context: context)
                    : multiLanguageString(name: "show_filter_form", defaultValue: "Show filter form", context: context),
              )
            ],
          ),
          if (isShowFilterForm) filterWidget,
          if (isShowFilterForm)
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 20),
                  child: ElevatedButton(
                      onPressed: () {
                        isSearched = true;
                        handleCallBackSearchFunction();
                      },
                      child: const MultiLanguageText(name: "search", defaultValue: "Search", isLowerCase: false)),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 20),
                  child: ElevatedButton(
                      onPressed: () {
                        isSearched = true;
                        handleCallBackSearchFunction(type: "resend");
                      },
                      child: const MultiLanguageText(name: "resend", defaultValue: "Resend", isLowerCase: false)),
                ),
                if (widgetList.length > 2)
                  ElevatedButton(
                      onPressed: () {
                        setState(() {
                          descriptionController.clear();
                          tfTestCaseCodeController.clear();
                          tfTestCaseNameController.clear();
                          bugTrackerTypeController.clearDropDown();
                          widget.callbackFilter!({});
                        });
                      },
                      child: const MultiLanguageText(name: "reset", defaultValue: "Reset", isLowerCase: false))
              ],
            )
        ],
      );
    });
  }
}
