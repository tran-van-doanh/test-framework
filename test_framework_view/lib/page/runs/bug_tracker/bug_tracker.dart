import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/components/badge/badge_text.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/runs/bug_tracker/header_bug_tracker.dart';

import '../../../common/custom_state.dart';
import '../../../components/menu_tab.dart';

class BugTrackerWidget extends StatefulWidget {
  const BugTrackerWidget({
    Key? key,
  }) : super(key: key);
  @override
  State<BugTrackerWidget> createState() => _BugTrackerWidgetState();
}

class _BugTrackerWidgetState extends CustomState<BugTrackerWidget> {
  late Future futureResultBugTracker;
  var rowCount = 0;
  var currentPage = 1;
  var resultListBugTracker = [];
  var rowPerPage = 5;
  var searchRequest = {};
  var prjProjectId = "";

  @override
  void initState() {
    super.initState();
    prjProjectId = Provider.of<NavigationModel>(context, listen: false).prjProjectId ?? "";
    futureResultBugTracker = getListTfTestBugTracker(currentPage);
  }

  getListTfTestBugTracker(page) async {
    if ((page - 1) * rowPerPage > rowCount) {
      page = (1.0 * rowCount / rowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    Map<dynamic, dynamic> findRequest = {
      "queryOffset": (page - 1) * rowPerPage,
      "queryLimit": rowPerPage,
      "prjProjectId": prjProjectId,
      // "status": 3,
    };
    findRequest.addAll(searchRequest);
    if (searchRequest["descriptionLike"] != null && searchRequest["descriptionLike"].isNotEmpty) {
      findRequest["descriptionLike"] = "%${searchRequest["descriptionLike"]}%";
    }
    if (searchRequest["tfTestCaseNameLike"] != null && searchRequest["tfTestCaseNameLike"].isNotEmpty) {
      findRequest["tfTestCaseNameLike"] = "%${searchRequest["tfTestCaseNameLike"]}%";
    }
    if (searchRequest["tfTestCaseTitleLike"] != null && searchRequest["tfTestCaseTitleLike"].isNotEmpty) {
      findRequest["tfTestCaseTitleLike"] = "%${searchRequest["tfTestCaseTitleLike"]}%";
    }
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-run-data-bug-tracker/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPage = page;
        rowCount = response["body"]["rowCount"];
        resultListBugTracker = response["body"]["resultList"];
      });
      return 0;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return FutureBuilder(
          future: futureResultBugTracker,
          builder: (context, dataBugTracker) {
            if (dataBugTracker.hasData) {
              return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
                return Material(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: ListView(
                          children: [
                            NavMenuTabV1(
                              currentUrl: "/runs/bug-tracker",
                              margin: const EdgeInsets.fromLTRB(50, 20, 50, 0),
                              navigationList: context.testRunsMenuList,
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(50, 20, 50, 30),
                              child: Column(
                                children: [
                                  HeaderBugTrackerWidget(
                                    countList: rowCount,
                                    callbackFilter: (result) {
                                      searchRequest = result;
                                      futureResultBugTracker = getListTfTestBugTracker(currentPage);
                                    },
                                    callbackResend: (result) async {
                                      searchRequest = result;
                                      var findRequestTestCase = {};
                                      findRequestTestCase.addAll(searchRequest);
                                      findRequestTestCase["prjProjectId"] = prjProjectId;
                                      if (searchRequest["descriptionLike"] != null && searchRequest["descriptionLike"].isNotEmpty) {
                                        findRequestTestCase["descriptionLike"] = "%${searchRequest["descriptionLike"]}%";
                                      }
                                      if (findRequestTestCase.containsKey("queryOffset")) {
                                        findRequestTestCase.remove("queryOffset");
                                      }
                                      if (findRequestTestCase.containsKey("queryLimit")) {
                                        findRequestTestCase.remove("queryLimit");
                                      }
                                      var response = await httpPost("/test-framework-api/user/test-framework/tf-test-run-data-bug-tracker/resend",
                                          findRequestTestCase, context);
                                      if (response.containsKey("body") && response["body"] is String == false) {
                                        if (response["body"].containsKey("errorMessage") && mounted) {
                                          showToast(
                                              context: this.context,
                                              msg: response["body"]["errorMessage"],
                                              color: Colors.red,
                                              icon: const Icon(Icons.error));
                                        } else if (mounted) {
                                          showToast(
                                              msg: multiLanguageString(
                                                  name: "resend_successful", defaultValue: "Resend successful", context: this.context),
                                              color: Colors.greenAccent,
                                              icon: const Icon(Icons.error),
                                              context: this.context);
                                        }
                                      }
                                    },
                                  ),
                                  if (resultListBugTracker.isNotEmpty) tableWidget(constraints),
                                  SelectionArea(
                                    child: DynamicTablePagging(
                                      rowCount,
                                      currentPage,
                                      rowPerPage,
                                      pageChangeHandler: (page) {
                                        futureResultBugTracker = getListTfTestBugTracker(page);
                                      },
                                      rowPerPageChangeHandler: (value) {
                                        setState(() {
                                          rowPerPage = value;
                                          futureResultBugTracker = getListTfTestBugTracker(1);
                                        });
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              });
            } else if (dataBugTracker.hasError) {
              return Text('${dataBugTracker.error}');
            }
            return const Center(child: CircularProgressIndicator());
          });
    });
  }

  SelectionArea tableWidget(BoxConstraints constraints) {
    return SelectionArea(
      child: Container(
        margin: const EdgeInsets.only(top: 40),
        child: (constraints.maxWidth < 1200)
            ? SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: SizedBox(
                  width: 1200,
                  child: tableBig(),
                ),
              )
            : tableBig(),
      ),
    );
  }

  Column tableBig() {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.all(15),
          decoration: const BoxDecoration(
            border: Border(
              top: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
              bottom: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
              left: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
              right: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
            ),
          ),
          child: Row(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5),
                  child:
                      MultiLanguageText(name: "test_code", defaultValue: "Test code", isLowerCase: false, style: Style(context).styleTextDataColumn),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5),
                  child:
                      MultiLanguageText(name: "test_name", defaultValue: "Test name", isLowerCase: false, style: Style(context).styleTextDataColumn),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5),
                  child: MultiLanguageText(name: "type", defaultValue: "Type", isLowerCase: false, style: Style(context).styleTextDataColumn),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5),
                  child: MultiLanguageText(name: "url", defaultValue: "Url", isLowerCase: false, style: Style(context).styleTextDataColumn),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5),
                  child: MultiLanguageText(
                      name: "description", defaultValue: "Description", isLowerCase: false, style: Style(context).styleTextDataColumn),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      MultiLanguageText(
                        name: "status",
                        defaultValue: "Status",
                        isLowerCase: false,
                        style: Style(context).styleTextDataColumn,
                      ),
                      Tooltip(
                        message: multiLanguageString(
                            name: "run_pass_run_fail_running_new_warning",
                            defaultValue: "Green = Run pass\nRed = Run fail\nOrange = Running\nBlue = New\nYellow = Warning",
                            context: context),
                        child: const Icon(
                          Icons.info,
                          color: Colors.grey,
                          size: 16,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        for (var testBugTracker in resultListBugTracker)
          RowTableBugTrackerWidget(
            testBugTracker,
            prjProjectId,
          ),
      ],
    );
  }
}

class RowTableBugTrackerWidget extends StatefulWidget {
  final dynamic rowData;
  final String prjProjectId;
  const RowTableBugTrackerWidget(this.rowData, this.prjProjectId, {Key? key}) : super(key: key);

  @override
  State<RowTableBugTrackerWidget> createState() => _RowTableBugTrackerWidgetState();
}

class _RowTableBugTrackerWidgetState extends CustomState<RowTableBugTrackerWidget> {
  @override
  Widget build(BuildContext context) {
    return Consumer<NavigationModel>(
      builder: (context, navigationModel, child) {
        return GestureDetector(
          onTap: () async {
            var responseGetTestRunData =
                await httpGet("/test-framework-api/user/test-framework/tf-test-run-data/${widget.rowData["tfTestRunDataId"]}", context);
            if (responseGetTestRunData.containsKey("body") && responseGetTestRunData["body"] is String == false) {
              navigationModel.navigate(
                  "/qa-platform/project/${navigationModel.prjProjectId}/runs/${responseGetTestRunData["body"]["result"]["tfTestCase"]["testCaseType"]}/test-run-data/${widget.rowData["tfTestRunDataId"]}");
            }
          },
          child: Container(
            padding: const EdgeInsets.all(15),
            decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
                bottom: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
                left: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
                right: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
              ),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Wrap(
                      children: [
                        Text(
                          widget.rowData["tfTestRun"]["tfTestCase"]["name"],
                          softWrap: true,
                          style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Wrap(
                      children: [
                        Text(
                          widget.rowData["tfTestRun"]["tfTestCase"]["title"],
                          softWrap: true,
                          style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Wrap(
                      children: [
                        Text(
                          widget.rowData["bugTrackerType"] ?? "",
                          softWrap: true,
                          style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Wrap(
                      children: [
                        Text(
                          widget.rowData["bugTrackerUrl"] ?? "",
                          softWrap: true,
                          style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Text(
                      widget.rowData["description"] ?? "",
                      softWrap: true,
                      style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                    ),
                  ),
                ),
                Expanded(
                  child: Center(
                    child: BadgeTextBugTrackerItemStatusWidget(
                      status: widget.rowData["status"],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
