import 'dart:io';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:pie_chart/pie_chart.dart' as pie;
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/custom_state.dart';
import 'package:test_framework_view/common/file_download.dart';
import 'package:test_framework_view/components/badge/badge_text.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/runs/batch_run/batch_run_dashboard.dart';

import '../../../common/dynamic_table.dart';
import '../../../components/style.dart';

class TestBatchRunDetailWidget extends StatefulWidget {
  final String id;
  final String prjProjectId;
  const TestBatchRunDetailWidget({Key? key, required this.id, required this.prjProjectId}) : super(key: key);

  @override
  State<TestBatchRunDetailWidget> createState() => _TestBatchRunDetailWidgetState();
}

class _TestBatchRunDetailWidgetState extends CustomState<TestBatchRunDetailWidget> with TickerProviderStateMixin {
  late Future future;
  var resultTestBatchRunDetail = {};
  var resultBatchRunStep = [];
  var searchRequest = {};
  var rowCountBatchRunStep = 0;
  var currentPageBatchRunStep = 1;
  var rowPerPageBatchRunStep = 10;
  Map<dynamic, dynamic> dataMapPieChar = {};

  String? currentTab;
  @override
  void initState() {
    super.initState();
    currentTab = multiLanguageString(name: "dashboard", defaultValue: "Dashboard", context: context);
    future = getInitPage();
  }

  getInitPage() async {
    await getTestBatchRunDetail(widget.id);
    await getBatchRunStep(widget.id, currentPageBatchRunStep);
    await getTestBatchRunReport(widget.id);
    return 0;
  }

  getTestBatchRunReport(id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-batch-run/$id/report", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        response["body"]["result"].remove("id");
        response["body"]["result"].remove("dataCount");
        dataMapPieChar = response["body"]["result"];
      });
    }
  }

  getBatchRunStep(String id, page) async {
    if ((page - 1) * rowPerPageBatchRunStep > rowCountBatchRunStep) {
      page = (1.0 * rowCountBatchRunStep / rowPerPageBatchRunStep).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {
      "tfTestBatchRunId": id,
      "prjProjectId": widget.prjProjectId,
      "queryOffset": (page - 1) * rowPerPageBatchRunStep,
      "queryLimit": rowPerPageBatchRunStep
    };
    findRequest["tfTestRunStatus"] = searchRequest["status"];
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-batch-run-step/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPageBatchRunStep = page;
        rowCountBatchRunStep = response["body"]["rowCount"];
        resultBatchRunStep = response["body"]["resultList"];
      });
    }
    return 0;
  }

  getTestBatchRunDetail(String id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-batch-run/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        resultTestBatchRunDetail = response["body"]["result"];
      });
    }
  }

  getTestRun(String tfTestBatchRunId, String tfTestBatchRunStepId) async {
    var findRequest = {
      "tfTestBatchRunId": tfTestBatchRunId,
      "tfTestBatchRunStepId": tfTestBatchRunStepId,
      "prjProjectId": widget.prjProjectId,
    };
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-run/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      return response["body"]["resultList"].isNotEmpty ? response["body"]["resultList"][0] : null;
    }
    return null;
  }

  downloadCode(id) async {
    File? downloadedFile = await httpDownloadFile(context, "/test-framework-api/user/test-framework-file/tf-test-run/$id/source-code",
        Provider.of<SecurityModel>(context, listen: false).authorization, 'Output.zip');
    if (downloadedFile != null && mounted) {
      var snackBar = SnackBar(
        content: MultiLanguageText(name: "file_downloaded_to", defaultValue: "File downloaded to \$0 .", variables: ["$downloadedFile"]),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  downloadResult(id) async {
    File? downloadedFile = await httpDownloadFile(context, "/test-framework-api/user/test-framework-file/tf-test-run/$id/export-result",
        Provider.of<SecurityModel>(context, listen: false).authorization, 'Output.xlsx');
    if (downloadedFile != null && mounted) {
      var snackBar = SnackBar(
        content: MultiLanguageText(name: "file_downloaded_to", defaultValue: "File downloaded to \$0 .", variables: ["$downloadedFile"]),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, LanguageModel>(
      builder: (context, navigationModel, languageModel, child) {
        return FutureBuilder(
          future: future,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView(
                padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
                children: [
                  NavMenuTabV1(
                    currentUrl: "/runs/test_batch",
                    margin: const EdgeInsets.only(bottom: 20),
                    navigationList: context.testRunsMenuList,
                  ),
                  headerWidget(navigationModel),
                  tabControlWidget(navigationModel),
                ],
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  Column tabControlWidget(navigationModel) {
    List<Map<dynamic, dynamic>> listTabItem = [
      {
        "name": multiLanguageString(name: "dashboard", defaultValue: "Dashboard", context: context),
        "widget": BatchRunDashBoardPageWidget(tfTestBatchRunId: widget.id),
      },
      {
        "name": multiLanguageString(name: "detail", defaultValue: "Detail", context: context),
        "widget": (resultBatchRunStep.isNotEmpty)
            ? Column(
                children: [
                  testBatchRunInformation(),
                  const SizedBox(
                    height: 10,
                  ),
                  batchRunStepTable(navigationModel),
                  DynamicTablePagging(
                    rowCountBatchRunStep,
                    currentPageBatchRunStep,
                    rowPerPageBatchRunStep,
                    pageChangeHandler: (page) {
                      setState(() {
                        future = getBatchRunStep(widget.id, page);
                      });
                    },
                    rowPerPageChangeHandler: (rowPerPageBatchRunStep) {
                      setState(() {
                        this.rowPerPageBatchRunStep = rowPerPageBatchRunStep!;
                        future = getBatchRunStep(widget.id, 1);
                      });
                    },
                  ),
                ],
              )
            : const SizedBox.shrink(),
      }
    ];
    return Column(
      children: [
        const SizedBox(
          height: 20,
        ),
        Row(
          children: [
            for (var tab in listTabItem)
              Container(
                margin: const EdgeInsets.only(right: 30),
                decoration:
                    tab["name"] == currentTab ? const BoxDecoration(border: Border(bottom: BorderSide(color: Color(0xff3d5afe), width: 2))) : null,
                child: TextButton(
                  child: Text(tab["name"],
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: const Color(0xff3d5afe),
                        fontSize: context.fontSizeManager.fontSizeText16,
                        fontWeight: FontWeight.w700,
                      )),
                  onPressed: () async {
                    setState(() {
                      currentTab = tab["name"];
                    });
                  },
                ),
              ),
          ],
        ),
        Container(
          color: const Color.fromRGBO(216, 218, 229, 1),
          height: 1,
          margin: const EdgeInsets.only(bottom: 20),
        ),
        for (var tab in listTabItem)
          if (currentTab == tab["name"]) tab["widget"]
      ],
    );
  }

  Container pieChartWidget() {
    Map<String, String> mapKeyTitle = {
      "dataCount": "Data count",
      "criticalCount": "Critical",
      "majorCount": "Major",
      "averageCount": "Average",
      "minorCount": "Minor",
      "trivialCount": "Trivial",
    };
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.only(top: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          pie.PieChart(
            dataMap: {
              for (var entry in dataMapPieChar.entries) mapKeyTitle[entry.key] ?? "Not exists": entry.value * 1.0,
            },
            animationDuration: const Duration(milliseconds: 1000),
            chartRadius: 150,
            chartType: pie.ChartType.ring,
            ringStrokeWidth: 40,
            legendOptions: const pie.LegendOptions(
              showLegendsInRow: false,
              legendPosition: pie.LegendPosition.right,
              showLegends: true,
              legendTextStyle: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            colorList: const [
              Color(0xFFff7675),
              Color(0xFF74b9ff),
              Color(0xFF55efc4),
              Color(0xFFffeaa7),
              Color(0xFFa29bfe),
              Color(0xFFfd79a8),
            ],
            chartValuesOptions: const pie.ChartValuesOptions(
              showChartValueBackground: true,
              showChartValues: true,
              showChartValuesOutside: true,
              decimalPlaces: 1,
            ),
          ),
        ],
      ),
    );
  }

  Widget batchRunStepTable(NavigationModel navigationModel) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MultiLanguageText(
          name: "batch_run_step",
          defaultValue: "Batch Run Step",
          style: Style(context).styleTitleDialog,
        ),
        const SizedBox(
          height: 5,
        ),
        CustomDataTableWidget(
          columns: [
            Expanded(
              flex: 3,
              child: MultiLanguageText(
                name: "batch_step_name",
                defaultValue: "Batch Step Name",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              flex: 3,
              child: MultiLanguageText(
                name: "test_run_type",
                defaultValue: "Test Run Type",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              flex: 3,
              child: MultiLanguageText(
                name: "test_case",
                defaultValue: "Test Case",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              flex: 3,
              child: MultiLanguageText(
                name: "version",
                defaultValue: "Version",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              flex: 3,
              child: MultiLanguageText(
                name: "environment",
                defaultValue: "Environment",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              flex: 3,
              child: MultiLanguageText(
                name: "create_date",
                defaultValue: "Create date",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              flex: 3,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  MultiLanguageText(
                    name: "status",
                    defaultValue: "Status",
                    isLowerCase: false,
                    style: Style(context).styleTextDataColumn,
                  ),
                  Tooltip(
                    message: multiLanguageString(
                        name: "pass_fail_waiting_processing_error_canceled",
                        defaultValue: "Green = Pass\nRed = Fail\nOrange = Waiting\nBlue = Processing\nGrey = Error\nBlack = Canceled",
                        context: context),
                    child: const Icon(
                      Icons.info,
                      color: Colors.grey,
                      size: 16,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(width: 100, child: Center(child: Text("", style: Style(context).styleTextDataColumn))),
          ],
          rows: [
            for (var batchStep in resultBatchRunStep)
              CustomDataRow(
                cells: [
                  Expanded(
                    flex: 3,
                    child: Text(
                      batchStep["tfTestBatchStep"]["title"] ?? "",
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Text(
                      batchStep["tfTestBatchStep"]["testRunType"],
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Text(
                      batchStep["tfTestRun"] != null ? batchStep["tfTestRun"]["tfTestCase"]["title"] : "",
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Text(
                      batchStep["tfTestRun"] != null ? batchStep["tfTestRun"]["tfTestVersion"]["title"] : "",
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Text(
                      batchStep["tfTestRun"] != null ? batchStep["tfTestRun"]["tfTestEnvironment"]["title"] : "",
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: (batchStep["createDate"] != null)
                        ? Text(
                            "${DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(batchStep["createDate"]).toLocal())} ${DateFormat('HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(batchStep["createDate"]).toLocal())}",
                            softWrap: true,
                            style: TextStyle(
                              fontSize: context.fontSizeManager.fontSizeText16,
                              color: const Color.fromRGBO(49, 49, 49, 1),
                              fontWeight: FontWeight.w500,
                              letterSpacing: 1.5,
                            ),
                          )
                        : const SizedBox.shrink(),
                  ),
                  Expanded(
                    flex: 3,
                    child: Center(
                      child: batchStep["tfTestRun"] != null
                          ? BadgeText6StatusWidget(
                              status: batchStep["tfTestRun"]["status"],
                            )
                          : const SizedBox.shrink(),
                    ),
                  ),
                  SizedBox(
                    width: 100,
                    child: batchStep["tfTestRun"] != null
                        ? Row(
                            children: [
                              IconButton(
                                onPressed: () {
                                  downloadResult(batchStep["tfTestRun"]["id"]);
                                },
                                icon: const Icon(Icons.download, color: Color.fromRGBO(49, 49, 49, 1)),
                                splashRadius: 20,
                                tooltip: multiLanguageString(name: "download_result", defaultValue: "Download result", context: context),
                              ),
                              if (Provider.of<SecurityModel>(context, listen: false).hasUserPermission("ADMIN_CODE") ||
                                  Provider.of<SecurityModel>(context, listen: false).hasUserPermission("PROJECT_CODE"))
                                IconButton(
                                  onPressed: () {
                                    downloadCode(batchStep["tfTestRun"]["id"]);
                                  },
                                  icon: const FaIcon(FontAwesomeIcons.download),
                                  color: batchStep["tfTestRun"]["status"] == 1
                                      ? Colors.green
                                      : batchStep["tfTestRun"]["status"] == -1
                                          ? Colors.red
                                          : batchStep["tfTestRun"]["status"] == 0
                                              ? Colors.orange
                                              : batchStep["tfTestRun"]["status"] == -13
                                                  ? const Color.fromRGBO(49, 49, 49, 1)
                                                  : batchStep["tfTestRun"]["status"] < -1
                                                      ? Colors.grey
                                                      : Colors.blueAccent,
                                  splashRadius: 20,
                                  tooltip: multiLanguageString(name: "download_code", defaultValue: "Download code", context: context),
                                ),
                            ],
                          )
                        : const SizedBox.shrink(),
                  ),
                ],
                onSelectChanged: () {
                  navigationModel.navigate(
                      "/qa-platform/project/${navigationModel.prjProjectId}/runs/${batchStep["tfTestRun"]["tfTestCase"]["testCaseType"]}/test-run/${batchStep["tfTestRun"]["id"]}");
                },
              ),
          ],
        ),
        const SizedBox(
          height: 20,
        )
      ],
    );
  }

  Container testBatchRunInformation() {
    return Container(
      padding: const EdgeInsets.only(bottom: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "run_owner",
                  defaultValue: "Run owner",
                  style: Style(context).styleTitleDialog,
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                flex: 8,
                child: Text(
                  resultTestBatchRunDetail["sysUser"]["fullname"],
                  softWrap: true,
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(49, 49, 49, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "schedule_time",
                  defaultValue: "Schedule time",
                  style: Style(context).styleTitleDialog,
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                flex: 8,
                child: (resultTestBatchRunDetail["scheduledDate"] != null)
                    ? Text(
                        "${DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultTestBatchRunDetail["scheduledDate"]).toLocal())} ${DateFormat('HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultTestBatchRunDetail["scheduledDate"]).toLocal())}",
                        softWrap: true,
                        style: TextStyle(
                          fontSize: context.fontSizeManager.fontSizeText16,
                          color: const Color.fromRGBO(49, 49, 49, 1),
                          fontWeight: FontWeight.w500,
                          letterSpacing: 1.5,
                        ),
                      )
                    : const SizedBox.shrink(),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "start_run",
                  defaultValue: "Start run",
                  style: Style(context).styleTitleDialog,
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                flex: 8,
                child: (resultTestBatchRunDetail["startDate"] != null)
                    ? Text(
                        "${DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultTestBatchRunDetail["startDate"]).toLocal())} ${DateFormat('HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultTestBatchRunDetail["startDate"]).toLocal())}",
                        softWrap: true,
                        style: TextStyle(
                          fontSize: context.fontSizeManager.fontSizeText16,
                          color: const Color.fromRGBO(49, 49, 49, 1),
                          fontWeight: FontWeight.w500,
                          letterSpacing: 1.5,
                        ),
                      )
                    : const SizedBox.shrink(),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "end_run",
                  defaultValue: "End run",
                  style: Style(context).styleTitleDialog,
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                flex: 8,
                child: (resultTestBatchRunDetail["endDate"] != null)
                    ? Text(
                        "${DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultTestBatchRunDetail["endDate"]).toLocal())} ${DateFormat('HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultTestBatchRunDetail["endDate"]).toLocal())}",
                        softWrap: true,
                        style: TextStyle(
                          fontSize: context.fontSizeManager.fontSizeText16,
                          color: const Color.fromRGBO(49, 49, 49, 1),
                          fontWeight: FontWeight.w500,
                          letterSpacing: 1.5,
                        ),
                      )
                    : const SizedBox.shrink(),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "total_run_time",
                  defaultValue: "Total run time",
                  style: Style(context).styleTitleDialog,
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                flex: 8,
                child: Text(
                  (resultTestBatchRunDetail["totalTime"] != null && resultTestBatchRunDetail["recordTime"] != null)
                      ? "${(Duration(milliseconds: resultTestBatchRunDetail["totalTime"] - resultTestBatchRunDetail["recordTime"]).inMilliseconds / 1000).toStringAsFixed(2).replaceFirst('.', ',').padLeft(5, '0')} (S)"
                      : "0 (S)",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(49, 49, 49, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "status",
                  defaultValue: "Status",
                  style: Style(context).styleTitleDialog,
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                flex: 8,
                child: Row(
                  children: [
                    IntrinsicWidth(
                      child: BadgeTextTestBatchRunStatusWidget(
                        status: resultTestBatchRunDetail["status"],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "version",
                  defaultValue: "Version",
                  style: Style(context).styleTitleDialog,
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                flex: 8,
                child: Text(
                  resultTestBatchRunDetail["tfTestVersion"]["title"] ?? "",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(49, 49, 49, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "environment",
                  defaultValue: "Environment",
                  style: Style(context).styleTitleDialog,
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                flex: 8,
                child: Text(
                  resultTestBatchRunDetail["tfTestEnvironment"]["title"] ?? "",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(49, 49, 49, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          if (resultTestBatchRunDetail["description"] != null)
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 10,
                ),
                MultiLanguageText(
                  name: "description",
                  defaultValue: "Description",
                  style: Style(context).styleTitleDialog,
                ),
                Text(
                  resultTestBatchRunDetail["description"],
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(49, 49, 49, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ],
            ),
        ],
      ),
    );
  }

  Widget headerWidget(NavigationModel navigationModel) {
    return Row(
      children: [
        IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(Icons.arrow_back),
          padding: EdgeInsets.zero,
          splashRadius: 16,
        ),
        Expanded(
          child: Text(
            resultTestBatchRunDetail["tfTestBatch"]["title"],
            style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
          ),
        ),
        Row(
          children: [
            IconButton(
              onPressed: () {
                Provider.of<NavigationModel>(context, listen: false).navigate(
                    "/qa-platform/project/${Provider.of<NavigationModel>(context, listen: false).prjProjectId}/test-list/test_batch/${resultTestBatchRunDetail["tfTestBatchId"]}/re-run/${resultTestBatchRunDetail["id"]}");
              },
              icon: const Icon(Icons.refresh, color: Color.fromRGBO(49, 49, 49, 1)),
              splashRadius: 20,
              tooltip: multiLanguageString(name: "rerun", defaultValue: "Rerun", context: context),
            ),
            IconButton(
              onPressed: () {
                downloadResult(resultTestBatchRunDetail["id"]);
              },
              icon: const Icon(Icons.download, color: Color.fromRGBO(49, 49, 49, 1)),
              splashRadius: 20,
              tooltip: multiLanguageString(name: "download_result", defaultValue: "Download result", context: context),
            ),
            IconButton(
              onPressed: () {
                future = getInitPage();
              },
              icon: const Icon(Icons.autorenew, color: Color.fromRGBO(49, 49, 49, 1)),
              splashRadius: 20,
              tooltip: multiLanguageString(name: "refresh", defaultValue: "Refresh", context: context),
            ),
          ],
        )
      ],
    );
  }
}
