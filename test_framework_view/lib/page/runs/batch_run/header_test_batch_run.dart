import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';

import '../../../api.dart';
import '../../../common/custom_state.dart';
import '../../../common/dynamic_dropdown_button.dart';

class HeaderTestBatchRunWidget extends StatefulWidget {
  final int countList;
  final Function? callbackFilter;

  const HeaderTestBatchRunWidget({
    Key? key,
    required this.countList,
    this.callbackFilter,
  }) : super(key: key);

  @override
  State<HeaderTestBatchRunWidget> createState() => _HeaderTestBatchRunWidgetState();
}

class _HeaderTestBatchRunWidgetState extends CustomState<HeaderTestBatchRunWidget> {
  final TextEditingController _searchLibrary = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  TextEditingController startDate = TextEditingController();
  TextEditingController endDate = TextEditingController();
  SingleValueDropDownController statusController = SingleValueDropDownController();
  SingleValueDropDownController testRunOwnerController = SingleValueDropDownController();
  SingleValueDropDownController versionController = SingleValueDropDownController();
  SingleValueDropDownController environmentController = SingleValueDropDownController();
  var prjProjectId = "";
  var _listTestOwner = [];
  var _listVersions = [];
  var _listEnvironment = [];
  bool isShowFilterForm = true;
  bool isSearched = false;
  getListOwner() async {
    var requestBody = {"prjProjectId": prjProjectId};
    var response = await httpPost("/test-framework-api/user/sys/sys-user-project/search", requestBody, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listTestOwner = response["body"]["resultList"];
      });
    }
  }

  @override
  void initState() {
    prjProjectId = Provider.of<NavigationModel>(context, listen: false).prjProjectId ?? "";
    getInfoFilter();
    super.initState();
  }

  getInfoFilter() {
    getListOwner();
    getVersionList();
    getEnvironmentList();
  }

  handleCallBackSearchFunction() {
    var result = {
      "status": statusController.dropDownValue?.value,
      "sysUserId": testRunOwnerController.dropDownValue?.value,
      "tfTestVersionId": versionController.dropDownValue?.value,
      "tfTestEnvironmentId": environmentController.dropDownValue?.value,
      "createDateGt": (startDate.text != "") ? "${DateFormat('dd-MM-yyyy HH:mm').parse(startDate.text).toIso8601String()}+07:00" : "",
      "createDateLt": (endDate.text != "") ? "${DateFormat('dd-MM-yyyy HH:mm').parse(endDate.text).toIso8601String()}+07:00" : "",
      "tfTestBatchTitleLike": _searchLibrary.text,
      "tfTestBatchNameLike": _nameController.text,
    };
    widget.callbackFilter!(result);
  }

  getVersionList() async {
    var findRequest = {"prjProjectId": prjProjectId, "status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-version/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listVersions = response["body"]["resultList"];
      });
    }
    return _listVersions;
  }

  getEnvironmentList() async {
    var findRequest = {"prjProjectId": prjProjectId, "status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-environment/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listEnvironment = response["body"]["resultList"];
      });
    }
    return _listEnvironment;
  }

  @override
  void dispose() {
    startDate.dispose();
    _searchLibrary.dispose();
    _nameController.dispose();
    endDate.dispose();
    statusController.dispose();
    testRunOwnerController.dispose();
    versionController.dispose();
    environmentController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      List<Widget> widgetList = [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
            child: SizedBox(
              height: 50,
              child: DynamicTextField(
                controller: _nameController,
                onComplete: () => handleCallBackSearchFunction(),
                onChanged: (value) {
                  setState(() {
                    _nameController.text;
                  });
                },
                labelText: multiLanguageString(name: "search_batch_code", defaultValue: "Search batch code", context: context),
                hintText: multiLanguageString(name: "by_code", defaultValue: "By code", context: context),
                suffixIcon: (_nameController.text != "")
                    ? IconButton(
                        onPressed: () {
                          setState(() {
                            _nameController.clear();
                            if (isSearched) {
                              handleCallBackSearchFunction();
                            }
                          });
                        },
                        icon: const Icon(Icons.clear),
                        splashRadius: 20,
                      )
                    : null,
              ),
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
            child: SizedBox(
              height: 50,
              child: DynamicTextField(
                controller: _searchLibrary,
                onComplete: () => handleCallBackSearchFunction(),
                onChanged: (value) {
                  setState(() {
                    _searchLibrary.text;
                  });
                },
                labelText: multiLanguageString(name: "search_batch_name", defaultValue: "Search batch name", context: context),
                hintText: multiLanguageString(name: "by_name", defaultValue: "By name", context: context),
                suffixIcon: (_searchLibrary.text != "")
                    ? IconButton(
                        onPressed: () {
                          setState(() {
                            _searchLibrary.clear();
                            if (isSearched) {
                              handleCallBackSearchFunction();
                            }
                          });
                        },
                        icon: const Icon(Icons.clear),
                        splashRadius: 20,
                      )
                    : null,
              ),
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
            child: DynamicDropdownSearchClearOption(
              labelText: multiLanguageString(name: "project_version", defaultValue: "Project version", context: context),
              controller: versionController,
              hint: multiLanguageString(name: "search_version", defaultValue: "Search version", context: context),
              items: _listVersions.map<DropDownValueModel>((dynamic result) {
                return DropDownValueModel(
                  value: result["id"],
                  name: result["title"],
                );
              }).toList(),
              maxHeight: 200,
              onChanged: (dynamic newValue) {
                if (isSearched && newValue == "") {
                  handleCallBackSearchFunction();
                }
              },
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
            child: DynamicDropdownSearchClearOption(
              labelText: multiLanguageString(name: "project_environment", defaultValue: "Project environment", context: context),
              controller: environmentController,
              hint: multiLanguageString(name: "search_environment", defaultValue: "Search environment", context: context),
              items: _listEnvironment.map<DropDownValueModel>((dynamic result) {
                return DropDownValueModel(
                  value: result["id"],
                  name: result["title"],
                );
              }).toList(),
              maxHeight: 200,
              onChanged: (dynamic newValue) {
                if (isSearched && newValue == "") {
                  handleCallBackSearchFunction();
                }
              },
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
            child: DynamicDropdownSearchClearOption(
              labelText: multiLanguageString(name: "search_status", defaultValue: "Search status", context: context),
              controller: statusController,
              hint: multiLanguageString(name: "search_status", defaultValue: "Search status", context: context),
              items: {"New": 0, "Running": 1, "Run pass": 2, "Run fail": -2}.entries.map<DropDownValueModel>((result) {
                return DropDownValueModel(
                  value: result.value,
                  name: result.key,
                );
              }).toList(),
              maxHeight: 200,
              onChanged: (dynamic newValue) {
                if (isSearched && newValue == "") {
                  handleCallBackSearchFunction();
                }
                // if (newValue == "") {
                //   handleCallBackSearchFunction();
                // }
              },
            ),
          ),
        ),
        Expanded(
          child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
              child: DynamicDropdownSearchClearOption(
                onChanged: (dynamic newValue) {
                  if (isSearched && newValue == "") {
                    handleCallBackSearchFunction();
                  }
                  // if (newValue == "") {
                  //   handleCallBackSearchFunction();
                  // }
                },
                labelText: multiLanguageString(name: "test_run_owner", defaultValue: "Test run owner", context: context),
                controller: testRunOwnerController,
                hint: multiLanguageString(name: "search_test_run_owner", defaultValue: "Search Test run owner", context: context),
                items: _listTestOwner.map<DropDownValueModel>((dynamic result) {
                  return DropDownValueModel(
                    value: result["sysUser"]["id"],
                    name: result["sysUser"]["fullname"],
                  );
                }).toList(),
                maxHeight: 200,
              )),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
            child: DynamicTextField(
              controller: startDate,
              onChanged: (value) {
                setState(() {
                  startDate.text;
                });
              },
              labelText: multiLanguageString(name: "create_date_from", defaultValue: "Create date from", context: context),
              hintText: multiLanguageString(name: "create_date_from", defaultValue: "Create date from", context: context),
              suffixIcon: (startDate.text != "")
                  ? IconButton(
                      onPressed: () {
                        setState(() {
                          startDate.clear();
                          if (isSearched) {
                            handleCallBackSearchFunction();
                          }
                        });
                      },
                      icon: const Icon(Icons.close),
                      splashRadius: 20,
                    )
                  : const Icon(Icons.calendar_today),
              readOnly: true,
              onTap: () {
                _selectDate(startDate);
              },
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
            child: DynamicTextField(
              controller: endDate,
              onChanged: (value) {
                setState(() {
                  endDate.text;
                });
              },
              labelText: multiLanguageString(name: "create_date_to", defaultValue: "Create date to", context: context),
              hintText: multiLanguageString(name: "create_date_to", defaultValue: "Create date to", context: context),
              suffixIcon: (endDate.text != "")
                  ? IconButton(
                      onPressed: () {
                        setState(() {
                          endDate.clear();
                          if (isSearched) {
                            handleCallBackSearchFunction();
                          }
                        });
                      },
                      icon: const Icon(Icons.close),
                      splashRadius: 20,
                    )
                  : const Icon(Icons.calendar_today),
              readOnly: true,
              onTap: () {
                _selectDate(endDate);
              },
            ),
          ),
        ),
      ];
      var itemPerRow = 4;
      if (constraints.maxWidth < 600) {
        itemPerRow = 1;
      } else if (constraints.maxWidth < 800) {
        itemPerRow = 2;
      } else if (constraints.maxWidth < 1200) {
        itemPerRow = 3;
      }
      var rowCount = (1.0 * widgetList.length / itemPerRow).ceil();
      var filterWidget = Column(
        children: [
          for (var rowI = 0; rowI < rowCount; rowI++)
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                for (var colI = 0; colI < itemPerRow; colI++)
                  rowI * itemPerRow + colI < widgetList.length ? widgetList[rowI * itemPerRow + colI] : Expanded(child: Container())
              ],
            )
        ],
      );
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MultiLanguageText(
            name: "test_batch_runs_count",
            defaultValue: "Test Batch Runs (\$0)",
            variables: ["${widget.countList}"],
            style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
          ),
          Row(
            children: [
              MultiLanguageText(
                name: "filter_form",
                defaultValue: "Filter form",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: const Color.fromRGBO(0, 0, 0, 0.8),
                    fontSize: context.fontSizeManager.fontSizeText16,
                    letterSpacing: 2),
              ),
              IconButton(
                onPressed: () {
                  setState(() {
                    isShowFilterForm = !isShowFilterForm;
                  });
                },
                icon: isShowFilterForm ? const Icon(Icons.visibility) : const Icon(Icons.visibility_off),
                splashRadius: 1,
                tooltip: isShowFilterForm
                    ? multiLanguageString(name: "hide_filter_form", defaultValue: "Hide filter form", context: context)
                    : multiLanguageString(name: "show_filter_form", defaultValue: "Show filter form", context: context),
              )
            ],
          ),
          if (isShowFilterForm) filterWidget,
          if (isShowFilterForm)
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 20),
                  child: ElevatedButton(
                      onPressed: () {
                        isSearched = true;
                        handleCallBackSearchFunction();
                      },
                      child: const MultiLanguageText(name: "search", defaultValue: "Search", isLowerCase: false)),
                ),
                if (widgetList.length > 2)
                  ElevatedButton(
                      onPressed: () {
                        setState(() {
                          statusController.clearDropDown();
                          _searchLibrary.clear();
                          _nameController.clear();
                          testRunOwnerController.clearDropDown();
                          versionController.clearDropDown();
                          environmentController.clearDropDown();
                          startDate.clear();
                          endDate.clear();
                          widget.callbackFilter!({});
                        });
                      },
                      child: const MultiLanguageText(name: "reset", defaultValue: "Reset", isLowerCase: false))
              ],
            )
        ],
      );
    });
  }

  _selectDate(TextEditingController controller) async {
    final DateTime? datePicked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (datePicked != null && mounted) {
      final TimeOfDay? timePicked = await showTimePicker(
        context: context,
        initialTime: TimeOfDay.now(),
      );
      if (timePicked != null) {
        setState(() {
          controller.text =
              "${DateFormat('dd-MM-yyyy').format(datePicked)} ${timePicked.hour.toString().padLeft(2, '0')}:${timePicked.minute.toString().padLeft(2, '0')}";
        });
      }
    }
  }
}
