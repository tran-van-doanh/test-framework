import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/common/file_download.dart';
import 'package:test_framework_view/components/badge/badge_text.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';

import '../../../common/custom_state.dart';
import '../../../components/menu_tab.dart';
import 'header_test_batch_run.dart';

class BatchRun extends StatefulWidget {
  final String? tfTestBatchId;
  const BatchRun({
    Key? key,
    this.tfTestBatchId,
  }) : super(key: key);
  @override
  State<BatchRun> createState() => _BatchRunState();
}

class _BatchRunState extends CustomState<BatchRun> {
  late Future futureResultBatchRun;
  var rowCount = 0;
  var currentPage = 1;
  var resultListBatchRun = [];
  var rowPerPage = 5;
  var searchRequest = {};
  var prjProjectId = "";

  @override
  void initState() {
    super.initState();
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    prjProjectId = navigationModel.prjProjectId ?? "";
    futureResultBatchRun = getListTfTestBatchRun(currentPage);
  }

  getListTfTestBatchRun(page) async {
    if ((page - 1) * rowPerPage > rowCount) {
      page = (1.0 * rowCount / rowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    Map<dynamic, dynamic> findRequest = {
      "queryOffset": (page - 1) * rowPerPage,
      "queryLimit": rowPerPage,
      "prjProjectId": prjProjectId,
    };
    findRequest.addAll(searchRequest);
    if (searchRequest["tfTestBatchTitleLike"] != null && searchRequest["tfTestBatchTitleLike"].isNotEmpty) {
      findRequest["tfTestBatchTitleLike"] = "%${searchRequest["tfTestBatchTitleLike"]}%";
    }
    if (searchRequest["tfTestBatchNameLike"] != null && searchRequest["tfTestBatchNameLike"].isNotEmpty) {
      findRequest["tfTestBatchNameLike"] = "%${searchRequest["tfTestBatchNameLike"]}%";
    }
    if (widget.tfTestBatchId != null) {
      findRequest["tfTestBatchId"] = widget.tfTestBatchId!;
    }
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-batch-run/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPage = page;
        rowCount = response["body"]["rowCount"];
        resultListBatchRun = response["body"]["resultList"];
      });
      return 0;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, LanguageModel>(builder: (context, navigationModel, languageModel, child) {
      return FutureBuilder(
          future: futureResultBatchRun,
          builder: (context, dataBatchRun) {
            if (dataBatchRun.hasData) {
              return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
                return Material(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: ListView(
                          children: [
                            NavMenuTabV1(
                              currentUrl: "/runs/test_batch",
                              margin: const EdgeInsets.fromLTRB(50, 20, 50, 0),
                              navigationList: context.testRunsMenuList,
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(50, 20, 50, 30),
                              child: Column(
                                children: [
                                  HeaderTestBatchRunWidget(
                                    countList: rowCount,
                                    callbackFilter: (result) {
                                      searchRequest = result;
                                      futureResultBatchRun = getListTfTestBatchRun(currentPage);
                                    },
                                  ),
                                  if (resultListBatchRun.isNotEmpty) tableWidget(constraints),
                                  SelectionArea(
                                    child: DynamicTablePagging(
                                      rowCount,
                                      currentPage,
                                      rowPerPage,
                                      pageChangeHandler: (page) {
                                        futureResultBatchRun = getListTfTestBatchRun(page);
                                      },
                                      rowPerPageChangeHandler: (value) {
                                        setState(() {
                                          rowPerPage = value;
                                          futureResultBatchRun = getListTfTestBatchRun(1);
                                        });
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              });
            } else if (dataBatchRun.hasError) {
              return Text('${dataBatchRun.error}');
            }
            return const Center(child: CircularProgressIndicator());
          });
    });
  }

  SelectionArea tableWidget(BoxConstraints constraints) {
    return SelectionArea(
      child: Container(
        margin: const EdgeInsets.only(top: 40),
        child: (constraints.maxWidth < 1200)
            ? SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: SizedBox(
                  width: 1200,
                  child: tableBig(),
                ),
              )
            : tableBig(),
      ),
    );
  }

  Column tableBig() {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.all(15),
          decoration: const BoxDecoration(
            border: Border(
              top: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
              bottom: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
              left: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
              right: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
            ),
          ),
          child: Row(
            children: [
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: MultiLanguageText(
                    name: "test_batch_code", defaultValue: "Test Batch Code", isLowerCase: false, style: Style(context).styleTextDataColumn),
              )),
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: MultiLanguageText(
                    name: "test_batch_name", defaultValue: "Test Batch Name", isLowerCase: false, style: Style(context).styleTextDataColumn),
              )),
              SizedBox(
                  width: 150,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Center(
                        child: MultiLanguageText(
                            name: "schedule_date", defaultValue: "Schedule Date", isLowerCase: false, style: Style(context).styleTextDataColumn)),
                  )),
              SizedBox(
                  width: 150,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Center(
                        child: MultiLanguageText(
                            name: "create_date", defaultValue: "Create Date", isLowerCase: false, style: Style(context).styleTextDataColumn)),
                  )),
              SizedBox(
                  width: 150,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Center(
                        child:
                            MultiLanguageText(name: "owner", defaultValue: "Owner", isLowerCase: false, style: Style(context).styleTextDataColumn)),
                  )),
              SizedBox(
                width: 150,
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        MultiLanguageText(
                          name: "status",
                          defaultValue: "Status",
                          isLowerCase: false,
                          style: Style(context).styleTextDataColumn,
                        ),
                        Tooltip(
                          message: multiLanguageString(
                              name: "run_pass_run_fail_running_new_warning",
                              defaultValue: "Green = Run pass\nRed = Run fail\nOrange = Running\nBlue = New\nYellow = Warning",
                              context: context),
                          child: const Icon(
                            Icons.info,
                            color: Colors.grey,
                            size: 16,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(width: 100, child: Center(child: Text("", style: Style(context).styleTextDataColumn))),
            ],
          ),
        ),
        for (var testBatchRun in resultListBatchRun)
          RowTableBatchRunWidget(
            testBatchRun,
            prjProjectId,
          ),
      ],
    );
  }
}

class RowTableBatchRunWidget extends StatefulWidget {
  final dynamic rowData;
  final String prjProjectId;
  final String? tfTestId;
  const RowTableBatchRunWidget(this.rowData, this.prjProjectId, {Key? key, this.tfTestId}) : super(key: key);

  @override
  State<RowTableBatchRunWidget> createState() => _RowTableBatchRunWidgetState();
}

class _RowTableBatchRunWidgetState extends CustomState<RowTableBatchRunWidget> {
  downloadResult(id) async {
    File? downloadedFile = await httpDownloadFile(context, "/test-framework-api/user/test-framework-file/tf-test-batch-run/$id/export-result",
        Provider.of<SecurityModel>(context, listen: false).authorization, 'Output.xlsx');
    if (downloadedFile != null && mounted) {
      var snackBar = SnackBar(
        content: MultiLanguageText(name: "file_downloaded_to", defaultValue: "File downloaded to \$0 .", variables: ["$downloadedFile"]),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<NavigationModel>(
      builder: (context, navigationModel, child) {
        return Column(
          children: [
            GestureDetector(
              onTap: () async {
                navigationModel.navigate("/qa-platform/project/${navigationModel.prjProjectId}/runs/test_batch/test-run/${widget.rowData["id"]}");
              },
              child: Container(
                padding: const EdgeInsets.all(15),
                decoration: const BoxDecoration(
                  border: Border(
                    top: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
                    bottom: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
                    left: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
                    right: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
                  ),
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: Wrap(
                          children: [
                            Text(
                              widget.rowData["tfTestBatch"]["name"] ?? "",
                              softWrap: true,
                              style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: Wrap(
                          children: [
                            Text(
                              widget.rowData["tfTestBatch"]["title"] ?? "",
                              softWrap: true,
                              style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 150,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              widget.rowData["scheduledDate"] != null
                                  ? DateFormat('dd-MM-yyyy')
                                      .format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(widget.rowData["scheduledDate"]).toLocal())
                                  : "",
                              softWrap: true,
                              style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                            ),
                            Text(
                              widget.rowData["scheduledDate"] != null
                                  ? DateFormat('HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(widget.rowData["scheduledDate"]).toLocal())
                                  : "",
                              softWrap: true,
                              style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 150,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              widget.rowData["createDate"] != null
                                  ? DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(widget.rowData["createDate"]).toLocal())
                                  : "",
                              softWrap: true,
                              style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                            ),
                            Text(
                              widget.rowData["createDate"] != null
                                  ? DateFormat('HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(widget.rowData["createDate"]).toLocal())
                                  : "",
                              softWrap: true,
                              style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 150,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: Center(
                          child: Text(
                            widget.rowData["sysUser"]["fullname"],
                            softWrap: true,
                            style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 150,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          IntrinsicWidth(
                            child: BadgeTextTestBatchRunStatusWidget(
                              status: widget.rowData["status"],
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 100,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          IconButton(
                            onPressed: () {
                              Provider.of<NavigationModel>(context, listen: false).navigate(
                                  "/qa-platform/project/${Provider.of<NavigationModel>(context, listen: false).prjProjectId}/test-list/test_batch/${widget.rowData["tfTestBatchId"]}/re-run/${widget.rowData["id"]}");
                            },
                            icon: const Icon(Icons.refresh, color: Color.fromRGBO(49, 49, 49, 1)),
                            splashRadius: 20,
                            tooltip: multiLanguageString(name: "rerun", defaultValue: "Rerun", context: context),
                          ),
                          IconButton(
                            onPressed: () {
                              downloadResult(widget.rowData["id"]);
                            },
                            icon: const Icon(Icons.download, color: Color.fromRGBO(49, 49, 49, 1)),
                            splashRadius: 20,
                            tooltip: multiLanguageString(name: "download_result", defaultValue: "Download result", context: context),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}
