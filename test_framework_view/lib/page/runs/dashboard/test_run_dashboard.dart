import 'dart:convert';

import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dashboard/dynamic_dashboard.dart';
import 'package:test_framework_view/components/dynamic_filter_widget_list.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';

class TestRunDashBoardPageWidget extends StatefulWidget {
  const TestRunDashBoardPageWidget({Key? key}) : super(key: key);

  @override
  State<TestRunDashBoardPageWidget> createState() => _TestRunDashBoardPageWidgetState();
}

class _TestRunDashBoardPageWidgetState extends State<TestRunDashBoardPageWidget> {
  TextEditingController startDate = TextEditingController();
  TextEditingController endDate = TextEditingController();
  SingleValueDropDownController testCaseTypeController = SingleValueDropDownController();
  SingleValueDropDownController versionController = SingleValueDropDownController();
  SingleValueDropDownController environmentController = SingleValueDropDownController();
  SingleValueDropDownController componentController = SingleValueDropDownController();
  SingleValueDropDownController testOwnerController = SingleValueDropDownController();
  var prjProjectId = "";
  var _listVersions = [];
  var _listEnvironment = [];
  var _listComponents = [];
  var _listTestOwner = [];
  bool isShowFilterForm = true;
  bool isSearched = false;
  List dashboardList = [];
  String? dashboardSelectedId;
  Map<dynamic, dynamic> requestBody = {};
  @override
  void initState() {
    prjProjectId = Provider.of<NavigationModel>(context, listen: false).prjProjectId ?? "";
    super.initState();
    getInfoPage();
  }

  getInfoPage() async {
    getVersionList();
    getEnvironmentList();
    getComponentList();
    getListOwner();
    await getDashboardList();
    if (dashboardList.isNotEmpty) {
      setState(() {
        dashboardSelectedId = dashboardList[0]["id"];
      });
    }
  }

  getDashboardList() async {
    var findRequest = {"dashboardType": "qa_test_run", "status": 1};
    for (var key in requestBody.keys) {
      if (requestBody[key] != null && requestBody[key].isNotEmpty) {
        findRequest[key] = requestBody[key];
      }
    }
    var response = await httpPost("/test-framework-api/user/report/rpt-dashboard/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false && response["body"]["resultList"].isNotEmpty) {
      setState(() {
        dashboardList = response["body"]["resultList"];
      });
    }
  }

  getVersionList() async {
    var findRequest = {"prjProjectId": prjProjectId, "status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-version/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listVersions = response["body"]["resultList"];
      });
    }
    return _listVersions;
  }

  getEnvironmentList() async {
    var findRequest = {"prjProjectId": prjProjectId, "status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-environment/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listEnvironment = response["body"]["resultList"];
      });
    }
    return _listEnvironment;
  }

  getComponentList() async {
    var findRequest = {"prjProjectId": prjProjectId, "status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-component/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listComponents = response["body"]["resultList"];
      });
    }
    return _listComponents;
  }

  getListOwner() async {
    var requestBody = {"prjProjectId": prjProjectId};
    var response = await httpPost("/test-framework-api/user/sys/sys-user-project/search", requestBody, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      _listTestOwner = response["body"]["resultList"];
    }
  }

  @override
  void dispose() {
    versionController.dispose();
    environmentController.dispose();
    componentController.dispose();
    testOwnerController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return ListView(
        padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
        children: [
          NavMenuTabV1(
            currentUrl: "/runs/dashboard",
            navigationList: context.testRunsMenuList,
            margin: const EdgeInsets.only(bottom: 20),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 30),
            child: headerWidget(),
          ),
          Row(
            children: [
              if (dashboardList.length > 1)
                Expanded(
                  child: DynamicDropdownButton(
                    value: dashboardSelectedId,
                    hint: multiLanguageString(name: "choose_a_dashboard", defaultValue: "Choose a dashboard", context: context),
                    items: dashboardList.map<DropdownMenuItem<String>>((dynamic result) {
                      return DropdownMenuItem(
                        value: result["id"],
                        child: Text(result["title"]),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        dashboardSelectedId = newValue!;
                      });
                    },
                    borderRadius: 5,
                  ),
                ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          if (dashboardSelectedId != null)
            DynamicDashboardWidget(
                key: Key("${dashboardSelectedId!}:${jsonEncode(requestBody)}"),
                dashboardType: "qa_test_run",
                dashboardSelectedId: dashboardSelectedId!,
                requestBody: requestBody),
        ],
      );
    });
  }

  Column headerWidget() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MultiLanguageText(
            name: "dashboard",
            defaultValue: "Dashboard",
            style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500)),
        Row(
          children: [
            MultiLanguageText(
              name: "filter_form",
              defaultValue: "Filter form",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: const Color.fromRGBO(0, 0, 0, 0.8),
                  fontSize: context.fontSizeManager.fontSizeText16,
                  letterSpacing: 2),
            ),
            IconButton(
              onPressed: () {
                setState(() {
                  isShowFilterForm = !isShowFilterForm;
                });
              },
              icon: isShowFilterForm ? const Icon(Icons.visibility) : const Icon(Icons.visibility_off),
              splashRadius: 1,
              tooltip: isShowFilterForm
                  ? multiLanguageString(name: "hide_filter_form", defaultValue: "Hide filter form", context: context)
                  : multiLanguageString(name: "show_filter_form", defaultValue: "Show filter form", context: context),
            )
          ],
        ),
        if (isShowFilterForm)
          DynamicFilterListWidget(
            widgetList: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: SizedBox(
                    height: 50,
                    child: DynamicDropdownSearchClearOption(
                      labelText: multiLanguageString(name: "test_type", defaultValue: "Test type", context: context),
                      controller: testCaseTypeController,
                      hint: multiLanguageString(name: "search_test_type", defaultValue: "Search test type", context: context),
                      items: [
                        {"name": multiLanguageString(name: "map_test_case", defaultValue: "Test case", context: context), "value": "test_case"},
                        {
                          "name": multiLanguageString(name: "map_test_scenario", defaultValue: "Test scenario", context: context),
                          "value": "test_scenario"
                        },
                        {"name": multiLanguageString(name: "map_test_manual", defaultValue: "Test manual", context: context), "value": "manual_test"},
                        {"name": multiLanguageString(name: "map_test_suite", defaultValue: "Test suite", context: context), "value": "test_suite"},
                        {"name": multiLanguageString(name: "map_api", defaultValue: "Api", context: context), "value": "test_api"},
                        {"name": multiLanguageString(name: "map_shared_steps", defaultValue: "Shared steps", context: context), "value": "action"},
                      ].map<DropDownValueModel>((dynamic result) {
                        return DropDownValueModel(
                          value: result["value"],
                          name: result["name"],
                        );
                      }).toList(),
                      maxHeight: 200,
                      onChanged: (dynamic newValue) {
                        if (isSearched && newValue == "") {
                          setRequestBody();
                        }
                      },
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: DynamicDropdownSearchClearOption(
                    labelText: multiLanguageString(name: "project_version", defaultValue: "Project version", context: context),
                    controller: versionController,
                    hint: multiLanguageString(name: "search_version", defaultValue: "Search version", context: context),
                    items: _listVersions.map<DropDownValueModel>((dynamic result) {
                      return DropDownValueModel(
                        value: result["id"],
                        name: result["title"],
                      );
                    }).toList(),
                    maxHeight: 200,
                    onChanged: (dynamic newValue) {
                      if (isSearched && newValue == "") {
                        setRequestBody();
                      }
                    },
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: DynamicDropdownSearchClearOption(
                    labelText: multiLanguageString(name: "project_environment", defaultValue: "Project environment", context: context),
                    controller: environmentController,
                    hint: multiLanguageString(name: "search_environment", defaultValue: "Search environment", context: context),
                    items: _listEnvironment.map<DropDownValueModel>((dynamic result) {
                      return DropDownValueModel(
                        value: result["id"],
                        name: result["title"],
                      );
                    }).toList(),
                    maxHeight: 200,
                    onChanged: (dynamic newValue) {
                      if (isSearched && newValue == "") {
                        setRequestBody();
                      }
                    },
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                    padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                    child: DynamicDropdownSearchClearOption(
                      labelText: multiLanguageString(name: "project_module", defaultValue: "Project module", context: context),
                      controller: componentController,
                      hint: multiLanguageString(name: "up_search_module", defaultValue: "Search Module", context: context),
                      items: _listComponents.map<DropDownValueModel>((dynamic result) {
                        return DropDownValueModel(
                          value: result["id"],
                          name: result["title"],
                        );
                      }).toList(),
                      maxHeight: 200,
                      onChanged: (dynamic newValue) {
                        if (isSearched && newValue == "") {
                          setRequestBody();
                        }
                      },
                    )),
              ),
              Expanded(
                child: Padding(
                    padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                    child: DynamicDropdownSearchClearOption(
                      labelText: multiLanguageString(name: "test_owner", defaultValue: "Test owner", context: context),
                      controller: testOwnerController,
                      hint: multiLanguageString(name: "search_test_owner", defaultValue: "Search Test owner", context: context),
                      items: _listTestOwner.map<DropDownValueModel>((dynamic result) {
                        return DropDownValueModel(
                          value: result["sysUser"]["id"],
                          name: result["sysUser"]["fullname"],
                        );
                      }).toList(),
                      maxHeight: 200,
                      onChanged: (dynamic newValue) {
                        if (isSearched && newValue == "") {
                          setRequestBody();
                        }
                      },
                    )),
              ),
              Expanded(
                child: Padding(
                    padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                    child: DynamicTextField(
                      controller: startDate,
                      onChanged: (value) {
                        setState(() {
                          startDate.text;
                        });
                      },
                      labelText: multiLanguageString(name: "create_date_from", defaultValue: "Create date from", context: context),
                      hintText: multiLanguageString(name: "create_date_from", defaultValue: "Create date from", context: context),
                      suffixIcon: (startDate.text != "")
                          ? IconButton(
                              onPressed: () {
                                setState(() {
                                  startDate.clear();
                                  if (isSearched) {
                                    setRequestBody();
                                  }
                                });
                              },
                              icon: const Icon(Icons.close),
                              splashRadius: 20,
                            )
                          : const Icon(Icons.calendar_today),
                      readOnly: true,
                      onTap: () {
                        _selectDate(startDate);
                      },
                    )),
              ),
              Expanded(
                child: Padding(
                    padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                    child: DynamicTextField(
                      controller: endDate,
                      onChanged: (value) {
                        setState(() {
                          endDate.text;
                        });
                      },
                      labelText: multiLanguageString(name: "create_date_to", defaultValue: "Create date to", context: context),
                      hintText: multiLanguageString(name: "create_date_to", defaultValue: "Create date to", context: context),
                      suffixIcon: (endDate.text != "")
                          ? IconButton(
                              onPressed: () {
                                setState(() {
                                  endDate.clear();
                                  if (isSearched) {
                                    setRequestBody();
                                  }
                                });
                              },
                              icon: const Icon(Icons.close),
                              splashRadius: 20,
                            )
                          : const Icon(Icons.calendar_today),
                      readOnly: true,
                      onTap: () {
                        _selectDate(endDate);
                      },
                    )),
              ),
            ],
          ),
        if (isShowFilterForm)
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              ElevatedButton(
                  onPressed: () {
                    isSearched = true;
                    setRequestBody();
                  },
                  child: const MultiLanguageText(name: "search", defaultValue: "Search", isLowerCase: false)),
              Padding(
                padding: const EdgeInsets.only(left: 20),
                child: ElevatedButton(
                    onPressed: () {
                      setState(() {
                        componentController.clearDropDown();
                        testOwnerController.clearDropDown();
                        testCaseTypeController.clearDropDown();
                        versionController.clearDropDown();
                        environmentController.clearDropDown();
                        startDate.clear();
                        endDate.clear();
                        setRequestBody();
                      });
                    },
                    child: const MultiLanguageText(name: "reset", defaultValue: "Reset", isLowerCase: false)),
              ),
            ],
          )
      ],
    );
  }

  _selectDate(TextEditingController controller) async {
    final DateTime? datePicked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (datePicked != null && mounted) {
      final TimeOfDay? timePicked = await showTimePicker(
        context: context,
        initialTime: TimeOfDay.now(),
      );
      if (timePicked != null) {
        setState(() {
          controller.text =
              "${DateFormat('dd-MM-yyyy').format(datePicked)} ${timePicked.hour.toString().padLeft(2, '0')}:${timePicked.minute.toString().padLeft(2, '0')}";
        });
      }
    }
  }

  void setRequestBody() {
    setState(() {
      requestBody = {
        if (componentController.dropDownValue != null) "tfTestComponentId": componentController.dropDownValue?.value,
        if (testOwnerController.dropDownValue != null) "sysUserId": testOwnerController.dropDownValue?.value,
        if (versionController.dropDownValue != null) "tfTestVersionId": versionController.dropDownValue?.value,
        if (environmentController.dropDownValue != null) "tfTestEnvironmentId": environmentController.dropDownValue?.value,
        if (testCaseTypeController.dropDownValue != null) "testCaseType": testCaseTypeController.dropDownValue?.value,
        if (startDate.text != "") "createDateGt": "${DateFormat('dd-MM-yyyy HH:mm').parse(startDate.text).toIso8601String()}+07:00",
        if (endDate.text != "") "createDateLt": "${DateFormat('dd-MM-yyyy HH:mm').parse(endDate.text).toIso8601String()}+07:00",
      };
    });
  }
}
