import 'dart:async';
import 'dart:io';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/components/badge/badge_text.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:video_player/video_player.dart';

import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/common/file_download.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/config.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';

import '../../../common/custom_state.dart';
import '../../../components/dynamic_button.dart';
import '../../../components/menu_tab.dart';
import '../../list_test/tests/header/header_list_test_widget.dart';

class TestRun extends StatefulWidget {
  final String? testCaseId;
  final String type;
  const TestRun({Key? key, this.testCaseId, required this.type}) : super(key: key);
  @override
  State<TestRun> createState() => _TestRunState();
}

class _TestRunState extends CustomState<TestRun> {
  // late TooltipBehavior _tooltipBehavior;
  // late List<ChartData> chartData = [];
  late Future futureResultTestRun;
  var rowCount = 0;
  var currentPage = 1;
  var resultListTestSuiteRun = [];
  var rowPerPage = 5;
  var searchRequest = {};
  var prjProjectId = "";
  // int passedCount = 0;
  // int failedCount = 0;
  // String runTimeAvg = "";

  // getTestCaseRunReport() async {
  //   var findRequest = {
  //     "prjProjectId": prjProjectId,
  //     "tfTestCaseIdNotNull": true,
  //     "tfTestCaseType": widget.type,
  //   };
  //   if (searchRequest["tfTestCaseTitleLike"] != null && searchRequest["tfTestCaseTitleLike"].isNotEmpty) {
  //     findRequest["tfTestCaseTitleLike"] = "%${searchRequest["tfTestCaseTitleLike"]}%";
  //   }
  //   if (searchRequest["tfTestCaseNameLike"] != null && searchRequest["tfTestCaseNameLike"].isNotEmpty) {
  //     findRequest["tfTestCaseNameLike"] = "%${searchRequest["tfTestCaseNameLike"]}%";
  //   }
  //   if (searchRequest["tfTestCaseTfTestVersionId"] != null) {
  //     findRequest["tfTestCaseTfTestVersionId"] = searchRequest["tfTestCaseTfTestVersionId"];
  //   }
  //   if (searchRequest["tfTestCaseTfTestEnvironmentId"] != null && searchRequest["tfTestCaseTfTestEnvironmentId"].isNotEmpty) {
  //     findRequest["tfTestCaseTfTestEnvironmentId"] = searchRequest["tfTestCaseTfTestEnvironmentId"];
  //   }
  //   if (searchRequest["status"] != null) {
  //     findRequest["status"] = searchRequest["status"];
  //   }
  //   if (searchRequest["statusGt"] != null) {
  //     findRequest["statusGt"] = searchRequest["statusGt"];
  //   }
  //   if (searchRequest["statusLt"] != null) {
  //     findRequest["statusLt"] = searchRequest["statusLt"];
  //   }
  //   if (searchRequest["tfTestCaseTfTestComponentId"] != null) {
  //     findRequest["tfTestCaseTfTestComponentId"] = searchRequest["tfTestCaseTfTestComponentId"];
  //   }
  //   if (searchRequest["sysUserId"] != null) {
  //     findRequest["sysUserId"] = searchRequest["sysUserId"];
  //   }
  //   if (searchRequest["tfTestCaseSysUserId"] != null) {
  //     findRequest["tfTestCaseSysUserId"] = searchRequest["tfTestCaseSysUserId"];
  //   }
  //   if (searchRequest["startDateGt"] != null) {
  //     findRequest["startDateGt"] = searchRequest["startDateGt"];
  //   }
  //   if (searchRequest["createDateGt"] != null) {
  //     findRequest["createDateGt"] = searchRequest["createDateGt"];
  //   }
  //   if (searchRequest["createDateLt"] != null) {
  //     findRequest["createDateLt"] = searchRequest["createDateLt"];
  //   }
  //   if (widget.testCaseId != null) {
  //     findRequest["tfTestCaseId"] = widget.testCaseId!;
  //   }
  //   var response = await httpPost("/test-framework-api/user/test-framework/tf-test-run-report/search", findRequest, context);
  //   if (response.containsKey("body") && response["body"] is String == false) {
  //     setState(() {
  //       chartData = [
  //         if (response["body"]["result"]["passedCount"] != null) ChartData("Pass", response["body"]["result"]["passedCount"], Colors.green),
  //         if (response["body"]["result"]["failedCount"] != null) ChartData("Fail", response["body"]["result"]["failedCount"], Colors.red),
  //         if (response["body"]["result"]["errorCount"] != null) ChartData("Error", response["body"]["result"]["errorCount"], Colors.grey),
  //         if (response["body"]["result"]["processingCount"] != null)
  //           ChartData("Processing", response["body"]["result"]["processingCount"], Colors.blueAccent),
  //         if (response["body"]["result"]["waitingCount"] != null) ChartData("Waiting", response["body"]["result"]["waitingCount"], Colors.orange),
  //         if (response["body"]["result"]["canceledCount"] != null)
  //           ChartData("Canceled", response["body"]["result"]["canceledCount"], const Color.fromARGB(255, 141, 108, 108)),
  //       ];
  //       passedCount = response["body"]["result"]["passedCount"] ?? 0;
  //       failedCount = response["body"]["result"]["failedCount"] ?? 0;
  //       if (response["body"]["result"]["runTimeAvg"] != null) {
  //         runTimeAvg = (Duration(milliseconds: response["body"]["result"]["runTimeAvg"] ?? 0).inMilliseconds / 1000)
  //             .toStringAsFixed(2)
  //             .replaceFirst('.', ',')
  //             .padLeft(5, '0');
  //       } else {
  //         runTimeAvg = "0";
  //       }
  //     });
  //   }
  // }

  getListTfTestCaseRun(page) async {
    if ((page - 1) * rowPerPage > rowCount) {
      page = (1.0 * rowCount / rowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    Map<dynamic, dynamic> findRequest = {
      "queryOffset": (page - 1) * rowPerPage,
      "queryLimit": rowPerPage,
      "prjProjectId": prjProjectId,
      "tfTestCaseIdNotNull": true,
      "tfTestCaseType": widget.type,
    };
    findRequest.addAll(searchRequest);
    if (searchRequest["tfTestCaseTitleLike"] != null && searchRequest["tfTestCaseTitleLike"].isNotEmpty) {
      findRequest["tfTestCaseTitleLike"] = "%${searchRequest["tfTestCaseTitleLike"]}%";
    }
    if (searchRequest["tfTestCaseNameLike"] != null && searchRequest["tfTestCaseNameLike"].isNotEmpty) {
      findRequest["tfTestCaseNameLike"] = "%${searchRequest["tfTestCaseNameLike"]}%";
    }
    if (widget.testCaseId != null) {
      findRequest["tfTestCaseId"] = widget.testCaseId!;
    }
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-run/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPage = page;
        rowCount = response["body"]["rowCount"];
        resultListTestSuiteRun = response["body"]["resultList"];
      });
      return 0;
    }
  }

  @override
  void initState() {
    super.initState();
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    prjProjectId = navigationModel.prjProjectId ?? "";
    futureResultTestRun = getListTfTestCaseRun(currentPage);
    // _tooltipBehavior = TooltipBehavior(
    //     enable: true,
    //     animationDuration: 500,
    //     duration: 1000,
    //     color: Colors.transparent,
    //     builder: (dynamic data, dynamic point, dynamic series, int pointIndex, int seriesIndex) {
    //       return Container(
    //         padding: const EdgeInsets.all(10),
    //         decoration: BoxDecoration(
    //           borderRadius: BorderRadius.circular(5),
    //           color: data.color,
    //         ),
    //         child: MultiLanguageText(
    //           name: data.x == "Pass"
    //               ? "pass_count"
    //               : data.x == "Fail"
    //                   ? "fail_count"
    //                   : data.x == "Error"
    //                       ? "error_count"
    //                       : data.x == "Processing"
    //                           ? "processing_count"
    //                           : data.x == "Waiting"
    //                               ? "waiting_count"
    //                               : "canceled_count",
    //           defaultValue: "${data.x} : \$0",
    //           variables: ["${data.y}"],
    //           style:
    //               TextStyle(color: const Color.fromARGB(255, 0, 0, 0), fontSize: context.fontSizeManager.fontSizeText16, fontWeight: FontWeight.w700),
    //         ),
    //       );
    //     });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, LanguageModel>(builder: (context, navigationModel, languageModel, child) {
      return FutureBuilder(
          future: futureResultTestRun,
          builder: (context, dataTestRun) {
            if (dataTestRun.hasData) {
              return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
                return Material(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: ListView(
                          children: [
                            NavMenuTabV1(
                              currentUrl: "/runs/${widget.type}",
                              margin: const EdgeInsets.fromLTRB(50, 20, 50, 0),
                              navigationList: context.testRunsMenuList,
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(50, 20, 50, 30),
                              child: Column(
                                children: [
                                  HeaderListTestWidget(
                                    type: widget.type == "test_case"
                                        ? "Test Case Runs"
                                        : widget.type == "test_scenario"
                                            ? "Test Scenario Runs"
                                            : widget.type == "manual_test"
                                                ? "Test Manual Runs"
                                                : "Test Suite Runs",
                                    title: widget.type == "test_case"
                                        ? multiLanguageString(name: "test_case_runs", defaultValue: "Test Case Runs", context: context)
                                        : widget.type == "test_scenario"
                                            ? multiLanguageString(name: "test_scenario_runs", defaultValue: "Test Scenario Runs", context: context)
                                            : widget.type == "manual_test"
                                                ? multiLanguageString(name: "test_manual_runs", defaultValue: "Test Manual Runs", context: context)
                                                : multiLanguageString(name: "test_suite_runs", defaultValue: "Test Suite Runs", context: context),
                                    countList: rowCount,
                                    callbackFilter: (result) {
                                      searchRequest = result;
                                      getListTfTestCaseRun(currentPage);
                                    },
                                  ),
                                  // chartWidget(),
                                  if (resultListTestSuiteRun.isNotEmpty) tableWidget(constraints, context),
                                  SelectionArea(
                                    child: DynamicTablePagging(
                                      rowCount,
                                      currentPage,
                                      rowPerPage,
                                      pageChangeHandler: (page) {
                                        futureResultTestRun = getListTfTestCaseRun(page);
                                      },
                                      rowPerPageChangeHandler: (value) {
                                        setState(() {
                                          rowPerPage = value;
                                          futureResultTestRun = getListTfTestCaseRun(1);
                                        });
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              });
            } else if (dataTestRun.hasError) {
              return Text('${dataTestRun.error}');
            }
            return const Center(child: CircularProgressIndicator());
          });
    });
  }

  SelectionArea tableWidget(BoxConstraints constraints, BuildContext context) {
    return SelectionArea(
      child: Container(
        margin: const EdgeInsets.only(top: 40),
        child: (constraints.maxWidth < 1200)
            ? SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: SizedBox(
                  width: 1200,
                  child: tableBodyWidget(context),
                ),
              )
            : tableBodyWidget(context),
      ),
    );
  }

  Column tableBodyWidget(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.all(15),
          decoration: const BoxDecoration(
            border: Border(
              top: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
              bottom: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
              left: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
              right: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
            ),
          ),
          child: Row(
            children: [
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: MultiLanguageText(name: "test_code", defaultValue: "Test code", isLowerCase: false, style: Style(context).styleTextDataColumn),
              )),
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: MultiLanguageText(name: "name", defaultValue: "Test name", isLowerCase: false, style: Style(context).styleTextDataColumn),
              )),
              SizedBox(
                  width: 150,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Center(
                        child: MultiLanguageText(
                            name: "start_run", defaultValue: "Start run", isLowerCase: false, style: Style(context).styleTextDataColumn)),
                  )),
              SizedBox(
                  width: 150,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Center(
                        child: MultiLanguageText(
                            name: "run_time", defaultValue: "Run time", isLowerCase: false, style: Style(context).styleTextDataColumn)),
                  )),
              SizedBox(
                  width: 150,
                  child: Center(
                      child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        MultiLanguageText(
                          name: "status",
                          defaultValue: "Status",
                          isLowerCase: false,
                          style: Style(context).styleTextDataColumn,
                        ),
                        Tooltip(
                          message: multiLanguageString(
                              name: "pass_fail_waiting_error_processing_canceled",
                              defaultValue: "Green = Pass\nRed = Fail\nOrange = Waiting\nGrey = Error\nBlue = Processing\nBlack = Canceled",
                              context: context),
                          child: const Icon(
                            Icons.info,
                            color: Colors.grey,
                            size: 16,
                          ),
                        ),
                      ],
                    ),
                  ))),
              SizedBox(width: 150, child: Center(child: Text("", style: Style(context).styleTextDataColumn))),
            ],
          ),
        ),
        for (var testSuiteRun in resultListTestSuiteRun)
          RowTableTestRunWidget(
            testSuiteRun,
            prjProjectId,
            type: widget.type,
            callback: () {
              getListTfTestCaseRun(currentPage);
            },
          ),
      ],
    );
  }

  // Container chartWidget() {
  //   return Container(
  //     margin: const EdgeInsets.only(top: 30),
  //     height: 250,
  //     child: Row(
  //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //       children: [
  //         Expanded(
  //           flex: 8,
  //           child: Container(
  //             padding: const EdgeInsets.only(top: 20),
  //             decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: const Color(0xffEEEFF4)),
  //             child: SfCartesianChart(tooltipBehavior: _tooltipBehavior, primaryXAxis: CategoryAxis(), series: <CartesianSeries>[
  //               ColumnSeries<ChartData, String>(
  //                   animationDuration: 2000,
  //                   width: 0.3,
  //                   enableTooltip: true,
  //                   borderRadius: const BorderRadius.only(topRight: Radius.circular(5.0), topLeft: Radius.circular(5.0)),
  //                   dataSource: chartData,
  //                   xValueMapper: (ChartData data, _) => multiLanguageString(
  //                       name: data.x == "Pass"
  //                           ? "pass"
  //                           : data.x == "Fail"
  //                               ? "fail"
  //                               : data.x == "Error"
  //                                   ? "error"
  //                                   : data.x == "Processing"
  //                                       ? "processing"
  //                                       : data.x == "Waiting"
  //                                           ? "waiting"
  //                                           : "canceled",
  //                       defaultValue: data.x,
  //                       context: context),
  //                   yValueMapper: (ChartData data, _) => data.y,
  //                   pointColorMapper: (ChartData data, _) => data.color),
  //             ]),
  //           ),
  //         ),
  //         Expanded(
  //           flex: 2,
  //           child: Column(
  //             children: [
  //               Expanded(
  //                 child: Container(
  //                   margin: const EdgeInsets.only(bottom: 10, left: 10),
  //                   decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.green),
  //                   child: InkWell(
  //                     borderRadius: BorderRadius.circular(10),
  //                     onTap: () {
  //                       if (searchRequest["status"] == null || searchRequest["status"] != 1) {
  //                         searchRequest["status"] = 1;
  //                       } else {
  //                         searchRequest["status"] = null;
  //                       }
  //                       search(searchRequest);
  //                     },
  //                     child: Row(
  //                       children: [
  //                         Container(
  //                           margin: const EdgeInsets.only(left: 10, right: 10),
  //                           child: const Icon(Icons.done, size: 20),
  //                         ),
  //                         MultiLanguageText(
  //                           name: "count_passed",
  //                           defaultValue: "\$0 Passed",
  //                           variables: ["$passedCount"],
  //                           style: TextStyle(
  //                               color: const Color(0xff313131), fontSize: context.fontSizeManager.fontSizeText16, fontWeight: FontWeight.w700),
  //                         )
  //                       ],
  //                     ),
  //                   ),
  //                 ),
  //               ),
  //               Expanded(
  //                 child: Container(
  //                   margin: const EdgeInsets.only(bottom: 10, left: 10),
  //                   decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.red),
  //                   child: InkWell(
  //                     borderRadius: BorderRadius.circular(10),
  //                     onTap: () {
  //                       if (searchRequest["status"] == null || searchRequest["status"] != -1) {
  //                         searchRequest["status"] = -1;
  //                       } else {
  //                         searchRequest["status"] = null;
  //                       }
  //                       search(searchRequest);
  //                     },
  //                     child: Row(
  //                       children: [
  //                         Container(
  //                           margin: const EdgeInsets.only(left: 10, right: 10),
  //                           child: const Icon(Icons.bar_chart_rounded, size: 20),
  //                         ),
  //                         MultiLanguageText(
  //                           name: "count_failed",
  //                           defaultValue: "\$0 Failed",
  //                           variables: ["$failedCount"],
  //                           style: TextStyle(
  //                               color: const Color(0xff313131), fontSize: context.fontSizeManager.fontSizeText16, fontWeight: FontWeight.w700),
  //                         )
  //                       ],
  //                     ),
  //                   ),
  //                 ),
  //               ),
  //               Expanded(
  //                 child: Container(
  //                   margin: const EdgeInsets.only(left: 10),
  //                   decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.lightBlue[100]),
  //                   child: Row(
  //                     children: [
  //                       Container(
  //                         margin: const EdgeInsets.only(left: 10, right: 10),
  //                         child: const Icon(Icons.timer, size: 20),
  //                       ),
  //                       Expanded(
  //                         child: MultiLanguageText(
  //                           name: "avg_duration",
  //                           defaultValue: "\$0s Avg. duration",
  //                           variables: [runTimeAvg],
  //                           style: TextStyle(
  //                               color: const Color(0xff313131), fontSize: context.fontSizeManager.fontSizeText16, fontWeight: FontWeight.w700),
  //                         ),
  //                       )
  //                     ],
  //                   ),
  //                 ),
  //               ),
  //             ],
  //           ),
  //         )
  //       ],
  //     ),
  //   );
  // }
}

class RowTableTestRunWidget extends StatefulWidget {
  final dynamic rowData;
  final String prjProjectId;
  final String? type;
  final String? tfTestId;
  final VoidCallback? callback;
  const RowTableTestRunWidget(this.rowData, this.prjProjectId, {Key? key, this.type, this.tfTestId, this.callback}) : super(key: key);

  @override
  State<RowTableTestRunWidget> createState() => _RowTableTestRunWidgetState();
}

class _RowTableTestRunWidgetState extends CustomState<RowTableTestRunWidget> {
  late SecurityModel securityModel;
  @override
  void initState() {
    super.initState();
    securityModel = Provider.of<SecurityModel>(context, listen: false);
  }

  downloadCode(id) async {
    File? downloadedFile = await httpDownloadFile(
        context, "/test-framework-api/user/test-framework-file/tf-test-run/$id/source-code", securityModel.authorization, 'Output.zip');
    if (downloadedFile != null && mounted) {
      var snackBar = SnackBar(
        content: MultiLanguageText(name: "file_downloaded_to", defaultValue: "File downloaded to \$0 .", variables: ["$downloadedFile"]),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  downloadResult(id) async {
    File? downloadedFile = await httpDownloadFile(
        context, "/test-framework-api/user/test-framework-file/tf-test-run/$id/export-result", securityModel.authorization, 'Output.xlsx');
    if (downloadedFile != null && mounted) {
      var snackBar = SnackBar(
        content: MultiLanguageText(name: "file_downloaded_to", defaultValue: "File downloaded to \$0 .", variables: ["$downloadedFile"]),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<NavigationModel>(
      builder: (context, navigationModel, child) {
        return Column(
          children: [
            GestureDetector(
              onTap: () async {
                navigationModel.navigate("/qa-platform/project/${navigationModel.prjProjectId}/runs/${widget.type}/test-run/${widget.rowData["id"]}");
              },
              child: Container(
                padding: const EdgeInsets.all(15),
                decoration: const BoxDecoration(
                  border: Border(
                    top: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
                    bottom: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
                    left: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
                    right: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
                  ),
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: Wrap(
                          children: [
                            Text(
                              widget.rowData["tfTestCase"]["name"] ?? "",
                              softWrap: true,
                              style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: Text(
                          widget.rowData["tfTestCase"]["title"] ?? "",
                          softWrap: true,
                          style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 150,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              widget.rowData["startDate"] != null
                                  ? DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(widget.rowData["startDate"]).toLocal())
                                  : "",
                              softWrap: true,
                              style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                            ),
                            Text(
                              widget.rowData["startDate"] != null
                                  ? DateFormat('HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(widget.rowData["startDate"]).toLocal())
                                  : "",
                              softWrap: true,
                              style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 150,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: Center(
                          child: Text(
                            (widget.rowData["endDate"] != null && widget.rowData["startDate"] != null)
                                ? "${(Duration(milliseconds: DateTime.parse(widget.rowData["endDate"]).difference(DateTime.parse(widget.rowData["startDate"])).inMilliseconds).inMilliseconds / 1000).toStringAsFixed(2).replaceFirst('.', ',').padLeft(5, '0')} (S)"
                                : "0 (S)",
                            softWrap: true,
                            style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 150,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          IntrinsicWidth(
                            child: BadgeText6StatusWidget(
                              status: widget.rowData["status"],
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 150,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              IconButton(
                                onPressed: () {
                                  NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
                                  if (widget.type == "test_case") {
                                    navigationModel.navigate(
                                        "/qa-platform/project/${navigationModel.prjProjectId}/test-list/test_case/${widget.rowData["tfTestCase"]["id"]}/re-run/${widget.rowData["id"]}/${widget.rowData["tfTestCase"]["parentId"]}");
                                  } else {
                                    navigationModel.navigate(
                                        "/qa-platform/project/${navigationModel.prjProjectId}/test-list/${widget.type}/${widget.rowData["tfTestCaseId"]}/re-run/${widget.rowData["id"]}");
                                  }
                                },
                                icon: const Icon(Icons.refresh, color: Color.fromRGBO(49, 49, 49, 1)),
                                splashRadius: 20,
                                tooltip: multiLanguageString(name: "rerun", defaultValue: "Rerun", context: context),
                              ),
                              IconButton(
                                onPressed: () {
                                  downloadResult(widget.rowData["id"]);
                                },
                                icon: const Icon(Icons.download, color: Color.fromRGBO(49, 49, 49, 1)),
                                splashRadius: 20,
                                tooltip: multiLanguageString(name: "download_result", defaultValue: "Download result", context: context),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              if (widget.rowData["status"] > -1 &&
                                  widget.rowData["status"] != 1 &&
                                  widget.rowData["status"] != 0 &&
                                  widget.rowData["status"] != 1)
                                IconButton(
                                  onPressed: () {
                                    dialogDeleteDirectoryTestCase(widget.rowData["id"]);
                                  },
                                  icon: const Icon(Icons.stop_circle, color: Color.fromRGBO(49, 49, 49, 1)),
                                  splashRadius: 20,
                                  tooltip: multiLanguageString(name: "cancel", defaultValue: "Cancel", context: context),
                                ),
                              if (securityModel.hasUserPermission("ADMIN_CODE") || securityModel.hasUserPermission("PROJECT_CODE"))
                                IconButton(
                                  onPressed: () {
                                    downloadCode(widget.rowData["id"]);
                                  },
                                  icon: const FaIcon(FontAwesomeIcons.download),
                                  color: widget.rowData["status"] == 1
                                      ? Colors.green
                                      : widget.rowData["status"] == -1
                                          ? Colors.red
                                          : widget.rowData["status"] == 0
                                              ? Colors.orange
                                              : widget.rowData["status"] == -13
                                                  ? const Color.fromRGBO(49, 49, 49, 1)
                                                  : widget.rowData["status"] < -1
                                                      ? Colors.grey
                                                      : Colors.blueAccent,
                                  splashRadius: 20,
                                  tooltip: multiLanguageString(name: "download_code", defaultValue: "Download code", context: context),
                                ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  dialogViewImageTestRun(String finalImageFile) {
    showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          return ViewImageTestRunWidget(finalImageFile: finalImageFile);
        });
  }

  dialogDeleteDirectoryTestCase(String id) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: const EdgeInsets.all(0),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MultiLanguageText(
                name: "cancel",
                defaultValue: "Cancel",
                isLowerCase: false,
                style: Style(context).styleTitleDialog,
              ),
              TextButton(
                  onPressed: () => Navigator.pop(context, 'Cancel'),
                  child: const Icon(
                    Icons.close,
                    color: Colors.black,
                  ))
            ],
          ),
          content: Container(
            decoration: const BoxDecoration(
                border:
                    Border(top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)), bottom: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)))),
            margin: const EdgeInsets.only(top: 20, bottom: 10),
            width: 500,
            padding: const EdgeInsets.all(20),
            child: IntrinsicHeight(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text.rich(
                    TextSpan(
                        text: multiLanguageString(
                            name: "are_you_sure_you_want_to_cancel", defaultValue: "Are you sure you want to cancel ", context: context),
                        children: const <InlineSpan>[
                          TextSpan(
                            text: 'this test run',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(text: ' ?')
                        ]),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 20),
                    decoration: BoxDecoration(
                      color: const Color.fromRGBO(228, 115, 87, 1),
                      borderRadius: BorderRadius.circular(5),
                    ),
                    padding: const EdgeInsets.only(left: 5),
                    child: Container(
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.horizontal(
                          right: Radius.circular(5),
                        ),
                        color: Color.fromRGBO(255, 233, 217, 1),
                      ),
                      padding: const EdgeInsets.all(15),
                      child: const Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Icon(Icons.warning, color: Color.fromRGBO(243, 86, 40, 1)),
                          SizedBox(width: 10),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              MultiLanguageText(
                                name: "warning",
                                defaultValue: "Warning",
                                style: TextStyle(color: Color.fromARGB(255, 206, 20, 20), fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 10),
                              MultiLanguageText(
                                name: "cant_undo_action",
                                defaultValue: "You can't undo this action.",
                                style: TextStyle(color: Color.fromARGB(255, 187, 103, 82)),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          actionsAlignment: MainAxisAlignment.center,
          actions: [
            Container(
              margin: const EdgeInsets.only(right: 5),
              height: 40,
              width: 120,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color>(
                    const Color.fromRGBO(225, 46, 59, 1),
                  ),
                ),
                onPressed: () async {
                  Navigator.of(context).pop();
                  await httpPost("/test-framework-api/user/test-framework/tf-test-run/${widget.rowData["id"]}/stop", {}, context);
                  widget.callback!();
                },
                child: const MultiLanguageText(
                  name: "continue",
                  defaultValue: "Continue",
                  isLowerCase: false,
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
            const ButtonCancel()
          ],
        );
      },
    );
  }
}

class ViewImageTestRunWidget extends StatelessWidget {
  final String finalImageFile;
  const ViewImageTestRunWidget({Key? key, required this.finalImageFile}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Image image = Image.network(
      "$baseUrl/record/$finalImageFile",
      alignment: Alignment.topCenter,
      fit: BoxFit.fill,
    );
    Completer<ui.Image> completer = Completer<ui.Image>();

    image.image.resolve(const ImageConfiguration()).addListener(ImageStreamListener((ImageInfo info, bool _) {
      completer.complete(info.image);
    }));
    return FutureBuilder<ui.Image>(
      future: completer.future,
      builder: (BuildContext context, AsyncSnapshot<ui.Image> snapshot) {
        if (snapshot.hasData) {
          return AlertDialog(
            contentPadding: const EdgeInsets.all(0),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                MultiLanguageText(
                  name: "view_error_image",
                  defaultValue: "View the error image of Test Run",
                  style: Style(context).styleTitleDialog,
                ),
                TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Icon(
                      Icons.close,
                      color: Colors.black,
                    ))
              ],
            ),
            content: Container(
              padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
              decoration: const BoxDecoration(
                border: Border(
                  top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
                  bottom: BorderSide(
                    color: Color.fromRGBO(216, 218, 229, 1),
                  ),
                ),
              ),
              margin: const EdgeInsets.only(top: 20, bottom: 10),
              child: Center(
                child: image,
              ),
            ),
          );
        } else {
          return const Center(
              child: Column(
            children: [
              MultiLanguageText(name: "image_loading", defaultValue: "Image Loading..."),
              SizedBox(height: 15),
              CircularProgressIndicator(),
            ],
          ));
        }
      },
    );
  }
}

class PlayVideoTestRunWidget extends StatefulWidget {
  final String recordFile;
  const PlayVideoTestRunWidget({
    Key? key,
    required this.recordFile,
  }) : super(key: key);

  @override
  State<PlayVideoTestRunWidget> createState() => _PlayVideoTestRunWidgetState();
}

class _PlayVideoTestRunWidgetState extends CustomState<PlayVideoTestRunWidget> {
  late VideoPlayerController _controller;
  bool loading = true;

  @override
  void initState() {
    loadVideoPlayer();
    super.initState();
  }

  loadVideoPlayer() async {
    _controller = VideoPlayerController.networkUrl(Uri.parse("$baseUrl/record/${widget.recordFile}"));
    _controller.initialize().then((value) {
      loading = false;
      setState(() {});
      _controller.play();
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueAccent,
        title: const MultiLanguageText(
          name: "play_video",
          defaultValue: "Play video test run",
        ),
      ),
      body: _controller.value.isInitialized
          ? loading
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : Center(
                  child: AspectRatio(
                      aspectRatio: _controller.value.aspectRatio,
                      child: Stack(
                        children: [
                          VideoPlayer(_controller),
                          _ControlsOverlay(controller: _controller),
                          VideoProgressIndicator(_controller, allowScrubbing: true),
                        ],
                      )),
                )
          : const Center(child: CircularProgressIndicator()),
    );
  }
}

class _ControlsOverlay extends StatefulWidget {
  const _ControlsOverlay({Key? key, required this.controller}) : super(key: key);
  final VideoPlayerController controller;

  @override
  State<_ControlsOverlay> createState() => _ControlsOverlayState();
}

class _ControlsOverlayState extends CustomState<_ControlsOverlay> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        AnimatedSwitcher(
          duration: const Duration(milliseconds: 50),
          reverseDuration: const Duration(milliseconds: 200),
          child: widget.controller.value.isPlaying && widget.controller.value.position != widget.controller.value.duration
              ? const SizedBox.shrink()
              : Container(
                  color: Colors.black26,
                  child: Center(
                    child: Icon(
                      Icons.play_arrow,
                      color: Colors.white,
                      size: 100.0,
                      semanticLabel: multiLanguageString(name: "play", defaultValue: "Play", context: context),
                    ),
                  ),
                ),
        ),
        GestureDetector(
          onTap: () {
            setState(() {
              widget.controller.value.isPlaying ? widget.controller.pause() : widget.controller.play();
            });
          },
        ),
      ],
    );
  }
}
