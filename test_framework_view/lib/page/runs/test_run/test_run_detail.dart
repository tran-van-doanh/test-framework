import 'dart:io';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/file_download.dart';
import 'package:test_framework_view/components/badge/badge_text.dart';
import 'package:test_framework_view/components/dialog/dialog_confirm.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/page/runs/test_run/test_scenario_run.dart';

import '../../../common/custom_state.dart';
import '../../../common/dynamic_table.dart';
import '../../../components/dynamic_button.dart';
import '../../../components/style.dart';
import '../../../components/toast.dart';
import '../../../model/model.dart';
import '../../../type.dart';
import '../../editor/editor/editor_page.dart';

class TestRunDetailWidget extends StatefulWidget {
  final String id;
  final String prjProjectId;
  final String type;
  const TestRunDetailWidget({Key? key, required this.id, required this.prjProjectId, required this.type}) : super(key: key);

  @override
  State<TestRunDetailWidget> createState() => _TestRunDetailWidgetState();
}

class _TestRunDetailWidgetState extends CustomState<TestRunDetailWidget> {
  late TooltipBehavior _tooltipBehavior;
  late List<ChartData> chartData = [];
  late Future future;
  var resultTestRun = {};
  var resultTestRunData = [];
  var rowCountTestRunData = 0;
  var currentPageTestRunData = 1;
  var rowPerPageTestRunData = 10;
  late String testRunId;
  late SecurityModel securityModel;
  var searchRequest = {};
  int passedCount = 0;
  int failedCount = 0;
  String runTimeAvg = "";

  @override
  void initState() {
    super.initState();
    securityModel = Provider.of<SecurityModel>(context, listen: false);
    testRunId = widget.id;
    future = search();
    _tooltipBehavior = TooltipBehavior(
        enable: true,
        animationDuration: 500,
        duration: 1000,
        color: Colors.transparent,
        builder: (dynamic data, dynamic point, dynamic series, int pointIndex, int seriesIndex) {
          return Container(
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: data.color,
            ),
            child: MultiLanguageText(
              name: data.x == "Pass"
                  ? "pass_count"
                  : data.x == "Fail"
                      ? "fail_count"
                      : data.x == "Error"
                          ? "error_count"
                          : data.x == "Processing"
                              ? "processing_count"
                              : data.x == "Waiting"
                                  ? "waiting_count"
                                  : "canceled_count",
              defaultValue: "${data.x} : \$0",
              variables: ["${data.y}"],
              style:
                  TextStyle(color: const Color.fromARGB(255, 0, 0, 0), fontSize: context.fontSizeManager.fontSizeText16, fontWeight: FontWeight.w700),
            ),
          );
        });
  }

  search() async {
    await getTestRun(testRunId);
    await getListTestRunData(testRunId, currentPageTestRunData);
    await getTestRunDataReport(testRunId);
    return 0;
  }

  getTestRunDataReport(id) async {
    var findRequest = {
      "prjProjectId": widget.prjProjectId,
      "tfTestRunId": id,
    };
    if (searchRequest["status"] != null) {
      findRequest["status"] = searchRequest["status"];
    }
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-run-data-report/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        chartData = [
          if (response["body"]["result"]["passedCount"] != null) ChartData("Pass", response["body"]["result"]["passedCount"], Colors.green),
          if (response["body"]["result"]["failedCount"] != null) ChartData("Fail", response["body"]["result"]["failedCount"], Colors.red),
          if (response["body"]["result"]["errorCount"] != null) ChartData("Error", response["body"]["result"]["errorCount"], Colors.grey),
          if (response["body"]["result"]["canceledCount"] != null)
            ChartData("Canceled", response["body"]["result"]["canceledCount"], const Color.fromRGBO(49, 49, 49, 1)),
          if (response["body"]["result"]["processingCount"] != null)
            ChartData("Processing", response["body"]["result"]["processingCount"], Colors.blueAccent),
          if (response["body"]["result"]["waitingCount"] != null) ChartData("Waiting", response["body"]["result"]["waitingCount"], Colors.orange),
        ];
        passedCount = response["body"]["result"]["passedCount"] ?? 0;
        failedCount = response["body"]["result"]["failedCount"] ?? 0;
        if (response["body"]["result"]["runTimeAvg"] != null) {
          runTimeAvg = (Duration(milliseconds: response["body"]["result"]["runTimeAvg"] ?? 0).inMilliseconds / 1000)
              .toStringAsFixed(2)
              .replaceFirst('.', ',')
              .padLeft(5, '0');
        } else {
          runTimeAvg = "0";
        }
      });
    }
  }

  setTestActualRun(String id) async {
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-run/$id/mark-actual-run", {}, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      getTestRun(id);
    }
  }

  getTestRun(String id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-run/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        resultTestRun = response["body"]["result"];
      });
      if (resultTestRun["testRunConfig"] != null) {
        await getClient();
      }
    }
    return 0;
  }

  getClient() async {
    for (var client in resultTestRun["testRunConfig"]["clientList"] ?? []) {
      var response = await httpGet("/test-framework-api/user/test-framework/tf-test-profile/${client['tfTestProfileId']}", context);
      if (response.containsKey("body") && response["body"] is String == false) {
        setState(() {
          client["profile"] = response["body"]["result"];
        });
      }
    }
  }

  getListTestRunData(String parentId, page) async {
    if ((page - 1) * rowPerPageTestRunData > rowCountTestRunData) {
      page = (1.0 * rowCountTestRunData / rowPerPageTestRunData).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {
      "prjProjectId": widget.prjProjectId,
      "tfTestRunId": parentId,
      "queryOffset": (page - 1) * rowPerPageTestRunData,
      "queryLimit": rowPerPageTestRunData
    };
    if (searchRequest["status"] != null) {
      findRequest["status"] = searchRequest["status"];
    }
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-run-data/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPageTestRunData = page;
        rowCountTestRunData = response["body"]["rowCount"];
        resultTestRunData = response["body"]["resultList"];
      });
    }
    return 0;
  }

  downloadResult(id) async {
    File? downloadedFile = await httpDownloadFile(
        context, "/test-framework-api/user/test-framework-file/tf-test-run/$id/export-result", securityModel.authorization, 'Output.xlsx');
    if (downloadedFile != null && mounted) {
      var snackBar = SnackBar(
        content: MultiLanguageText(name: "file_downloaded_to", defaultValue: "File downloaded to \$0 .", variables: ["$downloadedFile"]),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  downloadCode(id) async {
    File? downloadedFile = await httpDownloadFile(
        context, "/test-framework-api/user/test-framework-file/tf-test-run/$id/source-code", securityModel.authorization, 'Output.zip');
    if (downloadedFile != null && mounted) {
      var snackBar = SnackBar(
        content: MultiLanguageText(name: "file_downloaded_to", defaultValue: "File downloaded to \$0 .", variables: ["$downloadedFile"]),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, LanguageModel>(
      builder: (context, navigationModel, languageModel, child) {
        return FutureBuilder(
          future: future,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(50, 20, 50, 30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        NavMenuTabV1(
                          currentUrl: "/runs/${widget.type}",
                          navigationList: context.testRunsMenuList,
                          margin: const EdgeInsets.only(bottom: 20),
                        ),
                        header(navigationModel),
                        const SizedBox(
                          height: 20,
                        ),
                        LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
                          if (constraints.maxWidth < 1200) {
                            return Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 20),
                                  child: chartWidget(),
                                ),
                                testRunInformation(),
                              ],
                            );
                          } else {
                            return Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                IntrinsicWidth(
                                  child: testRunInformation(),
                                ),
                                const SizedBox(
                                  width: 50,
                                ),
                                Expanded(
                                  child: chartWidget(),
                                ),
                              ],
                            );
                          }
                        }),
                        if (resultTestRun["testRunConfig"] != null) profileTable(),
                        (resultTestRunData.isNotEmpty) ? testRunTable(navigationModel) : const SizedBox.shrink(),
                      ],
                    ),
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  dialogViewImageTestRun(String finalImageFile) {
    showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          return ViewImageTestRunWidget(finalImageFile: finalImageFile);
        });
  }

  dialogDeleteDirectoryTestCase(String id) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: const EdgeInsets.all(0),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MultiLanguageText(
                name: "cancel",
                defaultValue: "Cancel",
                isLowerCase: false,
                style: Style(context).styleTitleDialog,
              ),
              TextButton(
                  onPressed: () => Navigator.pop(context, 'Cancel'),
                  child: const Icon(
                    Icons.close,
                    color: Colors.black,
                  ))
            ],
          ),
          content: Container(
            decoration: const BoxDecoration(
                border:
                    Border(top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)), bottom: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)))),
            margin: const EdgeInsets.only(top: 20, bottom: 10),
            width: 500,
            padding: const EdgeInsets.all(20),
            child: IntrinsicHeight(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text.rich(
                    TextSpan(
                        text: multiLanguageString(
                            name: "are_you_sure_you_want_to_cancel", defaultValue: "Are you sure you want to cancel ", context: context),
                        children: const <InlineSpan>[
                          TextSpan(
                            text: 'this test run',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(text: ' ?')
                        ]),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 20),
                    decoration: BoxDecoration(
                      color: const Color.fromRGBO(228, 115, 87, 1),
                      borderRadius: BorderRadius.circular(5),
                    ),
                    padding: const EdgeInsets.only(left: 5),
                    child: Container(
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.horizontal(
                          right: Radius.circular(5),
                        ),
                        color: Color.fromRGBO(255, 233, 217, 1),
                      ),
                      padding: const EdgeInsets.all(15),
                      child: const Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Icon(Icons.warning, color: Color.fromRGBO(243, 86, 40, 1)),
                          SizedBox(width: 10),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              MultiLanguageText(
                                name: "warning",
                                defaultValue: "Warning",
                                style: TextStyle(color: Color.fromARGB(255, 206, 20, 20), fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 10),
                              MultiLanguageText(
                                name: "cant_undo_action",
                                defaultValue: "You can't undo this action.",
                                style: TextStyle(color: Color.fromARGB(255, 187, 103, 82)),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          actionsAlignment: MainAxisAlignment.center,
          actions: [
            Container(
              margin: const EdgeInsets.only(right: 5),
              height: 40,
              width: 120,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color>(
                    const Color.fromRGBO(225, 46, 59, 1),
                  ),
                ),
                onPressed: () async {
                  Navigator.of(context).pop();
                  await httpPost("/test-framework-api/user/test-framework/tf-test-run/${resultTestRun["id"]}/stop", {}, context);
                  future = search();
                },
                child: const MultiLanguageText(
                  name: "continue",
                  defaultValue: "Continue",
                  isLowerCase: false,
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
            const ButtonCancel()
          ],
        );
      },
    );
  }

  Widget profileTable() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 10,
        ),
        MultiLanguageText(
          name: "profile",
          defaultValue: "Profile",
          style: Style(context).styleTitleDialog,
        ),
        const SizedBox(
          height: 5,
        ),
        CustomDataTableWidget(
          columns: [
            Expanded(
              flex: 2,
              child: MultiLanguageText(
                name: "client_name",
                defaultValue: "Client name",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              flex: 2,
              child: MultiLanguageText(
                name: "profile_name",
                defaultValue: "Profile name",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "default",
                defaultValue: "Default",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              flex: 2,
              child: MultiLanguageText(
                name: "operating_system",
                defaultValue: "Operating system",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "browser",
                defaultValue: "Browser",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "resolution",
                defaultValue: "Resolution",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
          ],
          rows: [
            for (var client in resultTestRun["testRunConfig"]["clientList"] ?? [])
              if (client["profile"] != null)
                CustomDataRow(
                  cells: [
                    Expanded(
                      flex: 2,
                      child: Text(
                        client["name"],
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Row(
                          children: [
                            const Icon(Icons.description, color: Color.fromRGBO(61, 148, 254, 1)),
                            const SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: Text(
                                client["profile"]["title"] ?? "",
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                                style: Style(context).styleTextDataCell,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: Text(
                        client["profile"]["defaultProfile"] == 0 ? "Not default" : "Default",
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Text(
                        client["profile"]["tfOperatingSystem"]["title"],
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 5),
                        child: Text(
                          client["profile"]["tfBrowser"] != null && client["profile"]["tfBrowserVersion"] != null
                              ? ("${client["profile"]["tfBrowser"]["title"]} ( ${client["profile"]["tfBrowserVersion"]["title"]} )")
                              : "",
                          style: Style(context).styleTextDataCell,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Text(
                        "${client["profile"]["screenWidth"]} X ${client["profile"]["screenHeight"]}",
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ],
                ),
          ],
        ),
        const SizedBox(
          height: 20,
        )
      ],
    );
  }

  Widget testRunTable(NavigationModel navigationModel) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MultiLanguageText(
          name: "test_run_data",
          defaultValue: "Test run data",
          style: Style(context).styleTitleDialog,
        ),
        const SizedBox(
          height: 5,
        ),
        CustomDataTableWidget(
          columns: [
            Expanded(
              child: Text(
                "",
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "agent",
                defaultValue: "Agent",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "start_run",
                defaultValue: "Start run",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "end_run",
                defaultValue: "End run",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "total_run_time",
                defaultValue: "Total run time",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: Center(
                child: MultiLanguageText(
                  name: "final_status",
                  defaultValue: "Final status",
                  isLowerCase: false,
                  style: Style(context).styleTextDataColumn,
                ),
              ),
            ),
            Container(
              width: 150,
            ),
          ],
          rows: [
            for (var testRunData in resultTestRunData)
              CustomDataRow(
                cells: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: MultiLanguageText(
                        name: "data_test_run_data",
                        defaultValue: "Data \$0",
                        variables: ["${testRunData["dataIndex"]}"],
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        (testRunData["sysNode"] != null) ? testRunData["sysNode"]["name"] : "",
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        testRunData["startDate"] != null
                            ? "${DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(testRunData["startDate"]).toLocal())} ${DateFormat('HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(testRunData["startDate"]).toLocal())}"
                            : "",
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        testRunData["endDate"] != null
                            ? "${DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(testRunData["endDate"]).toLocal())} ${DateFormat('HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(testRunData["endDate"]).toLocal())}"
                            : "",
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Text(
                      (testRunData["endDate"] != null && testRunData["startDate"] != null)
                          ? "${(Duration(milliseconds: DateTime.parse(testRunData["endDate"]).difference(DateTime.parse(testRunData["startDate"])).inMilliseconds).inMilliseconds / 1000).toStringAsFixed(2).replaceFirst('.', ',').padLeft(5, '0')} (S)"
                          : "0 (S)",
                      style: TextStyle(
                        fontSize: context.fontSizeManager.fontSizeText16,
                        color: const Color.fromRGBO(49, 49, 49, 1),
                        fontWeight: FontWeight.w500,
                        letterSpacing: 1.5,
                      ),
                    ),
                  ),
                  Expanded(
                      child: Center(
                    child: BadgeText4StatusTestRunDataWidget(
                      status: testRunData["status"],
                    ),
                  )),
                  SizedBox(
                      width: 150,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              if (testRunData["status"] == 0 && testRunData["bugTrackerStatus"] != -1 && testRunData["tfTestCase"]["status"] == 1)
                                IconButton(
                                  onPressed: () {
                                    dialogConfirmReportBug(testRunData);
                                  },
                                  icon: const FaIcon(FontAwesomeIcons.spider),
                                  color: Colors.red,
                                  splashRadius: 20,
                                  tooltip: multiLanguageString(name: "report_bug", defaultValue: "Report bug", context: context),
                                ),
                              if (testRunData["status"] == 0 && testRunData["bugTrackerStatus"] == -1)
                                IconButton(
                                  onPressed: () {},
                                  icon: const FaIcon(FontAwesomeIcons.bugSlash),
                                  color: Colors.red,
                                  splashRadius: 20,
                                  tooltip: multiLanguageString(name: "not_bug", defaultValue: "Not bug", context: context),
                                ),
                              if (testRunData["recordFile"] != null)
                                IconButton(
                                  onPressed: () {
                                    Navigator.of(context).push(
                                      createRoute(
                                        PlayVideoTestRunWidget(
                                          recordFile: testRunData["recordFile"],
                                        ),
                                      ),
                                    );
                                  },
                                  icon: const FaIcon(FontAwesomeIcons.solidCirclePlay),
                                  color: testRunData["status"] == 1 ? Colors.green : Colors.red,
                                  splashRadius: 20,
                                  tooltip: multiLanguageString(name: "play_video_of_test", defaultValue: "Play video of test", context: context),
                                ),
                              if (testRunData["recordFile"] != null)
                                IconButton(
                                  onPressed: () async {
                                    File? downloadedFile =
                                        await httpDownloadFile(context, "/record/${testRunData["recordFile"]}", null, 'Output.mp4');
                                    if (downloadedFile != null && mounted) {
                                      var snackBar = SnackBar(
                                        content: MultiLanguageText(
                                            name: "file_downloaded_to", defaultValue: "File downloaded to \$0 .", variables: ["$downloadedFile"]),
                                      );
                                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                                    }
                                  },
                                  icon: const FaIcon(FontAwesomeIcons.film),
                                  color: testRunData["status"] == 1 ? Colors.green : Colors.red,
                                  splashRadius: 20,
                                  tooltip:
                                      multiLanguageString(name: "download_video_of_test", defaultValue: "Download video of test", context: context),
                                ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              if (testRunData["errorActionPath"] != null && testRunData["errorActionPath"].isNotEmpty)
                                IconButton(
                                  onPressed: () async {
                                    Navigator.of(context).push(
                                      createRoute(
                                        EditorPageRootWidget(
                                          testCaseType: widget.type,
                                          testCaseId:
                                              (widget.type == "test_case") ? testRunData["tfTestCase"]["parentId"] : testRunData["tfTestCaseId"],
                                          errorTestRunResultId: testRunData["id"],
                                          callback: () {},
                                        ),
                                      ),
                                    );
                                  },
                                  icon: const FaIcon(FontAwesomeIcons.bug),
                                  color: Colors.grey,
                                  splashRadius: 20,
                                  tooltip: multiLanguageString(name: "view_debug", defaultValue: "View debug", context: context),
                                ),
                              if (testRunData["finalImageFile"] != null && testRunData["finalImageFile"].isNotEmpty)
                                IconButton(
                                  onPressed: () async {
                                    dialogViewImageTestRun(testRunData["finalImageFile"]);
                                  },
                                  icon: const FaIcon(FontAwesomeIcons.solidImage),
                                  color: Colors.grey,
                                  splashRadius: 20,
                                  tooltip: multiLanguageString(
                                      name: "view_image_error_of_test", defaultValue: "View image error of test", context: context),
                                ),
                            ],
                          )
                        ],
                      )),
                ],
                onSelectChanged: () {
                  navigationModel
                      .navigate("/qa-platform/project/${navigationModel.prjProjectId}/runs/${widget.type}/test-run-data/${testRunData["id"]}");
                },
              ),
          ],
        ),
        const SizedBox(
          height: 20,
        ),
        DynamicTablePagging(
          rowCountTestRunData,
          currentPageTestRunData,
          rowPerPageTestRunData,
          pageChangeHandler: (page) {
            setState(() {
              future = getListTestRunData(testRunId, page);
            });
          },
          rowPerPageChangeHandler: (rowPerPage) {
            setState(() {
              rowPerPageTestRunData = rowPerPage!;
              future = getListTestRunData(testRunId, 1);
            });
          },
        ),
      ],
    );
  }

  dialogConfirmReportBug(testRunData) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: const EdgeInsets.all(0),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MultiLanguageText(
                name: "confirm",
                defaultValue: "Confirm",
                isLowerCase: false,
                style: Style(context).styleTitleDialog,
              ),
              TextButton(
                  onPressed: () => Navigator.pop(context, 'Cancel'),
                  child: const Icon(
                    Icons.close,
                    color: Colors.black,
                  ))
            ],
          ),
          content: Container(
            decoration: const BoxDecoration(
                border:
                    Border(top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)), bottom: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)))),
            margin: const EdgeInsets.only(top: 20, bottom: 10),
            width: 500,
            padding: const EdgeInsets.all(20),
            child: IntrinsicHeight(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text.rich(
                    TextSpan(
                        text: multiLanguageString(
                            name: "are_you_sure_you_want_to_report_debug", defaultValue: "Are you sure you want to report bug ", context: context),
                        children: <InlineSpan>[
                          TextSpan(
                            text: '"Data ${testRunData["dataIndex"]}"',
                            style: const TextStyle(fontWeight: FontWeight.bold),
                          ),
                          const TextSpan(text: ' ?')
                        ]),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 20),
                    decoration: BoxDecoration(
                      color: const Color.fromRGBO(228, 115, 87, 1),
                      borderRadius: BorderRadius.circular(5),
                    ),
                    padding: const EdgeInsets.only(left: 5),
                    child: Container(
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.horizontal(
                          right: Radius.circular(5),
                        ),
                        color: Color.fromRGBO(255, 233, 217, 1),
                      ),
                      padding: const EdgeInsets.all(15),
                      child: const Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Icon(Icons.warning, color: Color.fromRGBO(243, 86, 40, 1)),
                          SizedBox(width: 10),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              MultiLanguageText(
                                name: "warning",
                                defaultValue: "Warning",
                                style: TextStyle(color: Color.fromARGB(255, 206, 20, 20), fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 10),
                              MultiLanguageText(
                                name: "cant_undo_action",
                                defaultValue: "You can't undo this action.",
                                style: TextStyle(color: Color.fromARGB(255, 187, 103, 82)),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          actionsAlignment: MainAxisAlignment.center,
          actions: [
            Container(
              margin: const EdgeInsets.only(right: 5),
              height: 40,
              width: 120,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color>(
                    const Color.fromRGBO(225, 46, 59, 1),
                  ),
                ),
                onPressed: () async {
                  Navigator.of(context).pop();
                  var response =
                      await httpPost("/test-framework-api/user/test-framework/tf-test-run-data/${testRunData["id"]}/report-bug", {}, context);
                  if (response.containsKey("body") && response["body"] is String == false && mounted) {
                    if (response["body"].containsKey("errorMessage")) {
                      showToast(
                        context: this.context,
                        msg: response["body"]["errorMessage"],
                        color: Colors.red,
                        icon: const Icon(Icons.error),
                      );
                    } else {
                      showToast(
                          context: this.context,
                          msg: response["body"]["result"] == true
                              ? multiLanguageString(name: "report_bug_successful", defaultValue: "Report bug successful", context: this.context)
                              : multiLanguageString(name: "report_bug_failure", defaultValue: "Report bug failure", context: this.context),
                          color: response["body"]["result"] == true ? Colors.greenAccent : Colors.red,
                          icon: Icon(response["body"]["result"] == true ? Icons.done : Icons.error));
                    }
                  }
                },
                child: const MultiLanguageText(
                  name: "ok",
                  defaultValue: "Ok",
                  isLowerCase: false,
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
            const ButtonCancel()
          ],
        );
      },
    );
  }

  Widget testRunInformation() {
    return SelectionArea(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                child: MultiLanguageText(
                  name: "run_owner",
                  defaultValue: "Run owner",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(130, 130, 130, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Row(
                children: [
                  const Icon(Icons.person),
                  Text(
                    resultTestRun["sysUser"]["fullname"] ?? "",
                    softWrap: true,
                    style: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText16,
                      color: const Color.fromRGBO(49, 49, 49, 1),
                      fontWeight: FontWeight.w500,
                      letterSpacing: 1.5,
                    ),
                  ),
                ],
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                child: MultiLanguageText(
                  name: "schedule_time",
                  defaultValue: "Schedule time",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(130, 130, 130, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              (resultTestRun["scheduledDate"] != null)
                  ? Text(
                      "${DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultTestRun["scheduledDate"]).toLocal())} ${DateFormat('HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultTestRun["scheduledDate"]).toLocal())}",
                      softWrap: true,
                      style: TextStyle(
                        fontSize: context.fontSizeManager.fontSizeText16,
                        color: const Color.fromRGBO(49, 49, 49, 1),
                        fontWeight: FontWeight.w500,
                        letterSpacing: 1.5,
                      ),
                    )
                  : const SizedBox.shrink(),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MultiLanguageText(
                name: "start_run",
                defaultValue: "Start run",
                style: TextStyle(
                  fontSize: context.fontSizeManager.fontSizeText16,
                  color: const Color.fromRGBO(130, 130, 130, 1),
                  fontWeight: FontWeight.w500,
                  letterSpacing: 1.5,
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              (resultTestRun["startDate"] != null)
                  ? Text(
                      "${DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultTestRun["startDate"]).toLocal())} ${DateFormat('HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultTestRun["startDate"]).toLocal())}",
                      softWrap: true,
                      style: TextStyle(
                        fontSize: context.fontSizeManager.fontSizeText16,
                        color: const Color.fromRGBO(49, 49, 49, 1),
                        fontWeight: FontWeight.w500,
                        letterSpacing: 1.5,
                      ),
                    )
                  : const SizedBox.shrink(),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MultiLanguageText(
                name: "end_run",
                defaultValue: "End run",
                style: TextStyle(
                  fontSize: context.fontSizeManager.fontSizeText16,
                  color: const Color.fromRGBO(130, 130, 130, 1),
                  fontWeight: FontWeight.w500,
                  letterSpacing: 1.5,
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              (resultTestRun["endDate"] != null)
                  ? Text(
                      "${DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultTestRun["endDate"]).toLocal())} ${DateFormat('HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultTestRun["endDate"]).toLocal())}",
                      softWrap: true,
                      style: TextStyle(
                        fontSize: context.fontSizeManager.fontSizeText16,
                        color: const Color.fromRGBO(49, 49, 49, 1),
                        fontWeight: FontWeight.w500,
                        letterSpacing: 1.5,
                      ),
                    )
                  : const SizedBox.shrink(),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MultiLanguageText(
                name: "total_run_time",
                defaultValue: "Total run time",
                style: TextStyle(
                  fontSize: context.fontSizeManager.fontSizeText16,
                  color: const Color.fromRGBO(130, 130, 130, 1),
                  fontWeight: FontWeight.w500,
                  letterSpacing: 1.5,
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Text(
                (resultTestRun["endDate"] != null && resultTestRun["startDate"] != null)
                    ? "${(Duration(milliseconds: DateTime.parse(resultTestRun["endDate"]).difference(DateTime.parse(resultTestRun["startDate"])).inMilliseconds).inMilliseconds / 1000).toStringAsFixed(2).replaceFirst('.', ',').padLeft(5, '0')} (S)"
                    : "0 (S)",
                // (resultTestRun["endDate"] != null && resultTestRun["startDate"] != null && resultTestRun["recordTime"] != null)
                //     ? "${(Duration(milliseconds: DateTime.parse(resultTestRun["endDate"]).difference(DateTime.parse(resultTestRun["startDate"])).inMilliseconds - int.parse(resultTestRun["recordTime"].toString())).inMilliseconds / 1000).toStringAsFixed(2).replaceFirst('.', ',').padLeft(5, '0')} (S)"
                //     : "0 (S)",
                style: TextStyle(
                  fontSize: context.fontSizeManager.fontSizeText16,
                  color: const Color.fromRGBO(49, 49, 49, 1),
                  fontWeight: FontWeight.w500,
                  letterSpacing: 1.5,
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MultiLanguageText(
                name: "status",
                defaultValue: "Status",
                style: TextStyle(
                  fontSize: context.fontSizeManager.fontSizeText16,
                  color: const Color.fromRGBO(130, 130, 130, 1),
                  fontWeight: FontWeight.w500,
                  letterSpacing: 1.5,
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Row(
                children: [
                  IntrinsicWidth(
                    child: BadgeText6StatusWidget(
                      status: resultTestRun["status"],
                    ),
                  ),
                ],
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MultiLanguageText(
                name: "test_run_type",
                defaultValue: "Test run type",
                style: TextStyle(
                  fontSize: context.fontSizeManager.fontSizeText16,
                  color: const Color.fromRGBO(130, 130, 130, 1),
                  fontWeight: FontWeight.w500,
                  letterSpacing: 1.5,
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Text(
                resultTestRun["testRunType"] == "parallel" ? "Parallel" : "Sequence",
                style: TextStyle(
                  fontSize: context.fontSizeManager.fontSizeText16,
                  color: const Color.fromRGBO(49, 49, 49, 1),
                  fontWeight: FontWeight.w500,
                  letterSpacing: 1.5,
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MultiLanguageText(
                name: "version",
                defaultValue: "Version",
                style: TextStyle(
                  fontSize: context.fontSizeManager.fontSizeText16,
                  color: const Color.fromRGBO(130, 130, 130, 1),
                  fontWeight: FontWeight.w500,
                  letterSpacing: 1.5,
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Text(
                resultTestRun["tfTestVersion"]["title"] ?? "",
                style: TextStyle(
                  fontSize: context.fontSizeManager.fontSizeText16,
                  color: const Color.fromRGBO(49, 49, 49, 1),
                  fontWeight: FontWeight.w500,
                  letterSpacing: 1.5,
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MultiLanguageText(
                name: "environment",
                defaultValue: "Environment",
                style: TextStyle(
                  fontSize: context.fontSizeManager.fontSizeText16,
                  color: const Color.fromRGBO(130, 130, 130, 1),
                  fontWeight: FontWeight.w500,
                  letterSpacing: 1.5,
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Text(
                resultTestRun["tfTestEnvironment"]["title"] ?? "",
                style: TextStyle(
                  fontSize: context.fontSizeManager.fontSizeText16,
                  color: const Color.fromRGBO(49, 49, 49, 1),
                  fontWeight: FontWeight.w500,
                  letterSpacing: 1.5,
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                child: MultiLanguageText(
                  name: "agent_type",
                  defaultValue: "Agent type",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(130, 130, 130, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              MultiLanguageText(
                name: resultTestRun["usePublicNode"] == true
                    ? "public"
                    : resultTestRun["usePublicNode"] == false
                        ? "private"
                        : "any_node",
                defaultValue: resultTestRun["usePublicNode"] == true
                    ? "Public"
                    : resultTestRun["usePublicNode"] == false
                        ? "Private"
                        : "Any agent",
                style: TextStyle(
                  fontSize: context.fontSizeManager.fontSizeText16,
                  color: const Color.fromRGBO(49, 49, 49, 1),
                  fontWeight: FontWeight.w500,
                  letterSpacing: 1.5,
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                child: MultiLanguageText(
                  name: "test_run",
                  defaultValue: "Test Run",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(130, 130, 130, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              MultiLanguageText(
                name: resultTestRun["statisticsStatus"] == 0
                    ? "draft"
                    : resultTestRun["statisticsStatus"] == 1
                        ? "complete"
                        : "",
                defaultValue: resultTestRun["statisticsStatus"] == 0
                    ? "Draft"
                    : resultTestRun["statisticsStatus"] == 1
                        ? "Complete"
                        : "",
                style: TextStyle(
                  fontSize: context.fontSizeManager.fontSizeText16,
                  color: const Color.fromRGBO(49, 49, 49, 1),
                  fontWeight: FontWeight.w500,
                  letterSpacing: 1.5,
                ),
              ),
            ],
          ),
          if (resultTestRun["description"] != null)
            ConstrainedBox(
              constraints: const BoxConstraints(maxWidth: 500),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 10,
                  ),
                  MultiLanguageText(
                    name: "description",
                    defaultValue: "Description",
                    style: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText16,
                      color: const Color.fromRGBO(49, 49, 49, 1),
                      fontWeight: FontWeight.w600,
                      letterSpacing: 1.5,
                    ),
                  ),
                  Text(
                    resultTestRun["description"],
                    style: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText16,
                      color: const Color.fromRGBO(49, 49, 49, 1),
                      fontWeight: FontWeight.w500,
                      letterSpacing: 1.5,
                    ),
                  ),
                ],
              ),
            ),
        ],
      ),
    );
  }

  Widget header(NavigationModel navigationModel) {
    return Row(
      children: [
        GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: const Icon(Icons.arrow_back),
        ),
        const SizedBox(
          width: 5,
        ),
        Expanded(
          child: Text(
            resultTestRun["tfTestCase"]["title"],
            style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
          ),
        ),
        Row(
          children: [
            if (resultTestRun["statisticsStatus"] == 0)
              IconButton(
                onPressed: () {
                  showDialog<String>(
                    context: context,
                    builder: (BuildContext context) {
                      return DialogConfirm(
                        callback: () => setTestActualRun(resultTestRun["id"]),
                      );
                    },
                  );
                },
                icon: const Icon(Icons.verified_user, color: Color.fromRGBO(49, 49, 49, 1)),
                splashRadius: 20,
                tooltip: multiLanguageString(name: "mark_complete", defaultValue: "Mark complete", context: context),
              ),
            IconButton(
              onPressed: () {
                Navigator.of(context).push(
                  createRoute(
                    EditorPageRootWidget(
                      testCaseType: widget.type,
                      testCaseId: (widget.type == "test_case") ? resultTestRun["tfTestCase"]["parentId"] : resultTestRun["tfTestCaseId"],
                      callback: () {},
                    ),
                  ),
                );
              },
              icon: const Icon(Icons.settings, color: Color.fromRGBO(49, 49, 49, 1)),
              splashRadius: 20,
              tooltip: multiLanguageString(name: "config", defaultValue: "Config", context: context),
            ),
            IconButton(
              onPressed: () {
                NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
                if (widget.type == "test_case") {
                  navigationModel.navigate(
                      "/qa-platform/project/${navigationModel.prjProjectId}/test-list/test_case/${resultTestRun["tfTestCaseId"]}/re-run/${resultTestRun["id"]}/${resultTestRun["tfTestCase"]["parentId"]}");
                } else {
                  navigationModel.navigate(
                      "/qa-platform/project/${navigationModel.prjProjectId}/test-list/${widget.type}/${resultTestRun["tfTestCaseId"]}/re-run/${resultTestRun["id"]}");
                }
              },
              icon: const Icon(Icons.refresh, color: Color.fromRGBO(49, 49, 49, 1)),
              splashRadius: 20,
              tooltip: multiLanguageString(name: "rerun", defaultValue: "Rerun", context: context),
            ),
            IconButton(
              onPressed: () {
                downloadResult(resultTestRun["id"]);
              },
              icon: const Icon(Icons.download, color: Color.fromRGBO(49, 49, 49, 1)),
              splashRadius: 20,
              tooltip: multiLanguageString(name: "download_result", defaultValue: "Download result", context: context),
            ),
            if (resultTestRun["status"] > -1 && resultTestRun["status"] != 1 && resultTestRun["status"] != 0 && resultTestRun["status"] != 1)
              IconButton(
                onPressed: () {
                  dialogDeleteDirectoryTestCase(resultTestRun["id"]);
                },
                icon: const Icon(Icons.stop_circle, color: Color.fromRGBO(49, 49, 49, 1)),
                splashRadius: 20,
                tooltip: multiLanguageString(name: "cancel", defaultValue: "Cancel", context: context),
              ),
            if (securityModel.hasUserPermission("ADMIN_CODE") || securityModel.hasUserPermission("PROJECT_CODE"))
              IconButton(
                onPressed: () {
                  downloadCode(resultTestRun["id"]);
                },
                icon: const FaIcon(FontAwesomeIcons.download),
                color: resultTestRun["status"] == 1
                    ? Colors.green
                    : resultTestRun["status"] == -1
                        ? Colors.red
                        : resultTestRun["status"] == 0
                            ? Colors.orange
                            : resultTestRun["status"] < -1
                                ? const Color.fromRGBO(49, 49, 49, 1)
                                : Colors.blueAccent,
                splashRadius: 20,
                tooltip: multiLanguageString(name: "download_code", defaultValue: "Download code", context: context),
              ),
          ],
        ),
        IconButton(
          onPressed: () {
            future = search();
          },
          icon: const Icon(Icons.autorenew, color: Color.fromRGBO(49, 49, 49, 1)),
          splashRadius: 20,
          tooltip: multiLanguageString(name: "refresh", defaultValue: "Refresh", context: context),
        ),
      ],
    );
  }

  SizedBox chartWidget() {
    return SizedBox(
      height: 250,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 8,
            child: Container(
              padding: const EdgeInsets.only(top: 20),
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: const Color(0xffEEEFF4)),
              child: SfCartesianChart(tooltipBehavior: _tooltipBehavior, primaryXAxis: const CategoryAxis(), series: <CartesianSeries>[
                ColumnSeries<ChartData, String>(
                    animationDuration: 2000,
                    width: 0.3,
                    enableTooltip: true,
                    borderRadius: const BorderRadius.only(topRight: Radius.circular(5.0), topLeft: Radius.circular(5.0)),
                    dataSource: chartData,
                    xValueMapper: (ChartData data, _) => multiLanguageString(
                        name: data.x == "Pass"
                            ? "pass"
                            : data.x == "Fail"
                                ? "fail"
                                : data.x == "Error"
                                    ? "error"
                                    : data.x == "Processing"
                                        ? "processing"
                                        : data.x == "Waiting"
                                            ? "waiting"
                                            : "canceled",
                        defaultValue: data.x,
                        context: context),
                    yValueMapper: (ChartData data, _) => data.y,
                    pointColorMapper: (ChartData data, _) => data.color),
              ]),
            ),
          ),
          Expanded(
            flex: 2,
            child: Column(
              children: [
                Expanded(
                  child: Container(
                    margin: const EdgeInsets.only(bottom: 10, left: 10),
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.green),
                    child: InkWell(
                      borderRadius: BorderRadius.circular(10),
                      onTap: () {
                        if (searchRequest["status"] == null || searchRequest["status"] != 1) {
                          searchRequest["status"] = 1;
                        } else {
                          searchRequest["status"] = null;
                        }
                        getTestRunDataReport(testRunId);
                        getListTestRunData(testRunId, currentPageTestRunData);
                      },
                      child: Row(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(left: 10, right: 10),
                            child: const Icon(Icons.done, size: 20),
                          ),
                          MultiLanguageText(
                            name: "count_passed",
                            defaultValue: "\$0 Passed",
                            variables: ["$passedCount"],
                            style: TextStyle(
                                color: const Color(0xff313131), fontSize: context.fontSizeManager.fontSizeText16, fontWeight: FontWeight.w700),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    margin: const EdgeInsets.only(bottom: 10, left: 10),
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.red),
                    child: InkWell(
                      borderRadius: BorderRadius.circular(10),
                      onTap: () {
                        if (searchRequest["status"] == null || searchRequest["status"] != 0) {
                          searchRequest["status"] = 0;
                        } else {
                          searchRequest["status"] = null;
                        }
                        getTestRunDataReport(testRunId);
                        getListTestRunData(testRunId, currentPageTestRunData);
                      },
                      child: Row(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(left: 10, right: 10),
                            child: const Icon(Icons.bar_chart_rounded, size: 20),
                          ),
                          MultiLanguageText(
                            name: "count_failed",
                            defaultValue: "\$0 Failed",
                            variables: ["$failedCount"],
                            style: TextStyle(
                                color: const Color(0xff313131), fontSize: context.fontSizeManager.fontSizeText16, fontWeight: FontWeight.w700),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    margin: const EdgeInsets.only(left: 10),
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.lightBlue[100]),
                    child: Row(
                      children: [
                        Container(
                          margin: const EdgeInsets.only(left: 10, right: 10),
                          child: const Icon(Icons.timer, size: 20),
                        ),
                        Expanded(
                          child: MultiLanguageText(
                            name: "average_duration_of_test_run_data",
                            defaultValue: "\$0s Average duration of Test run data",
                            variables: [runTimeAvg],
                            style: TextStyle(
                                color: const Color(0xff313131), fontSize: context.fontSizeManager.fontSizeText16, fontWeight: FontWeight.w700),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
