import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_paping.dart';
import 'package:test_framework_view/common/file_download.dart';
import 'package:test_framework_view/components/badge/badge_text.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/page/runs/test_run/test_scenario_run.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../common/custom_state.dart';
import '../../../common/dynamic_table.dart';
import '../../../components/dynamic_button.dart';
import '../../../components/style.dart';
import '../../../components/toast.dart';
import '../../../model/model.dart';
import '../../../type.dart';
import '../../editor/editor/editor_page.dart';

class TestRunDataDetailWidget extends StatefulWidget {
  final String id;
  final String prjProjectId;
  final String type;
  const TestRunDataDetailWidget({Key? key, required this.id, required this.prjProjectId, required this.type}) : super(key: key);

  @override
  State<TestRunDataDetailWidget> createState() => _TestRunDataDetailWidgetState();
}

class _TestRunDataDetailWidgetState extends CustomState<TestRunDataDetailWidget> {
  late Future future;
  var resultTestRun = {};
  var resultTestRunData = [];
  var resultTestRunDataBugTracker = [];
  var rowCountTestRunData = 0;
  var rowCountTestRunDataBugTracker = 0;
  var currentPageTestRunDataBugTracker = 1;
  var rowPerPageTestRunDataBugTracker = 10;
  List? parameterFieldList;
  List? outputFieldList;
  late String testRunId;
  late SecurityModel securityModel;
  List<Map<String, String>> listMapPriority = [];
  List<Map<String, dynamic>> listMapTestRunExpectedResult = [];
  List<Map<String, String>> listMapSeverity = [];

  @override
  void initState() {
    super.initState();
    securityModel = Provider.of<SecurityModel>(context, listen: false);
    testRunId = widget.id;
    future = search();
    listMapPriority = [
      {"value": "1", "name": multiLanguageString(name: "item_highest", defaultValue: "Highest", context: context)},
      {"value": "2", "name": multiLanguageString(name: "item_high", defaultValue: "High", context: context)},
      {"value": "3", "name": multiLanguageString(name: "item_medium", defaultValue: "Medium", context: context)},
      {"value": "4", "name": multiLanguageString(name: "item_low", defaultValue: "Low", context: context)},
      {"value": "5", "name": multiLanguageString(name: "item_lowest", defaultValue: "Lowest", context: context)},
    ];
    listMapTestRunExpectedResult = [
      {"name": multiLanguageString(name: "item_pass", defaultValue: "Pass", context: context), "value": 1},
      {"name": multiLanguageString(name: "item_fail", defaultValue: "Fail", context: context), "value": 0},
    ];
    listMapSeverity = [
      {"value": "1", "name": multiLanguageString(name: "item_critical", defaultValue: "Critical", context: context)},
      {"value": "2", "name": multiLanguageString(name: "item_major", defaultValue: "Major", context: context)},
      {"value": "3", "name": multiLanguageString(name: "item_average", defaultValue: "Average", context: context)},
      {"value": "4", "name": multiLanguageString(name: "item_minor", defaultValue: "Minor", context: context)},
      {"value": "5", "name": multiLanguageString(name: "item_trivial", defaultValue: "Trivial", context: context)},
    ];
  }

  search() async {
    await getTestRunData(testRunId);
    await getListTestRunData(resultTestRun["tfTestRunId"]);
    await getTestCase(widget.type == "test_case" ? resultTestRun["tfTestCase"]["parentId"] : resultTestRun["tfTestCaseId"]);
    await getListTestRunDataBugTracker(testRunId, currentPageTestRunDataBugTracker);
    return 0;
  }

  getTestCase(id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-case/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        parameterFieldList = response["body"]["result"]["content"]["parameterFieldList"] ?? [];
        outputFieldList = response["body"]["result"]["content"]["outputFieldList"] ?? [];
      });
    }
  }

  getTestRunData(String id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-run-data/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        resultTestRun = response["body"]["result"];
      });
      return 0;
    }
  }

  getListTestRunData(String parentId) async {
    var findRequest = {
      "prjProjectId": widget.prjProjectId,
      "tfTestRunId": parentId,
    };
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-run-data/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        rowCountTestRunData = response["body"]["rowCount"];
        resultTestRunData = response["body"]["resultList"];
      });
    }
    return 0;
  }

  getListTestRunDataBugTracker(String tfTestRunDataId, page) async {
    if ((page - 1) * rowPerPageTestRunDataBugTracker > rowCountTestRunDataBugTracker) {
      page = (1.0 * rowCountTestRunDataBugTracker / rowPerPageTestRunDataBugTracker).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {
      "prjProjectId": widget.prjProjectId,
      "tfTestRunDataId": tfTestRunDataId,
      "queryOffset": (page - 1) * rowPerPageTestRunDataBugTracker,
      "queryLimit": rowPerPageTestRunDataBugTracker
    };
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-run-data-bug-tracker/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPageTestRunDataBugTracker = page;
        rowCountTestRunDataBugTracker = response["body"]["rowCount"];
        resultTestRunDataBugTracker = response["body"]["resultList"];
      });
    }
    return 0;
  }

  downloadResult(id) async {
    File? downloadedFile = await httpDownloadFile(
        context, "/test-framework-api/user/test-framework-file/tf-test-run/$id/export-result", securityModel.authorization, 'Output.xlsx');
    if (downloadedFile != null && mounted) {
      var snackBar = SnackBar(
        content: MultiLanguageText(name: "file_downloaded_to", defaultValue: "File downloaded to \$0 .", variables: ["$downloadedFile"]),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  downloadCode(id) async {
    File? downloadedFile = await httpDownloadFile(
        context, "/test-framework-api/user/test-framework-file/tf-test-run/$id/source-code", securityModel.authorization, 'Output.zip');
    if (downloadedFile != null && mounted) {
      var snackBar = SnackBar(
        content: MultiLanguageText(name: "file_downloaded_to", defaultValue: "File downloaded to \$0 .", variables: ["$downloadedFile"]),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, LanguageModel>(
      builder: (context, navigationModel, languageModel, child) {
        return FutureBuilder(
          future: future,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(50, 20, 50, 30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        NavMenuTabV1(
                          currentUrl: "/runs/${widget.type}",
                          navigationList: context.testRunsMenuList,
                          margin: const EdgeInsets.only(bottom: 20),
                        ),
                        header(navigationModel),
                        const SizedBox(
                          height: 20,
                        ),
                        if (rowCountTestRunData > 1)
                          Container(
                            alignment: Alignment.center,
                            margin: const EdgeInsets.only(bottom: 20),
                            child: DynamicPagging(
                              rowCountTestRunData,
                              resultTestRunData.indexWhere((element) => element["id"] == testRunId) + 1,
                              pageChangeHandler: (page) {
                                testRunId = resultTestRunData[page - 1]["id"];
                                search();
                              },
                            ),
                          ),
                        testRunDataInformation(),
                        (resultTestRunDataBugTracker.isNotEmpty) ? testRunDataBugTrackerTable(navigationModel) : const SizedBox.shrink(),
                        DynamicTablePagging(
                          rowCountTestRunDataBugTracker,
                          currentPageTestRunDataBugTracker,
                          rowPerPageTestRunDataBugTracker,
                          pageChangeHandler: (page) {
                            setState(() {
                              future = getListTestRunDataBugTracker(testRunId, page);
                            });
                          },
                          rowPerPageChangeHandler: (rowPerPage) {
                            setState(() {
                              rowPerPageTestRunDataBugTracker = rowPerPage!;
                              future = getListTestRunDataBugTracker(testRunId, 1);
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  dialogViewImageTestRun(String finalImageFile) {
    showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          return ViewImageTestRunWidget(finalImageFile: finalImageFile);
        });
  }

  dialogDeleteDirectoryTestCase(String id) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: const EdgeInsets.all(0),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MultiLanguageText(
                name: "cancel",
                defaultValue: "Cancel",
                isLowerCase: false,
                style: Style(context).styleTitleDialog,
              ),
              TextButton(
                  onPressed: () => Navigator.pop(context, 'Cancel'),
                  child: const Icon(
                    Icons.close,
                    color: Colors.black,
                  ))
            ],
          ),
          content: Container(
            decoration: const BoxDecoration(
                border:
                    Border(top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)), bottom: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)))),
            margin: const EdgeInsets.only(top: 20, bottom: 10),
            width: 500,
            padding: const EdgeInsets.all(20),
            child: IntrinsicHeight(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text.rich(
                    TextSpan(
                        text: multiLanguageString(
                            name: "are_you_sure_you_want_to_cancel", defaultValue: "Are you sure you want to cancel ", context: context),
                        children: const <InlineSpan>[
                          TextSpan(
                            text: 'this test run',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(text: ' ?')
                        ]),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 20),
                    decoration: BoxDecoration(
                      color: const Color.fromRGBO(228, 115, 87, 1),
                      borderRadius: BorderRadius.circular(5),
                    ),
                    padding: const EdgeInsets.only(left: 5),
                    child: Container(
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.horizontal(
                          right: Radius.circular(5),
                        ),
                        color: Color.fromRGBO(255, 233, 217, 1),
                      ),
                      padding: const EdgeInsets.all(15),
                      child: const Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Icon(Icons.warning, color: Color.fromRGBO(243, 86, 40, 1)),
                          SizedBox(width: 10),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              MultiLanguageText(
                                name: "warning",
                                defaultValue: "Warning",
                                style: TextStyle(color: Color.fromARGB(255, 206, 20, 20), fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 10),
                              MultiLanguageText(
                                name: "cant_undo_action",
                                defaultValue: "You can't undo this action.",
                                style: TextStyle(color: Color.fromARGB(255, 187, 103, 82)),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          actionsAlignment: MainAxisAlignment.center,
          actions: [
            Container(
              margin: const EdgeInsets.only(right: 5),
              height: 40,
              width: 120,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color>(
                    const Color.fromRGBO(225, 46, 59, 1),
                  ),
                ),
                onPressed: () async {
                  Navigator.of(context).pop();
                  await httpPost("/test-framework-api/user/test-framework/tf-test-run/${resultTestRun["id"]}/stop", {}, context);
                  future = search();
                },
                child: const MultiLanguageText(
                  name: "continue",
                  defaultValue: "Continue",
                  isLowerCase: false,
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
            const ButtonCancel()
          ],
        );
      },
    );
  }

  Widget testRunDataBugTrackerTable(NavigationModel navigationModel) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MultiLanguageText(
          name: "data_bug_tracker",
          defaultValue: "Test run data bug tracker",
          style: Style(context).styleTitleDialog,
        ),
        const SizedBox(
          height: 5,
        ),
        SelectionArea(
          child: CustomDataTableWidget(
            columns: [
              Expanded(
                child: MultiLanguageText(
                  name: "type",
                  defaultValue: "Type",
                  isLowerCase: false,
                  style: Style(context).styleTextDataColumn,
                ),
              ),
              Expanded(
                child: Text(
                  "URL",
                  style: Style(context).styleTextDataColumn,
                ),
              ),
              Expanded(
                child: MultiLanguageText(
                  name: "description",
                  defaultValue: "Description",
                  isLowerCase: false,
                  style: Style(context).styleTextDataColumn,
                ),
              ),
              SizedBox(width: 100, child: Center(child: Text("", style: Style(context).styleTextDataColumn))),
            ],
            rows: [
              for (var testRunDataBugTracker in resultTestRunDataBugTracker)
                CustomDataRow(
                  cells: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Text(
                          testRunDataBugTracker["bugTrackerType"] ?? "",
                          style: Style(context).styleTextDataCell,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: testRunDataBugTracker["bugTrackerUrl"] != null
                            ? InkWell(
                                onTap: () async {
                                  await launchUrl(
                                    Uri.parse(testRunDataBugTracker["bugTrackerUrl"]),
                                    webOnlyWindowName: kIsWeb ? '_blank' : '_self',
                                  );
                                },
                                child: Text(testRunDataBugTracker["bugTrackerUrl"]),
                              )
                            : Text(
                                "",
                                style: Style(context).styleTextDataCell,
                              ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Text(
                          testRunDataBugTracker["description"] ?? "",
                          style: Style(context).styleTextDataCell,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 100,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          if (testRunDataBugTracker["status"] == 3)
                            IconButton(
                              onPressed: () async {
                                var response = await httpPost(
                                    "/test-framework-api/user/test-framework/tf-test-run-data-bug-tracker/${testRunDataBugTracker["id"]}/resend",
                                    testRunDataBugTracker,
                                    context);
                                if (response.containsKey("body") && response["body"] is String == false) {
                                  if (response["body"].containsKey("errorMessage") && mounted) {
                                    showToast(
                                        context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
                                  } else if (mounted) {
                                    showToast(
                                        msg: multiLanguageString(name: "resend_successful", defaultValue: "Resend successful", context: context),
                                        color: Colors.greenAccent,
                                        icon: const Icon(Icons.error),
                                        context: context);
                                  }
                                }
                              },
                              icon: const Icon(Icons.reply, color: Color.fromRGBO(49, 49, 49, 1)),
                              splashRadius: 20,
                              tooltip: multiLanguageString(name: "resend", defaultValue: "Resend", context: context),
                            ),
                        ],
                      ),
                    ),
                  ],
                ),
            ],
          ),
        ),
        const SizedBox(
          height: 20,
        )
      ],
    );
  }

  dialogConfirmReportBug(testRunData) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: const EdgeInsets.all(0),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MultiLanguageText(
                name: "confirm",
                defaultValue: "Confirm",
                isLowerCase: false,
                style: Style(context).styleTitleDialog,
              ),
              TextButton(
                  onPressed: () => Navigator.pop(context, 'Cancel'),
                  child: const Icon(
                    Icons.close,
                    color: Colors.black,
                  ))
            ],
          ),
          content: Container(
            decoration: const BoxDecoration(
                border:
                    Border(top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)), bottom: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)))),
            margin: const EdgeInsets.only(top: 20, bottom: 10),
            width: 500,
            padding: const EdgeInsets.all(20),
            child: IntrinsicHeight(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text.rich(
                    TextSpan(
                        text: multiLanguageString(
                            name: "are_you_sure_you_want_to_report_debug", defaultValue: "Are you sure you want to report bug ", context: context),
                        children: <InlineSpan>[
                          TextSpan(
                            text: '"Data ${testRunData["dataIndex"]}"',
                            style: const TextStyle(fontWeight: FontWeight.bold),
                          ),
                          const TextSpan(text: ' ?')
                        ]),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 20),
                    decoration: BoxDecoration(
                      color: const Color.fromRGBO(228, 115, 87, 1),
                      borderRadius: BorderRadius.circular(5),
                    ),
                    padding: const EdgeInsets.only(left: 5),
                    child: Container(
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.horizontal(
                          right: Radius.circular(5),
                        ),
                        color: Color.fromRGBO(255, 233, 217, 1),
                      ),
                      padding: const EdgeInsets.all(15),
                      child: const Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Icon(Icons.warning, color: Color.fromRGBO(243, 86, 40, 1)),
                          SizedBox(width: 10),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              MultiLanguageText(
                                name: "warning",
                                defaultValue: "Warning",
                                style: TextStyle(color: Color.fromARGB(255, 206, 20, 20), fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 10),
                              MultiLanguageText(
                                name: "cant_undo_action",
                                defaultValue: "You can't undo this action.",
                                style: TextStyle(color: Color.fromARGB(255, 187, 103, 82)),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          actionsAlignment: MainAxisAlignment.center,
          actions: [
            Container(
              margin: const EdgeInsets.only(right: 5),
              height: 40,
              width: 120,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color>(
                    const Color.fromRGBO(225, 46, 59, 1),
                  ),
                ),
                onPressed: () async {
                  Navigator.of(context).pop();
                  var response =
                      await httpPost("/test-framework-api/user/test-framework/tf-test-run-data/${testRunData["id"]}/report-bug", {}, context);
                  if (response.containsKey("body") && response["body"] is String == false && mounted) {
                    if (response["body"].containsKey("errorMessage")) {
                      showToast(
                        context: this.context,
                        msg: response["body"]["errorMessage"],
                        color: Colors.red,
                        icon: const Icon(Icons.error),
                      );
                    } else {
                      showToast(
                          context: this.context,
                          msg: response["body"]["result"] == true
                              ? multiLanguageString(name: "report_bug_successful", defaultValue: "Report bug successful", context: this.context)
                              : multiLanguageString(name: "report_bug_failure", defaultValue: "Report bug failure", context: this.context),
                          color: response["body"]["result"] == true ? Colors.greenAccent : Colors.red,
                          icon: Icon(response["body"]["result"] == true ? Icons.done : Icons.error));
                    }
                  }
                },
                child: const MultiLanguageText(
                  name: "ok",
                  defaultValue: "Ok",
                  isLowerCase: false,
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
            const ButtonCancel()
          ],
        );
      },
    );
  }

  Widget testRunDataInformation() {
    return SelectionArea(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 2,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Column(
                            children: [
                              Container(
                                alignment: Alignment.centerLeft,
                                padding: const EdgeInsets.all(10),
                                margin: const EdgeInsets.only(bottom: 10),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: const Color.fromRGBO(212, 212, 212, 1),
                                  ),
                                  color: Colors.grey[100],
                                ),
                                child: MultiLanguageText(
                                  name: "test_run_result",
                                  defaultValue: "Test run result",
                                  isLowerCase: false,
                                  style: Style(context).styleTextDataColumn,
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  rowItemTestRunData(
                                    "expected_result",
                                    "Expected result",
                                    widget: Row(
                                      children: [
                                        IntrinsicWidth(
                                          child: BadgeText4StatusTestRunDataWidget(
                                            status: resultTestRun["expectedStatus"],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  if (resultTestRun["status"] == 0 || resultTestRun["status"] == 1)
                                    rowItemTestRunData(
                                      "actual_result",
                                      "Actual result",
                                      widget: Row(
                                        children: [
                                          IntrinsicWidth(
                                            child: BadgeText4StatusTestRunDataWidget(
                                              status: resultTestRun["actualStatus"],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  rowItemTestRunData(
                                    "final_status",
                                    "Final status",
                                    widget: Row(
                                      children: [
                                        IntrinsicWidth(
                                          child: BadgeText4StatusTestRunDataWidget(
                                            status: resultTestRun["status"],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  if (resultTestRun["status"] == 0)
                                    rowItemTestRunData(
                                      "report_bug",
                                      "Report bug",
                                      widget: Row(
                                        children: [
                                          IntrinsicWidth(
                                            child: BadgeTextBugTrackerStatusWidget(
                                              status: resultTestRun["bugTrackerStatus"],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Expanded(
                          child: Column(
                            children: [
                              Container(
                                alignment: Alignment.centerLeft,
                                padding: const EdgeInsets.all(10),
                                margin: const EdgeInsets.only(bottom: 10),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: const Color.fromRGBO(212, 212, 212, 1),
                                  ),
                                  color: Colors.grey[100],
                                ),
                                child: MultiLanguageText(
                                  name: "test_run_information",
                                  defaultValue: "Test run information",
                                  isLowerCase: false,
                                  style: Style(context).styleTextDataColumn,
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  if (resultTestRun["sysNode"] != null) rowItemTestRunData("agent", "Agent", data: resultTestRun["sysNode"]["name"]),
                                  rowItemTestRunData(
                                    "start_run",
                                    "Start run",
                                    data: (resultTestRun["startDate"] != null)
                                        ? "${DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultTestRun["startDate"]).toLocal())} ${DateFormat('HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultTestRun["startDate"]).toLocal())}"
                                        : "",
                                  ),
                                  rowItemTestRunData(
                                    "end_run",
                                    "End run",
                                    data: (resultTestRun["endDate"] != null)
                                        ? "${DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultTestRun["endDate"]).toLocal())} ${DateFormat('HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultTestRun["endDate"]).toLocal())}"
                                        : "",
                                  ),
                                  rowItemTestRunData("total_run_time", "Total run time",
                                      data: (resultTestRun["endDate"] != null && resultTestRun["startDate"] != null)
                                          ? "${(Duration(milliseconds: DateTime.parse(resultTestRun["endDate"]).difference(DateTime.parse(resultTestRun["startDate"])).inMilliseconds).inMilliseconds / 1000).toStringAsFixed(2).replaceFirst('.', ',').padLeft(5, '0')} (S)"
                                          : "0 (S)"),
                                  // (resultTestRun["endDate"] != null && resultTestRun["startDate"] != null && resultTestRun["recordTime"] != null)
                                  //     ? "${(Duration(milliseconds: DateTime.parse(resultTestRun["endDate"]).difference(DateTime.parse(resultTestRun["startDate"])).inMilliseconds - int.parse(resultTestRun["recordTime"].toString())).inMilliseconds / 1000).toStringAsFixed(2).replaceFirst('.', ',').padLeft(5, '0')} (S)"
                                  //     : "0 (S)"),
                                  rowItemTestRunData("version", "Version", data: resultTestRun["tfTestVersion"]["title"]),
                                  rowItemTestRunData("environment", "Environment", data: resultTestRun["tfTestEnvironment"]["title"]),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    if (resultTestRun["description"] != null)
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(
                            height: 10,
                          ),
                          MultiLanguageText(
                            name: "actual_result_description",
                            defaultValue: "Actual result description",
                            style: TextStyle(
                              fontSize: context.fontSizeManager.fontSizeText16,
                              color: const Color.fromRGBO(49, 49, 49, 1),
                              fontWeight: FontWeight.w600,
                              letterSpacing: 1.5,
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Text(
                            resultTestRun["description"],
                            style: TextStyle(
                              fontSize: context.fontSizeManager.fontSizeText16,
                              color: const Color.fromRGBO(49, 49, 49, 1),
                              fontWeight: FontWeight.w500,
                              letterSpacing: 1.5,
                            ),
                          ),
                        ],
                      ),
                    if (outputFieldList!.isNotEmpty)
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(
                            height: 10,
                          ),
                          MultiLanguageText(
                            name: "test_run_output",
                            defaultValue: "Test run output",
                            style: TextStyle(
                              fontSize: context.fontSizeManager.fontSizeText16,
                              color: const Color.fromRGBO(49, 49, 49, 1),
                              fontWeight: FontWeight.w600,
                              letterSpacing: 1.5,
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          for (var outputField in outputFieldList!)
                            Text(
                              "${outputField["name"]}: ${(resultTestRun["testRunOutput"] != null && resultTestRun["testRunOutput"].isNotEmpty && resultTestRun["testRunOutput"][outputField["name"]] != null) ? (resultTestRun["testRunOutput"][outputField["name"]] ?? "") : ""}",
                              style: TextStyle(
                                fontSize: context.fontSizeManager.fontSizeText16,
                                color: const Color.fromRGBO(49, 49, 49, 1),
                                fontWeight: FontWeight.w500,
                                letterSpacing: 1.5,
                              ),
                            ),
                        ],
                      ),
                  ],
                ),
              ),
              const SizedBox(
                width: 20,
              ),
              Expanded(
                child: Column(
                  children: [
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: const EdgeInsets.all(10),
                      margin: const EdgeInsets.only(bottom: 10),
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: const Color.fromRGBO(212, 212, 212, 1),
                        ),
                        color: Colors.grey[100],
                      ),
                      child: MultiLanguageText(
                        name: "test_run_data",
                        defaultValue: "Test run data",
                        isLowerCase: false,
                        style: Style(context).styleTextDataColumn,
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        rowItemTestRunData("test_case", "Test Case",
                            isExpanded: false,
                            data:
                                "${resultTestRun["tfTestCase"]["testCaseType"] == "test_case" ? resultTestRun["tfTestCase"]["name"] : resultTestRun["testRunData"]["testCaseName"]}"),
                        rowItemTestRunData("test_run_expected_result", "Test Run Expected Result",
                            isExpanded: false,
                            data: "${listMapTestRunExpectedResult.firstWhere((map) => map["value"] == resultTestRun["expectedStatus"])["name"]}"),
                        rowItemTestRunData("severity", "Severity",
                            isExpanded: false,
                            data: "${listMapSeverity.firstWhere((map) => map["value"] == resultTestRun["dataSeverity"].toString())["name"]}"),
                        rowItemTestRunData("priority", "Priority",
                            isExpanded: false,
                            data: "${listMapPriority.firstWhere((map) => map["value"] == resultTestRun["dataPriority"].toString())["name"]}"),
                        for (var parameterField in parameterFieldList ?? [])
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: TestRunDataItemWidget(
                              parameterField: parameterField,
                              resultTestRun: resultTestRun,
                            ),
                          ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Column rowItemTestRunData(String name, String defaultValue, {String? data, Widget? widget, bool isExpanded = true}) {
    return Column(
      children: [
        const SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            isExpanded
                ? Expanded(
                    flex: 4,
                    child: MultiLanguageText(
                      name: name,
                      defaultValue: defaultValue,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: const Color.fromRGBO(0, 0, 0, 0.8),
                          fontSize: context.fontSizeManager.fontSizeText14,
                          letterSpacing: 2),
                    ),
                  )
                : MultiLanguageText(
                    name: name,
                    defaultValue: defaultValue,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: const Color.fromRGBO(0, 0, 0, 0.8),
                        fontSize: context.fontSizeManager.fontSizeText14,
                        letterSpacing: 2),
                  ),
            const SizedBox(
              width: 10,
            ),
            Expanded(
              flex: 6,
              child: widget ??
                  Text(
                    data ?? "",
                    style: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText14,
                      color: const Color.fromRGBO(49, 49, 49, 1),
                      fontWeight: FontWeight.w500,
                      letterSpacing: 1.5,
                    ),
                  ),
            ),
          ],
        ),
      ],
    );
  }

  Widget header(NavigationModel navigationModel) {
    return Row(
      children: [
        GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: const Icon(Icons.arrow_back),
        ),
        const SizedBox(
          width: 5,
        ),
        Expanded(
          child: Text(
            "Data ${resultTestRun["dataIndex"]}",
            style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
          ),
        ),
        Row(
          children: [
            if (resultTestRun["status"] == 0 && resultTestRun["bugTrackerStatus"] != -1 && resultTestRun["tfTestCase"]["status"] == 1)
              IconButton(
                onPressed: () {
                  dialogConfirmReportBug(resultTestRun);
                },
                icon: const FaIcon(FontAwesomeIcons.spider),
                color: Colors.red,
                splashRadius: 20,
                tooltip: multiLanguageString(name: "report_bug", defaultValue: "Report bug", context: context),
              ),
            if (resultTestRun["status"] == 0 && resultTestRun["bugTrackerStatus"] == -1)
              IconButton(
                onPressed: () {},
                icon: const FaIcon(FontAwesomeIcons.bugSlash),
                color: Colors.red,
                splashRadius: 20,
                tooltip: multiLanguageString(name: "not_bug", defaultValue: "Not bug", context: context),
              ),
            if (resultTestRun["recordFile"] != null)
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconButton(
                    onPressed: () {
                      Navigator.of(context).push(
                        createRoute(
                          PlayVideoTestRunWidget(
                            recordFile: resultTestRun["recordFile"],
                          ),
                        ),
                      );
                    },
                    icon: const FaIcon(FontAwesomeIcons.solidCirclePlay),
                    color: resultTestRun["status"] == 1 ? Colors.green : Colors.red,
                    splashRadius: 20,
                    tooltip: multiLanguageString(name: "play_video_of_test", defaultValue: "Play video of test", context: context),
                  ),
                  IconButton(
                    onPressed: () async {
                      File? downloadedFile = await httpDownloadFile(context, "/record/${resultTestRun["recordFile"]}", null, 'Output.mp4');
                      if (downloadedFile != null && mounted) {
                        var snackBar = SnackBar(
                          content:
                              MultiLanguageText(name: "file_downloaded_to", defaultValue: "File downloaded to \$0 .", variables: ["$downloadedFile"]),
                        );
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      }
                    },
                    icon: const FaIcon(FontAwesomeIcons.film),
                    color: resultTestRun["status"] == 1 ? Colors.green : Colors.red,
                    splashRadius: 20,
                    tooltip: multiLanguageString(name: "download_video_of_test", defaultValue: "Download video of test", context: context),
                  ),
                ],
              ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                if (resultTestRun["errorActionPath"] != null && resultTestRun["errorActionPath"].isNotEmpty)
                  IconButton(
                    onPressed: () async {
                      Navigator.of(context).push(
                        createRoute(
                          EditorPageRootWidget(
                            testCaseType: widget.type,
                            testCaseId: (widget.type == "test_case") ? resultTestRun["tfTestCase"]["parentId"] : resultTestRun["tfTestCaseId"],
                            errorTestRunResultId: resultTestRun["id"],
                            callback: () {},
                          ),
                        ),
                      );
                    },
                    icon: const FaIcon(FontAwesomeIcons.bug),
                    color: Colors.grey,
                    splashRadius: 20,
                    tooltip: multiLanguageString(name: "view_debug", defaultValue: "View debug", context: context),
                  ),
                if (resultTestRun["finalImageFile"] != null && resultTestRun["finalImageFile"].isNotEmpty)
                  IconButton(
                    onPressed: () async {
                      dialogViewImageTestRun(resultTestRun["finalImageFile"]);
                    },
                    icon: const FaIcon(FontAwesomeIcons.solidImage),
                    color: Colors.grey,
                    splashRadius: 20,
                    tooltip: multiLanguageString(name: "view_image_error_of_test", defaultValue: "View image error of test", context: context),
                  ),
              ],
            )
          ],
        ),
        IconButton(
          onPressed: () {
            future = search();
          },
          icon: const Icon(Icons.autorenew, color: Color.fromRGBO(49, 49, 49, 1)),
          splashRadius: 20,
          tooltip: multiLanguageString(name: "refresh", defaultValue: "Refresh", context: context),
        ),
      ],
    );
  }
}

class TestRunDataItemWidget extends StatefulWidget {
  const TestRunDataItemWidget({
    Key? key,
    required this.parameterField,
    required this.resultTestRun,
  }) : super(key: key);

  final dynamic parameterField;
  final Map resultTestRun;

  @override
  State<TestRunDataItemWidget> createState() => _TestRunDataItemWidgetState();
}

class _TestRunDataItemWidgetState extends State<TestRunDataItemWidget> {
  @override
  void initState() {
    super.initState();
    if (widget.parameterField["dataType"] == "Password") {
      widget.parameterField["isInvisible"] = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          "${widget.parameterField["name"]}",
          style: Style(context).styleTitleDialog,
        ),
        const SizedBox(
          width: 10,
        ),
        if (widget.resultTestRun["testRunData"] != null && widget.resultTestRun["testRunData"].isNotEmpty)
          Expanded(
            child: Text(
              !(widget.parameterField["isInvisible"] ?? false) ? widget.resultTestRun["testRunData"][widget.parameterField["name"]] ?? "" : "***",
              softWrap: true,
              style: TextStyle(
                fontSize: context.fontSizeManager.fontSizeText16,
                color: const Color.fromRGBO(49, 49, 49, 1),
                fontWeight: FontWeight.w500,
                letterSpacing: 1.5,
              ),
            ),
          ),
        if (widget.parameterField["dataType"] == "Password")
          GestureDetector(
            onTap: () => setState(() {
              widget.parameterField["isInvisible"] = !(widget.parameterField["isInvisible"] ?? false);
            }),
            child: Icon((widget.parameterField["isInvisible"] ?? false) ? Icons.visibility_off : Icons.remove_red_eye),
          ),
      ],
    );
  }
}
