import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/custom_state.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/components/badge/badge_text.dart';
import 'package:test_framework_view/components/dialog/dialog_delete.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';

import '../../components/menu_tab.dart';
import '../list_test/tests/header/header_list_test_widget.dart';

class DeleteTestRun extends StatefulWidget {
  final String? testCaseId;
  final String type;
  const DeleteTestRun({Key? key, this.testCaseId, required this.type}) : super(key: key);
  @override
  State<DeleteTestRun> createState() => _TestRunState();
}

class _TestRunState extends CustomState<DeleteTestRun> {
  late Future futureResultTestRun;
  var rowCount = 0;
  var currentPage = 1;
  var resultListTestSuiteRun = [];
  var rowPerPage = 5;
  var searchRequest = {};
  var prjProjectId = "";

  @override
  void initState() {
    super.initState();
    prjProjectId = Provider.of<NavigationModel>(context, listen: false).prjProjectId ?? "";
    futureResultTestRun = getListTfTestCaseRun(currentPage);
  }

  getListTfTestCaseRun(page) async {
    if ((page - 1) * rowPerPage > rowCount) {
      page = (1.0 * rowCount / rowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = searchRequest;
    if (searchRequest["tfTestCaseTitleLike"] != null && searchRequest["tfTestCaseTitleLike"].isNotEmpty) {
      findRequest["tfTestCaseTitleLike"] = "%${searchRequest["tfTestCaseTitleLike"]}%";
    }
    if (searchRequest["tfTestCaseNameLike"] != null && searchRequest["tfTestCaseNameLike"].isNotEmpty) {
      findRequest["tfTestCaseNameLike"] = "%${searchRequest["tfTestCaseNameLike"]}%";
    }
    if (widget.testCaseId != null) {
      findRequest["tfTestCaseId"] = widget.testCaseId;
    }
    findRequest["queryOffset"] = (page - 1) * rowPerPage;
    findRequest["queryLimit"] = rowPerPage;
    findRequest["prjProjectId"] = prjProjectId;
    findRequest["tfTestCaseIdNotNull"] = true;
    findRequest["tfTestCaseType"] = widget.type;
    findRequest["markedForDelete"] = 0;
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-run/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPage = page;
        rowCount = response["body"]["rowCount"];
        resultListTestSuiteRun = response["body"]["resultList"];
      });
      return 0;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return FutureBuilder(
          future: futureResultTestRun,
          builder: (context, dataTestRun) {
            if (dataTestRun.hasData) {
              return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
                return Material(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: ListView(
                          children: [
                            NavMenuTabV1(
                              currentUrl: "/delete-data/${widget.type}",
                              margin: const EdgeInsets.fromLTRB(50, 20, 50, 0),
                              navigationList: context.deleteDataMenuList,
                            ),
                            Container(
                              margin: const EdgeInsets.fromLTRB(50, 20, 50, 30),
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Expanded(
                                        child: HeaderListTestWidget(
                                            type: widget.type == "test_case"
                                                ? "Delete Test Case Runs"
                                                : widget.type == "test_scenario"
                                                    ? "Delete Test Scenario Runs"
                                                    : widget.type == "test_suite"
                                                        ? "Delete Test Suite Runs"
                                                        : "Delete Test Manual Runs",
                                            title: widget.type == "test_case"
                                                ? multiLanguageString(
                                                    name: "delete_test_case_runs", defaultValue: "Delete Test Case Runs", context: context)
                                                : widget.type == "test_scenario"
                                                    ? multiLanguageString(
                                                        name: "delete_test_scenario_runs",
                                                        defaultValue: "Delete Test Scenario Runs",
                                                        context: context)
                                                    : widget.type == "test_suite"
                                                        ? multiLanguageString(
                                                            name: "delete_test_suite_runs", defaultValue: "Delete Test Suite Runs", context: context)
                                                        : multiLanguageString(
                                                            name: "delete_test_manual_runs",
                                                            defaultValue: "Delete Test Manual Runs",
                                                            context: context),
                                            countList: rowCount,
                                            callbackFilter: (result) {
                                              searchRequest = result;
                                              getListTfTestCaseRun(currentPage);
                                            },
                                            callbackDelete: (result) {
                                              showDialog<String>(
                                                context: context,
                                                builder: (BuildContext context) {
                                                  return DialogDelete(
                                                    selectedItem: const {},
                                                    callback: () async {
                                                      searchRequest = result;
                                                      Map<dynamic, dynamic> bodyRequest = {
                                                        "queryOffset": (currentPage - 1) * rowPerPage,
                                                        "queryLimit": rowPerPage,
                                                        "prjProjectId": prjProjectId,
                                                        "tfTestCaseIdNotNull": true,
                                                        "tfTestCaseType": widget.type,
                                                        "markedForDelete": 0
                                                      };
                                                      bodyRequest.addAll(searchRequest);
                                                      if (searchRequest["tfTestCaseTitleLike"] != null &&
                                                          searchRequest["tfTestCaseTitleLike"].isNotEmpty) {
                                                        bodyRequest["tfTestCaseTitleLike"] = "%${searchRequest["tfTestCaseTitleLike"]}%";
                                                      }
                                                      if (searchRequest["tfTestCaseNameLike"] != null &&
                                                          searchRequest["tfTestCaseNameLike"].isNotEmpty) {
                                                        bodyRequest["tfTestCaseNameLike"] = "%${searchRequest["tfTestCaseNameLike"]}%";
                                                      }
                                                      var response = await httpPost(
                                                          "/test-framework-api/user/test-framework/tf-test-run/delete", bodyRequest, context);
                                                      if (response.containsKey("body") && response["body"] is String == false && mounted) {
                                                        if (response["body"].containsKey("errorMessage")) {
                                                          showToast(
                                                              context: this.context,
                                                              msg: response["body"]["errorMessage"],
                                                              color: Colors.red,
                                                              icon: const Icon(Icons.error));
                                                        } else {
                                                          futureResultTestRun = getListTfTestCaseRun(currentPage);
                                                          showToast(
                                                              context: this.context,
                                                              msg: multiLanguageString(
                                                                  name: "delete_successful",
                                                                  defaultValue: "Delete successful",
                                                                  context: this.context),
                                                              color: Colors.greenAccent,
                                                              icon: const Icon(Icons.done));
                                                        }
                                                      }
                                                    },
                                                    type: "Delete Test Run",
                                                  );
                                                },
                                              );
                                            }),
                                      ),
                                    ],
                                  ),
                                  if (resultListTestSuiteRun.isNotEmpty)
                                    SelectionArea(
                                      child: Container(
                                          margin: const EdgeInsets.only(top: 40),
                                          child: (constraints.maxWidth < 1200)
                                              ? SingleChildScrollView(
                                                  scrollDirection: Axis.horizontal,
                                                  child: SizedBox(
                                                    width: 1200,
                                                    child: tableWidget(),
                                                  ),
                                                )
                                              : tableWidget()),
                                    ),
                                  SelectionArea(
                                    child: DynamicTablePagging(
                                      rowCount,
                                      currentPage,
                                      rowPerPage,
                                      pageChangeHandler: (page) {
                                        futureResultTestRun = getListTfTestCaseRun(page);
                                      },
                                      rowPerPageChangeHandler: (value) {
                                        setState(() {
                                          rowPerPage = value;
                                          futureResultTestRun = getListTfTestCaseRun(1);
                                        });
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                );
              });
            } else if (dataTestRun.hasError) {
              return Text('${dataTestRun.error}');
            }
            return const Center(child: CircularProgressIndicator());
          });
    });
  }

  Column tableWidget() {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.all(15),
          decoration: const BoxDecoration(
            border: Border(
              top: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
              bottom: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
              left: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
              right: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
            ),
          ),
          child: Row(
            children: [
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: MultiLanguageText(name: "test_code", defaultValue: "Test code", isLowerCase: false, style: Style(context).styleTextDataColumn),
              )),
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: MultiLanguageText(name: "test_name", defaultValue: "Test name", isLowerCase: false, style: Style(context).styleTextDataColumn),
              )),
              SizedBox(
                  width: 150,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Center(
                        child: MultiLanguageText(
                            name: "start_date", defaultValue: "Start date", isLowerCase: false, style: Style(context).styleTextDataColumn)),
                  )),
              SizedBox(
                  width: 150,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Center(
                        child: MultiLanguageText(
                            name: "run_time", defaultValue: "Run time", isLowerCase: false, style: Style(context).styleTextDataColumn)),
                  )),
              SizedBox(
                  width: 150,
                  child: Center(
                      child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        MultiLanguageText(
                          name: "status",
                          defaultValue: "Status",
                          isLowerCase: false,
                          style: Style(context).styleTextDataColumn,
                        ),
                        Tooltip(
                          message: multiLanguageString(
                              name: "pass_fail_waiting_error_processing",
                              defaultValue: "Green = Pass\nRed = Fail\nOrange = Waiting\nGrey = Error\nBlue = Processing ",
                              context: context),
                          child: const Icon(
                            Icons.info,
                            color: Colors.grey,
                            size: 16,
                          ),
                        ),
                      ],
                    ),
                  ))),
              SizedBox(width: 150, child: Center(child: Text("", style: Style(context).styleTextDataColumn))),
            ],
          ),
        ),
        for (var testSuiteRun in resultListTestSuiteRun)
          RowTableDeleteTestRunWidget(
            testSuiteRun,
            callback: () async {
              var response = await httpDelete("/test-framework-api/user/test-framework/tf-test-run/${testSuiteRun["id"]}", context);
              if (response.containsKey("body") && response["body"] is String == false && mounted) {
                if (response["body"].containsKey("errorMessage")) {
                  showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
                } else {
                  futureResultTestRun = getListTfTestCaseRun(currentPage);
                  showToast(
                      context: context,
                      msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
                      color: Colors.greenAccent,
                      icon: const Icon(Icons.done));
                }
              }
            },
          ),
      ],
    );
  }
}

class RowTableDeleteTestRunWidget extends StatefulWidget {
  final dynamic rowData;
  final VoidCallback? callback;
  const RowTableDeleteTestRunWidget(this.rowData, {Key? key, this.callback}) : super(key: key);

  @override
  State<RowTableDeleteTestRunWidget> createState() => _RowTableTestRunWidgetState();
}

class _RowTableTestRunWidgetState extends CustomState<RowTableDeleteTestRunWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(15),
      decoration: const BoxDecoration(
        border: Border(
          top: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
          bottom: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
          left: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
          right: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
        ),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5),
              child: Wrap(
                children: [
                  Text(
                    widget.rowData["tfTestCase"]["name"] ?? "",
                    softWrap: true,
                    style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5),
              child: Wrap(
                children: [
                  Text(
                    widget.rowData["tfTestCase"]["title"] ?? "",
                    softWrap: true,
                    style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            width: 150,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    widget.rowData["startDate"] != null
                        ? DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(widget.rowData["startDate"]).toLocal())
                        : "",
                    softWrap: true,
                    style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                  ),
                  Text(
                    widget.rowData["startDate"] != null
                        ? DateFormat('HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(widget.rowData["startDate"]).toLocal())
                        : "",
                    softWrap: true,
                    style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            width: 150,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5),
              child: Center(
                child: Text(
                  (widget.rowData["totalTime"] != null && widget.rowData["recordTime"] != null)
                      ? "${(Duration(milliseconds: widget.rowData["totalTime"] - widget.rowData["recordTime"]).inMilliseconds / 1000).toStringAsFixed(2).replaceFirst('.', ',').padLeft(5, '0')} (S)"
                      : "0 (S)",
                  softWrap: true,
                  style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                ),
              ),
            ),
          ),
          SizedBox(
            width: 150,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                IntrinsicWidth(
                  child: BadgeText6StatusWidget(
                    status: widget.rowData["status"],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            width: 150,
            child: Center(
              child: IconButton(
                onPressed: () {
                  dialogDeleteDirectoryTestCase(widget.rowData["id"]);
                },
                icon: const Icon(Icons.delete, color: Color.fromRGBO(49, 49, 49, 1)),
                splashRadius: 20,
                tooltip: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
              ),
            ),
          ),
        ],
      ),
    );
  }

  dialogDeleteDirectoryTestCase(String id) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogDelete(selectedItem: widget.rowData, type: "Delete Test Run", callback: () => widget.callback!());
      },
    );
  }
}
