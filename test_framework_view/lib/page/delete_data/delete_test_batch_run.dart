import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/custom_state.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/components/badge/badge_text.dart';
import 'package:test_framework_view/components/dialog/dialog_delete.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';

import '../../components/menu_tab.dart';
import '../list_test/tests/header/header_list_test_widget.dart';

class DeleteTestBatchRun extends StatefulWidget {
  const DeleteTestBatchRun({
    Key? key,
  }) : super(key: key);
  @override
  State<DeleteTestBatchRun> createState() => _DeleteTestBatchRunState();
}

class _DeleteTestBatchRunState extends CustomState<DeleteTestBatchRun> {
  late Future futureResultTestBatchRun;
  var rowCount = 0;
  var currentPage = 1;
  var resultListTestBatchRun = [];
  var rowPerPage = 5;
  var searchRequest = {};
  var prjProjectId = "";

  @override
  void initState() {
    super.initState();
    prjProjectId = Provider.of<NavigationModel>(context, listen: false).prjProjectId ?? "";
    futureResultTestBatchRun = getListTfTestBatchRun(currentPage);
  }

  getListTfTestBatchRun(page) async {
    if ((page - 1) * rowPerPage > rowCount) {
      page = (1.0 * rowCount / rowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = searchRequest;
    if (searchRequest["tfTestBatchTitleLike"] != null && searchRequest["tfTestBatchTitleLike"].isNotEmpty) {
      findRequest["tfTestBatchTitleLike"] = "%${searchRequest["tfTestBatchTitleLike"]}%";
    }
    if (searchRequest["tfTestBatchNameLike"] != null && searchRequest["tfTestBatchNameLike"].isNotEmpty) {
      findRequest["tfTestBatchNameLike"] = "%${searchRequest["tfTestBatchNameLike"]}%";
    }
    findRequest["queryOffset"] = (page - 1) * rowPerPage;
    findRequest["queryLimit"] = rowPerPage;
    findRequest["prjProjectId"] = prjProjectId;
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-batch-run/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPage = page;
        rowCount = response["body"]["rowCount"];
        resultListTestBatchRun = response["body"]["resultList"];
      });
      return 0;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return FutureBuilder(
          future: futureResultTestBatchRun,
          builder: (context, dataTestBatchRun) {
            if (dataTestBatchRun.hasData) {
              return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
                return Material(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: ListView(
                          children: [
                            NavMenuTabV1(
                              currentUrl: "/delete-data/test_batch",
                              margin: const EdgeInsets.fromLTRB(50, 20, 50, 0),
                              navigationList: context.deleteDataMenuList,
                            ),
                            Container(
                              margin: const EdgeInsets.fromLTRB(50, 20, 50, 30),
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Expanded(
                                        child: HeaderListTestWidget(
                                            type: "Delete Test Batch Runs",
                                            title: multiLanguageString(
                                                name: "delete_test_batch_runs", defaultValue: "Delete Test Batch Runs", context: context),
                                            countList: rowCount,
                                            callbackFilter: (result) {
                                              searchRequest = result;
                                              getListTfTestBatchRun(currentPage);
                                            },
                                            callbackDelete: (result) {
                                              showDialog<String>(
                                                context: context,
                                                builder: (BuildContext context) {
                                                  return DialogDelete(
                                                    selectedItem: const {},
                                                    callback: () async {
                                                      searchRequest = result;
                                                      Map<dynamic, dynamic> bodyRequest = {
                                                        "queryOffset": (currentPage - 1) * rowPerPage,
                                                        "queryLimit": rowPerPage,
                                                        "prjProjectId": prjProjectId,
                                                        // "tfTestBatchIdNotNull": true,
                                                        // "markedForDelete": 0
                                                      };
                                                      bodyRequest.addAll(searchRequest);
                                                      if (searchRequest["tfTestBatchTitleLike"] != null &&
                                                          searchRequest["tfTestBatchTitleLike"].isNotEmpty) {
                                                        bodyRequest["tfTestBatchTitleLike"] = "%${searchRequest["tfTestBatchTitleLike"]}%";
                                                      }
                                                      if (searchRequest["tfTestBatchNameLike"] != null &&
                                                          searchRequest["tfTestBatchNameLike"].isNotEmpty) {
                                                        bodyRequest["tfTestBatchNameLike"] = "%${searchRequest["tfTestBatchNameLike"]}%";
                                                      }
                                                      var response = await httpPost(
                                                          "/test-framework-api/user/test-framework/tf-test-batch-run/delete", bodyRequest, context);
                                                      if (response.containsKey("body") && response["body"] is String == false && mounted) {
                                                        if (response["body"].containsKey("errorMessage")) {
                                                          showToast(
                                                              context: this.context,
                                                              msg: response["body"]["errorMessage"],
                                                              color: Colors.red,
                                                              icon: const Icon(Icons.error));
                                                        } else {
                                                          futureResultTestBatchRun = getListTfTestBatchRun(currentPage);
                                                          showToast(
                                                              context: this.context,
                                                              msg: multiLanguageString(
                                                                  name: "delete_successful",
                                                                  defaultValue: "Delete successful",
                                                                  context: this.context),
                                                              color: Colors.greenAccent,
                                                              icon: const Icon(Icons.done));
                                                        }
                                                      }
                                                    },
                                                    type: "Delete Test Batch Run",
                                                  );
                                                },
                                              );
                                            }),
                                      ),
                                    ],
                                  ),
                                  if (resultListTestBatchRun.isNotEmpty)
                                    SelectionArea(
                                      child: Container(
                                          margin: const EdgeInsets.only(top: 40),
                                          child: (constraints.maxWidth < 1200)
                                              ? SingleChildScrollView(
                                                  scrollDirection: Axis.horizontal,
                                                  child: SizedBox(
                                                    width: 1200,
                                                    child: tableWidget(),
                                                  ),
                                                )
                                              : tableWidget()),
                                    ),
                                  SelectionArea(
                                    child: DynamicTablePagging(
                                      rowCount,
                                      currentPage,
                                      rowPerPage,
                                      pageChangeHandler: (page) {
                                        futureResultTestBatchRun = getListTfTestBatchRun(page);
                                      },
                                      rowPerPageChangeHandler: (value) {
                                        setState(() {
                                          rowPerPage = value;
                                          futureResultTestBatchRun = getListTfTestBatchRun(1);
                                        });
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                );
              });
            } else if (dataTestBatchRun.hasError) {
              return Text('${dataTestBatchRun.error}');
            }
            return const Center(child: CircularProgressIndicator());
          });
    });
  }

  Column tableWidget() {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.all(15),
          decoration: const BoxDecoration(
            border: Border(
              top: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
              bottom: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
              left: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
              right: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
            ),
          ),
          child: Row(
            children: [
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: MultiLanguageText(name: "test_code", defaultValue: "Test code", isLowerCase: false, style: Style(context).styleTextDataColumn),
              )),
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: MultiLanguageText(name: "test_name", defaultValue: "Test name", isLowerCase: false, style: Style(context).styleTextDataColumn),
              )),
              SizedBox(
                  width: 150,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Center(
                        child: MultiLanguageText(
                            name: "start_date", defaultValue: "Start date", isLowerCase: false, style: Style(context).styleTextDataColumn)),
                  )),
              SizedBox(
                  width: 150,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Center(
                        child: MultiLanguageText(
                            name: "run_time", defaultValue: "Run time", isLowerCase: false, style: Style(context).styleTextDataColumn)),
                  )),
              SizedBox(
                  width: 150,
                  child: Center(
                      child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        MultiLanguageText(
                          name: "status",
                          defaultValue: "Status",
                          isLowerCase: false,
                          style: Style(context).styleTextDataColumn,
                        ),
                        Tooltip(
                          message: multiLanguageString(
                              name: "pass_failed_waiting_error_processing",
                              defaultValue: "Green = Pass\nRed = Fail\nOrange = Waiting\nGrey = Error\nBlue = Processing ",
                              context: context),
                          child: const Icon(
                            Icons.info,
                            color: Colors.grey,
                            size: 16,
                          ),
                        ),
                      ],
                    ),
                  ))),
              SizedBox(width: 150, child: Center(child: Text("", style: Style(context).styleTextDataColumn))),
            ],
          ),
        ),
        for (var testBatchRun in resultListTestBatchRun)
          RowTableDeleteTestBatchRunWidget(
            testBatchRun,
            callback: () async {
              var response = await httpDelete("/test-framework-api/user/test-framework/tf-test-batch-run/${testBatchRun["id"]}", context);
              if (response.containsKey("body") && response["body"] is String == false && mounted) {
                if (response["body"].containsKey("errorMessage")) {
                  showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
                } else {
                  futureResultTestBatchRun = getListTfTestBatchRun(currentPage);
                  showToast(
                      context: context,
                      msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
                      color: Colors.greenAccent,
                      icon: const Icon(Icons.done));
                }
              }
            },
          ),
      ],
    );
  }
}

class RowTableDeleteTestBatchRunWidget extends StatefulWidget {
  final dynamic rowData;
  final VoidCallback? callback;
  const RowTableDeleteTestBatchRunWidget(this.rowData, {Key? key, this.callback}) : super(key: key);

  @override
  State<RowTableDeleteTestBatchRunWidget> createState() => _RowTableTestBatchRunWidgetState();
}

class _RowTableTestBatchRunWidgetState extends CustomState<RowTableDeleteTestBatchRunWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(15),
      decoration: const BoxDecoration(
        border: Border(
          top: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
          bottom: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
          left: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
          right: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
        ),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5),
              child: Wrap(
                children: [
                  Text(
                    widget.rowData["tfTestBatch"]["name"] ?? "",
                    softWrap: true,
                    style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5),
              child: Wrap(
                children: [
                  Text(
                    widget.rowData["tfTestBatch"]["title"] ?? "",
                    softWrap: true,
                    style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            width: 150,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    widget.rowData["startDate"] != null
                        ? DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(widget.rowData["startDate"]).toLocal())
                        : "",
                    softWrap: true,
                    style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                  ),
                  Text(
                    widget.rowData["startDate"] != null
                        ? DateFormat('HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(widget.rowData["startDate"]).toLocal())
                        : "",
                    softWrap: true,
                    style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            width: 150,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5),
              child: Center(
                child: Text(
                  (widget.rowData["totalTime"] != null && widget.rowData["recordTime"] != null)
                      ? "${(Duration(milliseconds: widget.rowData["totalTime"] - widget.rowData["recordTime"]).inMilliseconds / 1000).toStringAsFixed(2).replaceFirst('.', ',').padLeft(5, '0')} (S)"
                      : "0 (S)",
                  softWrap: true,
                  style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                ),
              ),
            ),
          ),
          SizedBox(
            width: 150,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                IntrinsicWidth(
                  child: BadgeTextTestBatchRunStatusWidget(
                    status: widget.rowData["status"],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            width: 150,
            child: Center(
              child: IconButton(
                onPressed: () {
                  dialogDeleteDirectoryTestBatch(widget.rowData["id"]);
                },
                icon: const Icon(Icons.delete, color: Color.fromRGBO(49, 49, 49, 1)),
                splashRadius: 20,
                tooltip: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
              ),
            ),
          ),
        ],
      ),
    );
  }

  dialogDeleteDirectoryTestBatch(String id) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogDelete(selectedItem: widget.rowData, type: "Delete Test Batch Run", callback: () => widget.callback!());
      },
    );
  }
}
