import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/body_list_root_item_widget.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/header_root_item_widget.dart';
import 'package:test_framework_view/page/editor/property_widget/property_page_widget.dart';
import 'package:test_framework_view/type.dart';

class ControlWidget extends StatefulWidget {
  final Item item;
  final int index;
  final VoidCallback onDelete;
  final Function onDuplicate;
  final Function onViewConfig;
  final String? tfTestCaseId;
  final bool hasCopy;
  final bool hasDelete;
  final bool hasDuplicate;
  final bool hasClick;
  final bool hasCheckBox;
  final VoidCallback? onChecked;
  final bool isDragAndDrop;
  final bool hasGlobalKey;
  const ControlWidget(
    this.item,
    this.index, {
    Key? key,
    required this.onDelete,
    this.tfTestCaseId,
    required this.hasCopy,
    required this.hasDelete,
    required this.hasClick,
    required this.hasCheckBox,
    this.onChecked,
    required this.isDragAndDrop,
    required this.hasGlobalKey,
    required this.onDuplicate,
    required this.hasDuplicate,
    required this.onViewConfig,
  }) : super(key: key);

  @override
  State<ControlWidget> createState() => _ControlWidgetState();
}

class _ControlWidgetState extends State<ControlWidget> {
  bool isExpandElseList = false;
  @override
  void initState() {
    super.initState();
    isExpandElseList = widget.item.elseBodyList.isNotEmpty;
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<ListElementPlayIdModel, EditorNotificationModel>(builder: (context, listElementPlayIdModel, editorNotificationModel, child) {
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        if (widget.hasCheckBox) {
          if (listElementPlayIdModel.isSelectedAll != null) {
            await handleSelectedItem(widget.item, listElementPlayIdModel.isSelectedAll!, context);
            if (listElementPlayIdModel.isSelectedAll == false) {
              listElementPlayIdModel.setIsSelectedAll(null);
            }
          }
        }
      });
      Widget childControlWidget = (widget.item.type == "condition")
          ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                HeaderRootItemWidget(
                  widget.item,
                  widget.index,
                  onDelete: () => widget.onDelete(),
                  onDuplicate: (item) => widget.onDuplicate(item),
                  hasCopy: widget.hasCopy,
                  hasDelete: widget.hasDelete,
                  hasDuplicate: widget.hasDuplicate,
                  hasClick: widget.hasClick,
                  isDragAndDrop: widget.isDragAndDrop,
                ),
                if (widget.hasGlobalKey)
                  Stack(
                    children: [
                      Positioned(
                        top: 6,
                        left: -8,
                        child: SizedBox(
                          width: 30,
                          child: Tooltip(
                            message: isExpandElseList
                                ? multiLanguageString(name: "collapse", defaultValue: "Collapse", context: context)
                                : multiLanguageString(name: "expand", defaultValue: "Expand", context: context),
                            child: GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              onTap: () {
                                setState(() {
                                  isExpandElseList = !isExpandElseList;
                                });
                              },
                              child: Icon(isExpandElseList ? Icons.expand_more : Icons.chevron_right),
                            ),
                          ),
                        ),
                      ),
                      Column(
                        children: [
                          if (!isExpandElseList)
                            BodyListRootItemWidget(
                              widget.item.thenBodyList,
                              widget.item.type,
                              1,
                              tfTestCaseId: widget.tfTestCaseId,
                              hasCheckBox: widget.hasCheckBox,
                              item: widget.item,
                              callbackChecked: () {
                                widget.onChecked!();
                              },
                              isDragAndDrop: widget.isDragAndDrop,
                              onViewConfig: (testCaseType, testCaseId) {
                                widget.onViewConfig(testCaseType, testCaseId);
                              },
                            ),
                          if (isExpandElseList)
                            BodyListRootItemWidget(
                              widget.item.thenBodyList,
                              widget.item.type,
                              0,
                              tfTestCaseId: widget.tfTestCaseId,
                              hasCheckBox: widget.hasCheckBox,
                              item: widget.item,
                              bodyType: multiLanguageString(name: "else", defaultValue: "Else", context: context),
                              callbackChecked: () {
                                widget.onChecked!();
                              },
                              isDragAndDrop: widget.isDragAndDrop,
                              onViewConfig: (testCaseType, testCaseId) {
                                widget.onViewConfig(testCaseType, testCaseId);
                              },
                            ),
                          if (isExpandElseList)
                            BodyListRootItemWidget(
                              widget.item.elseBodyList,
                              widget.item.type,
                              1,
                              tfTestCaseId: widget.tfTestCaseId,
                              hasCheckBox: widget.hasCheckBox,
                              item: widget.item,
                              callbackChecked: () {
                                widget.onChecked!();
                              },
                              isDragAndDrop: widget.isDragAndDrop,
                              onViewConfig: (testCaseType, testCaseId) {
                                widget.onViewConfig(testCaseType, testCaseId);
                              },
                            ),
                        ],
                      ),
                    ],
                  )
              ],
            )
          : (widget.item.type == "try")
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    HeaderRootItemWidget(
                      widget.item,
                      widget.index,
                      onDelete: () => widget.onDelete(),
                      onDuplicate: (item) => widget.onDuplicate(item),
                      hasDuplicate: widget.hasDuplicate,
                      hasCopy: widget.hasCopy,
                      hasDelete: widget.hasDelete,
                      hasClick: widget.hasClick,
                      isDragAndDrop: widget.isDragAndDrop,
                    ),
                    if (widget.hasGlobalKey)
                      BodyListRootItemWidget(
                        widget.item.bodyList,
                        widget.item.type,
                        0,
                        tfTestCaseId: widget.tfTestCaseId,
                        hasCheckBox: widget.hasCheckBox,
                        item: widget.item,
                        bodyType: multiLanguageString(name: "on_error", defaultValue: "onError", context: context),
                        callbackChecked: () {
                          widget.onChecked!();
                        },
                        isDragAndDrop: widget.isDragAndDrop,
                        onViewConfig: (testCaseType, testCaseId) {
                          widget.onViewConfig(testCaseType, testCaseId);
                        },
                      ),
                    if (widget.hasGlobalKey)
                      BodyListRootItemWidget(
                        widget.item.onErrorBodyList,
                        widget.item.type,
                        0,
                        tfTestCaseId: widget.tfTestCaseId,
                        hasCheckBox: widget.hasCheckBox,
                        item: widget.item,
                        bodyType: multiLanguageString(name: "on_finish", defaultValue: "onFinish", context: context),
                        callbackChecked: () {
                          widget.onChecked!();
                        },
                        isDragAndDrop: widget.isDragAndDrop,
                        onViewConfig: (testCaseType, testCaseId) {
                          widget.onViewConfig(testCaseType, testCaseId);
                        },
                      ),
                    if (widget.hasGlobalKey)
                      BodyListRootItemWidget(
                        widget.item.onFinishBodyList,
                        widget.item.type,
                        1,
                        tfTestCaseId: widget.tfTestCaseId,
                        hasCheckBox: widget.hasCheckBox,
                        item: widget.item,
                        callbackChecked: () {
                          widget.onChecked!();
                        },
                        isDragAndDrop: widget.isDragAndDrop,
                        onViewConfig: (testCaseType, testCaseId) {
                          widget.onViewConfig(testCaseType, testCaseId);
                        },
                      ),
                  ],
                )
              : Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    HeaderRootItemWidget(
                      widget.item,
                      widget.index,
                      onDelete: () => widget.onDelete(),
                      onDuplicate: (item) => widget.onDuplicate(item),
                      hasDuplicate: widget.hasDuplicate,
                      hasCopy: widget.hasCopy,
                      hasDelete: widget.hasDelete,
                      hasClick: widget.hasClick,
                      isDragAndDrop: widget.isDragAndDrop,
                    ),
                    if (widget.hasGlobalKey)
                      BodyListRootItemWidget(
                        widget.item.bodyList,
                        widget.item.type,
                        1,
                        tfTestCaseId: widget.tfTestCaseId,
                        hasCheckBox: widget.hasCheckBox,
                        item: widget.item,
                        callbackChecked: () {
                          widget.onChecked!();
                        },
                        isDragAndDrop: widget.isDragAndDrop,
                        onViewConfig: (testCaseType, testCaseId) {
                          widget.onViewConfig(testCaseType, testCaseId);
                        },
                      ),
                  ],
                );
      return GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () {
          if (widget.hasClick &&
              (widget.item.type == "forEach" || widget.item.type == "group" || widget.item.type == "client" || widget.item.type == "step")) {
            Navigator.of(context).push(
              createRoute(
                PropertyPageWidget(
                  item: widget.item,
                  callback: () {
                    editorNotificationModel.updateNotification();
                  },
                  isDragAndDrop: widget.isDragAndDrop,
                ),
              ),
            );
          }
        },
        child: widget.hasCheckBox
            ? Padding(
                padding: const EdgeInsets.symmetric(vertical: 5),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Checkbox(
                      value: widget.item.isSelected,
                      onChanged: (bool? selected) {
                        widget.item.isSelected = selected!;
                        if (selected) {
                          listElementPlayIdModel.addElementPlayIdList(widget.item.id!, true);
                          widget.onChecked!();
                        } else {
                          listElementPlayIdModel.removeElementPlayIdList(widget.item.id);
                          listElementPlayIdModel.setIsSelectedAll(null);
                        }
                        handleSelectedItem(widget.item, widget.item.isSelected, context);
                        editorNotificationModel.updateNotification();
                      },
                    ),
                    Expanded(child: childControlWidget),
                  ],
                ),
              )
            : childControlWidget,
      );
    });
  }

  handleSelectedItem(Item item, bool isSelect, BuildContext context) {
    if (item.thenBodyList.isNotEmpty) {
      loopListItem(item.thenBodyList, isSelect, context);
    }
    if (item.elseBodyList.isNotEmpty) {
      loopListItem(item.elseBodyList, isSelect, context);
    }
    if (item.bodyList.isNotEmpty) {
      loopListItem(item.bodyList, isSelect, context);
    }
  }

  loopListItem(List<Item> listItem, bool isSelected, BuildContext context) {
    for (Item item in listItem) {
      if (item.isSelected != isSelected) {
        item.isSelected = isSelected;
        if (item.isSelected) {
          Provider.of<ListElementPlayIdModel>(context, listen: false).addElementPlayIdList(item.id!, true);
        } else {
          Provider.of<ListElementPlayIdModel>(context, listen: false).removeElementPlayIdList(item.id!);
        }
      }
      handleSelectedItem(item, isSelected, context);
    }
  }
}
