import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/extension/hex_color.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/editor/dialog_delete_item.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/root_item_widget.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/variable_group_default_widget.dart';
import '../../../common/custom_state.dart';
import '../../../type.dart';

class HeaderRootItemWidget extends StatelessWidget {
  final Item item;
  final int index;
  final VoidCallback onDelete;
  final Function onDuplicate;
  final bool hasCopy;
  final bool hasDelete;
  final bool hasDuplicate;
  final bool hasClick;
  final bool isDragAndDrop;
  const HeaderRootItemWidget(this.item, this.index,
      {Key? key,
      required this.onDelete,
      required this.hasCopy,
      required this.hasDelete,
      required this.hasClick,
      required this.isDragAndDrop,
      required this.onDuplicate,
      required this.hasDuplicate})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer2<EditorNotificationModel, TestCaseConfigModel>(builder: (context, editorNotificationModel, testCaseConfigModel, child) {
      Widget headerView = Container();
      if (item.type == "condition") {
        headerView = Wrap(
          alignment: WrapAlignment.start,
          crossAxisAlignment: WrapCrossAlignment.center,
          children: [
            MultiLanguageText(
              name: "if",
              defaultValue: "If",
              style: Style(context).textDragAndDropWidget,
            ),
            const SizedBox(width: 10),
            RootItemWidget(
              item.conditionGroup,
              parentType: item.type,
              isDragAndDrop: isDragAndDrop,
            ),
            const SizedBox(width: 10),
            MultiLanguageText(
              name: "then",
              defaultValue: "then",
              style: Style(context).textDragAndDropWidget,
            ),
          ],
        );
      }
      if (item.type == "try") {
        headerView = Wrap(
          alignment: WrapAlignment.start,
          crossAxisAlignment: WrapCrossAlignment.center,
          children: [
            MultiLanguageText(
              name: "try",
              defaultValue: "Try",
              style: Style(context).textDragAndDropWidget,
            ),
          ],
        );
      }
      if (item.type == "forEach") {
        headerView = Wrap(
          crossAxisAlignment: WrapCrossAlignment.center,
          children: [
            Wrap(
              children: [
                MultiLanguageText(
                  name: "foreach",
                  defaultValue: "Foreach",
                  style: Style(context).textDragAndDropWidget,
                ),
                Text(
                  " (${item.objectName != "" ? item.objectName : 'variable'} in",
                  style: Style(context).textDragAndDropWidget,
                ),
              ],
            ),
            const SizedBox(width: 10),
            (item.variablePath[0].type == "variableGroup_default" ||
                    item.variablePath[0].type == "variable" ||
                    item.variablePath[0].type == "version_variable" ||
                    item.variablePath[0].type == "environment_variable" ||
                    item.variablePath[0].type == "node_variable" ||
                    item.variablePath[0].type == "value")
                ? VariableGroupDefaultWidget(
                    item,
                    (Item itemCallBack) {
                      item.variablePath = [itemCallBack];
                      editorNotificationModel.updateNotification();
                    },
                    hasClick: hasClick,
                    isDragAndDrop: isDragAndDrop,
                  )
                : RootItemWidget(
                    item.variablePath[0],
                    parentType: item.type,
                    callback: () {
                      item.variablePath = [Item("variableGroup_default", "variableGroup_default")];
                      editorNotificationModel.updateNotification();
                    },
                    isDragAndDrop: isDragAndDrop,
                  ),
            Text(
              ")",
              style: Style(context).textDragAndDropWidget,
            ),
          ],
        );
      }
      if (item.type == "while") {
        headerView = Wrap(
          crossAxisAlignment: WrapCrossAlignment.center,
          children: [
            MultiLanguageText(
              name: "while",
              defaultValue: "While",
              style: Style(context).textDragAndDropWidget,
            ),
            const SizedBox(
              width: 10,
            ),
            RootItemWidget(
              item.conditionGroup,
              parentType: item.type,
              isDragAndDrop: isDragAndDrop,
            ),
          ],
        );
      }
      if (item.type == "group") {
        headerView = Wrap(
          crossAxisAlignment: WrapCrossAlignment.center,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Wrap(
                  children: [
                    MultiLanguageText(
                      name: "group",
                      defaultValue: "Group",
                      style: Style(context).textDragAndDropWidget,
                    ),
                  ],
                ),
                if (item.description != null && item.description!.isNotEmpty)
                  Text(
                    item.description!,
                    style: TextStyle(color: Colors.white, fontSize: context.fontSizeManager.fontSizeText12, decoration: TextDecoration.none),
                  ),
              ],
            ),
          ],
        );
      }
      if (item.type == "step") {
        headerView = Wrap(
          crossAxisAlignment: WrapCrossAlignment.center,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Wrap(
                  children: [
                    MultiLanguageText(
                      name: "flow",
                      defaultValue: "Flow",
                      style: Style(context).textDragAndDropWidget,
                    ),
                  ],
                ),
                if (item.description != null && item.description!.isNotEmpty)
                  Text(
                    item.description!,
                    style: TextStyle(color: Colors.white, fontSize: context.fontSizeManager.fontSizeText12, decoration: TextDecoration.none),
                  ),
              ],
            ),
          ],
        );
      }
      if (item.type == "client") {
        headerView = Wrap(
          crossAxisAlignment: WrapCrossAlignment.center,
          children: [
            MultiLanguageText(
              name: "client",
              defaultValue: "Client",
              style: Style(context).textDragAndDropWidget,
            ),
          ],
        );
      }
      return Container(
        constraints: const BoxConstraints(minHeight: 35, minWidth: 220),
        decoration: BoxDecoration(
          color: HexColor.fromHex(testCaseConfigModel.editorComponentList.firstWhere((element) => element["type"] == item.type)["componentConfig"]
                  ["color"] ??
              "#FFFFAB19"),
          // border: Border.all(
          //   color: (item.type == "group")
          //       ? const Color.fromARGB(255, 69, 94, 92)
          //       : const Color.fromRGBO(207, 139, 23, 1),
          // ),
          borderRadius: BorderRadius.circular(10),
        ),
        padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
        child: Row(
          children: [
            if (index != 0)
              Tooltip(
                message: (item.isError != null && item.isError!) ? multiLanguageString(name: "error", defaultValue: "Error", context: context) : "",
                child: (item.isError != null && item.isError!)
                    ? ErrorIndexWidget(
                        index: index,
                      )
                    : Container(
                        height: 25,
                        width: 25,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20),
                          border: Border.all(color: Colors.grey),
                        ),
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(
                            index.toString(),
                            style: TextStyle(
                              fontSize: context.fontSizeManager.fontSizeText14,
                              color: Colors.black,
                              decoration: TextDecoration.none,
                            ),
                          ),
                        ),
                      ),
              ),
            const SizedBox(
              width: 15,
            ),
            if (item.type == "client")
              Padding(
                padding: const EdgeInsets.only(right: 5),
                child: Text(
                  "${item.platform}(${item.name})",
                  style: Style(context).textDragAndDropWidget,
                ),
              ),
            Expanded(child: headerView),
            if (hasCopy)
              const SizedBox(
                width: 10,
              ),
            if (hasCopy)
              Tooltip(
                message: multiLanguageString(name: "copy", defaultValue: "Copy", context: context),
                child: GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () async {
                    Provider.of<CopyWidgetModel>(context, listen: false).setCopyWidget(item);
                    showToast(
                      context: context,
                      msg: multiLanguageString(name: "copy_success", defaultValue: "Copy success", context: context),
                      color: Colors.greenAccent,
                      icon: const Icon(Icons.done),
                    );
                  },
                  child: const Icon(
                    Icons.content_copy,
                    size: 18,
                    color: Colors.white,
                  ),
                ),
              ),
            if (hasDuplicate)
              const SizedBox(
                width: 10,
              ),
            if (hasDuplicate)
              Tooltip(
                message: multiLanguageString(name: "duplicate", defaultValue: "Duplicate", context: context),
                child: GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () => onDuplicate(item),
                  child: const Icon(
                    Icons.add,
                    size: 20,
                    color: Colors.white,
                  ),
                ),
              ),
            if (hasDelete)
              const SizedBox(
                width: 10,
              ),
            if (hasDelete)
              Tooltip(
                message: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                child: GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    showDialog<String>(
                      context: context,
                      builder: (BuildContext context) {
                        return DialogDeleteItemWidget(
                          selectedItem: item,
                          callback: () {
                            onDelete();
                          },
                        );
                      },
                    );
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                      border: Border.all(color: Colors.red),
                    ),
                    child: const Icon(
                      Icons.close,
                      size: 16,
                      color: Colors.red,
                    ),
                  ),
                ),
              ),
          ],
        ),
      );
    });
  }
}

class ErrorIndexWidget extends StatefulWidget {
  final int index;
  const ErrorIndexWidget({Key? key, required this.index}) : super(key: key);

  @override
  State<ErrorIndexWidget> createState() => _ErrorIndexWidgetState();
}

class _ErrorIndexWidgetState extends CustomState<ErrorIndexWidget> with TickerProviderStateMixin {
  late final AnimationController _controller;
  late Animation _colorTween;
  @override
  void initState() {
    _controller = AnimationController(
      duration: const Duration(seconds: 1),
      vsync: this,
    )..repeat(reverse: true);
    _colorTween = Tween<double>(begin: 1, end: 5).animate(_controller);
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: _colorTween,
        builder: (context, child) => Container(
              height: 25,
              width: 25,
              decoration: BoxDecoration(
                color: _colorTween.value >= 1 && _colorTween.value < 2
                    ? Colors.red
                    : _colorTween.value >= 2 && _colorTween.value < 3
                        ? Colors.green
                        : _colorTween.value >= 3 && _colorTween.value < 4
                            ? Colors.blue
                            : Colors.black,
                borderRadius: BorderRadius.circular(20),
                border: Border.all(
                    color: _colorTween.value >= 1 && _colorTween.value < 2
                        ? Colors.red
                        : _colorTween.value >= 2 && _colorTween.value < 3
                            ? Colors.green
                            : _colorTween.value >= 3 && _colorTween.value < 4
                                ? Colors.blue
                                : Colors.black),
              ),
              child: Align(
                alignment: Alignment.center,
                child: Text(
                  widget.index.toString(),
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText14,
                    color: _colorTween.value >= 1 && _colorTween.value < 2
                        ? Colors.green
                        : _colorTween.value >= 2 && _colorTween.value < 3
                            ? Colors.red
                            : _colorTween.value >= 3 && _colorTween.value < 4
                                ? Colors.yellow
                                : Colors.white,
                    decoration: TextDecoration.none,
                  ),
                ),
              ),
            ));
  }
}
