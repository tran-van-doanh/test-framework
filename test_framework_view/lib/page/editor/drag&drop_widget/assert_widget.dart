import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/extension/hex_color.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/editor/action_button_editor/dialog_action_element.dart';
import 'package:test_framework_view/page/editor/dialog_delete_item.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/action_widget.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/header_root_item_widget.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/root_item_widget.dart';
import 'package:test_framework_view/page/editor/property_widget/property_page_widget.dart';
import 'package:test_framework_view/type.dart';

import '../../../api.dart';

class AssertWidget extends StatefulWidget {
  final Item item;
  final int index;
  final VoidCallback onDelete;
  final Function onViewConfig;
  final Function onDuplicate;
  final String? tfTestCaseId;
  final bool hasCopy;
  final bool hasDelete;
  final bool hasDuplicate;
  final bool hasClick;
  final bool hasPlay;
  final bool hasViewConfig;
  final bool hasCheckBox;
  final VoidCallback? onChecked;
  final bool isDragAndDrop;
  const AssertWidget(
    this.item,
    this.index, {
    Key? key,
    required this.onDelete,
    this.tfTestCaseId,
    required this.hasCopy,
    required this.hasDelete,
    required this.hasDuplicate,
    required this.hasClick,
    required this.hasPlay,
    required this.hasCheckBox,
    this.onChecked,
    required this.isDragAndDrop,
    required this.hasViewConfig,
    required this.onDuplicate,
    required this.onViewConfig,
  }) : super(key: key);

  @override
  State<AssertWidget> createState() => _AssertWidgetState();
}

class _AssertWidgetState extends State<AssertWidget> {
  bool isLoad = false;
  var editorComponent = {};
  @override
  void initState() {
    super.initState();
    getInitPage();
  }

  getInitPage() async {
    final futures = <Future>[
      getEditorComponent(),
      if (widget.item.actionName != null || widget.item.testCaseName != null || widget.item.apiName != null)
        getAction(widget.item.actionName ?? widget.item.testCaseName ?? widget.item.apiName!),
    ];
    await Future.wait<dynamic>(futures);
    if (mounted) {
      setState(() {
        isLoad = true;
      });
    }
  }

  getEditorComponent() async {
    TestCaseConfigModel testCaseConfigModel = Provider.of<TestCaseConfigModel>(context, listen: false);
    if (testCaseConfigModel.editorComponentList.isNotEmpty) {
      setState(() {
        editorComponent = testCaseConfigModel.editorComponentList.firstWhere((element) => element["type"] == widget.item.type);
      });
    }
  }

  getAction(String name) async {
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-case/search", {"name": name}, context);
    if (response.containsKey("body") && response["body"] is String == false && response["body"]["resultList"].isNotEmpty && mounted) {
      setState(() {
        widget.item.actionId = response["body"]["resultList"][0]["id"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if (isLoad) {
      return Consumer3<DebugStatusModel, CopyWidgetModel, EditorNotificationModel>(
        builder: (context, debugStatusModel, copyWidgetModel, editorNotificationModel, child) {
          Widget playWidgetButton = (debugStatusModel.debugStatus && widget.hasPlay)
              ? Row(
                  children: [
                    const SizedBox(
                      width: 10,
                    ),
                    Tooltip(
                      message: multiLanguageString(name: "play", defaultValue: "Play", context: context),
                      child: GestureDetector(
                        onTap: () async {
                          var parameterList = [];
                          for (var parameter in widget.item.parameterList) {
                            var parameterNew = Map.from(parameter);
                            var variablePathNewList = [];
                            for (var variablePath in parameter["variablePath"]) {
                              var variablePathNew = Map.from(variablePath);
                              variablePathNew["type"] = "value";
                              if (variablePathNew.containsKey("name")) {
                                variablePathNew.remove("name");
                              }
                              variablePathNewList.add(variablePathNew);
                            }
                            parameterNew["variablePath"] = variablePathNewList;
                            parameterList.add(parameterNew);
                          }
                          showDialog<String>(
                            context: context,
                            builder: (BuildContext context) {
                              return DialogActionElement(
                                tfTestCaseId: widget.tfTestCaseId,
                                type: widget.item.type,
                                parameterList: parameterList,
                                mouseActionTypeItem: widget.item.mouseActionType,
                                keyItem: widget.item.key,
                                clientName: widget.item.clientName,
                                platform: widget.item.platform,
                              );
                            },
                          );
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(color: Colors.white, width: 2),
                          ),
                          child: const Icon(
                            Icons.play_arrow,
                            size: 18,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              : const SizedBox.shrink();
          Widget copyWidgetButton = (widget.hasCopy)
              ? Row(
                  children: [
                    const SizedBox(
                      width: 10,
                    ),
                    Tooltip(
                      message: multiLanguageString(name: "copy", defaultValue: "Copy", context: context),
                      child: GestureDetector(
                        onTap: () {
                          copyWidgetModel.setCopyWidget(widget.item);
                          showToast(
                            context: context,
                            msg: multiLanguageString(name: "copy_success", defaultValue: "Copy success", context: context),
                            color: Colors.greenAccent,
                            icon: const Icon(Icons.done),
                          );
                        },
                        child: const Icon(
                          Icons.content_copy,
                          size: 18,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                )
              : const SizedBox.shrink();
          Widget duplicateWidget = (widget.hasDuplicate)
              ? Row(
                  children: [
                    const SizedBox(
                      width: 10,
                    ),
                    Tooltip(
                      message: multiLanguageString(name: "duplicate", defaultValue: "Duplicate", context: context),
                      child: GestureDetector(
                        onTap: () => widget.onDuplicate(widget.item),
                        child: const Icon(
                          Icons.add,
                          size: 20,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                )
              : const SizedBox.shrink();
          Widget deleteWidget = (widget.hasDelete)
              ? Row(
                  children: [
                    const SizedBox(
                      width: 10,
                    ),
                    Tooltip(
                      message: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                      child: GestureDetector(
                        onTap: () {
                          showDialog<String>(
                            context: context,
                            builder: (BuildContext context) {
                              return DialogDeleteItemWidget(
                                selectedItem: widget.item,
                                callback: () {
                                  widget.onDelete();
                                },
                              );
                            },
                          );
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20),
                            border: Border.all(color: Colors.red),
                          ),
                          child: const Icon(
                            Icons.close,
                            size: 18,
                            color: Colors.red,
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              : const SizedBox.shrink();
          Widget indexWidget = Tooltip(
            message: (widget.item.isError != null && widget.item.isError!)
                ? multiLanguageString(name: "error", defaultValue: "Error", context: context)
                : "",
            child: (widget.item.isError != null && widget.item.isError!)
                ? ErrorIndexWidget(
                    index: widget.index,
                  )
                : Container(
                    height: 25,
                    width: 25,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                      border: Border.all(color: Colors.grey),
                    ),
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        widget.index.toString(),
                        style: TextStyle(
                          fontSize: context.fontSizeManager.fontSizeText14,
                          color: Colors.black,
                          decoration: TextDecoration.none,
                        ),
                      ),
                    ),
                  ),
          );
          Widget parameterWidget = Wrap(
            children: [
              Text(
                " (",
                style: Style(context).textDragAndDropWidget,
              ),
              for (var parameter in widget.item.parameterList)
                Wrap(
                  children: [
                    if (parameter["variablePath"] == null)
                      MultiLanguageText(
                        name: "null",
                        defaultValue: "null",
                        style: Style(context).textDragAndDropWidget,
                      ),
                    if (parameter["variablePath"] != null)
                      for (var variablePath in parameter["variablePath"])
                        VariablePathWidget(
                          isLast: parameter["variablePath"].last != variablePath,
                          variablePath: variablePath,
                          key: Key(variablePath.toString()),
                          //key: UniqueKey(),
                        ),
                    if (widget.item.parameterList.last != parameter)
                      Text(
                        ", ",
                        style: Style(context).textDragAndDropWidget,
                      ),
                  ],
                ),
              Text(
                ")",
                style: Style(context).textDragAndDropWidget,
              ),
            ],
          );
          Widget childActionWidget = (widget.item.type == "assertEquals" || widget.item.type == "assertNull" || widget.item.type == "assertNotNull")
              ? Row(
                  children: [
                    if (widget.index != 0) indexWidget,
                    if (widget.index != 0)
                      const SizedBox(
                        width: 15,
                      ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Wrap(
                            children: [
                              MultiLanguageText(
                                name: widget.item.type.toLowerCase(),
                                defaultValue: editorComponent["title"],
                                overflow: TextOverflow.clip,
                                style: Style(context).textDragAndDropWidget,
                              ),
                              if (widget.item.parameterList.isNotEmpty) parameterWidget,
                            ],
                          ),
                          if (widget.item.description != null && widget.item.description!.isNotEmpty)
                            Text(
                              widget.item.description!,
                              style:
                                  TextStyle(color: Colors.white, fontSize: context.fontSizeManager.fontSizeText12, decoration: TextDecoration.none),
                            ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    playWidgetButton,
                    copyWidgetButton,
                    duplicateWidget,
                    deleteWidget
                  ],
                )
              : (widget.item.type == "assertTrue" || widget.item.type == "assertFalse")
                  ? Row(
                      children: [
                        if (widget.index != 0) indexWidget,
                        if (widget.index != 0)
                          const SizedBox(
                            width: 15,
                          ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Wrap(
                                crossAxisAlignment: WrapCrossAlignment.center,
                                alignment: WrapAlignment.center,
                                children: [
                                  MultiLanguageText(
                                    name: widget.item.type.toLowerCase(),
                                    defaultValue: editorComponent["title"],
                                    overflow: TextOverflow.clip,
                                    style: Style(context).textDragAndDropWidget,
                                  ),
                                  if (widget.item.parameterList.isNotEmpty) parameterWidget,
                                  const SizedBox(width: 5),
                                  RootItemWidget(
                                    widget.item.conditionGroup,
                                    parentType: widget.item.type,
                                    callback: () {
                                      widget.item.conditionList = [Item("condition_default", "condition_default")];
                                      editorNotificationModel.updateNotification();
                                    },
                                    isDragAndDrop: widget.isDragAndDrop,
                                  ),
                                ],
                              ),
                              if (widget.item.description != null && widget.item.description!.isNotEmpty)
                                Text(
                                  widget.item.description!,
                                  style: TextStyle(
                                      color: Colors.white, fontSize: context.fontSizeManager.fontSizeText12, decoration: TextDecoration.none),
                                ),
                            ],
                          ),
                        ),
                        copyWidgetButton,
                        duplicateWidget,
                        deleteWidget
                      ],
                    )
                  : MultiLanguageText(name: "type_not_implement", defaultValue: "\$0 not implement", variables: [widget.item.type]);
          return Material(
            borderRadius: BorderRadius.circular(10),
            child: GestureDetector(
              onTap: () {
                if (widget.hasClick) {
                  Navigator.of(context).push(
                    createRoute(
                      PropertyPageWidget(
                        item: widget.item,
                        callback: () {
                          editorNotificationModel.updateNotification();
                        },
                        callbackAction: (actionId) => widget.item.actionId = actionId,
                        isDragAndDrop: widget.isDragAndDrop,
                      ),
                    ),
                  );
                }
              },
              child: widget.hasCheckBox
                  ? Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Checkbox(
                            value: widget.item.isSelected,
                            onChanged: (bool? selected) {
                              widget.item.isSelected = selected!;
                              if (widget.item.isSelected) {
                                Provider.of<ListElementPlayIdModel>(context, listen: false).addElementPlayIdList(widget.item.id, true);
                                widget.onChecked!();
                              } else {
                                Provider.of<ListElementPlayIdModel>(context, listen: false).removeElementPlayIdList(widget.item.id);
                                Provider.of<ListElementPlayIdModel>(context, listen: false).setIsSelectedAll(null);
                              }
                              editorNotificationModel.updateNotification();
                            },
                          ),
                          Expanded(
                              child: Container(
                                  constraints: const BoxConstraints(minHeight: 35, minWidth: 200),
                                  decoration: BoxDecoration(
                                    color: HexColor.fromHex(editorComponent["componentConfig"]["color"] ?? "#4C97FF"),
                                    // border: Border.all(
                                    //   color: const Color.fromRGBO(51, 115, 204, 1),
                                    // ),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  padding: const EdgeInsets.all(10),
                                  child: childActionWidget)),
                        ],
                      ),
                    )
                  : Container(
                      constraints: const BoxConstraints(minHeight: 35, minWidth: 200),
                      decoration: BoxDecoration(
                        color: HexColor.fromHex(editorComponent["componentConfig"]["color"] ?? "#397397"),
                        // border: Border.all(
                        //   color: const Color.fromRGBO(51, 115, 204, 1),
                        // ),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      padding: const EdgeInsets.all(10),
                      child: childActionWidget),
            ),
          );
        },
      );
    }
    return const Center(
      child: CircularProgressIndicator(),
    );
  }
}
