import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/custom_draggable.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/extension/hex_color.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/root_item_widget.dart';
import 'package:test_framework_view/page/editor/property_widget/property_page_widget.dart';
import 'package:test_framework_view/type.dart';
import '../../../model/model.dart';

class VariableWidget extends StatelessWidget {
  final Item item;
  final Function callback;
  final bool hasClick;
  final bool isDragAndDrop;
  const VariableWidget(this.item, this.callback, {Key? key, required this.hasClick, required this.isDragAndDrop}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer2<EditorNotificationModel, TestCaseConfigModel>(builder: (context, editorNotificationModel, testCaseConfigModel, child) {
      return Wrap(
        alignment: WrapAlignment.center,
        crossAxisAlignment: WrapCrossAlignment.center,
        children: [
          GestureDetector(
            onTap: (hasClick)
                ? () {
                    Navigator.of(context).push(
                      createRoute(
                        PropertyPageWidget(
                          item: item,
                          callback: () {
                            editorNotificationModel.updateNotification();
                          },
                          isDragAndDrop: isDragAndDrop,
                        ),
                      ),
                    );
                  }
                : null,
            child: isDragAndDrop
                ? CustomDraggable(
                    data: item,
                    childWhenDragging: Container(
                      width: 30,
                      height: 30,
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey),
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(16),
                      ),
                      child: Center(
                        child: Text(
                          "\$",
                          style: TextStyle(fontSize: context.fontSizeManager.fontSizeText14),
                        ),
                      ),
                    ),
                    feedback: RootItemWidget(
                      item,
                    ),
                    onDragStarted: () {},
                    onDragCompleted: () {
                      callback();
                    },
                    child: variableBodyWidget(context, testCaseConfigModel, editorNotificationModel),
                  )
                : variableBodyWidget(context, testCaseConfigModel, editorNotificationModel),
          ),
        ],
      );
    });
  }

  Widget btnAddVariableWidget(editorNotificationModel) {
    return Material(
      borderRadius: BorderRadius.circular(16),
      child: InkWell(
        onTap: () {
          item.operatorList.add(Item("variable_default", "variable_default"));
          editorNotificationModel.updateNotification();
        },
        child: Container(
          width: 30,
          height: 30,
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
            color: Colors.white,
            borderRadius: BorderRadius.circular(16),
          ),
          child: const Center(
            child: Icon(
              Icons.add,
              color: Colors.black,
              size: 20,
            ),
          ),
        ),
      ),
    );
  }

  Container variableBodyWidget(context, testCaseConfigModel, editorNotificationModel) {
    return Container(
      constraints: const BoxConstraints(minHeight: 35),
      decoration: BoxDecoration(
        color: HexColor.fromHex(
            testCaseConfigModel.editorComponentList.firstWhere((element) => element["type"] == item.type)["componentConfig"]["color"] ?? "#FFFF8C1A"),
        border: Border.all(
          color: HexColor.fromHex(testCaseConfigModel.editorComponentList.firstWhere((element) => element["type"] == item.type)["componentConfig"]
                  ["borderColor"] ??
              "#FFDB6E00"),
        ),
        borderRadius: BorderRadius.circular(50),
      ),
      padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
      child: Wrap(
        alignment: WrapAlignment.center,
        crossAxisAlignment: WrapCrossAlignment.center,
        children: [
          for (var i = 0; i < item.operatorList.length; i++) variableItemWidget(i, context),
          const SizedBox(width: 10),
          AbsorbPointer(absorbing: !isDragAndDrop, child: btnAddVariableWidget(editorNotificationModel))
        ],
      ),
    );
  }

  Widget variableItemWidget(int i, context) {
    return Wrap(
      alignment: WrapAlignment.center,
      crossAxisAlignment: WrapCrossAlignment.center,
      children: [
        if (i != 0)
          Wrap(
            alignment: WrapAlignment.center,
            crossAxisAlignment: WrapCrossAlignment.center,
            children: [
              const SizedBox(width: 10),
              Text(
                item.operatorType ?? "",
                style: Style(context).textDragAndDropWidget,
              ),
              const SizedBox(width: 10)
            ],
          ),
        RootItemWidget(
          item.operatorList[i],
          isDragAndDrop: isDragAndDrop,
        ),
      ],
    );
  }
}
