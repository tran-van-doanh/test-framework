import 'package:flutter/material.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/root_item_widget.dart';
import 'package:test_framework_view/page/editor/template_list_widget/condition_variable_default_template.dart';
import '../../../common/custom_state.dart';
import '../../../type.dart';

class ConditionGroupDefaultWidget extends StatefulWidget {
  final Item item;
  final Function callback;
  const ConditionGroupDefaultWidget(this.item, this.callback, {Key? key}) : super(key: key);

  @override
  State<ConditionGroupDefaultWidget> createState() => _ConditionGroupDefaultWidgetState();
}

class _ConditionGroupDefaultWidgetState extends CustomState<ConditionGroupDefaultWidget> {
  late Widget widgetGhost;
  bool willAccept = false;

  @override
  Widget build(BuildContext context) {
    return DragTarget<Dragable>(
      builder: (
        BuildContext context,
        List<dynamic> accepted,
        List<dynamic> rejected,
      ) {
        return willAccept
            ? IntrinsicWidth(
                child: Opacity(
                  opacity: 0.5,
                  child: widgetGhost,
                ),
              )
            : const ConditionDefaultTemplate();
      },
      onWillAccept: (data) {
        setState(() {
          if (data is Template && data.category == "conditionGroup") {
            willAccept = true;
            widgetGhost = data.widget;
          }
          if (data is Item && data.category == "conditionGroup") {
            willAccept = true;
            widgetGhost = RootItemWidget(data);
          }
        });
        return true;
      },
      onLeave: (data) {
        setState(() {
          willAccept = false;
        });
      },
      onAccept: (Dragable data) {
        setState(() {
          if (data is Template && data.category == "conditionGroup") {
            Item item = Item(data.type, data.category);
            willAccept = false;
            widget.callback(item);
          }
          if (data is Item && data.category == "conditionGroup") {
            willAccept = false;
            widget.callback(data);
          }
        });
      },
    );
  }
}
