import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/extension/hex_color.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/editor/action_button_editor/dialog_action_element.dart';
import 'package:test_framework_view/page/editor/action_button_editor/dialog_call_action_widget.dart';
import 'package:test_framework_view/page/editor/action_button_editor/dialog_go_to_url.dart';
import 'package:test_framework_view/page/editor/dialog_delete_item.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/header_root_item_widget.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/root_item_widget.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/variable_group_default_widget.dart';
import 'package:test_framework_view/page/editor/property_widget/property_page_widget.dart';
import 'package:test_framework_view/type.dart';

import '../../../api.dart';
import '../../../common/custom_state.dart';

class ActionWidget extends StatefulWidget {
  final Item item;
  final int index;
  final VoidCallback onDelete;
  final Function onViewConfig;
  final Function onDuplicate;
  final String? tfTestCaseId;
  final bool hasCopy;
  final bool hasDelete;
  final bool hasDuplicate;
  final bool hasClick;
  final bool hasPlay;
  final bool hasViewConfig;
  final bool hasCheckBox;
  final VoidCallback? onChecked;
  final bool isDragAndDrop;
  const ActionWidget(
    this.item,
    this.index, {
    Key? key,
    required this.onDelete,
    this.tfTestCaseId,
    required this.hasCopy,
    required this.hasDelete,
    required this.hasDuplicate,
    required this.hasClick,
    required this.hasPlay,
    required this.hasCheckBox,
    this.onChecked,
    required this.isDragAndDrop,
    required this.hasViewConfig,
    required this.onDuplicate,
    required this.onViewConfig,
  }) : super(key: key);

  @override
  State<ActionWidget> createState() => _ActionWidgetState();
}

class _ActionWidgetState extends State<ActionWidget> {
  bool isLoad = false;
  var editorComponent = {};
  @override
  void initState() {
    super.initState();
    getInitPage();
  }

  getInitPage() async {
    final futures = <Future>[
      getEditorComponent(),
      if (widget.item.actionName != null || widget.item.testCaseName != null || widget.item.apiName != null)
        getAction(widget.item.actionName ?? widget.item.testCaseName ?? widget.item.apiName!),
    ];
    await Future.wait<dynamic>(futures);
    if (mounted) {
      setState(() {
        isLoad = true;
      });
    }
  }

  getEditorComponent() async {
    TestCaseConfigModel testCaseConfigModel = Provider.of<TestCaseConfigModel>(context, listen: false);
    if (testCaseConfigModel.editorComponentList.isNotEmpty) {
      setState(() {
        editorComponent = testCaseConfigModel.editorComponentList.firstWhere((element) => element["type"] == widget.item.type);
      });
    }
  }

  getAction(String name) async {
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-case/search", {"name": name}, context);
    if (response.containsKey("body") && response["body"] is String == false && response["body"]["resultList"].isNotEmpty && mounted) {
      setState(() {
        widget.item.actionId = response["body"]["resultList"][0]["id"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if (isLoad) {
      return Consumer3<DebugStatusModel, CopyWidgetModel, EditorNotificationModel>(
        builder: (context, debugStatusModel, copyWidgetModel, editorNotificationModel, child) {
          Widget playWidgetButton = (debugStatusModel.debugStatus && widget.hasPlay)
              ? Row(
                  children: [
                    const SizedBox(
                      width: 10,
                    ),
                    Tooltip(
                      message: multiLanguageString(name: "play", defaultValue: "Play", context: context),
                      child: GestureDetector(
                        onTap: () async {
                          var parameterList = [];
                          for (var parameter in widget.item.parameterList) {
                            var parameterNew = Map.from(parameter);
                            var variablePathNewList = [];
                            for (var variablePath in parameter["variablePath"]) {
                              var variablePathNew = Map.from(variablePath);
                              variablePathNew["type"] = "value";
                              if (variablePathNew.containsKey("name")) {
                                variablePathNew.remove("name");
                              }
                              variablePathNewList.add(variablePathNew);
                            }
                            parameterNew["variablePath"] = variablePathNewList;
                            parameterList.add(parameterNew);
                          }
                          showDialog<String>(
                            context: context,
                            builder: (BuildContext context) {
                              if (widget.item.type == "actionCall" || widget.item.type == "testCaseCall" || widget.item.type == "apiCall") {
                                return DialogCallActionWidget(
                                  typeWidget: widget.item.type,
                                  actionName: widget.item.type == "actionCall"
                                      ? widget.item.actionName
                                      : widget.item.type == "testCaseCall"
                                          ? widget.item.testCaseName
                                          : widget.item.apiName,
                                  parameterList: parameterList,
                                  typeCallAction: "play_widget",
                                  clientList: widget.item.clientList,
                                );
                              }
                              if (widget.item.type == "goTo") {
                                return DialogGoToUrl(
                                  tfTestCaseId: widget.tfTestCaseId,
                                  textController: parameterList.isNotEmpty ? parameterList[0]["variablePath"][0]["value"] : "",
                                  clientName: widget.item.clientName,
                                );
                              }
                              return DialogActionElement(
                                tfTestCaseId: widget.tfTestCaseId,
                                type: widget.item.type,
                                parameterList: parameterList,
                                mouseActionTypeItem: widget.item.mouseActionType,
                                keyItem: widget.item.key,
                                clientName: widget.item.clientName,
                                platform: widget.item.platform,
                              );
                            },
                          );
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(color: Colors.white, width: 2),
                          ),
                          child: const Icon(
                            Icons.play_arrow,
                            size: 18,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              : const SizedBox.shrink();
          Widget copyWidgetButton = (widget.hasCopy)
              ? Row(
                  children: [
                    const SizedBox(
                      width: 10,
                    ),
                    Tooltip(
                      message: multiLanguageString(name: "copy", defaultValue: "Copy", context: context),
                      child: GestureDetector(
                        onTap: () {
                          copyWidgetModel.setCopyWidget(widget.item);
                          showToast(
                            context: context,
                            msg: multiLanguageString(name: "copy_success", defaultValue: "Copy success", context: context),
                            color: Colors.greenAccent,
                            icon: const Icon(Icons.done),
                          );
                        },
                        child: const Icon(
                          Icons.content_copy,
                          size: 18,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                )
              : const SizedBox.shrink();
          Widget duplicateWidget = (widget.hasDuplicate)
              ? Row(
                  children: [
                    const SizedBox(
                      width: 10,
                    ),
                    Tooltip(
                      message: multiLanguageString(name: "duplicate", defaultValue: "Duplicate", context: context),
                      child: GestureDetector(
                        onTap: () => widget.onDuplicate(widget.item),
                        child: const Icon(
                          Icons.add,
                          size: 20,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                )
              : const SizedBox.shrink();
          Widget deleteWidget = (widget.hasDelete)
              ? Row(
                  children: [
                    const SizedBox(
                      width: 10,
                    ),
                    Tooltip(
                      message: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                      child: GestureDetector(
                        onTap: () {
                          showDialog<String>(
                            context: context,
                            builder: (BuildContext context) {
                              return DialogDeleteItemWidget(
                                selectedItem: widget.item,
                                callback: () {
                                  widget.onDelete();
                                },
                              );
                            },
                          );
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20),
                            border: Border.all(color: Colors.red),
                          ),
                          child: const Icon(
                            Icons.close,
                            size: 18,
                            color: Colors.red,
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              : const SizedBox.shrink();
          Widget indexWidget = Tooltip(
            message: (widget.item.isError != null && widget.item.isError!)
                ? multiLanguageString(name: "error", defaultValue: "Error", context: context)
                : "",
            child: (widget.item.isError != null && widget.item.isError!)
                ? ErrorIndexWidget(
                    index: widget.index,
                  )
                : Container(
                    height: 25,
                    width: 25,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                      border: Border.all(color: Colors.grey),
                    ),
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        widget.index.toString(),
                        style: TextStyle(
                          fontSize: context.fontSizeManager.fontSizeText14,
                          color: Colors.black,
                          decoration: TextDecoration.none,
                        ),
                      ),
                    ),
                  ),
          );
          Widget parameterWidget = Wrap(
            children: [
              Text(
                " (",
                style: Style(context).textDragAndDropWidget,
              ),
              if ((widget.item.type == "mouseAction" || widget.item.type == "mouseActionWithOffset") && widget.item.mouseActionType != "NONE")
                Text(
                  "${widget.item.mouseActionType}, ",
                  style: Style(context).textDragAndDropWidget,
                ),
              for (var parameter in widget.item.parameterList)
                Wrap(
                  children: [
                    if (parameter["variablePath"] == null)
                      MultiLanguageText(
                        name: "null",
                        defaultValue: "null",
                        style: Style(context).textDragAndDropWidget,
                      ),
                    if (parameter["variablePath"] != null)
                      for (var variablePath in parameter["variablePath"])
                        VariablePathWidget(
                          isLast: parameter["variablePath"].last != variablePath,
                          variablePath: variablePath,
                          key: Key(variablePath.toString()),
                          //key: UniqueKey(),
                        ),
                    if (widget.item.parameterList.last != parameter)
                      Text(
                        ", ",
                        style: Style(context).textDragAndDropWidget,
                      ),
                  ],
                ),
              if ((widget.item.type == "sendKeys" ||
                      widget.item.type == "sendKeysByPosition" ||
                      widget.item.type == "clearAndSendKeys" ||
                      widget.item.type == "clearAndSendKeysByPosition" ||
                      widget.item.type == "sendKeyFromElement" ||
                      widget.item.type == "childElementSendKeyFromElement" ||
                      widget.item.type == "sendKeyWithoutElement") &&
                  widget.item.key != "NONE")
                Text(
                  ", ${widget.item.key}",
                  style: Style(context).textDragAndDropWidget,
                ),
              Text(
                ")",
                style: Style(context).textDragAndDropWidget,
              ),
            ],
          );
          Widget childActionWidget = (widget.item.type == "actionCall" || widget.item.type == "testCaseCall" || widget.item.type == "apiCall")
              ? Row(
                  children: [
                    if (widget.index != 0) indexWidget,
                    if (widget.index != 0)
                      const SizedBox(
                        width: 15,
                      ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Wrap(
                            children: [
                              (widget.item.returnType.isNotEmpty && widget.item.returnType["type"] != "none")
                                  ? Text(
                                      "${widget.item.returnType["isList"] == true ? "List<${widget.item.returnType["dataType"]}>" : widget.item.returnType["dataType"]} ${widget.item.returnType["name"]} = ",
                                      style: Style(context).textDragAndDropWidget,
                                    )
                                  : const SizedBox.shrink(),
                              MultiLanguageText(
                                name: widget.item.type.toLowerCase(),
                                defaultValue: editorComponent["title"],
                                overflow: TextOverflow.clip,
                                style: Style(context).textDragAndDropWidget,
                              ),
                              const SizedBox(width: 5),
                              Text(
                                ": ${(widget.item.type == "actionCall") ? widget.item.actionName : (widget.item.type == "testCaseCall") ? widget.item.testCaseName : widget.item.apiName}",
                                style: Style(context).textDragAndDropWidget,
                              ),
                              if (widget.item.parameterList.isNotEmpty) parameterWidget
                            ],
                          ),
                          if (widget.item.description != null && widget.item.description!.isNotEmpty)
                            Text(
                              widget.item.description!,
                              style:
                                  TextStyle(color: Colors.white, fontSize: context.fontSizeManager.fontSizeText12, decoration: TextDecoration.none),
                            ),
                        ],
                      ),
                    ),
                    if (widget.item.actionName != null || widget.item.testCaseName != null || widget.item.apiName != null)
                      const SizedBox(
                        width: 10,
                      ),
                    if (widget.hasViewConfig &&
                        widget.item.type != "apiCall" &&
                        (widget.item.actionName != null || widget.item.testCaseName != null || widget.item.apiName != null))
                      Tooltip(
                        message: multiLanguageString(name: "view_config", defaultValue: "View config", context: context),
                        child: GestureDetector(
                          onTap: () => widget.onViewConfig(widget.item.type == "actionCall" ? "action" : "test_scenario", widget.item.actionId),
                          child: const Icon(
                            Icons.visibility_outlined,
                            size: 20,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    playWidgetButton,
                    copyWidgetButton,
                    duplicateWidget,
                    deleteWidget
                  ],
                )
              : (widget.item.type == "sendKeys" ||
                      widget.item.type == "sendKeysByPosition" ||
                      widget.item.type == "childElementSendKeys" ||
                      widget.item.type == "clearAndSendKeys" ||
                      widget.item.type == "clearAndSendKeysByPosition" ||
                      widget.item.type == "childElementClearAndSendKeys" ||
                      widget.item.type == "sendKeyFromElement" ||
                      widget.item.type == "childElementSendKeyFromElement" ||
                      widget.item.type == "sendKeyWithoutElement" ||
                      widget.item.type == "sendKeysWithoutElement" ||
                      widget.item.type == "clearAndSendFile" ||
                      widget.item.type == "childElementClearAndSendFile" ||
                      widget.item.type == "closeWindow" ||
                      widget.item.type == "applicationRefresh" ||
                      widget.item.type == "bringForeground" ||
                      widget.item.type == "startApplication" ||
                      widget.item.type == "installApplication" ||
                      widget.item.type == "mouseAction" ||
                      widget.item.type == "mouseActionWithOffset" ||
                      widget.item.type == "click" ||
                      widget.item.type == "switchToFrame" ||
                      widget.item.type == "clickByPosition" ||
                      widget.item.type == "childElementClick" ||
                      widget.item.type == "selectTableRow" ||
                      widget.item.type == "selectByValue" ||
                      widget.item.type == "getSelectedValue" ||
                      widget.item.type == "getSelectedVisibleValue" ||
                      widget.item.type == "selectByValueByPosition" ||
                      widget.item.type == "selectByVisibleTextByPosition" ||
                      widget.item.type == "childElementSelectByValue" ||
                      widget.item.type == "selectByVisibleText" ||
                      widget.item.type == "childElementSelectByVisibleText" ||
                      widget.item.type == "goTo" ||
                      widget.item.type == "executeScript" ||
                      widget.item.type == "executeScriptOnElement" ||
                      widget.item.type == "waitUrl" ||
                      widget.item.type == "waitElementExists" ||
                      widget.item.type == "waitElementDisappear" ||
                      widget.item.type == "waitRepositoryElementDisappear" ||
                      widget.item.type == "waitRepositoryElementExists" ||
                      widget.item.type == "childElementWaitElement" ||
                      widget.item.type == "clickWithOffset" ||
                      widget.item.type == "childElementClickWithOffset" ||
                      widget.item.type == "sendKeysAlert" ||
                      widget.item.type == "split" ||
                      widget.item.type == "ftpUpload" ||
                      widget.item.type == "ftpDownload" ||
                      widget.item.type == "ftpWaitFile" ||
                      widget.item.type == "sftpUpload" ||
                      widget.item.type == "sftpDownload" ||
                      widget.item.type == "sftpWaitFile" ||
                      widget.item.type == "textFileRead" ||
                      widget.item.type == "textFileWrite" ||
                      widget.item.type == "excelFileRead" ||
                      widget.item.type == "excelFileWrite" ||
                      widget.item.type == "jsonFileRead" ||
                      widget.item.type == "jsonFileWrite")
                  ? Row(
                      children: [
                        if (widget.index != 0) indexWidget,
                        if (widget.index != 0)
                          const SizedBox(
                            width: 15,
                          ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Wrap(
                                children: [
                                  MultiLanguageText(
                                    name: widget.item.type.toLowerCase(),
                                    defaultValue: editorComponent["title"],
                                    overflow: TextOverflow.clip,
                                    style: Style(context).textDragAndDropWidget,
                                  ),
                                  if (widget.item.parameterList.isNotEmpty) parameterWidget,
                                ],
                              ),
                              if (widget.item.description != null && widget.item.description!.isNotEmpty)
                                Text(
                                  widget.item.description!,
                                  style: TextStyle(
                                      color: Colors.white, fontSize: context.fontSizeManager.fontSizeText12, decoration: TextDecoration.none),
                                ),
                            ],
                          ),
                        ),
                        if (widget.item.type != "childElementClearAndSendFile" &&
                            widget.item.type != "waitUrl" &&
                            widget.item.type != "waitElementExists" &&
                            widget.item.type != "waitElementDisappear" &&
                            widget.item.type != "waitRepositoryElementDisappear" &&
                            widget.item.type != "waitRepositoryElementExists" &&
                            widget.item.type != "childElementWaitElement" &&
                            widget.item.type != "split")
                          Row(
                            children: [
                              const SizedBox(
                                width: 10,
                              ),
                              playWidgetButton,
                            ],
                          ),
                        copyWidgetButton,
                        duplicateWidget,
                        deleteWidget
                      ],
                    )
                  : (widget.item.type == "acceptAlert" || widget.item.type == "dismissAlert")
                      ? Row(
                          children: [
                            if (widget.index != 0) indexWidget,
                            if (widget.index != 0)
                              const SizedBox(
                                width: 15,
                              ),
                            Expanded(
                              child: MultiLanguageText(
                                name: widget.item.type.toLowerCase(),
                                defaultValue: editorComponent["title"],
                                overflow: TextOverflow.clip,
                                style: Style(context).textDragAndDropWidget,
                              ),
                            ),
                            playWidgetButton,
                            copyWidgetButton,
                            duplicateWidget,
                            deleteWidget
                          ],
                        )
                      : (widget.item.type == "findElements" ||
                              widget.item.type == "childElementFindElements" ||
                              widget.item.type == "findElementByPosition" ||
                              widget.item.type == "findElementsByPosition" ||
                              widget.item.type == "findElement" ||
                              widget.item.type == "childElementFindElement" ||
                              widget.item.type == "getUrl" ||
                              widget.item.type == "getText" ||
                              widget.item.type == "getSelectOptionsValue" ||
                              widget.item.type == "getSelectVisibleOptionsValue" ||
                              widget.item.type == "childElementGetText" ||
                              widget.item.type == "getAttribute" ||
                              widget.item.type == "childElementGetAttribute" ||
                              widget.item.type == "getAlertText" ||
                              widget.item.type == "getWindowHandle" ||
                              widget.item.type == "getWindowHandles" ||
                              widget.item.type == "selectWindow" ||
                              widget.item.type == "switchToWindow" ||
                              widget.item.type == "switchToDefaultContent" ||
                              widget.item.type == "switchToNextWindow" ||
                              widget.item.type == "switchToPreviousWindow" ||
                              widget.item.type == "replaceAll" ||
                              widget.item.type == "substring" ||
                              widget.item.type == "parseInteger" ||
                              widget.item.type == "parseLong" ||
                              widget.item.type == "parseDouble" ||
                              widget.item.type == "parseFloat" ||
                              widget.item.type == "parseDate" ||
                              widget.item.type == "format" ||
                              widget.item.type == "datePlus" ||
                              widget.item.type == "dateMinus" ||
                              widget.item.type == "parseDateTime")
                          ? Row(
                              children: [
                                if (widget.index != 0) indexWidget,
                                if (widget.index != 0)
                                  const SizedBox(
                                    width: 15,
                                  ),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Wrap(
                                        crossAxisAlignment: WrapCrossAlignment.center,
                                        alignment: WrapAlignment.center,
                                        children: [
                                          (widget.item.returnType.isNotEmpty && widget.item.returnType["type"] != "none")
                                              ? Text(
                                                  "${widget.item.returnType["isList"] == true ? "List" : ""} ${widget.item.returnType["name"]} = ",
                                                  style: Style(context).textDragAndDropWidget,
                                                )
                                              : const SizedBox.shrink(),
                                          MultiLanguageText(
                                            name: widget.item.type.toLowerCase(),
                                            defaultValue: editorComponent["title"],
                                            overflow: TextOverflow.clip,
                                            style: Style(context).textDragAndDropWidget,
                                          ),
                                          if (widget.item.parameterList.isNotEmpty) parameterWidget
                                        ],
                                      ),
                                      if (widget.item.description != null && widget.item.description!.isNotEmpty)
                                        Text(
                                          widget.item.description!,
                                          style: TextStyle(
                                              color: Colors.white, fontSize: context.fontSizeManager.fontSizeText12, decoration: TextDecoration.none),
                                        ),
                                    ],
                                  ),
                                ),
                                if (widget.item.type != "findElements" &&
                                    widget.item.type != "childElementFindElements" &&
                                    widget.item.type != "findElement" &&
                                    widget.item.type != "findElementByPosition" &&
                                    widget.item.type != "findElementsByPosition" &&
                                    widget.item.type != "childElementFindElement" &&
                                    widget.item.type != "getUrl" &&
                                    widget.item.type != "replaceAll" &&
                                    widget.item.type != "substring" &&
                                    widget.item.type != "parseInteger" &&
                                    widget.item.type != "parseLong" &&
                                    widget.item.type != "parseDouble" &&
                                    widget.item.type != "parseFloat" &&
                                    widget.item.type != "parseDate" &&
                                    widget.item.type != "format" &&
                                    widget.item.type != "datePlus" &&
                                    widget.item.type != "dateMinus" &&
                                    widget.item.type != "parseDateTime")
                                  Row(
                                    children: [
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      playWidgetButton,
                                    ],
                                  ),
                                copyWidgetButton,
                                duplicateWidget,
                                deleteWidget
                              ],
                            )
                          : (widget.item.type == "sleep")
                              ? Row(
                                  children: [
                                    if (widget.index != 0) indexWidget,
                                    if (widget.index != 0)
                                      const SizedBox(
                                        width: 15,
                                      ),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Wrap(
                                            crossAxisAlignment: WrapCrossAlignment.center,
                                            alignment: WrapAlignment.center,
                                            children: [
                                              MultiLanguageText(
                                                name: widget.item.type.toLowerCase(),
                                                defaultValue: editorComponent["title"],
                                                overflow: TextOverflow.clip,
                                                style: Style(context).textDragAndDropWidget,
                                              ),
                                              const SizedBox(width: 5),
                                              (widget.item.variablePath[0].type == "variableGroup_default" ||
                                                      widget.item.variablePath[0].type == "variable" ||
                                                      widget.item.variablePath[0].type == "version_variable" ||
                                                      widget.item.variablePath[0].type == "environment_variable" ||
                                                      widget.item.variablePath[0].type == "node_variable" ||
                                                      widget.item.variablePath[0].type == "value")
                                                  ? VariableGroupDefaultWidget(
                                                      widget.item,
                                                      (Item itemCallBack) {
                                                        widget.item.variablePath = [itemCallBack];
                                                        editorNotificationModel.updateNotification();
                                                      },
                                                      hasClick: widget.hasClick,
                                                      isDragAndDrop: widget.isDragAndDrop,
                                                    )
                                                  : RootItemWidget(
                                                      widget.item.variablePath[0],
                                                      parentType: widget.item.type,
                                                      callback: () {
                                                        widget.item.variablePath = [Item("variableGroup_default", "variableGroup_default")];
                                                        editorNotificationModel.updateNotification();
                                                      },
                                                      isDragAndDrop: widget.isDragAndDrop,
                                                    ),
                                              const SizedBox(width: 5),
                                              Text(
                                                "ms",
                                                style: Style(context).textDragAndDropWidget,
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                    copyWidgetButton,
                                    duplicateWidget,
                                    deleteWidget
                                  ],
                                )
                              : (widget.item.type == "newVariable" || widget.item.type == "assignValue")
                                  ? Row(
                                      children: [
                                        if (widget.index != 0) indexWidget,
                                        if (widget.index != 0)
                                          const SizedBox(
                                            width: 15,
                                          ),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Wrap(
                                                crossAxisAlignment: WrapCrossAlignment.center,
                                                alignment: WrapAlignment.center,
                                                children: [
                                                  MultiLanguageText(
                                                    name: widget.item.type.toLowerCase(),
                                                    defaultValue: editorComponent["title"],
                                                    overflow: TextOverflow.clip,
                                                    style: Style(context).textDragAndDropWidget,
                                                  ),
                                                  if (widget.item.name != "")
                                                    Text(
                                                      " : ${widget.item.isList == true ? "List" : ""} ${widget.item.name}",
                                                      style: Style(context).textDragAndDropWidget,
                                                    ),
                                                  const SizedBox(width: 5),
                                                  (widget.item.variablePath[0].type == "variableGroup_default" ||
                                                          widget.item.variablePath[0].type == "variable" ||
                                                          widget.item.variablePath[0].type == "version_variable" ||
                                                          widget.item.variablePath[0].type == "environment_variable" ||
                                                          widget.item.variablePath[0].type == "node_variable" ||
                                                          widget.item.variablePath[0].type == "value")
                                                      ? VariableGroupDefaultWidget(
                                                          widget.item,
                                                          (Item itemCallBack) {
                                                            widget.item.variablePath = [itemCallBack];
                                                            editorNotificationModel.updateNotification();
                                                          },
                                                          hasClick: widget.hasClick,
                                                          isDragAndDrop: widget.isDragAndDrop,
                                                        )
                                                      : RootItemWidget(
                                                          widget.item.variablePath[0],
                                                          parentType: widget.item.type,
                                                          callback: () {
                                                            widget.item.variablePath = [Item("variableGroup_default", "variableGroup_default")];
                                                            editorNotificationModel.updateNotification();
                                                          },
                                                          isDragAndDrop: widget.isDragAndDrop,
                                                        ),
                                                ],
                                              ),
                                              if (widget.item.description != null && widget.item.description!.isNotEmpty)
                                                Text(
                                                  widget.item.description!,
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: context.fontSizeManager.fontSizeText12,
                                                      decoration: TextDecoration.none),
                                                ),
                                            ],
                                          ),
                                        ),
                                        copyWidgetButton,
                                        duplicateWidget,
                                        deleteWidget
                                      ],
                                    )
                                  : (widget.item.type == "break" || widget.item.type == "continue")
                                      ? Row(
                                          children: [
                                            if (widget.index != 0) indexWidget,
                                            if (widget.index != 0)
                                              const SizedBox(
                                                width: 15,
                                              ),
                                            Expanded(
                                              child: MultiLanguageText(
                                                name: widget.item.type.toLowerCase(),
                                                defaultValue: editorComponent["title"],
                                                overflow: TextOverflow.clip,
                                                style: Style(context).textDragAndDropWidget,
                                              ),
                                            ),
                                            copyWidgetButton,
                                            duplicateWidget,
                                            deleteWidget
                                          ],
                                        )
                                      : widget.item.type == "error"
                                          ? Row(
                                              children: [
                                                if (widget.index != 0) indexWidget,
                                                if (widget.index != 0)
                                                  const SizedBox(
                                                    width: 15,
                                                  ),
                                                Expanded(
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      Wrap(
                                                        crossAxisAlignment: WrapCrossAlignment.center,
                                                        alignment: WrapAlignment.center,
                                                        children: [
                                                          MultiLanguageText(
                                                            name: widget.item.type.toLowerCase(),
                                                            defaultValue: editorComponent["title"],
                                                            overflow: TextOverflow.clip,
                                                            style: Style(context).textDragAndDropWidget,
                                                          ),
                                                          const SizedBox(width: 5),
                                                          (widget.item.variablePath[0].type == "variableGroup_default" ||
                                                                  widget.item.variablePath[0].type == "variable" ||
                                                                  widget.item.variablePath[0].type == "version_variable" ||
                                                                  widget.item.variablePath[0].type == "environment_variable" ||
                                                                  widget.item.variablePath[0].type == "node_variable" ||
                                                                  widget.item.variablePath[0].type == "value")
                                                              ? VariableGroupDefaultWidget(
                                                                  widget.item,
                                                                  (Item itemCallBack) {
                                                                    widget.item.variablePath = [itemCallBack];
                                                                    editorNotificationModel.updateNotification();
                                                                  },
                                                                  hasClick: widget.hasClick,
                                                                  isDragAndDrop: widget.isDragAndDrop,
                                                                )
                                                              : RootItemWidget(
                                                                  widget.item.variablePath[0],
                                                                  parentType: widget.item.type,
                                                                  callback: () {
                                                                    widget.item.variablePath = [
                                                                      Item("variableGroup_default", "variableGroup_default")
                                                                    ];
                                                                    editorNotificationModel.updateNotification();
                                                                  },
                                                                  isDragAndDrop: widget.isDragAndDrop,
                                                                ),
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                copyWidgetButton,
                                                duplicateWidget,
                                                deleteWidget
                                              ],
                                            )
                                          : widget.item.type == "finish"
                                              ? Row(
                                                  children: [
                                                    if (widget.index != 0) indexWidget,
                                                    if (widget.index != 0)
                                                      const SizedBox(
                                                        width: 15,
                                                      ),
                                                    Expanded(
                                                      child: Column(
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: [
                                                          Wrap(
                                                            children: [
                                                              MultiLanguageText(
                                                                name: widget.item.type.toLowerCase(),
                                                                defaultValue: editorComponent["title"],
                                                                overflow: TextOverflow.clip,
                                                                style: Style(context).textDragAndDropWidget,
                                                              ),
                                                            ],
                                                          ),
                                                          if (widget.item.message != null && widget.item.message!.isNotEmpty)
                                                            Text(
                                                              widget.item.message!,
                                                              style: TextStyle(
                                                                  color: Colors.white,
                                                                  fontSize: context.fontSizeManager.fontSizeText12,
                                                                  decoration: TextDecoration.none),
                                                            ),
                                                        ],
                                                      ),
                                                    ),
                                                    copyWidgetButton,
                                                    duplicateWidget,
                                                    deleteWidget
                                                  ],
                                                )
                                              : (widget.item.type == "comment")
                                                  ? Row(
                                                      children: [
                                                        if (widget.index != 0) indexWidget,
                                                        if (widget.index != 0)
                                                          const SizedBox(
                                                            width: 15,
                                                          ),
                                                        Expanded(
                                                          child: Column(
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            children: [
                                                              Wrap(
                                                                children: [
                                                                  MultiLanguageText(
                                                                    name: widget.item.type.toLowerCase(),
                                                                    defaultValue: editorComponent["title"],
                                                                    overflow: TextOverflow.clip,
                                                                    style: Style(context).textDragAndDropWidget,
                                                                  ),
                                                                  if (widget.item.parameterList.isNotEmpty) parameterWidget
                                                                ],
                                                              ),
                                                              if (widget.item.description != null && widget.item.description!.isNotEmpty)
                                                                Text(
                                                                  widget.item.description!,
                                                                  style: TextStyle(
                                                                      color: Colors.white,
                                                                      fontSize: context.fontSizeManager.fontSizeText12,
                                                                      decoration: TextDecoration.none),
                                                                ),
                                                            ],
                                                          ),
                                                        ),
                                                        copyWidgetButton,
                                                        duplicateWidget,
                                                        deleteWidget
                                                      ],
                                                    )
                                                  : MultiLanguageText(
                                                      name: "type_not_implement", defaultValue: "\$0 not implement", variables: [widget.item.type]);
          return Material(
            borderRadius: BorderRadius.circular(10),
            child: GestureDetector(
              onTap: () {
                if (widget.hasClick &&
                    widget.item.type != "break" &&
                    widget.item.type != "closeWindow" &&
                    widget.item.type != "applicationRefresh" &&
                    widget.item.type != "bringForeground" &&
                    widget.item.type != "continue" &&
                    widget.item.type != "acceptAlert" &&
                    widget.item.type != "dismissAlert" &&
                    widget.item.type != "switchToDefaultContent" &&
                    widget.item.type != "switchToNextWindow" &&
                    widget.item.type != "switchToPreviousWindow" &&
                    widget.item.type != "sleep" &&
                    widget.item.type != "error") {
                  Navigator.of(context).push(
                    createRoute(
                      PropertyPageWidget(
                        item: widget.item,
                        callback: () {
                          editorNotificationModel.updateNotification();
                        },
                        callbackAction: (actionId) => widget.item.actionId = actionId,
                        isDragAndDrop: widget.isDragAndDrop,
                      ),
                    ),
                  );
                }
              },
              child: widget.hasCheckBox
                  ? Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Checkbox(
                            value: widget.item.isSelected,
                            onChanged: (bool? selected) {
                              widget.item.isSelected = selected!;
                              if (widget.item.isSelected) {
                                Provider.of<ListElementPlayIdModel>(context, listen: false).addElementPlayIdList(widget.item.id, true);
                                widget.onChecked!();
                              } else {
                                Provider.of<ListElementPlayIdModel>(context, listen: false).removeElementPlayIdList(widget.item.id);
                                Provider.of<ListElementPlayIdModel>(context, listen: false).setIsSelectedAll(null);
                              }
                              editorNotificationModel.updateNotification();
                            },
                          ),
                          Expanded(
                              child: Container(
                                  constraints: const BoxConstraints(minHeight: 35, minWidth: 200),
                                  decoration: BoxDecoration(
                                    color: HexColor.fromHex(editorComponent["componentConfig"]["color"] ?? "#4C97FF"),
                                    // border: Border.all(
                                    //   color: const Color.fromRGBO(51, 115, 204, 1),
                                    // ),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  padding: const EdgeInsets.all(10),
                                  child: childActionWidget)),
                        ],
                      ),
                    )
                  : Container(
                      constraints: const BoxConstraints(minHeight: 35, minWidth: 200),
                      decoration: BoxDecoration(
                        color: HexColor.fromHex(editorComponent["componentConfig"]["color"] ?? "#4C97FF"),
                        // border: Border.all(
                        //   color: const Color.fromRGBO(51, 115, 204, 1),
                        // ),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      padding: const EdgeInsets.all(10),
                      child: childActionWidget),
            ),
          );
        },
      );
    }
    return const Center(
      child: CircularProgressIndicator(),
    );
  }
}

class VariablePathWidget extends StatefulWidget {
  final dynamic variablePath;
  final bool isLast;
  final Color? colorText;
  const VariablePathWidget({Key? key, this.variablePath, this.isLast = false, this.colorText}) : super(key: key);

  @override
  State<VariablePathWidget> createState() => _VariablePathWidgetState();
}

class _VariablePathWidgetState extends CustomState<VariablePathWidget> {
  String? elementRepository;
  @override
  void initState() {
    super.initState();
    if (widget.variablePath["type"] == "value" &&
        widget.variablePath["dataType"] == "RepositoryElement" &&
        widget.variablePath["tfTestElementRepositoryId"] != null &&
        widget.variablePath["tfTestElementId"] != null) {
      getElementRepositoryPath(widget.variablePath["tfTestElementRepositoryId"], widget.variablePath["tfTestElementId"],
          name: widget.variablePath["tfTestElementXpathName"]);
    }
    if (widget.variablePath["type"] == "value" && widget.variablePath["tfValueTemplateId"] != null) {
      getElementTemplate(widget.variablePath["tfValueTemplateId"]);
    }
  }

  getElementTemplate(id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-value-template/$id", context);
    if (response.containsKey("body") && response["body"] is String == false && response["body"]["result"] != null) {
      setState(() {
        elementRepository =
            (response["body"]["result"]["tfValueType"] != null ? response["body"]["result"]["tfValueType"]["name"] ?? "null" : "null") +
                " / " +
                (response["body"]["result"]["name"] ?? "null");
      });
    }
  }

  getElementRepositoryPath(parentId, id, {name}) async {
    String result = "";
    var directoryPathResponse = await httpGet("/test-framework-api/user/test-framework/tf-test-element-repository/$parentId/path", context);
    if (directoryPathResponse.containsKey("body") && directoryPathResponse["body"] is String == false) {
      for (var path in directoryPathResponse["body"]["resultList"] ?? []) {
        if (path != null) {
          result += (path["title"] ?? "null") + " / ";
        }
      }
    }
    if (mounted) {
      var elementResponse = await httpGet("/test-framework-api/user/test-framework/tf-test-element/$id", context);
      if (elementResponse.containsKey("body") && elementResponse["body"] is String == false) {
        if (elementResponse["body"]["result"] != null) {
          result += elementResponse["body"]["result"]["name"];
          if (name != null) result += " # $name";
        } else {
          result += '"has been deleted"';
        }
      }
      setState(() {
        elementRepository = result;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: [
        Text(
          (widget.variablePath["type"] == "variable" ||
                  widget.variablePath["type"] == "version_variable" ||
                  widget.variablePath["type"] == "environment_variable" ||
                  widget.variablePath["type"] == "node_variable" ||
                  widget.variablePath["type"] == "get")
              ? widget.variablePath["name"] ?? "null"
              : ((widget.variablePath["type"] == "value" && widget.variablePath["dataType"] == "RepositoryElement") ||
                      (widget.variablePath["type"] == "value" && widget.variablePath["tfValueTemplateId"] != null))
                  ? elementRepository ?? ''
                  : widget.variablePath["value"] ?? "null",
          style: TextStyle(
              color: widget.colorText ?? Colors.yellowAccent,
              fontSize: context.fontSizeManager.fontSizeText14,
              // fontWeight: FontWeight.bold,
              letterSpacing: 1.5,
              decoration: TextDecoration.none),
        ),
        if (widget.isLast)
          Text(
            ".",
            style: TextStyle(
                color: widget.colorText ?? Colors.white,
                fontSize: context.fontSizeManager.fontSizeText14,
                fontWeight: FontWeight.bold,
                letterSpacing: 1.5,
                decoration: TextDecoration.none),
          ),
      ],
    );
  }
}
