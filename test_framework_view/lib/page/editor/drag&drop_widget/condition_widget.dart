import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/custom_draggable.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/extension/hex_color.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/root_item_widget.dart';
import 'package:test_framework_view/page/editor/property_widget/property_page_widget.dart';
import 'package:test_framework_view/page/editor/template_list_widget/condition_variable_default_template.dart';
import 'package:test_framework_view/type.dart';
import '../../../model/model.dart';
import 'action_widget.dart';

class ConditionWidget extends StatelessWidget {
  final Item item;
  final Function callback;
  final bool hasClick;
  final bool isDragAndDrop;
  const ConditionWidget(this.item, this.callback, {Key? key, required this.hasClick, required this.isDragAndDrop}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer2<EditorNotificationModel, TestCaseConfigModel>(
      builder: (context, editorNotificationModel, testCaseConfigModel, child) {
        return GestureDetector(
          onTap: (hasClick && item.type != "checkAlertExists")
              ? () {
                  Navigator.of(context).push(
                    createRoute(
                      PropertyPageWidget(
                        item: item,
                        isDragAndDrop: isDragAndDrop,
                        callback: () {
                          editorNotificationModel.updateNotification();
                        },
                      ),
                    ),
                  );
                }
              : null,
          child: isDragAndDrop
              ? CustomDraggable(
                  data: item,
                  childWhenDragging: const ConditionDefaultTemplate(),
                  feedback: RootItemWidget(
                    item,
                  ),
                  onDragStarted: () {},
                  onDragCompleted: () {
                    callback(item.category);
                  },
                  child: conditionBodyWidget(context, testCaseConfigModel),
                )
              : conditionBodyWidget(context, testCaseConfigModel),
        );
      },
    );
  }

  Container conditionBodyWidget(context, testCaseConfigModel) {
    return Container(
      constraints: const BoxConstraints(minHeight: 35),
      decoration: BoxDecoration(
        color: HexColor.fromHex(
            testCaseConfigModel.editorComponentList.firstWhere((element) => element["type"] == item.type)["componentConfig"]["color"] ?? "#FF59C059"),
        border: Border.all(
          color: HexColor.fromHex(testCaseConfigModel.editorComponentList.firstWhere((element) => element["type"] == item.type)["componentConfig"]
                  ["borderColor"] ??
              "#FF389838"),
        ),
        borderRadius: BorderRadius.circular(10),
      ),
      padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
      child: (item.type == "checkWindowExists" ||
              item.type == "checkElementExists" ||
              item.type == "checkAlertExists" ||
              item.type == "childElementCheckElementExists" ||
              item.type == "checkElementEnabled" ||
              item.type == "childElementCheckElementEnabled" ||
              item.type == "isNull" ||
              item.type == "isNotNull" ||
              item.type == "isEmpty" ||
              item.type == "isNotEmpty")
          ? conditionWithParmWidget(context)
          : conditionWithoutParmWidget(context),
    );
  }

  Widget conditionWithoutParmWidget(context) {
    return Wrap(
      alignment: WrapAlignment.center,
      crossAxisAlignment: WrapCrossAlignment.center,
      children: [
        RootItemWidget(item.o1!, isDragAndDrop: isDragAndDrop),
        const SizedBox(width: 10),
        MultiLanguageText(
          name: item.operatorType!.toLowerCase().replaceAll(' ', '_'),
          defaultValue: item.operatorType!,
          style: Style(context).textDragAndDropWidget,
        ),
        const SizedBox(width: 10),
        RootItemWidget(item.o2!, isDragAndDrop: isDragAndDrop),
      ],
    );
  }

  Widget conditionWithParmWidget(context) {
    return IntrinsicWidth(
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Wrap(
                  children: [
                    MultiLanguageText(
                      name: item.operatorType!.toLowerCase().replaceAll(' ', '_'),
                      defaultValue: item.operatorType!,
                      style: Style(context).textDragAndDropWidget,
                    ),
                    if (item.parameterList.isNotEmpty)
                      Wrap(
                        children: [
                          Text(
                            " (",
                            style: Style(context).textDragAndDropWidget,
                          ),
                          for (var parameter in item.parameterList)
                            Wrap(
                              children: [
                                if (parameter["variablePath"] == null)
                                  MultiLanguageText(
                                    name: "null",
                                    defaultValue: "null",
                                    style: Style(context).textDragAndDropWidget,
                                  ),
                                if (parameter["variablePath"] != null)
                                  for (var variablePath in parameter["variablePath"])
                                    VariablePathWidget(
                                      isLast: parameter["variablePath"].last != variablePath,
                                      variablePath: variablePath,
                                      key: UniqueKey(),
                                    ),
                                if (item.parameterList.last != parameter)
                                  Text(
                                    ", ",
                                    style: Style(context).textDragAndDropWidget,
                                  ),
                              ],
                            ),
                          Text(
                            ")",
                            style: Style(context).textDragAndDropWidget,
                          ),
                        ],
                      ),
                  ],
                ),
                if (item.description != null && item.description!.isNotEmpty)
                  Text(
                    item.description!,
                    style: Style(context).textDescriptionWidget,
                  ),
              ],
            ),
          ),
          for (var i = 0; i < item.conditionList.length; i++)
            Padding(
              padding: const EdgeInsets.only(left: 5),
              child: RootItemWidget(
                item.conditionList[i],
                isDragAndDrop: isDragAndDrop,
              ),
            ),
        ],
      ),
    );
  }
}
