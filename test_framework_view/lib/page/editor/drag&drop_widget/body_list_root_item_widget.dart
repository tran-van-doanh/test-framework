import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/custom_draggable.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/footer_root_item_widget.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/root_item_widget.dart';
import '../../../common/custom_state.dart';
import '../../../type.dart';

class BodyListRootItemWidget extends StatefulWidget {
  final List<Item> bodyList;
  final Item? item;
  final String type;
  final String? bodyType;
  final int isHaveFooter;
  final String? tfTestCaseId;
  final bool hasCheckBox;
  final VoidCallback? callbackChecked;
  final Function? onViewConfig;
  final bool isDragAndDrop;

  const BodyListRootItemWidget(
    this.bodyList,
    this.type,
    this.isHaveFooter, {
    Key? key,
    this.tfTestCaseId,
    required this.hasCheckBox,
    this.item,
    this.callbackChecked,
    this.bodyType,
    this.isDragAndDrop = true,
    this.onViewConfig,
  }) : super(key: key);

  @override
  State<BodyListRootItemWidget> createState() => _BodyListRootItemWidgetState();
}

class _BodyListRootItemWidgetState extends CustomState<BodyListRootItemWidget> {
  Widget widgetGhost = Container();
  late int oldIndexItem;
  late int newIndexItem;
  int acceptedDataIndex = -1;
  bool dragAccepted = false;
  bool willAccept = false;

  setClientNameAndPlatform(Item parentItem, Item item) {
    // if (item.type != "actionCall" && item.type != "testcaseCall" && item.type != "apiCall") {
    if (parentItem.type == "client" && parentItem.name != null) {
      item.clientName = parentItem.name;
    }
    if (parentItem.type != "client" && parentItem.clientName != null) {
      item.clientName = parentItem.clientName;
    }
    if (parentItem.platform != null) {
      item.platform = parentItem.platform;
    }
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ModeViewEditorModel>(
      builder: (context, modeViewEditorModel, child) {
        return DragTarget<Dragable>(
          builder: (
            BuildContext context,
            List<dynamic> accepted,
            List<dynamic> rejected,
          ) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: widget.type != "rootDefault" ? const EdgeInsets.only(left: 15) : const EdgeInsets.symmetric(vertical: 5),
                  padding: widget.type != "rootDefault" ? const EdgeInsets.fromLTRB(0, 5, 0, 5) : null,
                  constraints: widget.type != "rootDefault" ? const BoxConstraints(minHeight: 35) : null,
                  decoration: widget.type != "rootDefault"
                      ? BoxDecoration(
                          border: Border(
                            left: BorderSide(
                              width: 2,
                              color: (widget.type == "client")
                                  ? const Color.fromARGB(255, 205, 167, 206)
                                  : (widget.type == "group" || widget.type == "step")
                                      ? const Color.fromARGB(255, 86, 112, 111)
                                      : const Color.fromRGBO(255, 171, 25, 1),
                            ),
                          ),
                        )
                      : null,
                  child: Column(
                    children: [
                      if (widget.bodyList.isNotEmpty && widget.bodyList[0].isExpand)
                        for (var i = 0; i < widget.bodyList.length; i++)
                          (!modeViewEditorModel.isStepMode)
                              ? expandedWidget(i)
                              : widget.bodyList[i].type == "step"
                                  ? expandedWidget(i)
                                  : const SizedBox.shrink(),
                      if (widget.bodyList.isNotEmpty && !widget.bodyList[0].isExpand && widget.type != "rootDefault") collapsedWidget(),
                      if (willAccept)
                        Container(
                          margin: const EdgeInsets.fromLTRB(30, 5, 0, 5),
                          child: Opacity(
                            opacity: 0.5,
                            child: widgetGhost,
                          ),
                        ),
                    ],
                  ),
                ),
                if (widget.isHaveFooter == 1)
                  FooterRootItemWidget(
                    widget.bodyList,
                    "footer",
                    widget.type,
                    platform: widget.item!.platform,
                    callbackWidgetGhost: (willAccept, widgetGhost) {
                      setState(() {
                        this.willAccept = willAccept;
                        this.widgetGhost = widgetGhost;
                        if (willAccept && widget.bodyList.isNotEmpty && !widget.bodyList[0].isExpand) {
                          widget.bodyList[0].isExpand = true;
                        }
                      });
                    },
                  )
                else if (widget.isHaveFooter == 0)
                  FooterRootItemWidget(
                    widget.bodyList,
                    widget.bodyType ?? "footer",
                    widget.type,
                    platform: widget.item!.platform,
                    callbackWidgetGhost: (willAccept, widgetGhost) {
                      setState(() {
                        this.willAccept = willAccept;
                        this.widgetGhost = widgetGhost;
                        if (willAccept && widget.bodyList.isNotEmpty && !widget.bodyList[0].isExpand) {
                          widget.bodyList[0].isExpand = true;
                        }
                      });
                    },
                  )
              ],
            );
          },
          onWillAccept: (data) {
            if (data is Template &&
                checkStepWidget(widget.item != null ? widget.item!.type : "rootDefault", data.type) &&
                checkCategoryBodyListItem(data.category) &&
                checkPlatformTemplateContainBodyListItem(widget.item, data)) {
              willAccept = true;
              widgetGhost = data.widget;
            }
            if (data is Item &&
                checkStepWidget(widget.item != null ? widget.item!.type : "rootDefault", data.type) &&
                checkCategoryBodyListItem(data.category) &&
                checkPlatformTemplateContainBodyListItem(widget.item, data)) {
              willAccept = true;
              widgetGhost = RootItemWidget(data);
            }
            return true;
          },
          onLeave: (data) {
            willAccept = false;
          },
          onAccept: (Dragable data) {
            setState(() {
              willAccept = false;
              if (data is Template &&
                  checkStepWidget(widget.item != null ? widget.item!.type : "rootDefault", data.type) &&
                  checkCategoryBodyListItem(data.category) &&
                  checkPlatformTemplateContainBodyListItem(widget.item, data)) {
                Item item = Item(data.type, data.category);
                if (data.type != "client" && widget.item != null) {
                  setClientNameAndPlatform(widget.item!, item);
                }
                widget.bodyList.add(item);
              }
              if (data is Item &&
                  checkStepWidget(widget.item != null ? widget.item!.type : "rootDefault", data.type) &&
                  checkCategoryBodyListItem(data.category) &&
                  checkPlatformTemplateContainBodyListItem(widget.item, data)) {
                if (data.type != "client" && widget.item != null) {
                  setClientNameAndPlatform(widget.item!, data);
                }
                if (data.bodyList.length > 1 && !data.bodyList[0].isExpand) {
                  data.bodyList[0].isExpand = true;
                }
                if (data.elseBodyList.length > 1 && !data.elseBodyList[0].isExpand) {
                  data.elseBodyList[0].isExpand = true;
                }
                if (data.thenBodyList.length > 1 && !data.thenBodyList[0].isExpand) {
                  data.thenBodyList[0].isExpand = true;
                }
                widget.bodyList.add(data);
                Provider.of<CopyWidgetModel>(context, listen: false).setCopyWidget(data);
              }
            });
          },
        );
      },
    );
  }

  Widget expandedWidget(int i) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (widget.type != "rootDefault" && i == 0 && widget.bodyList.length > 1)
          Padding(
            padding: const EdgeInsets.only(top: 15),
            child: SizedBox(
              width: 30,
              child: Tooltip(
                message: multiLanguageString(name: "collapse", defaultValue: "Collapse", context: context),
                child: GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    setState(() {
                      widget.bodyList[i].isExpand = !widget.bodyList[i].isExpand;
                    });
                  },
                  child: const Icon(Icons.expand_more),
                ),
              ),
            ),
          ),
        if (widget.type != "rootDefault" && (i != 0 || widget.bodyList.length == 1))
          const SizedBox(
            width: 30,
          ),
        Expanded(
          child: widget.hasCheckBox
              ? RootItemWidget(
                  widget.bodyList[i],
                  hasClick: false,
                  hasCopy: false,
                  hasDelete: false,
                  hasDuplicate: false,
                  hasPlay: false,
                  hasCheckBox: true,
                  hasGlobalKey: true,
                  parentItem: widget.item,
                  callbackChecked: () {
                    widget.callbackChecked!();
                  },
                )
              : DragTarget<Dragable>(
                  builder: (
                    BuildContext context,
                    List<dynamic> accepted,
                    List<dynamic> rejected,
                  ) {
                    return Column(
                      children: [
                        if (i == acceptedDataIndex)
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 5),
                            child: Opacity(
                              opacity: 0.5,
                              child: widgetGhost,
                            ),
                          ),
                        Container(
                          margin: const EdgeInsets.symmetric(vertical: 5),
                          child: widget.isDragAndDrop
                              ? CustomDraggable(
                                  data: widget.bodyList[i],
                                  childWhenDragging: Container(),
                                  feedback: SizedBox(
                                    width: MediaQuery.of(context).size.width - 380,
                                    child: RootItemWidget(
                                      widget.bodyList[i],
                                      isFeedbackDrag: true,
                                    ),
                                  ),
                                  onDragStarted: () {
                                    oldIndexItem = i;
                                    dragAccepted = false;
                                  },
                                  onDragCompleted: () {
                                    setState(() {
                                      if (dragAccepted) {
                                        if (newIndexItem <= oldIndexItem) {
                                          widget.bodyList.removeAt(oldIndexItem + 1);
                                        } else {
                                          widget.bodyList.removeAt(oldIndexItem);
                                        }
                                      } else {
                                        widget.bodyList.removeAt(oldIndexItem);
                                      }
                                    });
                                  },
                                  child: RootItemWidget(
                                    widget.bodyList[i],
                                    index: i + 1,
                                    callback: () {
                                      setState(() {
                                        widget.bodyList.remove(widget.bodyList[i]);
                                      });
                                    },
                                    onDuplicate: (item) {
                                      Item itemAdd = Item(item.type, item.category);
                                      setClientNameAndPlatform(item, itemAdd);
                                      setState(() {
                                        widget.bodyList.insert(i + 1, itemAdd);
                                      });
                                    },
                                    tfTestCaseId: widget.tfTestCaseId,
                                    hasGlobalKey: true,
                                    onViewConfig: (testCaseType, testCaseId) {
                                      widget.onViewConfig!(testCaseType, testCaseId);
                                    },
                                  ),
                                )
                              : RootItemWidget(
                                  widget.bodyList[i],
                                  index: i + 1,
                                  callback: () {
                                    setState(() {
                                      widget.bodyList.remove(widget.bodyList[i]);
                                    });
                                  },
                                  onDuplicate: (item) {
                                    Item itemAdd = Item(item.type, item.category);
                                    setClientNameAndPlatform(item, itemAdd);
                                    setState(() {
                                      widget.bodyList.insert(i + 1, itemAdd);
                                    });
                                  },
                                  tfTestCaseId: widget.tfTestCaseId,
                                  isDragAndDrop: widget.isDragAndDrop,
                                ),
                        )
                      ],
                    );
                  },
                  onWillAccept: (data) {
                    setState(() {
                      if (data is Template &&
                          checkStepWidget(widget.item != null ? widget.item!.type : "rootDefault", data.type) &&
                          checkCategoryBodyListItem(data.category) &&
                          checkPlatformTemplateContainBodyListItem(widget.item, data)) {
                        acceptedDataIndex = i;
                        widgetGhost = data.widget;
                      }
                      if (data is Item &&
                          checkStepWidget(widget.item != null ? widget.item!.type : "rootDefault", data.type) &&
                          checkCategoryBodyListItem(data.category) &&
                          checkPlatformTemplateContainBodyListItem(widget.item, data)) {
                        acceptedDataIndex = i;
                        widgetGhost = RootItemWidget(data);
                      }
                    });
                    return true;
                  },
                  onLeave: (data) {
                    acceptedDataIndex = -1;
                  },
                  onAccept: (Dragable data) {
                    setState(() {
                      dragAccepted = true;
                      acceptedDataIndex = -1;
                      newIndexItem = widget.bodyList.indexOf(widget.bodyList[i]);
                      if (data is Template &&
                          checkStepWidget(widget.item != null ? widget.item!.type : "rootDefault", data.type) &&
                          checkCategoryBodyListItem(data.category) &&
                          checkPlatformTemplateContainBodyListItem(widget.item, data)) {
                        Item item = Item(data.type, data.category);
                        if (data.type != "client" && widget.item != null) {
                          setClientNameAndPlatform(widget.item!, item);
                        }
                        widget.bodyList.insert(newIndexItem, item);
                      }
                      if (data is Item &&
                          checkStepWidget(widget.item != null ? widget.item!.type : "rootDefault", data.type) &&
                          checkCategoryBodyListItem(data.category) &&
                          checkPlatformTemplateContainBodyListItem(widget.item, data)) {
                        if (data.type != "client" && widget.item != null) {
                          setClientNameAndPlatform(widget.item!, data);
                        }
                        if (data.bodyList.length > 1 && !data.bodyList[0].isExpand) {
                          data.bodyList[0].isExpand = true;
                        }
                        if (data.elseBodyList.length > 1 && !data.elseBodyList[0].isExpand) {
                          data.elseBodyList[0].isExpand = true;
                        }
                        if (data.thenBodyList.length > 1 && !data.thenBodyList[0].isExpand) {
                          data.thenBodyList[0].isExpand = true;
                        }
                        widget.bodyList.insert(newIndexItem, data);
                        Provider.of<CopyWidgetModel>(context, listen: false).setCopyWidget(data);
                      }
                    });
                  },
                ),
        ),
      ],
    );
  }

  Widget collapsedWidget() {
    return Row(
      children: [
        SizedBox(
          width: 30,
          child: Center(
            child: Tooltip(
              message: multiLanguageString(name: "expand", defaultValue: "Expand", context: context),
              child: Material(
                child: InkWell(
                  hoverColor: Colors.transparent,
                  onTap: () {
                    setState(() {
                      widget.bodyList[0].isExpand = !widget.bodyList[0].isExpand;
                    });
                  },
                  child: const Icon(Icons.chevron_right),
                ),
              ),
            ),
          ),
        ),
        Expanded(
          child: DragTarget<Dragable>(
            builder: (
              BuildContext context,
              List<dynamic> accepted,
              List<dynamic> rejected,
            ) {
              return Column(
                children: [
                  if (0 == acceptedDataIndex)
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 5),
                      child: Opacity(
                        opacity: 0.5,
                        child: widgetGhost,
                      ),
                    ),
                  Container(
                    height: 50,
                    margin: const EdgeInsets.symmetric(vertical: 5),
                    decoration: BoxDecoration(
                      color: Colors.blueGrey,
                      // border: Border.all(
                      //   color: Colors.grey,
                      // ),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    padding: const EdgeInsets.all(10),
                    child: const Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "......",
                          style: TextStyle(color: Colors.white),
                        ),
                      ],
                    ),
                  )
                ],
              );
            },
            onWillAccept: (data) {
              widget.bodyList[0].isExpand = true;
              if (data is Template &&
                  checkStepWidget(widget.item != null ? widget.item!.type : "rootDefault", data.type) &&
                  checkCategoryBodyListItem(data.category) &&
                  checkPlatformTemplateContainBodyListItem(widget.item, data)) {
                acceptedDataIndex = 0;
                widgetGhost = data.widget;
              }
              if (data is Item &&
                  checkStepWidget(widget.item != null ? widget.item!.type : "rootDefault", data.type) &&
                  checkCategoryBodyListItem(data.category) &&
                  checkPlatformTemplateContainBodyListItem(widget.item, data)) {
                acceptedDataIndex = 0;
                widgetGhost = RootItemWidget(data);
              }
              return true;
            },
            onLeave: (data) {
              acceptedDataIndex = -1;
            },
            onAccept: (Dragable data) {
              setState(() {
                dragAccepted = true;
                acceptedDataIndex = -1;
                if (data is Template &&
                    checkStepWidget(widget.item != null ? widget.item!.type : "rootDefault", data.type) &&
                    checkCategoryBodyListItem(data.category) &&
                    checkPlatformTemplateContainBodyListItem(widget.item, data)) {
                  Item item = Item(data.type, data.category);
                  if (data.type != "client" && widget.item != null) {
                    setClientNameAndPlatform(widget.item!, item);
                  }
                  widget.bodyList.add(item);
                }
                if (data is Item &&
                    checkStepWidget(widget.item != null ? widget.item!.type : "rootDefault", data.type) &&
                    checkCategoryBodyListItem(data.category) &&
                    checkPlatformTemplateContainBodyListItem(widget.item, data)) {
                  if (data.type != "client" && widget.item != null) {
                    setClientNameAndPlatform(widget.item!, data);
                  }
                  if (data.bodyList.length > 1 && !data.bodyList[0].isExpand) {
                    data.bodyList[0].isExpand = true;
                  }
                  if (data.elseBodyList.length > 1 && !data.elseBodyList[0].isExpand) {
                    data.elseBodyList[0].isExpand = true;
                  }
                  if (data.thenBodyList.length > 1 && !data.thenBodyList[0].isExpand) {
                    data.thenBodyList[0].isExpand = true;
                  }
                  widget.bodyList.add(data);
                  Provider.of<CopyWidgetModel>(context, listen: false).setCopyWidget(data);
                }
              });
            },
          ),
        ),
      ],
    );
  }
}
