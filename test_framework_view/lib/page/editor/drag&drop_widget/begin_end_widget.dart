import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import '../../../type.dart';
import 'root_item_widget.dart';

class BeginWidget extends StatelessWidget {
  const BeginWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<TestCaseConfigModel>(
      builder: (context, testCaseConfigModel, child) {
        return Container(
          decoration: BoxDecoration(
              color: const Color.fromRGBO(92, 177, 214, 1),
              // border: Border.all(
              //   color: const Color.fromRGBO(46, 142, 184, 1),
              // ),
              borderRadius: BorderRadius.circular(10)),
          padding: const EdgeInsets.fromLTRB(100, 10, 190, 10),
          child: Row(
            children: [
              Expanded(
                child: Wrap(
                  alignment: WrapAlignment.center,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    MultiLanguageText(
                      name: "begin",
                      defaultValue: "Begin",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: context.fontSizeManager.fontSizeText18,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 1.5,
                          decoration: TextDecoration.none),
                    ),
                    for (var i = 0; i < testCaseConfigModel.testCaseNameList.length; i++)
                      Text(
                        ' ${testCaseConfigModel.testCaseNameList[i]} ${i == testCaseConfigModel.testCaseNameList.length - 1 ? "" : " / "}',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: context.fontSizeManager.fontSizeText18,
                            fontWeight: FontWeight.bold,
                            letterSpacing: 1.5,
                            decoration: TextDecoration.none),
                      ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}

class EndRootWidget extends StatelessWidget {
  final Function callback;
  const EndRootWidget({Key? key, required this.callback}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    late Widget widgetGhost;
    bool willAccept = false;
    return DragTarget<Dragable>(
      builder: (
        BuildContext context,
        List<dynamic> accepted,
        List<dynamic> rejected,
      ) {
        return Column(
          children: [
            if (willAccept)
              Column(
                children: [
                  Opacity(
                    opacity: 0.5,
                    child: widgetGhost,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                ],
              ),
            const EndWidget(),
          ],
        );
      },
      onWillAccept: (data) {
        if (data is Template) {
          if (checkCategoryBodyListItem(data.category)) {
            willAccept = true;
            widgetGhost = data.widget;
          }
        }
        if (data is Item) {
          if (checkCategoryBodyListItem(data.category)) {
            willAccept = true;
            widgetGhost = RootItemWidget(data);
          }
        }
        return true;
      },
      onLeave: (data) {
        willAccept = false;
      },
      onAccept: (Dragable data) {
        willAccept = false;
        if (data is Template) {
          if (checkCategoryBodyListItem(data.category)) {
            Item item = Item(data.type, data.category);
            callback(item);
          }
        }
        if (data is Item) {
          if (checkCategoryBodyListItem(data.category)) {
            if (data.bodyList.length > 1 && !data.bodyList[0].isExpand) {
              data.bodyList[0].isExpand = true;
            }
            if (data.elseBodyList.length > 1 && !data.elseBodyList[0].isExpand) {
              data.elseBodyList[0].isExpand = true;
            }
            if (data.thenBodyList.length > 1 && !data.thenBodyList[0].isExpand) {
              data.thenBodyList[0].isExpand = true;
            }
            callback(data);
            Provider.of<CopyWidgetModel>(context, listen: false).setCopyWidget(data);
          }
        }
      },
    );
  }
}

class EndWidget extends StatelessWidget {
  const EndWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: const Color.fromRGBO(92, 177, 214, 1),
          // border: Border.all(
          //   color: const Color.fromRGBO(46, 142, 184, 1),
          // ),
          borderRadius: BorderRadius.circular(10)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          MultiLanguageText(
            name: "end",
            defaultValue: "End",
            style: TextStyle(color: Colors.white, fontSize: context.fontSizeManager.fontSizeText18, fontWeight: FontWeight.bold, letterSpacing: 4),
          ),
        ],
      ),
    );
  }
}
