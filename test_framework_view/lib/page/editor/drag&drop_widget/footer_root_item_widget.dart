import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/root_item_widget.dart';
import 'package:test_framework_view/type.dart';

import '../../../model/model.dart';

class FooterRootItemWidget extends StatelessWidget {
  final List<Item> bodyList;
  final String footerType;
  final String type;
  final String? platform;
  final Function callbackWidgetGhost;
  const FooterRootItemWidget(this.bodyList, this.footerType, this.type, {Key? key, required this.callbackWidgetGhost, this.platform})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DragTarget<Dragable>(
      builder: (
        BuildContext context,
        List<dynamic> accepted,
        List<dynamic> rejected,
      ) {
        return (footerType != "footer")
            ? Container(
                constraints: const BoxConstraints(minHeight: 35, minWidth: 220),
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(255, 171, 25, 1),
                  // border: Border.all(
                  //   color: const Color.fromRGBO(207, 139, 23, 1),
                  // ),
                  borderRadius: BorderRadius.circular(10),
                ),
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    footerType,
                    style: Style(context).textDragAndDropWidget,
                  ),
                ),
              )
            : Container(
                constraints: const BoxConstraints(minHeight: 35, minWidth: 220),
                decoration: BoxDecoration(
                  color: (type == "client")
                      ? const Color.fromARGB(255, 205, 167, 206)
                      : (type == "group" || type == "step")
                          ? const Color.fromARGB(255, 86, 112, 111)
                          : const Color.fromRGBO(255, 171, 25, 1),
                  // border: Border.all(
                  //   color: (type == "group") ? const Color.fromARGB(255, 86, 112, 111) : const Color.fromRGBO(207, 139, 23, 1),
                  // ),
                  borderRadius: BorderRadius.circular(10),
                ),
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Align(
                    alignment: Alignment.centerLeft,
                    child: MultiLanguageText(
                      name: type == "condition"
                          ? "end_if"
                          : type == "try"
                              ? "end_try"
                              : type == "forEach"
                                  ? "end_foreach"
                                  : type == "while"
                                      ? "end_while"
                                      : type == "group"
                                          ? "end_group"
                                          : type == "client"
                                              ? "end_client"
                                              : type == "step"
                                                  ? "end_flow"
                                                  : "",
                      defaultValue: type == "condition"
                          ? "End If"
                          : type == "try"
                              ? "End Try"
                              : type == "forEach"
                                  ? "End Foreach"
                                  : type == "while"
                                      ? "End While"
                                      : type == "group"
                                          ? "End Group"
                                          : type == "client"
                                              ? "End Client"
                                              : type == "step"
                                                  ? "End Flow"
                                                  : "",
                      overflow: TextOverflow.clip,
                      style: Style(context).textDragAndDropWidget,
                    )));
      },
      onWillAccept: (data) {
        if (data is Template && checkCategoryBodyListItem(data.category) && checkPlatformTemplateContainFooterItem(platform, data)) {
          callbackWidgetGhost(true, data.widget);
        }
        if (data is Item && checkCategoryBodyListItem(data.category) && checkPlatformTemplateContainFooterItem(platform, data)) {
          callbackWidgetGhost(true, RootItemWidget(data));
        }
        return true;
      },
      onLeave: (data) {
        callbackWidgetGhost(false, const SizedBox.shrink());
      },
      onAccept: (Dragable data) {
        if (data is Template && checkCategoryBodyListItem(data.category)) {
          Item item = Item(data.type, data.category);
          bodyList.add(item);
        }
        if (data is Item && checkCategoryBodyListItem(data.category)) {
          if (data.bodyList.length > 1 && !data.bodyList[0].isExpand) {
            data.bodyList[0].isExpand = true;
          }
          if (data.elseBodyList.length > 1 && !data.elseBodyList[0].isExpand) {
            data.elseBodyList[0].isExpand = true;
          }
          if (data.thenBodyList.length > 1 && !data.thenBodyList[0].isExpand) {
            data.thenBodyList[0].isExpand = true;
          }
          bodyList.add(data);
          Provider.of<CopyWidgetModel>(context, listen: false).setCopyWidget(data);
        }
        callbackWidgetGhost(false, const SizedBox.shrink());
      },
    );
  }
}
