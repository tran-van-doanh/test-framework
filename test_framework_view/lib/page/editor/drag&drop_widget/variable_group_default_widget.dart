import 'package:flutter/material.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/root_item_widget.dart';
import 'package:test_framework_view/page/editor/property_widget/variable_default_property_widget.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';
import '../../../common/custom_state.dart';
import '../../../type.dart';

class VariableGroupDefaultWidget extends StatefulWidget {
  final Item item;
  final Function callback;
  final String? parentType;
  final bool hasClick;
  final bool isDragAndDrop;
  const VariableGroupDefaultWidget(this.item, this.callback, {Key? key, this.parentType, required this.hasClick, required this.isDragAndDrop})
      : super(key: key);

  @override
  State<VariableGroupDefaultWidget> createState() => _VariableGroupDefaultWidgetState();
}

class _VariableGroupDefaultWidgetState extends CustomState<VariableGroupDefaultWidget> {
  late Widget widgetGhost;
  bool willAccept = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (widget.hasClick)
          ? () {
              Navigator.of(context).push(
                createRoute(
                  variableGroupDefaultPropertyWidget(),
                ),
              );
            }
          : null,
      child: DragTarget<Dragable>(
        builder: (
          BuildContext context,
          List<dynamic> accepted,
          List<dynamic> rejected,
        ) {
          return willAccept
              ? IntrinsicWidth(
                  child: Opacity(
                    opacity: 0.5,
                    child: widgetGhost,
                  ),
                )
              : Container(
                  padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey),
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: (widget.item.variablePath.isNotEmpty && widget.item.variablePath[0].type != "variableGroup_default")
                      ? Wrap(
                          children: [
                            for (var variablePath in widget.item.variablePath)
                              Wrap(
                                children: [
                                  Text(
                                    (variablePath.type == "variable" ||
                                            variablePath.type == "version_variable" ||
                                            variablePath.type == "environment_variable" ||
                                            variablePath.type == "node_variable" ||
                                            variablePath.type == "get")
                                        ? variablePath.name ?? "null"
                                        : variablePath.value ?? "empty",
                                    style: TextStyle(
                                        fontSize: context.fontSizeManager.fontSizeText14,
                                        color: (variablePath.name == null && variablePath.value == null) ? Colors.red : null),
                                  ),
                                  if (widget.item.variablePath.last != variablePath)
                                    Text(
                                      ".",
                                      style: TextStyle(fontSize: context.fontSizeManager.fontSizeText14),
                                    ),
                                ],
                              ),
                          ],
                        )
                      : Text(
                          "\$",
                          style: TextStyle(
                              fontSize: context.fontSizeManager.fontSizeText14,
                              decoration: TextDecoration.none,
                              color: const Color.fromRGBO(49, 49, 49, 1)),
                        ),
                );
        },
        onWillAccept: (data) {
          setState(() {
            if (data is Template && data.category == "variable") {
              willAccept = true;
              widgetGhost = data.widget;
            }
            if (data is Item && data.category == "variable") {
              willAccept = true;
              widgetGhost = RootItemWidget(data);
            }
          });
          return true;
        },
        onLeave: (data) {
          setState(() {
            willAccept = false;
          });
        },
        onAccept: (Dragable data) {
          setState(() {
            if (data is Template && data.category == "variable") {
              Item item = Item(data.type, data.category);
              willAccept = false;
              widget.callback(item);
            }
            if (data is Item && data.category == "variable") {
              willAccept = false;
              widget.callback(data);
            }
          });
        },
      ),
    );
  }

  Widget variableGroupDefaultPropertyWidget() {
    return TestFrameworkRootPageWidget(
      child: VariableDefaultProperty(
        parentType: widget.item.type,
        variablePath: widget.item.variablePath,
        callback: (List<Item> variablePathList) {
          setState(() {
            widget.item.variablePath = variablePathList;
          });
        },
        isDragAndDrop: widget.isDragAndDrop,
      ),
    );
  }
}
