import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/custom_draggable.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/extension/hex_color.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/root_item_widget.dart';
import 'package:test_framework_view/page/editor/property_widget/property_page_widget.dart';
import 'package:test_framework_view/page/editor/template_list_widget/condition_variable_default_template.dart';
import 'package:test_framework_view/type.dart';

import '../../../model/model.dart';

class ConditionGroupWidget extends StatelessWidget {
  final Item item;
  final Function callback;
  final bool hasClick;
  final bool isDragAndDrop;
  const ConditionGroupWidget(this.item, this.callback, {Key? key, required this.hasClick, required this.isDragAndDrop}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer2<EditorNotificationModel, TestCaseConfigModel>(
      builder: (context, editorNotificationModel, testCaseConfigModel, child) {
        Widget btnAddConditionWidget() {
          return Wrap(
            alignment: WrapAlignment.center,
            crossAxisAlignment: WrapCrossAlignment.center,
            children: [
              const SizedBox(width: 10),
              Material(
                borderRadius: BorderRadius.circular(7),
                child: InkWell(
                  onTap: () {
                    item.conditionList.add(Item("condition_default", "condition_default"));
                    editorNotificationModel.updateNotification();
                  },
                  child: Container(
                      height: 30,
                      width: 30,
                      decoration: BoxDecoration(
                        color: HexColor.fromHex(testCaseConfigModel.editorComponentList
                                .firstWhere((element) => element["type"] == item.type)["componentConfig"]["borderColor"] ??
                            "#FF389438"),
                        borderRadius: BorderRadius.circular(7),
                      ),
                      child: const Center(
                        child: Icon(
                          Icons.add,
                          color: Colors.white,
                          size: 20,
                        ),
                      )),
                ),
              ),
            ],
          );
        }

        Widget conditionGroupItemWidget(int i) {
          return Wrap(
            alignment: WrapAlignment.center,
            crossAxisAlignment: WrapCrossAlignment.center,
            children: [
              if (i == 0 && (item.type == "NOT" || item.type == "assertTrue" || item.type == "assertFalse"))
                Wrap(
                  alignment: WrapAlignment.center,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    MultiLanguageText(
                      name: item.operatorType!.toLowerCase().replaceAll(' ', '_'),
                      defaultValue: item.operatorType!,
                      style: Style(context).textDragAndDropWidget,
                    ),
                    const SizedBox(width: 10)
                  ],
                ),
              if (i != 0)
                Wrap(
                  alignment: WrapAlignment.center,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    const SizedBox(width: 10),
                    MultiLanguageText(
                      name: item.operatorType!.toLowerCase().replaceAll(' ', '_'),
                      defaultValue: item.operatorType!,
                      style: Style(context).textDragAndDropWidget,
                    ),
                    const SizedBox(width: 10)
                  ],
                ),
              RootItemWidget(
                item.conditionList[i],
                isDragAndDrop: isDragAndDrop,
              ),
            ],
          );
        }

        Container conditionGroupBodyWidget() {
          return Container(
            constraints: const BoxConstraints(minHeight: 35),
            decoration: BoxDecoration(
              color: HexColor.fromHex(testCaseConfigModel.editorComponentList.firstWhere((element) => element["type"] == item.type)["componentConfig"]
                      ["color"] ??
                  "#FF59C059"),
              border: Border.all(
                color: HexColor.fromHex(
                    testCaseConfigModel.editorComponentList.firstWhere((element) => element["type"] == item.type)["componentConfig"]["borderColor"] ??
                        "#FF389438"),
              ),
              borderRadius: BorderRadius.circular(10),
            ),
            padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
            child: Wrap(
              alignment: WrapAlignment.center,
              crossAxisAlignment: WrapCrossAlignment.center,
              children: [
                for (var i = 0; i < item.conditionList.length; i++) conditionGroupItemWidget(i),
                if (item.type != "NOT" && item.type != "assertTrue" && item.type != "assertFalse")
                  AbsorbPointer(absorbing: !isDragAndDrop, child: btnAddConditionWidget()),
              ],
            ),
          );
        }

        return GestureDetector(
          onTap: (hasClick)
              ? () {
                  Navigator.of(context).push(
                    createRoute(
                      PropertyPageWidget(
                        item: item,
                        callback: () {
                          editorNotificationModel.updateNotification();
                        },
                        isDragAndDrop: isDragAndDrop,
                      ),
                    ),
                  );
                }
              : null,
          child: isDragAndDrop
              ? CustomDraggable(
                  data: item,
                  childWhenDragging: const ConditionDefaultTemplate(),
                  feedback: RootItemWidget(
                    item,
                  ),
                  onDragCompleted: () {
                    callback(item.category);
                  },
                  child: conditionGroupBodyWidget(),
                )
              : conditionGroupBodyWidget(),
        );
      },
    );
  }
}
