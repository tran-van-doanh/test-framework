import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/action_widget.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/assert_widget.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/condition_group_default_widget.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/condition_group_widget.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/condition_widget.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/control_widget.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/variable_condition_default_widget.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/variable_group_default_widget.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/variable_widget.dart';
import '../../../common/custom_state.dart';
import '../../../type.dart';

class RootItemWidget extends StatefulWidget {
  final Item item;
  final Item? parentItem;
  final int? index;
  final String? parentType;
  final VoidCallback? callback;
  final Function? onViewConfig;
  final Function? onDuplicate;
  final String? tfTestCaseId;
  final bool hasGlobalKey;
  final bool hasPlay;
  final bool hasCopy;
  final bool hasDelete;
  final bool hasDuplicate;
  final bool hasClick;
  final bool hasCheckBox;
  final VoidCallback? callbackChecked;
  final bool isDragAndDrop;
  final bool isFeedbackDrag;
  const RootItemWidget(
    this.item, {
    Key? key,
    this.index,
    this.callback,
    this.parentType,
    this.tfTestCaseId,
    this.hasPlay = true,
    this.hasCopy = true,
    this.hasDelete = true,
    this.hasDuplicate = true,
    this.hasClick = true,
    this.hasCheckBox = false,
    this.parentItem,
    this.callbackChecked,
    this.isDragAndDrop = true,
    this.isFeedbackDrag = false,
    this.hasGlobalKey = false,
    this.onDuplicate,
    this.onViewConfig,
  }) : super(key: key);

  @override
  State<RootItemWidget> createState() => _RootItemWidgetState();
}

class _RootItemWidgetState extends CustomState<RootItemWidget> {
  @override
  Widget build(BuildContext context) {
    if (widget.item.type == "conditionGroup_default") {
      widget.item.category = "conditionGroup_default";
    }
    if (widget.item.type == "variableGroup_default") {
      widget.item.category = "variableGroup_default";
    }
    if (widget.item.type == "variable_default") {
      widget.item.category = "variable_default";
    }
    if (widget.item.type == "condition_default") {
      widget.item.category = "condition_default";
    }
    if (widget.item.type == "value" ||
        widget.item.type == "variable" ||
        widget.item.type == "version_variable" ||
        widget.item.type == "environment_variable" ||
        widget.item.type == "node_variable" ||
        widget.item.type == "get") {
      widget.item.category = "variablePath";
    }
    if (widget.item.type == "contains") {
      widget.item.category = "condition";
      widget.item.operatorType = "contains";
    }
    if (widget.item.type == "matches") {
      widget.item.category = "condition";
      widget.item.operatorType = "matches";
    }
    if (widget.item.type == "checkWindowExists") {
      widget.item.category = "condition";
      widget.item.operatorType = "Check Window Exists";
    }
    if (widget.item.type == "checkElementExists") {
      widget.item.category = "condition";
      widget.item.operatorType = "Check Element Exists";
    }
    if (widget.item.type == "checkAlertExists") {
      widget.item.category = "condition";
      widget.item.operatorType = "Check Alert Exists";
    }
    if (widget.item.type == "childElementCheckElementExists") {
      widget.item.category = "condition";
      widget.item.operatorType = "Child Element Check Element Exists";
    }
    if (widget.item.type == "checkElementEnabled") {
      widget.item.category = "condition";
      widget.item.operatorType = "Check Element Enabled";
    }
    if (widget.item.type == "childElementCheckElementEnabled") {
      widget.item.category = "condition";
      widget.item.operatorType = "Child Element Check Element Enabled";
    }
    if (widget.item.type == "isNull") {
      widget.item.category = "condition";
      widget.item.operatorType = "Is Null";
    }
    if (widget.item.type == "isNotNull") {
      widget.item.category = "condition";
      widget.item.operatorType = "Is Not Null";
    }
    if (widget.item.type == "isEmpty") {
      widget.item.category = "condition";
      widget.item.operatorType = "Is Empty";
    }
    if (widget.item.type == "isNotEmpty") {
      widget.item.category = "condition";
      widget.item.operatorType = "Is Not Empty";
    }
    if (widget.item.type == "gt") {
      widget.item.category = "condition";
      widget.item.operatorType = ">";
    }
    if (widget.item.type == "gte") {
      widget.item.category = "condition";
      widget.item.operatorType = ">=";
    }
    if (widget.item.type == "lt") {
      widget.item.category = "condition";
      widget.item.operatorType = "<";
    }
    if (widget.item.type == "lte") {
      widget.item.category = "condition";
      widget.item.operatorType = "<=";
    }
    if (widget.item.type == "eq") {
      widget.item.category = "condition";
      widget.item.operatorType = "==";
    }
    if (widget.item.type == "ne") {
      widget.item.category = "condition";
      widget.item.operatorType = "!=";
    }
    if (widget.item.type == "AND") {
      widget.item.category = "conditionGroup";
      widget.item.operatorType = "AND";
    }
    if (widget.item.type == "OR") {
      widget.item.category = "conditionGroup";
      widget.item.operatorType = "OR";
    }
    if (widget.item.type == "NOT") {
      widget.item.category = "conditionGroup";
      widget.item.operatorType = "NOT";
    }
    if (widget.item.type == "add") {
      widget.item.category = "variable";
      widget.item.operatorType = "+";
    }
    if (widget.item.type == "sub") {
      widget.item.category = "variable";
      widget.item.operatorType = "-";
    }
    if (widget.item.type == "mul") {
      widget.item.category = "variable";
      widget.item.operatorType = "x";
    }
    if (widget.item.type == "div") {
      widget.item.category = "variable";
      widget.item.operatorType = "/";
    }
    if (widget.item.category == "control" || widget.item.type == "group" || widget.item.type == "client" || widget.item.type == "step") {
      return ControlWidget(
        widget.item,
        widget.index != null ? widget.index! : 0,
        onDelete: () => widget.callback!(),
        onViewConfig: (testCaseType, testCaseId) {
          widget.onViewConfig!(testCaseType, testCaseId);
        },
        tfTestCaseId: widget.tfTestCaseId,
        hasGlobalKey: widget.hasGlobalKey,
        hasClick: widget.hasClick,
        hasCopy: !widget.isDragAndDrop ? false : widget.hasCopy,
        hasDelete: !widget.isDragAndDrop ? false : widget.hasDelete,
        hasCheckBox: widget.hasCheckBox,
        onChecked: () {
          if (widget.parentItem!.isSelected != true) {
            widget.parentItem!.isSelected = true;
            Provider.of<ListElementPlayIdModel>(context, listen: false).addElementPlayIdList(widget.parentItem!.id, true);
            widget.callbackChecked!();
          }
        },
        isDragAndDrop: widget.isDragAndDrop,
        key: (!widget.isFeedbackDrag && widget.hasGlobalKey) ? widget.item.globalKey : null,
        onDuplicate: (item) => widget.onDuplicate!(item),
        hasDuplicate: !widget.isDragAndDrop ? false : widget.hasDuplicate,
      );
    }
    if (widget.item.category == "action" || widget.item.type == "comment" || widget.item.category == "file") {
      return ActionWidget(
        widget.item,
        widget.index != null ? widget.index! : 0,
        onDelete: () => widget.callback!(),
        onViewConfig: (testCaseType, testCaseId) {
          widget.onViewConfig!(testCaseType, testCaseId);
        },
        tfTestCaseId: widget.tfTestCaseId,
        hasClick: widget.hasClick,
        hasViewConfig: widget.isDragAndDrop,
        hasCopy: !widget.isDragAndDrop ? false : widget.hasCopy,
        hasDelete: !widget.isDragAndDrop ? false : widget.hasDelete,
        hasPlay: !widget.isDragAndDrop ? false : widget.hasPlay,
        hasCheckBox: widget.hasCheckBox,
        isDragAndDrop: widget.isDragAndDrop,
        onChecked: () {
          if (widget.parentItem!.isSelected != true) {
            widget.parentItem!.isSelected = true;
            Provider.of<ListElementPlayIdModel>(context, listen: false).addElementPlayIdList(widget.parentItem!.id, true);
            widget.callbackChecked!();
          }
        },
        key: (!widget.isFeedbackDrag && widget.hasGlobalKey) ? widget.item.globalKey : Key(widget.item.globalKey.toString()),
        onDuplicate: (item) => widget.onDuplicate!(item),
        hasDuplicate: !widget.isDragAndDrop ? false : widget.hasDuplicate,
      );
    }
    if (widget.item.category == "assert") {
      return AssertWidget(
        widget.item,
        widget.index != null ? widget.index! : 0,
        onDelete: () => widget.callback!(),
        onViewConfig: (testCaseType, testCaseId) {
          widget.onViewConfig!(testCaseType, testCaseId);
        },
        tfTestCaseId: widget.tfTestCaseId,
        hasClick: widget.hasClick,
        hasViewConfig: widget.isDragAndDrop,
        hasCopy: !widget.isDragAndDrop ? false : widget.hasCopy,
        hasDelete: !widget.isDragAndDrop ? false : widget.hasDelete,
        hasPlay: !widget.isDragAndDrop ? false : widget.hasPlay,
        hasCheckBox: widget.hasCheckBox,
        isDragAndDrop: widget.isDragAndDrop,
        onChecked: () {
          if (widget.parentItem!.isSelected != true) {
            widget.parentItem!.isSelected = true;
            Provider.of<ListElementPlayIdModel>(context, listen: false).addElementPlayIdList(widget.parentItem!.id, true);
            widget.callbackChecked!();
          }
        },
        key: (!widget.isFeedbackDrag && widget.hasGlobalKey) ? widget.item.globalKey : Key(widget.item.globalKey.toString()),
        onDuplicate: (item) => widget.onDuplicate!(item),
        hasDuplicate: !widget.isDragAndDrop ? false : widget.hasDuplicate,
      );
    }
    if (widget.item.type == "AND" || widget.item.type == "OR" || widget.item.type == "NOT") {
      return ConditionGroupWidget(
        widget.item,
        (String categoryCallBack) {
          setState(() {
            if (widget.parentType == "condition" || widget.parentType == "while") {
              widget.item.type = "conditionGroup_default";
              widget.item.category = "conditionGroup_default";
              widget.item.conditionList = [];
              widget.item.operatorType = "";
            } else {
              widget.item.type = "condition_default";
              widget.item.category = "condition_default";
              widget.item.conditionList = [];
              widget.item.operatorType = "";
            }
          });
        },
        hasClick: widget.hasClick,
        isDragAndDrop: widget.isDragAndDrop,
      );
    }
    if (widget.item.category == "variable") {
      return VariableWidget(
        widget.item,
        () {
          setState(() {
            if (widget.parentType == "forEach" ||
                widget.parentType == "newVariable" ||
                widget.parentType == "assignValue" ||
                widget.parentType == "sleep" ||
                widget.parentType == "error") {
              widget.callback!();
            } else {
              widget.item.type = "variable_default";
              widget.item.category = "variable_default";
            }
            widget.item.operatorList = [];
            widget.item.operatorType = "";
          });
          setState(() {});
        },
        hasClick: widget.hasClick,
        isDragAndDrop: widget.isDragAndDrop,
      );
    }
    if (widget.item.category == "condition") {
      return ConditionWidget(
        widget.item,
        (String categoryCallBack) {
          setState(() {
            widget.item.type = "condition_default";
            widget.item.category = "condition_default";
            widget.item.conditionList = [];
            widget.item.operatorType = "";
          });
        },
        hasClick: widget.hasClick,
        isDragAndDrop: widget.isDragAndDrop,
      );
    }
    if (widget.item.type == "variableGroup_default") {
      return VariableGroupDefaultWidget(
        widget.item,
        (Item itemCallBack) {
          widget.item.variablePath[0].type = itemCallBack.type;
          widget.item.variablePath[0].operatorList = itemCallBack.operatorList;
          setState(() {});
        },
        hasClick: widget.hasClick,
        isDragAndDrop: widget.isDragAndDrop,
      );
    }
    if (widget.item.type == "conditionGroup_default") {
      return ConditionGroupDefaultWidget(
        widget.item,
        (Item itemCallBack) {
          setState(() {
            widget.item.type = itemCallBack.type;
            widget.item.conditionList = itemCallBack.conditionList;
          });
        },
      );
    }
    if (widget.item.type == "variable_default" || widget.item.type == "condition_default") {
      return VariableConditionDefaultWidget(
        widget.item,
        (Item itemCallBack) {
          setState(() {
            widget.item.type = itemCallBack.type;
            widget.item.category = itemCallBack.category;
            widget.item.operatorType = itemCallBack.operatorType;
            widget.item.platform = itemCallBack.platform;
            widget.item.clientName = itemCallBack.clientName;
            widget.item.description = itemCallBack.description;
            widget.item.parameterList = itemCallBack.parameterList;
            if (itemCallBack.type == "AND" ||
                itemCallBack.type == "OR" ||
                itemCallBack.type == "NOT" ||
                itemCallBack.type == "assertTrue" ||
                itemCallBack.type == "assertFalse") {
              widget.item.conditionList = itemCallBack.conditionList;
            } else if (itemCallBack.category == "variable") {
              widget.item.operatorList = itemCallBack.operatorList;
            } else {
              widget.item.o1 = itemCallBack.o1;
              widget.item.o2 = itemCallBack.o2;
            }
          });
        },
        hasClick: widget.hasClick,
        isDragAndDrop: widget.isDragAndDrop,
      );
    }
    if (widget.item.type == "value" ||
        widget.item.type == "variable" ||
        widget.item.type == "version_variable" ||
        widget.item.type == "environment_variable" ||
        widget.item.type == "node_variable" ||
        widget.item.type == "get") {
      return ValuePathWidget(
        widget.item,
        hasClick: widget.hasClick,
        isDragAndDrop: widget.isDragAndDrop,
      );
    }
    return MultiLanguageText(
      name: "type_not_implemented",
      defaultValue: "\$0 not implemented",
      variables: [widget.item.type],
      style: TextStyle(fontSize: context.fontSizeManager.fontSizeText14),
    );
  }
}
