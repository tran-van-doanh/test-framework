import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/root_item_widget.dart';
import 'package:test_framework_view/page/editor/property_widget/property_page_widget.dart';
import 'package:test_framework_view/page/editor/template_list_widget/condition_variable_default_template.dart';
import '../../../common/custom_state.dart';
import '../../../model/model.dart';
import '../../../type.dart';

class VariableConditionDefaultWidget extends StatefulWidget {
  final Item item;
  final Function callback;
  final bool hasClick;
  final bool isDragAndDrop;
  const VariableConditionDefaultWidget(this.item, this.callback, {Key? key, required this.hasClick, required this.isDragAndDrop}) : super(key: key);

  @override
  State<VariableConditionDefaultWidget> createState() => _VariableConditionDefaultWidgetState();
}

class _VariableConditionDefaultWidgetState extends CustomState<VariableConditionDefaultWidget> {
  late Widget widgetGhost;
  bool willAccept = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (widget.hasClick && widget.item.type != "condition_default") {
          Navigator.of(context).push(
            createRoute(
              PropertyPageWidget(
                item: widget.item,
                isDragAndDrop: widget.isDragAndDrop,
                callback: (variablePathList) {
                  setState(() {
                    widget.item.variablePath = variablePathList;
                  });
                },
              ),
            ),
          );
        }
      },
      child: DragTarget<Dragable>(
        builder: (
          BuildContext context,
          List<dynamic> accepted,
          List<dynamic> rejected,
        ) {
          return willAccept
              ? IntrinsicWidth(
                  child: Opacity(
                    opacity: 0.5,
                    child: widgetGhost,
                  ),
                )
              : widget.item.type == "variable_default"
                  ? Container(
                      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey),
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(16),
                      ),
                      child: widget.item.variablePath.isNotEmpty
                          ? Wrap(
                              children: [
                                for (var variablePath in widget.item.variablePath)
                                  Wrap(
                                    children: [
                                      Text(
                                        (variablePath.type == "variable" ||
                                                variablePath.type == "version_variable" ||
                                                variablePath.type == "environment_variable" ||
                                                variablePath.type == "node_variable" ||
                                                variablePath.type == "get")
                                            ? variablePath.name ?? "\$"
                                            : variablePath.value ?? "\$",
                                        style: TextStyle(fontSize: context.fontSizeManager.fontSizeText14),
                                      ),
                                      if (widget.item.variablePath.last != variablePath)
                                        Text(
                                          ".",
                                          style: TextStyle(fontSize: context.fontSizeManager.fontSizeText14),
                                        ),
                                    ],
                                  ),
                              ],
                            )
                          : Text(
                              "\$",
                              style: TextStyle(
                                  fontSize: context.fontSizeManager.fontSizeText14,
                                  decoration: TextDecoration.none,
                                  color: const Color.fromRGBO(49, 49, 49, 1)),
                            ),
                    )
                  : const ConditionDefaultTemplate();
        },
        onWillAccept: (data) {
          setState(() {
            if (data is Template &&
                ((data.category == "variable" && widget.item.type == "variable_default") ||
                    ((data.category == "condition" || data.category == "conditionGroup") && widget.item.type == "condition_default"))) {
              willAccept = true;
              widgetGhost = data.widget;
            }
            if (data is Item &&
                ((data.category == "variable" && widget.item.type == "variable_default") ||
                    ((data.category == "condition" || data.category == "conditionGroup") && widget.item.type == "condition_default"))) {
              willAccept = true;
              widgetGhost = RootItemWidget(data);
            }
          });
          return true;
        },
        onLeave: (data) {
          setState(() {
            willAccept = false;
          });
        },
        onAccept: (Dragable data) {
          setState(() {
            if (data is Template &&
                ((data.category == "variable" && widget.item.type == "variable_default") ||
                    ((data.category == "condition" || data.category == "conditionGroup") && widget.item.type == "condition_default"))) {
              Item item = Item(data.type, data.category);
              willAccept = false;
              widget.callback(item);
            }
            if (data is Item &&
                ((data.category == "variable" && widget.item.type == "variable_default") ||
                    ((data.category == "condition" || data.category == "conditionGroup") && widget.item.type == "condition_default"))) {
              willAccept = false;
              widget.callback(data);
            }
          });
        },
      ),
    );
  }
}

class ValuePathWidget extends StatelessWidget {
  final Item item;
  final bool hasClick;
  final bool isDragAndDrop;
  const ValuePathWidget(this.item, {Key? key, required this.hasClick, required this.isDragAndDrop}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<EditorNotificationModel>(builder: (context, editorNotificationModel, child) {
      return GestureDetector(
        onTap: (hasClick)
            ? () {
                Navigator.of(context).push(
                  createRoute(
                    PropertyPageWidget(
                      item: item,
                      isDragAndDrop: isDragAndDrop,
                      callback: () {
                        editorNotificationModel.updateNotification();
                      },
                    ),
                  ),
                );
              }
            : null,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 3),
          child: Wrap(
            children: [
              item.type == "value"
                  ? Text(
                      item.value ?? "1",
                      style: TextStyle(
                          fontSize: context.fontSizeManager.fontSizeText14,
                          decoration: TextDecoration.none,
                          color: const Color.fromRGBO(49, 49, 49, 1)),
                    )
                  : Text(
                      item.name ?? "1",
                      style: TextStyle(
                          fontSize: context.fontSizeManager.fontSizeText14,
                          decoration: TextDecoration.none,
                          color: const Color.fromRGBO(49, 49, 49, 1)),
                    ),
            ],
          ),
        ),
      );
    });
  }
}
