import 'package:flutter/material.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/extension/hex_color.dart';
import 'package:test_framework_view/page/editor/template_list_widget/condition_variable_default_template.dart';

class ActionTemplate extends StatelessWidget {
  final String type;
  final String title;
  final List platformsList;
  final dynamic componentConfig;
  const ActionTemplate(this.type, {Key? key, required this.platformsList, required this.title, this.componentConfig}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (type == "actionCall" ||
        type == "testCaseCall" ||
        type == "apiCall" ||
        type == "sendKeys" ||
        type == "childElementSendKeys" ||
        type == "clearAndSendKeys" ||
        type == "childElementClearAndSendKeys" ||
        type == "sendKeyFromElement" ||
        type == "childElementSendKeyFromElement" ||
        type == "sendKeyWithoutElement" ||
        type == "sendKeysWithoutElement" ||
        type == "clearAndSendFile" ||
        type == "childElementClearAndSendFile" ||
        type == "startApplication" ||
        type == "installApplication" ||
        type == "mouseAction" ||
        type == "mouseActionWithOffset" ||
        type == "click" ||
        type == "switchToFrame" ||
        type == "clickByPosition" ||
        type == "childElementClick" ||
        type == "selectTableRow" ||
        type == "selectByValue" ||
        type == "getSelectedValue" ||
        type == "getSelectOptionsValue" ||
        type == "getSelectVisibleOptionsValue" ||
        type == "getSelectedVisibleValue" ||
        type == "childElementSelectByValue" ||
        type == "selectByVisibleText" ||
        type == "childElementSelectByVisibleText" ||
        type == "findElements" ||
        type == "childElementFindElements" ||
        type == "findElement" ||
        type == "findElementByPosition" ||
        type == "sendKeysByPosition" ||
        type == "clearAndSendKeysByPosition" ||
        type == "selectByValueByPosition" ||
        type == "selectByVisibleTextByPosition" ||
        type == "findElementsByPosition" ||
        type == "childElementFindElement" ||
        type == "getText" ||
        type == "childElementGetText" ||
        type == "getAttribute" ||
        type == "childElementGetAttribute" ||
        type == "goTo" ||
        type == "getUrl" ||
        type == "executeScript" ||
        type == "executeScriptOnElement" ||
        type == "waitUrl" ||
        type == "waitElementExists" ||
        type == "waitElementDisappear" ||
        type == "waitRepositoryElementDisappear" ||
        type == "waitRepositoryElementExists" ||
        type == "childElementWaitElement" ||
        type == "clickWithOffset" ||
        type == "childElementClickWithOffset" ||
        type == "getAlertText" ||
        type == "acceptAlert" ||
        type == "sendKeysAlert" ||
        type == "dismissAlert" ||
        type == "getWindowHandle" ||
        type == "getWindowHandles" ||
        type == "selectWindow" ||
        type == "closeWindow" ||
        type == "applicationRefresh" ||
        type == "bringForeground" ||
        type == "switchToDefaultContent" ||
        type == "switchToWindow" ||
        type == "switchToNextWindow" ||
        type == "switchToPreviousWindow" ||
        type == "ftpUpload" ||
        type == "ftpDownload" ||
        type == "ftpWaitFile" ||
        type == "sftpUpload" ||
        type == "sftpDownload" ||
        type == "sftpWaitFile" ||
        type == "textFileRead" ||
        type == "textFileWrite" ||
        type == "excelFileRead" ||
        type == "excelFileWrite" ||
        type == "jsonFileRead" ||
        type == "jsonFileWrite") {
      return Row(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.fromLTRB(10, 10, 30, 10),
            decoration: BoxDecoration(
              color: HexColor.fromHex(componentConfig["color"] ?? "#FF59C059"),
              border: componentConfig["borderColor"] != null
                  ? Border.all(
                      color: HexColor.fromHex(componentConfig["borderColor"]),
                    )
                  : null,
              borderRadius: BorderRadius.circular(10),
            ),
            constraints: const BoxConstraints(maxWidth: 220),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MultiLanguageText(
                  name: type.toLowerCase(),
                  defaultValue: title,
                  overflow: TextOverflow.clip,
                  style: Style(context).textDragAndDropWidget,
                ),
                if (platformsList.isNotEmpty)
                  RichText(
                    text: TextSpan(
                      style: TextStyle(fontSize: context.fontSizeManager.fontSizeText12, color: Colors.white),
                      text: multiLanguageString(name: "platform_suffix", defaultValue: "Platform: ", context: context),
                      children: <TextSpan>[
                        for (var platform in platformsList)
                          TextSpan(
                            text: "$platform${platform == platformsList.last ? "" : ", "}",
                          ),
                      ],
                    ),
                  )
              ],
            ),
          ),
        ],
      );
    }
    if (type == "replaceAll" ||
        type == "substring" ||
        type == "split" ||
        type == "format" ||
        type == "datePlus" ||
        type == "dateMinus" ||
        type == "parseInteger" ||
        type == "parseLong" ||
        type == "parseDouble" ||
        type == "parseFloat" ||
        type == "parseDate" ||
        type == "parseDateTime") {
      return Row(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.fromLTRB(10, 10, 30, 10),
            decoration: BoxDecoration(
              color: const Color.fromRGBO(255, 140, 26, 1),
              border: Border.all(
                color: const Color.fromRGBO(219, 110, 0, 1),
              ),
              borderRadius: BorderRadius.circular(10),
            ),
            constraints: const BoxConstraints(maxWidth: 220),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MultiLanguageText(
                  name: type.toLowerCase(),
                  defaultValue: title,
                  overflow: TextOverflow.clip,
                  style: Style(context).textDragAndDropWidget,
                ),
                if (platformsList.isNotEmpty)
                  RichText(
                    text: TextSpan(
                      style: TextStyle(fontSize: context.fontSizeManager.fontSizeText12, color: Colors.white),
                      text: multiLanguageString(name: "platform_suffix", defaultValue: "Platform: ", context: context),
                      children: <TextSpan>[
                        for (var platform in platformsList)
                          TextSpan(
                            text: "$platform${platform == platformsList.last ? "" : ", "}",
                          ),
                      ],
                    ),
                  )
              ],
            ),
          ),
        ],
      );
    }
    if (type == "newVariable" || type == "assignValue" || type == "sleep") {
      return Row(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.fromLTRB(10, 5, 30, 5),
            decoration: BoxDecoration(
              color: const Color.fromRGBO(255, 140, 26, 1),
              border: Border.all(
                color: const Color.fromRGBO(219, 110, 0, 1),
              ),
              borderRadius: BorderRadius.circular(10),
            ),
            constraints: const BoxConstraints(maxWidth: 220),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    MultiLanguageText(
                      name: type.toLowerCase(),
                      defaultValue: title,
                      overflow: TextOverflow.clip,
                      style: Style(context).textDragAndDropWidget,
                    ),
                    const SizedBox(width: 10),
                    const VariableDefaultTemplate()
                  ],
                ),
                if (platformsList.isNotEmpty)
                  RichText(
                    text: TextSpan(
                      style: TextStyle(fontSize: context.fontSizeManager.fontSizeText12, color: Colors.white),
                      text: multiLanguageString(name: "platform_suffix", defaultValue: "Platform: ", context: context),
                      children: <TextSpan>[
                        for (var platform in platformsList)
                          TextSpan(
                            text: "$platform${platform == platformsList.last ? "" : ", "}",
                          ),
                      ],
                    ),
                  )
              ],
            ),
          ),
        ],
      );
    }
    if (type == "break" || type == "continue") {
      return Row(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.fromLTRB(10, 10, 30, 10),
            decoration: BoxDecoration(
              color: const Color.fromRGBO(255, 102, 128, 1),
              border: Border.all(
                color: const Color.fromRGBO(255, 51, 85, 1),
              ),
              borderRadius: BorderRadius.circular(10),
            ),
            constraints: const BoxConstraints(maxWidth: 220),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MultiLanguageText(
                  name: type.toLowerCase(),
                  defaultValue: title,
                  overflow: TextOverflow.clip,
                  style: Style(context).textDragAndDropWidget,
                ),
                if (platformsList.isNotEmpty)
                  RichText(
                    text: TextSpan(
                      style: TextStyle(fontSize: context.fontSizeManager.fontSizeText12, color: Colors.white),
                      text: multiLanguageString(name: "platform_suffix", defaultValue: "Platform: ", context: context),
                      children: <TextSpan>[
                        for (var platform in platformsList)
                          TextSpan(
                            text: "$platform${platform == platformsList.last ? "" : ", "}",
                          ),
                      ],
                    ),
                  )
              ],
            ),
          ),
        ],
      );
    }
    if (type == "error") {
      return Row(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.fromLTRB(10, 5, 30, 5),
            decoration: BoxDecoration(
              color: Colors.red,
              border: Border.all(
                color: const Color.fromARGB(255, 247, 50, 36),
              ),
              borderRadius: BorderRadius.circular(10),
            ),
            constraints: const BoxConstraints(maxWidth: 220),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    MultiLanguageText(
                      name: type.toLowerCase(),
                      defaultValue: title,
                      overflow: TextOverflow.clip,
                      style: Style(context).textDragAndDropWidget,
                    ),
                    const SizedBox(width: 10),
                    const VariableDefaultTemplate()
                  ],
                ),
                if (platformsList.isNotEmpty)
                  RichText(
                    text: TextSpan(
                      style: TextStyle(fontSize: context.fontSizeManager.fontSizeText12, color: Colors.white),
                      text: multiLanguageString(name: "platform_suffix", defaultValue: "Platform: ", context: context),
                      children: <TextSpan>[
                        for (var platform in platformsList)
                          TextSpan(
                            text: "$platform${platform == platformsList.last ? "" : ", "}",
                          ),
                      ],
                    ),
                  )
              ],
            ),
          ),
        ],
      );
    }
    if (type == "finish") {
      return Row(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.fromLTRB(10, 10, 30, 10),
            decoration: BoxDecoration(
              color: Colors.green,
              border: Border.all(
                color: Colors.green[600]!,
              ),
              borderRadius: BorderRadius.circular(10),
            ),
            constraints: const BoxConstraints(maxWidth: 220),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MultiLanguageText(
                  name: type.toLowerCase(),
                  defaultValue: title,
                  overflow: TextOverflow.clip,
                  style: Style(context).textDragAndDropWidget,
                ),
                if (platformsList.isNotEmpty)
                  RichText(
                    text: TextSpan(
                      style: TextStyle(fontSize: context.fontSizeManager.fontSizeText12, color: Colors.white),
                      text: multiLanguageString(name: "platform_suffix", defaultValue: "Platform: ", context: context),
                      children: <TextSpan>[
                        for (var platform in platformsList)
                          TextSpan(
                            text: "$platform${platform == platformsList.last ? "" : ", "}",
                          ),
                      ],
                    ),
                  )
              ],
            ),
          ),
        ],
      );
    }

    if (type == "comment") {
      return Row(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: const Color.fromARGB(255, 86, 112, 111),
              border: Border.all(
                color: const Color.fromARGB(255, 69, 94, 92),
              ),
              borderRadius: BorderRadius.circular(10),
            ),
            constraints: const BoxConstraints(maxWidth: 220),
            padding: const EdgeInsets.fromLTRB(10, 10, 30, 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MultiLanguageText(
                  name: type.toLowerCase(),
                  defaultValue: title,
                  overflow: TextOverflow.clip,
                  style: Style(context).textDragAndDropWidget,
                ),
                if (platformsList.isNotEmpty)
                  RichText(
                    text: TextSpan(
                      style: TextStyle(fontSize: context.fontSizeManager.fontSizeText12, color: Colors.white),
                      text: multiLanguageString(name: "platform_suffix", defaultValue: "Platform: ", context: context),
                      children: <TextSpan>[
                        for (var platform in platformsList)
                          TextSpan(
                            text: "$platform${platform == platformsList.last ? "" : ", "}",
                          ),
                      ],
                    ),
                  )
              ],
            ),
          ),
        ],
      );
    }

    return MultiLanguageText(
      name: "type_not_implements",
      defaultValue: "\$0 not implements",
      variables: [type],
      style: TextStyle(fontSize: context.fontSizeManager.fontSizeText14),
    );
  }
}
