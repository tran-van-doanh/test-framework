import 'package:flutter/material.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/extension/hex_color.dart';
import 'package:test_framework_view/page/editor/template_list_widget/condition_variable_default_template.dart';

class AssertTemplate extends StatelessWidget {
  final String type;
  final String title;
  final List platformsList;
  final dynamic componentConfig;
  const AssertTemplate(this.type, {Key? key, required this.platformsList, required this.title, this.componentConfig}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (type == "assertEquals" || type == "assertNull" || type == "assertNotNull" || type == "assertTrue" || type == "assertFalse") {
      return Row(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.fromLTRB(10, 10, 30, 10),
            decoration: BoxDecoration(
              color: HexColor.fromHex(componentConfig["color"] ?? "#397397"),
              border: componentConfig["borderColor"] != null
                  ? Border.all(
                      color: HexColor.fromHex(componentConfig["borderColor"]),
                    )
                  : null,
              borderRadius: BorderRadius.circular(10),
            ),
            constraints: const BoxConstraints(maxWidth: 220),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    MultiLanguageText(
                      name: type.toLowerCase(),
                      defaultValue: title,
                      overflow: TextOverflow.clip,
                      style: Style(context).textDragAndDropWidget,
                    ),
                    if (type == "assertTrue" || type == "assertFalse")
                      const Row(
                        children: [
                          SizedBox(width: 10),
                          ConditionDefaultTemplate(),
                        ],
                      )
                  ],
                ),
                if (platformsList.isNotEmpty)
                  RichText(
                    text: TextSpan(
                      style: TextStyle(fontSize: context.fontSizeManager.fontSizeText12, color: Colors.white),
                      text: multiLanguageString(name: "platform_suffix", defaultValue: "Platform: ", context: context),
                      children: <TextSpan>[
                        for (var platform in platformsList)
                          TextSpan(
                            text: "$platform${platform == platformsList.last ? "" : ", "}",
                          ),
                      ],
                    ),
                  )
              ],
            ),
          ),
        ],
      );
    }
    return MultiLanguageText(
      name: "type_not_implements",
      defaultValue: "\$0 not implements",
      variables: [type],
      style: TextStyle(fontSize: context.fontSizeManager.fontSizeText14),
    );
  }
}
