import 'package:flutter/material.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';

class ConditionDefaultTemplate extends StatelessWidget {
  const ConditionDefaultTemplate({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30,
      width: 30,
      decoration: BoxDecoration(
        color: const Color.fromRGBO(56, 148, 56, 1),
        border: Border.all(
          color: const Color.fromRGBO(56, 148, 56, 1),
        ),
        borderRadius: BorderRadius.circular(7),
      ),
    );
  }
}

class VariableDefaultTemplate extends StatelessWidget {
  const VariableDefaultTemplate({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 30,
      height: 30,
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
        color: Colors.white,
        borderRadius: BorderRadius.circular(16),
      ),
      child: Center(
        child: Text(
          "\$",
          style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, decoration: TextDecoration.none),
        ),
      ),
    );
  }
}
