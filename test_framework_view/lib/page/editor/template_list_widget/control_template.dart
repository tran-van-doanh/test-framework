import 'package:flutter/material.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/extension/hex_color.dart';
import 'package:test_framework_view/page/editor/template_list_widget/condition_variable_default_template.dart';

class ControlTemplate extends StatelessWidget {
  final String type;
  final List platformsList;
  final bool hasBodyWidget;
  final dynamic componentConfig;
  const ControlTemplate(this.type, {Key? key, required this.platformsList, this.hasBodyWidget = true, this.componentConfig}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (type == "forEach" || type == "while" || type == "group" || type == "step" || type == "client") {
      return Align(
        alignment: Alignment.centerLeft,
        child: Column(
          children: [
            HeaderTemplate(type, platformsList, componentConfig: componentConfig),
            if (hasBodyWidget)
              Column(
                children: [
                  borderLeftWidget(),
                  FooterTemplate(type: type, componentConfig: componentConfig),
                ],
              )
          ],
        ),
      );
    }
    if (type == "condition") {
      return Align(
        alignment: Alignment.centerLeft,
        child: Column(
          children: [
            HeaderTemplate(type, platformsList, componentConfig: componentConfig),
            if (hasBodyWidget)
              Column(
                children: [
                  borderLeftWidget(),
                  IfThenElseBodyTemplate(
                      value: multiLanguageString(name: 'else', defaultValue: "Else", context: context), componentConfig: componentConfig),
                  borderLeftWidget(),
                  FooterTemplate(type: type, componentConfig: componentConfig),
                ],
              )
          ],
        ),
      );
    }
    if (type == "try") {
      return Align(
        alignment: Alignment.centerLeft,
        child: Column(
          children: [
            HeaderTemplate(type, platformsList, componentConfig: componentConfig),
            if (hasBodyWidget)
              Column(
                children: [
                  borderLeftWidget(),
                  IfThenElseBodyTemplate(
                      value: multiLanguageString(name: 'on_error', defaultValue: "onError", context: context), componentConfig: componentConfig),
                  borderLeftWidget(),
                  IfThenElseBodyTemplate(
                      value: multiLanguageString(name: 'on_finish', defaultValue: "onFinish", context: context), componentConfig: componentConfig),
                  borderLeftWidget(),
                  FooterTemplate(type: type, componentConfig: componentConfig),
                ],
              )
          ],
        ),
      );
    }
    return MultiLanguageText(
      name: "type_keyword_not_implemented",
      defaultValue: "\$0 keyword not implemented",
      variables: [type],
    );
  }

  Container borderLeftWidget() {
    return Container(
        height: 35,
        margin: const EdgeInsets.only(left: 10),
        padding: const EdgeInsets.fromLTRB(10, 5, 0, 5),
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(
              width: 2,
              color: HexColor.fromHex(componentConfig["color"] ?? "#FFFFAB19"),
            ),
          ),
        ));
  }
}

class HeaderTemplate extends StatelessWidget {
  final String type;
  final List platformsList;
  final dynamic componentConfig;
  const HeaderTemplate(this.type, this.platformsList, {Key? key, this.componentConfig}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget childWidget = Container();
    if (type == "condition") {
      childWidget = Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          MultiLanguageText(
            name: "if",
            defaultValue: "If",
            style: Style(context).textDragAndDropWidget,
          ),
          Container(margin: const EdgeInsets.symmetric(horizontal: 10), child: const ConditionDefaultTemplate()),
          Padding(
            padding: const EdgeInsets.only(right: 20),
            child: MultiLanguageText(
              name: "then",
              defaultValue: "then",
              style: Style(context).textDragAndDropWidget,
            ),
          ),
        ],
      );
    }

    if (type == "forEach") {
      childWidget = Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          MultiLanguageText(
            name: "foreach",
            defaultValue: "Foreach",
            style: Style(context).textDragAndDropWidget,
          ),
          const SizedBox(width: 10),
          const VariableDefaultTemplate(),
        ],
      );
    }
    if (type == "while") {
      childWidget = Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          MultiLanguageText(
            name: "while",
            defaultValue: "While",
            style: Style(context).textDragAndDropWidget,
          ),
          const SizedBox(width: 10),
          const ConditionDefaultTemplate(),
        ],
      );
    }
    if (type == "group" || type == "step" || type == "client" || type == "try") {
      childWidget = MultiLanguageText(
        name: type.toLowerCase(),
        defaultValue: type == "step" ? "Flow" : type[0].toUpperCase() + type.substring(1),
        style: Style(context).textDragAndDropWidget,
      );
    }
    return Container(
      constraints: const BoxConstraints(minHeight: 35, minWidth: 220, maxWidth: 220),
      decoration: BoxDecoration(
        color: HexColor.fromHex(componentConfig["color"] ?? "#FFFFAB19"),
        border: componentConfig["borderColor"] != null
            ? Border.all(
                color: HexColor.fromHex(componentConfig["borderColor"]),
              )
            : null,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            childWidget,
            if (platformsList.isNotEmpty)
              RichText(
                text: TextSpan(
                  style: TextStyle(fontSize: context.fontSizeManager.fontSizeText12, color: Colors.white),
                  text: multiLanguageString(name: "platform_suffix", defaultValue: "Platform: ", context: context),
                  children: <TextSpan>[
                    for (var platform in platformsList)
                      TextSpan(
                        text: "$platform${platform == platformsList.last ? "" : ", "}",
                      ),
                  ],
                ),
              )
          ],
        ),
      ),
    );
  }
}

class FooterTemplate extends StatelessWidget {
  final String type;
  final dynamic componentConfig;
  const FooterTemplate({Key? key, required this.type, this.componentConfig}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(minHeight: 35, minWidth: 220, maxWidth: 220),
      decoration: BoxDecoration(
        color: HexColor.fromHex(componentConfig["color"] ?? "#FFFFAB19"),
        border: componentConfig["borderColor"] != null
            ? Border.all(
                color: HexColor.fromHex(componentConfig["borderColor"]),
              )
            : null,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Align(
          alignment: Alignment.centerLeft,
          child: MultiLanguageText(
            name: type == "condition"
                ? "end_if"
                : type == "step"
                    ? "end_flow"
                    : "end_${type.toLowerCase()}",
            defaultValue: type == "condition"
                ? "End If"
                : type == "step"
                    ? "End Flow"
                    : "End ${type[0].toUpperCase() + type.substring(1).toLowerCase()}",
            style: Style(context).textDragAndDropWidget,
          ),
        ),
      ),
    );
  }
}

class IfThenElseBodyTemplate extends StatelessWidget {
  final String value;
  final dynamic componentConfig;
  const IfThenElseBodyTemplate({Key? key, required this.value, this.componentConfig}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(minHeight: 35, minWidth: 220, maxWidth: 220),
      decoration: BoxDecoration(
        color: HexColor.fromHex(componentConfig["color"] ?? "#FFFFAB19"),
        border: componentConfig["borderColor"] != null
            ? Border.all(
                color: HexColor.fromHex(componentConfig["borderColor"]),
              )
            : null,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Align(
          alignment: Alignment.centerLeft,
          child: Text(
            value,
            style: Style(context).textDragAndDropWidget,
          ),
        ),
      ),
    );
  }
}
