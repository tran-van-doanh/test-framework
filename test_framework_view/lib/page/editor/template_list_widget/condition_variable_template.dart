import 'package:flutter/material.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/extension/hex_color.dart';
import 'package:test_framework_view/page/editor/template_list_widget/condition_variable_default_template.dart';

class ConditionVariableTemplate extends StatelessWidget {
  final String category;
  final String operatorType;
  final String? childrenType;
  final dynamic componentConfig;
  const ConditionVariableTemplate({Key? key, required this.category, required this.operatorType, this.childrenType, this.componentConfig})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(minHeight: 35),
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: HexColor.fromHex(componentConfig["color"] ?? ((category == "condition" || category == "conditionGroup") ? "#FF59C059" : "#FFFF8C1A")),
        border: componentConfig["borderColor"] != null
            ? Border.all(
                color: HexColor.fromHex(componentConfig["borderColor"] ?? "#FF389438"),
              )
            : null,
        borderRadius: BorderRadius.circular(category == "variable" ? 50 : 10),
      ),
      child: childrenType == "variable_default"
          ? Container(
              // alignment: Alignment.centerLeft,
              //constraints: const BoxConstraints(maxWidth: 198),
              padding: const EdgeInsets.only(right: 20),
              child: IntrinsicWidth(
                child: Row(
                  // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const VariableDefaultTemplate(),
                    const SizedBox(width: 10),
                    MultiLanguageText(
                      name: operatorType.toLowerCase().replaceAll(' ', '_'),
                      defaultValue: operatorType,
                      style: Style(context).textDragAndDropWidget,
                    ),
                    const SizedBox(width: 10),
                    const VariableDefaultTemplate()
                  ],
                ),
              ),
            )
          : childrenType == "condition_default"
              ? Container(
                  //constraints: const BoxConstraints(maxWidth: 198),
                  padding: const EdgeInsets.only(right: 20),
                  // alignment: Alignment.centerLeft,
                  child: IntrinsicWidth(
                    child: Row(
                      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        if (operatorType != "NOT" && operatorType != "Assert True" && operatorType != "Assert False")
                          const ConditionDefaultTemplate(),
                        if (operatorType != "NOT" && operatorType != "Assert True" && operatorType != "Assert False") const SizedBox(width: 10),
                        MultiLanguageText(
                          name: operatorType.toLowerCase().replaceAll(' ', '_'),
                          defaultValue: operatorType,
                          style: Style(context).textDragAndDropWidget,
                        ),
                        const SizedBox(width: 10),
                        const ConditionDefaultTemplate(),
                      ],
                    ),
                  ),
                )
              : Container(
                  //constraints: const BoxConstraints(maxWidth: 198),
                  // alignment: Alignment.centerLeft,
                  padding: const EdgeInsets.only(right: 20),
                  child: MultiLanguageText(
                    name: operatorType.toLowerCase().replaceAll(' ', '_'),
                    defaultValue: operatorType,
                    style: Style(context).textDragAndDropWidget,
                  ),
                ),
    );
  }
}
