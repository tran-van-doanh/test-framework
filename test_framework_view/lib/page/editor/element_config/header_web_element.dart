import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/custom_state.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';

class HeaderWebElementWidget extends StatefulWidget {
  final Function? callbackFilter;
  final String? dx;
  final String? dy;
  final String? elementTemplate;
  final bool isShowFilterForm;
  final VoidCallback onClickFilterForm;
  const HeaderWebElementWidget({
    Key? key,
    this.callbackFilter,
    this.dx,
    this.dy,
    this.isShowFilterForm = true,
    required this.onClickFilterForm,
    this.elementTemplate,
  }) : super(key: key);

  @override
  State<HeaderWebElementWidget> createState() => _HeaderWebElementWidgetState();
}

class _HeaderWebElementWidgetState extends CustomState<HeaderWebElementWidget> {
  final TextEditingController dxController = TextEditingController();
  final TextEditingController dyController = TextEditingController();
  Timer? _debounceTimer;
  String? selectElementId;
  String? prjProjectTypeId;
  List<dynamic> listElementsTemplate = [];
  List<dynamic> listProjectType = [];
  @override
  void initState() {
    super.initState();
    getInfoPage();
  }

  handleCallBackSearchFunction({String? dx, String? dy}) {
    var result = {
      "tfFindElementTemplateId": selectElementId,
      "dx": dx,
      "dy": dy,
    };
    widget.callbackFilter!(result);
  }

  getInfoPage() async {
    String prjProjectId = Provider.of<NavigationModel>(context, listen: false).prjProjectId ?? "";
    setState(() {
      selectElementId = widget.elementTemplate;
    });
    await getListProjectType(prjProjectId);
    await getCurrentProjectType(prjProjectId);
    getListElementTemplate();
  }

  getCurrentProjectType(prjProjectId) async {
    var projectResponse = await httpGet("/test-framework-api/user/project/prj-project/$prjProjectId", context);
    if (projectResponse.containsKey("body") && projectResponse["body"] is String == false) {
      prjProjectTypeId = projectResponse["body"]["result"]["prjProjectTypeId"];
    }
  }

  getListElementTemplate() async {
    var findRequest = {"status": 1, "prjProjectTypeId": prjProjectTypeId};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-find-element-template/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        listElementsTemplate = response["body"]["resultList"];
      });
    }
  }

  getListProjectType(prjProjectId) async {
    var findRequest = {"prjProjectId": prjProjectId, "status": 1};
    var response = await httpPost("/test-framework-api/user/project/prj-project-type/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        listProjectType += response["body"]["resultList"];
      });
    }
  }

  @override
  void dispose() {
    _debounceTimer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    dxController.text = widget.dx ?? "";
    dyController.text = widget.dy ?? "";
    return Consumer<WebElementModel>(
      builder: (context, webElementModel, child) {
        return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
          List<Widget> widgetList = [
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
              child: SizedBox(
                height: 50,
                child: DynamicDropdownButton(
                  borderRadius: 5,
                  value: prjProjectTypeId,
                  hint: multiLanguageString(name: "choose_a_project_type", defaultValue: "Choose a project type", context: context),
                  items: listProjectType.map<DropdownMenuItem<String>>((dynamic result) {
                    return DropdownMenuItem(
                      value: result["id"],
                      child: Text(
                        result["title"],
                      ),
                    );
                  }).toList(),
                  onChanged: (dynamic newValue) async {
                    setState(() {
                      prjProjectTypeId = newValue;
                      webElementModel.selectElementId = null;
                      dxController.clear();
                      dyController.clear();
                      selectElementId = null;
                    });
                    getListElementTemplate();
                  },
                ),
              ),
            ),
            if (prjProjectTypeId != null)
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                child: SizedBox(
                  height: 50,
                  child: DynamicDropdownButton(
                    borderRadius: 5,
                    value: selectElementId ?? webElementModel.selectElementId,
                    hint: multiLanguageString(name: "choose_a_elements", defaultValue: "Choose a Elements", context: context),
                    items: listElementsTemplate.map<DropdownMenuItem<String>>((dynamic result) {
                      return DropdownMenuItem(
                        value: result["id"],
                        child: Text(
                          result["title"],
                        ),
                      );
                    }).toList(),
                    onChanged: widget.elementTemplate == null
                        ? (dynamic newValue) async {
                            setState(() {
                              selectElementId = newValue;
                            });
                            handleCallBackSearchFunction();
                          }
                        : null,
                  ),
                ),
              ),
            if (prjProjectTypeId != null && (selectElementId != null || webElementModel.selectElementId != null))
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                child: SizedBox(
                  height: 50,
                  child: DynamicTextField(
                    controller: dxController,
                    hintText: multiLanguageString(name: "by_position_x", defaultValue: "By position x", context: context),
                    keyboardType: TextInputType.number,
                    onChanged: (value) {
                      if (_debounceTimer?.isActive ?? false) {
                        _debounceTimer?.cancel();
                      }
                      _debounceTimer = Timer(const Duration(milliseconds: 500), () {
                        handleCallBackSearchFunction(dx: value, dy: widget.dy);
                      });
                    },
                    suffixIcon: (widget.dx != null && widget.dx != "")
                        ? IconButton(
                            onPressed: () {
                              handleCallBackSearchFunction(dx: null, dy: widget.dy);
                            },
                            icon: const Icon(Icons.clear),
                            splashRadius: 20,
                          )
                        : null,
                  ),
                ),
              ),
            if (prjProjectTypeId != null && (selectElementId != null || webElementModel.selectElementId != null))
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                child: SizedBox(
                  height: 50,
                  child: DynamicTextField(
                    controller: dyController,
                    hintText: multiLanguageString(name: "by_position_y", defaultValue: "By position y", context: context),
                    keyboardType: TextInputType.number,
                    onChanged: (value) {
                      if (_debounceTimer?.isActive ?? false) {
                        _debounceTimer?.cancel();
                      }
                      _debounceTimer = Timer(const Duration(milliseconds: 500), () {
                        handleCallBackSearchFunction(dx: widget.dx, dy: value);
                      });
                    },
                    suffixIcon: (widget.dy != null && widget.dy != "")
                        ? IconButton(
                            onPressed: () {
                              handleCallBackSearchFunction(dx: widget.dx, dy: null);
                            },
                            icon: const Icon(Icons.clear),
                            splashRadius: 20,
                          )
                        : null,
                  ),
                ),
              ),
          ];
          var itemPerRow = 4;
          if (constraints.maxWidth < 600) {
            itemPerRow = 1;
          } else if (constraints.maxWidth < 800) {
            itemPerRow = 2;
          } else if (constraints.maxWidth < 1200) {
            itemPerRow = 3;
          }
          var rowCount = (1.0 * widgetList.length / itemPerRow).ceil();
          var filterWidget = Column(
            children: [
              for (var rowI = 0; rowI < rowCount; rowI++)
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    for (var colI = 0; colI < itemPerRow; colI++)
                      rowI * itemPerRow + colI < widgetList.length
                          ? Expanded(child: widgetList[rowI * itemPerRow + colI])
                          : Expanded(child: Container())
                  ],
                )
            ],
          );

          return Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  MultiLanguageText(
                    name: "web_element",
                    defaultValue: "Web element",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: const Color.fromRGBO(0, 0, 0, 0.8),
                        fontSize: context.fontSizeManager.fontSizeText16,
                        letterSpacing: 2),
                  ),
                  IconButton(
                    onPressed: () {
                      widget.onClickFilterForm();
                    },
                    icon: widget.isShowFilterForm ? const Icon(Icons.visibility) : const Icon(Icons.visibility_off),
                    splashRadius: 1,
                    tooltip: widget.isShowFilterForm
                        ? multiLanguageString(name: "hide_filter_form", defaultValue: "Hide filter form", context: context)
                        : multiLanguageString(name: "show_filter_form", defaultValue: "Show filter form", context: context),
                  )
                ],
              ),
              if (widget.isShowFilterForm)
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    filterWidget,
                    if (widgetList.length > 2)
                      ElevatedButton(
                        onPressed: () {
                          setState(() {
                            dxController.clear();
                            dyController.clear();
                            selectElementId = null;
                            prjProjectTypeId = null;
                            handleCallBackSearchFunction();
                          });
                        },
                        child: const MultiLanguageText(name: "reset", defaultValue: "Reset", isLowerCase: false),
                      ),
                  ],
                ),
            ],
          );
        });
      },
    );
  }
}
