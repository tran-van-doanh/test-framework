import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/config.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/editor/element_config/header_web_element.dart';
import 'package:test_framework_view/page/editor/element_config/view_img_page_url.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';

import '../../../common/custom_state.dart';

class WebElementConfigWidget extends StatefulWidget {
  final Function callback;
  final String? clientName;
  final String? platform;
  final String? elementTemplate;
  const WebElementConfigWidget(
    this.callback, {
    Key? key,
    this.clientName,
    this.platform,
    this.elementTemplate,
  }) : super(key: key);

  @override
  State<WebElementConfigWidget> createState() => _WebElementConfigWidgetState();
}

class _WebElementConfigWidgetState extends CustomState<WebElementConfigWidget> {
  bool isChangeWebElement = false;
  bool isExpandFormSearch = false;
  var listSearchElementsResult = [];
  var listBeforeSearchElementsResult = [];
  var positionList = [];
  var isSelectList = [];
  String direction = "up";
  String? clientName;
  String? platform;
  TextEditingController length = TextEditingController();
  String? dx;
  String? dy;
  bool isViewVertical = false;
  bool isShowFilterForm = true;
  @override
  void initState() {
    length.text = "100";
    if (widget.clientName != null) {
      setState(() {
        clientName = widget.clientName;
      });
    }
    if (widget.platform != null) {
      setState(() {
        platform = widget.platform;
      });
    }
    isSelectList = List.generate(Provider.of<WebElementModel>(context, listen: false).listFindElementsResult.length, (index) => false);
    listSearchElementsResult = Provider.of<WebElementModel>(context, listen: false).listFindElementsResult;
    super.initState();
  }

  getListElement(elementId, String? clientName) async {
    var responseFindElement = await httpGet("/test-framework-api/user/node/find-elements/template/$elementId/$clientName", context);
    if (responseFindElement.containsKey("body") && responseFindElement["body"] is String == false) {
      await getElementTemplate(elementId);
      setState(() {
        Provider.of<WebElementModel>(context, listen: false).listFindElementsResult = responseFindElement["body"]["resultList"] ?? [];
        Provider.of<WebElementModel>(context, listen: false).currentTimeMicrosecondsSinceEpoch = DateTime.now().microsecondsSinceEpoch;
        listSearchElementsResult = responseFindElement["body"]["resultList"] ?? [];
        listBeforeSearchElementsResult = responseFindElement["body"]["resultList"] ?? [];

        isSelectList = List.generate(listSearchElementsResult.length, (index) => false);
        positionList = [];
      });
    }
  }

  searchElementsResult(List<dynamic> inputlist, attributeName, String text) {
    setState(() {
      listSearchElementsResult = inputlist.where((input) {
        return input[attributeName].toString().toLowerCase().contains(text.toLowerCase());
      }).toList();
    });
  }

  getElementTemplate(elementId) async {
    var responseElementTemplate = await httpGet("/test-framework-api/user/test-framework/tf-find-element-template/$elementId", context);
    if (responseElementTemplate.containsKey("body") && responseElementTemplate["body"] is String == false) {
      setState(() {
        Provider.of<WebElementModel>(context, listen: false).selectElementName = responseElementTemplate["body"]["result"]["name"];
        Provider.of<WebElementModel>(context, listen: false).selectElementId = responseElementTemplate["body"]["result"]["id"];
        Provider.of<WebElementModel>(context, listen: false).resultAttributeList =
            responseElementTemplate["body"]["result"]["content"]["resultAttributeList"];
      });
    }
  }

  onSearchOffset(double? dx, double? dy) {
    setState(() {
      listSearchElementsResult = listBeforeSearchElementsResult.where((element) {
        return (dx != null ? (element["position"]["x"] <= dx) && (element["position"]["x"] + element["position"]["width"] >= dx) : true) &&
            (dy != null ? (element["position"]["y"] <= dy) && (element["position"]["y"] + element["position"]["height"] >= dy) : true);
      }).toList();
    });
  }

  @override
  void dispose() {
    length.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<WebElementModel, LanguageModel>(
      builder: (context, webElementModel, languageModel, child) {
        return TestFrameworkRootPageWidget(
          child: LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
              if (constraints.maxWidth >= 1200 && !isViewVertical) {
                return Column(
                  children: [
                    headerWidget(),
                    Expanded(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: 600,
                            child: bodyPageWidget(webElementModel, true, isViewImg: false),
                          ),
                          Expanded(
                            child: imgWidget(webElementModel, true),
                          ),
                        ],
                      ),
                    ),
                    btnWidget(webElementModel),
                  ],
                );
              }
              return Column(
                children: [
                  headerWidget(),
                  Expanded(
                    child: Stack(
                      children: [
                        bodyPageWidget(webElementModel, false),
                        if (constraints.maxWidth >= 1200)
                          GestureDetector(
                              onTap: () {
                                setState(() {
                                  isViewVertical = false;
                                });
                              },
                              child: const Align(alignment: Alignment.centerRight, child: Icon(Icons.keyboard_double_arrow_left))),
                      ],
                    ),
                  ),
                  btnWidget(webElementModel),
                ],
              );
            },
          ),
        );
      },
    );
  }

  SingleChildScrollView bodyPageWidget(WebElementModel webElementModel, bool isSmall, {bool isViewImg = true}) {
    return SingleChildScrollView(
      child: Padding(
        padding: isViewImg ? const EdgeInsets.fromLTRB(50, 0, 50, 0) : const EdgeInsets.fromLTRB(20, 0, 20, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            clientWidget(),
            if (clientName != null)
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  selectElementTemplateWidget(webElementModel),
                  if (webElementModel.resultAttributeList.isNotEmpty) elementListWidget(webElementModel, isSmall),
                  if (isChangeWebElement && listSearchElementsResult.isEmpty)
                    Container(
                      margin: const EdgeInsets.only(top: 20),
                      child: Align(
                        alignment: Alignment.center,
                        child: MultiLanguageText(
                          name: "not_found_element",
                          defaultValue: "Not found element",
                          style: TextStyle(
                            fontSize: context.fontSizeManager.fontSizeText16,
                            fontWeight: FontWeight.bold,
                            letterSpacing: 1.5,
                          ),
                        ),
                      ),
                    ),
                  if (isViewImg) imgWidget(webElementModel, false),
                ],
              ),
          ],
        ),
      ),
    );
  }

  HeaderWebElementWidget selectElementTemplateWidget(WebElementModel webElementModel) {
    return HeaderWebElementWidget(
      isShowFilterForm: isShowFilterForm,
      onClickFilterForm: () => setState(() {
        isShowFilterForm = !isShowFilterForm;
      }),
      dx: dx,
      dy: dy,
      callbackFilter: (result) async {
        onLoading(context);
        await getListElement(result["tfFindElementTemplateId"], clientName);
        if (mounted) {
          Navigator.pop(context);
        }
        isChangeWebElement = true;
        setState(() {
          dx = result["dx"];
          dy = result["dy"];
        });
        onSearchOffset(dx != null ? double.tryParse(dx!) : null, dy != null ? double.tryParse(dy!) : null);
      },
    );
  }

  Padding imgWidget(WebElementModel webElementModel, bool hasArrow) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: Stack(
        children: [
          ViewImgPageUrl(
            key: UniqueKey(),
            dx: dx != null ? double.tryParse(dx!) : null,
            dy: dy != null ? double.tryParse(dy!) : null,
            positionList: positionList,
            src: "$baseUrl/test-framework-api/user/node-file/screenshot/$clientName?time=${webElementModel.currentTimeMicrosecondsSinceEpoch}",
            onClickOffset: (tapOffset) {
              setState(() {
                dx = tapOffset.dx.toInt().toString();
                dy = tapOffset.dy.toInt().toString();
              });
              onSearchOffset(dx != null ? double.tryParse(dx!) : null, dy != null ? double.tryParse(dy!) : null);
            },
          ),
          if (hasArrow)
            GestureDetector(
                onTap: () {
                  setState(() {
                    isViewVertical = true;
                  });
                },
                child: const Align(alignment: Alignment.centerLeft, child: Icon(Icons.keyboard_double_arrow_right))),
        ],
      ),
    );
  }

  Widget btnWidget(WebElementModel webElementModel) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10),
      decoration: const BoxDecoration(
          border: Border(
        top: BorderSide(color: Color.fromARGB(255, 207, 205, 205)),
      )),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          if (Provider.of<NavigationModel>(context, listen: false).prjProject != null &&
              Provider.of<NavigationModel>(context, listen: false).prjProject["prjProjectType"] != null &&
              (Provider.of<NavigationModel>(context, listen: false).prjProject["prjProjectType"]["platform"] == "iOS" ||
                  Provider.of<NavigationModel>(context, listen: false).prjProject["prjProjectType"]["platform"] == "Android"))
            Container(
              margin: const EdgeInsets.only(right: 15),
              height: 40,
              child: Row(
                children: [
                  SizedBox(
                    width: 100,
                    child: DynamicDropdownButton(
                      value: direction,
                      hint: multiLanguageString(name: "direction", defaultValue: "direction", context: context),
                      items: ["up", "down", "left", "right"].map<DropdownMenuItem<String>>((String result) {
                        return DropdownMenuItem(
                          value: result,
                          child: Text(result),
                        );
                      }).toList(),
                      onChanged: (newValue) {
                        setState(() {
                          direction = newValue;
                        });
                      },
                      isRequiredNotEmpty: true,
                    ),
                  ),
                  SizedBox(
                    width: 70,
                    child: TextField(
                      controller: length,
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.symmetric(vertical: 5, horizontal: 12),
                        hintText: multiLanguageString(name: "enter_a_length", defaultValue: "Enter a length", context: context),
                        counterText: "",
                        border: const OutlineInputBorder(),
                      ),
                      maxLength: 5,
                    ),
                  ),
                  ElevatedButton(
                    onPressed: () async {
                      var findRequest = {
                        "type": "swipe",
                        "data": {
                          "direction": direction,
                          "length": length.text,
                        }
                      };
                      await httpPost("/test-framework-api/user/node/message", findRequest, context);
                      setState(() {
                        webElementModel.setCurrentTimeMicrosecondsSinceEpoch(DateTime.now().microsecondsSinceEpoch);
                      });
                    },
                    child: const MultiLanguageText(
                      name: "swipe",
                      defaultValue: "Swipe",
                    ),
                  ),
                ],
              ),
            ),
          Container(
            margin: const EdgeInsets.only(right: 15),
            height: 40,
            child: ElevatedButton(
                onPressed: () async {
                  if (webElementModel.selectElementId != null) {
                    onLoading(context);
                    await getListElement(webElementModel.selectElementId, clientName);
                    if (mounted) {
                      Navigator.pop(context);
                    }
                    isChangeWebElement = true;
                  }
                },
                child: const MultiLanguageText(name: "refresh_list_webelement", defaultValue: "Refresh List Webelement")),
          ),
          Container(
            margin: const EdgeInsets.only(right: 15),
            height: 40,
            child: ElevatedButton(
                onPressed: () {
                  webElementModel.setCurrentTimeMicrosecondsSinceEpoch(DateTime.now().microsecondsSinceEpoch);
                },
                child: const MultiLanguageText(name: "resfresh_screenshot_view", defaultValue: "Refresh Screenshot View")),
          ),
          const ButtonCancel(),
        ],
      ),
    );
  }

  Widget elementListWidget(WebElementModel webElementModel, bool isSmall) {
    return Container(
      padding: const EdgeInsets.only(top: 10),
      child: isSmall
          ? CustomDataTableWidget(
              minWidth: 450,
              columns: [
                Expanded(
                  child: Text(
                    "",
                    style: Style(context).styleTextDataColumn,
                  ),
                ),
                Container(
                  width: 50,
                ),
              ],
              rows: [
                for (var findElementsResult in listSearchElementsResult)
                  CustomDataRow(
                    cells: [
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              for (var resultAttribute in webElementModel.resultAttributeList)
                                if (resultAttribute["attributeName"] != "position")
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text("${resultAttribute["name"].toUpperCase()}: ", style: Style(context).styleTextDataColumn),
                                      Expanded(
                                        child: Text(
                                          findElementsResult["${resultAttribute["name"]}"] ?? "",
                                          style: TextStyle(
                                              fontSize: context.fontSizeManager.fontSizeText14,
                                              color: const Color.fromRGBO(49, 49, 49, 1),
                                              decoration: TextDecoration.none),
                                        ),
                                      ),
                                    ],
                                  ),
                              Wrap(
                                runSpacing: 10,
                                spacing: 10,
                                children: [
                                  for (var xpath in findElementsResult["xpathList"] ?? [])
                                    ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                        backgroundColor: const Color.fromRGBO(76, 151, 255, 1),
                                      ),
                                      onPressed: () {
                                        Navigator.pop(context);
                                        widget.callback(xpath, findElementsResult);
                                      },
                                      child: Text(
                                        xpath["name"].toString(),
                                      ),
                                    ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: IconButton(
                          icon: isSelectList[listSearchElementsResult.indexOf(findElementsResult)]
                              ? const Icon(Icons.visibility_off)
                              : const Icon(Icons.remove_red_eye_rounded),
                          onPressed: () {
                            setState(() {
                              int index = listSearchElementsResult.indexOf(findElementsResult);
                              isSelectList[index] = !isSelectList[index];
                              if (isSelectList[index]) {
                                if (findElementsResult["position"] != null) {
                                  positionList.add(findElementsResult["position"]);
                                }
                              } else {
                                findElementsResult["position"] != null && positionList.remove(findElementsResult["position"]);
                              }
                            });
                          },
                          splashRadius: 20,
                          tooltip: multiLanguageString(name: "view_position", defaultValue: "View position", context: context),
                        ),
                      ),
                    ],
                  ),
              ],
            )
          : CustomDataTableWidget(
              columns: [
                for (var resultAttribute in webElementModel.resultAttributeList)
                  if (resultAttribute["attributeName"] != "position")
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          setState(() {
                            resultAttribute["isSearch"] = !(resultAttribute["isSearch"] ?? false);
                          });
                        },
                        child: (resultAttribute["isSearch"] ?? false)
                            ? DynamicTextField(
                                autofocus: true,
                                hintText: multiLanguageString(
                                    name: "search_result_attribute",
                                    defaultValue: "Search \$0",
                                    variables: ["${resultAttribute["name"]}"],
                                    context: context),
                                labelText: resultAttribute["name"],
                                prefixIcon: const Icon(Icons.search),
                                suffixIcon: InkWell(
                                  child: const Icon(Icons.close),
                                  onTap: () {
                                    setState(() {
                                      resultAttribute["isSearch"] = false;
                                      searchElementsResult(webElementModel.listFindElementsResult, resultAttribute["name"], "");
                                    });
                                  },
                                ),
                                onChanged: (text) {
                                  searchElementsResult(webElementModel.listFindElementsResult, resultAttribute["name"], text);
                                },
                              )
                            : Text(resultAttribute["name"], style: Style(context).styleTextDataColumn),
                      ),
                    ),
                Expanded(
                  child: Center(
                    child: MultiLanguageText(
                        name: "select_type", defaultValue: "Select Type", isLowerCase: false, style: Style(context).styleTextDataColumn),
                  ),
                ),
                Container(
                  width: 50,
                ),
              ],
              rows: [
                for (var findElementsResult in listSearchElementsResult)
                  CustomDataRow(
                    cells: [
                      for (var resultAttribute in webElementModel.resultAttributeList)
                        if (resultAttribute["attributeName"] != "position")
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.all(5),
                              child: Text(
                                findElementsResult[resultAttribute["name"]] ?? "",
                                style: Style(context).styleTextDataCell,
                              ),
                            ),
                          ),
                      Expanded(
                          child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Wrap(
                          runSpacing: 10,
                          spacing: 10,
                          children: [
                            for (var xpath in findElementsResult["xpathList"] ?? [])
                              ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    backgroundColor: const Color.fromRGBO(76, 151, 255, 1),
                                  ),
                                  onPressed: () {
                                    Navigator.pop(context);
                                    widget.callback(xpath, findElementsResult);
                                  },
                                  child: Text(xpath["name"].toString()))
                          ],
                        ),
                      )),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: IconButton(
                          icon: isSelectList[listSearchElementsResult.indexOf(findElementsResult)]
                              ? const Icon(Icons.visibility_off)
                              : const Icon(Icons.remove_red_eye_rounded),
                          onPressed: () {
                            setState(() {
                              int index = listSearchElementsResult.indexOf(findElementsResult);
                              isSelectList[index] = !isSelectList[index];
                              if (isSelectList[index]) {
                                if (findElementsResult["position"] != null) {
                                  positionList.add(findElementsResult["position"]);
                                }
                              } else {
                                findElementsResult["position"] != null && positionList.remove(findElementsResult["position"]);
                              }
                            });
                          },
                          splashRadius: 20,
                          tooltip: multiLanguageString(name: "view_position", defaultValue: "View position", context: context),
                        ),
                      ),
                    ],
                  ),
              ],
            ),
    );
  }

  Widget clientWidget() {
    TestCaseConfigModel testCaseConfigModel = Provider.of<TestCaseConfigModel>(context, listen: false);
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        children: [
          const Expanded(
              child: MultiLanguageText(
                  name: "client",
                  defaultValue: "Client",
                  suffiText: " : ",
                  style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
          Expanded(
            flex: 3,
            child: DynamicDropdownButton(
                value: clientName,
                hint: multiLanguageString(name: "choose_a_client", defaultValue: "Choose a client", context: context),
                items: testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"].map<DropdownMenuItem>((result) {
                  return DropdownMenuItem(
                    value: result["name"],
                    child: Text(result["name"]),
                  );
                }).toList(),
                onChanged: (newValue) async {
                  setState(() {
                    clientName = newValue;
                    platform = testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"]
                        .firstWhere((element) => element['name'] == newValue)["platform"];
                  });
                  if (widget.elementTemplate != null && mounted) {
                    onLoading(context);
                    await getListElement(widget.elementTemplate, clientName);
                    if (mounted) {
                      Navigator.pop(context);
                    }
                    isChangeWebElement = true;
                  }
                },
                isRequiredNotEmpty: true),
          ),
        ],
      ),
    );
  }

  Widget headerWidget() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 50, 0),
      child: Row(
        children: [
          Expanded(
            child: Center(
              child: MultiLanguageText(
                name: "find_element_property",
                defaultValue: "Find element property",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: context.fontSizeManager.fontSizeText20, letterSpacing: 1.5),
              ),
            ),
          ),
          TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: const Icon(
                Icons.close,
                color: Colors.black,
              ))
        ],
      ),
    );
  }
}
