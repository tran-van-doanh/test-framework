import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';

import '../../../common/custom_state.dart';

class FindTemplateWidget extends StatefulWidget {
  final Function callback;
  const FindTemplateWidget(this.callback, {Key? key}) : super(key: key);

  @override
  State<FindTemplateWidget> createState() => _FindTemplateWidgetState();
}

class _FindTemplateWidgetState extends CustomState<FindTemplateWidget> {
  bool isChangeWebElement = false;
  List listTemplateType = [
    {"id": null, "name": "All"}
  ];
  var listSearchResponse = [];
  String? typeController;
  TextEditingController nameController = TextEditingController();
  Timer? _debounceTimer;

  getListTemplateType() async {
    var response = await httpPost("/test-framework-api/user/test-framework/tf-value-type/search", {"status": 1}, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        listTemplateType.addAll(response["body"]["resultList"]);
      });
    }
  }

  handleSearch() async {
    var findRequest = {
      "tfValueTypeId": typeController,
      "nameLike": "%${nameController.text}%",
      "status": 1,
    };
    var response = await httpPost("/test-framework-api/user/test-framework/tf-value-template/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        listSearchResponse = response["body"]["resultList"];
      });
    }
  }

  @override
  void initState() {
    getListTemplateType();
    handleSearch();
    super.initState();
  }

  @override
  void dispose() {
    _debounceTimer?.cancel();
    nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return TestFrameworkRootPageWidget(
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Center(
                          child: MultiLanguageText(
                            name: "find_value_property",
                            defaultValue: "Find value property",
                            style: TextStyle(fontWeight: FontWeight.bold, fontSize: context.fontSizeManager.fontSizeText20, letterSpacing: 1.5),
                          ),
                        ),
                      ),
                      TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: const Icon(
                            Icons.close,
                            color: Colors.black,
                          ))
                    ],
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  MultiLanguageText(name: "value_type", defaultValue: "Value Type", style: Style(context).styleTitleDialog),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    decoration: BoxDecoration(border: Border.all(color: const Color.fromRGBO(216, 218, 229, 1))),
                    child: DynamicDropdownButton(
                      value: typeController,
                      onChanged: (String? newValue) async {
                        setState(() {
                          typeController = newValue;
                        });
                        await handleSearch();
                        setState(() {
                          isChangeWebElement = true;
                        });
                      },
                      items: listTemplateType.map<DropdownMenuItem<String>>((dynamic result) {
                        return DropdownMenuItem(
                          value: result["id"],
                          child: Text(
                            result["name"],
                          ),
                        );
                      }).toList(),
                      hint: multiLanguageString(name: "choose_a_template_type", defaultValue: "Choose a template type", context: context),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  MultiLanguageText(name: "code", defaultValue: "Code", style: Style(context).styleTitleDialog),
                  const SizedBox(
                    height: 10,
                  ),
                  DynamicTextField(
                    controller: nameController,
                    hintText: multiLanguageString(name: "enter_a_template_code", defaultValue: "Enter a template code", context: context),
                    onChanged: (text) async {
                      setState(() {
                        nameController.text;
                      });
                      if (_debounceTimer?.isActive ?? false) {
                        _debounceTimer?.cancel();
                      }

                      _debounceTimer = Timer(const Duration(milliseconds: 500), () async {
                        await handleSearch();
                        setState(() {
                          isChangeWebElement = true;
                        });
                      });
                    },
                  ),
                  // const SizedBox(
                  //   height: 20,
                  // ),
                  // Center(
                  //   child: ElevatedButton.icon(
                  //     style: ButtonStyle(
                  //       padding: MaterialStateProperty.all<EdgeInsets>(const EdgeInsets.all(15)),
                  //     ),
                  //     onPressed: () async {
                  //       await handleSearch();
                  //       setState(() {
                  //         isChangeWebElement = true;
                  //       });
                  //     },
                  //     icon: const Icon(Icons.search),
                  //     label: const MultiLanguageText(name: "search", defaultValue: "SEARCH", isLowerCase: false),
                  //   ),
                  // ),
                  if (listSearchResponse.isNotEmpty)
                    Column(
                      children: [
                        const SizedBox(
                          height: 20,
                        ),
                        Container(
                          constraints: BoxConstraints(maxHeight: MediaQuery.of(context).size.height * 0.3),
                          child: SingleChildScrollView(
                            child: Row(
                              children: [
                                Expanded(
                                  child: DataTable(
                                    showCheckboxColumn: false,
                                    border: TableBorder(
                                        verticalInside: Style(context).styleBorderTable,
                                        horizontalInside: Style(context).styleBorderTable,
                                        bottom: Style(context).styleBorderTable,
                                        left: Style(context).styleBorderTable,
                                        right: Style(context).styleBorderTable,
                                        top: Style(context).styleBorderTable),
                                    columns: [
                                      DataColumn(
                                        label: Expanded(
                                          child: MultiLanguageText(
                                            name: "code",
                                            defaultValue: "Code",
                                            isLowerCase: false,
                                            style: Style(context).styleTextDataColumn,
                                            align: TextAlign.center,
                                          ),
                                        ),
                                      ),
                                      DataColumn(
                                        label: Expanded(
                                          flex: 2,
                                          child: MultiLanguageText(
                                            name: "value",
                                            defaultValue: "Value",
                                            isLowerCase: false,
                                            style: Style(context).styleTextDataColumn,
                                            align: TextAlign.center,
                                          ),
                                        ),
                                      ),
                                      DataColumn(
                                        label: Expanded(
                                          flex: 3,
                                          child: MultiLanguageText(
                                            name: "description",
                                            defaultValue: "Description",
                                            isLowerCase: false,
                                            style: Style(context).styleTextDataColumn,
                                            align: TextAlign.center,
                                          ),
                                        ),
                                      ),
                                    ],
                                    rows: [
                                      for (var searchResponseItem in listSearchResponse)
                                        DataRow(
                                          onSelectChanged: (value) {
                                            Navigator.pop(context);
                                            widget.callback(searchResponseItem);
                                          },
                                          cells: [
                                            DataCell(
                                              Text(searchResponseItem["name"] ?? ""),
                                            ),
                                            DataCell(
                                              Text(searchResponseItem["value"] ?? ""),
                                            ),
                                            DataCell(
                                              Text(searchResponseItem["description"] ?? ""),
                                            ),
                                          ],
                                        ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  if (isChangeWebElement && listSearchResponse.isEmpty)
                    Container(
                      margin: const EdgeInsets.only(top: 20),
                      child: Align(
                        alignment: Alignment.center,
                        child: MultiLanguageText(
                          name: "not_found_element",
                          defaultValue: "Not found element",
                          style: TextStyle(
                            fontSize: context.fontSizeManager.fontSizeText16,
                            fontWeight: FontWeight.bold,
                            letterSpacing: 1.5,
                          ),
                        ),
                      ),
                    ),
                  const SizedBox(height: 20),
                  const Center(child: ButtonCancel()),
                  const SizedBox(height: 20),
                ],
              ),
            ),
          ],
        ),
      );
    });
  }
}
