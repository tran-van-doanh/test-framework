import 'dart:async';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/badge/badge_icon.dart';
import 'package:test_framework_view/components/dialog/dialog_delete.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/config.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/editor/element_config/view_img_page_url.dart';
import 'package:test_framework_view/page/element_repository/header_element.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';

import '../../../common/custom_state.dart';
import '../../../type.dart';
import '../../element_repository/dialog_option_element.dart';
import '../../element_repository/dialog_option_element_repository.dart';

class ElementRepositoryConfigWidget extends StatefulWidget {
  final Function callback;
  const ElementRepositoryConfigWidget(this.callback, {Key? key}) : super(key: key);

  @override
  State<ElementRepositoryConfigWidget> createState() => _ElementRepositoryConfigWidgetState();
}

class _ElementRepositoryConfigWidgetState extends CustomState<ElementRepositoryConfigWidget> {
  TextEditingController elementTemplateSearchController = TextEditingController();
  final TextEditingController _searchLibrary = TextEditingController();
  Timer? _debounceTimer;
  late Future futureListAll;
  var resultListElementRepository = [];
  var resultBeforeFilterListElement = [];
  var resultListElement = [];
  var rowCountListAll = 0;
  // var rowCountListElement = 0;
  var rowCountListElementRepository = 0;
  // var currentPageElement = 1;
  var currentPageElementRepository = 1;
  // var rowPerPageElement = 10;
  var rowPerPageElementRepository = 10;
  var findRequestElement = {};
  var findRequestElementRepository = {};
  var prjProjectId = "";
  dynamic elementTemplate;
  List? elementRepositoryPathList;
  var positionList = [];
  var isSelectList = [];
  String? dx;
  String? dy;
  bool isViewVertical = true;
  bool hasImgSuccess = false;
  bool isShowFilterForm = true;
  String? screenshotFileName;
  late ElementRepositoryModel elementRepositoryModel;
  @override
  void initState() {
    elementRepositoryModel = Provider.of<ElementRepositoryModel>(context, listen: false);
    prjProjectId = Provider.of<NavigationModel>(context, listen: false).prjProjectId ?? "";
    futureListAll = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    await getListTfAll();
    if (mounted && elementRepositoryModel.selectRepositoryId != null) {
      await getElementRepository(Provider.of<ElementRepositoryModel>(context, listen: false).selectRepositoryId);
      await getElementRepositoryPathList();
    }
    if (elementRepositoryModel.selectElementId != null) {
      isShowFilterForm = true;
    }
    return 0;
  }

  getElementRepository(parentId) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-element-repository/$parentId", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        hasImgSuccess = response["body"]["result"]["screenshotFilePath"] != null;
        screenshotFileName = response["body"]["result"]["screenshotFileName"];
        isViewVertical = !hasImgSuccess;
        isShowFilterForm = !hasImgSuccess;
      });
    }
  }

  getElementRepositoryPathList() async {
    var directoryPathResponse = await httpGet(
        "/test-framework-api/user/test-framework/tf-test-element-repository/${elementRepositoryModel.selectRepositoryId}/path", context);
    if (directoryPathResponse.containsKey("body") && directoryPathResponse["body"] is String == false) {
      setState(() {
        elementRepositoryPathList = directoryPathResponse["body"]["resultList"];
      });
    }
  }

  getElementTemplate(String id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-find-element-template/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        elementTemplate = response["body"]["result"];
      });
    }
  }

  getListTfAll() async {
    var parentId = elementRepositoryModel.selectRepositoryId;
    if (parentId == null) {
      rowCountListElementRepository = 0;
      currentPageElementRepository = 1;
      rowPerPageElementRepository = 10;
      findRequestElementRepository = {};
      findRequestElementRepository["parentIdIsNull"] = true;
      await getListTfElementRepository(currentPageElementRepository, findRequestElementRepository);
    } else {
      findRequestElement = {};
      findRequestElement["tfTestElementRepositoryId"] = parentId;
      findRequestElement["tfFindElementTemplateId"] = elementRepositoryModel.selectElementId;
      findRequestElementRepository = {};
      findRequestElementRepository["parentId"] = parentId;
      await getListTfElement(findRequestElement);
      await getListTfElementRepository(currentPageElementRepository, findRequestElementRepository);
    }
    setState(() {
      rowCountListAll = rowCountListElementRepository;
    });
  }

  getListTfElement(findRequest) async {
    var findRequestElement = {};
    findRequestElement.addAll(findRequest);
    findRequestElement["prjProjectId"] = prjProjectId;
    findRequestElement["status"] = 1;
    if (findRequest["nameLike"] != null && findRequest["nameLike"].isNotEmpty) {
      findRequestElement["nameLike"] = "%${findRequest["nameLike"]}%";
    }
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-element/search", findRequestElement, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        resultListElement = response["body"]["resultList"];
        resultBeforeFilterListElement = response["body"]["resultList"];
        positionList = [];
        isSelectList = List.generate(resultListElement.length, (index) => false);
      });
    }
    return 0;
  }

  getListTfElementRepository(page, findRequest) async {
    if ((page - 1) * rowPerPageElementRepository > rowCountListElementRepository) {
      page = (1.0 * rowCountListElementRepository / rowPerPageElementRepository).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequestElementRepository = {};
    findRequestElementRepository.addAll(findRequest);
    findRequestElementRepository["queryOffset"] = (page - 1) * rowPerPageElementRepository;
    findRequestElementRepository["queryLimit"] = rowPerPageElementRepository;
    findRequestElementRepository["prjProjectId"] = prjProjectId;
    findRequestElementRepository["status"] = 1;
    var responseElementRepository =
        await httpPost("/test-framework-api/user/test-framework/tf-test-element-repository/search", findRequestElementRepository, context);
    if (responseElementRepository.containsKey("body") && responseElementRepository["body"] is String == false) {
      setState(() {
        currentPageElementRepository = page;
        rowCountListElementRepository = responseElementRepository["body"]["rowCount"];
        resultListElementRepository = responseElementRepository["body"]["resultList"];
      });
    }
    return 0;
  }

  handleAddTfAll(result, String type) async {
    var parentId = elementRepositoryModel.selectRepositoryId;
    var response = {};
    if (type == "elementRepository") {
      if (parentId != null) {
        result["parentId"] = parentId;
      }
      response = await httpPut("/test-framework-api/user/test-framework/tf-test-element-repository", result, context);
    }
    if (type == "element" && mounted) {
      response = await httpPut("/test-framework-api/user/test-framework/tf-test-element", result, context);
    }
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        Navigator.of(context).pop();
        futureListAll = getListTfAll();
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
      }
    }
  }

  handleEditTfAll(result, id, type) async {
    var response = {};
    if (type == "elementRepository") {
      response = await httpPost("/test-framework-api/user/test-framework/tf-test-element-repository/$id", result, context);
    }
    if (type == "element" && mounted) {
      response = await httpPost("/test-framework-api/user/test-framework/tf-test-element/$id", result, context);
    }
    if (response.containsKey("body") && response["body"] is String == false) {
      if (response["body"].containsKey("errorMessage") && mounted) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        futureListAll = getListTfAll();
        return true;
      }
    }
    return false;
  }

  handleDeleteAll(id, type) async {
    var response = {};
    if (type == "elementRepository") {
      response = await httpDelete("/test-framework-api/user/test-framework/tf-test-element-repository/$id", context);
    }
    if (type == "element" && mounted) {
      response = await httpDelete("/test-framework-api/user/test-framework/tf-test-element/$id", context);
    }

    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        if (type == "elementRepository") {
          futureListAll = getListTfElementRepository(currentPageElementRepository, findRequestElementRepository);
        }
        if (type == "element") {
          futureListAll = getListTfElement(findRequestElement);
        }
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
      }
    }
  }

  handleDeleteAllElement(id) async {
    var response = await httpDelete("/test-framework-api/user/test-framework/tf-test-element-repository/$id/tf-test-element", context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
      }
    }
  }

  handleDuplicateElement(result) async {
    var response = await httpPut("/test-framework-api/user/test-framework/tf-test-element", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        Navigator.of(context).pop();
        futureListAll = getListTfAll();
        showToast(
            msg: multiLanguageString(name: "duplicate_successful", defaultValue: "Duplicate successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.error),
            context: context);
      }
    }
  }

  onSearchOffset(double? dx, double? dy) {
    setState(() {
      resultListElement = resultBeforeFilterListElement.where((element) {
        if (element["content"]["position"] != null) {
          return (dx != null
                  ? (element["content"]["position"]["x"] <= dx) &&
                      (element["content"]["position"]["x"] + element["content"]["position"]["width"] >= dx)
                  : true) &&
              (dy != null
                  ? (element["content"]["position"]["y"] <= dy) &&
                      (element["content"]["position"]["y"] + element["content"]["position"]["height"] >= dy)
                  : true);
        }
        return false;
      }).toList();
    });
  }

  @override
  void dispose() {
    _searchLibrary.dispose();
    _debounceTimer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<ElementRepositoryModel, LanguageModel>(
      builder: (context, elementRepositoryModel, languageModel, child) {
        return TestFrameworkRootPageWidget(
          child: LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
              if (constraints.maxWidth >= 1200 && !isViewVertical) {
                return Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: 600,
                      child: bodyPageWidget(elementRepositoryModel, true, isViewImg: false),
                    ),
                    if (hasImgSuccess)
                      Expanded(
                        child: imgWidget(elementRepositoryModel, true),
                      ),
                  ],
                );
              }
              return Stack(
                children: [
                  bodyPageWidget(elementRepositoryModel, false),
                  if (hasImgSuccess && constraints.maxWidth >= 1200)
                    GestureDetector(
                        onTap: () {
                          setState(() {
                            isViewVertical = false;
                          });
                        },
                        child: const Align(alignment: Alignment.centerRight, child: Icon(Icons.keyboard_double_arrow_left))),
                ],
              );
            },
          ),
        );
      },
    );
  }

  Column bodyPageWidget(ElementRepositoryModel elementRepositoryModel, bool isSmall, {bool isViewImg = true}) {
    return Column(
      children: [
        Expanded(
          child: ListView(
            children: [
              Padding(
                padding: isViewImg ? const EdgeInsets.fromLTRB(50, 20, 50, 10) : const EdgeInsets.fromLTRB(20, 20, 20, 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    headerWidget(),
                    const SizedBox(
                      height: 20,
                    ),
                    if (elementRepositoryPathList != null && elementRepositoryPathList!.isNotEmpty) breadCrumbWidget(elementRepositoryModel),
                    SelectionArea(
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Expanded(
                                child: SizedBox(
                                  height: 50,
                                  child: DynamicTextField(
                                    controller: _searchLibrary,
                                    onChanged: (value) {
                                      setState(() {
                                        _searchLibrary.text;
                                      });
                                      if (_debounceTimer?.isActive ?? false) {
                                        _debounceTimer?.cancel();
                                      }
                                      _debounceTimer = Timer(const Duration(milliseconds: 500), () {
                                        findRequestElementRepository["titleLike"] = "%${_searchLibrary.text}%";
                                        getListTfElementRepository(currentPageElementRepository, findRequestElementRepository);
                                      });
                                    },
                                    labelText: multiLanguageString(
                                        name: "search_element_repository_name", defaultValue: "Search element repository name", context: context),
                                    hintText: multiLanguageString(
                                        name: "by_element_repository_name", defaultValue: "By element repository name", context: context),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          if (resultListElementRepository.isNotEmpty)
                            const SizedBox(
                              height: 10,
                            ),
                          if (resultListElementRepository.isNotEmpty) elementRepositoryWidget(elementRepositoryModel),
                          if (resultListElementRepository.isNotEmpty)
                            const SizedBox(
                              height: 10,
                            ),
                          if (elementRepositoryModel.selectRepositoryId != null) selectElementTemplateWidget(elementRepositoryModel),
                          if (elementRepositoryModel.selectRepositoryId != null) elementListWidget(elementRepositoryModel, isSmall),
                          if (isViewImg && elementRepositoryModel.selectRepositoryId != null) imgWidget(elementRepositoryModel, false),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
        btnWidget(elementRepositoryModel, isViewImg),
      ],
    );
  }

  Padding imgWidget(elementRepositoryModel, bool hasArrow) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: Stack(
        children: [
          ViewImgPageUrl(
            key: UniqueKey(),
            dx: dx != null ? double.tryParse(dx!) : null,
            dy: dy != null ? double.tryParse(dy!) : null,
            positionList: positionList,
            src:
                "$baseUrl/test-framework-api/user/test-framework-file/tf-test-element-repository/${elementRepositoryModel.selectRepositoryId}/screenshot/$screenshotFileName",
            onClickOffset: (tapOffset) {
              setState(() {
                dx = tapOffset.dx.toInt().toString();
                dy = tapOffset.dy.toInt().toString();
              });
              onSearchOffset(dx != null ? double.tryParse(dx!) : null, dy != null ? double.tryParse(dy!) : null);
            },
          ),
          if (hasArrow)
            GestureDetector(
                onTap: () {
                  setState(() {
                    isViewVertical = true;
                  });
                },
                child: const Align(alignment: Alignment.centerLeft, child: Icon(Icons.keyboard_double_arrow_right))),
        ],
      ),
    );
  }

  HeaderElementWidget selectElementTemplateWidget(ElementRepositoryModel elementRepositoryModel) {
    return HeaderElementWidget(
      selectElementId: elementRepositoryModel.selectElementId,
      prjProjectId: prjProjectId,
      isShowFilterForm: isShowFilterForm,
      onClickFilterForm: () => setState(() {
        isShowFilterForm = !isShowFilterForm;
      }),
      dx: dx,
      dy: dy,
      callbackFilter: (result) async {
        findRequestElement = result;
        findRequestElement["tfTestElementRepositoryId"] = elementRepositoryModel.selectRepositoryId;
        if (result["tfFindElementTemplateId"].isNotEmpty) {
          findRequestElement["tfFindElementTemplateId"] = result["tfFindElementTemplateId"];
          elementRepositoryModel.selectElementId = result["tfFindElementTemplateId"];
          getElementTemplate(result["tfFindElementTemplateId"]);
          await getListTfElement(findRequestElement);
        } else {
          findRequestElement["tfFindElementTemplateId"] = null;
          elementRepositoryModel.selectElementId = null;
          elementTemplate = null;
          await getListTfElement(findRequestElement);
        }
        setState(() {
          dx = result["dx"];
          dy = result["dy"];
        });
        if ((dx != null && dx!.isNotEmpty) || (dy != null && dy!.isNotEmpty)) {
          onSearchOffset(dx != null ? double.tryParse(dx!) : null, dy != null ? double.tryParse(dy!) : null);
        }
      },
    );
  }

  Widget btnWidget(ElementRepositoryModel elementRepositoryModel, bool isViewImg) {
    return Padding(
      padding: isViewImg ? const EdgeInsets.fromLTRB(50, 10, 50, 10) : const EdgeInsets.fromLTRB(20, 10, 20, 10),
      child: Align(
        alignment: Alignment.centerRight,
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              Container(
                margin: const EdgeInsets.only(right: 10),
                height: 40,
                child: FloatingActionButton.extended(
                  heroTag: null,
                  onPressed: () {
                    Navigator.of(context).push(
                      createRoute(
                        OptionElementRepositoryWidget(
                          type: "create",
                          title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                          callbackOptionTest: (result) {
                            handleAddTfAll(result, "elementRepository");
                          },
                          prjProjectId: prjProjectId,
                        ),
                      ),
                    );
                  },
                  icon: const Icon(Icons.note_add, color: Color.fromRGBO(49, 49, 49, 1)),
                  label: MultiLanguageText(
                    name: "new_element_repository",
                    defaultValue: "New Element Repository",
                    isLowerCase: false,
                    style: TextStyle(
                        fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
                  ),
                  backgroundColor: const Color.fromARGB(175, 8, 196, 80),
                  elevation: 0,
                  hoverElevation: 0,
                ),
              ),
              if (elementRepositoryModel.selectRepositoryId != null)
                Container(
                  margin: const EdgeInsets.only(right: 10),
                  height: 40,
                  child: FloatingActionButton.extended(
                    heroTag: null,
                    onPressed: () {
                      Navigator.of(context).push(
                        createRoute(
                          OptionElementWidget(
                            isStartDebug: Provider.of<DebugStatusModel>(context, listen: false).debugStatus,
                            type: "create",
                            title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                            elementTemplateId: elementRepositoryModel.selectElementId,
                            callbackOptionTest: (result) {
                              handleAddTfAll(result, "element");
                            },
                            prjProjectId: prjProjectId,
                          ),
                        ),
                      );
                    },
                    icon: const Icon(Icons.note_add, color: Color.fromRGBO(49, 49, 49, 1)),
                    label: MultiLanguageText(
                      name: "new_element",
                      defaultValue: "New Element",
                      isLowerCase: false,
                      style: TextStyle(
                          fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
                    ),
                    backgroundColor: const Color.fromARGB(175, 8, 196, 80),
                    elevation: 0,
                    hoverElevation: 0,
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }

  Widget elementListWidget(ElementRepositoryModel elementRepositoryModel, bool isSmall) {
    return Container(
      padding: const EdgeInsets.only(top: 10),
      child: isSmall
          ? CustomDataTableWidget(
              minWidth: 450,
              columns: [
                Expanded(
                  flex: 2,
                  child: MultiLanguageText(name: "name", defaultValue: "Name", isLowerCase: false, style: Style(context).styleTextDataColumn),
                ),
                Expanded(
                  child: MultiLanguageText(name: "type", defaultValue: "Type", isLowerCase: false, style: Style(context).styleTextDataColumn),
                ),
                Container(
                  width: 88,
                ),
              ],
              rows: [
                for (var resultElement in resultListElement)
                  CustomDataRow(
                    cell2: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            if (elementTemplate != null && elementTemplate["content"] != null)
                              for (var resultAttribute in elementTemplate["content"]["resultAttributeList"])
                                if (resultAttribute["name"] != "position")
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text("${resultAttribute["name"].toUpperCase()}: ", style: Style(context).styleTextDataColumn),
                                      Expanded(
                                        child: Text(
                                          resultElement["content"][resultAttribute["name"]] ?? "",
                                          style: Style(context).styleTextDataCell,
                                        ),
                                      ),
                                    ],
                                  ),
                            Wrap(
                              runSpacing: 10,
                              spacing: 10,
                              children: [
                                for (var xpath in resultElement["content"]["xpathList"] ?? [])
                                  ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      backgroundColor: const Color.fromRGBO(76, 151, 255, 1),
                                    ),
                                    onPressed: () {
                                      Navigator.pop(context);
                                      widget.callback(elementRepositoryModel.selectRepositoryId, resultElement, xpath["xpath"], xpath["name"]);
                                    },
                                    child: Text(
                                      xpath["name"].toString(),
                                    ),
                                  ),
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                    cells: [
                      Expanded(
                        flex: 2,
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Text(
                            "${resultElement["name"]}",
                            style: TextStyle(
                                fontSize: context.fontSizeManager.fontSizeText14,
                                color: const Color.fromRGBO(49, 49, 49, 1),
                                decoration: TextDecoration.none),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Text(
                            "${resultElement["tfFindElementTemplate"]["title"]}",
                            style: TextStyle(
                                fontSize: context.fontSizeManager.fontSizeText14,
                                color: const Color.fromRGBO(49, 49, 49, 1),
                                decoration: TextDecoration.none),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: (resultElement["content"]["position"] != null)
                            ? IconButton(
                                icon: isSelectList[resultListElement.indexOf(resultElement)]
                                    ? const Icon(Icons.visibility_off)
                                    : const Icon(Icons.remove_red_eye_rounded),
                                onPressed: () {
                                  setState(() {
                                    int index = resultListElement.indexOf(resultElement);
                                    isSelectList[index] = !isSelectList[index];
                                    if (isSelectList[index]) {
                                      if (resultElement["content"]["position"] != null) {
                                        positionList.add(resultElement["content"]["position"]);
                                      }
                                    } else {
                                      resultElement["content"]["position"] != null && positionList.remove(resultElement["content"]["position"]);
                                    }
                                  });
                                },
                                splashRadius: 20,
                                tooltip: multiLanguageString(name: "view_position", defaultValue: "View position", context: context),
                              )
                            : const SizedBox(
                                width: 40,
                              ),
                      ),
                      menuButtonElementWidget(resultElement),
                    ],
                  ),
              ],
            )
          : CustomDataTableWidget(
              columns: [
                Expanded(
                  child: MultiLanguageText(
                    name: "name",
                    defaultValue: "Name",
                    isLowerCase: false,
                    style: Style(context).styleTextDataColumn,
                  ),
                ),
                if (elementTemplate != null && elementTemplate["content"] != null)
                  for (var resultAttribute in elementTemplate["content"]["resultAttributeList"])
                    if (resultAttribute["name"] != "position")
                      Expanded(
                        child: Text(
                          resultAttribute["name"].toUpperCase(),
                          style: Style(context).styleTextDataColumn,
                        ),
                      ),
                Expanded(
                  child: Center(
                    child: MultiLanguageText(
                      name: "create_date",
                      defaultValue: "Create date",
                      isLowerCase: false,
                      style: Style(context).styleTextDataColumn,
                    ),
                  ),
                ),
                Expanded(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      MultiLanguageText(
                        name: "status",
                        defaultValue: "Status",
                        isLowerCase: false,
                        style: Style(context).styleTextDataColumn,
                      ),
                      Tooltip(
                        message: multiLanguageString(name: "active_inactive", defaultValue: "Green = Active\nRed = Inactive ", context: context),
                        child: const Icon(
                          Icons.info,
                          color: Colors.grey,
                          size: 16,
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Center(
                    child: MultiLanguageText(
                        name: "select_type", defaultValue: "Select Type", isLowerCase: false, style: Style(context).styleTextDataColumn),
                  ),
                ),
                Container(
                  width: 88,
                ),
              ],
              rows: [
                for (var resultElement in resultListElement)
                  CustomDataRow(
                    cells: [
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: Text(
                            "${resultElement["name"]}",
                            style: Style(context).styleTextDataCell,
                          ),
                        ),
                      ),
                      if (elementTemplate != null && elementTemplate["content"] != null)
                        for (var resultAttribute in elementTemplate["content"]["resultAttributeList"])
                          if (resultAttribute["name"] != "position")
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: Text(
                                  resultElement["content"][resultAttribute["name"]] ?? "",
                                  style: Style(context).styleTextDataCell,
                                ),
                              ),
                            ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: Center(
                            child: Text(
                              DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultElement["createDate"]).toLocal()),
                              style: Style(context).styleTextDataCell,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: Center(
                            child: Badge2StatusSettingWidget(
                              status: resultElement["status"],
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                          child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Wrap(
                          children: [
                            for (var xpath in resultElement["content"]["xpathList"] ?? [])
                              Container(
                                  margin: resultElement["content"]["xpathList"].indexOf(xpath) == resultElement["content"]["xpathList"].length - 1
                                      ? const EdgeInsets.symmetric(vertical: 10)
                                      : const EdgeInsets.only(right: 10, bottom: 10, top: 10),
                                  child: ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                        backgroundColor: const Color.fromRGBO(76, 151, 255, 1),
                                      ),
                                      onPressed: () {
                                        Navigator.pop(context);
                                        widget.callback(elementRepositoryModel.selectRepositoryId, resultElement, xpath["xpath"], xpath["name"]);
                                      },
                                      child: Text(xpath["name"].toString())))
                          ],
                        ),
                      )),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: (resultElement["content"]["position"] != null)
                            ? IconButton(
                                icon: isSelectList[resultListElement.indexOf(resultElement)]
                                    ? const Icon(Icons.visibility_off)
                                    : const Icon(Icons.remove_red_eye_rounded),
                                onPressed: () {
                                  setState(() {
                                    int index = resultListElement.indexOf(resultElement);
                                    isSelectList[index] = !isSelectList[index];
                                    if (isSelectList[index]) {
                                      if (resultElement["content"]["position"] != null) {
                                        positionList.add(resultElement["content"]["position"]);
                                      }
                                    } else {
                                      resultElement["content"]["position"] != null && positionList.remove(resultElement["content"]["position"]);
                                    }
                                  });
                                },
                                splashRadius: 20,
                                tooltip: multiLanguageString(name: "view_position", defaultValue: "View position", context: context),
                              )
                            : const SizedBox(
                                width: 40,
                              ),
                      ),
                      menuButtonElementWidget(resultElement),
                    ],
                  ),
              ],
            ),
    );
  }

  PopupMenuButton<dynamic> menuButtonElementWidget(resultElement) {
    return PopupMenuButton(
      offset: const Offset(5, 50),
      tooltip: '',
      splashRadius: 10,
      icon: const Icon(
        Icons.more_vert,
      ),
      itemBuilder: (context) => [
        PopupMenuItem(
          padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
          child: ListTile(
            onTap: () {
              Navigator.pop(context);
              Navigator.of(context).push(
                createRoute(
                  OptionElementWidget(
                    isStartDebug: Provider.of<DebugStatusModel>(context, listen: false).debugStatus,
                    type: "edit",
                    title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                    id: resultElement["id"],
                    callbackOptionTest: (result) async {
                      bool resultFunction = await handleEditTfAll(result, resultElement["id"], "element");
                      if (resultFunction && mounted) {
                        Navigator.of(this.context).pop();
                        showToast(
                            context: this.context,
                            msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: this.context),
                            color: Colors.greenAccent,
                            icon: const Icon(Icons.done));
                      }
                    },
                    prjProjectId: prjProjectId,
                  ),
                ),
              );
            },
            contentPadding: const EdgeInsets.all(0),
            hoverColor: Colors.transparent,
            title: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
            leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
          ),
        ),
        PopupMenuItem(
          padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
          child: ListTile(
            onTap: () {
              Navigator.pop(context);
              dialogDeleteAll(resultElement, "element");
            },
            contentPadding: const EdgeInsets.all(0),
            hoverColor: Colors.transparent,
            title: const MultiLanguageText(name: "delete", defaultValue: "Delete"),
            leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
          ),
        ),
        PopupMenuItem(
          padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
          child: ListTile(
            onTap: () {
              Navigator.pop(context);
              Navigator.of(context).push(
                createRoute(
                  OptionElementWidget(
                    isStartDebug: Provider.of<DebugStatusModel>(context, listen: false).debugStatus,
                    type: "duplicate",
                    title: multiLanguageString(name: "duplicate", defaultValue: "Duplicate", context: context),
                    id: resultElement["id"],
                    callbackOptionTest: (result) {
                      handleDuplicateElement(result);
                    },
                    prjProjectId: prjProjectId,
                  ),
                ),
              );
            },
            contentPadding: const EdgeInsets.all(0),
            hoverColor: Colors.transparent,
            title: const MultiLanguageText(name: "duplicate", defaultValue: "Duplicate"),
            leading: const Icon(Icons.control_point_duplicate, color: Color.fromARGB(255, 112, 114, 119)),
          ),
        ),
      ],
    );
  }

  Widget elementRepositoryWidget(ElementRepositoryModel elementRepositoryModel) {
    return Column(
      children: [
        CustomDataTableWidget(
          columns: [
            Expanded(
              flex: 5,
              child: MultiLanguageText(
                name: "element_repository_name",
                defaultValue: "Element Repository Name",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              flex: 5,
              child: MultiLanguageText(
                name: "description",
                defaultValue: "Description",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              flex: 2,
              child: MultiLanguageText(
                name: "owner",
                defaultValue: "Owner",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              flex: 2,
              child: Center(
                child: MultiLanguageText(
                  name: "create_date",
                  defaultValue: "Create date",
                  isLowerCase: false,
                  style: Style(context).styleTextDataColumn,
                ),
              ),
            ),
            Container(
              width: 38,
            ),
          ],
          rows: [
            for (var resultElementRepository in resultListElementRepository)
              CustomDataRow(
                cells: [
                  Expanded(
                    flex: 5,
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Icon(Icons.folder, color: Color.fromRGBO(61, 148, 254, 1)),
                          const SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Text(
                              "${resultElementRepository["title"] ?? ""} ",
                              maxLines: 3,
                              overflow: TextOverflow.ellipsis,
                              style: Style(context).styleTextDataCell,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 5,
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        resultElementRepository["description"] ?? "",
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        resultElementRepository["sysUser"]["fullname"],
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Center(
                        child: Text(
                          DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultElementRepository["createDate"]).toLocal()),
                          style: Style(context).styleTextDataCell,
                        ),
                      ),
                    ),
                  ),
                  PopupMenuButton(
                    offset: const Offset(5, 50),
                    tooltip: '',
                    splashRadius: 10,
                    icon: const Icon(
                      Icons.more_vert,
                    ),
                    itemBuilder: (context) => [
                      PopupMenuItem(
                        padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                        child: ListTile(
                          onTap: () {
                            Navigator.pop(context);
                            Navigator.of(context).push(
                              createRoute(
                                OptionElementRepositoryWidget(
                                  type: "edit",
                                  title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                  id: resultElementRepository["id"],
                                  callbackOptionTest: (result) async {
                                    bool resultFunction = await handleEditTfAll(result, resultElementRepository["id"], "elementRepository");
                                    if (resultFunction && mounted) {
                                      Navigator.of(this.context).pop();
                                      showToast(
                                          context: this.context,
                                          msg: multiLanguageString(
                                              name: "update_successful", defaultValue: "Update successful", context: this.context),
                                          color: Colors.greenAccent,
                                          icon: const Icon(Icons.done));
                                    }
                                  },
                                  prjProjectId: prjProjectId,
                                ),
                              ),
                            );
                          },
                          contentPadding: const EdgeInsets.all(0),
                          hoverColor: Colors.transparent,
                          title: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                          leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
                        ),
                      ),
                      PopupMenuItem(
                        padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                        child: ListTile(
                          onTap: () {
                            Navigator.pop(context);
                            dialogDeleteAll(resultElementRepository, "elementRepository");
                          },
                          contentPadding: const EdgeInsets.all(0),
                          hoverColor: Colors.transparent,
                          title: const MultiLanguageText(name: "delete", defaultValue: "Delete"),
                          leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                        ),
                      ),
                      PopupMenuItem(
                        padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                        child: ListTile(
                          onTap: () {
                            Navigator.pop(context);
                            dialogDeleteAll(resultElementRepository, "delete_element");
                          },
                          contentPadding: const EdgeInsets.all(0),
                          hoverColor: Colors.transparent,
                          title: const MultiLanguageText(name: "delete_all_element", defaultValue: "Delete all element"),
                          leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                        ),
                      ),
                    ],
                  ),
                ],
                onSelectChanged: () {
                  elementRepositoryModel.selectRepositoryId = resultElementRepository["id"];
                  _searchLibrary.clear();
                  getListTfAll();
                  getElementRepository(elementRepositoryModel.selectRepositoryId);
                  getElementRepositoryPathList();
                },
              ),
          ],
        ),
        DynamicTablePagging(
          rowCountListElementRepository,
          currentPageElementRepository,
          rowPerPageElementRepository,
          pageChangeHandler: (page) {
            setState(() {
              futureListAll = getListTfElementRepository(page, findRequestElementRepository);
            });
          },
          rowPerPageChangeHandler: (rowPerPage) {
            setState(() {
              rowPerPageElementRepository = rowPerPage!;
              futureListAll = getListTfElementRepository(1, findRequestElementRepository);
            });
          },
        ),
      ],
    );
  }

  dialogDeleteAll(selectedItem, type) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogDelete(
          selectedItem: selectedItem,
          callback: () {
            if (type == "delete_element") {
              handleDeleteAllElement(selectedItem["id"]);
            } else {
              handleDeleteAll(selectedItem["id"], type);
            }
          },
          type: type,
        );
      },
    );
  }

  Widget breadCrumbWidget(ElementRepositoryModel elementRepositoryModel) {
    return Row(
      children: [
        IconButton(
          onPressed: () {
            elementRepositoryModel.selectRepositoryId = null;
            elementRepositoryModel.selectElementId = "";
            _searchLibrary.clear();
            getListTfAll();
            setState(() {
              hasImgSuccess = false;
              isViewVertical = true;
            });
            elementRepositoryPathList = [];
          },
          icon: const FaIcon(FontAwesomeIcons.house, size: 16),
          color: Colors.grey,
          splashRadius: 10,
          tooltip: multiLanguageString(name: "back_to_element_repository", defaultValue: "Back to element repository", context: context),
        ),
        for (var elementRepositoryPath in elementRepositoryPathList ?? [])
          RichText(
              text: TextSpan(children: [
            const TextSpan(
              text: " / ",
              style: TextStyle(color: Colors.grey),
            ),
            TextSpan(
                text: "${elementRepositoryPath["title"] ?? ''}",
                style: TextStyle(
                    color: elementRepositoryPathList!.last == elementRepositoryPath ? Colors.blue : Colors.grey,
                    fontSize: context.fontSizeManager.fontSizeText16,
                    fontWeight: FontWeight.bold),
                recognizer: TapGestureRecognizer()
                  ..onTap = () {
                    elementRepositoryModel.selectRepositoryId = elementRepositoryPath["id"];
                    _searchLibrary.clear();
                    getListTfAll();
                    getElementRepository(elementRepositoryModel.selectRepositoryId);
                    getElementRepositoryPathList();
                  }),
          ]))
      ],
    );
  }

  Widget headerWidget() {
    return Row(
      children: [
        Expanded(
          child: MultiLanguageText(
            name: "select_element_repository",
            defaultValue: "Select element repository",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: context.fontSizeManager.fontSizeText20, letterSpacing: 1.5),
          ),
        ),
        TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Icon(
              Icons.close,
              color: Colors.black,
            ))
      ],
    );
  }
}
