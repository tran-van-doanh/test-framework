import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/model/model.dart';
import 'dart:ui' as ui;

class ViewImgPageUrl extends StatefulWidget {
  final List? positionList;
  final double? height;
  final String src;
  final double? dx;
  final double? dy;
  final Function(Offset tapOffset)? onClickOffset;

  const ViewImgPageUrl({
    Key? key,
    this.positionList,
    this.height = 600,
    required this.src,
    this.onClickOffset,
    this.dx,
    this.dy,
  }) : super(key: key);

  @override
  State<ViewImgPageUrl> createState() => _ViewImgPageUrlState();
}

class _ViewImgPageUrlState extends State<ViewImgPageUrl> {
  final scrollHorController = ScrollController();
  final scrollVerController = ScrollController();
  late ImageProvider imageProvider;
  late Completer<ui.Image> completer;
  @override
  void initState() {
    super.initState();
    getImg();
  }

  getImg() {
    imageProvider = NetworkImage(
      widget.src,
      headers: {"Authorization": Provider.of<SecurityModel>(context, listen: false).authorization!},
    );
    completer = Completer<ui.Image>();
    final imageStream = imageProvider.resolve(const ImageConfiguration());
    imageStream.addListener(
      ImageStreamListener(
        (ImageInfo info, bool _) {
          completer.complete(info.image);
        },
        onError: (exception, stackTrace) {},
      ),
    );
  }

  @override
  void dispose() {
    scrollHorController.dispose();
    scrollVerController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        if (widget.positionList != null && widget.positionList!.isNotEmpty) {
          scrollHorController.animateTo(
            (widget.positionList!.last["x"] + (widget.positionList!.last["width"]) / 2 - (constraints.maxWidth) / 2).toDouble(),
            duration: const Duration(milliseconds: 500),
            curve: Curves.ease,
          );
          scrollVerController.animateTo(
            (widget.positionList!.last["y"] + (widget.positionList!.last["height"]) / 2 - (constraints.maxHeight) / 2).toDouble(),
            duration: const Duration(milliseconds: 500),
            curve: Curves.ease,
          );
        }
        return FutureBuilder<ui.Image>(
          future: completer.future,
          builder: (BuildContext context, AsyncSnapshot<ui.Image> snapshot) {
            if (snapshot.hasData) {
              return Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(color: const Color.fromARGB(255, 207, 205, 205)),
                ),
                width: double.infinity,
                height: widget.height,
                alignment: Alignment.center,
                child: Scrollbar(
                  thumbVisibility: true,
                  controller: scrollHorController,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    controller: scrollHorController,
                    child: SingleChildScrollView(
                      controller: scrollVerController,
                      child: SizedBox(
                        width: snapshot.data?.width.toDouble(),
                        height: snapshot.data?.height.toDouble(),
                        child: Stack(
                          children: [
                            GestureDetector(
                              onTapDown: (TapDownDetails details) {
                                Offset tapOffset = details.localPosition;
                                if (widget.onClickOffset != null) widget.onClickOffset!(tapOffset);
                              },
                              child: Center(
                                child: ClipRRect(borderRadius: BorderRadius.circular(10), child: Image(image: imageProvider)),
                              ),
                            ),
                            if (widget.positionList != null && widget.positionList!.isNotEmpty)
                              for (var position in widget.positionList!)
                                CustomPaint(
                                  painter: ImageEditor(position: position),
                                ),
                            CustomPaint(
                              key: Key("${widget.dx}${widget.dy}"),
                              painter: OffsetImageEditor(
                                  dx: widget.dx, dy: widget.dy, width: snapshot.data?.width.toDouble(), height: snapshot.data?.height.toDouble()),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              );
            } else {
              return const SizedBox.shrink();
            }
          },
        );
      },
    );
  }
}

class ImageEditor extends CustomPainter {
  final dynamic position;
  const ImageEditor({required this.position});

  @override
  void paint(Canvas canvas, Size size) {
    var strokePaint = Paint()
      ..color = const Color.fromRGBO(255, 0, 0, 0.5)
      ..strokeWidth = 2
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;
    var path = Path();
    path.moveTo(position["x"] * 1.0, position["y"] * 1.0);
    path.lineTo(position["x"] * 1.0 + position["width"] * 1.0, position["y"] * 1.0);
    path.lineTo(position["x"] * 1.0 + position["width"] * 1.0, position["y"] * 1.0 + position["height"] * 1.0);
    path.lineTo(position["x"] * 1.0, position["y"] * 1.0 + position["height"] * 1.0);
    path.close();
    canvas.drawPath(path, strokePaint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

class OffsetImageEditor extends CustomPainter {
  final double? dx;
  final double? dy;
  final double? width;
  final double? height;
  const OffsetImageEditor({
    this.dx,
    this.dy,
    this.width,
    this.height,
  });

  @override
  void paint(Canvas canvas, Size size) {
    var strokePaint = Paint()
      ..color = const Color.fromRGBO(255, 0, 0, 0.5)
      ..strokeWidth = 2
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;
    if (dx == null && dy != null) {
      canvas.drawLine(
        Offset(0, dy!),
        Offset(width!, dy!),
        strokePaint,
      );
    }
    if (dy == null && dx != null) {
      canvas.drawLine(
        Offset(dx!, 0),
        Offset(dx!, height!),
        strokePaint,
      );
    }
    if (dx != null && dy != null) {
      strokePaint.strokeWidth = 7;
      canvas.drawPoints(
        ui.PointMode.points,
        [Offset(dx!, dy!)],
        strokePaint,
      );
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
