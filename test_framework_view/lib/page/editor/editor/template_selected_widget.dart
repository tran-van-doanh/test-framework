import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/root_item_widget.dart';
import 'package:test_framework_view/type.dart';

class SearchTemplateWidget extends StatefulWidget {
  final Function onToggleSearchTemplate;
  final List<Item> listItemSelected;

  const SearchTemplateWidget({
    Key? key,
    required this.onToggleSearchTemplate,
    required this.listItemSelected,
  }) : super(key: key);

  @override
  State<SearchTemplateWidget> createState() => _SearchTemplateWidgetState();
}

class _SearchTemplateWidgetState extends State<SearchTemplateWidget> {
  List<Template> platformTemplateList = [];
  late Future futureListTemplate;
  TextEditingController filterText = TextEditingController();
  bool isViewListItemSelected = false;
  @override
  void initState() {
    super.initState();
    futureListTemplate = initPage();
  }

  initPage() async {
    await getListTemplate();
    return 0;
  }

  getListTemplate() async {
    var securityModel = Provider.of<SecurityModel>(context, listen: false);
    var findRequest = {"status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-editor-component/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      List<Template> platformTemplateList = [];
      for (var elementResponse in response["body"]["resultList"]) {
        List<String> elementPlatformsList = [];
        for (var platform in elementResponse["platformsList"] ?? []) {
          if (securityModel.hasSysOrganizationPlatforms(platform)) {
            elementPlatformsList.add(platform);
          }
        }
        if (elementPlatformsList.isNotEmpty) {
          Template template = Template(
            category: elementResponse["category"],
            type: elementResponse["type"],
            description: elementResponse["description"] ?? "",
            title: elementResponse["title"],
            widget: mapTemplateWidget(elementResponse["type"], elementResponse["title"], elementPlatformsList,
                componentConfig: elementResponse["componentConfig"]),
            platformsList: elementPlatformsList,
          );
          platformTemplateList.add(template);
        }
      }
      setState(() {
        this.platformTemplateList = platformTemplateList;
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
    filterText.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 260,
      alignment: Alignment.topCenter,
      decoration: const BoxDecoration(
        color: Colors.white,
        border: Border(
          right: BorderSide(
            width: 1.0,
            color: Color.fromRGBO(212, 212, 212, 1),
          ),
        ),
      ),
      child: (isViewListItemSelected)
          ? SingleChildScrollView(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 19),
              child: Column(
                children: [
                  Row(
                    children: [
                      IconButton(
                        onPressed: () {
                          setState(() {
                            isViewListItemSelected = false;
                          });
                        },
                        icon: const Icon(Icons.arrow_back),
                        padding: EdgeInsets.zero,
                        splashRadius: 16,
                      ),
                      MultiLanguageText(
                        name: "list_keywords",
                        defaultValue: "List keywords",
                        style: Style(context).styleTitleDialog,
                      ),
                    ],
                  ),
                  for (var item in widget.listItemSelected)
                    Padding(
                      padding: const EdgeInsets.only(bottom: 20),
                      child: Stack(
                        children: [
                          RootItemWidget(
                            item,
                            isDragAndDrop: false,
                            hasCopy: false,
                            hasPlay: false,
                            hasDelete: false,
                            hasDuplicate: false,
                            hasClick: false,
                          ),
                          Positioned.fill(child: GestureDetector(
                            onTap: () {
                              if (item.globalKey != null && item.globalKey!.currentContext != null) {
                                // item.globalKey!.currentState!.initState();
                                Scrollable.ensureVisible(item.globalKey!.currentContext!, duration: const Duration(milliseconds: 1000));
                              }
                            },
                          )),
                        ],
                      ),
                    )
                ],
              ),
            )
          : FutureBuilder(
              future: futureListTemplate,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        child: MultiLanguageText(
                          name: "search_a_keyword_title",
                          defaultValue: "Search a keyword",
                          style: Style(context).styleTitleDialog,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                        child: Container(
                          decoration: const BoxDecoration(
                            border: Border(
                              bottom: BorderSide(
                                color: Colors.white,
                                width: 2,
                              ),
                            ),
                          ),
                          padding: const EdgeInsets.only(bottom: 10),
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: Colors.white,
                            ),
                            height: 40,
                            child: DynamicTextField(
                              controller: filterText,
                              onChanged: (value) => setState(() {
                                filterText.text;
                              }),
                              hintText: multiLanguageString(name: "search_a_keyword_input", defaultValue: "Search a keyword", context: context),
                              suffixIcon: (filterText.text.isNotEmpty)
                                  ? GestureDetector(
                                      onTap: () => setState(() {
                                            filterText.clear();
                                          }),
                                      child: const Icon(Icons.close))
                                  : null,
                              prefixIcon: const Icon(Icons.search),
                              // enabledBorder: const OutlineInputBorder(
                              //   borderSide: BorderSide(
                              //     color: Colors.white,
                              //   ),
                              // ),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: SingleChildScrollView(
                          padding: const EdgeInsets.symmetric(horizontal: 19),
                          child: Column(
                            children: [
                              for (var platformTemplate in platformTemplateList)
                                if (platformTemplate.category == "control" ||
                                    platformTemplate.category == "action" ||
                                    platformTemplate.category == "assert" ||
                                    platformTemplate.category == "file" ||
                                    platformTemplate.category == "client" ||
                                    platformTemplate.category == "other")
                                  if (filterText.text.isEmpty || platformTemplate.title.toLowerCase().contains(filterText.text.toLowerCase()))
                                    Container(
                                      margin: const EdgeInsets.only(bottom: 20),
                                      child: Stack(
                                        children: [
                                          GestureDetector(
                                              onTap: () {
                                                widget.onToggleSearchTemplate(platformTemplate.type);
                                                setState(() {
                                                  isViewListItemSelected = true;
                                                });
                                              },
                                              child: platformTemplate.widget),
                                          Positioned(
                                            right: 10,
                                            top: 12,
                                            child: GestureDetector(
                                              onTap: () {
                                                showDialog<String>(
                                                  context: context,
                                                  builder: (BuildContext context) {
                                                    return AlertDialog(
                                                      contentPadding: const EdgeInsets.all(0),
                                                      title: Row(
                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                        children: [
                                                          Text(
                                                            platformTemplate.title,
                                                            style: Style(context).styleTitleDialog,
                                                          ),
                                                          TextButton(
                                                              onPressed: () => Navigator.pop(context, 'Cancel'),
                                                              child: const Icon(
                                                                Icons.close,
                                                                color: Colors.black,
                                                              ))
                                                        ],
                                                      ),
                                                      content: Container(
                                                        decoration: const BoxDecoration(
                                                            border: Border(
                                                                top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
                                                                bottom: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)))),
                                                        margin: const EdgeInsets.only(top: 20, bottom: 10),
                                                        width: 500,
                                                        padding: const EdgeInsets.all(20),
                                                        child: Text(platformTemplate.description),
                                                      ),
                                                    );
                                                  },
                                                );
                                              },
                                              child: const Icon(
                                                Icons.info,
                                                size: 20,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                            ],
                          ),
                        ),
                      )
                    ],
                  );
                } else if (snapshot.hasError) {
                  return Text('${snapshot.error}');
                }
                return const Center(child: CircularProgressIndicator());
              },
            ),
    );
  }
}
