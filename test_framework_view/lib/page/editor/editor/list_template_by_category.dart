import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/custom_draggable.dart';
import 'package:test_framework_view/common/custom_state.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/type.dart';

class ListTemplateByCategoryWidget extends StatefulWidget {
  final String testCaseType;
  final String? flyoutCategoryType;
  final Function onToggleFlyout;
  const ListTemplateByCategoryWidget({
    Key? key,
    required this.testCaseType,
    required this.flyoutCategoryType,
    required this.onToggleFlyout,
  }) : super(key: key);

  @override
  State<ListTemplateByCategoryWidget> createState() => _ListTemplateByCategoryWidgetState();
}

class _ListTemplateByCategoryWidgetState extends CustomState<ListTemplateByCategoryWidget> {
  List<Template> platformTemplateList = [];
  late Future futureListTemplate;
  TextEditingController filterText = TextEditingController();
  @override
  void initState() {
    super.initState();
    futureListTemplate = initPage();
  }

  initPage() async {
    await getListTemplate();
    return 0;
  }

  getListTemplate() async {
    var securityModel = Provider.of<SecurityModel>(context, listen: false);
    var findRequest = {"status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-editor-component/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      List<Template> platformTemplateList = [];
      for (var elementResponse in response["body"]["resultList"]) {
        List<String> elementPlatformsList = [];
        for (var platform in (elementResponse["platformsList"] ?? [])) {
          if (securityModel.hasSysOrganizationPlatforms(platform)) {
            elementPlatformsList.add(platform);
          }
        }
        if (elementPlatformsList.isNotEmpty) {
          Template template = Template(
              category: elementResponse["category"],
              type: elementResponse["type"],
              description: elementResponse["description"] ?? "",
              widget: mapTemplateWidget(elementResponse["type"], elementResponse["title"], elementPlatformsList,
                  hasBodyWidget: false, componentConfig: elementResponse["componentConfig"]),
              title: elementResponse["title"],
              platformsList: elementPlatformsList,
              componentConfig: elementResponse["componentConfig"]);
          if (widget.testCaseType != "test_suite") {
            if (elementResponse["type"] != "testCaseCall") {
              platformTemplateList.add(template);
            }
          } else {
            platformTemplateList.add(template);
          }
        }
      }
      setState(() {
        this.platformTemplateList = platformTemplateList;
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
    filterText.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.flyoutCategoryType != null) {
      return FutureBuilder(
        future: futureListTemplate,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Positioned(
              top: 0,
              left: 0,
              child: Container(
                width: 260,
                height: MediaQuery.of(context).size.height - 60,
                color: const Color(0xffdddddd),
                child: GestureDetector(
                  onTap: () {},
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(20),
                        child: Container(
                          decoration: const BoxDecoration(
                            border: Border(
                              bottom: BorderSide(
                                color: Colors.white,
                                width: 2,
                              ),
                            ),
                          ),
                          padding: const EdgeInsets.only(bottom: 10),
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: Colors.white,
                            ),
                            height: 40,
                            child: DynamicTextField(
                              controller: filterText,
                              onChanged: (value) => setState(() {
                                filterText.text;
                              }),
                              hintText: multiLanguageString(name: "search_a_keyword", defaultValue: "Search a keyword", context: context),
                              suffixIcon: (filterText.text.isNotEmpty)
                                  ? GestureDetector(
                                      onTap: () => setState(() {
                                            filterText.clear();
                                          }),
                                      child: const Icon(Icons.close))
                                  : null,
                              prefixIcon: const Icon(Icons.search),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: SingleChildScrollView(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              for (var platformTemplate in platformTemplateList)
                                if (filterText.text.isEmpty || platformTemplate.title.toLowerCase().contains(filterText.text.toLowerCase()))
                                  if (conditionCheck(platformTemplate.type, platformTemplate.category, widget.flyoutCategoryType!))
                                    Container(
                                      margin: const EdgeInsets.only(bottom: 20),
                                      child: Stack(
                                        children: [
                                          CustomDraggable<Template>(
                                            data: platformTemplate,
                                            feedback: platformTemplate.widget,
                                            childWhenDragging: mapTemplateWidget(
                                                platformTemplate.type, platformTemplate.title, platformTemplate.platformsList,
                                                componentConfig: platformTemplate.componentConfig),
                                            child: mapTemplateWidget(platformTemplate.type, platformTemplate.title, platformTemplate.platformsList,
                                                componentConfig: platformTemplate.componentConfig),
                                            onDragStarted: () => widget.onToggleFlyout(null),
                                          ),
                                          Positioned(
                                            right: 10,
                                            top: 12,
                                            child: GestureDetector(
                                              onTap: () {
                                                showDialog<String>(
                                                  context: context,
                                                  builder: (BuildContext context) {
                                                    return AlertDialog(
                                                      contentPadding: const EdgeInsets.all(0),
                                                      title: Row(
                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                        children: [
                                                          Text(
                                                            platformTemplate.title,
                                                            style: Style(context).styleTitleDialog,
                                                          ),
                                                          TextButton(
                                                              onPressed: () => Navigator.pop(context, 'Cancel'),
                                                              child: const Icon(
                                                                Icons.close,
                                                                color: Colors.black,
                                                              ))
                                                        ],
                                                      ),
                                                      content: Container(
                                                        decoration: const BoxDecoration(
                                                            border: Border(
                                                                top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
                                                                bottom: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)))),
                                                        margin: const EdgeInsets.only(top: 20, bottom: 10),
                                                        width: 500,
                                                        padding: const EdgeInsets.all(20),
                                                        child: Text(platformTemplate.description),
                                                      ),
                                                    );
                                                  },
                                                );
                                              },
                                              child: const Icon(
                                                Icons.info,
                                                size: 20,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        },
      );
    }
    return const SizedBox.shrink();
  }
}
