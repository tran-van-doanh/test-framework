import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/editor/action_button_editor/dialog_choose_element.dart';
import 'package:test_framework_view/page/editor/action_button_editor/list_action_editor.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/begin_end_widget.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/body_list_root_item_widget.dart';
import 'package:test_framework_view/page/editor/editor/comment/comment_widget.dart';
import 'package:test_framework_view/page/editor/editor/list_template_by_category.dart';
import 'package:test_framework_view/page/editor/editor/sidebar_editor.dart';
import 'package:test_framework_view/page/editor/editor/template_selected_widget.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';
import 'package:test_framework_view/page/list_test/tests/test/dialog_option_test.dart';
import 'package:test_framework_view/type.dart';
import '../../../common/custom_state.dart';

class EditorPageRootWidget extends StatelessWidget {
  final String testCaseId;
  final String? errorTestRunResultId;
  final VoidCallback callback;
  final String? tfTestDirectoryId;
  final String testCaseType;
  final bool? isOpenListComment;
  const EditorPageRootWidget(
      {Key? key,
      required this.testCaseId,
      this.errorTestRunResultId,
      required this.callback,
      this.tfTestDirectoryId,
      required this.testCaseType,
      this.isOpenListComment})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Provider.of<TestCaseConfigModel>(context, listen: false).clearProvider();
    });

    return EditorPageWidget(
        isOpenListComment: isOpenListComment,
        errorTestRunResultId: errorTestRunResultId,
        testCaseId: testCaseId,
        tfTestDirectoryId: tfTestDirectoryId,
        callback: callback,
        testCaseType: testCaseType);
  }
}

class EditorPageWidget extends StatefulWidget {
  final String testCaseId;
  final String? errorTestRunResultId;
  final VoidCallback callback;
  final String? tfTestDirectoryId;
  final String testCaseType;
  final bool? isOpenListComment;
  const EditorPageWidget(
      {Key? key,
      required this.testCaseId,
      this.errorTestRunResultId,
      required this.callback,
      this.tfTestDirectoryId,
      required this.testCaseType,
      this.isOpenListComment})
      : super(key: key);

  @override
  State<EditorPageWidget> createState() => _EditorPageWidgetState();
}

class _EditorPageWidgetState extends CustomState<EditorPageWidget> {
  bool isOpenListTemplateWidget = true;
  bool isOpenListComment = false;
  bool isOpenTemplateSelected = false;
  Item item = Item("rootDefault", "rootDefault");
  late Future futureTfTestCase;
  var errorWidgetId = [];
  List<Item> listItemSelected = [];
  String? flyoutCategoryType;
  late TestCaseConfigModel testCaseConfigModel;
  @override
  void initState() {
    testCaseConfigModel = Provider.of<TestCaseConfigModel>(context, listen: false);
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      testCaseConfigModel.addTestCaseIdList(widget.testCaseId);
      Provider.of<PlayTestCaseModel>(context, listen: false).clearProvider();
    });
    futureTfTestCase = getInfoPage();
  }

  getInfoPage() async {
    if (widget.isOpenListComment != null) {
      setState(() {
        isOpenListComment = widget.isOpenListComment!;
        isOpenListTemplateWidget = false;
        isOpenTemplateSelected = false;
      });
    }
    if (widget.errorTestRunResultId != null) {
      await getErrorIdWidget(widget.errorTestRunResultId!);
    }
    await getListTemplate();
    await getTestCase(
      widget.testCaseId,
    );

    return 0;
  }

  getListTemplate() async {
    var response = await httpPost("/test-framework-api/user/test-framework/tf-editor-component/search", {"status": 1}, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      Provider.of<TestCaseConfigModel>(context, listen: false).setEditorComponentList(response["body"]["resultList"]);
    }
  }

  getErrorIdWidget(String id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-run-data/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        String? responseErrorActionPath = response["body"]["result"]["errorActionPath"];
        errorWidgetId = [];
        if (responseErrorActionPath != null) {
          errorWidgetId = responseErrorActionPath.split("/");
        }
      });
    }
  }

  Future getTestCase(String idTestCase) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-case/$idTestCase", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      if (response["body"].isEmpty && mounted) {
        Provider.of<NavigationModel>(context, listen: false).navigate("/qa-platform/404-page-not-found");
      } else {
        setState(() {
          testCaseConfigModel.addTestCaseNameList(response["body"]["result"]["title"]);
          if (response["body"]["result"]["content"] != null &&
              response["body"]["result"]["content"]["bodyList"] != null &&
              response["body"]["result"]["content"]["bodyList"].isNotEmpty) {
            testCaseConfigModel.setClientList(response["body"]["result"]["content"]["clientList"] ?? []);
            testCaseConfigModel.setParameterFieldList(response["body"]["result"]["content"]["parameterFieldList"] ?? []);
            testCaseConfigModel.setOutputFieldList(response["body"]["result"]["content"]["outputFieldList"] ?? []);
            for (var bodyListTestCase in response["body"]["result"]["content"]["bodyList"]) {
              checkTypeWidget(
                  bodyListTestCase, item.bodyList, response["body"]["result"]["content"]["bodyList"].indexOf(bodyListTestCase), errorWidgetId);
            }
            item.parameterFieldList = response["body"]["result"]["content"]["parameterFieldList"] ?? [];
          } else {
            setState(() {
              Item newStep = Item("step", "other");
              Item newClient = Item("client", "client");
              newClient.name = "default_client";
              newClient.platform = "Web";
              newStep.bodyList.add(newClient);
              item.bodyList.add(newStep);
              var client = {
                "name": "default_client",
                "platform": "Web",
              };
              WidgetsBinding.instance.addPostFrameCallback((_) {
                testCaseConfigModel.addClientList(client);
              });
            });
          }
        });
      }
    }
  }

  checkTypeWidget(dynamic listCheckType, bodyTypeAdd, int index, List? errorWidgetId) {
    Item itemBodyList = checkItemWidget(listCheckType, errorWidgetId);
    bodyTypeAdd.insert(index, itemBodyList);
  }

  checkItemWidget(dynamic listCheckType, List? errorWidgetId) {
    if (listCheckType["type"] == "waitElement") {
      listCheckType["type"] = "waitElementExists";
    }
    if (listCheckType["type"] == "checkAlterExists") {
      listCheckType["type"] = "checkAlertExists";
    }
    if (listCheckType["type"] == "checkElement") {
      listCheckType["type"] = "checkElementExists";
    }
    if (listCheckType["type"] == "childElementCheckElement") {
      listCheckType["type"] = "childElementCheckElementExists";
    }
    if (listCheckType["type"] == "sendKey") {
      listCheckType["type"] = "sendKeyFromElement";
    }
    if (listCheckType["type"] == "childElementSendKey") {
      listCheckType["type"] = "childElementSendKeyFromElement";
    }
    if (listCheckType["type"] == "error" && listCheckType["message"] != null) {
      listCheckType["variablePath"] = [
        {"type": "value", "value": listCheckType["message"], "category": "variablePath", "dataType": "String"}
      ];
    }
    Item itemBodyList = Item(listCheckType["type"] ?? "", listCheckType["category"] ?? "");
    if (listCheckType["type"] == "value" ||
        listCheckType["type"] == "variable" ||
        listCheckType["type"] == "version_variable" ||
        listCheckType["type"] == "environment_variable" ||
        listCheckType["type"] == "node_variable" ||
        listCheckType["type"] == "get") {
      itemBodyList = Item(listCheckType["type"], "variablePath");
    }
    if (listCheckType["type"] == "value") {
      if (listCheckType["tfTestElementId"] != null) {
        itemBodyList.tfTestElementId = listCheckType["tfTestElementId"];
      }
      if (listCheckType["tfTestElementRepositoryId"] != null) {
        itemBodyList.tfTestElementRepositoryId = listCheckType["tfTestElementRepositoryId"];
      }
      if (listCheckType["tfValueTemplateId"] != null) {
        itemBodyList.tfValueTemplateId = listCheckType["tfValueTemplateId"];
      }
    }
    if (listCheckType["type"] == "variable_default") {
      itemBodyList = Item(listCheckType["type"], "variable_default");
      if (listCheckType["variablePath"] != null) {
        for (var i = 0; i < listCheckType["variablePath"].length; i++) {
          checkTypeWidget(listCheckType["variablePath"][i], itemBodyList.variablePath, i, errorWidgetId);
        }
      }
    }
    if (listCheckType["id"] != null) itemBodyList.id = listCheckType["id"];
    if (listCheckType["key"] != null) itemBodyList.key = listCheckType["key"];
    if (listCheckType["mouseActionType"] != null) itemBodyList.mouseActionType = listCheckType["mouseActionType"];
    if (listCheckType["endKey"] != null) itemBodyList.endKey = listCheckType["endKey"];
    if (listCheckType["thenBodyList"] != null) {
      for (var thenBody in listCheckType["thenBodyList"]) {
        checkTypeWidget(thenBody, itemBodyList.thenBodyList, listCheckType["thenBodyList"].indexOf(thenBody), errorWidgetId);
      }
    }
    if (listCheckType["elseBodyList"] != null) {
      for (var elseBody in listCheckType["elseBodyList"]) {
        checkTypeWidget(elseBody, itemBodyList.elseBodyList, listCheckType["elseBodyList"].indexOf(elseBody), errorWidgetId);
      }
    }
    if (listCheckType["onErrorBodyList"] != null) {
      for (var errorBody in listCheckType["onErrorBodyList"]) {
        checkTypeWidget(errorBody, itemBodyList.onErrorBodyList, listCheckType["onErrorBodyList"].indexOf(errorBody), errorWidgetId);
      }
    }
    if (listCheckType["onFinishBodyList"] != null) {
      for (var finalBody in listCheckType["onFinishBodyList"]) {
        checkTypeWidget(finalBody, itemBodyList.onFinishBodyList, listCheckType["onFinishBodyList"].indexOf(finalBody), errorWidgetId);
      }
    }
    if (listCheckType["bodyList"] != null) {
      for (var body in listCheckType["bodyList"]) {
        checkTypeWidget(body, itemBodyList.bodyList, listCheckType["bodyList"].indexOf(body), errorWidgetId);
      }
    }
    if (listCheckType["variablePath"] != null) {
      itemBodyList.variablePath = [];
      for (var list in listCheckType["variablePath"]) {
        checkTypeWidget(list, itemBodyList.variablePath, listCheckType["variablePath"].indexOf(list), errorWidgetId);
      }
    }
    if (listCheckType["conditionList"] != null) {
      if (listCheckType["conditionList"].length == 1) {
        if (listCheckType["type"] == "NOT") {
          itemBodyList.conditionList = [];
        } else {
          itemBodyList.conditionList = [Item("condition_default", "condition_default")];
        }
        checkTypeWidget(listCheckType["conditionList"][0], itemBodyList.conditionList, 0, errorWidgetId);
      } else {
        itemBodyList.conditionList = [];
        for (var i = 0; i < listCheckType["conditionList"].length; i++) {
          checkTypeWidget(listCheckType["conditionList"][i], itemBodyList.conditionList, i, errorWidgetId);
        }
      }
    }
    if (listCheckType["operatorList"] != null) {
      if (listCheckType["operatorList"].length == 1) {
        itemBodyList.operatorList = [Item("variable_default", "variable_default")];
        checkTypeWidget(listCheckType["operatorList"][0], itemBodyList.operatorList, 0, errorWidgetId);
      } else {
        itemBodyList.operatorList = [];
        for (var i = 0; i < listCheckType["operatorList"].length; i++) {
          checkTypeWidget(listCheckType["operatorList"][i], itemBodyList.operatorList, i, errorWidgetId);
        }
      }
    }
    if (listCheckType["conditionGroup"] != null) {
      itemBodyList.conditionGroup = checkItemWidget(listCheckType["conditionGroup"], errorWidgetId);
    }
    if (listCheckType["name"] != null) {
      itemBodyList.name = listCheckType["name"];
    }
    if (listCheckType["platform"] != null) {
      itemBodyList.platform = listCheckType["platform"];
    }
    if (listCheckType["clientName"] != null) {
      itemBodyList.clientName = listCheckType["clientName"];
    }
    if (listCheckType["clientList"] != null) {
      itemBodyList.clientList = listCheckType["clientList"];
    }
    if (listCheckType["description"] != null) {
      itemBodyList.description = listCheckType["description"];
    }
    if (listCheckType["value"] != null) {
      itemBodyList.value = listCheckType["value"];
    }
    if (listCheckType["dataType"] != null) {
      itemBodyList.dataType = listCheckType["dataType"];
    }
    if (listCheckType["type"] == "actionCall" && listCheckType["actionName"] != null) {
      itemBodyList.actionName = listCheckType["actionName"];
    }
    if (listCheckType["type"] == "testCaseCall" && listCheckType["testCaseName"] != null) {
      itemBodyList.testCaseName = listCheckType["testCaseName"];
    }
    if (listCheckType["type"] == "apiCall" && listCheckType["apiName"] != null) {
      itemBodyList.apiName = listCheckType["apiName"];
    }
    if (listCheckType["objectName"] != null) {
      itemBodyList.objectName = listCheckType["objectName"];
      if (!testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["newVariableList"]
          .map((newVariable) => newVariable["name"])
          .contains(itemBodyList.objectName)) {
        testCaseConfigModel.addNewVariableList({
          "name": itemBodyList.objectName,
          "dataType": itemBodyList.dataType,
          "parentId": itemBodyList.id,
        });
      }
    }
    if (listCheckType["operatorType"] != null) {
      itemBodyList.operatorType = listCheckType["operatorType"];
    }
    if (listCheckType["message"] != null) {
      itemBodyList.message = listCheckType["message"];
    }
    if (listCheckType["isList"] != null) {
      itemBodyList.isList = listCheckType["isList"];
    }
    if (listCheckType["returnType"] != null) {
      itemBodyList.returnType = listCheckType["returnType"];
      itemBodyList.returnType = listCheckType["returnType"];
      if (itemBodyList.returnType["type"] == "newVariable") {
        var returnType = Map.from(itemBodyList.returnType);
        if (!testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["newVariableList"]
            .map((newVariable) => newVariable["name"])
            .contains(returnType["name"])) {
          Map<dynamic, dynamic> value = Map.from(returnType);
          value["parentId"] = itemBodyList.id;
          testCaseConfigModel.addNewVariableList(value);
        }
      }
    }
    if (listCheckType["o1"] != null) {
      itemBodyList.o1 = checkItemWidget(listCheckType["o1"], errorWidgetId);
    }
    if (listCheckType["o2"] != null) {
      itemBodyList.o2 = checkItemWidget(listCheckType["o2"], errorWidgetId);
    }
    if (listCheckType["parameterList"] != null) {
      itemBodyList.parameterList = listCheckType["parameterList"];
    }
    if ((listCheckType["type"] == "newVariable" ||
            listCheckType["type"] == "assignValue" ||
            listCheckType["type"] == "sleep" ||
            listCheckType["type"] == "error") &&
        (listCheckType["name"] != null)) {
      if (!testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["newVariableList"]
          .map((newVariable) => newVariable["name"])
          .contains(itemBodyList.name)) {
        testCaseConfigModel.addNewVariableList({
          "name": itemBodyList.name,
          "dataType": itemBodyList.dataType,
          "parentId": itemBodyList.id,
        });
      }
    }
    GlobalKey<State<StatefulWidget>> globalKey = GlobalKey();
    itemBodyList.globalKey = globalKey;
    if (errorWidgetId != null) {
      for (var errorId in errorWidgetId) {
        if (errorId == listCheckType["id"]) {
          itemBodyList.isError = true;
          WidgetsBinding.instance.addPostFrameCallback((_) {
            Scrollable.ensureVisible(globalKey.currentContext!, duration: const Duration(milliseconds: 1000));
          });
        }
      }
    }
    return itemBodyList;
  }

  onFilterTemplate(String type, List<Item> bodyList) {
    for (Item item in bodyList) {
      if (item.id != null && item.type == type) {
        setState(() {
          listItemSelected.add(item);
        });
      }
      if (item.thenBodyList.isNotEmpty) {
        onFilterTemplate(type, item.thenBodyList);
      }
      if (item.elseBodyList.isNotEmpty) {
        onFilterTemplate(type, item.elseBodyList);
      }
      if (item.bodyList.isNotEmpty) {
        onFilterTemplate(type, item.bodyList);
      }
      if (item.onErrorBodyList.isNotEmpty) {
        onFilterTemplate(type, item.onErrorBodyList);
      }
      if (item.onFinishBodyList.isNotEmpty) {
        onFilterTemplate(type, item.onFinishBodyList);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return FutureBuilder(
        future: futureTfTestCase,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return TestFrameworkRootPageWidget(
              child: Row(
                children: [
                  if (isOpenListTemplateWidget)
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          flyoutCategoryType = null;
                        });
                      },
                      child: SideBarEditorWidget(
                        flyoutCategoryType: flyoutCategoryType,
                        tfTestCaseId: widget.testCaseId,
                        onToggleFlyout: (isOpenFlyoutNew) {
                          setState(() {
                            flyoutCategoryType = isOpenFlyoutNew;
                          });
                        },
                        onClickHistoryTestCase: (content) async {
                          onLoading(context);
                          if (content["bodyList"] != null) {
                            item.bodyList = [];
                            for (var body in content["bodyList"]) {
                              await checkTypeWidget(body, item.bodyList, content["bodyList"].indexOf(body), errorWidgetId);
                            }
                          }
                          if (content["parameterFieldList"] != null) {
                            testCaseConfigModel.setParameterFieldList(content["parameterFieldList"]);
                            testCaseConfigModel.setParameterHistory(true);
                          }
                          if (content["clientList"] != null && mounted) {
                            testCaseConfigModel.setClientList(content["clientList"]);
                            testCaseConfigModel.setClientListHistory(true);
                          }
                          if (mounted) {
                            Navigator.pop(this.context);
                          }
                          setState(() {});
                        },
                      ),
                    ),
                  if (isOpenListComment)
                    CommentTestWidget(
                      objectType: "test_case",
                      objectId: widget.testCaseId,
                    ),
                  if (isOpenTemplateSelected)
                    SearchTemplateWidget(
                      listItemSelected: listItemSelected,
                      onToggleSearchTemplate: (type) {
                        listItemSelected = [];
                        onFilterTemplate(type, item.bodyList);
                      },
                    ),
                  RootDefaultWidget(
                    flyoutCategoryType: flyoutCategoryType,
                    testCaseType: widget.testCaseType,
                    testCaseId: widget.testCaseId,
                    isOpenListComment: isOpenListComment,
                    isOpenTemplateSelected: isOpenTemplateSelected,
                    onViewConfig: (testCaseType, testCaseId) {
                      Navigator.of(context).push(
                        createRoute(
                          EditorPageWidget(
                            testCaseType: testCaseType,
                            testCaseId: testCaseId,
                            errorTestRunResultId: widget.errorTestRunResultId,
                            callback: () {},
                          ),
                        ),
                      );
                    },
                    callbackToggleListTemplate: () {
                      setState(() {
                        isOpenListComment = false;
                        isOpenTemplateSelected = false;
                        isOpenListTemplateWidget = !isOpenListTemplateWidget;
                      });
                    },
                    callbackToggleTemplateSelected: () {
                      setState(() {
                        isOpenListTemplateWidget = isOpenTemplateSelected;
                        isOpenListComment = false;
                        isOpenTemplateSelected = !isOpenTemplateSelected;
                      });
                    },
                    callbackToggleListComment: () {
                      setState(() {
                        isOpenListTemplateWidget = isOpenListComment;
                        isOpenTemplateSelected = false;
                        isOpenListComment = !isOpenListComment;
                      });
                    },
                    isOpenListTemplateWidget: isOpenListTemplateWidget,
                    item: item,
                    callbackBackButton: () {
                      widget.callback();
                    },
                    onToggleFlyout: (isOpenFlyoutNew) {
                      setState(() {
                        flyoutCategoryType = isOpenFlyoutNew;
                      });
                    },
                    tfTestDirectoryId: widget.tfTestDirectoryId,
                    onUpdateBodyItemTestCase: (testCaseItem) async {
                      setState(() {
                        listItemSelected = [];
                        isOpenListComment = false;
                        isOpenTemplateSelected = false;
                        isOpenListTemplateWidget = true;
                      });
                      var response = await httpGet("/test-framework-api/user/test-framework/tf-test-case/${testCaseItem["id"]}", context);
                      if (response.containsKey("body") && response["body"] is String == false) {
                        if (response["body"].isEmpty && mounted) {
                          Provider.of<NavigationModel>(this.context, listen: false).navigate("/qa-platform/404-page-not-found");
                        } else {
                          setState(() {
                            testCaseConfigModel.updateTestCaseNameLast(response["body"]["result"]["title"]);
                            if (response["body"]["result"]["content"] != null &&
                                response["body"]["result"]["content"]["bodyList"] != null &&
                                response["body"]["result"]["content"]["bodyList"].isNotEmpty) {
                              testCaseConfigModel.setClientList(response["body"]["result"]["content"]["clientList"] ?? []);
                              testCaseConfigModel.setParameterFieldList(response["body"]["result"]["content"]["parameterFieldList"] ?? []);
                              testCaseConfigModel.setOutputFieldList(response["body"]["result"]["content"]["outputFieldList"] ?? []);
                              item.bodyList.clear();
                              for (var bodyListTestCase in response["body"]["result"]["content"]["bodyList"]) {
                                checkTypeWidget(bodyListTestCase, item.bodyList,
                                    response["body"]["result"]["content"]["bodyList"].indexOf(bodyListTestCase), errorWidgetId);
                              }
                              item.parameterFieldList = response["body"]["result"]["content"]["parameterFieldList"] ?? [];
                            }
                          });
                        }
                      }
                    },
                  ),
                ],
              ),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        },
      );
    });
  }
}

class RootDefaultWidget extends StatefulWidget {
  final Item item;
  final String testCaseId;
  final VoidCallback callbackToggleListTemplate;
  final VoidCallback callbackToggleListComment;
  final VoidCallback callbackToggleTemplateSelected;
  final bool isOpenListTemplateWidget;
  final bool isOpenListComment;
  final bool isOpenTemplateSelected;
  final VoidCallback callbackBackButton;
  final Function? onViewConfig;
  final String? tfTestDirectoryId;
  final String testCaseType;
  final String? flyoutCategoryType;
  final Function onToggleFlyout;
  final Function onUpdateBodyItemTestCase;
  const RootDefaultWidget({
    Key? key,
    required this.testCaseId,
    required this.callbackToggleListTemplate,
    required this.isOpenListTemplateWidget,
    required this.item,
    required this.callbackBackButton,
    this.tfTestDirectoryId,
    required this.testCaseType,
    required this.flyoutCategoryType,
    required this.onToggleFlyout,
    required this.isOpenListComment,
    required this.callbackToggleListComment,
    required this.isOpenTemplateSelected,
    required this.callbackToggleTemplateSelected,
    required this.onUpdateBodyItemTestCase,
    this.onViewConfig,
  }) : super(key: key);

  @override
  State<RootDefaultWidget> createState() => _RootDefaultWidgetState();
}

class _RootDefaultWidgetState extends CustomState<RootDefaultWidget> {
  var testCaseItem = {};

  @override
  void initState() {
    getTestCase();
    super.initState();
  }

  getTestCase() async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-case/${widget.testCaseId}", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      if (response["body"].isEmpty && mounted) {
        Provider.of<NavigationModel>(context, listen: false).navigate("/qa-platform/404-page-not-found");
      } else {
        setState(() {
          testCaseItem = response["body"]["result"];
        });
      }
    }
    return 0;
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<DebugStatusModel, ModeViewEditorModel>(
      builder: (context, debugStatusModel, modeViewEditorModel, child) {
        return Expanded(
          child: GestureDetector(
            onTap: () => widget.onToggleFlyout(null),
            child: Container(
              decoration: BoxDecoration(border: Border.all(color: Colors.transparent)),
              child: Stack(
                children: [
                  Column(
                    children: [
                      Expanded(
                        child: Column(
                          children: [
                            const Padding(
                              padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                              child: BeginWidget(),
                            ),
                            Expanded(
                                child: Padding(
                              padding: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                              child: ListView(
                                children: [
                                  BodyListRootItemWidget(
                                    widget.item.bodyList,
                                    widget.item.type,
                                    2,
                                    tfTestCaseId: testCaseItem["id"],
                                    hasCheckBox: false,
                                    onViewConfig: (testCaseType, testCaseId) {
                                      widget.onViewConfig!(testCaseType, testCaseId);
                                    },
                                  ),
                                  EndRootWidget(
                                    callback: (newItem) {
                                      setState(() {
                                        widget.item.bodyList.add(newItem);
                                      });
                                    },
                                  ),
                                ],
                              ),
                            ))
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                        child: OptionEditorWidget(
                          testCaseType: widget.testCaseType,
                          testCaseItem: testCaseItem,
                          bodyList: widget.item.bodyList,
                          testCaseId: testCaseItem["id"],
                          parameterFieldList: widget.item.parameterFieldList,
                          callback: () {
                            widget.callbackBackButton();
                          },
                          callbackAddTest: (value) {
                            setState(() {
                              testCaseItem = value;
                            });
                            widget.onUpdateBodyItemTestCase(testCaseItem);
                          },
                          tfTestDirectoryId: widget.tfTestDirectoryId,
                        ),
                      )
                    ],
                  ),
                  Positioned(
                    top: 13,
                    left: 10,
                    child: IconButton(
                      padding: const EdgeInsets.all(5),
                      tooltip: widget.isOpenListTemplateWidget
                          ? multiLanguageString(name: "collapse_list_keywords", defaultValue: "Collapse list keywords", context: context)
                          : multiLanguageString(name: "expand_list_keywords", defaultValue: "Expand list keywords", context: context),
                      icon: const Icon(
                        Icons.menu,
                        color: Colors.white,
                      ),
                      splashRadius: 20,
                      onPressed: () {
                        widget.callbackToggleListTemplate();
                      },
                    ),
                  ),
                  Positioned(
                    top: 13,
                    left: 45,
                    child: IconButton(
                      padding: const EdgeInsets.all(5),
                      tooltip: widget.isOpenListComment
                          ? multiLanguageString(name: "close_comment", defaultValue: "Close comment", context: context)
                          : multiLanguageString(name: "open_comment", defaultValue: "Open comment", context: context),
                      icon: const Icon(
                        Icons.comment,
                        color: Colors.white,
                      ),
                      splashRadius: 20,
                      onPressed: () {
                        widget.callbackToggleListComment();
                      },
                    ),
                  ),
                  Positioned(
                    top: 13,
                    left: 80,
                    child: IconButton(
                      padding: const EdgeInsets.all(5),
                      tooltip: widget.isOpenTemplateSelected
                          ? multiLanguageString(name: "close_search", defaultValue: "Close search", context: context)
                          : multiLanguageString(name: "open_search", defaultValue: "Open search", context: context),
                      icon: const Icon(
                        Icons.search,
                        color: Colors.white,
                      ),
                      splashRadius: 20,
                      onPressed: () {
                        widget.callbackToggleTemplateSelected();
                      },
                    ),
                  ),
                  Positioned(
                    top: 13,
                    right: 10,
                    child: Row(
                      children: [
                        if (debugStatusModel.debugStatus)
                          IconButton(
                            padding: const EdgeInsets.all(5),
                            icon: const Icon(
                              Icons.playlist_play,
                              color: Colors.white,
                            ),
                            splashRadius: 20,
                            onPressed: () {
                              Provider.of<ListElementPlayIdModel>(context, listen: false).clearProvider();
                              showDialog<String>(
                                context: context,
                                builder: (BuildContext context) {
                                  return DialogChooseElement(
                                    tfTestCaseId: testCaseItem["id"],
                                  );
                                },
                              );
                            },
                          ),
                        Row(
                          children: [
                            const MultiLanguageText(
                              name: "config",
                              defaultValue: "Config",
                              style: TextStyle(color: Colors.white),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 2),
                              child: FlutterSwitch(
                                value: modeViewEditorModel.isStepMode,
                                toggleSize: 20,
                                width: 40,
                                height: 25,
                                padding: 1,
                                toggleColor: Colors.white,
                                switchBorder: Border.all(
                                  color: const Color.fromRGBO(2, 107, 206, 1),
                                  width: 1.0,
                                ),
                                toggleBorder: Border.all(
                                  color: const Color.fromRGBO(2, 107, 206, 1),
                                  width: 1.0,
                                ),
                                activeColor: const Color.fromRGBO(36, 100, 212, 1),
                                inactiveColor: const Color.fromRGBO(36, 100, 212, 1),
                                onToggle: (val) {
                                  modeViewEditorModel.setIsStepMode(val);
                                },
                              ),
                            ),
                            const MultiLanguageText(
                              name: "flow",
                              defaultValue: "Flow",
                              style: TextStyle(color: Colors.white),
                            ),
                          ],
                        ),
                        IconButton(
                          padding: const EdgeInsets.all(5),
                          icon: const Icon(
                            Icons.settings,
                            color: Colors.white,
                          ),
                          splashRadius: 20,
                          onPressed: () {
                            Navigator.of(context).push(
                              createRoute(
                                DialogOptionTestWidget(
                                  testCaseType: widget.testCaseType,
                                  type: "edit",
                                  title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                  selectedItem: testCaseItem,
                                  callbackOptionTest: (result) {
                                    setState(() {
                                      testCaseItem = result;
                                      Navigator.pop(context);
                                    });
                                  },
                                  parentId: testCaseItem["tfTestDirectoryId"],
                                ),
                              ),
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                  ListTemplateByCategoryWidget(
                    testCaseType: widget.testCaseType,
                    flyoutCategoryType: widget.flyoutCategoryType,
                    onToggleFlyout: (isOpenFlyoutNew) => widget.onToggleFlyout(isOpenFlyoutNew),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
