import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';

class MentionListWidget extends StatelessWidget {
  final dynamic mention;
  final VoidCallback? onRemove;
  const MentionListWidget({Key? key, required this.mention, this.onRemove}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 2),
      decoration: BoxDecoration(
        color: Colors.grey.withOpacity(0.5),
        border: const Border(
          left: BorderSide(color: Colors.grey, width: 4),
        ),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Text(
              mention["sysUser"]["fullname"],
              style: Style(context).styleTextDataCell,
            ),
          ),
          Row(
            children: [
              if (onRemove != null)
                GestureDetector(
                  onTap: () => onRemove!(),
                  child: const Icon(Icons.close),
                ),
            ],
          )
        ],
      ),
    );
  }
}

class ManageMentionWidget extends StatefulWidget {
  final List? mentionList;
  final Function onSave;
  const ManageMentionWidget({Key? key, required this.mentionList, required this.onSave}) : super(key: key);

  @override
  State<ManageMentionWidget> createState() => _ManageMentionWidgetState();
}

class _ManageMentionWidgetState extends State<ManageMentionWidget> {
  final TextEditingController fullnameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  List resultListUser = [];
  var rowCountListMention = 0;
  var currentPage = 1;
  var rowPerPage = 10;
  bool isShowFilterForm = true;
  List mentionList = [];
  bool isLoad = false;
  var prjProjectId = "";
  @override
  void initState() {
    prjProjectId = Provider.of<NavigationModel>(context, listen: false).prjProjectId ?? "";
    super.initState();
    setInitValue();
  }

  setInitValue() async {
    if (widget.mentionList != null && widget.mentionList!.isNotEmpty) {
      mentionList.addAll(widget.mentionList!);
    }
    await handleSearchMention(currentPage);
    isLoad = true;
  }

  handleSearchMention(page) async {
    var findRequest = {
      "queryOffset": (page - 1) * rowPerPage,
      "queryLimit": rowPerPage,
      "prjProjectId": prjProjectId,
    };
    findRequest["fullnameLike"] = fullnameController.text.isNotEmpty ? "%${fullnameController.text}%" : null;
    findRequest["emailLike"] = emailController.text.isNotEmpty ? "%${emailController.text}%" : null;
    var response = await httpPost("/test-framework-api/user/sys/sys-user-project/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPage = page;
        rowCountListMention = response["body"]["rowCount"];
        resultListUser = response["body"]["resultList"];
      });
    }
    return 0;
  }

  @override
  void dispose() {
    super.dispose();
    fullnameController.dispose();
    emailController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return isLoad
          ? TestFrameworkRootPageWidget(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(50, 30, 50, 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            headerWidget(context),
                            const SizedBox(
                              height: 20,
                            ),
                            filterFormWidget(),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                    child: tableMentionWidget(
                                        multiLanguageString(name: "list_user_search", defaultValue: "List User Search", context: context),
                                        "List User Search",
                                        resultListUser,
                                        mentionList)),
                                const SizedBox(
                                  width: 10,
                                ),
                                Expanded(
                                    child: tableMentionWidget(multiLanguageString(name: "list_user", defaultValue: "List User", context: context),
                                        "List User", mentionList, resultListUser))
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  btnWidget(context),
                ],
              ),
            )
          : const Center(child: CircularProgressIndicator());
    });
  }

  Column tableMentionWidget(String title, String mention, List list, List list1) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: Style(context).styleTitleDialog,
        ),
        CustomDataTableWidget(
          minWidth: 500,
          columns: [
            Expanded(
              child: MultiLanguageText(
                name: "fullname",
                defaultValue: "Fullname",
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "email",
                defaultValue: "Email",
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Container(
              width: 56,
            ),
          ],
          rows: [
            for (var item in list)
              CustomDataRow(
                cells: [
                  Expanded(
                    child: Text(
                      item["sysUser"]["fullname"],
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                  Expanded(
                    child: Text(
                      item["sysUser"]["email"],
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                  SizedBox(
                    width: 56,
                    height: 35,
                    child: (mention == "List User Search" &&
                            !list1.any(
                              (element) => element["id"] == item["id"],
                            ))
                        ? ElevatedButton(
                            onPressed: () {
                              setState(() {
                                list1.add(item);
                              });
                            },
                            child: const Icon(Icons.add),
                          )
                        : (mention == "List User")
                            ? ElevatedButton(
                                onPressed: () {
                                  setState(() {
                                    list.remove(item);
                                  });
                                },
                                child: const Icon(Icons.remove),
                              )
                            : const SizedBox.shrink(),
                  ),
                ],
              ),
          ],
        ),
        if (mention == "List User Search")
          DynamicTablePagging(
            rowCountListMention,
            currentPage,
            rowPerPage,
            pageChangeHandler: (page) {
              handleSearchMention(page);
            },
            rowPerPageChangeHandler: (rowPerPage) {
              setState(() {
                rowPerPage = rowPerPage!;
                handleSearchMention(1);
              });
            },
          ),
      ],
    );
  }

  Column filterFormWidget() {
    return Column(
      children: [
        Row(
          children: [
            MultiLanguageText(
              name: "filter_form",
              defaultValue: "Filter form",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: const Color.fromRGBO(0, 0, 0, 0.8),
                  fontSize: context.fontSizeManager.fontSizeText16,
                  letterSpacing: 2),
            ),
            IconButton(
              onPressed: () {
                setState(() {
                  isShowFilterForm = !isShowFilterForm;
                });
              },
              icon: isShowFilterForm ? const Icon(Icons.visibility) : const Icon(Icons.visibility_off),
              splashRadius: 1,
              tooltip: isShowFilterForm
                  ? multiLanguageString(name: "hide_filter_form", defaultValue: "Hide filter form", context: context)
                  : multiLanguageString(name: "show_filter_form", defaultValue: "Show filter form", context: context),
            )
          ],
        ),
        if (isShowFilterForm)
          Row(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: SizedBox(
                    height: 50,
                    child: TextField(
                      controller: fullnameController,
                      decoration: InputDecoration(
                        label: const MultiLanguageText(name: "code", defaultValue: "Code"),
                        contentPadding: const EdgeInsets.fromLTRB(10, 0, 10, 20),
                        border: const OutlineInputBorder(),
                        hintText: multiLanguageString(name: "by_code", defaultValue: "By code", context: context),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: SizedBox(
                    height: 50,
                    child: TextField(
                      controller: emailController,
                      decoration: InputDecoration(
                        label: const MultiLanguageText(name: "name", defaultValue: "Name"),
                        contentPadding: const EdgeInsets.fromLTRB(10, 0, 10, 20),
                        border: const OutlineInputBorder(),
                        hintText: multiLanguageString(name: "by_name", defaultValue: "By name", context: context),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        if (isShowFilterForm)
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: ElevatedButton(
                    onPressed: () {
                      handleSearchMention(currentPage);
                    },
                    child: const MultiLanguageText(
                      name: "search",
                      defaultValue: "Search",
                      isLowerCase: false,
                    )),
              ),
              ElevatedButton(
                  onPressed: () {
                    setState(() {
                      fullnameController.clear();
                      emailController.clear();
                      handleSearchMention(currentPage);
                    });
                  },
                  child: const MultiLanguageText(
                    name: "reset",
                    defaultValue: "Reset",
                    isLowerCase: false,
                  ))
            ],
          ),
      ],
    );
  }

  Row headerWidget(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        MultiLanguageText(
          name: "mention",
          defaultValue: "Mention",
          style: Style(context).styleTitleDialog,
        ),
        TextButton(
          onPressed: () => Navigator.pop(context, 'cancel'),
          child: const Icon(
            Icons.close,
            color: Colors.black,
          ),
        ),
      ],
    );
  }

  Padding btnWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: 40,
            width: 80,
            child: ElevatedButton(
              onPressed: () {
                widget.onSave(mentionList);
                Navigator.pop(context);
              },
              child: const MultiLanguageText(
                name: "ok",
                defaultValue: "Ok",
                isLowerCase: false,
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 10),
            height: 40,
            width: 120,
            child: ElevatedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              style: ElevatedButton.styleFrom(
                side: const BorderSide(color: Color.fromRGBO(209, 212, 216, 1)),
                backgroundColor: Colors.white,
                foregroundColor: const Color.fromRGBO(61, 90, 254, 1),
              ),
              child: const MultiLanguageText(
                name: "cancel",
                defaultValue: "Cancel",
                isLowerCase: false,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          )
        ],
      ),
    );
  }
}
