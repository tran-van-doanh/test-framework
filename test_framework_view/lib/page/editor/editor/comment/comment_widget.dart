import 'package:flutter/material.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/page/editor/editor/comment/comment_item.dart';
import 'package:test_framework_view/page/editor/editor/comment/input_comment.dart';

class CommentTestWidget extends StatefulWidget {
  final String objectId;
  final String objectType;
  const CommentTestWidget({Key? key, required this.objectId, required this.objectType}) : super(key: key);

  @override
  State<CommentTestWidget> createState() => _CommentTestWidgetState();
}

class _CommentTestWidgetState extends State<CommentTestWidget> {
  List comments = [];
  var rowCount = 0;
  var currentPage = 1;
  var rowPerPage = 10;
  var searchRequest = {};

  @override
  void initState() {
    search({});
    super.initState();
  }

  search(searchRequest) {
    this.searchRequest = searchRequest;
    getCommentList(currentPage);
  }

  getCommentList(page) async {
    if ((page - 1) * rowPerPage > rowCount) {
      page = (1.0 * rowCount / rowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {
      "objectType": widget.objectType,
      "objectId": widget.objectId,
      "queryOffset": (page - 1) * rowPerPage,
      "queryLimit": rowPerPage,
      "status": 1,
      "parentIdIsNull": true,
    };
    var response = await httpPost("/test-framework-api/user/soc/soc-comment/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPage = page;
        rowCount = response["body"]["rowCount"];
        comments = response["body"]["resultList"];
      });
    }
  }

  handleAddComment(String content, List mentionList) async {
    var result = {
      "objectType": widget.objectType,
      "objectId": widget.objectId,
      "content": content,
      "status": 1,
      "socCommentMentionList": mentionList
          .map((e) => {
                "status": 0,
                "sysUserId": e["sysUser"]["id"],
              })
          .toList(),
    };
    var response = await httpPut("/test-framework-api/user/soc/soc-comment", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        getCommentList(currentPage);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 350,
      decoration: const BoxDecoration(
        color: Colors.white,
        border: Border(
          right: BorderSide(
            width: 1.0,
            color: Color.fromRGBO(212, 212, 212, 1),
          ),
        ),
      ),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: MultiLanguageText(
              name: "comment",
              defaultValue: "Comment",
              style: Style(context).styleTitleDialog,
            ),
          ),
          Expanded(
            child: ListView(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              children: [
                for (var comment in comments)
                  Padding(
                    padding: EdgeInsets.only(bottom: comments.last != comment ? 10 : 0),
                    child: CommentItemWidget(
                      key: Key(comment["id"]),
                      comment: comment,
                    ),
                  ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: InputCommentWidget(
              onAddComment: (content, mentionList) => handleAddComment(content, mentionList),
            ),
          ),
        ],
      ),
    );
  }
}
