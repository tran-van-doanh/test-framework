import 'package:flutter/material.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/page/editor/editor/comment/input_comment.dart';
import 'package:test_framework_view/type.dart';

class CommentItemWidget extends StatefulWidget {
  final dynamic comment;
  const CommentItemWidget({Key? key, required this.comment}) : super(key: key);

  @override
  State<CommentItemWidget> createState() => _CommentItemWidgetState();
}

class _CommentItemWidgetState extends State<CommentItemWidget> {
  List subCommentList = [];
  int rowCount = 0;
  bool isShowInputComment = false;
  bool isShowSubComment = false;
  @override
  initState() {
    super.initState();
    getListSubComment();
  }

  getFirstChar(String fullname) => fullname.isNotEmpty ? fullname.trim().split(' ').map((l) => l[0]).take(4).join() : '';
  getListSubComment() async {
    var findRequest = {
      "status": 1,
      "parentId": widget.comment["id"],
    };
    var response = await httpPost("/test-framework-api/user/soc/soc-comment/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        subCommentList = response["body"]["resultList"];
        rowCount = response["body"]["rowCount"];
      });
    }
  }

  handleAddComment(String content, String parentId, List mentionList) async {
    var result = {
      "objectType": widget.comment["objectType"],
      "objectId": widget.comment["objectId"],
      "content": content,
      "status": 1,
      "parentId": parentId,
      "socCommentMentionList": mentionList
          .map((e) => {
                "status": 0,
                "sysUserId": e["sysUser"]["id"],
              })
          .toList(),
    };
    var response = await httpPut("/test-framework-api/user/soc/soc-comment", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        getListSubComment();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 2, right: 5),
          child: userAvt(widget.comment["sysUser"]["fullname"]),
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                alignment: Alignment.topLeft,
                padding: const EdgeInsets.fromLTRB(12, 8, 12, 8),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(6),
                  color: Colors.grey[200],
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.comment["sysUser"]["fullname"],
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: const Color.fromRGBO(0, 0, 0, 0.8),
                          fontSize: context.fontSizeManager.fontSizeText14,
                          letterSpacing: 2),
                    ),
                    Text(widget.comment["content"] ?? ""),
                    if (widget.comment["socCommentMentionList"] != null && widget.comment["socCommentMentionList"].isNotEmpty)
                      Wrap(
                        children: [
                          const Padding(
                            padding: EdgeInsets.only(top: 5),
                            child: Icon(
                              Icons.person,
                              size: 20,
                            ),
                          ),
                          const SizedBox(
                            width: 5,
                          ),
                          for (var user in widget.comment["socCommentMentionList"])
                            Padding(
                              padding: const EdgeInsets.only(top: 3, right: 3),
                              child: userAvt(user["sysUser"]["fullname"]),
                            ),
                        ],
                      ),
                  ],
                ),
              ),
              Row(
                children: [
                  Expanded(child: Text(widget.comment["createDate"] != null ? convertToAgo(widget.comment["createDate"]) : "")),
                  const SizedBox(
                    width: 10,
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        isShowInputComment = true;
                      });
                    },
                    child: MultiLanguageText(
                      name: "reply",
                      defaultValue: "Reply",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: const Color.fromRGBO(0, 0, 0, 0.8),
                          fontSize: context.fontSizeManager.fontSizeText14,
                          letterSpacing: 2),
                    ),
                  ),
                ],
              ),
              if (!isShowSubComment && rowCount > 0)
                GestureDetector(
                  onTap: () {
                    setState(() {
                      isShowSubComment = true;
                    });
                  },
                  child: MultiLanguageText(
                    name: "see_all_count_responses",
                    defaultValue: "See all \$0 responses",
                    variables: ["$rowCount"],
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: const Color.fromRGBO(0, 0, 0, 0.8),
                        fontSize: context.fontSizeManager.fontSizeText14,
                        letterSpacing: 2),
                  ),
                ),
              if (isShowSubComment && rowCount > 0)
                GestureDetector(
                  onTap: () {
                    setState(() {
                      isShowSubComment = false;
                    });
                  },
                  child: MultiLanguageText(
                    name: "hide_all_count_responses",
                    defaultValue: "Hide all \$0 responses",
                    variables: ["$rowCount"],
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: const Color.fromRGBO(0, 0, 0, 0.8),
                        fontSize: context.fontSizeManager.fontSizeText14,
                        letterSpacing: 2),
                  ),
                ),
              if (isShowSubComment)
                for (var comment in subCommentList)
                  CommentItemWidget(
                    comment: comment,
                  ),
              if (isShowInputComment)
                InputCommentWidget(
                  onAddComment: (content, mentionList) => handleAddComment(content, widget.comment["id"], mentionList),
                )
            ],
          ),
        ),
      ],
    );
  }

  Container userAvt(String fullname) {
    return Container(
      height: 25,
      width: 25,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: const Color(0xff7986cb),
      ),
      child: Center(
        child: Transform.scale(
          scale: 1,
          child: Text(
            getFirstChar(fullname).toUpperCase(),
            style: const TextStyle(
              color: Colors.white,
              fontSize: 11,
              decoration: TextDecoration.none,
              fontWeight: FontWeight.normal,
            ),
          ),
        ),
      ),
    );
  }
}
