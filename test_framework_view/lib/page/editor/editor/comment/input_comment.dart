import 'package:flutter/material.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/page/editor/editor/comment/manage_mention.dart';
import 'package:test_framework_view/type.dart';

class InputCommentWidget extends StatefulWidget {
  final Function(String content, List mentionList) onAddComment;
  const InputCommentWidget({Key? key, required this.onAddComment}) : super(key: key);

  @override
  State<InputCommentWidget> createState() => _InputCommentWidgetState();
}

class _InputCommentWidgetState extends State<InputCommentWidget> {
  TextEditingController commentController = TextEditingController();
  List mentionList = [];

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (mentionList.isNotEmpty)
          Container(
            alignment: Alignment.centerLeft,
            constraints: const BoxConstraints(minHeight: 50),
            padding: const EdgeInsets.all(5),
            decoration: BoxDecoration(
              border: Border.all(
                color: const Color.fromRGBO(216, 218, 229, 1),
              ),
              borderRadius: BorderRadius.circular(8),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: MultiLanguageText(
                    name: "mention",
                    defaultValue: "Mention",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: const Color.fromRGBO(0, 0, 0, 0.8),
                        fontSize: context.fontSizeManager.fontSizeText14,
                        letterSpacing: 2),
                  ),
                ),
                Expanded(
                  child: Wrap(
                    children: [
                      for (var mention in mentionList)
                        Container(
                          margin: EdgeInsets.only(right: mentionList.last != mention ? 10 : 0, bottom: 5, top: 5),
                          child: IntrinsicWidth(
                            child: MentionListWidget(
                              key: UniqueKey(),
                              mention: mention,
                              onRemove: () {
                                setState(() {
                                  mentionList.remove(mention);
                                });
                              },
                            ),
                          ),
                        )
                    ],
                  ),
                ),
              ],
            ),
          ),
        const SizedBox(
          height: 5,
        ),
        DynamicTextField(
          controller: commentController,
          hintText: multiLanguageString(name: "write_a_public_comment", defaultValue: "Write a public comment", context: context),
          onFieldSubmitted: (p0) {
            widget.onAddComment(commentController.text, mentionList);
            setState(() {
              commentController.clear();
              mentionList.clear();
            });
          },
          onChanged: (String text) {
            setState(() {
              commentController.text;
            });
            if (text.trim().isNotEmpty && text.endsWith('\n')) {
              widget.onAddComment(commentController.text, mentionList);
              setState(() {
                commentController.clear();
                mentionList.clear();
              });
            }
          },
          minline: 1,
          maxline: 6,
        ),
        const SizedBox(
          height: 5,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GestureDetector(
              onTap: () {
                Navigator.of(context).push(
                  createRoute(
                    ManageMentionWidget(
                      mentionList: mentionList,
                      onSave: (mentionList) {
                        setState(() {
                          this.mentionList = mentionList;
                        });
                      },
                    ),
                  ),
                );
              },
              child: const Icon(
                Icons.person_add,
                size: 20,
              ),
            ),
            GestureDetector(
              onTap: commentController.text.trim().isNotEmpty
                  ? () {
                      widget.onAddComment(commentController.text, mentionList);
                      setState(() {
                        commentController.clear();
                        mentionList.clear();
                      });
                    }
                  : null,
              child: const Icon(
                Icons.send,
                size: 20,
              ),
            )
          ],
        )
      ],
    );
  }
}
