import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/custom_state.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/extension/hex_color.dart';
import 'package:test_framework_view/type.dart';

class SideBarEditorWidget extends StatefulWidget {
  final String? tfTestCaseId;
  final Function onClickHistoryTestCase;
  final String? flyoutCategoryType;
  final Function onToggleFlyout;

  const SideBarEditorWidget({
    Key? key,
    this.tfTestCaseId,
    required this.onClickHistoryTestCase,
    required this.flyoutCategoryType,
    required this.onToggleFlyout,
  }) : super(key: key);

  @override
  State<SideBarEditorWidget> createState() => _SideBarEditorWidgetState();
}

class _SideBarEditorWidgetState extends State<SideBarEditorWidget> with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;
  String? selectedId;
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      width: 250,
      decoration: const BoxDecoration(color: Colors.white, border: Border(right: BorderSide(width: 1.0, color: Color.fromRGBO(212, 212, 212, 1)))),
      child: DefaultTabController(
        length: 2,
        child: Column(
          children: [
            TabBar(
              onTap: (value) => widget.onToggleFlyout(null),
              labelColor: const Color(0xff3d5afe),
              indicatorColor: const Color(0xff3d5afe),
              unselectedLabelColor: const Color(0xff9ea0ab),
              labelStyle: TextStyle(fontSize: context.fontSizeManager.fontSizeText14, fontWeight: FontWeight.w700),
              tabs: [
                Tab(
                  text: multiLanguageString(name: "sidebar_keywords", defaultValue: "Keywords", isLowerCase: false, context: context),
                  height: 30,
                ),
                Tab(
                  text: multiLanguageString(name: "sidebar_history", defaultValue: "History", isLowerCase: false, context: context),
                  height: 30,
                ),
              ],
            ),
            Expanded(
              child: TabBarView(
                children: <Widget>[
                  ListCategoryWidget(
                    flyoutCategoryType: widget.flyoutCategoryType,
                    onToggleFlyout: (isOpenFlyoutNew) => widget.onToggleFlyout(isOpenFlyoutNew),
                  ),
                  HistoryEditorWidget(
                      selectedId: selectedId,
                      tfTestCaseId: widget.tfTestCaseId,
                      onClickHistoryTestCase: (value, id) {
                        selectedId = id;
                        widget.onClickHistoryTestCase(value);
                      }),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class HistoryEditorWidget extends StatefulWidget {
  final String? tfTestCaseId;
  final String? selectedId;
  final Function onClickHistoryTestCase;

  const HistoryEditorWidget({Key? key, this.tfTestCaseId, required this.onClickHistoryTestCase, this.selectedId}) : super(key: key);

  @override
  State<HistoryEditorWidget> createState() => _HistoryEditorWidgetState();
}

class _HistoryEditorWidgetState extends CustomState<HistoryEditorWidget> {
  var listHistoryTestCase = [];
  @override
  void initState() {
    super.initState();
    if (widget.tfTestCaseId != null) getListHistoryTestCase();
  }

  getListHistoryTestCase() async {
    var findRequest = {"tfTestCaseId": widget.tfTestCaseId};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-case-history/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        listHistoryTestCase = response["body"]["resultList"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: [
            if (listHistoryTestCase.isNotEmpty)
              for (var historyTestCase in listHistoryTestCase)
                GestureDetector(
                  onTap: () {
                    widget.onClickHistoryTestCase(historyTestCase["content"], historyTestCase["id"]);
                  },
                  child: Card(
                    elevation: 20,
                    shape: RoundedRectangleBorder(
                      side: BorderSide(color: historyTestCase["id"] == widget.selectedId ? Colors.green : Colors.grey, width: 3),
                      borderRadius: const BorderRadius.all(
                        Radius.circular(15),
                      ),
                    ),
                    shadowColor: Colors.green[100],
                    child: ListTile(
                      contentPadding: const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
                      title: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Icon(Icons.person),
                          const SizedBox(
                            width: 5,
                          ),
                          Expanded(child: Text(historyTestCase["sysUser"]["fullname"])),
                        ],
                      ),
                      subtitle: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Icon(
                            Icons.schedule,
                            size: 18,
                          ),
                          const SizedBox(
                            width: 5,
                          ),
                          Expanded(
                              child: Text(DateFormat('dd-MM-yyyy HH:mm:ss')
                                  .format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(historyTestCase["createDate"]).toLocal()))),
                        ],
                      ),
                    ),
                  ),
                ),
            if (listHistoryTestCase.isEmpty) const MultiLanguageText(name: "no_data", defaultValue: "No data")
          ],
        ),
      ),
    );
  }
}

class ListCategoryWidget extends StatefulWidget {
  final String? flyoutCategoryType;
  final Function onToggleFlyout;
  const ListCategoryWidget({
    Key? key,
    required this.flyoutCategoryType,
    required this.onToggleFlyout,
  }) : super(key: key);

  @override
  State<ListCategoryWidget> createState() => _ListCategoryWidgetState();
}

class _ListCategoryWidgetState extends State<ListCategoryWidget> {
  List editorComponentList = [];
  List categoryList = [];
  @override
  void initState() {
    getListTemplate();
    super.initState();
  }

  getListTemplate() async {
    var response = await httpPost("/test-framework-api/user/test-framework/tf-editor-component/search", {"status": 1}, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        editorComponentList = response["body"]["resultList"];
        categoryList = editorComponentList.where((element) => element["category"] == "category").toList();
      });
    }
  }

  countTemplate(String type) {
    int count = editorComponentList.where((platformTemplate) => conditionCheck(platformTemplate["type"], platformTemplate["category"], type)).length;
    return count;
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Column(
      children: [
        categoryItem("", multiLanguageString(name: "config_all", defaultValue: "All", context: context), Colors.redAccent, context),
        for (var category in categoryList)
          categoryItem(
              category["type"],
              multiLanguageString(name: category["type"], defaultValue: category["title"], context: context),
              (category["componentConfig"] != null && category["componentConfig"].isNotEmpty)
                  ? (HexColor.fromHex(category["componentConfig"]["color"]))
                  : Colors.grey[300],
              context),
      ],
    ));
  }

  GestureDetector categoryItem(String type, String text, Color? color, BuildContext context) {
    return GestureDetector(
      onTap: () => widget.onToggleFlyout(widget.flyoutCategoryType != null && widget.flyoutCategoryType == type ? null : type),
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 10),
        alignment: Alignment.centerLeft,
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: widget.flyoutCategoryType == type ? color : null,
          border: Border(
            right: BorderSide(
              color: (color ?? Colors.grey[300])!,
              width: 10,
            ),
          ),
        ),
        child: Text(
          "$text (${countTemplate(type)})",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: widget.flyoutCategoryType == type ? Colors.white : const Color.fromRGBO(0, 0, 0, 0.8),
            fontSize: context.fontSizeManager.fontSizeText16,
            letterSpacing: 2,
          ),
        ),
      ),
    );
  }
}
