import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/dynamic_play_form.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/type.dart';

import '../../../common/custom_state.dart';

class DialogPlayTestcase extends StatefulWidget {
  final String? tfTestCaseId;
  const DialogPlayTestcase({Key? key, required this.tfTestCaseId}) : super(key: key);

  @override
  State<DialogPlayTestcase> createState() => _DialogPlayTestcaseState();
}

class _DialogPlayTestcaseState extends CustomState<DialogPlayTestcase> {
  late Future futureParameterListPlay;
  List resultparameterFieldList = [];
  List variableReturnList = [];
  String? testcaseName;

  @override
  void initState() {
    futureParameterListPlay = getInitPage();
    super.initState();
  }

  getInitPage() async {
    getVariableReturnList();
    getParameterFieldList();
    return 0;
  }

  getVariableReturnList() {
    TestCaseConfigModel testCaseConfigModel = Provider.of<TestCaseConfigModel>(context, listen: false);
    ListElementPlayIdModel listElementPlayIdModel = Provider.of<ListElementPlayIdModel>(context, listen: false);
    (testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["newVariableList"] ?? []).every((element) {
      if (listElementPlayIdModel.listId.contains(element["parentId"])) {
        variableReturnList.add(element);
      }
      return true;
    });
  }

  getParameterFieldList() async {
    var response = {};
    response = await httpGet("/test-framework-api/user/test-framework/tf-test-case/${widget.tfTestCaseId}", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        resultparameterFieldList = response["body"]["result"]["content"]["parameterFieldList"] ?? [];
        testcaseName = response["body"]["result"]["title"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<PlayTestCaseModel>(builder: (context, playTestCaseModel, child) {
      return FutureBuilder(
        future: futureParameterListPlay,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return AlertDialog(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  MultiLanguageText(
                    name: "play_test_case_name",
                    defaultValue: "Play \$0",
                    variables: ["$testcaseName"],
                    style: Style(context).styleTitleDialog,
                  ),
                  TextButton(
                    onPressed: () => Navigator.pop(context, 'Cancel'),
                    child: const Icon(
                      Icons.close,
                      color: Colors.black,
                    ),
                  )
                ],
              ),
              content: Container(
                decoration: const BoxDecoration(
                  border: Border(
                    top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
                    bottom: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
                  ),
                ),
                padding: const EdgeInsets.all(10),
                constraints: const BoxConstraints(minWidth: 600),
                child: SingleChildScrollView(
                  child: IntrinsicHeight(
                    child: (resultparameterFieldList.isNotEmpty || variableReturnList.isNotEmpty)
                        ? Column(
                            children: [
                              if (resultparameterFieldList.isNotEmpty)
                                Container(
                                  alignment: Alignment.centerLeft,
                                  color: Colors.yellow[200],
                                  child: MultiLanguageText(
                                    name: "enter_value_parameters",
                                    defaultValue: "Enter value for parameters",
                                    style: Style(context).styleTitleDialog,
                                  ),
                                ),
                              for (var parameterField in resultparameterFieldList) parameterFieldWidget(parameterField),
                              if (resultparameterFieldList.isNotEmpty && variableReturnList.isNotEmpty)
                                const SizedBox(
                                  height: 20,
                                ),
                              if (variableReturnList.isNotEmpty)
                                Container(
                                  alignment: Alignment.centerLeft,
                                  color: Colors.yellow[200],
                                  child: MultiLanguageText(
                                    name: "enter_value_variables",
                                    defaultValue: "Enter value for other variables",
                                    style: Style(context).styleTitleDialog,
                                  ),
                                ),
                              for (var variableReturn in variableReturnList) variableReturnWidget(variableReturn),
                            ],
                          )
                        : Center(
                            child: MultiLanguageText(
                              name: "param_field_and_variables",
                              defaultValue: "No parameterField and variables return, please configure !",
                              style: TextStyle(fontSize: context.fontSizeManager.fontSizeText20, fontWeight: FontWeight.bold),
                            ),
                          ),
                  ),
                ),
              ),
              actionsAlignment: MainAxisAlignment.center,
              actions: [
                if (playTestCaseModel.playDataList.isNotEmpty)
                  Container(
                    margin: const EdgeInsets.only(right: 5),
                    height: 40,
                    width: 120,
                    child: FloatingActionButton.extended(
                      heroTag: null,
                      onPressed: () {
                        showDialog<String>(
                            context: context,
                            builder: (BuildContext context) {
                              return dialogRerunPlayTestCase(playTestCaseModel);
                            });
                      },
                      icon: const Icon(Icons.refresh, color: Color.fromRGBO(49, 49, 49, 1)),
                      label: MultiLanguageText(
                        name: "rerun",
                        defaultValue: "Rerun",
                        style: TextStyle(
                            fontSize: context.fontSizeManager.fontSizeText13,
                            fontWeight: FontWeight.w600,
                            color: const Color.fromRGBO(49, 49, 49, 1)),
                      ),
                      backgroundColor: const Color.fromRGBO(216, 218, 229, .7),
                      elevation: 0,
                      hoverElevation: 0,
                    ),
                  ),
                Container(
                  margin: const EdgeInsets.only(right: 5),
                  height: 40,
                  width: 120,
                  child: ElevatedButton(
                    onPressed: () async {
                      onLoading(context);
                      DebugStatusModel debugStatusModel = Provider.of<DebugStatusModel>(context, listen: false);
                      var findRequest = {
                        "type": "callAction",
                        "tfTestCaseId": widget.tfTestCaseId,
                        "tfTestVersionId": (debugStatusModel.versionStartDebug != null) ? debugStatusModel.versionStartDebug["id"] : null,
                        "tfTestEnvironmentId": (debugStatusModel.environmentStartDebug != null) ? debugStatusModel.environmentStartDebug["id"] : null,
                        "tfTestProfileId": (debugStatusModel.profileStartDebug != null) ? debugStatusModel.profileStartDebug["id"] : null,
                        "runIdList": Provider.of<ListElementPlayIdModel>(context, listen: false).listId,
                        "data": {
                          for (var parameter in resultparameterFieldList) parameter['name']: parameter["value"],
                        },
                        "initValueData": {
                          for (var variableReturn in variableReturnList) variableReturn['name']: variableReturn["value"],
                        }
                      };
                      var response = await httpPost("/test-framework-api/user/node/message", findRequest, context);

                      if (response.containsKey("body") && response["body"] is String == false && mounted) {
                        if (response["body"].containsKey("errorMessage")) {
                          showToast(context: this.context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
                        } else {
                          showToast(
                            context: this.context,
                            msg: multiLanguageString(
                                name: "call_action_test_case_name_success",
                                defaultValue: "Call action : \$0 success",
                                variables: ["$testcaseName"],
                                context: this.context),
                            color: Colors.greenAccent,
                            icon: const Icon(Icons.done),
                          );
                        }
                      }
                      if (mounted) {
                        Navigator.pop(this.context);
                        Navigator.pop(this.context);
                        Provider.of<WebElementModel>(this.context, listen: false)
                            .setCurrentTimeMicrosecondsSinceEpoch(DateTime.now().microsecondsSinceEpoch);
                        playTestCaseModel.addPlayDataList({
                          for (var parameter in resultparameterFieldList) parameter['name']: parameter["value"],
                          "createDate": DateTime.now().toIso8601String(),
                        });
                      }
                    },
                    child: const MultiLanguageText(
                      name: "ok",
                      defaultValue: "Ok",
                      isLowerCase: false,
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
                const ButtonCancel()
              ],
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        },
      );
    });
  }

  Widget variableReturnWidget(variableReturn) {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Row(
        children: [
          Expanded(
            child: Text(
              variableReturn["name"],
              style: const TextStyle(
                fontWeight: FontWeight.bold,
                color: Color.fromRGBO(87, 94, 117, 1),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: DynamicTextField(
                    controller: TextEditingController(text: variableReturn["value"]),
                    hintText: multiLanguageString(
                        name: "enter_a_value_for", defaultValue: "Enter a value for \$0", variables: ["${variableReturn["name"]}"], context: context),
                    onChanged: (text) {
                      variableReturn["value"] = text;
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget parameterFieldWidget(parameterField) {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Row(
        children: [
          Expanded(
            child: Text(
              parameterField["name"],
              style: const TextStyle(
                fontWeight: FontWeight.bold,
                color: Color.fromRGBO(87, 94, 117, 1),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: DynamicTextField(
                    controller: TextEditingController(text: parameterField["value"]),
                    hintText: multiLanguageString(
                        name: "enter_a_value_for", defaultValue: "Enter a value for \$0", variables: ["${parameterField["name"]}"], context: context),
                    onChanged: (text) {
                      parameterField["value"] = text;
                    },
                    obscureText: parameterField["dataType"] == "Password",
                  ),
                ),
                if (parameterField["dataType"] == "File")
                  Container(
                    margin: const EdgeInsets.only(left: 10),
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(fixedSize: const Size(100, 50)),
                        onPressed: () {
                          Navigator.of(context).push(
                            createRoute(
                              FindFileWidget((String valueCallback) {
                                setState(() {
                                  parameterField["value"] = valueCallback;
                                });
                              }),
                            ),
                          );
                        },
                        child: const MultiLanguageText(name: "select_file", defaultValue: "Select file")),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  dialogRerunPlayTestCase(PlayTestCaseModel playTestCaseModel) {
    return AlertDialog(
      contentPadding: const EdgeInsets.all(0),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "select_play_test_data",
            defaultValue: "Select play test data",
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
              onPressed: () => Navigator.pop(context, "Cancel"),
              child: const Icon(
                Icons.close,
                color: Colors.black,
              ))
        ],
      ),
      content: Container(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
        decoration: const BoxDecoration(
          border: Border(
            top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
            bottom: BorderSide(
              color: Color.fromRGBO(216, 218, 229, 1),
            ),
          ),
        ),
        margin: const EdgeInsets.only(top: 20, bottom: 10),
        width: 1000,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                children: [
                  Container(
                    padding: const EdgeInsets.all(15),
                    decoration: const BoxDecoration(
                      border: Border(
                        top: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
                        bottom: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
                        left: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
                        right: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
                      ),
                    ),
                    child: Row(
                      children: [
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 5),
                            child: MultiLanguageText(name: "run", defaultValue: "Run", isLowerCase: false, style: Style(context).styleTextDataColumn),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 5),
                            child: MultiLanguageText(
                                name: "create_date", defaultValue: "Create date", isLowerCase: false, style: Style(context).styleTextDataColumn),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    children: [
                      for (var playData in playTestCaseModel.playDataList)
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              for (var parameterField in resultparameterFieldList) {
                                parameterField["value"] = playData[parameterField["name"]] ?? "";
                              }
                            });
                            Navigator.pop(context);
                          },
                          child: Container(
                            padding: const EdgeInsets.all(15),
                            decoration: const BoxDecoration(
                              border: Border(
                                top: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
                                bottom: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
                                left: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
                                right: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
                              ),
                            ),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 5),
                                    child: MultiLanguageText(
                                      name: "run_play_test_case_model",
                                      defaultValue: "Run \$0",
                                      variables: ["${playTestCaseModel.playDataList.reversed.toList().indexOf(playData) + 1}"],
                                      softWrap: true,
                                      style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 5),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(playData["createDate"]).toLocal()),
                                          softWrap: true,
                                          style:
                                              TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                                        ),
                                        Text(
                                          DateFormat('HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(playData["createDate"]).toLocal()),
                                          softWrap: true,
                                          style:
                                              TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
      actionsAlignment: MainAxisAlignment.center,
      actions: [
        Container(
          margin: const EdgeInsets.only(left: 5),
          height: 40,
          width: 120,
          child: ElevatedButton(
            onPressed: () {
              Navigator.pop(context, "Cancel");
            },
            style: ElevatedButton.styleFrom(
                side: const BorderSide(color: Color.fromRGBO(209, 212, 216, 1)),
                backgroundColor: Colors.white,
                foregroundColor: const Color.fromRGBO(61, 90, 254, 1)),
            child: const MultiLanguageText(
              name: "cancel",
              defaultValue: "Cancel",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ],
    );
  }
}
