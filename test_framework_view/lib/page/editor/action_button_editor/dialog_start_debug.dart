import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';

import '../../../common/custom_state.dart';
import '../../../common/dynamic_dropdown_button.dart';

class DialogStartDebug extends StatefulWidget {
  final List clientList;
  const DialogStartDebug({Key? key, required this.clientList}) : super(key: key);

  @override
  State<DialogStartDebug> createState() => _DialogStartDebugState();
}

class _DialogStartDebugState extends CustomState<DialogStartDebug> {
  late Future future;
  List clientList = [];
  var listVersion = [];
  dynamic selectedVersion;
  var listEnvironment = [];
  dynamic selectedEnvironment;
  var prjProjectId = "";
  bool? usePublicNode;
  bool isShowProfile = false;
  @override
  void initState() {
    prjProjectId = Provider.of<NavigationModel>(context, listen: false).prjProjectId ?? "";
    future = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    clientList = List.from(widget.clientList);
    getListVersion();
    getListEnvironment();
    for (var client in clientList) {
      await getListProfile(client);
      await getProfileDefault(client);
    }
    return 0;
  }

  onClickStartDebug(tfTestVersionId, tfTestEnvironmentId) async {
    var findRequest = {
      "testRunConfig": {"clientList": clientList},
      "prjProjectId": prjProjectId,
      "usePublicNode": usePublicNode,
      "tfTestVersionId": tfTestVersionId,
      "tfTestEnvironmentId": tfTestEnvironmentId,
    };
    var response = await httpPost("/test-framework-api/user/node/start", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      if (response["body"].containsKey("errorMessage") && mounted) {
        Navigator.of(context).pop();
        showToast(
          context: context,
          msg: response["body"]["errorMessage"],
          color: Colors.red,
          icon: const Icon(Icons.error),
        );
      } else {
        for (int i = 0; i < 30; i++) {
          await Future.delayed(const Duration(seconds: 1));
          if (mounted) {
            var statusResponse = await httpGet("/test-framework-api/user/node/status", context);
            if (statusResponse.containsKey("body") && statusResponse["body"] is String == false && statusResponse["body"].containsKey("result")) {
              if (statusResponse["body"]["result"]["status"] == 2 && mounted) {
                Navigator.of(context).pop();
                showToast(
                  context: context,
                  msg: multiLanguageString(name: "start_debug_success", defaultValue: "Start debug success", context: context),
                  color: Colors.greenAccent,
                  icon: const Icon(Icons.done),
                );
                DebugStatusModel debugStatusModel = Provider.of<DebugStatusModel>(context, listen: false);
                debugStatusModel.setVersionStartDebug(statusResponse["body"]["result"]["tfTestVersion"]);
                debugStatusModel.setEnvironmentStartDebug(statusResponse["body"]["result"]["tfTestEnvironment"]);
                debugStatusModel.setProfileStartDebug(statusResponse["body"]["result"]["tfTestProfile"]);
                debugStatusModel.setDebugStatus(true);
                break;
              }
            }
          }
        }
      }
    }
  }

  getListProfile(client) async {
    var findRequest = {"prjProjectId": prjProjectId, "status": 1, "platform": client["platform"]};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-profile/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        client["profileList"] = response["body"]["resultList"];
      });
    }
    return 0;
  }

  getListVersion() async {
    var findRequest = {"prjProjectId": prjProjectId, "status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-version/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        listVersion = response["body"]["resultList"];
        if (listVersion.isNotEmpty) {
          var versionItem = listVersion.firstWhere(
            (element) => element["defaultVersion"] == 1,
            orElse: () => null,
          );
          selectedVersion = versionItem ?? listVersion[0];
        }
      });
      return 0;
    }
  }

  getListEnvironment() async {
    var findRequest = {"prjProjectId": prjProjectId, "status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-environment/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        listEnvironment = response["body"]["resultList"];
        if (listEnvironment.isNotEmpty) {
          var environmentItem = listEnvironment.firstWhere(
            (element) => element["defaultEnvironment"] == 1,
            orElse: () => null,
          );
          selectedEnvironment = environmentItem ?? listEnvironment[0];
        }
      });
      return 0;
    }
  }

  getProfileDefault(client) async {
    var response = await httpGet("/test-framework-api/user/project/prj-project/$prjProjectId/tf-test-profile/${client["platform"]}/default", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        client["tfTestProfileId"] = response["body"]["result"]["id"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: future,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return AlertDialog(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  MultiLanguageText(
                    name: "start_debug",
                    defaultValue: "Start debug",
                    style: Style(context).styleTitleDialog,
                  ),
                  TextButton(
                    onPressed: () => Navigator.pop(context, 'Cancel'),
                    child: const Icon(
                      Icons.close,
                      color: Colors.black,
                    ),
                  )
                ],
              ),
              content: Container(
                decoration: const BoxDecoration(
                  border: Border(
                    top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
                    bottom: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
                  ),
                ),
                width: 1000,
                child: IntrinsicHeight(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Container(
                          margin: const EdgeInsets.only(top: 10),
                          color: Colors.yellow[200],
                          child: MultiLanguageText(
                            name: "select_profile_and_version",
                            defaultValue: "Select a profile and version to start debug",
                            style: Style(context).styleTitleDialog,
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.symmetric(vertical: 10),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 2,
                                child: MultiLanguageText(
                                  name: "version",
                                  defaultValue: "Version",
                                  style: Style(context).styleTitleDialog,
                                ),
                              ),
                              Expanded(
                                flex: 8,
                                child: DynamicDropdownSearchClearOption(
                                  hint: multiLanguageString(name: "version", defaultValue: "Version", context: context),
                                  controller: SingleValueDropDownController(
                                      data: DropDownValueModel(name: selectedVersion["name"], value: selectedVersion["id"])),
                                  items: listVersion.map<DropDownValueModel>((dynamic result) {
                                    return DropDownValueModel(
                                      value: result["id"],
                                      name: result["name"],
                                    );
                                  }).toList(),
                                  maxHeight: 200,
                                  onChanged: (dynamic newValue) {
                                    setState(() {
                                      selectedVersion = listVersion.firstWhere((element) => element["id"] == newValue.value);
                                    });
                                  },
                                  isRequiredNotEmpty: true,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.symmetric(vertical: 10),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 2,
                                child: MultiLanguageText(
                                  name: "environment",
                                  defaultValue: "Environment",
                                  style: Style(context).styleTitleDialog,
                                ),
                              ),
                              Expanded(
                                flex: 8,
                                child: DynamicDropdownSearchClearOption(
                                  hint: multiLanguageString(name: "environment", defaultValue: "Environment", context: context),
                                  controller: SingleValueDropDownController(
                                      data: DropDownValueModel(name: selectedEnvironment["name"], value: selectedEnvironment["id"])),
                                  items: listEnvironment.map<DropDownValueModel>((dynamic result) {
                                    return DropDownValueModel(
                                      value: result["id"],
                                      name: result["name"],
                                    );
                                  }).toList(),
                                  maxHeight: 200,
                                  onChanged: (dynamic newValue) {
                                    setState(() {
                                      selectedEnvironment = listEnvironment.firstWhere((element) => element["id"] == newValue.value);
                                    });
                                  },
                                  isRequiredNotEmpty: true,
                                ),
                              ),
                            ],
                          ),
                        ),
                        advanceWidget(),
                        Container(
                          alignment: Alignment.center,
                          padding: const EdgeInsets.symmetric(vertical: 15),
                          child: ConstrainedBox(
                            constraints: const BoxConstraints(minHeight: 40, minWidth: 120),
                            child: ElevatedButton(
                              onPressed: (selectedVersion != null && selectedEnvironment != null)
                                  ? () async {
                                      onLoading(context);
                                      await onClickStartDebug(selectedVersion["id"], selectedEnvironment["id"]);
                                      if (mounted) Navigator.pop(this.context);
                                    }
                                  : null,
                              child: const MultiLanguageText(
                                name: "start_debug",
                                defaultValue: "Start debug",
                                style: TextStyle(fontWeight: FontWeight.bold, letterSpacing: 2),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        });
  }

  Widget advanceWidget() {
    return Theme(
      data: ThemeData().copyWith(dividerColor: Colors.transparent),
      child: ExpansionTile(
        tilePadding: const EdgeInsets.all(0),
        childrenPadding: const EdgeInsets.all(10),
        title: MultiLanguageText(
          name: "advance",
          defaultValue: "Advance",
          style: TextStyle(fontSize: context.fontSizeManager.fontSizeText20, fontWeight: FontWeight.w500),
        ),
        expandedCrossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          MultiLanguageText(
            name: "choose_profile_for_client",
            defaultValue: "Choose profile for the client",
            style: Style(context).styleTitleDialog,
          ),
          const SizedBox(
            height: 10,
          ),
          for (var client in clientList)
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Expanded(
                      child: DynamicDropdownButton(
                        value: client["tfTestProfileId"],
                        hint: multiLanguageString(name: "choose_a_profile", defaultValue: "Choose a profile", context: context),
                        items: (client["profileList"] ?? []).map<DropdownMenuItem<dynamic>>((dynamic result) {
                          return DropdownMenuItem(
                            value: result["id"],
                            child: Text(result["title"]),
                          );
                        }).toList(),
                        onChanged: (newValue) {
                          setState(() {
                            client["tfTestProfileId"] = newValue!;
                          });
                        },
                        labelText: multiLanguageString(
                            name: "client_client_name", defaultValue: "Client \$0", variables: ["${client["name"]}"], context: context),
                        borderRadius: 5,
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                )
              ],
            ),
          MultiLanguageText(
            name: "agent_type",
            defaultValue: "Agent Type",
            style: Style(context).styleTitleDialog,
          ),
          const SizedBox(
            height: 10,
          ),
          DynamicDropdownButton(
            value: usePublicNode,
            hint: multiLanguageString(name: "agent_type_question_mark", defaultValue: "Agent Type ?", context: context),
            items: const [
              {"name": "Public", "value": true},
              {"name": "Private", "value": false},
              {"name": "Any agent", "value": null},
            ].map<DropdownMenuItem<dynamic>>((dynamic result) {
              return DropdownMenuItem(
                value: result["value"],
                child: Text(result["name"]),
              );
            }).toList(),
            onChanged: (newValue) {
              setState(() {
                usePublicNode = newValue!;
              });
            },
            labelText: multiLanguageString(name: "agent_type", defaultValue: "Agent Type", context: context),
            borderRadius: 5,
          ),
        ],
      ),
    );
  }
}
