import 'dart:async';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dialog/dialog_confirm.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/editor/action_button_editor/manage_element_template.dart';
import 'package:test_framework_view/page/element_repository/dialog_option_element_repository.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';
import 'package:test_framework_view/type.dart';
import '../../../common/custom_state.dart';

class DialogAnalyze extends StatefulWidget {
  final String? testCaseId;
  final String title;
  final Function onClickContinue;
  const DialogAnalyze({Key? key, required this.title, required this.onClickContinue, this.testCaseId}) : super(key: key);

  @override
  State<DialogAnalyze> createState() => _DialogAnalyzeState();
}

class _DialogAnalyzeState extends CustomState<DialogAnalyze> {
  late Future futureListElementRepository;
  final TextEditingController _searchLibrary = TextEditingController();
  var listElementRepository = [];
  var rowCountListElementRepository = 0;
  var currentPageListElementRepository = 1;
  var rowPerPageElementRepository = 10;
  dynamic selectedElementRepository;
  var findRequestElementRepository = {};
  String? parentId;
  List? elementRepositoryPathList;
  String? clientName;
  String? projectType;
  var listProjectType = [];
  List elementTemplateList = [];
  late TestCaseConfigModel testCaseConfigModel;
  Timer? _debounceTimer;
  String? prjProjectId;
  @override
  void initState() {
    prjProjectId = Provider.of<NavigationModel>(context, listen: false).prjProjectId;
    testCaseConfigModel = Provider.of<TestCaseConfigModel>(context, listen: false);
    if (testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"] != null &&
        testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"].isNotEmpty) {
      clientName = testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"][0]["name"];
    }
    futureListElementRepository = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    findRequestElementRepository = {};
    findRequestElementRepository["parentIdIsNull"] = true;
    final futures = <Future>[
      getListTfElementRepository(currentPageListElementRepository, findRequestElementRepository),
      if (widget.testCaseId != null) getListProjectType(),
      getCurrentProjectType(),
    ];
    final result = await Future.wait<dynamic>(futures);
    if (result.length == futures.length) {
      return 0;
    }
  }

  getListProjectType() async {
    var findRequest = {"prjProjectId": prjProjectId, "status": 1};
    var response = await httpPost("/test-framework-api/user/project/prj-project-type/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        listProjectType = [
          {"id": null, "title": "All"}
        ];
        listProjectType.addAll(response["body"]["resultList"]);
      });
    }
  }

  getCurrentProjectType() async {
    var projectResponse = await httpGet("/test-framework-api/user/project/prj-project/$prjProjectId", context);
    if (projectResponse.containsKey("body") && projectResponse["body"] is String == false) {
      projectType = projectResponse["body"]["result"]["prjProjectTypeId"];
    }
  }

  getElementRepositoryPathList() async {
    var directoryPathResponse = await httpGet("/test-framework-api/user/test-framework/tf-test-element-repository/$parentId/path", context);
    if (directoryPathResponse.containsKey("body") && directoryPathResponse["body"] is String == false) {
      elementRepositoryPathList = directoryPathResponse["body"]["resultList"];
    }
  }

  getListTfElementRepository(page, findRequest) async {
    if ((page - 1) * rowPerPageElementRepository > rowCountListElementRepository) {
      page = (1.0 * rowCountListElementRepository / rowPerPageElementRepository).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequestElementRepository = {};
    findRequestElementRepository.addAll(findRequest);
    if (findRequest["titleLike"] != null && findRequest["titleLike"].isNotEmpty) {
      findRequestElementRepository["titleLike"] = "%${findRequest["titleLike"]}%";
    }
    findRequestElementRepository["queryOffset"] = (page - 1) * rowPerPageElementRepository;
    findRequestElementRepository["queryLimit"] = rowPerPageElementRepository;
    findRequestElementRepository["prjProjectId"] = prjProjectId;
    findRequestElementRepository["status"] = 1;
    var responseElementRepository =
        await httpPost("/test-framework-api/user/test-framework/tf-test-element-repository/search", findRequestElementRepository, context);
    if (responseElementRepository.containsKey("body") && responseElementRepository["body"] is String == false) {
      setState(() {
        currentPageListElementRepository = page;
        rowCountListElementRepository = responseElementRepository["body"]["rowCount"];
        listElementRepository = responseElementRepository["body"]["resultList"];
      });
    }
    return 0;
  }

  handleAddTfAll(result) async {
    if (parentId != null) {
      result["parentId"] = parentId;
    }
    var response = await httpPut("/test-framework-api/user/test-framework/tf-test-element-repository", result, context);

    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        Navigator.of(context).pop();
        futureListElementRepository = getListTfElementRepository(currentPageListElementRepository, findRequestElementRepository);
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
      }
    }
  }

  @override
  void dispose() {
    _searchLibrary.dispose();
    _debounceTimer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return FutureBuilder(
          future: futureListElementRepository,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return TestFrameworkRootPageWidget(
                child: Column(
                  children: [
                    Expanded(
                      child: ListView(
                        padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
                        children: [
                          headerWidget(),
                          const SizedBox(
                            height: 20,
                          ),
                          if (widget.testCaseId != null)
                            Column(
                              children: [
                                Center(
                                  child: Container(
                                    margin: const EdgeInsets.only(top: 10),
                                    color: Colors.yellow[200],
                                    child: MultiLanguageText(
                                      name: "select_a_client",
                                      defaultValue: "Select a client",
                                      style: Style(context).styleTitleDialog,
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  children: [
                                    const Expanded(
                                        child: MultiLanguageText(
                                            name: "client",
                                            defaultValue: "Client",
                                            suffiText: " : ",
                                            style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
                                    Expanded(
                                      flex: 3,
                                      child: DynamicDropdownButton(
                                          value: clientName,
                                          hint: multiLanguageString(name: "choose_a_client", defaultValue: "Choose a client", context: context),
                                          items: testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"]
                                              .map<DropdownMenuItem>((result) {
                                            return DropdownMenuItem(
                                              value: result["name"],
                                              child: Text(result["name"]),
                                            );
                                          }).toList(),
                                          onChanged: (newValue) async {
                                            setState(() {
                                              clientName = newValue;
                                            });
                                          },
                                          isRequiredNotEmpty: true),
                                    ),
                                  ],
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  children: [
                                    const Expanded(
                                        child: MultiLanguageText(
                                            name: "project_type",
                                            defaultValue: "Project type",
                                            suffiText: " : ",
                                            style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
                                    Expanded(
                                      flex: 3,
                                      child: DynamicDropdownButton(
                                          value: projectType,
                                          hint: multiLanguageString(
                                              name: "choose_a_project_type", defaultValue: "Choose a project type", context: context),
                                          items: listProjectType.map<DropdownMenuItem>((result) {
                                            return DropdownMenuItem(
                                              value: result["id"],
                                              child: Text(result["title"]),
                                            );
                                          }).toList(),
                                          onChanged: (newValue) async {
                                            setState(() {
                                              projectType = newValue;
                                            });
                                          },
                                          isRequiredNotEmpty: true),
                                    ),
                                  ],
                                ),
                                if (projectType != null)
                                  const SizedBox(
                                    height: 10,
                                  ),
                                if (projectType != null)
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          MultiLanguageText(
                                              name: "element_template", defaultValue: "Element template", style: Style(context).styleTitleDialog),
                                          GestureDetector(
                                            onTap: () {
                                              Navigator.of(context).push(
                                                createRoute(
                                                  ManageElementTemplateWidget(
                                                    elementTemplateList: elementTemplateList,
                                                    prjProjectTypeId: projectType!,
                                                    onSave: (elementTemplateList) {
                                                      setState(() {
                                                        this.elementTemplateList = elementTemplateList;
                                                      });
                                                    },
                                                  ),
                                                ),
                                              );
                                            },
                                            child: MultiLanguageText(
                                              name: "click_to_manage_element_template",
                                              defaultValue: "Click To Manage Element Template",
                                              style: Style(context).styleTitleDialog,
                                            ),
                                          )
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          Navigator.of(context).push(
                                            createRoute(
                                              ManageElementTemplateWidget(
                                                elementTemplateList: elementTemplateList,
                                                prjProjectTypeId: projectType!,
                                                onSave: (elementTemplateList) {
                                                  setState(() {
                                                    this.elementTemplateList = elementTemplateList;
                                                  });
                                                },
                                              ),
                                            ),
                                          );
                                        },
                                        child: Container(
                                          alignment: Alignment.centerLeft,
                                          constraints: const BoxConstraints(minHeight: 50),
                                          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                              color: const Color.fromRGBO(216, 218, 229, 1),
                                            ),
                                            borderRadius: BorderRadius.circular(8),
                                          ),
                                          child: Wrap(
                                            children: [
                                              for (var elementTemplate in elementTemplateList)
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(right: elementTemplateList.last != elementTemplate ? 10 : 0, bottom: 5, top: 5),
                                                  child: IntrinsicWidth(
                                                    child: ElementTemplateWidget(
                                                      elementTemplate: elementTemplate,
                                                      onRemove: () {
                                                        showDialog<String>(
                                                          context: context,
                                                          builder: (BuildContext context) {
                                                            return DialogConfirm(
                                                              callback: () => setState(() {
                                                                elementTemplateList.remove(elementTemplate);
                                                              }),
                                                            );
                                                          },
                                                        );
                                                      },
                                                    ),
                                                  ),
                                                )
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                              ],
                            ),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: SizedBox(
                                  height: 50,
                                  child: DynamicTextField(
                                    controller: _searchLibrary,
                                    onChanged: (value) {
                                      setState(() {
                                        _searchLibrary.text;
                                      });
                                      if (_debounceTimer?.isActive ?? false) {
                                        _debounceTimer?.cancel();
                                      }
                                      _debounceTimer = Timer(const Duration(milliseconds: 500), () {
                                        findRequestElementRepository["titleLike"] = _searchLibrary.text;
                                        getListTfElementRepository(currentPageListElementRepository, findRequestElementRepository);
                                      });
                                    },
                                    labelText: multiLanguageString(
                                        name: "search_element_repository_name", defaultValue: "Search element repository name", context: context),
                                    hintText: multiLanguageString(
                                        name: "by_element_repository_name", defaultValue: "By element repository name", context: context),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Column(
                            children: [
                              Container(
                                margin: const EdgeInsets.only(top: 10),
                                color: Colors.yellow[200],
                                child: MultiLanguageText(
                                  name: "select_an_element_repository",
                                  defaultValue: "Select an element repository",
                                  style: Style(context).styleTitleDialog,
                                ),
                              ),
                              if (elementRepositoryPathList != null && elementRepositoryPathList!.isNotEmpty) breadCrumbsWidget(),
                              tableWidget(),
                              DynamicTablePagging(
                                rowCountListElementRepository,
                                currentPageListElementRepository,
                                rowPerPageElementRepository,
                                pageChangeHandler: (page) {
                                  getListTfElementRepository(page, findRequestElementRepository);
                                },
                                rowPerPageChangeHandler: (value) {
                                  setState(() {
                                    rowPerPageElementRepository = value;
                                    getListTfElementRepository(1, findRequestElementRepository);
                                  });
                                },
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    btnWidget(),
                  ],
                ),
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          });
    });
  }

  Widget headerWidget() {
    return Row(
      children: [
        Expanded(
          child: Center(
            child: Text(
              widget.title,
              style: Style(context).styleTitleDialog,
            ),
          ),
        ),
        TextButton(
          onPressed: () => Navigator.pop(context, 'Cancel'),
          child: const Icon(
            Icons.close,
            color: Colors.black,
          ),
        )
      ],
    );
  }

  Widget btnWidget() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 50),
      child: Column(
        children: [
          Container(
            margin: const EdgeInsets.only(bottom: 10),
            alignment: Alignment.centerRight,
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              onPressed: () {
                Navigator.of(context).push(
                  createRoute(
                    OptionElementRepositoryWidget(
                      type: "create",
                      title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                      callbackOptionTest: (result) {
                        handleAddTfAll(result);
                      },
                      prjProjectId: prjProjectId ?? "",
                    ),
                  ),
                );
              },
              icon: const Icon(Icons.note_add, color: Color.fromRGBO(49, 49, 49, 1)),
              label: MultiLanguageText(
                name: "new_element_repository",
                defaultValue: "New Element Repository",
                isLowerCase: false,
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
            ),
          ),
          ConstrainedBox(
            constraints: const BoxConstraints(minHeight: 40, minWidth: 120),
            child: ElevatedButton(
              onPressed: ((widget.testCaseId != null ? clientName != null : true) && selectedElementRepository != null)
                  ? () {
                      widget.onClickContinue(
                          selectedElementRepository,
                          (testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"] ?? []).firstWhere(
                            (element) => element["name"] == clientName,
                            orElse: () => null,
                          ),
                          widget.testCaseId != null
                              ? {
                                  "prjProjectTypeId": projectType,
                                  "tfFindElementTemplateIdInList": elementTemplateList.map((e) => e["id"]).toList(),
                                }
                              : {},
                          context);
                    }
                  : null,
              child: const MultiLanguageText(
                name: "continue",
                defaultValue: "Continue",
                style: TextStyle(fontWeight: FontWeight.bold, letterSpacing: 2),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget tableWidget() {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.all(15),
          decoration: const BoxDecoration(
            border: Border(
              top: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
              bottom: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
              left: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
              right: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
            ),
          ),
          child: Row(
            children: [
              Expanded(
                  flex: 3,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: MultiLanguageText(name: "name", defaultValue: "Name", isLowerCase: false, style: Style(context).styleTextDataColumn),
                  )),
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: Center(
                    child: MultiLanguageText(
                        name: "description", defaultValue: "Description", isLowerCase: false, style: Style(context).styleTextDataColumn)),
              )),
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: Center(
                    child: MultiLanguageText(name: "owner", defaultValue: "Owner", isLowerCase: false, style: Style(context).styleTextDataColumn)),
              )),
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: Center(
                    child: MultiLanguageText(
                        name: "create_date", defaultValue: "Create date", isLowerCase: false, style: Style(context).styleTextDataColumn)),
              )),
            ],
          ),
        ),
        Column(
          children: [
            for (var elementrepository in listElementRepository)
              GestureDetector(
                onTap: () {
                  findRequestElementRepository = {};
                  parentId = elementrepository["id"];
                  findRequestElementRepository["parentId"] = elementrepository["id"];
                  currentPageListElementRepository = 1;
                  getListTfElementRepository(currentPageListElementRepository, findRequestElementRepository);
                  getElementRepositoryPathList();
                },
                child: Container(
                  padding: const EdgeInsets.all(15),
                  decoration: const BoxDecoration(
                    border: Border(
                      top: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
                      bottom: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
                      left: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
                      right: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
                    ),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        flex: 3,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 5),
                          child: Row(
                            children: [
                              Radio(
                                value: elementrepository["id"],
                                groupValue: selectedElementRepository,
                                onChanged: (dynamic value) {
                                  setState(() {
                                    selectedElementRepository = value;
                                  });
                                },
                              ),
                              Text(
                                elementrepository["title"] ?? "",
                                maxLines: 3,
                                softWrap: true,
                                style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 5),
                          child: Center(
                            child: Text(
                              elementrepository["description"] ?? "",
                              maxLines: 3,
                              softWrap: true,
                              style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 5),
                          child: Center(
                            child: Text(
                              elementrepository["sysUser"]["fullname"],
                              maxLines: 3,
                              softWrap: true,
                              style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 5),
                          child: Center(
                            child: Text(
                              DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(elementrepository["createDate"]).toLocal()),
                              maxLines: 3,
                              softWrap: true,
                              style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
          ],
        )
      ],
    );
  }

  Widget breadCrumbsWidget() {
    return Row(
      children: [
        IconButton(
          onPressed: () {
            findRequestElementRepository = {};
            parentId = null;
            findRequestElementRepository["parentIdIsNull"] = true;
            getListTfElementRepository(currentPageListElementRepository, findRequestElementRepository);
            elementRepositoryPathList = [];
          },
          icon: const FaIcon(FontAwesomeIcons.house, size: 16),
          color: Colors.grey,
          splashRadius: 10,
          tooltip: multiLanguageString(name: "root", defaultValue: "Root", context: context),
        ),
        for (var elementRepositoryPath in elementRepositoryPathList!)
          RichText(
              text: TextSpan(children: [
            const TextSpan(
              text: " / ",
              style: TextStyle(color: Colors.grey),
            ),
            TextSpan(
                text: "${elementRepositoryPath["title"]}",
                style: TextStyle(
                    color: elementRepositoryPathList!.last == elementRepositoryPath ? Colors.blue : Colors.grey,
                    fontSize: context.fontSizeManager.fontSizeText16,
                    fontWeight: FontWeight.bold),
                recognizer: TapGestureRecognizer()
                  ..onTap = () {
                    findRequestElementRepository = {};
                    parentId = elementRepositoryPath["id"];
                    findRequestElementRepository["parentId"] = elementRepositoryPath["id"];
                    getListTfElementRepository(currentPageListElementRepository, findRequestElementRepository);
                    getElementRepositoryPathList();
                  }),
          ]))
      ],
    );
  }
}
