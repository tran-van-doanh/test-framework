import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/editor/action_button_editor/dialog_play_testcase.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/root_item_widget.dart';
import 'package:test_framework_view/type.dart';

import '../../../common/custom_state.dart';

class DialogChooseElement extends StatefulWidget {
  final String? tfTestCaseId;
  const DialogChooseElement({Key? key, required this.tfTestCaseId}) : super(key: key);

  @override
  State<DialogChooseElement> createState() => _DialogChooseElementState();
}

class _DialogChooseElementState extends CustomState<DialogChooseElement> {
  late Future futureBodyListPlay;
  List<Item> resultBodyList = [];

  @override
  void initState() {
    futureBodyListPlay = getInitPage();
    super.initState();
  }

  Future getInitPage() async {
    await getBodyList();
    await setListId();
    return 0;
  }

  setListId() {
    Provider.of<ListElementPlayIdModel>(context, listen: false).setListId(Provider.of<ListElementPlayIdModel>(context, listen: false).listAllId);
  }

  getBodyList() async {
    var response = {};
    response = await httpGet("/test-framework-api/user/test-framework/tf-test-case/${widget.tfTestCaseId}", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        if (response["body"]["result"]["content"]["bodyList"] != null) {
          for (var bodyListTestCase in response["body"]["result"]["content"]["bodyList"]) {
            checkTypeWidget(bodyListTestCase, resultBodyList, response["body"]["result"]["content"]["bodyList"].indexOf(bodyListTestCase));
          }
        }
      });
    }
  }

  checkTypeWidget(dynamic listCheckType, bodyTypeAdd, int index) {
    Item itemBodyList = checkItemWidget(listCheckType);
    bodyTypeAdd.insert(index, itemBodyList);
  }

  checkItemWidget(dynamic listCheckType) {
    if (listCheckType["type"] == "error" && listCheckType["message"] != null) {
      listCheckType["variablePath"] = [
        {"type": "value", "value": listCheckType["message"], "category": "variablePath", "dataType": "String"}
      ];
    }
    Item itemBodyList = Item(listCheckType["type"], listCheckType["category"]);
    if (listCheckType["type"] == "value") {
      if (listCheckType["tfTestElementId"] != null) {
        itemBodyList.tfTestElementId = listCheckType["tfTestElementId"];
      }
      if (listCheckType["tfTestElementRepositoryId"] != null) {
        itemBodyList.tfTestElementRepositoryId = listCheckType["tfTestElementRepositoryId"];
      }
      if (listCheckType["tfValueTemplateId"] != null) {
        itemBodyList.tfValueTemplateId = listCheckType["tfValueTemplateId"];
      }
    }
    if (listCheckType["type"] == "variable_default") {
      if (listCheckType["variablePath"] != null) {
        for (var i = 0; i < listCheckType["variablePath"].length; i++) {
          checkTypeWidget(listCheckType["variablePath"][i], itemBodyList.variablePath, i);
        }
      }
    }
    if (listCheckType["id"] != null) itemBodyList.id = listCheckType["id"];
    if (listCheckType["key"] != null) itemBodyList.key = listCheckType["key"];
    if (listCheckType["mouseActionType"] != null) itemBodyList.mouseActionType = listCheckType["mouseActionType"];
    if (listCheckType["endKey"] != null) itemBodyList.endKey = listCheckType["endKey"];
    if (listCheckType["thenBodyList"] != null) {
      for (var thenBody in listCheckType["thenBodyList"]) {
        checkTypeWidget(thenBody, itemBodyList.thenBodyList, listCheckType["thenBodyList"].indexOf(thenBody));
      }
    }
    if (listCheckType["elseBodyList"] != null) {
      for (var elseBody in listCheckType["elseBodyList"]) {
        checkTypeWidget(elseBody, itemBodyList.elseBodyList, listCheckType["elseBodyList"].indexOf(elseBody));
      }
    }
    if (listCheckType["onErrorBodyList"] != null) {
      for (var errorBody in listCheckType["onErrorBodyList"]) {
        checkTypeWidget(errorBody, itemBodyList.onErrorBodyList, listCheckType["onErrorBodyList"].indexOf(errorBody));
      }
    }
    if (listCheckType["onFinishBodyList"] != null) {
      for (var finalBody in listCheckType["onFinishBodyList"]) {
        checkTypeWidget(finalBody, itemBodyList.onFinishBodyList, listCheckType["onFinishBodyList"].indexOf(finalBody));
      }
    }
    if (listCheckType["bodyList"] != null) {
      for (var body in listCheckType["bodyList"]) {
        checkTypeWidget(body, itemBodyList.bodyList, listCheckType["bodyList"].indexOf(body));
      }
    }
    if (listCheckType["variablePath"] != null) {
      itemBodyList.variablePath = [];
      for (var list in listCheckType["variablePath"]) {
        checkTypeWidget(list, itemBodyList.variablePath, listCheckType["variablePath"].indexOf(list));
      }
    }
    if (listCheckType["conditionList"] != null) {
      if (listCheckType["conditionList"].length == 1) {
        itemBodyList.conditionList = [Item("condition_default", "condition_default")];
        checkTypeWidget(listCheckType["conditionList"][0], itemBodyList.conditionList, 0);
      } else {
        itemBodyList.conditionList = [];
        for (var i = 0; i < listCheckType["conditionList"].length; i++) {
          checkTypeWidget(listCheckType["conditionList"][i], itemBodyList.conditionList, i);
        }
      }
    }
    if (listCheckType["operatorList"] != null) {
      if (listCheckType["operatorList"].length == 1) {
        itemBodyList.operatorList = [Item("variable_default", "variable_default")];
        checkTypeWidget(listCheckType["operatorList"][0], itemBodyList.operatorList, 0);
      } else {
        itemBodyList.operatorList = [];
        for (var i = 0; i < listCheckType["operatorList"].length; i++) {
          checkTypeWidget(listCheckType["operatorList"][i], itemBodyList.operatorList, i);
        }
      }
    }
    if (listCheckType["conditionGroup"] != null) {
      itemBodyList.conditionGroup = checkItemWidget(listCheckType["conditionGroup"]);
    }
    if (listCheckType["name"] != null) {
      itemBodyList.name = listCheckType["name"];
    }
    if (listCheckType["platform"] != null) {
      itemBodyList.platform = listCheckType["platform"];
    }
    if (listCheckType["clientName"] != null) {
      itemBodyList.clientName = listCheckType["clientName"];
    }
    if (listCheckType["clientList"] != null) {
      itemBodyList.clientList = listCheckType["clientList"];
    }
    if (listCheckType["description"] != null) {
      itemBodyList.description = listCheckType["description"];
    }
    if (listCheckType["value"] != null) {
      itemBodyList.value = listCheckType["value"];
    }
    if (listCheckType["dataType"] != null) {
      itemBodyList.dataType = listCheckType["dataType"];
    }
    if (listCheckType["type"] == "actionCall" && listCheckType["actionName"] != null) {
      itemBodyList.actionName = listCheckType["actionName"];
    }
    if (listCheckType["type"] == "testCaseCall" && listCheckType["testCaseName"] != null) {
      itemBodyList.testCaseName = listCheckType["testCaseName"];
    }
    if (listCheckType["type"] == "apiCall" && listCheckType["apiName"] != null) {
      itemBodyList.apiName = listCheckType["apiName"];
    }
    if (listCheckType["objectName"] != null) {
      itemBodyList.objectName = listCheckType["objectName"];
    }
    if (listCheckType["operatorType"] != null) {
      itemBodyList.operatorType = listCheckType["operatorType"];
    }
    if (listCheckType["message"] != null) {
      itemBodyList.message = listCheckType["message"];
    }
    if (listCheckType["isList"] != null) {
      itemBodyList.isList = listCheckType["isList"];
    }
    if (listCheckType["returnType"] != null) {
      itemBodyList.returnType = listCheckType["returnType"];
    }
    if (listCheckType["o1"] != null) {
      itemBodyList.o1 = checkItemWidget(listCheckType["o1"]);
    }
    if (listCheckType["o2"] != null) {
      itemBodyList.o2 = checkItemWidget(listCheckType["o2"]);
    }
    if (listCheckType["parameterList"] != null) {
      itemBodyList.parameterList = listCheckType["parameterList"];
    }
    itemBodyList.isSelected = true;
    if (itemBodyList.type != "add" &&
        itemBodyList.type != "sub" &&
        itemBodyList.type != "mul" &&
        itemBodyList.type != "div" &&
        itemBodyList.category != "condition" &&
        itemBodyList.category != "conditionGroup" &&
        itemBodyList.type != "conditionGroup_default" &&
        itemBodyList.type != "variableGroup_default" &&
        itemBodyList.type != "condition_default" &&
        itemBodyList.type != "variable_default" &&
        itemBodyList.type != "value" &&
        itemBodyList.type != "variable" &&
        itemBodyList.type != "version_variable" &&
        itemBodyList.type != "environment_variable" &&
        itemBodyList.type != "node_variable" &&
        itemBodyList.type != "get") {
      Provider.of<ListElementPlayIdModel>(context, listen: false).addAllIdList(itemBodyList.id);
    }
    return itemBodyList;
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ListElementPlayIdModel>(builder: (context, listElementPlayIdModel, child) {
      return FutureBuilder(
        future: futureBodyListPlay,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return AlertDialog(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  MultiLanguageText(
                    name: "choose_steps_to_play",
                    defaultValue: "Choose steps to play",
                    style: Style(context).styleTitleDialog,
                  ),
                  TextButton(
                    onPressed: () {
                      listElementPlayIdModel.clearProvider();
                      Navigator.pop(context, 'Cancel');
                    },
                    child: const Icon(
                      Icons.close,
                      color: Colors.black,
                    ),
                  )
                ],
              ),
              content: Container(
                decoration: const BoxDecoration(
                  border: Border(
                    top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
                    bottom: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
                  ),
                ),
                padding: const EdgeInsets.all(10),
                constraints: const BoxConstraints(minWidth: 600),
                child: IntrinsicHeight(
                  child: resultBodyList.isNotEmpty
                      ? SingleChildScrollView(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              for (Item bodyItem in resultBodyList)
                                RootItemWidget(
                                  bodyItem,
                                  hasClick: false,
                                  hasCopy: false,
                                  hasDelete: false,
                                  hasDuplicate: false,
                                  hasPlay: false,
                                  hasCheckBox: true,
                                  parentItem: bodyItem,
                                  hasGlobalKey: true,
                                )
                            ],
                          ),
                        )
                      : Center(
                          child: MultiLanguageText(
                            name: "list_element_is_empty",
                            defaultValue: "List element is empty, please configure !",
                            style: TextStyle(fontSize: context.fontSizeManager.fontSizeText20, fontWeight: FontWeight.bold),
                          ),
                        ),
                ),
              ),
              actionsAlignment: MainAxisAlignment.center,
              actions: [
                Container(
                  margin: const EdgeInsets.only(right: 5),
                  height: 40,
                  width: 120,
                  child: ElevatedButton(
                    onPressed: () {
                      listElementPlayIdModel.setListId(listElementPlayIdModel.listAllId);
                      for (var bodyItem in resultBodyList) {
                        bodyItem.isSelected = true;
                      }
                      listElementPlayIdModel.setIsSelectedAll(true);
                    },
                    style: ElevatedButton.styleFrom(
                      side: const BorderSide(color: Color.fromRGBO(209, 212, 216, 1)),
                      backgroundColor: Colors.white,
                      foregroundColor: const Color.fromRGBO(61, 90, 254, 1),
                    ),
                    child: const MultiLanguageText(
                      name: "select_all",
                      defaultValue: "Select all",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(right: 5),
                  height: 40,
                  width: 120,
                  child: ElevatedButton(
                    onPressed: () {
                      listElementPlayIdModel.setListId(<String>[]);
                      for (var bodyItem in resultBodyList) {
                        bodyItem.isSelected = false;
                      }
                      listElementPlayIdModel.setIsSelectedAll(false);
                    },
                    style: ElevatedButton.styleFrom(
                      side: const BorderSide(color: Color.fromRGBO(209, 212, 216, 1)),
                      backgroundColor: Colors.white,
                      foregroundColor: const Color.fromRGBO(61, 90, 254, 1),
                    ),
                    child: const MultiLanguageText(
                      name: "unselect_all",
                      defaultValue: "Unselect all",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(right: 5),
                  height: 40,
                  width: 120,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.pop(context);
                      showDialog<String>(
                        context: context,
                        builder: (BuildContext context) {
                          return DialogPlayTestcase(
                            tfTestCaseId: widget.tfTestCaseId,
                          );
                        },
                      );
                    },
                    child: const MultiLanguageText(
                      name: "continue",
                      defaultValue: "Continue",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(left: 5),
                  height: 40,
                  width: 120,
                  child: ElevatedButton(
                    onPressed: () {
                      listElementPlayIdModel.clearProvider();
                      Navigator.pop(context);
                    },
                    style: ElevatedButton.styleFrom(
                      side: const BorderSide(color: Color.fromRGBO(209, 212, 216, 1)),
                      backgroundColor: Colors.white,
                      foregroundColor: const Color.fromRGBO(61, 90, 254, 1),
                    ),
                    child: const MultiLanguageText(
                      name: "cancel",
                      defaultValue: "Cancel",
                      isLowerCase: false,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                )
              ],
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        },
      );
    });
  }
}
