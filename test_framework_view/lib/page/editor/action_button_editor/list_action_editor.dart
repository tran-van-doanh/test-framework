import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/custom_draggable.dart';
import 'package:test_framework_view/components/dialog/dialog_back.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/page/editor/action_button_editor/dialog_start_debug.dart';
import 'package:test_framework_view/page/editor/action_button_editor/view_screen_shot_config.dart';
import 'package:test_framework_view/page/editor/drag&drop_widget/root_item_widget.dart';
import 'package:test_framework_view/page/list_test/tests/test/dialog_option_test.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/type.dart';

import '../../../common/custom_state.dart';

class OptionEditorWidget extends StatefulWidget {
  final dynamic parameterFieldList;
  final List<Item> bodyList;
  final String? testCaseId;
  final dynamic testCaseItem;
  final String? tfTestDirectoryId;
  final VoidCallback callback;
  final Function? callbackAddTest;
  final String testCaseType;
  const OptionEditorWidget({
    Key? key,
    this.parameterFieldList,
    required this.bodyList,
    this.testCaseId,
    this.testCaseItem,
    required this.callback,
    this.tfTestDirectoryId,
    this.callbackAddTest,
    required this.testCaseType,
  }) : super(key: key);

  @override
  State<OptionEditorWidget> createState() => _OptionEditorWidgetState();
}

class _OptionEditorWidgetState extends CustomState<OptionEditorWidget> {
  bool willAcceptDelete = false;
  String? testCaseNewId;
  updateTestCaseById(String id) async {
    TestCaseConfigModel testCaseConfigModel = Provider.of<TestCaseConfigModel>(context, listen: false);
    widget.testCaseItem["content"] ??= {};
    widget.testCaseItem["content"]["bodyList"] = widget.bodyList;
    widget.testCaseItem["content"]["parameterFieldList"] =
        (testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["parameterFieldList"] ?? []);
    widget.testCaseItem["content"]["clientList"] =
        (testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"] ?? []);
    var responseUpdate = await httpPost("/test-framework-api/user/test-framework/tf-test-case/$id", widget.testCaseItem, context);
    if (responseUpdate.containsKey("body") && responseUpdate["body"] is String == false && mounted) {
      if (responseUpdate["body"].containsKey("errorMessage") && mounted) {
        showToast(context: context, msg: responseUpdate["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        widget.callbackAddTest!(widget.testCaseItem);
        showToast(
            context: context,
            msg: multiLanguageString(name: "test_has_been_updated", defaultValue: "Test has been updated", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
      }
    }
  }

  handleAddTfTestCase(result) async {
    var response = await httpPut("/test-framework-api/user/test-framework/tf-test-case", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        var response1 = await httpGet("/test-framework-api/user/test-framework/tf-test-case/${response["body"]["result"]}", context);
        response1["body"]["result"]["content"]["bodyList"] = widget.bodyList;
        if (mounted) {
          var responseUpdate = await httpPost(
              "/test-framework-api/user/test-framework/tf-test-case/${response["body"]["result"]}", response1["body"]["result"], context);
          if (responseUpdate.containsKey("body") && responseUpdate["body"] is String == false && mounted) {
            if (responseUpdate["body"].containsKey("errorMessage")) {
              showToast(context: context, msg: responseUpdate["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
            } else {
              widget.callbackAddTest!(response1["body"]["result"]);
              testCaseNewId = response1["body"]["result"]["id"];
              Navigator.pop(context);
              showToast(
                  context: context,
                  msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
                  color: Colors.greenAccent,
                  icon: const Icon(Icons.done));
            }
          }
        }
      }
    }
  }

  handleStopDebug() async {
    var findRequest = {};
    await httpPost("/test-framework-api/user/node/stop", findRequest, context);
    if (mounted) {
      Provider.of<DebugStatusModel>(context, listen: false).clearProvider();
    }
  }

  getStatusDebug() async {
    var statusResponse = await httpGet("/test-framework-api/user/node/status", context);
    if (statusResponse.containsKey("body") && statusResponse["body"] is String == false && statusResponse["body"].containsKey("result")) {
      if (statusResponse["body"]["result"]["status"] == 2 && mounted) {
        DebugStatusModel debugStatusModel = Provider.of<DebugStatusModel>(context, listen: false);
        debugStatusModel.setVersionStartDebug(statusResponse["body"]["result"]["tfTestVersion"]);
        debugStatusModel.setEnvironmentStartDebug(statusResponse["body"]["result"]["tfTestEnvironment"]);
        debugStatusModel.setProfileStartDebug(statusResponse["body"]["result"]["tfTestProfile"]);
        debugStatusModel.setDebugStatus(true);
      }
    }
  }

  @override
  void initState() {
    super.initState();
    getStatusDebug();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<DebugStatusModel, CopyWidgetModel>(
      builder: (context, debugStatusModel, copyWidgetModel, child) {
        return Align(
          alignment: Alignment.centerRight,
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                FloatingActionButton.extended(
                  heroTag: null,
                  onPressed: () {
                    showDialog<String>(
                      context: context,
                      builder: (BuildContext context) {
                        return DialogBack(
                          callback: () {
                            TestCaseConfigModel testCaseConfigModel = Provider.of<TestCaseConfigModel>(context, listen: false);
                            testCaseConfigModel.removeTestCaseIdList(testCaseConfigModel.testCaseIdList.last);
                            testCaseConfigModel.removeTestCaseNameList(testCaseConfigModel.testCaseNameList.last);
                            widget.callback();
                            Navigator.pop(context);
                          },
                        );
                      },
                    );
                  },
                  icon: const Icon(Icons.arrow_back, color: Color.fromRGBO(49, 49, 49, 1)),
                  label: MultiLanguageText(
                    name: "back",
                    defaultValue: "Back",
                    style: TextStyle(
                        fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
                  ),
                  backgroundColor: Colors.grey,
                  elevation: 0,
                  hoverElevation: 0,
                ),
                if (copyWidgetModel.copyWidget != null) const SizedBox(width: 10),
                if (copyWidgetModel.copyWidget != null)
                  CustomDraggable(
                    data: copyWidgetModel.copyWidget,
                    childWhenDragging: Container(),
                    feedback: SizedBox(
                      width: MediaQuery.of(context).size.width * 0.5,
                      child: RootItemWidget(copyWidgetModel.copyWidget!),
                    ),
                    child: Container(
                      padding: const EdgeInsets.all(12),
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(25), color: Colors.deepPurple[200]),
                      child: Row(
                        children: [
                          const Icon(Icons.drag_indicator),
                          MultiLanguageText(
                              name: "drag_to_paste",
                              defaultValue: "Drag to paste",
                              style: TextStyle(
                                  fontSize: context.fontSizeManager.fontSizeText13,
                                  fontWeight: FontWeight.w600,
                                  color: const Color.fromRGBO(49, 49, 49, 1))),
                        ],
                      ),
                    ),
                    onDragCompleted: () {
                      // copyWidgetModel.setCopyWidget(null);
                    },
                  ),
                const SizedBox(width: 10),
                DragTarget<Dragable>(
                  builder: (
                    BuildContext context,
                    List<dynamic> accepted,
                    List<dynamic> rejected,
                  ) {
                    return Container(
                      padding: const EdgeInsets.fromLTRB(20, 12, 20, 12),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(25),
                        color: willAcceptDelete ? const Color.fromRGBO(220, 53, 69, 1) : const Color.fromRGBO(220, 53, 69, .7),
                      ),
                      child: Row(
                        children: [
                          const Icon(Icons.delete, color: Color.fromRGBO(49, 49, 49, 1)),
                          MultiLanguageText(
                            name: "delete",
                            defaultValue: "Delete",
                            style: TextStyle(
                                fontSize: context.fontSizeManager.fontSizeText13,
                                fontWeight: FontWeight.w600,
                                color: const Color.fromRGBO(49, 49, 49, 1)),
                          )
                        ],
                      ),
                    );
                  },
                  onWillAccept: (data) {
                    willAcceptDelete = true;
                    return true;
                  },
                  onLeave: (data) {
                    willAcceptDelete = false;
                  },
                  onAccept: (Dragable data) {
                    willAcceptDelete = false;
                  },
                ),
                const SizedBox(width: 10),
                FloatingActionButton.extended(
                  heroTag: null,
                  onPressed: () async {
                    if (testCaseNewId != null) {
                      updateTestCaseById(testCaseNewId!);
                    } else if (widget.testCaseId != null) {
                      updateTestCaseById(widget.testCaseId!);
                    } else {
                      dialogCreateNewTest();
                    }
                  },
                  icon: const Icon(Icons.save, color: Color.fromRGBO(49, 49, 49, 1)),
                  label: MultiLanguageText(
                    name: "save",
                    defaultValue: "Save",
                    style: TextStyle(
                        fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
                  ),
                  backgroundColor: const Color.fromARGB(177, 21, 117, 182),
                  elevation: 0,
                  hoverElevation: 0,
                ),
                if (widget.testCaseId != null || testCaseNewId != null) const SizedBox(width: 10),
                if (widget.testCaseId != null || testCaseNewId != null)
                  FloatingActionButton.extended(
                    heroTag: null,
                    onPressed: () async {
                      var responseGetTestCase =
                          await httpGet("/test-framework-api/user/test-framework/tf-test-case/${testCaseNewId ?? widget.testCaseId}", context);
                      if (mounted) {
                        Navigator.of(this.context).push(
                          createRoute(
                            DialogOptionTestWidget(
                              testCaseType: widget.testCaseType,
                              type: "duplicate",
                              title: multiLanguageString(name: "duplicate", defaultValue: "Duplicate", context: this.context),
                              selectedItem: (responseGetTestCase.containsKey("body") && responseGetTestCase["body"] is String == false)
                                  ? responseGetTestCase["body"]["result"] ?? {}
                                  : {},
                              callbackOptionTest: (result) {
                                handleAddTfTestCase(result);
                              },
                              parentId: widget.tfTestDirectoryId,
                            ),
                          ),
                        );
                      }
                    },
                    icon: const Icon(Icons.save, color: Color.fromRGBO(49, 49, 49, 1)),
                    label: MultiLanguageText(
                      name: "save_as",
                      defaultValue: "Save as",
                      style: TextStyle(
                          fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
                    ),
                    backgroundColor: const Color.fromARGB(177, 21, 117, 182),
                    elevation: 0,
                    hoverElevation: 0,
                  ),
                const SizedBox(width: 10),
                debugStatusModel.debugStatus
                    ? Wrap(
                        children: [
                          FloatingActionButton.extended(
                            heroTag: null,
                            onPressed: () async {
                              Navigator.of(context).push(
                                createRoute(
                                  ViewScreenShotWidget(tfTestCaseId: widget.testCaseId!),
                                ),
                              );
                            },
                            icon: const Icon(Icons.image, color: Color.fromRGBO(49, 49, 49, 1)),
                            label: MultiLanguageText(
                              name: "view_screenshoot",
                              defaultValue: "View Screenshot",
                              style: TextStyle(
                                  fontSize: context.fontSizeManager.fontSizeText13,
                                  fontWeight: FontWeight.w600,
                                  color: const Color.fromRGBO(49, 49, 49, 1)),
                            ),
                            backgroundColor: const Color.fromARGB(174, 71, 193, 209),
                            elevation: 0,
                            hoverElevation: 0,
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          FloatingActionButton.extended(
                            heroTag: null,
                            onPressed: () async {
                              onLoading(context);
                              await handleStopDebug();
                              if (mounted) {
                                Navigator.pop(this.context);
                                showToast(
                                    context: this.context,
                                    msg: multiLanguageString(name: "stop_debug_success", defaultValue: "Stop debug success", context: this.context),
                                    color: Colors.greenAccent,
                                    icon: const Icon(Icons.done));
                              }
                            },
                            icon: const Icon(Icons.cancel, color: Color.fromRGBO(49, 49, 49, 1)),
                            label: MultiLanguageText(
                              name: "stop_debug",
                              defaultValue: "Stop Debug",
                              style: TextStyle(
                                  fontSize: context.fontSizeManager.fontSizeText13,
                                  fontWeight: FontWeight.w600,
                                  color: const Color.fromRGBO(49, 49, 49, 1)),
                            ),
                            backgroundColor: const Color.fromARGB(175, 255, 94, 94),
                            elevation: 0,
                            hoverElevation: 0,
                          ),
                        ],
                      )
                    : Row(
                        children: [
                          FloatingActionButton.extended(
                            heroTag: null,
                            onPressed: () async {
                              dialogStartDebug();
                            },
                            icon: const Icon(Icons.play_arrow, color: Color.fromRGBO(49, 49, 49, 1)),
                            label: MultiLanguageText(
                              name: "start_debug",
                              defaultValue: "Start Debug",
                              style: TextStyle(
                                  fontSize: context.fontSizeManager.fontSizeText13,
                                  fontWeight: FontWeight.w600,
                                  color: const Color.fromRGBO(49, 49, 49, 1)),
                            ),
                            backgroundColor: const Color.fromARGB(175, 8, 196, 80),
                            elevation: 0,
                            hoverElevation: 0,
                          ),
                        ],
                      ),
              ],
            ),
          ),
        );
      },
    );
  }

  dialogCreateNewTest() {
    Navigator.of(context).push(
      createRoute(
        DialogOptionTestWidget(
          testCaseType: widget.testCaseType,
          type: "create",
          title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
          selectedItem: widget.testCaseItem,
          callbackOptionTest: (result) {
            handleAddTfTestCase(result);
          },
          parentId: widget.tfTestDirectoryId,
        ),
      ),
    );
  }

  dialogStartDebug() {
    TestCaseConfigModel testCaseConfigModel = Provider.of<TestCaseConfigModel>(context, listen: false);
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogStartDebug(clientList: testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"] ?? []);
      },
    );
  }
}
