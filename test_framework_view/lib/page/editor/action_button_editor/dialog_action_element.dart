import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/editor/property_widget/dynamic_variabla_path_widget.dart';
import 'package:test_framework_view/type.dart';

import '../../../common/custom_state.dart';
import '../../../common/dynamic_dropdown_button.dart';

class DialogActionElement extends StatefulWidget {
  final String? tfTestCaseId;
  final String type;
  final String? mouseActionTypeItem;
  final String? keyItem;
  final List? parameterList;
  final String? clientName;
  final String? platform;
  const DialogActionElement({
    Key? key,
    this.tfTestCaseId,
    required this.type,
    this.parameterList,
    this.mouseActionTypeItem,
    this.keyItem,
    this.clientName,
    this.platform,
  }) : super(key: key);

  @override
  State<DialogActionElement> createState() => _DialogActionElementState();
}

class _DialogActionElementState extends CustomState<DialogActionElement> {
  var parameterList = [];
  final _formKey = GlobalKey<FormState>();
  dynamic responseText;
  String mouseActionType = "NONE";
  String key = "NONE";
  String? clientName;
  String? title;
  bool isLoad = false;
  String? keyType;
  @override
  void initState() {
    super.initState();
    getInitPage();
  }

  getInitPage() async {
    setState(() {
      if (widget.clientName != null && widget.clientName!.isNotEmpty) {
        clientName = widget.clientName!;
      }
      if (widget.mouseActionTypeItem != null) {
        mouseActionType = widget.mouseActionTypeItem!;
      }
      if (widget.keyItem != null) {
        key = widget.keyItem!;
      }
    });
    await getEditorComponent();
    setState(() {
      isLoad = true;
    });
  }

  getEditorComponent() async {
    var findRequest = {"status": 1, "type": widget.type};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-editor-component/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false && response["body"]["resultList"].isNotEmpty && mounted) {
      setState(() {
        title = response["body"]["resultList"][0]["title"];
        keyType = response["body"]["resultList"][0]["componentConfig"]["keyType"];
        if ((widget.parameterList == null || widget.parameterList!.isEmpty) &&
            response["body"]["resultList"].isNotEmpty &&
            response["body"]["resultList"][0]["componentConfig"] != null) {
          parameterList = (response["body"]["resultList"][0]["componentConfig"]["componentDebug"] ?? []);
        } else {
          parameterList.addAll(widget.parameterList!);
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if (isLoad) {
      return AlertDialog(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title ?? (widget.type == "getPageSource" ? "Get Page Source" : "Not Defined"),
              style: Style(context).styleTitleDialog,
            ),
            TextButton(
              onPressed: () => Navigator.pop(context, 'Cancel'),
              child: const Icon(
                Icons.close,
                color: Colors.black,
              ),
            )
          ],
        ),
        content: Container(
          decoration: const BoxDecoration(
            border: Border(
              top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
              bottom: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
            ),
          ),
          constraints: const BoxConstraints(minWidth: 600),
          child: Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    clientWidget(),
                    const SizedBox(
                      height: 20,
                    ),
                    if (widget.type == "mouseAction" || widget.type == "mouseActionWithOffset") chooseMouseActionTypeWidget(),
                    parameterListWidget(),
                    if (keyType != null && keyType!.isNotEmpty) chooseKeyWidget(),
                    if (responseText != null && parameterList.isNotEmpty)
                      const SizedBox(
                        height: 10,
                      ),
                    if (responseText != null)
                      SelectionArea(
                        child: responseText.runtimeType == List
                            ? Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const MultiLanguageText(name: "data", defaultValue: "Data"),
                                  for (var text in responseText) Text(text),
                                ],
                              )
                            : MultiLanguageText(name: "data_response_text", defaultValue: "Data : \$0", variables: ["$responseText"]),
                      ),
                  ],
                ),
              ),
            ),
          ),
        ),
        actionsAlignment: MainAxisAlignment.center,
        actions: [
          ButtonSave(
            onPressed: () async {
              if (_formKey.currentState!.validate()) {
                onLoading(context);
                var requestData = {"clientName": clientName};
                var findRequest = {
                  "type": widget.type,
                  "tfTestCaseId": widget.tfTestCaseId,
                  "data": requestData,
                  if (widget.type == "mouseAction" || widget.type == "mouseActionWithOffset") "mouseActionType": mouseActionType,
                  if (widget.type == "sendKeyFromElement" ||
                      widget.type == "childElementSendKeyFromElement" ||
                      widget.type == "sendKeyWithoutElement" ||
                      widget.type == "clearAndSendKeys" ||
                      widget.type == "clearAndSendKeysByPosition" ||
                      widget.type == "sendKeys" ||
                      widget.type == "sendKeysByPosition")
                    "key": key,
                };

                for (var parameter in parameterList) {
                  if (parameter["variablePath"][0]["dataType"] == "RepositoryElement") {
                    requestData["tfTestElementId"] = parameter["variablePath"][0]["tfTestElementId"];
                    requestData["tfTestElementXpathName"] = parameter["variablePath"][0]["tfTestElementXpathName"];
                  } else {
                    requestData[parameter['name']] = parameter["variablePath"][0]["value"];
                  }
                }
                var response = await httpPost("/test-framework-api/user/node/message", findRequest, context);
                if ((widget.type == 'getText' ||
                        widget.type == "getSelectOptionsValue" ||
                        widget.type == "getSelectVisibleOptionsValue" ||
                        widget.type == 'getAttribute' ||
                        widget.type == 'getAlertText' ||
                        widget.type == "getPageSource" ||
                        widget.type == 'getWindowHandle' ||
                        widget.type == 'getWindowHandles') &&
                    response.containsKey("body") &&
                    response["body"] is String == false) {
                  setState(() {
                    responseText = response["body"]["data"];
                  });
                }
                if (widget.type != "getText" &&
                    widget.type != "getSelectOptionsValue" &&
                    widget.type != "getSelectVisibleOptionsValue" &&
                    widget.type != "getAttribute" &&
                    widget.type != "getAlertText" &&
                    widget.type != "getPageSource" &&
                    widget.type != "getWindowHandle" &&
                    widget.type != "getWindowHandles" &&
                    mounted) {
                  Navigator.pop(this.context);
                }
                if (widget.type != "getText" &&
                    widget.type != "getSelectOptionsValue" &&
                    widget.type != "getSelectVisibleOptionsValue" &&
                    widget.type != "getAttribute" &&
                    widget.type != "getAlertText" &&
                    widget.type != "getPageSource" &&
                    widget.type != "getWindowHandle" &&
                    widget.type != "getWindowHandles") {
                  if (response.containsKey("body") && response["body"] is String == false && mounted) {
                    if (response["body"].containsKey("errorMessage")) {
                      showToast(context: this.context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
                    } else {
                      showToast(
                        context: this.context,
                        msg: multiLanguageString(name: "type_success", defaultValue: "\$0 success", variables: [widget.type], context: this.context),
                        color: Colors.greenAccent,
                        icon: const Icon(Icons.done),
                      );
                    }
                  }
                }
                if (mounted) {
                  Navigator.pop(this.context);
                  Provider.of<WebElementModel>(this.context, listen: false)
                      .setCurrentTimeMicrosecondsSinceEpoch(DateTime.now().microsecondsSinceEpoch);
                }
              }
            },
          ),
          if (widget.type != "getText" &&
              widget.type != "getSelectOptionsValue" &&
              widget.type != "getSelectVisibleOptionsValue" &&
              widget.type != "getAttribute" &&
              widget.type != "getAlertText" &&
              widget.type != "getPageSource" &&
              widget.type != "getWindowHandle" &&
              widget.type != "getWindowHandles")
            const ButtonCancel()
        ],
      );
    }
    return const Center(
      child: CircularProgressIndicator(),
    );
  }

  Column parameterListWidget() {
    List locatorList = parameterList.where((parameter) => parameter["name"] == "xpath" || parameter["name"].startsWith("parent")).toList();
    List inputList = parameterList.where((parameter) => parameter["name"] != "xpath" && !parameter["name"].startsWith("parent")).toList();
    return Column(
      children: [
        if (locatorList.isNotEmpty)
          Column(
            children: [
              parameterTypeWidget(multiLanguageString(name: "locator", defaultValue: "Locator", context: context), locatorList),
              // for (var parameter in locatorList) variableParameterWidget(parameter, false),
            ],
          ),
        if (inputList.isNotEmpty) parameterTypeWidget(multiLanguageString(name: "input", defaultValue: "Input", context: context), inputList),
      ],
    );
  }

  Column parameterTypeWidget(String text, List<dynamic> list) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(text, style: const TextStyle(fontWeight: FontWeight.bold, color: Color.fromARGB(255, 35, 67, 172))),
        Container(
            padding: const EdgeInsets.all(10),
            margin: const EdgeInsets.only(top: 5, bottom: 20),
            decoration: BoxDecoration(
              border: Border.all(color: const Color.fromRGBO(216, 218, 229, 1)),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              children: [
                for (var parameter in list) variableParameterWidget(parameter),
              ],
            ))
      ],
    );
  }

  Widget clientWidget() {
    return Row(
      children: [
        const Expanded(
            child: MultiLanguageText(
                name: "client",
                defaultValue: "Client",
                suffiText: " : ",
                style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
        Expanded(
          flex: 3,
          child: DynamicDropdownButton(
              value: clientName,
              hint: multiLanguageString(name: "choose_a_client", defaultValue: "Choose a client", context: context),
              items: Provider.of<TestCaseConfigModel>(context, listen: false)
                  .testCaseConfigMap[Provider.of<TestCaseConfigModel>(context, listen: false).testCaseIdList.last]["clientList"]
                  .map<DropdownMenuItem>((result) {
                return DropdownMenuItem(
                  value: result["name"],
                  child: Text(result["name"]),
                );
              }).toList(),
              onChanged: (newValue) async {
                _formKey.currentState!.validate();
                setState(() {
                  clientName = newValue;
                });
              },
              isRequiredNotEmpty: true),
        ),
      ],
    );
  }

  Widget variableParameterWidget(parameter) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        (parameter["name"] == "xpath")
            ? const MultiLanguageText(
                name: "element", defaultValue: "Element", style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromARGB(255, 35, 67, 172)))
            : Text(parameter["name"].replaceFirst("parent", "Parent "),
                style: const TextStyle(fontWeight: FontWeight.bold, color: Color.fromARGB(255, 35, 67, 172))),
        const SizedBox(
          height: 5,
        ),
        Row(
          children: [
            Expanded(
              child: Container(
                margin: parameter != parameterList.last ? const EdgeInsets.only(bottom: 10) : null,
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  border: Border.all(color: const Color.fromRGBO(216, 218, 229, 1)),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Column(
                  children: [
                    for (var variablePath in parameter["variablePath"])
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          (widget.type == "clearAndSendFile" && parameter["variablePath"][0]["dataType"] == "File")
                              ? DynamicVariablePathWidget(
                                  variablePath,
                                  parameter["variablePath"].indexOf(variablePath),
                                  [
                                    {"value": "value", "name": multiLanguageString(name: "value", defaultValue: "Value", context: context)},
                                  ],
                                  const [
                                    {"value": "File", "name": "File"},
                                    {"value": "String", "name": "String"},
                                    {"value": "Null", "name": "Null"},
                                    {"value": "EmptyString", "name": "Empty String"},
                                  ],
                                  keyForm: _formKey,
                                  callback: () {
                                    setState(() {});
                                  },
                                  clientName: clientName,
                                  platform: widget.platform,
                                )
                              : (parameter["name"] == "parentElement" || parameter["name"] == "xpath")
                                  ? DynamicVariablePathWidget(
                                      variablePath,
                                      parameter["variablePath"].indexOf(variablePath),
                                      [
                                        {"value": "value", "name": multiLanguageString(name: "value", defaultValue: "Value", context: context)},
                                      ],
                                      const [
                                        {"value": "WebElement", "name": "Elements"},
                                        {"value": "RepositoryElement", "name": "Repository Element"},
                                        {"value": "Password", "name": "Password"},
                                        {"value": "Null", "name": "Null"},
                                        {"value": "EmptyString", "name": "Empty String"},
                                      ],
                                      keyForm: _formKey,
                                      callback: () {
                                        setState(() {});
                                      },
                                      clientName: clientName,
                                      platform: widget.platform,
                                    )
                                  : DynamicVariablePathWidget(
                                      variablePath,
                                      parameter["variablePath"].indexOf(variablePath),
                                      [
                                        {"value": "value", "name": multiLanguageString(name: "value", defaultValue: "Value", context: context)},
                                      ],
                                      dataTypeList,
                                      keyForm: _formKey, callback: () {
                                      setState(() {});
                                    },
                                      clientName: clientName,
                                      platform: widget.platform,
                                      isRequiredNotEmpty: parameter["name"] != "belowOf" &&
                                          parameter["name"] != "middleOf" &&
                                          parameter["name"] != "aboveOf" &&
                                          parameter["name"] != "rightOf" &&
                                          parameter["name"] != "centerOf" &&
                                          parameter["name"] != "leftOf",
                                      isRequiredRegex: parameter["name"] != "belowOf" &&
                                          parameter["name"] != "middleOf" &&
                                          parameter["name"] != "aboveOf" &&
                                          parameter["name"] != "rightOf" &&
                                          parameter["name"] != "centerOf" &&
                                          parameter["name"] != "leftOf"),
                          if (variablePath != parameter["variablePath"].last)
                            const SizedBox(
                              height: 10,
                            ),
                        ],
                      ),
                    const SizedBox(
                      height: 10,
                    ),
                    if (parameter["variablePath"] == null ||
                        parameter["variablePath"].length == 0 ||
                        parameter["variablePath"][0]["type"] == "variable" ||
                        parameter["variablePath"][0]["type"] == "version_variable" ||
                        parameter["variablePath"][0]["type"] == "environment_variable" ||
                        parameter["variablePath"][0]["type"] == "node_variable")
                      FloatingActionButton(
                        heroTag: null,
                        child: const Icon(Icons.add),
                        onPressed: () {
                          setState(() {
                            if (parameter["variablePath"] != null && parameter["variablePath"].length > 0) {
                              parameter["variablePath"].add({"type": "get"});
                            } else {
                              parameter["variablePath"] = [];
                              parameter["variablePath"].add({});
                            }
                          });
                        },
                      ),
                  ],
                ),
              ),
            ),
          ],
        ),
        if (parameter != parameterList.last)
          const SizedBox(
            height: 10,
          )
      ],
    );
  }

  Widget chooseMouseActionTypeWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const MultiLanguageText(
            name: "mouse_action_type",
            defaultValue: "Mouse action type",
            style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromARGB(255, 35, 67, 172))),
        const SizedBox(
          height: 5,
        ),
        Container(
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
            border: Border.all(color: const Color.fromRGBO(216, 218, 229, 1)),
            borderRadius: BorderRadius.circular(10),
          ),
          child: Row(
            children: [
              const Expanded(
                  child: MultiLanguageText(
                      name: "mouse_action_type",
                      defaultValue: "Mouse action type",
                      suffiText: " : ",
                      style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
              Expanded(
                flex: 3,
                child: DynamicDropdownButton(
                    value: mouseActionType,
                    hint: multiLanguageString(name: "mouse_action_type", defaultValue: "Mouse action type", context: context),
                    items: mouseActionTypeValueList.map<DropdownMenuItem<String>>((var result) {
                      return DropdownMenuItem(
                        value: result["value"],
                        child: Text(result["name"]),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      _formKey.currentState!.validate();
                      setState(() {
                        mouseActionType = newValue;
                      });
                    },
                    isRequiredNotEmpty: true),
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 20,
        ),
      ],
    );
  }

  Widget chooseKeyWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 20,
        ),
        MultiLanguageText(
            name: keyType!.toLowerCase().replaceAll(' ', '_'),
            defaultValue: keyType!,
            style: const TextStyle(fontWeight: FontWeight.bold, color: Color.fromARGB(255, 35, 67, 172))),
        const SizedBox(
          height: 5,
        ),
        Container(
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
            border: Border.all(color: const Color.fromRGBO(216, 218, 229, 1)),
            borderRadius: BorderRadius.circular(10),
          ),
          child: Row(
            children: [
              Expanded(
                child: MultiLanguageText(
                    name: keyType!.toLowerCase().replaceAll(' ', '_'),
                    defaultValue: keyType!,
                    style: const TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1))),
              ),
              Expanded(
                flex: 3,
                child: DynamicDropdownButton(
                    value: key,
                    hint: multiLanguageString(name: "choose_a_key", defaultValue: "Choose a key", context: context),
                    items: keyValueList.map<DropdownMenuItem<String>>((String result) {
                      return DropdownMenuItem(
                        value: result,
                        child: Text(result),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      _formKey.currentState!.validate();
                      setState(() {
                        key = newValue;
                      });
                    },
                    isRequiredNotEmpty: keyType == "End Key" ? false : true),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
