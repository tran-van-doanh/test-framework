import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/config.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/page/editor/action_button_editor/dialog_analyze.dart';
import 'package:test_framework_view/page/editor/action_button_editor/dialog_go_to_url.dart';
import 'package:test_framework_view/page/editor/element_config/view_img_page_url.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/editor/action_button_editor/dialog_call_action_widget.dart';
import 'package:test_framework_view/page/editor/action_button_editor/dialog_action_element.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';
import 'package:test_framework_view/type.dart';

import '../../../common/custom_state.dart';

class ViewScreenShotWidget extends StatefulWidget {
  final String tfTestCaseId;
  const ViewScreenShotWidget({Key? key, required this.tfTestCaseId}) : super(key: key);

  @override
  State<ViewScreenShotWidget> createState() => _ViewScreenShotWidgetState();
}

class _ViewScreenShotWidgetState extends CustomState<ViewScreenShotWidget> {
  String direction = "up";
  TextEditingController length = TextEditingController();
  String? clientName;

  @override
  void initState() {
    length.text = "100";
    TestCaseConfigModel testCaseConfigModel = Provider.of<TestCaseConfigModel>(context, listen: false);
    if (testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"] != null &&
        testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"].isNotEmpty) {
      clientName = testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"][0]["name"];
    }
    super.initState();
  }

  @override
  void dispose() {
    length.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<WebElementModel, LanguageModel>(
      builder: (context, webElementModel, languageModel, child) {
        return TestFrameworkRootPageWidget(
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: DynamicDropdownButton(
                      value: clientName,
                      hint: multiLanguageString(name: "choose_a_client", defaultValue: "Choose a client", context: context),
                      labelText: multiLanguageString(name: "client", defaultValue: "Client", context: context),
                      items: Provider.of<TestCaseConfigModel>(context, listen: false)
                          .testCaseConfigMap[Provider.of<TestCaseConfigModel>(context, listen: false).testCaseIdList.last]["clientList"]
                          .map<DropdownMenuItem>((result) {
                        return DropdownMenuItem(
                          value: result["name"],
                          child: Text(result["name"]),
                        );
                      }).toList(),
                      onChanged: (newValue) async {
                        setState(() {
                          clientName = newValue;
                        });
                      },
                      isRequiredNotEmpty: true),
                ),
                Expanded(
                  child: Row(
                    children: [
                      ListOptionDebugWidget(
                        testCaseId: widget.tfTestCaseId,
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: ViewImgPageUrl(
                            height: double.infinity,
                            src:
                                "$baseUrl/test-framework-api/user/node-file/screenshot/$clientName?time=${webElementModel.currentTimeMicrosecondsSinceEpoch}"),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    if (Provider.of<NavigationModel>(context, listen: false).prjProject != null &&
                        Provider.of<NavigationModel>(context, listen: false).prjProject["prjProjectType"] != null &&
                        (Provider.of<NavigationModel>(context, listen: false).prjProject["prjProjectType"]["platform"] == "iOS" ||
                            Provider.of<NavigationModel>(context, listen: false).prjProject["prjProjectType"]["platform"] == "Android"))
                      Container(
                        margin: const EdgeInsets.only(right: 15),
                        height: 40,
                        child: Row(
                          children: [
                            SizedBox(
                              width: 100,
                              child: DynamicDropdownButton(
                                value: direction,
                                hint: multiLanguageString(name: "direction", defaultValue: "direction", context: context),
                                items: ["up", "down", "left", "right"].map<DropdownMenuItem<String>>((String result) {
                                  return DropdownMenuItem(
                                    value: result,
                                    child: Text(result),
                                  );
                                }).toList(),
                                onChanged: (newValue) {
                                  setState(() {
                                    direction = newValue;
                                  });
                                },
                                isRequiredNotEmpty: true,
                              ),
                            ),
                            SizedBox(
                              width: 70,
                              child: TextField(
                                controller: length,
                                keyboardType: TextInputType.number,
                                inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
                                decoration: InputDecoration(
                                  contentPadding: const EdgeInsets.symmetric(vertical: 5, horizontal: 12),
                                  hintText: multiLanguageString(name: "enter_a_length", defaultValue: "Enter a length", context: context),
                                  counterText: "",
                                  border: const OutlineInputBorder(),
                                ),
                                maxLength: 5,
                              ),
                            ),
                            ElevatedButton(
                              onPressed: () async {
                                var findRequest = {
                                  "type": "swipe",
                                  "data": {
                                    "direction": direction,
                                    "length": length.text,
                                  }
                                };
                                await httpPost("/test-framework-api/user/node/message", findRequest, context);
                                setState(() {
                                  webElementModel.setCurrentTimeMicrosecondsSinceEpoch(DateTime.now().microsecondsSinceEpoch);
                                });
                              },
                              child: const MultiLanguageText(
                                name: "swipe",
                                defaultValue: "Swipe",
                              ),
                            ),
                          ],
                        ),
                      ),
                    Container(
                      margin: const EdgeInsets.only(right: 15),
                      height: 40,
                      child: ElevatedButton(
                        onPressed: () {
                          setState(() {
                            webElementModel.setCurrentTimeMicrosecondsSinceEpoch(DateTime.now().microsecondsSinceEpoch);
                          });
                        },
                        child: const MultiLanguageText(
                          name: "refresh",
                          defaultValue: "Refresh",
                          isLowerCase: false,
                        ),
                      ),
                    ),
                    const ButtonCancel(),
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }
}

class OptionDebug {
  final Function function;
  final String title;
  const OptionDebug({required this.title, required this.function});
}

class ListOptionDebugWidget extends StatefulWidget {
  final String? testCaseId;
  const ListOptionDebugWidget({Key? key, this.testCaseId}) : super(key: key);

  @override
  State<ListOptionDebugWidget> createState() => _ListOptionDebugWidgetState();
}

class _ListOptionDebugWidgetState extends CustomState<ListOptionDebugWidget> {
  List<Template> platformTemplateList = [];
  late Future futureListTemplate;
  TextEditingController filterText = TextEditingController();
  @override
  void initState() {
    super.initState();
    futureListTemplate = initPage();
  }

  initPage() async {
    await getListTemplate();
    return 0;
  }

  getListTemplate() async {
    var securityModel = Provider.of<SecurityModel>(context, listen: false);
    var findRequest = {"status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-editor-component/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      List<Template> platformTemplateList = [
        Template(
          category: "",
          type: "smartLocator",
          description: "",
          title: 'Smart Locator',
          platformsList: [],
        ),
        Template(
          category: "action",
          type: "getPageSource",
          description: "",
          title: 'Get Page Source',
          platformsList: [],
        )
      ];
      for (var elementResponse in response["body"]["resultList"]) {
        List<String> elementPlatformsList = [];
        for (var platform in elementResponse["platformsList"] ?? []) {
          if (securityModel.hasSysOrganizationPlatforms(platform)) {
            elementPlatformsList.add(platform);
          }
        }
        if (elementPlatformsList.isNotEmpty &&
            elementResponse["componentConfig"] != null &&
            elementResponse["componentConfig"].isNotEmpty &&
            (elementResponse["componentConfig"]["isDebug"] ?? false)) {
          Template template = Template(
            category: elementResponse["category"],
            type: elementResponse["type"],
            description: elementResponse["description"] ?? "",
            title: elementResponse["title"],
            platformsList: elementPlatformsList,
          );
          platformTemplateList.add(template);
        }
      }
      setState(() {
        this.platformTemplateList = platformTemplateList;
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
    filterText.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 300,
      child: Column(
        children: [
          SizedBox(
            height: 40,
            child: DynamicTextField(
              controller: filterText,
              onChanged: (value) => setState(() {
                filterText.text;
              }),
              hintText: multiLanguageString(name: "search_a_keyword", defaultValue: "Search a keyword", context: context),
              suffixIcon: (filterText.text.isNotEmpty)
                  ? GestureDetector(
                      onTap: () => setState(() {
                            filterText.clear();
                          }),
                      child: const Icon(Icons.close))
                  : null,
              prefixIcon: const Icon(Icons.search),
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  for (var platformTemplate in platformTemplateList)
                    if (filterText.text.isEmpty || platformTemplate.title.toLowerCase().contains(filterText.text.toLowerCase()))
                      optionDebugWidget(
                        onTap: () {
                          if (platformTemplate.type == "smartLocator") {
                            smartLocator();
                          } else if (platformTemplate.type == "actionCall") {
                            dialogCallAction();
                          } else if (platformTemplate.type == "goTo") {
                            dialogGoToUrl();
                          } else {
                            dialogActionElement(platformTemplate.type);
                          }
                        },
                        title: platformTemplate.title,
                      ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  optionDebugWidget({Function? onTap, required String title}) {
    return GestureDetector(
      onTap: onTap != null
          ? () {
              onTap();
            }
          : null,
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 5),
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        decoration: BoxDecoration(
          color: const Color.fromRGBO(76, 151, 255, 1),
          borderRadius: BorderRadius.circular(20),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Icon(Icons.call_end, color: Color.fromARGB(255, 248, 245, 245)),
            const SizedBox(
              width: 10,
            ),
            Expanded(
              child: Text(
                title,
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText14,
                    fontWeight: FontWeight.bold,
                    color: const Color.fromARGB(255, 248, 245, 245),
                    letterSpacing: 2),
              ),
            )
          ],
        ),
      ),
    );
  }

  dialogActionElement(String type) {
    TestCaseConfigModel testCaseConfigModel = Provider.of<TestCaseConfigModel>(context, listen: false);
    String? clientName;
    if (testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"] != null &&
        testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"].isNotEmpty) {
      clientName = testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"][0]["name"];
    }
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogActionElement(
          tfTestCaseId: widget.testCaseId,
          type: type,
          clientName: clientName,
        );
      },
    );
  }

  smartLocator() {
    Navigator.of(context).push(
      createRoute(
        DialogAnalyze(
          testCaseId: widget.testCaseId,
          onClickContinue: (value, client, requestBody, context) async {
            onLoading(context);
            await onClickAnalyze(value, client, requestBody, context);
            Provider.of<WebElementModel>(context, listen: false).setCurrentTimeMicrosecondsSinceEpoch(DateTime.now().microsecondsSinceEpoch);
          },
          title: multiLanguageString(name: "smart_locator", defaultValue: "Smart Locator", context: context),
        ),
      ),
    );
  }

  dialogCallAction() {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return const DialogCallActionWidget(
          actionName: "",
          typeWidget: "actionCall",
          typeCallAction: "action_editor",
        );
      },
    );
  }

  dialogGoToUrl() {
    TestCaseConfigModel testCaseConfigModel = Provider.of<TestCaseConfigModel>(context, listen: false);
    String? clientName;
    if (testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"] != null &&
        testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"].isNotEmpty) {
      clientName = testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"][0]["name"];
    }
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogGoToUrl(
          tfTestCaseId: widget.testCaseId,
          clientName: clientName,
        );
      },
    );
  }

  onClickAnalyze(selectedElementRepositoryId, client, requestBody, context) async {
    await httpPost(
        "/test-framework-api/user/node/analyze/$selectedElementRepositoryId/${client["platform"]}/${client["name"]}", requestBody, context);
    Navigator.of(context).pop();
    showToast(
      context: context,
      msg: multiLanguageString(name: "smart_locator_success", defaultValue: "Smart Locator success", context: context),
      color: Colors.greenAccent,
      icon: const Icon(Icons.done),
    );
    Navigator.pop(context);
  }
}
