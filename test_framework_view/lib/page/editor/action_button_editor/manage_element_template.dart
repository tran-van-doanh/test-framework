import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';
import '../../../api.dart';
import '../../../common/dynamic_table.dart';
import '../../../components/badge/badge_icon.dart';
import '../../../components/style.dart';
import '../../../model/model.dart';

class ManageElementTemplateWidget extends StatefulWidget {
  final String prjProjectTypeId;
  final List elementTemplateList;
  final Function(List elementTemplateList) onSave;
  const ManageElementTemplateWidget({Key? key, required this.onSave, required this.prjProjectTypeId, required this.elementTemplateList})
      : super(key: key);

  @override
  State<ManageElementTemplateWidget> createState() => _ManageElementTemplateWidgetState();
}

class _ManageElementTemplateWidgetState extends State<ManageElementTemplateWidget> {
  final TextEditingController nameController = TextEditingController();
  final TextEditingController titleController = TextEditingController();
  late Future futureListElementTemplate;
  List resultListElementTemplate = [];
  var rowCountListElementTemplate = 0;
  var currentPage = 1;
  var rowPerPage = 10;
  bool isShowFilterForm = true;
  List elementTemplateList = [];
  @override
  void initState() {
    super.initState();
    elementTemplateList = widget.elementTemplateList;
    handleSearchElementTemplate(currentPage);
  }

  handleSearchElementTemplate(page) async {
    var findRequest = {"queryOffset": (page - 1) * rowPerPage, "queryLimit": rowPerPage, "status": 1, "prjProjectTypeId": widget.prjProjectTypeId};
    findRequest["nameLike"] = nameController.text.isNotEmpty ? "%${nameController.text}%" : null;
    findRequest["titleLike"] = titleController.text.isNotEmpty ? "%${titleController.text}%" : null;
    var response = await httpPost("/test-framework-api/user/test-framework/tf-find-element-template/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPage = page;
        rowCountListElementTemplate = response["body"]["rowCount"];
        resultListElementTemplate = response["body"]["resultList"];
      });
    }
    return 0;
  }

  @override
  void dispose() {
    super.dispose();
    nameController.dispose();
    titleController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return TestFrameworkRootPageWidget(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(50, 30, 50, 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      headerWidget(context),
                      const SizedBox(
                        height: 20,
                      ),
                      filterFormWidget(),
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(child: tableElementTemplateWidget("List Element Template Search", resultListElementTemplate, elementTemplateList)),
                          const SizedBox(
                            width: 10,
                          ),
                          Expanded(child: tableElementTemplateWidget("List Element Template", elementTemplateList, resultListElementTemplate))
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
            btnWidget(context),
          ],
        ),
      );
    });
  }

  Column tableElementTemplateWidget(String label, List list, List list1) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: Style(context).styleTitleDialog,
        ),
        CustomDataTableWidget(
          minWidth: 500,
          columns: [
            Expanded(
              child: MultiLanguageText(
                name: "name",
                defaultValue: "Name",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: Center(
                child: MultiLanguageText(
                  name: "status",
                  defaultValue: "Status",
                  isLowerCase: false,
                  style: Style(context).styleTextDataColumn,
                ),
              ),
            ),
            Container(
              width: 56,
            ),
          ],
          rows: [
            for (var item in list)
              CustomDataRow(
                cells: [
                  Expanded(
                    child: Text(
                      item["title"],
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                  Expanded(
                    child: Center(
                      child: Badge2StatusSettingWidget(
                        status: item["status"],
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 56,
                    height: 35,
                    child: (label == "List Element Template Search" &&
                            !list1.any(
                              (element) => element["id"] == item["id"],
                            ))
                        ? ElevatedButton(
                            onPressed: () {
                              setState(() {
                                list1.add(item);
                              });
                            },
                            child: const Icon(Icons.add),
                          )
                        : (label == "List Element Template")
                            ? ElevatedButton(
                                onPressed: () {
                                  setState(() {
                                    list.remove(item);
                                  });
                                },
                                child: const Icon(Icons.remove),
                              )
                            : const SizedBox.shrink(),
                  ),
                ],
              ),
          ],
        ),
        if (label == "List Element Template Search")
          DynamicTablePagging(
            rowCountListElementTemplate,
            currentPage,
            rowPerPage,
            pageChangeHandler: (page) {
              handleSearchElementTemplate(page);
            },
            rowPerPageChangeHandler: (rowPerPage) {
              setState(() {
                rowPerPage = rowPerPage!;
                handleSearchElementTemplate(1);
              });
            },
          ),
      ],
    );
  }

  Column filterFormWidget() {
    return Column(
      children: [
        Row(
          children: [
            MultiLanguageText(
              name: "filter_form",
              defaultValue: "Filter form",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: const Color.fromRGBO(0, 0, 0, 0.8),
                  fontSize: context.fontSizeManager.fontSizeText16,
                  letterSpacing: 2),
            ),
            IconButton(
              onPressed: () {
                setState(() {
                  isShowFilterForm = !isShowFilterForm;
                });
              },
              icon: isShowFilterForm ? const Icon(Icons.visibility) : const Icon(Icons.visibility_off),
              splashRadius: 1,
              tooltip: isShowFilterForm
                  ? multiLanguageString(name: "hide_filter_form", defaultValue: "Hide filter form", context: context)
                  : multiLanguageString(name: "show_filter_form", defaultValue: "Show filter form", context: context),
            )
          ],
        ),
        if (isShowFilterForm)
          Row(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: SizedBox(
                    height: 50,
                    child: TextField(
                      controller: nameController,
                      decoration: InputDecoration(
                        label: const MultiLanguageText(name: "code", defaultValue: "Code"),
                        contentPadding: const EdgeInsets.fromLTRB(10, 0, 10, 20),
                        border: const OutlineInputBorder(),
                        hintText: multiLanguageString(name: "by_code", defaultValue: "By code", context: context),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                  child: SizedBox(
                    height: 50,
                    child: TextField(
                      controller: titleController,
                      decoration: InputDecoration(
                        label: const MultiLanguageText(name: "name", defaultValue: "Name"),
                        contentPadding: const EdgeInsets.fromLTRB(10, 0, 10, 20),
                        border: const OutlineInputBorder(),
                        hintText: multiLanguageString(name: "by_name", defaultValue: "By name", context: context),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        if (isShowFilterForm)
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: ElevatedButton(
                    onPressed: () {
                      handleSearchElementTemplate(currentPage);
                    },
                    child: const MultiLanguageText(name: "search", defaultValue: "Search", isLowerCase: false)),
              ),
              ElevatedButton(
                  onPressed: () {
                    setState(() {
                      nameController.clear();
                      titleController.clear();
                      handleSearchElementTemplate(currentPage);
                    });
                  },
                  child: const MultiLanguageText(name: "reset", defaultValue: "Reset", isLowerCase: false))
            ],
          ),
      ],
    );
  }

  Row headerWidget(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        MultiLanguageText(
          name: "element_template",
          defaultValue: "ElementTemplate",
          style: Style(context).styleTitleDialog,
        ),
        TextButton(
          onPressed: () => Navigator.pop(context, 'cancel'),
          child: const Icon(
            Icons.close,
            color: Colors.black,
          ),
        ),
      ],
    );
  }

  Padding btnWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: 40,
            width: 80,
            child: ElevatedButton(
              onPressed: () {
                widget.onSave(elementTemplateList);
                Navigator.pop(context);
              },
              child: const MultiLanguageText(
                name: "ok",
                defaultValue: "Ok",
                isLowerCase: false,
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 10),
            height: 40,
            width: 120,
            child: ElevatedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              style: ElevatedButton.styleFrom(
                side: const BorderSide(color: Color.fromRGBO(209, 212, 216, 1)),
                backgroundColor: Colors.white,
                foregroundColor: const Color.fromRGBO(61, 90, 254, 1),
              ),
              child: const MultiLanguageText(
                name: "cancel",
                defaultValue: "Cancel",
                isLowerCase: false,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          )
        ],
      ),
    );
  }
}

class ElementTemplateWidget extends StatelessWidget {
  final dynamic elementTemplate;
  final VoidCallback onRemove;
  const ElementTemplateWidget({Key? key, required this.elementTemplate, required this.onRemove}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
      decoration: BoxDecoration(
        color: Colors.grey.withOpacity(0.5),
        // color: Colors.grey.withOpacity(0.3),
        border: const Border(
          left: BorderSide(color: Colors.grey, width: 4),
          // left: BorderSide(color: Colors.grey, width: 4),
        ),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Text(
              elementTemplate["title"],
              style: Style(context).styleTextDataCell,
            ),
          ),
          Row(
            children: [
              GestureDetector(
                onTap: () => onRemove(),
                child: const Icon(Icons.close),
              ),
            ],
          )
        ],
      ),
    );
  }
}
