import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/editor/property_widget/dynamic_variabla_path_widget.dart';
import 'package:test_framework_view/type.dart';

import '../../../common/custom_state.dart';
import '../../../common/dynamic_dropdown_button.dart';

class DialogCallActionWidget extends StatefulWidget {
  final String? actionName;
  final List? clientList;
  final dynamic parameterList;
  final String typeCallAction;
  final String typeWidget;
  const DialogCallActionWidget(
      {Key? key, this.actionName, this.parameterList, required this.typeCallAction, this.clientList, required this.typeWidget})
      : super(key: key);

  @override
  State<DialogCallActionWidget> createState() => _DialogCallActionWidgetState();
}

class _DialogCallActionWidgetState extends CustomState<DialogCallActionWidget> {
  late Future futureListNameByActionCall;
  var resultListByAction = [];
  List clientList = [];
  String actionName = "";
  dynamic actionId;
  var parameterList = [];
  String prjProjectId = "";

  @override
  void initState() {
    prjProjectId = Provider.of<NavigationModel>(context, listen: false).prjProjectId!;
    futureListNameByActionCall = setInitValue();
    super.initState();
  }

  setInitValue() async {
    if (widget.actionName != null && widget.actionName!.isNotEmpty) {
      await getTestCase();
    }
    if (widget.clientList != null && widget.clientList!.isNotEmpty) {
      clientList = widget.clientList!;
    }
    await getActionName();
    return 0;
  }

  getTestCase() async {
    var response = await httpPost(
        "/test-framework-api/user/test-framework/tf-test-case/search", {"name": widget.actionName, "prjProjectId": prjProjectId}, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        actionName = response["body"]["resultList"][0]["name"];
        actionId = response["body"]["resultList"][0]["id"];
        if (widget.typeCallAction == "play_widget") {
          parameterList = widget.parameterList;
        } else {
          if (response["body"]["resultList"][0]["content"] != null) {
            parameterList = response["body"]["resultList"][0]["content"]["parameterFieldList"];
            for (var parameter in parameterList) {
              parameter["variablePath"] = [
                {"type": "value", "dataType": "String"}
              ];
            }
          } else {
            parameterList = [];
          }
        }
      });
    }
  }

  getActionName() async {
    var findRequest = {"testCaseType": "action", "prjProjectId": prjProjectId};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-case/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        resultListByAction = response["body"]["resultList"];
      });
    }
  }

  getParameterList(id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-case/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        actionName = response["body"]["result"]["name"];
        actionId = response["body"]["result"]["id"];
        if (response["body"]["result"]["content"] != null) {
          parameterList = response["body"]["result"]["content"]["parameterFieldList"] ?? [];
          clientList = response["body"]["result"]["content"]["clientList"] ?? [];
          TestCaseConfigModel testCaseConfigModel = Provider.of<TestCaseConfigModel>(context, listen: false);
          if (testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"] != null &&
              testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"].isNotEmpty) {
            for (var client in clientList) {
              client["clientName"] = testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"][0]["name"];
            }
          }
          for (var parameter in parameterList) {
            parameter["variablePath"] = [
              {"type": "value", "dataType": "String"}
            ];
          }
        } else {
          parameterList = [];
        }
      });
    }
  }

  onClickCallAction() async {
    DebugStatusModel debugStatusModel = Provider.of<DebugStatusModel>(context, listen: false);
    var findRequest = {
      "type": "callAction",
      "tfTestCaseId": actionId,
      "tfTestVersionId": (debugStatusModel.versionStartDebug != null) ? debugStatusModel.versionStartDebug["id"] : null,
      "tfTestEnvironmentId": (debugStatusModel.environmentStartDebug != null) ? debugStatusModel.environmentStartDebug["id"] : null,
      "clientList": clientList,
      "data": {
        for (var parameter in parameterList) parameter['name']: parameter["variablePath"][0]["value"],
      }
    };
    await httpPost("/test-framework-api/user/node/message", findRequest, context);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: futureListNameByActionCall,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return AlertDialog(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  (widget.typeWidget == "actionCall")
                      ? MultiLanguageText(
                          name: "call_action",
                          defaultValue: "Call Action",
                          style: Style(context).styleTitleDialog,
                        )
                      : (widget.typeWidget == "testCaseCall")
                          ? MultiLanguageText(name: "call_test_scenario", defaultValue: "Call Test Scenario", style: Style(context).styleTitleDialog)
                          : MultiLanguageText(name: "call_api", defaultValue: "Call Api", style: Style(context).styleTitleDialog),
                  TextButton(
                    onPressed: () => Navigator.pop(context, 'Cancel'),
                    child: const Icon(
                      Icons.close,
                      color: Colors.black,
                    ),
                  )
                ],
              ),
              content: Container(
                decoration: const BoxDecoration(
                  border: Border(
                    top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
                    bottom: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
                  ),
                ),
                padding: const EdgeInsets.all(10),
                constraints: const BoxConstraints(minWidth: 600),
                child: SingleChildScrollView(
                  child: IntrinsicHeight(
                    child: Column(
                      children: [
                        if (widget.typeCallAction != "play_widget") actionNameWidget(),
                        if (clientList.isNotEmpty) const SizedBox(height: 20),
                        if (clientList.isNotEmpty) clientListWidget(),
                        (parameterList.isNotEmpty)
                            ? variableParameterWidget()
                            : (actionId != null)
                                ? const Padding(
                                    padding: EdgeInsets.symmetric(vertical: 10),
                                    child: MultiLanguageText(name: "parameter_is_empty", defaultValue: "Parameter is empty !!!"),
                                  )
                                : const SizedBox.shrink(),
                      ],
                    ),
                  ),
                ),
              ),
              actionsAlignment: MainAxisAlignment.center,
              actions: [
                Container(
                  margin: const EdgeInsets.only(right: 5),
                  height: 40,
                  width: 120,
                  child: ElevatedButton(
                    onPressed: () async {
                      onLoading(context);
                      await onClickCallAction();
                      if (mounted) {
                        Navigator.pop(this.context);
                        Navigator.pop(this.context);
                        showToast(
                          context: this.context,
                          msg: multiLanguageString(
                              name: "call_action_action_name_success",
                              defaultValue: "Call action : \$0 success",
                              variables: [actionName],
                              context: this.context),
                          color: Colors.greenAccent,
                          icon: const Icon(Icons.done),
                        );
                        Provider.of<WebElementModel>(this.context, listen: false)
                            .setCurrentTimeMicrosecondsSinceEpoch(DateTime.now().microsecondsSinceEpoch);
                      }
                    },
                    child: const MultiLanguageText(
                      name: "ok",
                      defaultValue: "Ok",
                      isLowerCase: false,
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
                const ButtonCancel()
              ],
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        });
  }

  Widget actionNameWidget() {
    return Row(
      children: [
        const Expanded(
            child: MultiLanguageText(
                name: "code",
                defaultValue: "Code",
                suffiText: " : ",
                style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
        Expanded(
          flex: 3,
          child: Container(
            decoration: BoxDecoration(border: Border.all(color: const Color.fromRGBO(216, 218, 229, 1))),
            child: DynamicDropdownButton(
              value: actionId,
              onChanged: (dynamic newValue) {
                getParameterList(newValue);
              },
              items: resultListByAction.map<DropdownMenuItem<String>>((dynamic result) {
                return DropdownMenuItem(
                  value: result["id"],
                  child: Text(
                    result["name"],
                  ),
                );
              }).toList(),
              hint: multiLanguageString(name: "choose_an_action", defaultValue: "Choose an action", context: context),
            ),
          ),
        ),
      ],
    );
  }

  Widget variableParameterWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 10,
        ),
        const MultiLanguageText(
            name: "input", defaultValue: "Input", style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromARGB(255, 35, 67, 172))),
        Container(
          padding: const EdgeInsets.all(10),
          margin: const EdgeInsets.only(top: 5),
          decoration: BoxDecoration(
            border: Border.all(color: const Color.fromRGBO(216, 218, 229, 1)),
            borderRadius: BorderRadius.circular(10),
          ),
          child: Column(children: [
            for (var parameter in parameterList)
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(parameter["name"], style: const TextStyle(fontWeight: FontWeight.bold, color: Color.fromARGB(255, 35, 67, 172))),
                  const SizedBox(
                    height: 5,
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: Container(
                          margin: parameter != parameterList.last ? const EdgeInsets.only(bottom: 10) : null,
                          padding: const EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            border: Border.all(color: const Color.fromRGBO(216, 218, 229, 1)),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: DynamicVariablePathWidget(
                            parameter["variablePath"][0],
                            0,
                            [
                              {"value": "value", "name": multiLanguageString(name: "value", defaultValue: "Value", context: context)},
                            ],
                            dataTypeList,
                            callback: () {
                              setState(() {});
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              )
          ]),
        ),
      ],
    );
  }

  Widget clientListWidget() {
    TestCaseConfigModel testCaseConfigModel = Provider.of<TestCaseConfigModel>(context, listen: false);
    return Column(
      children: [
        for (var client in clientList)
          Padding(
            padding: EdgeInsets.only(bottom: clientList.last != client ? 20 : 0),
            child: DynamicDropdownButton(
                value: client["clientName"],
                hint: multiLanguageString(
                    name: "choose_a_client_for", defaultValue: "Choose a client for \$0", variables: ["${client["name"]}"], context: context),
                items: testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"].map<DropdownMenuItem>((result) {
                  return DropdownMenuItem(
                    value: result["name"],
                    child: Text(result["name"]),
                  );
                }).toList(),
                labelText: client["name"],
                onChanged: (newValue) async {
                  client["clientName"] = newValue;
                },
                isRequiredNotEmpty: true),
          ),
      ],
    );
  }
}
