import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';

import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/model/model.dart';

import '../../../common/custom_state.dart';
import '../../../common/dynamic_dropdown_button.dart';

class DialogGoToUrl extends StatefulWidget {
  final String? tfTestCaseId;
  final String? textController;
  final String? clientName;
  const DialogGoToUrl({Key? key, this.tfTestCaseId, this.textController, this.clientName}) : super(key: key);

  @override
  State<DialogGoToUrl> createState() => _DialogGoToUrlState();
}

class _DialogGoToUrlState extends CustomState<DialogGoToUrl> {
  final TextEditingController _goToUrlController = TextEditingController();
  final _formGoToUrlKey = GlobalKey<FormState>();
  String? clientName;

  handleGoToUrl() async {
    var findRequestGoToUrl = {
      "type": "goToUrl",
      "tfTestCaseId": widget.tfTestCaseId,
      "data": {
        "clientName": clientName,
        "url": _goToUrlController.text,
      }
    };
    await httpPost("/test-framework-api/user/node/message", findRequestGoToUrl, context);
  }

  @override
  void initState() {
    if (widget.textController != null && widget.textController!.isNotEmpty) {
      setState(() {
        _goToUrlController.text = widget.textController!;
      });
    }
    if (widget.clientName != null && widget.clientName!.isNotEmpty) {
      setState(() {
        clientName = widget.clientName!;
      });
    }
    super.initState();
  }

  @override
  void dispose() {
    _goToUrlController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "go_to_url",
            defaultValue: "Go to Url",
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
            onPressed: () => Navigator.pop(context, 'Cancel'),
            child: const Icon(
              Icons.close,
              color: Colors.black,
            ),
          )
        ],
      ),
      content: Container(
        decoration: const BoxDecoration(
          border: Border(
            top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
            bottom: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
          ),
        ),
        padding: const EdgeInsets.all(10),
        constraints: const BoxConstraints(minWidth: 600),
        child: IntrinsicHeight(
          child: Form(
            key: _formGoToUrlKey,
            child: Column(
              children: [
                Row(
                  children: [
                    const Expanded(
                        child: MultiLanguageText(
                            name: "client",
                            defaultValue: "Client",
                            suffiText: " : ",
                            style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
                    Expanded(
                      flex: 3,
                      child: DynamicDropdownButton(
                          value: clientName,
                          hint: multiLanguageString(name: "choose_a_client", defaultValue: "Choose a client", context: context),
                          items: Provider.of<TestCaseConfigModel>(context, listen: false)
                              .testCaseConfigMap[Provider.of<TestCaseConfigModel>(context, listen: false).testCaseIdList.last]["clientList"]
                              .map<DropdownMenuItem>((result) {
                            return DropdownMenuItem(
                              value: result["name"],
                              child: Text(result["name"]),
                            );
                          }).toList(),
                          onChanged: (newValue) async {
                            _formGoToUrlKey.currentState!.validate();
                            clientName = newValue;
                          },
                          isRequiredNotEmpty: true),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                DynamicTextField(
                  controller: _goToUrlController,
                  hintText: multiLanguageString(name: "enter_an_url", defaultValue: "Enter an url (https://...)", context: context),
                  onChanged: (text) {
                    _formGoToUrlKey.currentState!.validate();
                  },
                  isRequiredNotEmpty: true,
                ),
              ],
            ),
          ),
        ),
      ),
      actionsAlignment: MainAxisAlignment.center,
      actions: [
        ButtonSave(
          onPressed: () async {
            if (_formGoToUrlKey.currentState!.validate()) {
              onLoading(context);
              await handleGoToUrl();
              if (mounted) {
                Navigator.pop(this.context);
                Navigator.pop(this.context);
                showToast(
                  context: this.context,
                  msg: multiLanguageString(
                      name: "go_to_url_success",
                      defaultValue: "Go to Url : \$0 success",
                      variables: [_goToUrlController.text],
                      context: this.context),
                  color: Colors.greenAccent,
                  icon: const Icon(Icons.done),
                );
                Provider.of<WebElementModel>(this.context, listen: false).setCurrentTimeMicrosecondsSinceEpoch(DateTime.now().microsecondsSinceEpoch);
              }
            }
          },
        ),
        const ButtonCancel()
      ],
    );
  }
}
