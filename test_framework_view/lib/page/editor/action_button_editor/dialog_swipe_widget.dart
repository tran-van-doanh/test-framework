import 'package:flutter/material.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';

import '../../../common/custom_state.dart';

class DialogSwipeWidget extends StatefulWidget {
  final VoidCallback callback;
  const DialogSwipeWidget({Key? key, required this.callback}) : super(key: key);

  @override
  State<DialogSwipeWidget> createState() => _DialogSwipeWidgetState();
}

class _DialogSwipeWidgetState extends CustomState<DialogSwipeWidget> {
  String? direction;
  TextEditingController length = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  @override
  void initState() {
    length.text = "100";
    super.initState();
  }

  @override
  void dispose() {
    length.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "swipe",
            defaultValue: "Swipe",
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
            onPressed: () => Navigator.pop(context, 'Cancel'),
            child: const Icon(
              Icons.close,
              color: Colors.black,
            ),
          )
        ],
      ),
      content: Container(
        decoration: const BoxDecoration(
          border: Border(
            top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
            bottom: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
          ),
        ),
        constraints: const BoxConstraints(minWidth: 600),
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      const Expanded(
                        child: MultiLanguageText(
                          name: "direction",
                          defaultValue: "Direction",
                          suffiText: " : ",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color.fromRGBO(87, 94, 117, 1),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: DynamicDropdownButton(
                            value: direction,
                            hint: multiLanguageString(name: "choose_a_direction", defaultValue: "Choose a direction", context: context),
                            items: ["up", "down", "left", "right"].map<DropdownMenuItem<String>>((String result) {
                              return DropdownMenuItem(
                                value: result,
                                child: Text(result),
                              );
                            }).toList(),
                            onChanged: (newValue) {
                              _formKey.currentState!.validate();
                              setState(() {
                                direction = newValue;
                              });
                            },
                            isRequiredNotEmpty: true),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      const Expanded(
                        child: MultiLanguageText(
                          name: "length",
                          defaultValue: "Length",
                          suffiText: " : ",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color.fromRGBO(87, 94, 117, 1),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: DynamicTextField(
                          controller: length,
                          keyboardType: TextInputType.number,
                          hintText: multiLanguageString(name: "enter_a_length", defaultValue: "Enter a length", context: context),
                          onChanged: (text) {
                            _formKey.currentState!.validate();
                          },
                          isRequiredNotEmpty: true,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      actionsAlignment: MainAxisAlignment.center,
      actions: [
        const ButtonCancel(),
        ButtonSave(
          onPressed: () async {
            if (_formKey.currentState!.validate()) {
              var findRequest = {
                "type": "swipe",
                "data": {
                  "direction": direction,
                  "length": length.text,
                }
              };
              await httpPost("/test-framework-api/user/node/message", findRequest, context);
              if (mounted) Navigator.pop(this.context);
              widget.callback();
            }
          },
        ),
      ],
    );
  }
}
