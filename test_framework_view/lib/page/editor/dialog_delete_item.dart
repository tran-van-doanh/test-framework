import 'package:flutter/material.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/type.dart';

class DialogDeleteItemWidget extends StatefulWidget {
  final Item selectedItem;
  final Function callback;
  const DialogDeleteItemWidget({Key? key, required this.selectedItem, required this.callback}) : super(key: key);

  @override
  State<DialogDeleteItemWidget> createState() => _DialogDeleteItemWidgetState();
}

class _DialogDeleteItemWidgetState extends State<DialogDeleteItemWidget> {
  dynamic selectedItem;
  bool isDone = false;
  @override
  void initState() {
    getSelectedItem();

    super.initState();
  }

  getSelectedItem() async {
    var searchRequest = {
      "type": widget.selectedItem.type,
      "category": widget.selectedItem.category,
    };
    var response = await httpPost("/test-framework-api/user/test-framework/tf-editor-component/search", searchRequest, context);
    if (response.containsKey("body") &&
        response["body"] is String == false &&
        response["body"]["resultList"] != null &&
        response["body"]["resultList"].isNotEmpty) {
      setState(() {
        isDone = true;
        selectedItem = response["body"]["resultList"][0];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return isDone
        ? AlertDialog(
            contentPadding: const EdgeInsets.all(0),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                MultiLanguageText(
                  name: "delete",
                  defaultValue: "Delete",
                  isLowerCase: false,
                  style: Style(context).styleTitleDialog,
                ),
                TextButton(
                    onPressed: () => Navigator.pop(context, 'Cancel'),
                    child: const Icon(
                      Icons.close,
                      color: Colors.black,
                    ))
              ],
            ),
            content: Container(
              decoration: const BoxDecoration(
                  border:
                      Border(top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)), bottom: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)))),
              margin: const EdgeInsets.only(top: 20, bottom: 10),
              width: 500,
              padding: const EdgeInsets.all(20),
              child: IntrinsicHeight(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text.rich(
                      TextSpan(
                          text: multiLanguageString(
                              name: "are_you_sure_you_want_to_delete", defaultValue: "Are you sure you want to delete ", context: context),
                          children: <InlineSpan>[
                            TextSpan(
                              text: selectedItem["title"] ?? "",
                              style: const TextStyle(fontWeight: FontWeight.bold),
                            ),
                            const TextSpan(text: ' ?')
                          ]),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 20),
                      decoration: BoxDecoration(
                        color: const Color.fromRGBO(228, 115, 87, 1),
                        borderRadius: BorderRadius.circular(5),
                      ),
                      padding: const EdgeInsets.only(left: 5),
                      child: Container(
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.horizontal(
                            right: Radius.circular(5),
                          ),
                          color: Color.fromRGBO(255, 233, 217, 1),
                        ),
                        padding: const EdgeInsets.all(15),
                        child: const Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Icon(Icons.warning, color: Color.fromRGBO(243, 86, 40, 1)),
                            SizedBox(width: 10),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                MultiLanguageText(
                                  name: "warning",
                                  defaultValue: "Warning",
                                  style: TextStyle(color: Color.fromARGB(255, 206, 20, 20), fontWeight: FontWeight.bold),
                                ),
                                SizedBox(height: 10),
                                MultiLanguageText(
                                  name: "cant_undo_action",
                                  defaultValue: "You can't undo this action.",
                                  style: TextStyle(color: Color.fromARGB(255, 187, 103, 82)),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            actionsAlignment: MainAxisAlignment.center,
            actions: [
              Container(
                margin: const EdgeInsets.only(right: 5),
                height: 40,
                width: 120,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(
                      const Color.fromRGBO(225, 46, 59, 1),
                    ),
                  ),
                  onPressed: () {
                    widget.callback();
                    Navigator.of(context).pop();
                  },
                  child: const MultiLanguageText(
                    name: "delete",
                    defaultValue: "Delete",
                    isLowerCase: false,
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
              const ButtonCancel()
            ],
          )
        : const Center(child: CircularProgressIndicator());
  }
}
