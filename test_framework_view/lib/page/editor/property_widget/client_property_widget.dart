import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';

import 'package:test_framework_view/type.dart';

import '../../../common/custom_state.dart';
import '../../../common/dynamic_dropdown_button.dart';
import '../../../model/model.dart';

class ClientPropertyWidget extends StatefulWidget {
  final Item item;
  final VoidCallback callback;
  final bool isDragAndDrop;
  final dynamic editorComponentItem;
  const ClientPropertyWidget(this.item, this.callback, {Key? key, required this.isDragAndDrop, required this.editorComponentItem}) : super(key: key);

  @override
  State<ClientPropertyWidget> createState() => _ClientPropertyWidgetState();
}

class _ClientPropertyWidgetState extends CustomState<ClientPropertyWidget> {
  TextEditingController nameController = TextEditingController();
  String? platformController;
  final _formKey = GlobalKey<FormState>();
  @override
  void initState() {
    super.initState();
    nameController.text = widget.item.name ?? "";
    platformController = widget.item.platform;
  }

  @override
  void dispose() {
    nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var securityModel = Provider.of<SecurityModel>(context, listen: false);
    return Form(
      key: _formKey,
      child: Column(
        children: [
          headerWidget(),
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                child: AbsorbPointer(
                  absorbing: !widget.isDragAndDrop,
                  child: Column(
                    children: [
                      Row(
                        children: [
                          const Expanded(
                              child: MultiLanguageText(
                                  name: "platform",
                                  defaultValue: "Platform",
                                  style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
                          Expanded(
                            flex: 3,
                            child: DynamicDropdownButton(
                                value: platformController,
                                hint: multiLanguageString(name: "enter_a_platform", defaultValue: "Enter a platform", context: context),
                                items: (securityModel.sysOrganizationPlatformsList ?? []).map<DropdownMenuItem<String>>((String result) {
                                  return DropdownMenuItem(
                                    value: result,
                                    child: Text(result),
                                  );
                                }).toList(),
                                onChanged: (newValue) {
                                  _formKey.currentState!.validate();
                                  setState(() {
                                    platformController = newValue;
                                  });
                                },
                                isRequiredNotEmpty: true),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        children: [
                          const Expanded(
                              child: MultiLanguageText(
                                  name: "name",
                                  defaultValue: "Name",
                                  style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
                          Expanded(
                            flex: 3,
                            child: SizedBox(
                              child: DynamicTextField(
                                controller: nameController,
                                hintText: multiLanguageString(name: "enter_a_name", defaultValue: "Enter a name", context: context),
                                onChanged: (text) {
                                  _formKey.currentState!.validate();
                                },
                                isRequiredNotEmpty: true,
                                isRequiredRegex: true,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          const SizedBox(height: 20),
          if (widget.isDragAndDrop) btnWidget(),
          if (widget.isDragAndDrop) const SizedBox(height: 20),
        ],
      ),
    );
  }

  Widget btnWidget() {
    return DynamicButton(
      label: multiLanguageString(name: "close", defaultValue: "Close", isLowerCase: false, context: context),
      name: "close",
      onPressed: () {
        if (_formKey.currentState!.validate()) {
          TestCaseConfigModel testCaseConfigModel = Provider.of<TestCaseConfigModel>(context, listen: false);
          var client = {"name": nameController.text, "platform": platformController};
          var exists = (testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"] ?? []).firstWhere((clientMap) {
            return clientMap["name"] == client["name"] && clientMap["platform"] == client["platform"];
          }, orElse: () => null);
          if (exists != null) {
            widget.item.name = nameController.text;
            widget.item.platform = platformController;
            Navigator.pop(context);
            widget.callback();
          } else if ((testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"] ?? [])
              .every((element) => element["name"] != client["name"])) {
            testCaseConfigModel.addClientList(client);
            testCaseConfigModel.setTypeOptionEditor(true);
            widget.item.name = nameController.text;
            widget.item.platform = platformController;
            Navigator.pop(context);
            widget.callback();
          } else {
            showToast(
                context: context,
                msg: multiLanguageString(name: "client_name_has_been_duplicated", defaultValue: "Client name has been duplicated", context: context),
                color: Colors.red,
                icon: const Icon(Icons.error));
          }
        }
      },
    );
  }

  Widget headerWidget() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
      child: Row(
        children: [
          Expanded(
            child: Center(
              child: MultiLanguageText(
                name: "component_item_property",
                defaultValue: "\$0 property",
                variables: ["${widget.editorComponentItem["title"]}"],
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: context.fontSizeManager.fontSizeText20, letterSpacing: 1.5),
              ),
            ),
          ),
          TextButton(
              onPressed: () {
                Navigator.pop(context);
                widget.callback();
              },
              child: const Icon(
                Icons.close,
                color: Colors.black,
              ))
        ],
      ),
    );
  }
}
