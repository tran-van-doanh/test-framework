import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/editor/element_config/element_repository.dart';
import 'package:test_framework_view/page/editor/element_config/find_value.dart';
import 'package:test_framework_view/page/editor/element_config/web_element.dart';
import 'package:test_framework_view/page/editor/property_widget/find_variable_widget.dart';

import '../../../common/custom_state.dart';
import '../../../type.dart';

class VariableDefaultProperty extends StatefulWidget {
  final List<Item> variablePath;
  final Function callback;
  final String? parentType;
  final bool isDragAndDrop;
  const VariableDefaultProperty({Key? key, required this.variablePath, required this.callback, this.parentType, required this.isDragAndDrop})
      : super(key: key);

  @override
  State<VariableDefaultProperty> createState() => _VariableDefaultPropertyState();
}

class _VariableDefaultPropertyState extends CustomState<VariableDefaultProperty> {
  final _formKey = GlobalKey<FormState>();
  List<Item> variablePathList = [];
  bool isStartDebug = false;
  List? parameterListProject;
  TextEditingController? valueElementController;

  @override
  void initState() {
    super.initState();
    setState(() {
      if (widget.variablePath.isEmpty || widget.variablePath[0].type == "variableGroup_default") {
        if (widget.parentType == "sleep") {
          Item newItem = Item("value", "variablePath");
          newItem.dataType = "Long";
          variablePathList.add(newItem);
        } else if (widget.parentType == "error") {
          Item newItem = Item("value", "variablePath");
          newItem.dataType = "String";
          variablePathList.add(newItem);
        } else {
          Item newItem = Item("variable", "variablePath");
          newItem.dataType = "String";
          variablePathList.add(newItem);
        }
      } else {
        variablePathList = widget.variablePath;
      }
    });
    getInfoPage();
  }

  getInfoPage() {
    getStatusDebug();
    if (variablePathList[0].type == "version_variable" ||
        variablePathList[0].type == "environment_variable" ||
        variablePathList[0].type == "node_variable") {
      getParamProject(Provider.of<NavigationModel>(context, listen: false).prjProjectId);
    }
    if (variablePathList[0].tfValueTemplateId != null) {
      getElementTemplate(variablePathList[0].tfValueTemplateId);
    }
    if (variablePathList[0].tfTestElementId != null && variablePathList[0].tfTestElementRepositoryId != null) {
      getElementRepositoryPath(variablePathList[0].tfTestElementRepositoryId, variablePathList[0].tfTestElementId);
    }
  }

  getStatusDebug() async {
    var statusResponse = await httpGet("/test-framework-api/user/node/status", context);
    if (statusResponse.containsKey("body") && statusResponse["body"] is String == false && statusResponse["body"].containsKey("result")) {
      if (statusResponse["body"]["result"]["status"] == 2) {
        setState(() {
          isStartDebug = true;
        });
      }
    }
  }

  getParamProject(prjProjectId) async {
    var response = await httpGet("/test-framework-api/user/project/prj-project/$prjProjectId", context);
    if (response.containsKey("body") && response["body"] is String == false && response["body"].containsKey("result")) {
      setState(() {
        parameterListProject = response["body"]["result"]["projectConfig"]["parameterFieldList"];
      });
    }
  }

  getElementTemplate(id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-value-template/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        valueElementController =
            TextEditingController(text: response["body"]["result"]["tfValueType"]["name"] + " / " + response["body"]["result"]["name"]);
      });
    }
  }

  getElementRepositoryPath(parentId, id) async {
    String result = "";
    var directoryPathResponse = await httpGet("/test-framework-api/user/test-framework/tf-test-element-repository/$parentId/path", context);
    if (directoryPathResponse.containsKey("body") && directoryPathResponse["body"] is String == false) {
      for (var path in directoryPathResponse["body"]["resultList"]) {
        result += path["title"] + " / ";
      }
    }
    if (mounted) {
      var elementResponse = await httpGet("/test-framework-api/user/test-framework/tf-test-element/$id", context);
      if (elementResponse.containsKey("body") && elementResponse["body"] is String == false) {
        if (elementResponse["body"]["result"] != null) {
          result += elementResponse["body"]["result"]["name"];
        } else {
          result += '"has been deleted"';
        }
      }
      setState(() {
        valueElementController = TextEditingController(text: result);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          headerWidget(context),
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                child: AbsorbPointer(
                  absorbing: !widget.isDragAndDrop,
                  child: Column(
                    children: [
                      Container(
                        margin: const EdgeInsets.only(top: 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    padding: const EdgeInsets.all(10),
                                    decoration: BoxDecoration(
                                      border: Border.all(color: const Color.fromRGBO(216, 218, 229, 1)),
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: Column(
                                      children: [
                                        for (var variablePath in variablePathList) variablePathWidget(variablePath),
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        if (variablePathList.isEmpty ||
                                            variablePathList[0].type == "variable" ||
                                            variablePathList[0].type == "version_variable" ||
                                            variablePathList[0].type == "environment_variable" ||
                                            variablePathList[0].type == "node_variable")
                                          FloatingActionButton(
                                            heroTag: null,
                                            child: const Icon(Icons.add),
                                            onPressed: () {
                                              setState(() {
                                                if (variablePathList.isNotEmpty) {
                                                  var newItem = Item("get", "variablePath");
                                                  newItem.dataType = "String";
                                                  variablePathList.add(newItem);
                                                }
                                              });
                                            },
                                          ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          if (widget.isDragAndDrop)
            DynamicButton(
              name: "close",
              label: multiLanguageString(name: "close", defaultValue: "Close", isLowerCase: false, context: context),
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  TestCaseConfigModel testCaseConfigModel = Provider.of<TestCaseConfigModel>(context, listen: false);
                  checkVariableParameter(
                      (testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["parameterFieldList"] ?? []),
                      variablePathList,
                      (testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["newVariableList"] ?? []),
                      (testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["outputFieldList"] ?? []));
                  Navigator.pop(context);
                  widget.callback(variablePathList);
                }
              },
            ),
          if (widget.isDragAndDrop)
            const SizedBox(
              height: 20,
            ),
        ],
      ),
    );
  }

  Widget variablePathWidget(variablePath) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (variablePathList.indexOf(variablePath) > 0)
          Align(
            alignment: Alignment.centerRight,
            child: InkWell(
                onTap: () {
                  setState(() {
                    variablePathList.remove(variablePath);
                  });
                },
                child: const Icon(
                  Icons.remove_circle,
                  color: Colors.red,
                )),
          ),
        const SizedBox(
          height: 5,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Wrap(
              spacing: 10,
              children: [
                if (variablePathList.indexOf(variablePath) == 0)
                  for (var valueType in [
                    {"value": "value", "name": multiLanguageString(name: "value", defaultValue: "Value", context: context)},
                    {"value": "variable", "name": multiLanguageString(name: "variable", defaultValue: "Variable", context: context)},
                    {
                      "value": "version_variable",
                      "name": multiLanguageString(name: "version_parameter", defaultValue: "Version Parameter", context: context)
                    },
                    {
                      "value": "environment_variable",
                      "name": multiLanguageString(name: "environment_parameter", defaultValue: "Environment Parameter", context: context)
                    },
                    {
                      "value": "node_variable",
                      "name": multiLanguageString(name: "agent_parameter", defaultValue: "Agent Parameter", context: context)
                    },
                  ])
                    GestureDetector(
                      onTap: () async {
                        if (variablePath.type != valueType["value"]) {
                          variablePath.type = valueType["value"];
                          if (variablePath.type == "version_variable" ||
                              variablePath.type == "environment_variable" ||
                              variablePath.type == "node_variable") {
                            await getParamProject(Provider.of<NavigationModel>(context, listen: false).prjProjectId);
                          }
                          // else if (variablePath.type == "value") {
                          //   variablePath.name = "";
                          // } else {
                          //   variablePath.value = "";
                          // }
                          // if (variablePath.type != "value") {
                          //   if (variablePath.tfValueTemplateId != null) {
                          //     variablePath.tfValueTemplateId = null;
                          //   }
                          //   if (variablePath.tfTestElementId != null) {
                          //     variablePath.tfTestElementId = null;
                          //   }
                          //   if (variablePath.tfTestElementRepositoryId != null) {
                          //     variablePath.tfTestElementRepositoryId = null;
                          //   }
                          // }
                          setState(() {});
                        }
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            border: Border(
                          bottom: BorderSide(
                              color: variablePath.type == valueType["value"] ? Colors.blue : Colors.grey,
                              width: variablePath.type == valueType["value"] ? 3 : 2),
                        )),
                        child: Text(
                          valueType["name"] ?? "",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: context.fontSizeManager.fontSizeText16,
                              color: const Color.fromRGBO(87, 94, 117, 1)),
                        ),
                      ),
                    ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                    child: DynamicDropdownButton(
                        value: variablePath.dataType,
                        hint: multiLanguageString(name: "choose_a_data_type", defaultValue: "Choose a dataType", context: context),
                        items: dataTypeList.map<DropdownMenuItem<String>>((dynamic result) {
                          return DropdownMenuItem(
                            value: result["value"],
                            child: Text(result["name"]),
                          );
                        }).toList(),
                        onChanged: (newValue) {
                          _formKey.currentState!.validate();
                          setState(() {
                            variablePath.dataType = newValue;
                          });
                        },
                        isRequiredNotEmpty: true)),
                const SizedBox(
                  width: 10,
                ),
                Expanded(flex: 3, child: buildVariablePath(variablePath)),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            btnOptionWidget(variablePath)
          ],
        ),
      ],
    );
  }

  Widget buildVariablePath(variablePath) {
    if (variablePath.type == "variable" || variablePath.type == "get") {
      return DynamicTextField(
        key: const Key("variable_get"),
        controller: TextEditingController(text: variablePath.name),
        hintText: multiLanguageString(name: "enter_a_name", defaultValue: "Enter a name", context: context),
        onChanged: (text) {
          _formKey.currentState!.validate();
          variablePath.name = text;
        },
        isRequiredRegex: true,
        obscureText: variablePath.dataType == "Password",
      );
    } else if (variablePath.type == "version_variable" || variablePath.type == "environment_variable" || variablePath.type == "node_variable") {
      final parameterList = (parameterListProject ?? []).where((element) => element["variableType"] == variablePath.type).toList();

      if (parameterList.isNotEmpty) {
        return DynamicDropdownButton(
          key: const Key("version_environment_node_variable"),
          value: parameterList.every((element) => element["name"] == variablePath.name) ? variablePath.name : null,
          hint: multiLanguageString(name: "choose_a_name", defaultValue: "Choose a name", context: context),
          items:
              parameterListProject!.where((element) => element["variableType"] == variablePath.type).map<DropdownMenuItem<String>>((dynamic result) {
            return DropdownMenuItem(
              value: result["name"],
              child: Text(result["name"]),
            );
          }).toList(),
          onChanged: (newValue) {
            _formKey.currentState!.validate();
            setState(() {
              variablePath.name = newValue;
            });
          },
        );
      } else {
        return DynamicTextField(
          key: const Key("version_environment_node_variable"),
          controller: TextEditingController(text: variablePath.name),
          hintText: multiLanguageString(name: "enter_a_name", defaultValue: "Enter a name", context: context),
          onChanged: (text) {
            _formKey.currentState!.validate();
            variablePath.name = text;
          },
          isRequiredRegex: true,
          obscureText: variablePath.dataType == "Password",
        );
      }
    } else if (variablePath.type == "value" && variablePath.tfValueTemplateId == null) {
      return DynamicTextField(
        key: const Key("value"),
        controller: TextEditingController(text: variablePath.value),
        hintText: multiLanguageString(name: "enter_a_value", defaultValue: "Enter a value", context: context),
        onChanged: (text) {
          _formKey.currentState!.validate();
          variablePath.value = text;
        },
        readOnly: !(variablePath.tfValueTemplateId == null && variablePath.tfTestElementId == null && variablePath.tfTestElementRepositoryId == null),
        obscureText: variablePath.dataType == "Password" &&
            (variablePath.tfValueTemplateId == null && variablePath.tfTestElementId == null && variablePath.tfTestElementRepositoryId == null),
      );
    } else if (variablePath.tfValueTemplateId != null || (variablePath.tfTestElementId != null && variablePath.tfTestElementRepositoryId != null)) {
      return DynamicTextField(
        key: const Key("RepositoryElement"),
        readOnly: true,
        controller: valueElementController,
        suffixIcon: IconButton(
          onPressed: () {
            setState(() {
              if (variablePath.tfValueTemplateId != null) {
                variablePath.tfValueTemplateId = null;
              }
              if (variablePath.tfTestElementId != null) {
                variablePath.tfTestElementId = null;
              }
              if (variablePath.tfTestElementRepositoryId != null) {
                variablePath.tfTestElementRepositoryId = null;
              }
            });
          },
          icon: const Icon(Icons.close),
          splashRadius: 20,
        ),
        isRequiredNotEmpty: true,
      );
    } else {
      return const SizedBox.shrink();
    }
  }

  Row btnOptionWidget(variablePath) {
    return Row(
      children: [
        const Expanded(
          child: SizedBox.shrink(),
        ),
        Expanded(
          flex: 3,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              if (variablePath.dataType == "WebElement" && variablePath.type == "value" && isStartDebug)
                Container(
                  margin: const EdgeInsets.only(right: 10),
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(backgroundColor: const Color.fromARGB(255, 255, 63, 49), fixedSize: const Size(130, 50)),
                      onPressed: () {
                        Navigator.of(context).push(
                          createRoute(
                            WebElementConfigWidget((xpath, element) {
                              setState(() {
                                variablePath.value = xpath["xpath"];
                              });
                            }),
                          ),
                        );
                      },
                      child: const MultiLanguageText(name: "find_element", defaultValue: "Find element")),
                ),
              if (variablePath.type == "variable")
                Container(
                  margin: const EdgeInsets.only(right: 10),
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(fixedSize: const Size(130, 50)),
                      onPressed: () {
                        Navigator.of(context).push(
                          createRoute(
                            FindVariableWidget((valueCallback) {
                              setState(() {
                                variablePath.name = valueCallback["name"];
                                variablePath.dataType = valueCallback["dataType"];
                              });
                            }),
                          ),
                        );
                      },
                      child: const MultiLanguageText(name: "find_variable", defaultValue: "Find variable")),
                ),
              if (variablePath.type == "value")
                Container(
                  margin: const EdgeInsets.only(right: 10),
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(fixedSize: const Size(120, 50)),
                      onPressed: () {
                        Navigator.of(context).push(
                          createRoute(
                            FindTemplateWidget((valueCallback) {
                              setState(() {
                                if (variablePath.tfTestElementId != null) {
                                  variablePath.tfTestElementId = null;
                                }
                                if (variablePath.tfTestElementRepositoryId != null) {
                                  variablePath.tfTestElementRepositoryId = null;
                                }
                                variablePath.value = valueCallback["value"];
                                variablePath.tfValueTemplateId = valueCallback["id"];
                              });
                              getElementTemplate(variablePath.tfValueTemplateId);
                            }),
                          ),
                        );
                      },
                      child: const MultiLanguageText(name: "find_value", defaultValue: "Find value")),
                ),
              if (variablePath.type == "value" && variablePath.dataType == "RepositoryElement")
                Container(
                  margin: const EdgeInsets.only(right: 10),
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(fixedSize: const Size(170, 50)),
                      onPressed: () {
                        Navigator.of(context).push(
                          createRoute(
                            ElementRepositoryConfigWidget((parentId, valueCallback, xpath, name) {
                              setState(() {
                                if (variablePath.tfValueTemplateId != null) {
                                  variablePath.tfValueTemplateId = null;
                                }
                                variablePath.value = xpath;
                                variablePath.tfTestElementId = valueCallback["id"];
                                variablePath.tfTestElementRepositoryId = parentId;
                                variablePath.tfTestElementXpathName = name;
                              });
                              getElementRepositoryPath(variablePath.tfTestElementRepositoryId, variablePath.tfTestElementId);
                            }),
                          ),
                        );
                      },
                      child: const MultiLanguageText(name: "element_repository", defaultValue: "Element Repository")),
                )
            ],
          ),
        ),
      ],
    );
  }

  Padding headerWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Center(
              child: MultiLanguageText(
                name: "variable_default_property",
                defaultValue: "Variable default property",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: context.fontSizeManager.fontSizeText20, letterSpacing: 1.5),
              ),
            ),
          ),
          TextButton(
              onPressed: () {
                Navigator.pop(context);
                widget.callback(variablePathList);
              },
              child: const Icon(
                Icons.close,
                color: Colors.black,
              ))
        ],
      ),
    );
  }

  checkVariableParameter(List parameterFieldListTestCase, List<Item> variablePathList, List newVariableReturnType, List outputFieldList) {
    var resultParameterFieldListTestCase = [];
    for (var element in parameterFieldListTestCase) {
      resultParameterFieldListTestCase.add(element);
    }
    var resultNewVariableReturnType = [];
    for (var element in newVariableReturnType) {
      resultNewVariableReturnType.add(element);
    }

    if ((variablePathList[0].type == "variable"
        // ||
        //         variablePathList[0].type == "version_variable"
        ) &&
        variablePathList[0].dataType != "WebElement" &&
        variablePathList[0].dataType != "CallOutput" &&
        !resultParameterFieldListTestCase.map((parameterFieldTestCase) => parameterFieldTestCase["name"]).contains(variablePathList[0].name) &&
        !resultNewVariableReturnType.map((newVariableReturnType) => newVariableReturnType["name"]).contains(variablePathList[0].name) &&
        outputFieldList.every((outputField) => outputField["name"] != variablePathList[0].name)) {
      resultParameterFieldListTestCase.add({"dataType": variablePathList[0].dataType, "name": variablePathList[0].name});
    }

    Provider.of<TestCaseConfigModel>(context, listen: false).setParameterFieldList(resultParameterFieldListTestCase);
    Provider.of<TestCaseConfigModel>(context, listen: false).setTypeOptionEditor(true);
  }
}
