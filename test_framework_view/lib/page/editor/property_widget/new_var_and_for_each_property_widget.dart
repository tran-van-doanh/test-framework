import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';

import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/type.dart';

import '../../../common/custom_state.dart';

class NewVarAndForEachPropertyWidget extends StatefulWidget {
  final Item item;
  final VoidCallback callback;
  final bool isDragAndDrop;
  final dynamic editorComponentItem;
  const NewVarAndForEachPropertyWidget(this.item, this.callback, {Key? key, required this.isDragAndDrop, required this.editorComponentItem})
      : super(key: key);

  @override
  State<NewVarAndForEachPropertyWidget> createState() => _NewVarAndForEachPropertyWidgetState();
}

class _NewVarAndForEachPropertyWidgetState extends CustomState<NewVarAndForEachPropertyWidget> {
  String dataType = "String";
  String? isList;
  TextEditingController nameController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    if (widget.item.type == "newVariable" || widget.item.type == "assignValue") {
      nameController.text = widget.item.name ?? "";
      isList = widget.item.isList != null ? widget.item.isList.toString() : "false";
    } else if (widget.item.type == "forEach") {
      nameController.text = widget.item.objectName ?? "";
    }
    if (widget.item.dataType != "") dataType = widget.item.dataType ?? "";
    descriptionController.text = widget.item.description ?? "";
    super.initState();
  }

  @override
  void dispose() {
    nameController.dispose();
    descriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          headerWidget(),
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                child: bodyWidget(),
              ),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          if (widget.isDragAndDrop)
            DynamicButton(
              name: "close",
              label: multiLanguageString(name: "close", defaultValue: "Close", isLowerCase: false, context: context),
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  if (!Provider.of<TestCaseConfigModel>(context, listen: false)
                      .testCaseConfigMap[Provider.of<TestCaseConfigModel>(context, listen: false).testCaseIdList.last]["newVariableList"]
                      .map((newVariable) => newVariable["name"])
                      .contains(nameController.text)) {
                    Provider.of<TestCaseConfigModel>(context, listen: false).addNewVariableList({
                      "name": nameController.text,
                      "dataType": dataType,
                      "parentId": widget.item.id,
                    });
                  }
                  if (widget.item.type == "newVariable") {
                    widget.item.name = nameController.text;
                    widget.item.isList = isList == "true";
                  } else if (widget.item.type == "assignValue") {
                    widget.item.name = nameController.text;
                  } else if (widget.item.type == "forEach") {
                    widget.item.objectName = nameController.text;
                  }
                  widget.item.dataType = dataType;
                  widget.item.description = descriptionController.text;
                  Navigator.pop(context);
                  widget.callback();
                }
              },
            ),
          if (widget.isDragAndDrop) const SizedBox(height: 20),
        ],
      ),
    );
  }

  Widget bodyWidget() {
    return AbsorbPointer(
      absorbing: !widget.isDragAndDrop,
      child: Column(
        children: [
          Row(
            children: [
              const Expanded(
                  child: MultiLanguageText(
                      name: "data_type",
                      defaultValue: "Data Type",
                      suffiText: " : ",
                      style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
              Expanded(
                flex: 3,
                child: DynamicDropdownButton(
                    value: dataType != "" ? dataType : null,
                    hint: multiLanguageString(name: "choose_a_data_type", defaultValue: "Choose a dataType", context: context),
                    items: dataTypeList.map<DropdownMenuItem<String>>((dynamic result) {
                      return DropdownMenuItem(
                        value: result["value"],
                        child: Text(result["name"]),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      _formKey.currentState!.validate();
                      setState(() {
                        dataType = newValue;
                      });
                    },
                    isRequiredNotEmpty: true),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          if (widget.item.type == "newVariable")
            Row(
              children: [
                const Expanded(
                    child: MultiLanguageText(
                        name: "is_list",
                        defaultValue: "isList",
                        suffiText: " : ",
                        style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
                Expanded(
                  flex: 3,
                  child: DynamicDropdownButton(
                      value: isList != "" ? isList : null,
                      hint: multiLanguageString(name: "is_a_list", defaultValue: "Is a list ?", context: context),
                      items: const ["true", "false"].map<DropdownMenuItem<String>>((String result) {
                        return DropdownMenuItem(
                          value: result,
                          child: Text(result),
                        );
                      }).toList(),
                      onChanged: (newValue) {
                        _formKey.currentState!.validate();
                        setState(() {
                          isList = newValue;
                        });
                      },
                      isRequiredNotEmpty: true),
                ),
              ],
            ),
          if (widget.item.type == "newVariable")
            const SizedBox(
              height: 20,
            ),
          Row(
            children: [
              const Expanded(
                  child: MultiLanguageText(
                      name: "name",
                      defaultValue: "Name",
                      suffiText: " : ",
                      style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
              Expanded(
                flex: 3,
                child: SizedBox(
                  child: DynamicTextField(
                    controller: nameController,
                    hintText: multiLanguageString(name: "enter_a_name", defaultValue: "Enter a name", context: context),
                    onChanged: (text) {
                      _formKey.currentState!.validate();
                    },
                    isRequiredNotEmpty: true,
                    isRequiredRegex: true,
                    obscureText: dataType == "Password",
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              const Expanded(
                  child: MultiLanguageText(
                      name: "step_comment",
                      defaultValue: "Step Comment",
                      suffiText: " : ",
                      style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
              Expanded(
                flex: 3,
                child: SizedBox(
                  child: DynamicTextField(
                    controller: descriptionController,
                    hintText: multiLanguageString(name: "enter_a_description", defaultValue: "Enter a description", context: context),
                    onChanged: (text) {
                      _formKey.currentState!.validate();
                    },
                    maxline: 3,
                    minline: 1,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget headerWidget() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
      child: Row(
        children: [
          Expanded(
            child: Center(
              child: MultiLanguageText(
                name: "component_item_property",
                defaultValue: "\$0 property",
                variables: ["${widget.editorComponentItem["title"]}"],
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: context.fontSizeManager.fontSizeText20, letterSpacing: 1.5),
              ),
            ),
          ),
          TextButton(
              onPressed: () {
                Navigator.pop(context);
                widget.callback();
              },
              child: const Icon(
                Icons.close,
                color: Colors.black,
              ))
        ],
      ),
    );
  }
}
