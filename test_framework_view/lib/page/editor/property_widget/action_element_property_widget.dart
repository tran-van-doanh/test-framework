import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/editor/property_widget/dynamic_variabla_path_widget.dart';
import 'package:test_framework_view/page/editor/property_widget/find_variable_widget.dart';
import 'package:test_framework_view/type.dart';

import '../../../common/custom_state.dart';

class ActionElement extends StatefulWidget {
  final Item item;
  final VoidCallback callback;
  final bool isDragAndDrop;
  final dynamic editorComponentItem;
  const ActionElement(this.item, this.callback, {Key? key, required this.isDragAndDrop, required this.editorComponentItem}) : super(key: key);

  @override
  State<ActionElement> createState() => _ActionElementState();
}

class _ActionElementState extends CustomState<ActionElement> {
  List parameterList = [];
  String? clientName;

  TextEditingController descriptionController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  List? parameterListProject;

  @override
  void initState() {
    getInfoPage();
    super.initState();
  }

  getInfoPage() {
    setInitValue();
    if (widget.item.returnType["type"] == "version_variable" ||
        widget.item.returnType["type"] == "environment_variable" ||
        widget.item.returnType["type"] == "node_variable") {
      getParamProject(Provider.of<NavigationModel>(context, listen: false).prjProjectId);
    }
  }

  getParamProject(prjProjectId) async {
    var response = await httpGet("/test-framework-api/user/project/prj-project/$prjProjectId", context);
    if (response.containsKey("body") && response["body"] is String == false && response["body"].containsKey("result")) {
      setState(() {
        parameterListProject = response["body"]["result"]["projectConfig"]["parameterFieldList"];
      });
    }
  }

  setInitValue() {
    setState(() {
      clientName = widget.item.clientName;
      if (widget.item.parameterList.isNotEmpty) {
        parameterList = widget.item.parameterList;
      } else if (widget.editorComponentItem["componentConfig"] != null && widget.editorComponentItem["componentConfig"].isNotEmpty) {
        parameterList = widget.editorComponentItem["componentConfig"]["componentProperty"] ?? [];
      }
      descriptionController.text = widget.item.description ?? "";
      if (widget.item.returnType.isEmpty &&
          widget.editorComponentItem["componentConfig"] != null &&
          widget.editorComponentItem["componentConfig"].isNotEmpty) {
        widget.item.returnType = widget.editorComponentItem["componentConfig"]["returnType"] ?? {};
      }
    });
  }

  onRemoveVariablePath(variablePathList, variablePath) {
    setState(() {
      variablePathList.remove(variablePath);
    });
  }

  @override
  void dispose() {
    descriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<TestCaseConfigModel>(builder: (context, testCaseConfigModel, child) {
      return Form(
        key: _formKey,
        child: Column(
          children: [
            headerWidget(),
            Expanded(
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                  child: AbsorbPointer(
                    absorbing: !widget.isDragAndDrop,
                    child: Column(
                      children: [
                        Row(
                          children: [
                            const Expanded(
                              child: MultiLanguageText(
                                name: "step_comment",
                                defaultValue: "Step Comment",
                                suffiText: " : ",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Color.fromRGBO(87, 94, 117, 1),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 3,
                              child: SizedBox(
                                child: DynamicTextField(
                                  controller: descriptionController,
                                  hintText: multiLanguageString(name: "enter_a_description", defaultValue: "Enter a description", context: context),
                                  onChanged: (text) {
                                    _formKey.currentState!.validate();
                                  },
                                  maxline: 3,
                                  minline: 1,
                                ),
                              ),
                            ),
                          ],
                        ),
                        if (widget.item.type == "goTo")
                          const SizedBox(
                            height: 10,
                          ),
                        if (widget.item.type == "goTo")
                          Row(
                            children: [
                              const Expanded(
                                  child: MultiLanguageText(
                                      name: "client",
                                      defaultValue: "Client",
                                      suffiText: " : ",
                                      style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
                              Expanded(
                                flex: 3,
                                child: DynamicDropdownButton(
                                    value: clientName,
                                    hint: multiLanguageString(name: "choose_a_client", defaultValue: "Choose a client", context: context),
                                    items: testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"]
                                        .map<DropdownMenuItem>((result) {
                                      return DropdownMenuItem(
                                        value: result["name"],
                                        child: Text(result["name"]),
                                      );
                                    }).toList(),
                                    onChanged: (newValue) async {
                                      _formKey.currentState!.validate();
                                      clientName = newValue;
                                    },
                                    isRequiredNotEmpty: true),
                              ),
                            ],
                          ),
                        const SizedBox(
                          height: 10,
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 20),
                          child: Column(
                            children: [
                              if (widget.item.type == "mouseAction" || widget.item.type == "mouseActionWithOffset") chooseMouseActionTypeWidget(),
                              parameterListWidget(),
                              if (widget.editorComponentItem["componentConfig"]["keyType"] != null &&
                                  widget.editorComponentItem["componentConfig"]["keyType"].isNotEmpty)
                                chooseKeyWidget(),
                            ],
                          ),
                        ),
                        if (widget.item.returnType.isNotEmpty) returnTypeWidget(),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            if (widget.isDragAndDrop) btnWidget(testCaseConfigModel),
            if (widget.isDragAndDrop)
              const SizedBox(
                height: 20,
              ),
          ],
        ),
      );
    });
  }

  Column parameterListWidget() {
    List locatorList = parameterList.where((parameter) => parameter["name"] == "xpath" || parameter["name"].startsWith("parent")).toList();
    List inputList = parameterList.where((parameter) => parameter["name"] != "xpath" && !parameter["name"].startsWith("parent")).toList();
    return Column(
      children: [
        if (locatorList.isNotEmpty)
          Column(
            children: [
              if (locatorList.isNotEmpty)
                parameterTypeWidget(multiLanguageString(name: "locator", defaultValue: "Locator", context: context), locatorList),
            ],
          ),
        if (inputList.isNotEmpty) parameterTypeWidget(multiLanguageString(name: "input", defaultValue: "Input", context: context), inputList),
      ],
    );
  }

  Column parameterTypeWidget(String text, List<dynamic> list) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(text, style: const TextStyle(fontWeight: FontWeight.bold, color: Color.fromARGB(255, 35, 67, 172))),
        Container(
            padding: const EdgeInsets.all(10),
            margin: const EdgeInsets.only(top: 5, bottom: 20),
            decoration: BoxDecoration(
              border: Border.all(color: const Color.fromRGBO(216, 218, 229, 1)),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              children: [
                for (var parameter in list) parameterItemWidget(parameter),
              ],
            ))
      ],
    );
  }

  Widget btnWidget(TestCaseConfigModel testCaseConfigModel) {
    return DynamicButton(
      name: "close",
      label: multiLanguageString(name: "close", defaultValue: "Close", isLowerCase: false, context: context),
      onPressed: () {
        if (_formKey.currentState!.validate()) {
          if (widget.item.returnType["type"] == "newVariable") {
            var returnType = Map.from(widget.item.returnType);
            if (!testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["newVariableList"]
                .map((newVariable) => newVariable["name"])
                .contains(returnType["name"])) {
              Map<dynamic, dynamic> value = Map.from(returnType);
              value["parentId"] = widget.item.id;
              Provider.of<TestCaseConfigModel>(context, listen: false).addNewVariableList(value);
            }
          }
          checkVariableParameter(
              testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["parameterFieldList"],
              parameterList,
              testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["newVariableList"],
              testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["outputFieldList"]);
          widget.item.parameterList = parameterList;
          widget.item.description = descriptionController.text;
          widget.item.clientName = clientName;
          Navigator.pop(context);
          widget.callback();
        }
      },
    );
  }

  Widget returnTypeWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const MultiLanguageText(
              name: "output", defaultValue: "Output", style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromARGB(255, 35, 67, 172))),
          const SizedBox(
            height: 5,
          ),
          Row(
            children: [
              Expanded(
                child: Container(
                  padding: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    border: Border.all(color: const Color.fromRGBO(216, 218, 229, 1)),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          const Expanded(
                              child: MultiLanguageText(
                                  name: "type",
                                  defaultValue: "Type",
                                  suffiText: " : ",
                                  style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
                          Expanded(
                            flex: 3,
                            child: DynamicDropdownButton(
                                value: widget.item.returnType["type"],
                                hint: multiLanguageString(name: "choose_a_type", defaultValue: "Choose a type", context: context),
                                items: const [
                                  "none",
                                  "variable",
                                  "newVariable",
                                  "version_variable",
                                  "environment_variable",
                                  "node_variable",
                                ].map<DropdownMenuItem<String>>((String result) {
                                  return DropdownMenuItem(
                                    value: result,
                                    child: Text(result),
                                  );
                                }).toList(),
                                onChanged: (newValue) async {
                                  _formKey.currentState!.validate();
                                  widget.item.returnType["type"] = newValue;
                                  if (widget.item.returnType["type"] == "version_variable" ||
                                      widget.item.returnType["type"] == "environment_variable" ||
                                      widget.item.returnType["type"] == "node_variable") {
                                    widget.item.returnType["name"] = null;
                                    await getParamProject(Provider.of<NavigationModel>(context, listen: false).prjProjectId);
                                  }
                                  if (widget.item.returnType["type"] == "none") {
                                    widget.item.returnType = {
                                      "type": "none",
                                    };
                                  }
                                  setState(() {});
                                },
                                isRequiredNotEmpty: true),
                          ),
                        ],
                      ),
                      if (widget.item.returnType["type"] != "none")
                        Column(
                          children: [
                            if (widget.item.returnType["type"] == "newVariable" &&
                                widget.item.type != "findElements" &&
                                widget.item.type != "childElementFindElements" &&
                                widget.item.type != "findElement" &&
                                widget.item.type != "findElementsByPosition" &&
                                widget.item.type != "findElementByPosition" &&
                                widget.item.type != "childElementFindElement" &&
                                widget.item.type != "getWindowHandle" &&
                                widget.item.type != "getWindowHandles")
                              Column(
                                children: [
                                  const SizedBox(
                                    height: 20,
                                  ),
                                  Row(
                                    children: [
                                      const Expanded(
                                        child: MultiLanguageText(
                                          name: "is_list",
                                          defaultValue: "isList",
                                          suffiText: " : ",
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Color.fromRGBO(87, 94, 117, 1),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 3,
                                        child: DynamicDropdownButton(
                                          value: widget.item.returnType["isList"].toString() != "null"
                                              ? widget.item.returnType["isList"].toString()
                                              : null,
                                          hint: multiLanguageString(name: "is_a_list", defaultValue: "Is a list ?", context: context),
                                          items: const ["true", "false"].map<DropdownMenuItem<String>>((String result) {
                                            return DropdownMenuItem(
                                              value: result,
                                              child: Text(result),
                                            );
                                          }).toList(),
                                          onChanged: (newValue) {
                                            _formKey.currentState!.validate();
                                            setState(() {
                                              if (newValue == "true") {
                                                widget.item.returnType["isList"] = true;
                                              } else {
                                                widget.item.returnType["isList"] = false;
                                              }
                                            });
                                          },
                                          isRequiredNotEmpty: true,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            const SizedBox(
                              height: 20,
                            ),
                            Row(
                              children: [
                                const Expanded(
                                    child: MultiLanguageText(
                                        name: "data_type",
                                        defaultValue: "Data Type",
                                        suffiText: " : ",
                                        style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
                                Expanded(
                                    flex: 3,
                                    child: DynamicDropdownButton(
                                        value: widget.item.returnType["dataType"],
                                        hint: multiLanguageString(name: "choose_a_data_type", defaultValue: "Choose a dataType", context: context),
                                        items: dataTypeList.map<DropdownMenuItem<String>>((dynamic result) {
                                          return DropdownMenuItem(
                                            value: result["value"],
                                            child: Text(result["name"]),
                                          );
                                        }).toList(),
                                        onChanged: (newValue) {
                                          _formKey.currentState!.validate();
                                          setState(() {
                                            widget.item.returnType["dataType"] = newValue;
                                          });
                                        },
                                        isRequiredNotEmpty: true)),
                              ],
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            ((widget.item.returnType["type"] == "version_variable" ||
                                        widget.item.returnType["type"] == "environment_variable" ||
                                        widget.item.returnType["type"] == "node_variable") &&
                                    parameterListProject != null &&
                                    parameterListProject!.isNotEmpty)
                                ? Row(
                                    children: [
                                      const Expanded(
                                        child: MultiLanguageText(
                                          name: "name",
                                          defaultValue: "Name",
                                          suffiText: " : ",
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Color.fromRGBO(87, 94, 117, 1),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 3,
                                        child: DynamicDropdownButton(
                                            value: widget.item.returnType["name"],
                                            hint: multiLanguageString(name: "choose_a_name", defaultValue: "Choose a name", context: context),
                                            items: parameterListProject!
                                                .where((element) => element["variableType"] == widget.item.returnType["type"])
                                                .map<DropdownMenuItem<String>>((dynamic result) {
                                              return DropdownMenuItem(
                                                value: result["name"],
                                                child: Text(result["name"]),
                                              );
                                            }).toList(),
                                            onChanged: (newValue) {
                                              _formKey.currentState!.validate();
                                              setState(() {
                                                widget.item.returnType["name"] = newValue;
                                              });
                                            },
                                            isRequiredNotEmpty: true),
                                      ),
                                    ],
                                  )
                                : Row(
                                    children: [
                                      const Expanded(
                                          child: MultiLanguageText(
                                              name: "name",
                                              defaultValue: "Name",
                                              suffiText: " : ",
                                              style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
                                      Expanded(
                                        flex: 3,
                                        child: Row(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Expanded(
                                              child: DynamicTextField(
                                                controller: TextEditingController(text: widget.item.returnType["name"] ?? ""),
                                                hintText: multiLanguageString(name: "enter_a_name", defaultValue: "Enter a name", context: context),
                                                onChanged: (text) {
                                                  _formKey.currentState!.validate();
                                                  widget.item.returnType["name"] = text;
                                                },
                                                isRequiredNotEmpty: true,
                                                isRequiredRegex: true,
                                                obscureText: widget.item.returnType["dataType"] == "Password",
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                            if (widget.item.returnType["type"] == "variable")
                              const SizedBox(
                                height: 10,
                              ),
                            if (widget.item.returnType["type"] == "variable")
                              Row(
                                children: [
                                  const Expanded(
                                    child: SizedBox.shrink(),
                                  ),
                                  Expanded(
                                    flex: 3,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        ElevatedButton(
                                            style: ElevatedButton.styleFrom(fixedSize: const Size(130, 50)),
                                            onPressed: () {
                                              Navigator.of(context).push(
                                                createRoute(
                                                  FindVariableWidget((valueCallback) {
                                                    setState(() {
                                                      widget.item.returnType["name"] = valueCallback["name"];
                                                      widget.item.returnType["dataType"] = valueCallback["dataType"];
                                                    });
                                                  }),
                                                ),
                                              );
                                            },
                                            child: const MultiLanguageText(name: "find_variable", defaultValue: "Find variable")),
                                      ],
                                    ),
                                  ),
                                ],
                              )
                          ],
                        )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget chooseMouseActionTypeWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const MultiLanguageText(
            name: "mouse_action_type",
            defaultValue: "Mouse action type",
            style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromARGB(255, 35, 67, 172))),
        const SizedBox(
          height: 5,
        ),
        Container(
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
            border: Border.all(color: const Color.fromRGBO(216, 218, 229, 1)),
            borderRadius: BorderRadius.circular(10),
          ),
          child: Row(
            children: [
              const Expanded(
                  child: MultiLanguageText(
                      name: "mouse_action_type",
                      defaultValue: "Mouse action type",
                      style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
              Expanded(
                flex: 3,
                child: DynamicDropdownButton(
                    value: widget.item.mouseActionType,
                    hint: multiLanguageString(name: "choose_mouse_action_type", defaultValue: "Choose mouse action type", context: context),
                    items: mouseActionTypeValueList.map<DropdownMenuItem<String>>((var result) {
                      return DropdownMenuItem(
                        value: result["value"],
                        child: Text(result["name"]),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      _formKey.currentState!.validate();
                      setState(() {
                        widget.item.mouseActionType = newValue;
                      });
                    },
                    isRequiredNotEmpty: true),
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 20,
        ),
      ],
    );
  }

  Widget chooseKeyWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 20,
        ),
        MultiLanguageText(
            name: widget.editorComponentItem["componentConfig"]["keyType"].toLowerCase().replaceAll(' ', '_'),
            defaultValue: widget.editorComponentItem["componentConfig"]["keyType"],
            style: const TextStyle(fontWeight: FontWeight.bold, color: Color.fromARGB(255, 35, 67, 172))),
        const SizedBox(
          height: 5,
        ),
        Container(
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
            border: Border.all(color: const Color.fromRGBO(216, 218, 229, 1)),
            borderRadius: BorderRadius.circular(10),
          ),
          child: Row(
            children: [
              Expanded(
                  child: MultiLanguageText(
                      name: widget.editorComponentItem["componentConfig"]["keyType"],
                      defaultValue: widget.editorComponentItem["componentConfig"]["keyType"],
                      style: const TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
              Expanded(
                flex: 3,
                child: DynamicDropdownButton(
                    value: widget.item.key,
                    hint: multiLanguageString(name: "choose_a_key", defaultValue: "Choose a key", context: context),
                    items: keyValueList.map<DropdownMenuItem<String>>((String result) {
                      return DropdownMenuItem(
                        value: result,
                        child: Text(result),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      _formKey.currentState!.validate();
                      setState(() {
                        widget.item.key = newValue;
                      });
                    },
                    isRequiredNotEmpty: widget.editorComponentItem["componentConfig"]["keyType"] == "End Key" ? false : true),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget parameterItemWidget(parameter) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        (parameter["name"] == "xpath")
            ? MultiLanguageText(
                name: "element_description",
                defaultValue: "Element ${parameter["description"] ?? ""}",
                style: const TextStyle(fontWeight: FontWeight.bold, color: Color.fromARGB(255, 35, 67, 172)))
            : Text("${parameter["name"].replaceFirst("parent", "Parent ")} ${parameter["description"] ?? ""}",
                style: const TextStyle(fontWeight: FontWeight.bold, color: Color.fromARGB(255, 35, 67, 172))),
        const SizedBox(
          height: 5,
        ),
        Row(
          children: [
            Expanded(
              child: Container(
                margin: parameter != parameterList.last ? const EdgeInsets.only(bottom: 10) : null,
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  border: Border.all(color: const Color.fromRGBO(216, 218, 229, 1)),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Column(
                  children: [
                    if (parameter["variablePath"] != null)
                      for (var variablePath in parameter["variablePath"])
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            if (parameter["name"] == "unit")
                              DynamicVariablePathWidget(
                                variablePath,
                                parameter["variablePath"].indexOf(variablePath),
                                const [],
                                const [],
                                unitValueList: const ["SECONDS", "MINUTES", "HOURS", "DAYS", "MONTHS", "YEARS"],
                                keyForm: _formKey,
                                callback: () {
                                  setState(() {});
                                },
                                onRemove: () => onRemoveVariablePath(parameter["variablePath"], variablePath),
                              ),
                            if (parameter["name"] == "parentElement")
                              DynamicVariablePathWidget(
                                variablePath,
                                parameter["variablePath"].indexOf(variablePath),
                                [
                                  {"value": "variable", "name": multiLanguageString(name: "variable", defaultValue: "Variable", context: context)},
                                  {
                                    "value": "version_variable",
                                    "name": multiLanguageString(name: "version_parameter", defaultValue: "Version Parameter", context: context)
                                  },
                                  {
                                    "value": "environment_variable",
                                    "name":
                                        multiLanguageString(name: "environment_parameter", defaultValue: "Environment Parameter", context: context)
                                  },
                                  {
                                    "value": "node_variable",
                                    "name": multiLanguageString(name: "agent_parameter", defaultValue: "Agent Parameter", context: context)
                                  },
                                ],
                                const [
                                  {"value": "WebElement", "name": "Elements"},
                                  {"value": "RepositoryElement", "name": "Repository Element"},
                                  {"value": "Password", "name": "Password"},
                                  {"value": "Null", "name": "Null"},
                                  {"value": "EmptyString", "name": "Empty String"},
                                ],
                                keyForm: _formKey,
                                callback: () {
                                  setState(() {});
                                },
                                onRemove: () => onRemoveVariablePath(parameter["variablePath"], variablePath),
                              ),
                            if (parameter["name"] == "xpath")
                              DynamicVariablePathWidget(
                                variablePath,
                                parameter["variablePath"].indexOf(variablePath),
                                [
                                  {"value": "value", "name": multiLanguageString(name: "value", defaultValue: "Value", context: context)},
                                  {"value": "variable", "name": multiLanguageString(name: "variable", defaultValue: "Variable", context: context)},
                                  {
                                    "value": "version_variable",
                                    "name": multiLanguageString(name: "version_parameter", defaultValue: "Version Parameter", context: context)
                                  },
                                  {
                                    "value": "environment_variable",
                                    "name":
                                        multiLanguageString(name: "environment_parameter", defaultValue: "Environment Parameter", context: context)
                                  },
                                  {
                                    "value": "node_variable",
                                    "name": multiLanguageString(name: "agent_parameter", defaultValue: "Agent Parameter", context: context)
                                  },
                                ],
                                (widget.item.type != "waitRepositoryElementExists" && widget.item.type != "waitRepositoryElementDisappear")
                                    ? [
                                        {"value": "WebElement", "name": "Elements"},
                                        if (widget.item.type != "waitElementExists" && widget.item.type != "waitElementDisappear")
                                          {"value": "RepositoryElement", "name": "Repository Element"},
                                        {"value": "Password", "name": "Password"},
                                        {"value": "Null", "name": "Null"},
                                        {"value": "EmptyString", "name": "Empty String"},
                                      ]
                                    : [
                                        {"value": "RepositoryElement", "name": "Repository Element"},
                                      ],
                                keyForm: _formKey,
                                callback: () {
                                  setState(() {});
                                },
                                clientName: clientName,
                                platform: widget.item.platform,
                                onRemove: () => onRemoveVariablePath(parameter["variablePath"], variablePath),
                              ),
                            if (parameter["name"] != "xpath" &&
                                parameter["name"] != "parentElement" &&
                                parameter["name"] != "type" &&
                                widget.item.type != "childElementClearAndSendFile" &&
                                widget.item.type != "clearAndSendFile" &&
                                parameter["name"] != "unit")
                              DynamicVariablePathWidget(
                                variablePath,
                                parameter["variablePath"].indexOf(variablePath),
                                [
                                  {"value": "value", "name": multiLanguageString(name: "value", defaultValue: "Value", context: context)},
                                  {"value": "variable", "name": multiLanguageString(name: "variable", defaultValue: "Variable", context: context)},
                                  {
                                    "value": "version_variable",
                                    "name": multiLanguageString(name: "version_parameter", defaultValue: "Version Parameter", context: context)
                                  },
                                  {
                                    "value": "environment_variable",
                                    "name":
                                        multiLanguageString(name: "environment_parameter", defaultValue: "Environment Parameter", context: context)
                                  },
                                  {
                                    "value": "node_variable",
                                    "name": multiLanguageString(name: "agent_parameter", defaultValue: "Agent Parameter", context: context)
                                  },
                                ],
                                (widget.item.type == "waitElementExists" || widget.item.type == "waitElementDisappear")
                                    ? dataTypeList.where((element) => element["value"] != "RepositoryElement").toList()
                                    : dataTypeList,
                                keyForm: _formKey,
                                callback: () {
                                  setState(() {});
                                },
                                clientName: clientName,
                                platform: widget.item.platform,
                                isRequiredNotEmpty: parameter["name"] != "belowOf" &&
                                    parameter["name"] != "middleOf" &&
                                    parameter["name"] != "aboveOf" &&
                                    parameter["name"] != "rightOf" &&
                                    parameter["name"] != "centerOf" &&
                                    parameter["name"] != "leftOf",
                                isRequiredRegex: parameter["name"] != "belowOf" &&
                                    parameter["name"] != "middleOf" &&
                                    parameter["name"] != "aboveOf" &&
                                    parameter["name"] != "rightOf" &&
                                    parameter["name"] != "centerOf" &&
                                    parameter["name"] != "leftOf",
                                onRemove: () => onRemoveVariablePath(parameter["variablePath"], variablePath),
                              ),
                            if (parameter["name"] != "xpath" &&
                                parameter["name"] != "parentElement" &&
                                parameter["name"] != "type" &&
                                (widget.item.type == "childElementClearAndSendFile" || widget.item.type == "clearAndSendFile"))
                              DynamicVariablePathWidget(
                                variablePath,
                                parameter["variablePath"].indexOf(variablePath),
                                [
                                  {"value": "value", "name": multiLanguageString(name: "value", defaultValue: "Value", context: context)},
                                  {"value": "variable", "name": multiLanguageString(name: "variable", defaultValue: "Variable", context: context)},
                                  {
                                    "value": "version_variable",
                                    "name": multiLanguageString(name: "version_parameter", defaultValue: "Version Parameter", context: context)
                                  },
                                  {
                                    "value": "environment_variable",
                                    "name":
                                        multiLanguageString(name: "environment_parameter", defaultValue: "Environment Parameter", context: context)
                                  },
                                  {
                                    "value": "node_variable",
                                    "name": multiLanguageString(name: "agent_parameter", defaultValue: "Agent Parameter", context: context)
                                  },
                                ],
                                const [
                                  {"value": "File", "name": "File"},
                                  {"value": "String", "name": "String"},
                                  {"value": "Null", "name": "Null"},
                                  {"value": "EmptyString", "name": "Empty String"},
                                ],
                                keyForm: _formKey,
                                callback: () {
                                  setState(() {});
                                },
                                clientName: clientName,
                                platform: widget.item.platform,
                                onRemove: () => onRemoveVariablePath(parameter["variablePath"], variablePath),
                              ),
                            if (parameter["name"] == "type") variablePathType(variablePath),
                            if (variablePath != parameter["variablePath"].last)
                              const SizedBox(
                                height: 10,
                              ),
                          ],
                        ),
                    const SizedBox(
                      height: 10,
                    ),
                    if (parameter["variablePath"] == null ||
                        parameter["variablePath"].length == 0 ||
                        parameter["variablePath"][0]["type"] == "variable" ||
                        parameter["variablePath"][0]["type"] == "version_variable" ||
                        parameter["variablePath"][0]["type"] == "environment_variable" ||
                        parameter["variablePath"][0]["type"] == "node_variable")
                      FloatingActionButton(
                        heroTag: null,
                        child: const Icon(Icons.add),
                        onPressed: () {
                          setState(() {
                            if (parameter["variablePath"] != null && parameter["variablePath"].length > 0) {
                              parameter["variablePath"].add({"type": "get"});
                            } else {
                              parameter["variablePath"] = [];
                              parameter["variablePath"].add({});
                            }
                          });
                        },
                      ),
                  ],
                ),
              ),
            ),
          ],
        ),
        if (parameter != parameterList.last)
          const SizedBox(
            height: 10,
          )
      ],
    );
  }

  Widget headerWidget() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Center(
              child: MultiLanguageText(
                name: "component_item_property",
                defaultValue: "\$0 property",
                variables: ["${widget.editorComponentItem["title"]}"],
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: context.fontSizeManager.fontSizeText20, letterSpacing: 1.5),
              ),
            ),
          ),
          TextButton(
              onPressed: () {
                Navigator.pop(context);
                widget.callback();
              },
              child: const Icon(
                Icons.close,
                color: Colors.black,
              ))
        ],
      ),
    );
  }

  checkVariableParameter(List parameterFieldListTestCase, List parameterListWidget, List newVariableReturnType, List outputFieldList) {
    var resultParameterFieldListTestCase = [];
    for (var element in parameterFieldListTestCase) {
      resultParameterFieldListTestCase.add(element);
    }
    var resultNewVariableReturnType = [];
    for (var element in newVariableReturnType) {
      resultNewVariableReturnType.add(element);
    }
    for (var parameterWidget in parameterListWidget) {
      if ((parameterWidget["variablePath"][0]["type"] == "variable"
          // ||
          //         parameterWidget["variablePath"][0]["type"] ==
          //             "version_variable"
          ) &&
          parameterWidget["variablePath"][0]["dataType"] != "WebElement" &&
          parameterWidget["variablePath"][0]["dataType"] != "CallOutput" &&
          !resultParameterFieldListTestCase
              .map((parameterFieldTestCase) => parameterFieldTestCase["name"])
              .contains(parameterWidget["variablePath"][0]["name"]) &&
          !resultNewVariableReturnType
              .map((newVariableReturnType) => newVariableReturnType["name"])
              .contains(parameterWidget["variablePath"][0]["name"]) &&
          outputFieldList.every((outputField) => outputField["name"] != parameterWidget["variablePath"][0]["name"])) {
        resultParameterFieldListTestCase
            .add({"dataType": parameterWidget["variablePath"][0]["dataType"], "name": parameterWidget["variablePath"][0]["name"]});
      }
    }
    Provider.of<TestCaseConfigModel>(context, listen: false).setParameterFieldList(resultParameterFieldListTestCase);
    Provider.of<TestCaseConfigModel>(context, listen: false).setTypeOptionEditor(true);
  }

  Widget variablePathType(variablePath) {
    return Row(
      children: [
        const Expanded(
            child: MultiLanguageText(
                name: "value",
                defaultValue: "Value",
                suffiText: " : ",
                style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
        Expanded(
          flex: 3,
          child: DynamicDropdownButton(
              value: variablePath["value"],
              hint: multiLanguageString(name: "choose_a_value", defaultValue: "Choose a value", context: context),
              items: const ["contains", "matches", "toBe"].map<DropdownMenuItem<String>>((String result) {
                return DropdownMenuItem(
                  value: result,
                  child: Text(result),
                );
              }).toList(),
              onChanged: (newValue) {
                _formKey.currentState!.validate();
                setState(() {
                  variablePath["value"] = newValue;
                });
              },
              isRequiredNotEmpty: true),
        ),
      ],
    );
  }
}
