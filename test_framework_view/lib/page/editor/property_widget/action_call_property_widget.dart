import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/editor/property_widget/dynamic_variabla_path_widget.dart';
import 'package:test_framework_view/page/editor/property_widget/find_variable_widget.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';
import 'package:test_framework_view/type.dart';
import '../../../api.dart';
import '../../../common/custom_state.dart';

class ActionCallPropertyWidget extends StatefulWidget {
  final Item item;
  final VoidCallback callback;
  final Function callbackAction;
  final bool isDragAndDrop;
  final dynamic editorComponentItem;
  const ActionCallPropertyWidget(
    this.item,
    this.callback, {
    Key? key,
    required this.isDragAndDrop,
    required this.callbackAction,
    required this.editorComponentItem,
  }) : super(key: key);

  @override
  State<ActionCallPropertyWidget> createState() => _ActionCallPropertyWidgetState();
}

class _ActionCallPropertyWidgetState extends CustomState<ActionCallPropertyWidget> {
  String? actionName;
  String? actionId;
  var parameterList = [];
  List clientListItem = [];
  String? prjProjectId;
  TextEditingController descriptionController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  var returnType = {};
  List? parameterListProject;

  @override
  void initState() {
    prjProjectId = Provider.of<NavigationModel>(context, listen: false).prjProjectId;
    getInfoPage();
    super.initState();
  }

  getInfoPage() {
    if (widget.item.type == "actionCall") {
      actionName = widget.item.actionName;
    }
    if (widget.item.type == "testCaseCall") {
      actionName = widget.item.testCaseName;
    }
    if (widget.item.type == "apiCall") {
      actionName = widget.item.apiName;
    }
    parameterList = widget.item.parameterList;
    clientListItem = widget.item.clientList;
    descriptionController.text = widget.item.description ?? "";
    if (widget.item.returnType.isEmpty) {
      returnType = {
        "type": "none",
      };
    } else {
      returnType = widget.item.returnType;
      if (returnType["type"] == "version_variable" || returnType["type"] == "environment_variable" || returnType["type"] == "node_variable") {
        getParamProject(Provider.of<NavigationModel>(context, listen: false).prjProjectId);
      }
    }
  }

  getParamProject(prjProjectId) async {
    var response = await httpGet("/test-framework-api/user/project/prj-project/$prjProjectId", context);
    if (response.containsKey("body") && response["body"] is String == false && response["body"].containsKey("result")) {
      setState(() {
        parameterListProject = response["body"]["result"]["projectConfig"]["parameterFieldList"];
      });
    }
  }

  getParameterList(id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-case/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        actionName = response["body"]["result"]["name"];
        parameterList = [];
        if (response["body"]["result"]["content"] != null) {
          parameterList = response["body"]["result"]["content"]["parameterFieldList"] ?? [];
          clientListItem = response["body"]["result"]["content"]["clientList"] ?? [];
          for (var parameter in parameterList) {
            parameter["variablePath"] = [
              {"type": "variable", "dataType": parameter["dataType"], "name": parameter["name"]}
            ];
          }
        }
      });
    }
  }

  @override
  void dispose() {
    descriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<TestCaseConfigModel>(builder: (context, testCaseConfigModel, child) {
      return Form(
        key: _formKey,
        child: Column(
          children: [
            headerWidget(),
            Expanded(
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                  child: AbsorbPointer(
                    absorbing: !widget.isDragAndDrop,
                    child: Column(
                      children: [
                        Row(
                          children: [
                            const Expanded(
                                child: MultiLanguageText(
                                    name: "code",
                                    defaultValue: "Code",
                                    suffiText: " : ",
                                    style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
                            Expanded(
                              flex: 3,
                              child: Container(
                                decoration: BoxDecoration(border: Border.all(color: const Color.fromRGBO(216, 218, 229, 1))),
                                child: TextField(
                                  controller: TextEditingController(text: actionName),
                                  decoration: InputDecoration(
                                    hintText: (widget.item.type == "actionCall")
                                        ? multiLanguageString(name: "choose_a_action", defaultValue: "Choose a action", context: context)
                                        : (widget.item.type == "testCaseCall")
                                            ? multiLanguageString(
                                                name: "select_test_scenario", defaultValue: "Select test scenario", context: context)
                                            : multiLanguageString(name: "select_test_api", defaultValue: "Select test api", context: context),
                                    hoverColor: Colors.transparent,
                                    border: const OutlineInputBorder(
                                        borderSide: BorderSide(
                                      color: Color.fromARGB(255, 199, 199, 199),
                                    )),
                                  ),
                                  readOnly: true,
                                  onTap: () {
                                    Navigator.of(context).push(
                                      createRoute(
                                        FindTestCaseWidget(
                                          type: widget.item.type == "actionCall"
                                              ? "action"
                                              : widget.item.type == "testCaseCall"
                                                  ? "test_scenario"
                                                  : "test_api",
                                          callback: (valueCallback) {
                                            setState(() {
                                              actionName = valueCallback["name"];
                                              actionId = valueCallback["id"];
                                              getParameterList(valueCallback["id"]);
                                            });
                                          },
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            const Expanded(
                                child: MultiLanguageText(
                                    name: "step_comment",
                                    defaultValue: "Step Comment",
                                    suffiText: " : ",
                                    style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
                            Expanded(
                              flex: 3,
                              child: SizedBox(
                                child: DynamicTextField(
                                  controller: descriptionController,
                                  hintText: multiLanguageString(name: "enter_a_description", defaultValue: "Enter a description", context: context),
                                  maxline: 3,
                                  minline: 1,
                                ),
                              ),
                            ),
                          ],
                        ),
                        if (clientListItem.isNotEmpty) const SizedBox(height: 20),
                        if (clientListItem.isNotEmpty) clientListWidget(testCaseConfigModel),
                        const SizedBox(height: 20),
                        if (parameterList.isNotEmpty) parameterListWidget(),
                        if (actionName != null && actionName!.isNotEmpty) returnTypeWidget(),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 20),
            if (widget.isDragAndDrop) btnWidget(testCaseConfigModel),
            if (widget.isDragAndDrop) const SizedBox(height: 20),
          ],
        ),
      );
    });
  }

  Widget returnTypeWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const MultiLanguageText(
              name: "output", defaultValue: "Output", style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromARGB(255, 35, 67, 172))),
          const SizedBox(
            height: 5,
          ),
          Row(
            children: [
              Expanded(
                child: Container(
                  padding: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    border: Border.all(color: const Color.fromRGBO(216, 218, 229, 1)),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          const Expanded(
                              child: MultiLanguageText(
                                  name: "type",
                                  defaultValue: "Type",
                                  suffiText: " : ",
                                  style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
                          Expanded(
                            flex: 3,
                            child: DynamicDropdownButton(
                                value: returnType["type"],
                                hint: multiLanguageString(name: "choose_a_type", defaultValue: "Choose a type", context: context),
                                items: const [
                                  "none",
                                  "variable",
                                  "newVariable",
                                  "version_variable",
                                  "environment_variable",
                                  "node_variable",
                                ].map<DropdownMenuItem<String>>((String result) {
                                  return DropdownMenuItem(
                                    value: result,
                                    child: Text(result),
                                  );
                                }).toList(),
                                onChanged: (newValue) async {
                                  _formKey.currentState!.validate();
                                  returnType["type"] = newValue;
                                  if (returnType["type"] == "version_variable" ||
                                      returnType["type"] == "environment_variable" ||
                                      returnType["type"] == "node_variable") {
                                    returnType["name"] = null;
                                    await getParamProject(Provider.of<NavigationModel>(context, listen: false).prjProjectId);
                                  }
                                  if (returnType["type"] == "none") {
                                    returnType = {
                                      "type": "none",
                                    };
                                  }
                                  if (returnType["type"] == "newVariable" && returnType.containsKey("dataType")) {
                                    returnType.remove("dataType");
                                  }
                                  if (returnType["type"] != "none" && returnType["dataType"] == null) {
                                    returnType["dataType"] = "CallOutput";
                                  }
                                  setState(() {});
                                },
                                isRequiredNotEmpty: true),
                          ),
                        ],
                      ),
                      if (returnType["type"] != "none")
                        Column(
                          children: [
                            const SizedBox(
                              height: 20,
                            ),
                            Row(
                              children: [
                                const Expanded(
                                    child: MultiLanguageText(
                                        name: "data_type",
                                        defaultValue: "Data Type",
                                        suffiText: " : ",
                                        style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
                                Expanded(
                                    flex: 3,
                                    child: DynamicDropdownButton(
                                        value: returnType["dataType"],
                                        hint: multiLanguageString(name: "choose_a_data_type", defaultValue: "Choose a dataType", context: context),
                                        items: dataTypeList.map<DropdownMenuItem<String>>((dynamic result) {
                                          return DropdownMenuItem(
                                            value: result["value"],
                                            child: Text(result["name"]),
                                          );
                                        }).toList(),
                                        onChanged: (newValue) {
                                          _formKey.currentState!.validate();
                                          setState(() {
                                            returnType["dataType"] = newValue;
                                          });
                                        },
                                        isRequiredNotEmpty: true)),
                              ],
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            ((returnType["type"] == "version_variable" ||
                                        returnType["type"] == "environment_variable" ||
                                        returnType["type"] == "node_variable") &&
                                    parameterListProject != null &&
                                    parameterListProject!.isNotEmpty)
                                ? Row(
                                    children: [
                                      const Expanded(
                                          child: MultiLanguageText(
                                              name: "name",
                                              defaultValue: "Name",
                                              suffiText: " : ",
                                              style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
                                      Expanded(
                                        flex: 3,
                                        child: DynamicDropdownButton(
                                            value: returnType["name"],
                                            hint: multiLanguageString(name: "choose_a_name", defaultValue: "Choose a name", context: context),
                                            items: parameterListProject!
                                                .where((element) => element["variableType"] == returnType["type"])
                                                .map<DropdownMenuItem<String>>((dynamic result) {
                                              return DropdownMenuItem(
                                                value: result["name"],
                                                child: Text(result["name"]),
                                              );
                                            }).toList(),
                                            onChanged: (newValue) {
                                              _formKey.currentState!.validate();
                                              setState(() {
                                                returnType["name"] = newValue;
                                              });
                                            },
                                            isRequiredNotEmpty: true),
                                      ),
                                    ],
                                  )
                                : Row(
                                    children: [
                                      const Expanded(
                                          child: MultiLanguageText(
                                              name: "name",
                                              defaultValue: "Name",
                                              suffiText: " : ",
                                              style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
                                      Expanded(
                                        flex: 3,
                                        child: Row(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Expanded(
                                              child: DynamicTextField(
                                                controller: TextEditingController(text: returnType["name"] ?? ""),
                                                hintText: multiLanguageString(name: "enter_a_name", defaultValue: "Enter a name", context: context),
                                                onChanged: (text) {
                                                  _formKey.currentState!.validate();
                                                  returnType["name"] = text;
                                                },
                                                isRequiredNotEmpty: true,
                                                isRequiredRegex: true,
                                                obscureText: returnType["dataType"] == "Password",
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                            if (returnType["type"] == "variable")
                              const SizedBox(
                                height: 10,
                              ),
                            if (returnType["type"] == "variable")
                              Row(
                                children: [
                                  const Expanded(
                                    child: SizedBox.shrink(),
                                  ),
                                  Expanded(
                                    flex: 3,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        ElevatedButton(
                                            style: ElevatedButton.styleFrom(fixedSize: const Size(130, 50)),
                                            onPressed: () {
                                              Navigator.of(context).push(
                                                createRoute(
                                                  FindVariableWidget((valueCallback) {
                                                    setState(() {
                                                      returnType["name"] = valueCallback["name"];
                                                      returnType["dataType"] = valueCallback["dataType"];
                                                    });
                                                  }),
                                                ),
                                              );
                                            },
                                            child: const MultiLanguageText(name: "find_variable", defaultValue: "Find variable")),
                                      ],
                                    ),
                                  ),
                                ],
                              )
                          ],
                        )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget parameterListWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 10,
        ),
        const MultiLanguageText(
            name: "input", defaultValue: "Input", style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromARGB(255, 35, 67, 172))),
        Container(
          padding: const EdgeInsets.all(10),
          margin: const EdgeInsets.only(top: 5),
          decoration: BoxDecoration(
            border: Border.all(color: const Color.fromRGBO(216, 218, 229, 1)),
            borderRadius: BorderRadius.circular(10),
          ),
          child: Column(
            children: [
              for (var parameter in parameterList)
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(parameter["name"], style: const TextStyle(fontWeight: FontWeight.bold, color: Color.fromARGB(255, 35, 67, 172))),
                    const SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: Container(
                            margin: parameter != parameterList.last ? const EdgeInsets.only(bottom: 10) : null,
                            padding: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              border: Border.all(color: const Color.fromRGBO(216, 218, 229, 1)),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Column(
                              children: [
                                for (var variablePath in parameter["variablePath"]) variablePathWidget(parameter, variablePath),
                                const SizedBox(
                                  height: 10,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    if (parameter != parameterList.last)
                      const SizedBox(
                        height: 10,
                      )
                  ],
                )
            ],
          ),
        ),
      ],
    );
  }

  Widget variablePathWidget(parameter, variablePath) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        DynamicVariablePathWidget(
          variablePath,
          parameter["variablePath"].indexOf(variablePath),
          [
            {"value": "value", "name": multiLanguageString(name: "value", defaultValue: "Value", context: context)},
            {"value": "variable", "name": multiLanguageString(name: "variable", defaultValue: "Variable", context: context)},
            {
              "value": "version_variable",
              "name": multiLanguageString(name: "version_parameter", defaultValue: "Version Parameter", context: context)
            },
            {
              "value": "environment_variable",
              "name": multiLanguageString(name: "environment_parameter", defaultValue: "Environment Parameter", context: context)
            },
            {"value": "node_variable", "name": multiLanguageString(name: "agent_parameter", defaultValue: "Agent Parameter", context: context)},
          ],
          dataTypeList,
          keyForm: _formKey,
          callback: () {
            setState(() {});
          },
        ),
        if (variablePath != parameter["variablePath"].last)
          const SizedBox(
            height: 20,
          ),
      ],
    );
  }

  Widget clientListWidget(TestCaseConfigModel testCaseConfigModel) {
    return Column(
      children: [
        for (var client in clientListItem)
          Padding(
            padding: EdgeInsets.only(bottom: clientListItem.last != client ? 20 : 0),
            child: DynamicDropdownButton(
                value: client["clientName"],
                hint: multiLanguageString(
                    name: "choose_a_client_for", defaultValue: "Choose a client for \$0", variables: ["${client["name"]}"], context: context),
                items: testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"].map<DropdownMenuItem>((result) {
                  return DropdownMenuItem(
                    value: result["name"],
                    child: Text(result["name"]),
                  );
                }).toList(),
                labelText: client["name"],
                onChanged: (newValue) async {
                  _formKey.currentState!.validate();
                  client["clientName"] = newValue;
                },
                isRequiredNotEmpty: true),
          ),
      ],
    );
  }

  Widget btnWidget(TestCaseConfigModel testCaseConfigModel) {
    return DynamicButton(
      name: "close",
      label: multiLanguageString(name: "close", defaultValue: "Close", isLowerCase: false, context: context),
      onPressed: () {
        if (_formKey.currentState!.validate()) {
          if (returnType["type"] == "newVariable") {
            if (!testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["newVariableList"]
                .map((newVariable) => newVariable["name"])
                .contains(returnType["name"])) {
              Map<dynamic, dynamic> value = Map.from(returnType);
              value["parentId"] = widget.item.id;
              Provider.of<TestCaseConfigModel>(context, listen: false).addNewVariableList(value);
            }
          }
          checkVariableParameter(
              testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["parameterFieldList"],
              parameterList,
              testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["newVariableList"],
              testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["outputFieldList"]);
          if (actionName != null) {
            if (widget.item.type == "actionCall") {
              widget.item.actionName = actionName;
            } else if (widget.item.type == "testCaseCall") {
              widget.item.testCaseName = actionName;
            } else {
              widget.item.apiName = actionName;
            }
          }
          widget.item.clientList = clientListItem;
          widget.item.parameterList = parameterList;
          widget.item.description = descriptionController.text;
          widget.item.returnType = returnType;
          Navigator.pop(context);
          widget.callback();
          widget.callbackAction(actionId);
        }
      },
    );
  }

  Widget headerWidget() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
      child: Row(
        children: [
          Expanded(
            child: Center(
              child: MultiLanguageText(
                name: "component_item_property",
                defaultValue: "\$0 property",
                variables: ["${widget.editorComponentItem["title"]}"],
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: context.fontSizeManager.fontSizeText20, letterSpacing: 1.5),
              ),
            ),
          ),
          TextButton(
              onPressed: () {
                Navigator.pop(context);
                widget.callback();
              },
              child: const Icon(
                Icons.close,
                color: Colors.black,
              ))
        ],
      ),
    );
  }

  checkVariableParameter(List parameterFieldListTestCase, List parameterListWidget, List newVariableReturnType, List outputFieldList) {
    var resultParameterFieldListTestCase = [];
    for (var element in parameterFieldListTestCase) {
      resultParameterFieldListTestCase.add(element);
    }
    var resultNewVariableReturnType = [];
    for (var element in newVariableReturnType) {
      resultNewVariableReturnType.add(element);
    }
    for (var parameterWidget in parameterListWidget) {
      if ((parameterWidget["variablePath"][0]["type"] == "variable"
          // ||
          //         parameterWidget["variablePath"][0]["type"] ==
          //             "version_variable"
          ) &&
          parameterWidget["variablePath"][0]["dataType"] != "WebElement" &&
          parameterWidget["variablePath"][0]["dataType"] != "CallOutput" &&
          !resultParameterFieldListTestCase
              .map((parameterFieldTestCase) => parameterFieldTestCase["name"])
              .contains(parameterWidget["variablePath"][0]["name"]) &&
          !resultNewVariableReturnType
              .map((newVariableReturnType) => newVariableReturnType["name"])
              .contains(parameterWidget["variablePath"][0]["name"]) &&
          outputFieldList.every((outputField) => outputField["name"] != parameterWidget["variablePath"][0]["name"])) {
        resultParameterFieldListTestCase.add({
          "dataType": parameterWidget["variablePath"][0]["dataType"],
          "name": parameterWidget["variablePath"][0]["name"],
          if (parameterWidget["variablePath"][0]["dataType"] == "RandomValue") "tfRandomTypeId": parameterWidget["tfRandomTypeId"]
        });
      }
    }
    Provider.of<TestCaseConfigModel>(context, listen: false).setParameterFieldList(resultParameterFieldListTestCase);
    Provider.of<TestCaseConfigModel>(context, listen: false).setTypeOptionEditor(true);
  }
}

class FindTestCaseWidget extends StatefulWidget {
  final Function callback;
  final String type;
  const FindTestCaseWidget({Key? key, required this.callback, required this.type}) : super(key: key);

  @override
  State<FindTestCaseWidget> createState() => _FindTestCaseWidgetState();
}

class _FindTestCaseWidgetState extends CustomState<FindTestCaseWidget> {
  bool isChangeWebElement = false;
  var listSearchResponse = [];
  TextEditingController nameController = TextEditingController();
  var rowCountListTestCase = 0;
  var currentPageTestCase = 1;
  var rowPerPageTestCase = 10;
  var searchRequest = {};
  late Future futureListTfTestCase;

  getListTfTestCase(page) async {
    if ((page - 1) * rowPerPageTestCase > rowCountListTestCase) {
      page = (1.0 * rowCountListTestCase / rowPerPageTestCase).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {
      "queryOffset": (page - 1) * rowPerPageTestCase,
      "queryLimit": rowPerPageTestCase,
      "prjProjectId": Provider.of<NavigationModel>(context, listen: false).prjProjectId,
      "testCaseType": widget.type,
    };
    if (searchRequest["titleLike"] != null && searchRequest["titleLike"].isNotEmpty) {
      findRequest["titleLike"] = "%${nameController.text}%";
    }
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-case/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPageTestCase = page;
        rowCountListTestCase = response["body"]["rowCount"];
        listSearchResponse = response["body"]["resultList"];
      });
    }
  }

  search(searchRequest) {
    this.searchRequest = searchRequest;
    futureListTfTestCase = getListTfTestCase(currentPageTestCase);
  }

  @override
  void initState() {
    search({});
    super.initState();
  }

  @override
  void dispose() {
    nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return TestFrameworkRootPageWidget(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 20, 0, 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              headerWidget(),
              const SizedBox(
                height: 20,
              ),
              searchFormWidget(),
              const SizedBox(
                height: 20,
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 50),
                    child: Column(
                      children: [
                        if (listSearchResponse.isNotEmpty) tableTestCaseWidget(),
                      ],
                    ),
                  ),
                ),
              ),
              if (isChangeWebElement && listSearchResponse.isEmpty)
                Container(
                  margin: const EdgeInsets.only(top: 20),
                  child: Align(
                    alignment: Alignment.center,
                    child: MultiLanguageText(
                      name: "not_found_element",
                      defaultValue: "Not found element",
                      style: TextStyle(
                        fontSize: context.fontSizeManager.fontSizeText16,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1.5,
                      ),
                    ),
                  ),
                ),
              const SizedBox(height: 20),
              const Center(child: ButtonCancel()),
              const SizedBox(height: 20),
            ],
          ),
        ),
      );
    });
  }

  Widget tableTestCaseWidget() {
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.only(top: 10),
          child: CustomDataTableWidget(
            columns: [
              Expanded(
                  flex: 2,
                  child: widget.type == "test_scenario"
                      ? MultiLanguageText(
                          name: "test_scenario_code",
                          defaultValue: "Test Scenario Code",
                          isLowerCase: false,
                          style: Style(context).styleTextDataColumn,
                        )
                      : MultiLanguageText(
                          name: "action_code",
                          defaultValue: "Action Code",
                          isLowerCase: false,
                          style: Style(context).styleTextDataColumn,
                        )),
              Expanded(
                  flex: 2,
                  child: widget.type == "test_scenario"
                      ? MultiLanguageText(
                          name: "test_scenario_name",
                          defaultValue: "Test Scenario Name",
                          isLowerCase: false,
                          style: Style(context).styleTextDataColumn,
                        )
                      : MultiLanguageText(
                          name: "action_name",
                          defaultValue: "Action Name",
                          isLowerCase: false,
                          style: Style(context).styleTextDataColumn,
                        )),
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "description",
                  defaultValue: "Description",
                  isLowerCase: false,
                  style: Style(context).styleTextDataColumn,
                ),
              ),
              Expanded(
                child: MultiLanguageText(
                  name: "owner",
                  defaultValue: "Owner",
                  isLowerCase: false,
                  style: Style(context).styleTextDataColumn,
                ),
              ),
            ],
            rows: [
              for (var resultTestCase in listSearchResponse)
                CustomDataRow(
                  cells: [
                    Expanded(
                      flex: 2,
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Row(
                          children: [
                            const Icon(Icons.description, color: Color.fromRGBO(61, 148, 254, 1)),
                            const SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: Text(
                                resultTestCase["name"] ?? "",
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                                style: Style(context).styleTextDataCell,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Row(
                          children: [
                            const Icon(Icons.description, color: Color.fromRGBO(61, 148, 254, 1)),
                            const SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: Text(
                                resultTestCase["title"] ?? "",
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                                style: Style(context).styleTextDataCell,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Text(
                          resultTestCase["description"] ?? "",
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: Style(context).styleTextDataCell,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Text(
                          resultTestCase["sysUser"]["fullname"],
                          style: Style(context).styleTextDataCell,
                        ),
                      ),
                    ),
                  ],
                  onSelectChanged: () {
                    widget.callback(resultTestCase);
                    Navigator.pop(context);
                  },
                ),
            ],
          ),
        ),
        DynamicTablePagging(
          rowCountListTestCase,
          currentPageTestCase,
          rowPerPageTestCase,
          pageChangeHandler: (page) {
            setState(() {
              futureListTfTestCase = getListTfTestCase(page);
            });
          },
          rowPerPageChangeHandler: (rowPerPage) {
            setState(() {
              rowPerPageTestCase = rowPerPage!;
              futureListTfTestCase = getListTfTestCase(1);
            });
          },
        ),
      ],
    );
  }

  Widget searchFormWidget() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 50),
          child: DynamicTextField(
            controller: nameController,
            hintText: multiLanguageString(name: "enter_a_name", defaultValue: "Enter a name", context: context),
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        Center(
          child: ElevatedButton.icon(
            style: ButtonStyle(
              padding: MaterialStateProperty.all<EdgeInsets>(const EdgeInsets.all(15)),
            ),
            onPressed: () async {
              await search({"titleLike": nameController.text});
              setState(() {
                isChangeWebElement = true;
              });
            },
            icon: const Icon(Icons.search),
            label: const MultiLanguageText(name: "search", defaultValue: "Search", isLowerCase: false),
          ),
        ),
      ],
    );
  }

  Widget headerWidget() {
    return Row(
      children: [
        Expanded(
          child: Center(
            child: (widget.type == "action")
                ? MultiLanguageText(
                    name: "find_action_property",
                    defaultValue: "Find action property",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: context.fontSizeManager.fontSizeText20, letterSpacing: 1.5))
                : MultiLanguageText(
                    name: "find_test_scenario_property",
                    defaultValue: "Find test scenario property",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: context.fontSizeManager.fontSizeText20, letterSpacing: 1.5)),
          ),
        ),
        TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Icon(
              Icons.close,
              color: Colors.black,
            ))
      ],
    );
  }
}
