import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';

class FindVariableWidget extends StatefulWidget {
  final Function callback;
  const FindVariableWidget(this.callback, {Key? key}) : super(key: key);

  @override
  State<FindVariableWidget> createState() => _FindVariableWidgetState();
}

class _FindVariableWidgetState extends State<FindVariableWidget> {
  bool isShowFilterForm = true;
  final TextEditingController nameController = TextEditingController();
  List searchList = [];
  List resultList = [];
  Timer? _debounceTimer;

  @override
  void initState() {
    super.initState();
    setInitPage();
  }

  setInitPage() {
    TestCaseConfigModel testCaseConfigModel = Provider.of<TestCaseConfigModel>(context, listen: false);
    List newVariableList = (testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["newVariableList"] ?? []);
    List parameterFieldList = (testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["parameterFieldList"] ?? []);
    List outputFieldList = (testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["outputFieldList"] ?? []);
    if (newVariableList.isNotEmpty) {
      searchList.addAll(newVariableList);
    }
    if (parameterFieldList.isNotEmpty) {
      searchList.addAll(parameterFieldList);
    }
    if (outputFieldList.isNotEmpty) {
      searchList.addAll(outputFieldList);
    }
    setState(() {
      resultList = searchList;
    });
  }

  @override
  void dispose() {
    _debounceTimer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return TestFrameworkRootPageWidget(
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Center(
                          child: MultiLanguageText(
                            name: "find_variable",
                            defaultValue: "Find variable",
                            style: TextStyle(fontWeight: FontWeight.bold, fontSize: context.fontSizeManager.fontSizeText20, letterSpacing: 1.5),
                          ),
                        ),
                      ),
                      TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: const Icon(
                            Icons.close,
                            color: Colors.black,
                          ))
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      MultiLanguageText(
                        name: "filter_form",
                        defaultValue: "Filter form",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: const Color.fromRGBO(0, 0, 0, 0.8),
                            fontSize: context.fontSizeManager.fontSizeText16,
                            letterSpacing: 2),
                      ),
                      IconButton(
                        onPressed: () {
                          setState(() {
                            isShowFilterForm = !isShowFilterForm;
                          });
                        },
                        icon: isShowFilterForm ? const Icon(Icons.visibility) : const Icon(Icons.visibility_off),
                        splashRadius: 1,
                        tooltip: isShowFilterForm
                            ? multiLanguageString(name: "hide_filter_form", defaultValue: "Hide filter form", context: context)
                            : multiLanguageString(name: "show_filter_form", defaultValue: "Show filter form", context: context),
                      )
                    ],
                  ),
                  if (isShowFilterForm)
                    DynamicTextField(
                      controller: nameController,
                      onChanged: (value) {
                        setState(() {
                          nameController.text;
                        });
                        if (_debounceTimer?.isActive ?? false) {
                          _debounceTimer?.cancel();
                        }

                        _debounceTimer = Timer(const Duration(milliseconds: 500), () {
                          setState(() {
                            resultList = searchList.where((element) => element["name"].contains(nameController.text)).toList();
                          });
                        });
                      },
                      labelText: multiLanguageString(name: "variable_name", defaultValue: "Variable name", context: context),
                      hintText: multiLanguageString(name: "by_name", defaultValue: "By name", context: context),
                      suffixIcon: (nameController.text.isNotEmpty)
                          ? IconButton(
                              onPressed: () {
                                nameController.clear();
                                setState(() {
                                  resultList = searchList.where((element) => element["name"].contains(nameController.text)).toList();
                                });
                              },
                              icon: const Icon(Icons.clear),
                              splashRadius: 20,
                            )
                          : null,
                    ),
                  // const SizedBox(
                  //   height: 10,
                  // ),
                  // if (isShowFilterForm)
                  //   Row(
                  //     mainAxisAlignment: MainAxisAlignment.start,
                  //     children: [
                  //       Padding(
                  //         padding: const EdgeInsets.only(right: 20),
                  //         child: ElevatedButton(
                  //             onPressed: () {
                  //               // handleCallBackSearchFunction();
                  //               setState(() {
                  //                 resultList = searchList.where((element) => element["name"].contains(nameController.text)).toList();
                  //               });
                  //             },
                  //             child: const Text("SEARCH")),
                  //       ),
                  //       ElevatedButton(
                  //           onPressed: () {
                  //             setState(() {
                  //               resultList = searchList;
                  //             });
                  //           },
                  //           child: const Text("RESET"))
                  //     ],
                  //   ),
                  const SizedBox(
                    height: 30,
                  ),
                  (resultList.isEmpty)
                      ? Container(
                          margin: const EdgeInsets.only(top: 20),
                          child: Align(
                            alignment: Alignment.center,
                            child: MultiLanguageText(
                              name: "not_found_element",
                              defaultValue: "Not found element",
                              style: TextStyle(
                                fontSize: context.fontSizeManager.fontSizeText16,
                                fontWeight: FontWeight.bold,
                                letterSpacing: 1.5,
                              ),
                            ),
                          ),
                        )
                      : SingleChildScrollView(
                          child: Row(
                            children: [
                              Expanded(
                                child: DataTable(
                                  showCheckboxColumn: false,
                                  border: TableBorder(
                                      verticalInside: Style(context).styleBorderTable,
                                      horizontalInside: Style(context).styleBorderTable,
                                      bottom: Style(context).styleBorderTable,
                                      left: Style(context).styleBorderTable,
                                      right: Style(context).styleBorderTable,
                                      top: Style(context).styleBorderTable),
                                  columns: [
                                    DataColumn(
                                      label: Expanded(
                                        child: MultiLanguageText(
                                          name: "name",
                                          defaultValue: "Name",
                                          isLowerCase: false,
                                          style: Style(context).styleTextDataColumn,
                                          align: TextAlign.center,
                                        ),
                                      ),
                                    ),
                                    DataColumn(
                                      label: Expanded(
                                        flex: 2,
                                        child: MultiLanguageText(
                                          name: "data_type",
                                          defaultValue: "Data Type",
                                          isLowerCase: false,
                                          style: Style(context).styleTextDataColumn,
                                          align: TextAlign.center,
                                        ),
                                      ),
                                    ),
                                  ],
                                  rows: [
                                    for (var item in resultList)
                                      DataRow(
                                        onSelectChanged: (value) {
                                          Navigator.pop(context);
                                          widget.callback(item);
                                        },
                                        cells: [
                                          DataCell(
                                            Text(item["name"] ?? ""),
                                          ),
                                          DataCell(
                                            Text(item["dataType"] ?? ""),
                                          ),
                                        ],
                                      ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                  const SizedBox(height: 20),
                  const Center(child: ButtonCancel()),
                  const SizedBox(height: 20),
                ],
              ),
            ),
          ],
        ),
      );
    });
  }
}
