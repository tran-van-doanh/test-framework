import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/editor/property_widget/action_call_property_widget.dart';
import 'package:test_framework_view/page/editor/property_widget/action_element_property_widget.dart';
import 'package:test_framework_view/page/editor/property_widget/client_property_widget.dart';
import 'package:test_framework_view/page/editor/property_widget/new_var_and_for_each_property_widget.dart';
import 'package:test_framework_view/page/editor/property_widget/sleep_property_widget.dart';
import 'package:test_framework_view/page/editor/property_widget/variable_condition_property_widget.dart';
import 'package:test_framework_view/page/editor/property_widget/variable_default_property_widget.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';
import 'package:test_framework_view/type.dart';

class PropertyPageWidget extends StatefulWidget {
  final Item item;
  final Function callback;
  final Function? callbackAction;
  final bool isDragAndDrop;
  const PropertyPageWidget({Key? key, required this.item, required this.callback, required this.isDragAndDrop, this.callbackAction})
      : super(key: key);

  @override
  State<PropertyPageWidget> createState() => _PropertyPageWidgetState();
}

class _PropertyPageWidgetState extends State<PropertyPageWidget> {
  Widget propertyBodyWidget = Container();
  dynamic editorComponentItem;
  bool isLoad = false;
  @override
  void initState() {
    super.initState();
    getInitPage();
  }

  getInitPage() async {
    await getEditorComponent();
    setState(() {
      isLoad = true;
    });
  }

  getEditorComponent() async {
    var findRequest = {"status": 1, "type": widget.item.type};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-editor-component/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false && response["body"]["resultList"].isNotEmpty && mounted) {
      setState(() {
        editorComponentItem = response["body"]["resultList"][0];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      if (isLoad) {
        if (widget.item.type == "actionCall" || widget.item.type == "testCaseCall" || widget.item.type == "apiCall") {
          propertyBodyWidget = ActionCallPropertyWidget(
            widget.item,
            () {
              widget.callback();
            },
            editorComponentItem: editorComponentItem,
            callbackAction: (actionId) {
              if (widget.callbackAction != null) widget.callbackAction!(actionId);
            },
            isDragAndDrop: widget.isDragAndDrop,
          );
        }
        if (widget.item.type == "sendKeys" ||
            widget.item.type == "childElementSendKeys" ||
            widget.item.type == "clearAndSendKeys" ||
            widget.item.type == "childElementClearAndSendKeys" ||
            widget.item.type == "sendKeyFromElement" ||
            widget.item.type == "childElementSendKeyFromElement" ||
            widget.item.type == "sendKeyWithoutElement" ||
            widget.item.type == "sendKeysWithoutElement" ||
            widget.item.type == "clearAndSendFile" ||
            widget.item.type == "childElementClearAndSendFile" ||
            widget.item.type == "startApplication" ||
            widget.item.type == "installApplication" ||
            widget.item.type == "mouseAction" ||
            widget.item.type == "mouseActionWithOffset" ||
            widget.item.type == "click" ||
            widget.item.type == "switchToFrame" ||
            widget.item.type == "clickByPosition" ||
            widget.item.type == "childElementClick" ||
            widget.item.type == "selectTableRow" ||
            widget.item.type == "selectByValue" ||
            widget.item.type == "getSelectedValue" ||
            widget.item.type == "getSelectOptionsValue" ||
            widget.item.type == "getSelectVisibleOptionsValue" ||
            widget.item.type == "getSelectedVisibleValue" ||
            widget.item.type == "childElementSelectByValue" ||
            widget.item.type == "selectByVisibleText" ||
            widget.item.type == "childElementSelectByVisibleText" ||
            widget.item.type == "goTo" ||
            widget.item.type == "getUrl" ||
            widget.item.type == "findElements" ||
            widget.item.type == "childElementFindElements" ||
            widget.item.type == "findElement" ||
            widget.item.type == "findElementByPosition" ||
            widget.item.type == "sendKeysByPosition" ||
            widget.item.type == "clearAndSendKeysByPosition" ||
            widget.item.type == "selectByValueByPosition" ||
            widget.item.type == "selectByVisibleTextByPosition" ||
            widget.item.type == "findElementsByPosition" ||
            widget.item.type == "childElementFindElement" ||
            widget.item.type == "getText" ||
            widget.item.type == "childElementGetText" ||
            widget.item.type == "getAttribute" ||
            widget.item.type == "childElementGetAttribute" ||
            widget.item.type == "executeScript" ||
            widget.item.type == "executeScriptOnElement" ||
            widget.item.type == "waitUrl" ||
            widget.item.type == "waitElementExists" ||
            widget.item.type == "waitElementDisappear" ||
            widget.item.type == "waitRepositoryElementDisappear" ||
            widget.item.type == "waitRepositoryElementExists" ||
            widget.item.type == "childElementWaitElement" ||
            widget.item.type == "clickWithOffset" ||
            widget.item.type == "childElementClickWithOffset" ||
            widget.item.type == "getAlertText" ||
            widget.item.type == "sendKeysAlert" ||
            widget.item.type == "getWindowHandle" ||
            widget.item.type == "getWindowHandles" ||
            widget.item.type == "selectWindow" ||
            widget.item.type == "switchToWindow" ||
            widget.item.type == "replaceAll" ||
            widget.item.type == "substring" ||
            widget.item.type == "split" ||
            widget.item.type == "format" ||
            widget.item.type == "datePlus" ||
            widget.item.type == "dateMinus" ||
            widget.item.type == "parseInteger" ||
            widget.item.type == "parseLong" ||
            widget.item.type == "parseDouble" ||
            widget.item.type == "parseFloat" ||
            widget.item.type == "parseDate" ||
            widget.item.type == "parseDateTime" ||
            widget.item.type == "checkWindowExists" ||
            widget.item.type == "checkElementExists" ||
            widget.item.type == "childElementCheckElementExists" ||
            widget.item.type == "checkElementEnabled" ||
            widget.item.type == "childElementCheckElementEnabled" ||
            widget.item.type == "isEmpty" ||
            widget.item.type == "isNotEmpty" ||
            widget.item.type == "isNull" ||
            widget.item.type == "isNotNull" ||
            widget.item.type == "ftpUpload" ||
            widget.item.type == "ftpDownload" ||
            widget.item.type == "ftpWaitFile" ||
            widget.item.type == "sftpUpload" ||
            widget.item.type == "sftpDownload" ||
            widget.item.type == "sftpWaitFile" ||
            widget.item.type == "textFileRead" ||
            widget.item.type == "textFileWrite" ||
            widget.item.type == "excelFileRead" ||
            widget.item.type == "excelFileWrite" ||
            widget.item.type == "jsonFileRead" ||
            widget.item.type == "jsonFileWrite" ||
            widget.item.type == "assertEquals" ||
            widget.item.type == "assertNull" ||
            widget.item.type == "assertNotNull" ||
            widget.item.type == "assertTrue" ||
            widget.item.type == "assertFalse") {
          propertyBodyWidget = ActionElement(
            widget.item,
            () {
              widget.callback();
            },
            editorComponentItem: editorComponentItem,
            isDragAndDrop: widget.isDragAndDrop,
          );
        }
        if (widget.item.type == "error" ||
            widget.item.type == "finish" ||
            widget.item.type == "comment" ||
            widget.item.type == "group" ||
            widget.item.type == "step") {
          propertyBodyWidget = SleepPropertyWidget(
            widget.item,
            () {
              widget.callback();
            },
            isDragAndDrop: widget.isDragAndDrop,
            editorComponentItem: editorComponentItem,
          );
        }
        if (widget.item.type == "client") {
          propertyBodyWidget = ClientPropertyWidget(
            widget.item,
            () {
              widget.callback();
            },
            isDragAndDrop: widget.isDragAndDrop,
            editorComponentItem: editorComponentItem,
          );
        }
        if (widget.item.type == "newVariable" || widget.item.type == "assignValue" || widget.item.type == "forEach") {
          propertyBodyWidget = NewVarAndForEachPropertyWidget(
            widget.item,
            () {
              widget.callback();
            },
            isDragAndDrop: widget.isDragAndDrop,
            editorComponentItem: editorComponentItem,
          );
        }
        if (widget.item.type == "variable_default") {
          propertyBodyWidget = VariableDefaultProperty(
            variablePath: widget.item.variablePath,
            callback: (variablePathList) {
              widget.callback(variablePathList);
            },
            isDragAndDrop: widget.isDragAndDrop,
          );
        }
        if ((widget.item.category == "condition" || widget.item.category == "variable" || widget.item.category == "conditionGroup") &&
            widget.item.type != "checkWindowExists" &&
            widget.item.type != "checkElementExists" &&
            widget.item.type != "childElementCheckElementExists" &&
            widget.item.type != "checkElementEnabled" &&
            widget.item.type != "childElementCheckElementEnabled" &&
            widget.item.type != "isEmpty" &&
            widget.item.type != "isNotEmpty" &&
            widget.item.type != "isNull" &&
            widget.item.type != "isNotNull") {
          propertyBodyWidget = VariableConditionPropertyWidget(
            widget.item,
            () {
              widget.callback();
            },
            isDragAndDrop: widget.isDragAndDrop,
          );
        }
        return TestFrameworkRootPageWidget(child: propertyBodyWidget);
      }
      return const Center(
        child: CircularProgressIndicator(),
      );
    });
  }
}
