import 'package:flutter/material.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';

import 'package:test_framework_view/type.dart';

import '../../../common/custom_state.dart';

class SleepPropertyWidget extends StatefulWidget {
  final Item item;
  final VoidCallback callback;
  final bool isDragAndDrop;
  final dynamic editorComponentItem;
  const SleepPropertyWidget(this.item, this.callback, {Key? key, required this.isDragAndDrop, required this.editorComponentItem}) : super(key: key);

  @override
  State<SleepPropertyWidget> createState() => _SleepPropertyWidgetState();
}

class _SleepPropertyWidgetState extends CustomState<SleepPropertyWidget> {
  TextEditingController valueController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  @override
  void initState() {
    if (widget.item.type == "error" || widget.item.type == "finish") {
      valueController.text = widget.item.message ?? "";
    } else if (widget.item.type == "comment" || widget.item.type == "group" || widget.item.type == "step") {
      valueController.text = widget.item.description ?? "";
    }
    super.initState();
  }

  @override
  void dispose() {
    valueController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          headerWidget(context),
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                child: AbsorbPointer(
                  absorbing: !widget.isDragAndDrop,
                  child: Column(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                              child: Text(widget.editorComponentItem["title"],
                                  style: const TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
                          Expanded(
                            flex: 3,
                            child: SizedBox(
                              child: DynamicTextField(
                                controller: valueController,
                                hintText: multiLanguageString(name: "enter_a_message", defaultValue: "Enter a message", context: context),
                                onChanged: (text) {
                                  _formKey.currentState!.validate();
                                },
                                isRequiredNotEmpty: true,
                                minline: 3,
                                maxline: 10,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          const SizedBox(height: 20),
          if (widget.isDragAndDrop)
            DynamicButton(
              name: "close",
              label: multiLanguageString(name: "close", defaultValue: "Close", isLowerCase: false, context: context),
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  if (widget.item.type == "error" || widget.item.type == "finish") {
                    widget.item.message = valueController.text;
                  } else if (widget.item.type == "comment" || widget.item.type == "group" || widget.item.type == "step") {
                    widget.item.description = valueController.text;
                  }
                  Navigator.pop(context);
                  widget.callback();
                }
              },
            ),
          if (widget.isDragAndDrop) const SizedBox(height: 20),
        ],
      ),
    );
  }

  Padding headerWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
      child: Row(
        children: [
          Expanded(
            child: Center(
              child: MultiLanguageText(
                name: "component_item_property",
                defaultValue: "\$0 property",
                variables: ["${widget.editorComponentItem["title"]}"],
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: context.fontSizeManager.fontSizeText20, letterSpacing: 1.5),
              ),
            ),
          ),
          TextButton(
              onPressed: () {
                Navigator.pop(context);
                widget.callback();
              },
              child: const Icon(
                Icons.close,
                color: Colors.black,
              ))
        ],
      ),
    );
  }
}
