import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';

import '../../../common/custom_draggable.dart';
import '../../../common/custom_state.dart';

class RootPropertyWidget extends StatefulWidget {
  final List parameterFieldList;
  final GlobalKey<FormState>? keyForm;

  const RootPropertyWidget({
    Key? key,
    this.keyForm,
    required this.parameterFieldList,
  }) : super(key: key);

  @override
  State<RootPropertyWidget> createState() => _RootPropertyWidgetState();
}

class _RootPropertyWidgetState extends CustomState<RootPropertyWidget> {
  var resultListRandomType = [];
  Widget widgetGhost = Container();
  late int oldIndexItem;
  late int newIndexItem;
  int acceptedDataIndex = -1;
  bool dragAccepted = false;
  bool willAccept = false;
  @override
  void initState() {
    getListRandomType();
    super.initState();
  }

  getListRandomType() async {
    var findRequest = {"status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-random-type/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        resultListRandomType = response["body"]["resultList"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        headerTable(),
        Expanded(
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(right: 11),
              child: Column(
                children: [
                  for (var i = 0; i < widget.parameterFieldList.length; i++) parameterItemWidget(i),
                ],
              ),
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 5),
          child: FloatingActionButton.extended(
            heroTag: null,
            icon: const Icon(Icons.add),
            label: const MultiLanguageText(name: "new_parameter", defaultValue: "New parameter"),
            onPressed: () {
              setState(() {
                widget.parameterFieldList.add({"dataType": "String"});
              });
            },
          ),
        ),
      ],
    );
  }

  Widget parameterItemWidget(int i) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: DragTarget(
        builder: (
          BuildContext context,
          List<dynamic> accepted,
          List<dynamic> rejected,
        ) {
          return Column(
            children: [
              CustomDraggable(
                data: widget.parameterFieldList[i],
                childWhenDragging: Container(),
                feedback: SizedBox(
                  width: MediaQuery.of(context).size.width - 185,
                  child: Material(
                    color: Colors.transparent,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Container(
                            padding: const EdgeInsets.only(right: 2),
                            color: Colors.white,
                            child: DynamicDropdownButton(
                              value: widget.parameterFieldList[i]["dataType"] ?? "String",
                              hint: multiLanguageString(name: "choose_a_data_type", defaultValue: "Choose a dataType", context: context),
                              items: const ["String", "Integer", "Long", "Double", "Float", "Date", "DateTime", "File", "RandomValue", "Password"]
                                  .map<DropdownMenuItem<String>>((String result) {
                                return DropdownMenuItem(
                                  value: result,
                                  child: Text(result),
                                );
                              }).toList(),
                              borderRadius: 5,
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: Container(
                                  padding: const EdgeInsets.only(left: 2),
                                  color: Colors.white,
                                  child: DynamicTextField(
                                    fillColor: Colors.transparent,
                                    controller: TextEditingController(text: widget.parameterFieldList[i]["name"] ?? ""),
                                    hintText: multiLanguageString(name: "enter_a_name", defaultValue: "Enter a name", context: context),
                                    obscureText: widget.parameterFieldList[i]["dataType"] == "Password",
                                  ),
                                ),
                              ),
                              if (widget.parameterFieldList[i]["dataType"] == "RandomValue")
                                Expanded(
                                  child: Container(
                                    padding: const EdgeInsets.only(left: 2),
                                    color: Colors.white,
                                    child: DynamicDropdownButton(
                                      value: widget.parameterFieldList[i]["tfRandomTypeId"],
                                      hint: multiLanguageString(name: "choose_a_random_type", defaultValue: "Choose a random type", context: context),
                                      items: resultListRandomType.map<DropdownMenuItem<String>>((dynamic result) {
                                        return DropdownMenuItem(
                                          value: result["id"],
                                          child: Text(result["title"]),
                                        );
                                      }).toList(),
                                      borderRadius: 5,
                                    ),
                                  ),
                                ),
                              const SizedBox(
                                width: 165,
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                onDragStarted: () {
                  oldIndexItem = i;
                  dragAccepted = false;
                },
                onDragCompleted: () {
                  setState(() {
                    if (dragAccepted) {
                      if (newIndexItem <= oldIndexItem) {
                        widget.parameterFieldList.removeAt(oldIndexItem + 1);
                      } else {
                        widget.parameterFieldList.removeAt(oldIndexItem);
                      }
                    } else {
                      widget.parameterFieldList.removeAt(oldIndexItem);
                    }
                  });
                },
                child: Material(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(right: 2),
                          child: DynamicDropdownButton(
                            value: widget.parameterFieldList[i]["dataType"] ?? "String",
                            hint: multiLanguageString(name: "choose_a_data_type", defaultValue: "Choose a dataType", context: context),
                            items: const ["String", "Integer", "Long", "Double", "Float", "Date", "DateTime", "File", "RandomValue", "Password"]
                                .map<DropdownMenuItem<String>>((String result) {
                              return DropdownMenuItem(
                                value: result,
                                child: Text(result),
                              );
                            }).toList(),
                            onChanged: (newValue) {
                              setState(() {
                                widget.parameterFieldList[i]["dataType"] = newValue;
                              });
                            },
                            borderRadius: 5,
                            isRequiredNotEmpty: true,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 2),
                                child: DynamicTextField(
                                  fillColor: Colors.transparent,
                                  controller: TextEditingController(text: widget.parameterFieldList[i]["name"] ?? ""),
                                  hintText: multiLanguageString(name: "enter_a_name", defaultValue: "Enter a name", context: context),
                                  onChanged: (text) {
                                    (widget.keyForm!.currentState!.validate());
                                    widget.parameterFieldList[i]["name"] = text;
                                  },
                                  isRequiredNotEmpty: true,
                                  isRequiredRegex: true,
                                  obscureText: widget.parameterFieldList[i]["dataType"] == "Password",
                                ),
                              ),
                            ),
                            if (widget.parameterFieldList[i]["dataType"] == "RandomValue")
                              Expanded(
                                child: DynamicDropdownButton(
                                  value: widget.parameterFieldList[i]["tfRandomTypeId"],
                                  hint: multiLanguageString(name: "choose_a_random_type", defaultValue: "Choose a random type", context: context),
                                  items: resultListRandomType.map<DropdownMenuItem<String>>((dynamic result) {
                                    return DropdownMenuItem(
                                      value: result["id"],
                                      child: Text(result["title"]),
                                    );
                                  }).toList(),
                                  onChanged: (newValue) {
                                    setState(() {
                                      widget.parameterFieldList[i]["tfRandomTypeId"] = newValue!;
                                    });
                                  },
                                  borderRadius: 5,
                                  isRequiredNotEmpty: true,
                                ),
                              ),
                            Container(
                              margin: const EdgeInsets.only(left: 5),
                              padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
                              decoration: BoxDecoration(
                                border: Border.all(color: Colors.grey),
                                borderRadius: const BorderRadius.all(Radius.circular(5.0)),
                              ),
                              height: 48,
                              child: Row(
                                children: [
                                  Container(
                                    width: 45,
                                    height: 25,
                                    margin: const EdgeInsets.only(right: 7),
                                    child: Tooltip(
                                      message: multiLanguageString(name: "move_up", defaultValue: "Move up", context: context),
                                      child: ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                            backgroundColor: Colors.blue, padding: const EdgeInsets.symmetric(horizontal: 11)),
                                        onPressed: () {
                                          setState(() {
                                            if (i > 0) {
                                              widget.parameterFieldList.insert(i - 1, widget.parameterFieldList[i]);
                                              widget.parameterFieldList.removeAt(i + 1);
                                            } else {
                                              widget.parameterFieldList.insert(widget.parameterFieldList.length, widget.parameterFieldList[i]);
                                              widget.parameterFieldList.removeAt(0);
                                            }
                                          });
                                        },
                                        child: const Icon(Icons.arrow_upward_outlined, size: 15),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    width: 45,
                                    height: 25,
                                    margin: const EdgeInsets.only(right: 7),
                                    child: Tooltip(
                                      message: multiLanguageString(name: "move_down", defaultValue: "Move down", context: context),
                                      child: ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                            backgroundColor: Colors.blue, padding: const EdgeInsets.symmetric(horizontal: 11)),
                                        onPressed: () {
                                          setState(() {
                                            if (i + 1 < widget.parameterFieldList.length) {
                                              widget.parameterFieldList.insert(i + 2, widget.parameterFieldList[i]);
                                              widget.parameterFieldList.removeAt(i);
                                            } else {
                                              widget.parameterFieldList.insert(0, widget.parameterFieldList[i]);
                                              widget.parameterFieldList.removeAt(i + 1);
                                            }
                                          });
                                        },
                                        child: const Icon(Icons.arrow_downward_outlined, size: 15),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 45,
                                    height: 25,
                                    child: Tooltip(
                                      message: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                                      child: ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                            backgroundColor: Colors.red, padding: const EdgeInsets.symmetric(horizontal: 11)),
                                        onPressed: () {
                                          setState(() {
                                            widget.parameterFieldList.remove(widget.parameterFieldList[i]);
                                          });
                                        },
                                        child: const Icon(Icons.delete, color: Colors.white, size: 15),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              if (i == acceptedDataIndex)
                Padding(
                  padding: const EdgeInsets.only(top: 10, right: 25),
                  child: Opacity(
                    opacity: 0.5,
                    child: widgetGhost,
                  ),
                ),
            ],
          );
        },
        onWillAccept: (dynamic data) {
          setState(() {
            acceptedDataIndex = i;
            widgetGhost = SizedBox(
              width: MediaQuery.of(context).size.width - 185,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 2),
                      child: DynamicDropdownButton(
                        value: data["dataType"] ?? "String",
                        hint: multiLanguageString(name: "choose_a_data_type", defaultValue: "Choose a dataType", context: context),
                        items: const ["String", "Integer", "Long", "Double", "Float", "Date", "DateTime", "File", "RandomValue", "Password"]
                            .map<DropdownMenuItem<String>>((String result) {
                          return DropdownMenuItem(
                            value: result,
                            child: Text(result),
                          );
                        }).toList(),
                        onChanged: (newValue) {
                          widget.keyForm!.currentState!.validate();
                          setState(() {
                            data["dataType"] = newValue;
                          });
                        },
                        borderRadius: 5,
                        isRequiredNotEmpty: true,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 2),
                            child: DynamicTextField(
                              fillColor: Colors.transparent,
                              controller: TextEditingController(text: data["name"] ?? ""),
                              hintText: multiLanguageString(name: "enter_a_name", defaultValue: "Enter a name", context: context),
                              isRequiredNotEmpty: true,
                              isRequiredRegex: true,
                              obscureText: data["dataType"] == "Password",
                            ),
                          ),
                        ),
                        if (data["dataType"] == "RandomValue")
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(left: 2),
                              child: DynamicDropdownButton(
                                value: data["tfRandomTypeId"],
                                hint: multiLanguageString(name: "choose_a_random_type", defaultValue: "Choose a random type", context: context),
                                items: resultListRandomType.map<DropdownMenuItem<String>>((dynamic result) {
                                  return DropdownMenuItem(
                                    value: result["id"],
                                    child: Text(result["title"]),
                                  );
                                }).toList(),
                                onChanged: (newValue) {
                                  setState(() {
                                    data["tfRandomTypeId"] = newValue!;
                                  });
                                },
                                borderRadius: 5,
                                isRequiredNotEmpty: true,
                              ),
                            ),
                          ),
                        const SizedBox(
                          width: 140,
                        )
                      ],
                    ),
                  ),
                ],
              ),
            );
          });
          return true;
        },
        onLeave: (data) {
          acceptedDataIndex = -1;
        },
        onAccept: (data) {
          setState(() {
            dragAccepted = true;
            acceptedDataIndex = -1;
            newIndexItem = widget.parameterFieldList.indexOf(widget.parameterFieldList[i]) + 1;
            widget.parameterFieldList.insert(newIndexItem, data);
          });
        },
      ),
    );
  }

  Widget headerTable() {
    return DragTarget(
      builder: (
        BuildContext context,
        List<dynamic> accepted,
        List<dynamic> rejected,
      ) {
        return Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 11),
              child: Row(
                children: [
                  HeaderItemTable(title: multiLanguageString(name: "data_type", defaultValue: "Data Type", context: context)),
                  HeaderItemTable(
                    title: multiLanguageString(name: "name", defaultValue: "Name", context: context),
                    flex: 2,
                  ),
                ],
              ),
            ),
            if (willAccept)
              Padding(
                padding: const EdgeInsets.only(top: 10, right: 25),
                child: Opacity(
                  opacity: 0.5,
                  child: widgetGhost,
                ),
              ),
          ],
        );
      },
      onWillAccept: (dynamic data) {
        setState(() {
          willAccept = true;
          widgetGhost = SizedBox(
            width: MediaQuery.of(context).size.width - 185,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 2),
                    child: DynamicDropdownButton(
                      value: data["dataType"] ?? "String",
                      hint: multiLanguageString(name: "choose_a_data_type", defaultValue: "Choose a dataType", context: context),
                      items: const ["String", "Integer", "Long", "Double", "Float", "Date", "DateTime", "File", "RandomValue", "Password"]
                          .map<DropdownMenuItem<String>>((String result) {
                        return DropdownMenuItem(
                          value: result,
                          child: Text(result),
                        );
                      }).toList(),
                      onChanged: (newValue) {
                        widget.keyForm!.currentState!.validate();
                        setState(() {
                          data["dataType"] = newValue;
                        });
                      },
                      borderRadius: 5,
                      isRequiredNotEmpty: true,
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 2),
                          child: DynamicTextField(
                            fillColor: Colors.transparent,
                            controller: TextEditingController(text: data["name"] ?? ""),
                            hintText: multiLanguageString(name: "enter_a_name", defaultValue: "Enter a name", context: context),
                            isRequiredNotEmpty: true,
                            isRequiredRegex: true,
                            obscureText: data["dataType"] == "Password",
                          ),
                        ),
                      ),
                      if (data["dataType"] == "RandomValue")
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 2),
                            child: DynamicDropdownButton(
                              value: data["tfRandomTypeId"],
                              hint: multiLanguageString(name: "choose_a_random_type", defaultValue: "Choose a random type", context: context),
                              items: resultListRandomType.map<DropdownMenuItem<String>>((dynamic result) {
                                return DropdownMenuItem(
                                  value: result["id"],
                                  child: Text(result["title"]),
                                );
                              }).toList(),
                              onChanged: (newValue) {
                                setState(() {
                                  data["tfRandomTypeId"] = newValue!;
                                });
                              },
                              borderRadius: 5,
                              isRequiredNotEmpty: true,
                            ),
                          ),
                        ),
                      const SizedBox(
                        width: 150,
                      )
                    ],
                  ),
                ),
              ],
            ),
          );
        });
        return true;
      },
      onLeave: (data) {
        willAccept = false;
      },
      onAccept: (data) {
        setState(() {
          willAccept = false;
          dragAccepted = true;
          newIndexItem = 0;
          widget.parameterFieldList.insert(0, data);
        });
      },
    );
  }
}

class HeaderItemTable extends StatelessWidget {
  final int flex;
  final String title;
  const HeaderItemTable({
    Key? key,
    this.flex = 1,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: flex,
      child: Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 12),
        decoration: BoxDecoration(
          border: Border.all(width: 1, color: const Color.fromARGB(255, 205, 206, 207), style: BorderStyle.solid),
          color: const Color.fromARGB(255, 222, 223, 224),
        ),
        child: Text(
          title,
          style: TextStyle(fontSize: context.fontSizeManager.fontSizeText14, fontWeight: FontWeight.bold, letterSpacing: 2),
        ),
      ),
    );
  }
}

class OutputPropertyWidget extends StatefulWidget {
  final List outputFieldList;
  final GlobalKey<FormState>? keyForm;

  const OutputPropertyWidget({
    Key? key,
    this.keyForm,
    required this.outputFieldList,
  }) : super(key: key);

  @override
  State<OutputPropertyWidget> createState() => _OutputPropertyWidgetState();
}

class _OutputPropertyWidgetState extends CustomState<OutputPropertyWidget> {
  var resultListRandomType = [];
  Widget widgetGhost = Container();
  late int oldIndexItem;
  late int newIndexItem;
  int acceptedDataIndex = -1;
  bool dragAccepted = false;
  bool willAccept = false;
  @override
  void initState() {
    getListRandomType();
    super.initState();
  }

  getListRandomType() async {
    var findRequest = {"status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-random-type/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        resultListRandomType = response["body"]["resultList"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        headerWidget(),
        Expanded(
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(right: 11),
              child: Column(
                children: [
                  for (var i = 0; i < widget.outputFieldList.length; i++) outputItemWidget(i),
                ],
              ),
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 5),
          child: FloatingActionButton.extended(
            heroTag: null,
            icon: const Icon(Icons.add),
            label: const MultiLanguageText(name: "new_output", defaultValue: "New output"),
            onPressed: () {
              setState(() {
                widget.outputFieldList.add({"dataType": "String"});
              });
            },
          ),
        ),
      ],
    );
  }

  Widget outputItemWidget(int i) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Container(
                  margin: const EdgeInsets.only(right: 2),
                  child: DynamicDropdownButton(
                    value: widget.outputFieldList[i]["dataType"] ?? "String",
                    hint: multiLanguageString(name: "choose_a_data_type", defaultValue: "Choose a dataType", context: context),
                    items: const ["String", "Integer", "Long", "Double", "Float", "Date", "DateTime", "File", "RandomValue", "Password"]
                        .map<DropdownMenuItem<String>>((String result) {
                      return DropdownMenuItem(
                        value: result,
                        child: Text(result),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        widget.outputFieldList[i]["dataType"] = newValue;
                      });
                    },
                    borderRadius: 5,
                    isRequiredNotEmpty: true,
                  ),
                ),
              ),
              Expanded(
                flex: 2,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.only(left: 2),
                        child: DynamicTextField(
                          fillColor: Colors.transparent,
                          controller: TextEditingController(text: widget.outputFieldList[i]["name"] ?? ""),
                          hintText: multiLanguageString(name: "enter_a_name", defaultValue: "Enter a name", context: context),
                          onChanged: (text) {
                            (widget.keyForm!.currentState!.validate());
                            widget.outputFieldList[i]["name"] = text;
                          },
                          isRequiredNotEmpty: true,
                          isRequiredRegex: true,
                          obscureText: widget.outputFieldList[i]["dataType"] == "Password",
                        ),
                      ),
                    ),
                    if (widget.outputFieldList[i]["dataType"] == "RandomValue")
                      Expanded(
                        child: DynamicDropdownButton(
                          value: widget.outputFieldList[i]["tfRandomTypeId"],
                          hint: multiLanguageString(name: "choose_a_random_type", defaultValue: "Choose a random type", context: context),
                          items: resultListRandomType.map<DropdownMenuItem<String>>((dynamic result) {
                            return DropdownMenuItem(
                              value: result["id"],
                              child: Text(result["title"]),
                            );
                          }).toList(),
                          onChanged: (newValue) {
                            setState(() {
                              widget.outputFieldList[i]["tfRandomTypeId"] = newValue!;
                            });
                          },
                          borderRadius: 5,
                          isRequiredNotEmpty: true,
                        ),
                      ),
                    Container(
                      margin: const EdgeInsets.only(left: 5),
                      padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey),
                        borderRadius: const BorderRadius.all(Radius.circular(5.0)),
                      ),
                      height: 48,
                      child: Row(
                        children: [
                          Container(
                            width: 45,
                            height: 25,
                            margin: const EdgeInsets.only(right: 7),
                            child: Tooltip(
                              message: multiLanguageString(name: "move_up", defaultValue: "Move up", context: context),
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(backgroundColor: Colors.blue, padding: const EdgeInsets.symmetric(horizontal: 11)),
                                onPressed: () {
                                  setState(() {
                                    if (i > 0) {
                                      widget.outputFieldList.insert(i - 1, widget.outputFieldList[i]);
                                      widget.outputFieldList.removeAt(i + 1);
                                    } else {
                                      widget.outputFieldList.insert(widget.outputFieldList.length, widget.outputFieldList[i]);
                                      widget.outputFieldList.removeAt(0);
                                    }
                                  });
                                },
                                child: const Icon(Icons.arrow_upward_outlined, size: 15),
                              ),
                            ),
                          ),
                          Container(
                            width: 45,
                            height: 25,
                            margin: const EdgeInsets.only(right: 7),
                            child: Tooltip(
                              message: multiLanguageString(name: "move_down", defaultValue: "Move down", context: context),
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(backgroundColor: Colors.blue, padding: const EdgeInsets.symmetric(horizontal: 11)),
                                onPressed: () {
                                  setState(() {
                                    if (i + 1 < widget.outputFieldList.length) {
                                      widget.outputFieldList.insert(i + 2, widget.outputFieldList[i]);
                                      widget.outputFieldList.removeAt(i);
                                    } else {
                                      widget.outputFieldList.insert(0, widget.outputFieldList[i]);
                                      widget.outputFieldList.removeAt(i + 1);
                                    }
                                  });
                                },
                                child: const Icon(Icons.arrow_downward_outlined, size: 15),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 45,
                            height: 25,
                            child: Tooltip(
                              message: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(backgroundColor: Colors.red, padding: const EdgeInsets.symmetric(horizontal: 11)),
                                onPressed: () {
                                  setState(() {
                                    widget.outputFieldList.remove(widget.outputFieldList[i]);
                                  });
                                },
                                child: const Icon(Icons.delete, color: Colors.white, size: 15),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Padding headerWidget() {
    return Padding(
      padding: const EdgeInsets.only(right: 11),
      child: Row(
        children: [
          HeaderItemTable(title: multiLanguageString(name: "data_type", defaultValue: "Data Type", context: context)),
          HeaderItemTable(
            title: multiLanguageString(name: "name", defaultValue: "Name", context: context),
            flex: 2,
          ),
        ],
      ),
    );
  }
}

class ClientsPropertyWidget extends StatefulWidget {
  final List clientList;
  final GlobalKey<FormState>? keyForm;

  const ClientsPropertyWidget({
    Key? key,
    this.keyForm,
    required this.clientList,
  }) : super(key: key);

  @override
  State<ClientsPropertyWidget> createState() => _ClientsPropertyWidgetState();
}

class _ClientsPropertyWidgetState extends CustomState<ClientsPropertyWidget> {
  Widget widgetGhost = Container();
  late int oldIndexItem;
  late int newIndexItem;
  int acceptedDataIndex = -1;
  bool dragAccepted = false;
  bool willAccept = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        headerWidget(),
        Expanded(
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(right: 11),
              child: Column(
                children: [
                  for (var i = 0; i < widget.clientList.length; i++) clientItemWidget(i),
                ],
              ),
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 5),
          child: FloatingActionButton.extended(
            heroTag: null,
            icon: const Icon(Icons.add),
            label: const MultiLanguageText(name: "new_client", defaultValue: "New client"),
            onPressed: () {
              setState(() {
                widget.clientList.add({"name": "", "platform": null});
              });
            },
          ),
        ),
      ],
    );
  }

  Widget clientItemWidget(int i) {
    var securityModel = Provider.of<SecurityModel>(context, listen: false);
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Container(
                margin: const EdgeInsets.only(right: 2),
                child: DynamicDropdownButton(
                  value: widget.clientList[i]["platform"],
                  hint: multiLanguageString(name: "choose_a_platform", defaultValue: "Choose a platform", context: context),
                  items: (securityModel.sysOrganizationPlatformsList ?? []).map<DropdownMenuItem<String>>((String result) {
                    return DropdownMenuItem(
                      value: result,
                      child: Text(result),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      (widget.keyForm!.currentState!.validate());
                      widget.clientList[i]["platform"] = newValue;
                    });
                  },
                  borderRadius: 5,
                  isRequiredNotEmpty: true,
                )),
          ),
          Expanded(
            flex: 2,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Container(
                    margin: const EdgeInsets.only(left: 2),
                    child: DynamicTextField(
                      fillColor: Colors.transparent,
                      controller: TextEditingController(text: widget.clientList[i]["name"] ?? ""),
                      hintText: multiLanguageString(name: "enter_a_name", defaultValue: "Enter a name", context: context),
                      onChanged: (text) {
                        (widget.keyForm!.currentState!.validate());
                        widget.clientList[i]["name"] = text;
                      },
                      isRequiredNotEmpty: true,
                      isRequiredRegex: true,
                      maxline: 1,
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(left: 5),
                  padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey),
                    borderRadius: const BorderRadius.all(Radius.circular(5.0)),
                  ),
                  height: 48,
                  child: Row(
                    children: [
                      Container(
                        width: 45,
                        height: 25,
                        margin: const EdgeInsets.only(right: 7),
                        child: Tooltip(
                          message: multiLanguageString(name: "move_up", defaultValue: "Move up", context: context),
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(backgroundColor: Colors.blue, padding: const EdgeInsets.symmetric(horizontal: 11)),
                            onPressed: () {
                              setState(() {
                                if (i > 0) {
                                  widget.clientList.insert(i - 1, widget.clientList[i]);
                                  widget.clientList.removeAt(i + 1);
                                } else {
                                  widget.clientList.insert(widget.clientList.length, widget.clientList[i]);
                                  widget.clientList.removeAt(0);
                                }
                              });
                            },
                            child: const Icon(Icons.arrow_upward_outlined, size: 15),
                          ),
                        ),
                      ),
                      Container(
                        width: 45,
                        height: 25,
                        margin: const EdgeInsets.only(right: 7),
                        child: Tooltip(
                          message: multiLanguageString(name: "move_down", defaultValue: "Move down", context: context),
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(backgroundColor: Colors.blue, padding: const EdgeInsets.symmetric(horizontal: 11)),
                            onPressed: () {
                              setState(() {
                                if (i + 1 < widget.clientList.length) {
                                  widget.clientList.insert(i + 2, widget.clientList[i]);
                                  widget.clientList.removeAt(i);
                                } else {
                                  widget.clientList.insert(0, widget.clientList[i]);
                                  widget.clientList.removeAt(i + 1);
                                }
                              });
                            },
                            child: const Icon(Icons.arrow_downward_outlined, size: 15),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 45,
                        height: 25,
                        child: Tooltip(
                          message: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(backgroundColor: Colors.red, padding: const EdgeInsets.symmetric(horizontal: 11)),
                            onPressed: () {
                              setState(() {
                                widget.clientList.remove(widget.clientList[i]);
                              });
                            },
                            child: const Icon(Icons.delete, color: Colors.white, size: 15),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Padding headerWidget() {
    return Padding(
      padding: const EdgeInsets.only(right: 11),
      child: Row(
        children: [
          HeaderItemTable(title: multiLanguageString(name: "platform", defaultValue: "Platform", context: context)),
          HeaderItemTable(
            title: multiLanguageString(name: "name", defaultValue: "Name", context: context),
            flex: 2,
          ),
        ],
      ),
    );
  }
}
