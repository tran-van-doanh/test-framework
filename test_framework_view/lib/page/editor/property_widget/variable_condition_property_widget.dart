import 'package:flutter/material.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';

import '../../../common/custom_state.dart';
import '../../../type.dart';

class VariableConditionPropertyWidget extends StatefulWidget {
  final Item item;
  final VoidCallback callback;
  final bool isDragAndDrop;
  const VariableConditionPropertyWidget(this.item, this.callback, {Key? key, required this.isDragAndDrop}) : super(key: key);

  @override
  State<VariableConditionPropertyWidget> createState() => _VariableConditionPropertyWidgetState();
}

class _VariableConditionPropertyWidgetState extends CustomState<VariableConditionPropertyWidget> {
  String operatorType = "";
  @override
  void initState() {
    operatorType = widget.item.operatorType ?? "";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        headerWidget(context),
        Expanded(
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
              child: AbsorbPointer(
                absorbing: !widget.isDragAndDrop,
                child: Column(
                  children: [
                    Row(
                      children: [
                        const Expanded(
                            child: MultiLanguageText(
                                name: "operator_type",
                                defaultValue: "Operator Type",
                                suffiText: " : ",
                                style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromRGBO(87, 94, 117, 1)))),
                        Expanded(
                          flex: 3,
                          child: Container(
                            decoration: BoxDecoration(border: Border.all(color: const Color.fromRGBO(216, 218, 229, 1))),
                            child: DynamicDropdownButton(
                              value: operatorType != "" ? operatorType : null,
                              onChanged: (dynamic newValue) {
                                setState(() {
                                  operatorType = newValue;
                                });
                              },
                              items: widget.item.category == "variable"
                                  ? ["+", "-", "x", "/"].map<DropdownMenuItem<String>>((dynamic result) {
                                      return DropdownMenuItem(
                                        value: result,
                                        child: Text(result),
                                      );
                                    }).toList()
                                  : (widget.item.type == "AND" || widget.item.type == "OR" || widget.item.type == "NOT")
                                      ? [
                                          {
                                            "name": multiLanguageString(name: "and", defaultValue: "And", isLowerCase: false, context: context),
                                            "value": "AND"
                                          },
                                          {
                                            "name": multiLanguageString(name: "or", defaultValue: "Or", isLowerCase: false, context: context),
                                            "value": "OR"
                                          },
                                          {
                                            "name": multiLanguageString(name: "not", defaultValue: "Not", isLowerCase: false, context: context),
                                            "value": "NOT"
                                          }
                                        ].map<DropdownMenuItem<String>>((dynamic result) {
                                          return DropdownMenuItem(
                                            value: result["value"],
                                            child: Text(result["name"]),
                                          );
                                        }).toList()
                                      : ["contains", "matches", ">", ">=", "<", "<=", "==", "!="].map<DropdownMenuItem<String>>((dynamic result) {
                                          return DropdownMenuItem(
                                            value: result,
                                            child: Text(result),
                                          );
                                        }).toList(),
                              hint: multiLanguageString(name: "choose_an_operator", defaultValue: "Choose an operator", context: context),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        const SizedBox(height: 20),
        if (widget.isDragAndDrop)
          DynamicButton(
            name: "close",
            label: multiLanguageString(name: "close", defaultValue: "Close", isLowerCase: false, context: context),
            onPressed: () {
              widget.item.operatorType = operatorType;
              for (var operatorTypeItem in operatorTypeList) {
                if (operatorType == operatorTypeItem["operatorType"]) {
                  widget.item.type = operatorTypeItem["type"]!;
                }
              }
              Navigator.pop(context);
              widget.callback();
            },
          ),
        if (widget.isDragAndDrop) const SizedBox(height: 20),
      ],
    );
  }

  Padding headerWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
      child: Row(
        children: [
          Expanded(
            child: Center(
              child: MultiLanguageText(
                name: "choose_a_operator",
                defaultValue: "Choose an operator",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: context.fontSizeManager.fontSizeText20, letterSpacing: 1.5),
              ),
            ),
          ),
          TextButton(
              onPressed: () {
                Navigator.pop(context);
                widget.callback();
              },
              child: const Icon(
                Icons.close,
                color: Colors.black,
              ))
        ],
      ),
    );
  }
}
