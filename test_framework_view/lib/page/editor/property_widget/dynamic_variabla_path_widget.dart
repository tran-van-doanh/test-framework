import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/editor/element_config/element_repository.dart';
import 'package:test_framework_view/page/editor/element_config/find_value.dart';
import 'package:test_framework_view/page/editor/element_config/web_element.dart';
import 'package:test_framework_view/page/editor/property_widget/find_variable_widget.dart';
import 'package:test_framework_view/type.dart';

import '../../../common/custom_state.dart';

class DynamicVariablePathWidget extends StatefulWidget {
  final dynamic variablePath;
  final int indexOfVariablePath;
  final List<Map<String, String>> listValueType;
  final List<Map<String, String>> listValueDataType;
  final List<String>? unitValueList;
  final GlobalKey<FormState>? keyForm;
  final VoidCallback callback;
  final VoidCallback? onRemove;
  final String? clientName;
  final String? platform;
  final bool isRequiredNotEmpty;
  final bool isRequiredRegex;
  const DynamicVariablePathWidget(this.variablePath, this.indexOfVariablePath, this.listValueType, this.listValueDataType,
      {Key? key,
      this.keyForm,
      required this.callback,
      this.clientName,
      this.isRequiredNotEmpty = true,
      this.isRequiredRegex = true,
      this.platform,
      this.unitValueList,
      this.onRemove})
      : super(key: key);

  @override
  State<DynamicVariablePathWidget> createState() => _DynamicVariablePathWidgetState();
}

class _DynamicVariablePathWidgetState extends CustomState<DynamicVariablePathWidget> {
  bool isStartDebug = false;
  List? parameterListProject;
  TextEditingController valueElementController = TextEditingController();
  @override
  void initState() {
    getInfoPage();
    super.initState();
  }

  getInfoPage() {
    getStatusDebug();
    if (widget.variablePath["type"] == "version_variable" ||
        widget.variablePath["type"] == "environment_variable" ||
        widget.variablePath["type"] == "node_variable") {
      getParamProject(Provider.of<NavigationModel>(context, listen: false).prjProjectId);
    }
    if (widget.variablePath["tfValueTemplateId"] != null) {
      getElementTemplate(widget.variablePath["tfValueTemplateId"]);
    }
    if (widget.variablePath["tfTestElementId"] != null && widget.variablePath["tfTestElementRepositoryId"] != null) {
      getElementRepositoryPath(widget.variablePath["tfTestElementRepositoryId"], widget.variablePath["tfTestElementId"],
          name: widget.variablePath["tfTestElementXpathName"]);
    }
  }

  getParamProject(prjProjectId) async {
    var response = await httpGet("/test-framework-api/user/project/prj-project/$prjProjectId", context);
    if (response.containsKey("body") && response["body"] is String == false && response["body"].containsKey("result")) {
      setState(() {
        parameterListProject = response["body"]["result"]["projectConfig"]["parameterFieldList"];
      });
    }
  }

  getStatusDebug() async {
    var statusResponse = await httpGet("/test-framework-api/user/node/status", context);
    if (statusResponse.containsKey("body") && statusResponse["body"] is String == false && statusResponse["body"].containsKey("result")) {
      if (statusResponse["body"]["result"]["status"] == 2) {
        setState(() {
          isStartDebug = true;
        });
      }
    }
  }

  getElementTemplate(id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-value-template/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        valueElementController =
            TextEditingController(text: response["body"]["result"]["tfValueType"]["name"] + " / " + response["body"]["result"]["name"]);
      });
    }
  }

  getElementRepositoryPath(parentId, id, {name}) async {
    String result = "";
    var directoryPathResponse = await httpGet("/test-framework-api/user/test-framework/tf-test-element-repository/$parentId/path", context);
    if (directoryPathResponse.containsKey("body") && directoryPathResponse["body"] is String == false) {
      for (var path in directoryPathResponse["body"]["resultList"]) {
        result += (path["title"] ?? "Null") + " / ";
      }
    }
    if (mounted) {
      var elementResponse = await httpGet("/test-framework-api/user/test-framework/tf-test-element/$id", context);
      if (elementResponse.containsKey("body") && elementResponse["body"] is String == false) {
        if (elementResponse["body"]["result"] != null) {
          result += elementResponse["body"]["result"]["name"];
          if (name != null) result += " # $name";
        } else {
          result += '"has been deleted"';
        }
      }
      setState(() {
        valueElementController = TextEditingController(text: result);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (widget.unitValueList == null)
          Wrap(
            spacing: 10,
            children: [
              if (widget.indexOfVariablePath == 0)
                for (var valueType in widget.listValueType)
                  GestureDetector(
                    onTap: () {
                      if (widget.variablePath["type"] != valueType["value"]) {
                        widget.variablePath["type"] = valueType["value"];
                        if (widget.variablePath["type"] == "version_variable" ||
                            widget.variablePath["type"] == "environment_variable" ||
                            widget.variablePath["type"] == "node_variable") {
                          getParamProject(Provider.of<NavigationModel>(context, listen: false).prjProjectId);
                        }
                        // if (widget.variablePath["type"] == "version_variable" ||
                        //     widget.variablePath["type"] == "environment_variable" ||
                        //     widget.variablePath["type"] == "node_variable") {
                        //   widget.variablePath["name"] = "";
                        //   widget.variablePath["value"] = "";
                        //   await getParamProject(Provider.of<NavigationModel>(context, listen: false).prjProjectId);
                        // } else if (widget.variablePath["type"] == "value") {
                        //   widget.variablePath["name"] = "";
                        // } else {
                        //   widget.variablePath["value"] = "";
                        // }
                        // if (widget.variablePath["type"] != "value") {
                        //   if (widget.variablePath.containsKey("tfValueTemplateId")) {
                        //     widget.variablePath.remove("tfValueTemplateId");
                        //   }
                        //   if (widget.variablePath.containsKey("tfTestElementId")) {
                        //     widget.variablePath.remove("tfTestElementId");
                        //   }
                        //   if (widget.variablePath.containsKey("tfTestElementRepositoryId")) {
                        //     widget.variablePath.remove("tfTestElementRepositoryId");
                        //   }
                        // }
                        widget.callback();
                      }
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border(
                        bottom: BorderSide(
                            color: widget.variablePath["type"] == valueType["value"] ? Colors.blue : Colors.grey,
                            width: widget.variablePath["type"] == valueType["value"] ? 3 : 2),
                      )),
                      child: Text(
                        valueType["name"] ?? "",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: context.fontSizeManager.fontSizeText16,
                            color: const Color.fromRGBO(87, 94, 117, 1)),
                      ),
                    ),
                  ),
            ],
          ),
        if (widget.unitValueList == null)
          const SizedBox(
            height: 10,
          ),
        if (widget.unitValueList == null)
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                  child: DynamicDropdownButton(
                      value: widget.variablePath["dataType"],
                      hint: multiLanguageString(name: "choose_a_data_type", defaultValue: "Choose a dataType", context: context),
                      items: widget.listValueDataType.map<DropdownMenuItem<String>>((dynamic result) {
                        return DropdownMenuItem(
                          value: result["value"],
                          child: Text(result["name"]),
                        );
                      }).toList(),
                      onChanged: (newValue) {
                        widget.keyForm?.currentState!.validate();
                        setState(() {
                          widget.variablePath["dataType"] = newValue;
                        });
                      },
                      isRequiredNotEmpty: widget.isRequiredNotEmpty)),
              const SizedBox(
                width: 10,
              ),
              Expanded(flex: 3, child: buildVariableWidget()),
              if (widget.indexOfVariablePath > 0)
                GestureDetector(
                  onTap: widget.onRemove != null ? () => widget.onRemove!() : null,
                  child: const Padding(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Icon(
                      Icons.remove_circle,
                      color: Colors.red,
                    ),
                  ),
                ),
            ],
          ),
        const SizedBox(
          height: 10,
        ),
        listBtnOption(),
      ],
    );
  }

  Widget buildVariableWidget() {
    String? variableType = widget.variablePath["type"];
    String? variableName = widget.variablePath["name"];
    String? dataType = widget.variablePath["dataType"];

    if (variableType == "variable" || variableType == "get") {
      return DynamicTextField(
        key: const Key("variable_get"),
        controller: TextEditingController(text: variableName ?? ""),
        hintText: multiLanguageString(name: "enter_a_name", defaultValue: "Enter a name", context: context),
        onChanged: (text) {
          widget.keyForm?.currentState!.validate();
          widget.variablePath["name"] = text;
        },
        isRequiredNotEmpty: widget.isRequiredNotEmpty,
        isRequiredRegex: widget.isRequiredRegex,
        obscureText: dataType == "Password",
      );
    } else if (variableType == "version_variable" || variableType == "environment_variable" || variableType == "node_variable") {
      final parameterList = (parameterListProject ?? []).where((element) => element["variableType"] == variableType).toList();
      if (parameterList.isNotEmpty) {
        if (parameterList.every((element) => element["name"] != variableName)) {
          variableName = null;
        }
        return DynamicDropdownButton(
          key: const Key("version_environment_node_variable"),
          value: variableName != "" ? variableName : null,
          hint: multiLanguageString(name: "choose_a_data_name", defaultValue: "Choose a name", context: context),
          items: parameterList.where((element) => element["variableType"] == variableType).map<DropdownMenuItem<String>>((dynamic result) {
            return DropdownMenuItem(
              value: result["name"],
              child: Text(result["name"]),
            );
          }).toList(),
          onChanged: (newValue) {
            widget.keyForm?.currentState!.validate();
            setState(() {
              widget.variablePath["name"] = newValue;
            });
          },
          isRequiredNotEmpty: widget.isRequiredNotEmpty,
        );
      } else {
        return DynamicTextField(
          key: const Key("version_environment_node_variable"),
          controller: TextEditingController(text: variableName ?? ""),
          hintText: multiLanguageString(name: "enter_a_name", defaultValue: "Enter a name", context: context),
          onChanged: (text) {
            widget.keyForm?.currentState!.validate();
            widget.variablePath["name"] = text;
          },
          isRequiredNotEmpty: widget.isRequiredNotEmpty,
          isRequiredRegex: widget.isRequiredRegex,
          obscureText: dataType == "Password",
        );
      }
    } else if (variableType == "value" && dataType != "RepositoryElement") {
      if (widget.unitValueList == null) {
        return DynamicTextField(
          key: const Key("value"),
          controller: widget.variablePath["tfValueTemplateId"] != null
              ? valueElementController
              : TextEditingController(text: widget.variablePath["value"] ?? ""),
          hintText: multiLanguageString(name: "enter_a_value", defaultValue: "Enter a value", context: context),
          onChanged: (text) {
            widget.keyForm?.currentState!.validate();
            widget.variablePath["value"] = text;
            if (widget.variablePath["tfValueTemplateId"] != null) {
              widget.variablePath.remove("tfValueTemplateId");
            }
          },
          isRequiredNotEmpty: widget.isRequiredNotEmpty,
          obscureText: dataType == "Password",
        );
      } else {
        return DynamicDropdownButton(
          key: const Key("value"),
          value: widget.variablePath["value"] != "" ? widget.variablePath["value"] : null,
          hint: multiLanguageString(name: "choose_a_data_value", defaultValue: "Choose a value", context: context),
          items: widget.unitValueList!.map<DropdownMenuItem<String>>((String result) {
            return DropdownMenuItem(
              value: result,
              child: Text(result),
            );
          }).toList(),
          onChanged: (newValue) {
            widget.keyForm?.currentState!.validate();
            setState(() {
              widget.variablePath["value"] = newValue;
            });
          },
          isRequiredNotEmpty: widget.isRequiredNotEmpty,
        );
      }
    } else if (dataType == "RepositoryElement") {
      return DynamicTextField(
        key: const Key("RepositoryElement"),
        readOnly: true,
        controller: valueElementController,
        onChanged: (value) {
          setState(() {
            valueElementController.text;
          });
        },
        suffixIcon: valueElementController.text.isNotEmpty
            ? IconButton(
                onPressed: () {
                  setState(() {
                    valueElementController.clear();
                    widget.variablePath.remove("tfValueTemplateId");
                    widget.variablePath.remove("tfTestElementId");
                    widget.variablePath.remove("tfTestElementRepositoryId");
                  });
                },
                icon: const Icon(Icons.close),
                splashRadius: 20,
              )
            : null,
        isRequiredNotEmpty: widget.isRequiredNotEmpty,
      );
    } else {
      return const SizedBox.shrink();
    }
  }

  Widget listBtnOption() {
    return Row(
      children: [
        const Expanded(
          child: SizedBox.shrink(),
        ),
        Expanded(
          flex: 3,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              if (widget.variablePath["dataType"] == "WebElement" && widget.variablePath["type"] == "value" && isStartDebug)
                Container(
                  margin: const EdgeInsets.only(right: 10),
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(backgroundColor: const Color.fromARGB(255, 255, 63, 49), fixedSize: const Size(130, 50)),
                      onPressed: () {
                        Navigator.of(context).push(
                          createRoute(
                            WebElementConfigWidget((xpath, element) {
                              setState(() {
                                widget.variablePath["value"] = xpath["xpath"];
                                widget.callback();
                              });
                            }, clientName: widget.clientName, platform: widget.platform),
                          ),
                        );
                      },
                      child: const MultiLanguageText(
                        name: "find_element",
                        defaultValue: "Find element",
                      )),
                ),
              if (widget.variablePath["type"] == "variable")
                Container(
                  margin: const EdgeInsets.only(right: 10),
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(fixedSize: const Size(130, 50)),
                      onPressed: () {
                        Navigator.of(context).push(
                          createRoute(
                            FindVariableWidget((valueCallback) {
                              setState(() {
                                widget.keyForm!.currentState!.validate();
                                widget.variablePath["name"] = valueCallback["name"];
                                widget.variablePath["dataType"] = valueCallback["dataType"];
                                widget.callback();
                              });
                            }),
                          ),
                        );
                      },
                      child: const MultiLanguageText(
                        name: "find_variable",
                        defaultValue: "Find variable",
                      )),
                ),
              if (widget.variablePath["type"] == "value" && widget.variablePath["dataType"] != "RepositoryElement")
                Container(
                  margin: const EdgeInsets.only(right: 10),
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(fixedSize: const Size(120, 50)),
                      onPressed: () {
                        Navigator.of(context).push(
                          createRoute(
                            FindTemplateWidget((valueCallback) {
                              setState(() {
                                if (widget.variablePath.containsKey("tfTestElementId")) {
                                  widget.variablePath.remove("tfTestElementId");
                                }
                                if (widget.variablePath.containsKey("tfTestElementRepositoryId")) {
                                  widget.variablePath.remove("tfTestElementRepositoryId");
                                }
                                widget.variablePath["tfValueTemplateId"] = valueCallback["id"];
                                widget.variablePath["value"] = valueCallback["value"];
                              });
                              getElementTemplate(widget.variablePath["tfValueTemplateId"]);
                              widget.callback();
                            }),
                          ),
                        );
                      },
                      child: const MultiLanguageText(
                        name: "find_value",
                        defaultValue: "Find value",
                      )),
                ),
              if (widget.variablePath["type"] == "value" && widget.variablePath["dataType"] == "RepositoryElement")
                Container(
                  margin: const EdgeInsets.only(right: 10),
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(fixedSize: const Size(170, 50)),
                      onPressed: () {
                        Navigator.of(context).push(
                          createRoute(
                            ElementRepositoryConfigWidget((parentId, valueCallback, xpath, name) {
                              setState(() {
                                if (widget.variablePath.containsKey("tfValueTemplateId")) {
                                  widget.variablePath.remove("tfValueTemplateId");
                                }
                                widget.variablePath["value"] = '';
                                widget.variablePath["tfTestElementId"] = valueCallback["id"];
                                widget.variablePath["tfTestElementRepositoryId"] = parentId;
                                widget.variablePath["tfTestElementXpathName"] = name;
                              });
                              getElementRepositoryPath(widget.variablePath["tfTestElementRepositoryId"], widget.variablePath["tfTestElementId"],
                                  name: widget.variablePath["tfTestElementXpathName"]);
                              widget.callback();
                            }),
                          ),
                        );
                      },
                      child: const MultiLanguageText(
                        name: "element_repository",
                        defaultValue: "Element Repository",
                      )),
                )
            ],
          ),
        ),
      ],
    );
  }
}
