import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';

import '../../common/custom_state.dart';
import '../../components/menu_tab.dart';
import '../../model/model.dart';

class Password extends StatefulWidget {
  const Password({Key? key}) : super(key: key);

  @override
  State<Password> createState() => _PasswordState();
}

class _PasswordState extends CustomState<Password> {
  bool _isVisibleOldPass = true;
  bool _isVisibleNewPass = true;
  bool _isVisibleConfirmNewPass = true;
  TextEditingController oldPassword = TextEditingController();
  TextEditingController newPassword = TextEditingController();
  TextEditingController newPasswordConfirm = TextEditingController();
  dynamic userCurrent;

  void visibleOldPass() {
    setState(() {
      _isVisibleOldPass = !_isVisibleOldPass;
    });
  }

  void visibleNewPass() {
    setState(() {
      _isVisibleNewPass = !_isVisibleNewPass;
    });
  }

  void visibleConfirmNewPass() {
    setState(() {
      _isVisibleConfirmNewPass = !_isVisibleConfirmNewPass;
    });
  }

  handleChangePassword(context) async {
    if (newPassword.text != newPasswordConfirm.text) {
      showToast(
          context: context,
          msg: multiLanguageString(
              name: "new_password_and_confirm_password_dont_match", defaultValue: "New password and confirm password don't match", context: context),
          color: Colors.red,
          icon: const Icon(Icons.info));
    } else {
      var hashMethod = "SHA256";
      try {
        var hashMethodResponse = await httpGet("/test-framework-api/auth/hash-method", context);
        hashMethod = hashMethodResponse["body"]["result"];
      } catch (error) {
        //bypass
      }
      var oldPasswordValue = oldPassword.text;
      var newPasswordValue = newPassword.text;
      if ("SHA256" == hashMethod) {
        var passwordBytes = utf8.encode(newPassword.text);
        var passwordDigest = sha256.convert(passwordBytes);
        var oldPasswordBytes = utf8.encode(oldPassword.text);
        var oldPasswordDigest = sha256.convert(oldPasswordBytes);
        oldPasswordValue = base64.encode(oldPasswordDigest.bytes);
        newPasswordValue = base64.encode(passwordDigest.bytes);
      }
      if (mounted) {
        var findRequest = {"password": newPasswordValue, "oldPassword": oldPasswordValue};
        var response = await httpPost("/test-framework-api/auth/user-password", findRequest, context);
        if (response.containsKey("body") && response["body"] is String == false) {
          showToast(
              context: context,
              msg: multiLanguageString(name: "change_password_success", defaultValue: "Change password success", context: context),
              color: Colors.greenAccent,
              icon: const Icon(Icons.done));
          NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
          if (navigationModel.product == "qa-platform") {
            navigationModel.navigate("/qa-platform/project/${navigationModel.prjProjectId}/insights/dashboard");
          } else if (navigationModel.product == "resource-pool") {
            navigationModel.navigate("/resource-pool/insights");
          } else if (navigationModel.product == "admin") {
            navigationModel.navigate("/admin/dashboard");
          } else if (navigationModel.product == "workflow") {
            navigationModel.navigate("/workflow/status");
          } else if (navigationModel.product == "prj-360") {
            navigationModel.navigate("/prj-360/dashboard");
          } else {
            navigationModel.navigate("/${navigationModel.product}/project/${navigationModel.prjProjectId}/summary");
          }
        } else {
          showToast(
              context: context,
              msg: multiLanguageString(name: "old_password_is_not_correct", defaultValue: "Old password is not correct !!!", context: context),
              color: Colors.red,
              icon: const Icon(Icons.info));
        }
      }
    }
  }

  @override
  void initState() {
    userCurrent = Provider.of<SecurityModel>(context, listen: false).userLoginCurren;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<NavigationModel>(builder: (context, navigationModel, child) {
      return Scaffold(
        body: Column(
          children: [
            Container(
              padding: const EdgeInsets.fromLTRB(45, 30, 45, 10),
              color: const Color.fromRGBO(49, 61, 80, 1),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      MultiLanguageText(
                        name: "account",
                        defaultValue: "Account",
                        style: TextStyle(
                          fontSize: context.fontSizeManager.fontSizeText40,
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      InkWell(
                        child: const Icon(
                          Icons.cancel_outlined,
                          size: 40,
                          color: Colors.white,
                        ),
                        onTap: () {
                          if (navigationModel.product == "qa-platform") {
                            navigationModel.navigate("/qa-platform/project/${navigationModel.prjProjectId}/insights/dashboard");
                          } else if (navigationModel.product == "resource-pool") {
                            navigationModel.navigate("/resource-pool/insights");
                          } else if (navigationModel.product == "admin") {
                            navigationModel.navigate("/admin/dashboard");
                          } else if (navigationModel.product == "workflow") {
                            navigationModel.navigate("/workflow/status");
                          } else if (navigationModel.product == "prj-360") {
                            navigationModel.navigate("/prj-360/dashboard");
                          } else {
                            navigationModel.navigate("/${navigationModel.product}/project/${navigationModel.prjProjectId}/summary");
                          }
                        },
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      NavMenuTabV1(
                        currentUrl: "/profile/password",
                        margin: const EdgeInsets.only(top: 20),
                        navigationList: context.profileMenuList,
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          width: 400,
                          margin: const EdgeInsets.only(top: 60),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                                child: MultiLanguageText(
                                  name: "old_password",
                                  defaultValue: "Old Password",
                                  style: TextStyle(
                                    fontSize: context.fontSizeManager.fontSizeText15,
                                    color: const Color.fromRGBO(49, 49, 49, 1),
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 60,
                                child: TextFormField(
                                  controller: oldPassword,
                                  keyboardType: TextInputType.text,
                                  obscureText: _isVisibleOldPass,
                                  decoration: InputDecoration(
                                    hintText: multiLanguageString(name: "enter_old_password", defaultValue: "Enter Old Password", context: context),
                                    suffixIcon: IconButton(
                                      splashRadius: 1,
                                      onPressed: () => visibleOldPass(),
                                      icon: Icon(_isVisibleOldPass ? Icons.visibility : Icons.visibility_off),
                                    ),
                                    border: const OutlineInputBorder(),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                child: MultiLanguageText(
                                  name: "new_password",
                                  defaultValue: "New Password",
                                  style: TextStyle(
                                    fontSize: context.fontSizeManager.fontSizeText15,
                                    color: const Color.fromRGBO(49, 49, 49, 1),
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 60,
                                child: TextFormField(
                                  controller: newPassword,
                                  keyboardType: TextInputType.text,
                                  obscureText: _isVisibleNewPass,
                                  decoration: InputDecoration(
                                    hintText: multiLanguageString(name: "enter_new_password", defaultValue: "Enter New Password", context: context),
                                    suffixIcon: IconButton(
                                      splashRadius: 1,
                                      onPressed: () => visibleNewPass(),
                                      icon: Icon(_isVisibleNewPass ? Icons.visibility : Icons.visibility_off),
                                    ),
                                    border: const OutlineInputBorder(),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                child: MultiLanguageText(
                                  name: "confirm_password",
                                  defaultValue: "Confirm Password",
                                  style: TextStyle(
                                    fontSize: context.fontSizeManager.fontSizeText15,
                                    color: const Color.fromRGBO(49, 49, 49, 1),
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 60,
                                child: TextFormField(
                                  controller: newPasswordConfirm,
                                  keyboardType: TextInputType.text,
                                  obscureText: _isVisibleConfirmNewPass,
                                  decoration: InputDecoration(
                                    hintText:
                                        multiLanguageString(name: "enter_confirm_password", defaultValue: "Enter Confirm Password", context: context),
                                    suffixIcon: IconButton(
                                      splashRadius: 1,
                                      onPressed: () => visibleConfirmNewPass(),
                                      icon: Icon(_isVisibleConfirmNewPass ? Icons.visibility : Icons.visibility_off),
                                    ),
                                    border: const OutlineInputBorder(),
                                  ),
                                ),
                              ),
                              Container(
                                margin: const EdgeInsets.only(top: 10),
                                height: 50,
                                width: 400,
                                child: ElevatedButton(
                                  onPressed: () {
                                    handleChangePassword(context);
                                  },
                                  child: const MultiLanguageText(name: "update_password", defaultValue: "Update Password", isLowerCase: false),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      );
    });
  }
}
