import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import '../../api.dart';
import '../../common/custom_state.dart';
import '../../components/menu_tab.dart';
import '../../model/model.dart';

class ProfileView extends StatefulWidget {
  const ProfileView({Key? key}) : super(key: key);
  @override
  State<ProfileView> createState() => _ProfileViewState();
}

class _ProfileViewState extends CustomState<ProfileView> {
  TextEditingController fullNameAccount = TextEditingController();
  var userConfig = {};
  final _formProfileKey = GlobalKey<FormState>();

  @override
  void initState() {
    var securityModel = Provider.of<SecurityModel>(context, listen: false);
    fullNameAccount.text = securityModel.userLoginCurren["fullname"];
    if (securityModel.userLoginCurren["userConfig"] != null && securityModel.userLoginCurren["userConfig"].isNotEmpty) {
      userConfig = securityModel.userLoginCurren["userConfig"];
      userConfig["notification"] ??= {"email": true};
    } else {
      userConfig = {
        "jenkins_username": null,
        "jenkins_token": null,
        "notification": {"email": true}
      };
    }
    super.initState();
  }

  @override
  void dispose() {
    fullNameAccount.dispose();
    super.dispose();
  }

  String getFirstWord(String text) => text.isNotEmpty ? text.trim().split(' ').map((l) => l[0]).take(2).join() : '';
  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, SecurityModel>(builder: (context, navigationModel, securityModel, child) {
      return Scaffold(
        body: Column(
          children: [
            tabPageWidget(context, navigationModel),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(top: 20),
                      child: Form(
                        key: _formProfileKey,
                        child: IntrinsicWidth(
                          child: Column(
                            children: [
                              headerProfile(securityModel, context),
                              formProfile(context, securityModel),
                              tableUserConfig(),
                              btnWidget(securityModel, context)
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      );
    });
  }

  Container btnWidget(SecurityModel securityModel, BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
      height: 50,
      width: 400,
      child: ElevatedButton(
        onPressed: () async {
          if (_formProfileKey.currentState!.validate()) {
            var findRequest = securityModel.userLoginCurren;
            findRequest["fullname"] = fullNameAccount.text;
            findRequest["userConfig"] = userConfig;
            onLoading(context);
            await httpPost("/test-framework-api/user/sys/sys-user/${securityModel.userLoginCurren["id"]}", findRequest, context);
            if (mounted) {
              Navigator.pop(this.context);
              showToast(
                  context: this.context,
                  msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: this.context),
                  color: Colors.greenAccent,
                  icon: const Icon(Icons.done));
            }
          }
        },
        child: const MultiLanguageText(name: "update", defaultValue: "Update", isLowerCase: false),
      ),
    );
  }

  Column formProfile(BuildContext context, SecurityModel securityModel) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 5),
          child: MultiLanguageText(
            name: "name",
            defaultValue: "Name",
            style: TextStyle(
              fontSize: context.fontSizeManager.fontSizeText15,
              color: const Color.fromRGBO(49, 49, 49, 1),
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        DynamicTextField(
          controller: fullNameAccount,
          isRequiredNotEmpty: true,
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 5),
          child: MultiLanguageText(
            name: "email",
            defaultValue: "Email",
            style: TextStyle(
              fontSize: context.fontSizeManager.fontSizeText15,
              color: const Color.fromRGBO(49, 49, 49, 1),
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        DynamicTextField(
          controller: TextEditingController(text: securityModel.userLoginCurren["email"]),
          readOnly: true,
        ),
      ],
    );
  }

  SizedBox tableUserConfig() {
    return SizedBox(
      width: 800,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Row(
              children: [
                Expanded(
                  flex: 2,
                  child: Text(
                    "Jenkins Username",
                    style: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText14,
                      color: const Color.fromRGBO(49, 49, 49, 1),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Expanded(
                  flex: 8,
                  child: DynamicTextField(
                    controller: TextEditingController(text: userConfig["jenkins_username"] ?? ""),
                    hintText: multiLanguageString(name: "enter_a_value", defaultValue: "Enter a value", context: context),
                    onChanged: (text) {
                      userConfig["jenkins_username"] = text;
                    },
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Row(
              children: [
                Expanded(
                  flex: 2,
                  child: Text(
                    "Jenkins Token",
                    style: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText14,
                      color: const Color.fromRGBO(49, 49, 49, 1),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Expanded(
                  flex: 8,
                  child: DynamicTextField(
                    controller: TextEditingController(text: userConfig["jenkins_token"] ?? ""),
                    hintText: multiLanguageString(name: "enter_a_value", defaultValue: "Enter a value", context: context),
                    onChanged: (text) {
                      userConfig["jenkins_token"] = text;
                    },
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Row(
              children: [
                Expanded(
                  flex: 2,
                  child: Text(
                    "Notification",
                    style: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText14,
                      color: const Color.fromRGBO(49, 49, 49, 1),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Expanded(
                  flex: 8,
                  child: Column(
                    children: [
                      for (var notificationKey in userConfig["notification"].keys ?? [])
                        CheckboxListTile(
                          contentPadding: const EdgeInsets.all(0),
                          controlAffinity: ListTileControlAffinity.leading,
                          value: userConfig["notification"][notificationKey] ?? false,
                          onChanged: (bool? selected) {
                            setState(() {
                              userConfig["notification"][notificationKey] = selected!;
                            });
                          },
                          title: Text(notificationKey),
                        ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Row headerProfile(SecurityModel securityModel, BuildContext context) {
    return Row(
      children: [
        Container(
            width: 130,
            height: 130,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(75),
              color: const Color.fromRGBO(121, 134, 203, 1),
            ),
            alignment: Alignment.center,
            child: Text(
              getFirstWord(securityModel.userLoginCurren['nickname']).toUpperCase(),
              textAlign: TextAlign.center,
              style: const TextStyle(
                fontSize: 60,
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            )),
        Container(
          margin: const EdgeInsets.only(left: 20),
          child: MultiLanguageText(
            name: "profile_picture",
            defaultValue: "Profile picture",
            style: TextStyle(
              fontSize: context.fontSizeManager.fontSizeText20,
            ),
          ),
        ),
      ],
    );
  }

  Container tabPageWidget(BuildContext context, NavigationModel navigationModel) {
    return Container(
      padding: const EdgeInsets.fromLTRB(45, 30, 45, 10),
      color: const Color.fromRGBO(49, 61, 80, 1),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MultiLanguageText(
                name: "account",
                defaultValue: "Account",
                style: TextStyle(
                  fontSize: context.fontSizeManager.fontSizeText40,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
              InkWell(
                child: const Icon(
                  Icons.cancel_outlined,
                  size: 40,
                  color: Colors.white,
                ),
                onTap: () {
                  if (navigationModel.product == "qa-platform") {
                    navigationModel.navigate("/qa-platform/project/${navigationModel.prjProjectId}/insights/dashboard");
                  } else if (navigationModel.product == "resource-pool") {
                    navigationModel.navigate("/resource-pool/insights");
                  } else if (navigationModel.product == "admin") {
                    navigationModel.navigate("/admin/dashboard");
                  } else if (navigationModel.product == "workflow") {
                    navigationModel.navigate("/workflow/status");
                  } else if (navigationModel.product == "prj-360") {
                    navigationModel.navigate("/prj-360/dashboard");
                  } else {
                    navigationModel.navigate("/${navigationModel.product}/project/${navigationModel.prjProjectId}/summary");
                  }
                },
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              NavMenuTabV1(
                currentUrl: "/profile/view",
                margin: const EdgeInsets.only(top: 20),
                navigationList: context.profileMenuList,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
