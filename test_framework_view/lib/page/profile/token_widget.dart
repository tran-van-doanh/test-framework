import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';

import '../../api.dart';
import '../../common/custom_state.dart';
import '../../common/dynamic_dropdown_button.dart';
import '../../components/badge/badge_icon.dart';
import '../../components/dynamic_button.dart';
import '../../components/toast.dart';
import '../../model/model.dart';

class TokenWidget extends StatefulWidget {
  const TokenWidget({Key? key}) : super(key: key);

  @override
  State<TokenWidget> createState() => _TokenWidgetState();
}

class _TokenWidgetState extends CustomState<TokenWidget> {
  late Future futureListToken;
  var resultListToken = [];
  var rowCountListToken = 0;
  var currentPage = 1;
  var rowPerPage = 10;

  @override
  void initState() {
    futureListToken = getListToken(currentPage);
    super.initState();
  }

  Future getListToken(page) async {
    if ((page - 1) * rowPerPage > rowCountListToken) {
      page = (1.0 * rowCountListToken / rowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * rowPerPage, "queryLimit": rowPerPage};
    var response = await httpPost("/test-framework-api/user/sys/sys-token/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPage = page;
        rowCountListToken = response["body"]["rowCount"];
        resultListToken = response["body"]["resultList"];
      });
    }
    return 0;
  }

  onInactiveToken(id) async {
    var response = await httpDelete("/test-framework-api/user/sys/sys-token/$id/inactive", context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "inactive_successful", defaultValue: "Inactive successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        await getListToken(currentPage);
      }
    }
  }

  onGenerateToken(tokenType) async {
    var requestBody = {"tokenType": tokenType};
    var response = await httpPut("/test-framework-api/user/sys/sys-token", requestBody, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "generate_successful", defaultValue: "Generate successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
      }
    }
    return response["body"]["result"];
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<NavigationModel>(
      builder: (context, navigationModel, child) {
        return FutureBuilder(
          future: futureListToken,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Scaffold(
                body: Column(
                  children: [
                    tabPageWidget(context, navigationModel),
                    Expanded(
                      child: ListView(
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(50, 20, 50, 30),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                MultiLanguageText(
                                  name: "token_count",
                                  defaultValue: "Token ( \$0 )",
                                  variables: ["$rowCountListToken"],
                                  style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
                                ),
                                tableWidget(context),
                                SelectionArea(
                                  child: DynamicTablePagging(
                                    rowCountListToken,
                                    currentPage,
                                    rowPerPage,
                                    pageChangeHandler: (page) {
                                      setState(() {
                                        futureListToken = getListToken(page);
                                      });
                                    },
                                    rowPerPageChangeHandler: (rowPerPage) {
                                      setState(() {
                                        this.rowPerPage = rowPerPage!;
                                        futureListToken = getListToken(1);
                                      });
                                    },
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    btnWidget(),
                  ],
                ),
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  Padding btnWidget() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 40, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            margin: const EdgeInsets.only(right: 10),
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              onPressed: () {
                dialogGenerateToken();
              },
              icon: const Icon(Icons.note_add, color: Color.fromRGBO(49, 49, 49, 1)),
              label: MultiLanguageText(
                name: "generate_token",
                defaultValue: "Generate token",
                isLowerCase: false,
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
            ),
          ),
        ],
      ),
    );
  }

  Container tabPageWidget(BuildContext context, NavigationModel navigationModel) {
    return Container(
      padding: const EdgeInsets.fromLTRB(45, 30, 45, 10),
      color: const Color.fromRGBO(49, 61, 80, 1),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MultiLanguageText(
                name: "account",
                defaultValue: "Account",
                style: TextStyle(
                  fontSize: context.fontSizeManager.fontSizeText40,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
              InkWell(
                child: const Icon(
                  Icons.cancel_outlined,
                  size: 40,
                  color: Colors.white,
                ),
                onTap: () {
                  if (navigationModel.product == "qa-platform") {
                    navigationModel.navigate("/qa-platform/project/${navigationModel.prjProjectId}/insights/dashboard");
                  } else if (navigationModel.product == "resource-pool") {
                    navigationModel.navigate("/resource-pool/insights");
                  } else if (navigationModel.product == "admin") {
                    navigationModel.navigate("/admin/dashboard");
                  } else if (navigationModel.product == "workflow") {
                    navigationModel.navigate("/workflow/status");
                  } else if (navigationModel.product == "prj-360") {
                    navigationModel.navigate("/prj-360/dashboard");
                  } else {
                    navigationModel.navigate("/${navigationModel.product}/project/${navigationModel.prjProjectId}/summary");
                  }
                },
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              NavMenuTabV1(
                currentUrl: "/profile/token",
                margin: const EdgeInsets.only(top: 20),
                navigationList: context.profileMenuList,
              ),
            ],
          ),
        ],
      ),
    );
  }

  SelectionArea tableWidget(BuildContext context) {
    return SelectionArea(
      child: Container(
        margin: const EdgeInsets.only(top: 30),
        child: CustomDataTableWidget(
          columns: [
            Expanded(
              flex: 2,
              child: MultiLanguageText(
                name: "type",
                defaultValue: "Type",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              flex: 2,
              child: MultiLanguageText(
                name: "create_date",
                defaultValue: "Create date",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  MultiLanguageText(
                    name: "status",
                    defaultValue: "Status",
                    isLowerCase: false,
                    style: Style(context).styleTextDataColumn,
                  ),
                  Tooltip(
                    message: multiLanguageString(name: "active_inactive", defaultValue: "Green = Active\nRed = Inactive", context: context),
                    child: const Icon(
                      Icons.info,
                      color: Colors.grey,
                      size: 16,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: 38,
            ),
          ],
          rows: [
            for (var resultToken in resultListToken)
              CustomDataRow(
                cells: [
                  Expanded(
                    flex: 2,
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        resultToken["tokenType"] ?? "",
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        resultToken["createDate"] != null
                            ? DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultToken["createDate"]).toLocal())
                            : "",
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Center(
                      child: Badge2StatusSettingWidget(
                        status: resultToken["status"],
                      ),
                    ),
                  ),
                  PopupMenuButton(
                    offset: const Offset(5, 50),
                    tooltip: '',
                    splashRadius: 10,
                    itemBuilder: (context) => [
                      PopupMenuItem(
                        padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                        child: ListTile(
                          onTap: () {
                            Navigator.pop(context);
                            dialogInactiveToken(resultToken);
                          },
                          contentPadding: const EdgeInsets.all(0),
                          hoverColor: Colors.transparent,
                          title: const MultiLanguageText(name: "inactive", defaultValue: "Inactive"),
                          leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
          ],
        ),
      ),
    );
  }

  dialogInactiveToken(selectItem) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: const EdgeInsets.all(0),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MultiLanguageText(
                name: "inactive",
                defaultValue: "Inactive",
                isLowerCase: false,
                style: Style(context).styleTitleDialog,
              ),
              TextButton(
                  onPressed: () => Navigator.pop(context, 'Cancel'),
                  child: const Icon(
                    Icons.close,
                    color: Colors.black,
                  ))
            ],
          ),
          content: Container(
            decoration: const BoxDecoration(
                border:
                    Border(top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)), bottom: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)))),
            margin: const EdgeInsets.only(top: 20, bottom: 10),
            width: 500,
            padding: const EdgeInsets.all(20),
            child: IntrinsicHeight(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const MultiLanguageText(name: "inactive_token_confirm", defaultValue: "Are you sure you want to inactive this token ?"),
                  Container(
                    margin: const EdgeInsets.only(top: 20),
                    decoration: BoxDecoration(
                      color: const Color.fromRGBO(228, 115, 87, 1),
                      borderRadius: BorderRadius.circular(5),
                    ),
                    padding: const EdgeInsets.only(left: 5),
                    child: Container(
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.horizontal(
                          right: Radius.circular(5),
                        ),
                        color: Color.fromRGBO(255, 233, 217, 1),
                      ),
                      padding: const EdgeInsets.all(15),
                      child: const Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Icon(Icons.warning, color: Color.fromRGBO(243, 86, 40, 1)),
                          SizedBox(width: 10),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              MultiLanguageText(
                                name: "warning",
                                defaultValue: "Warning",
                                style: TextStyle(color: Color.fromARGB(255, 206, 20, 20), fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 10),
                              MultiLanguageText(
                                name: "cant_undo_action",
                                defaultValue: "You can't undo this action.",
                                style: TextStyle(color: Color.fromARGB(255, 187, 103, 82)),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          actionsAlignment: MainAxisAlignment.center,
          actions: [
            Container(
              margin: const EdgeInsets.only(right: 5),
              height: 40,
              width: 120,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color>(
                    const Color.fromRGBO(225, 46, 59, 1),
                  ),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                  onInactiveToken(selectItem["id"]);
                },
                child: const MultiLanguageText(
                  name: "ok",
                  defaultValue: "Ok",
                  isLowerCase: false,
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
            const ButtonCancel()
          ],
        );
      },
    );
  }

  dialogGenerateToken() {
    String? tokenType;
    String? generateTokenResponse;
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder(builder: (BuildContext context, StateSetter setState) {
          return AlertDialog(
            contentPadding: const EdgeInsets.all(0),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                MultiLanguageText(
                  name: "generate_token",
                  defaultValue: "Generate token",
                  isLowerCase: false,
                  style: Style(context).styleTitleDialog,
                ),
                TextButton(
                    onPressed: () => Navigator.pop(context, 'Cancel'),
                    child: const Icon(
                      Icons.close,
                      color: Colors.black,
                    ))
              ],
            ),
            content: Container(
              decoration: const BoxDecoration(
                  border:
                      Border(top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)), bottom: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)))),
              margin: const EdgeInsets.only(top: 20, bottom: 10),
              width: 1000,
              padding: const EdgeInsets.all(20),
              child: IntrinsicHeight(
                child: Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 3,
                            child: MultiLanguageText(name: "token_type", defaultValue: "Token type", style: Style(context).styleTitleDialog),
                          ),
                          Expanded(
                              flex: 7,
                              child: DynamicDropdownButton(
                                value: tokenType,
                                hint: multiLanguageString(name: "choose_a_token_type", defaultValue: "Choose a token type", context: context),
                                items: [
                                  {"label": multiLanguageString(name: "item_system", defaultValue: "System", context: context), "value": "SYSTEM"}
                                ].map<DropdownMenuItem<String>>((var result) {
                                  return DropdownMenuItem(
                                    value: result["value"],
                                    child: Text(result["label"]!),
                                  );
                                }).toList(),
                                onChanged: (newValue) async {
                                  setState(() {
                                    tokenType = newValue;
                                  });
                                },
                              )),
                        ],
                      ),
                    ),
                    if (generateTokenResponse != null)
                      Container(
                        margin: const EdgeInsets.symmetric(vertical: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            MultiLanguageText(name: "response", defaultValue: "Response", style: Style(context).styleTitleDialog),
                            Container(
                              alignment: Alignment.centerLeft,
                              padding: const EdgeInsets.all(10),
                              margin: const EdgeInsets.only(top: 5),
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: const Color.fromRGBO(212, 212, 212, 1),
                                ),
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: SelectionArea(child: Text(generateTokenResponse!)),
                            ),
                          ],
                        ),
                      ),
                  ],
                ),
              ),
            ),
            actionsAlignment: MainAxisAlignment.center,
            actions: [
              ConstrainedBox(
                constraints: const BoxConstraints(minHeight: 40, minWidth: 120),
                child: ElevatedButton(
                  onPressed: tokenType != null
                      ? () async {
                          String response = await onGenerateToken(tokenType);
                          setState(() {
                            generateTokenResponse = response;
                          });
                        }
                      : null,
                  child: const MultiLanguageText(
                    name: "generate",
                    defaultValue: "Generate",
                    isLowerCase: false,
                    style: TextStyle(fontWeight: FontWeight.bold, letterSpacing: 2),
                  ),
                ),
              ),
              const ButtonCancel()
            ],
          );
        });
      },
    );
  }
}
