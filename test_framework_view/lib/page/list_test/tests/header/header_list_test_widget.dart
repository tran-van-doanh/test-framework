import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';

import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import '../../../../api.dart';
import '../../../../common/custom_state.dart';
import '../../../../common/dynamic_dropdown_button.dart';

class HeaderListTestWidget extends StatefulWidget {
  final String type;
  final int? countList;
  final String title;
  final Function? onChangedDirectory;
  final bool? isDirectory;
  final Function? callbackFilter;
  final Function? callbackDelete;
  final Function? callbackExport;
  final Function? callbackImport;

  const HeaderListTestWidget({
    Key? key,
    required this.type,
    this.countList,
    this.callbackFilter,
    this.callbackDelete,
    this.callbackExport,
    this.callbackImport,
    required this.title,
    this.onChangedDirectory,
    this.isDirectory,
  }) : super(key: key);

  @override
  State<HeaderListTestWidget> createState() => _HeaderListTestWidgetState();
}

class _HeaderListTestWidgetState extends CustomState<HeaderListTestWidget> {
  final TextEditingController _searchLibrary = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  TextEditingController startDate = TextEditingController();
  TextEditingController endDate = TextEditingController();
  SingleValueDropDownController testCaseTypeController = SingleValueDropDownController();
  SingleValueDropDownController statusController = SingleValueDropDownController();
  SingleValueDropDownController statisticsStatusController = SingleValueDropDownController();
  SingleValueDropDownController versionController = SingleValueDropDownController();
  SingleValueDropDownController environmentController = SingleValueDropDownController();
  SingleValueDropDownController componentController = SingleValueDropDownController();
  SingleValueDropDownController testOwnerController = SingleValueDropDownController();
  SingleValueDropDownController testRunOwnerController = SingleValueDropDownController();
  SingleValueDropDownController labelController = SingleValueDropDownController();
  SingleValueDropDownController testPriorityController = SingleValueDropDownController();
  SingleValueDropDownController regressionCandidateController = SingleValueDropDownController();

  var prjProjectId = "";
  var _listVersions = [];
  var _listEnvironment = [];
  var _listComponents = [];
  var _listTestOwner = [];
  var _listLabel = [];
  bool isShowFilterForm = true;
  bool isSearched = false;

  getLabelList() async {
    var findRequest = {"prjProjectId": prjProjectId, "status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-label/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listLabel = response["body"]["resultList"];
      });
    }
    return _listLabel;
  }

  getVersionList() async {
    var findRequest = {"prjProjectId": prjProjectId, "status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-version/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listVersions = response["body"]["resultList"];
      });
    }
    return _listVersions;
  }

  getEnvironmentList() async {
    var findRequest = {"prjProjectId": prjProjectId, "status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-environment/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listEnvironment = response["body"]["resultList"];
      });
    }
    return _listEnvironment;
  }

  getComponentList() async {
    var findRequest = {"prjProjectId": prjProjectId, "status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-component/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listComponents = response["body"]["resultList"];
      });
    }
    return _listComponents;
  }

  getListOwner() async {
    var requestBody = {"prjProjectId": prjProjectId};
    var response = await httpPost("/test-framework-api/user/sys/sys-user-project/search", requestBody, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      _listTestOwner = response["body"]["resultList"];
    }
  }

  @override
  void initState() {
    prjProjectId = Provider.of<NavigationModel>(context, listen: false).prjProjectId ?? "";
    getInfoFilter();
    super.initState();
  }

  getInfoFilter() async {
    if (widget.type == "Directory" ||
        widget.type == "Test case batch step" ||
        widget.type == "Test case" ||
        widget.type == "All" ||
        widget.type == "Test scenario" ||
        widget.type == "Api" ||
        widget.type == "Test manual" ||
        widget.type == "Test suite" ||
        widget.type == "Test Case Runs" ||
        widget.type == "Test Scenario Runs" ||
        widget.type == "Test Manual Runs" ||
        widget.type == "Test Suite Runs" ||
        widget.type == "Delete Test Scenario Runs" ||
        widget.type == "Delete Test Case Runs" ||
        widget.type == "Delete Test Suite Runs" ||
        widget.type == "Delete Test Manual Runs" ||
        widget.type == "Shared steps") {
      await getComponentList();
    }
    await getListOwner();
    await getLabelList();
    await getVersionList();
    await getEnvironmentList();
  }

  handleCallBackSearchFunction({String type = "search"}) {
    var result = {};
    if (widget.type == "Test Scenario Runs" ||
        widget.type == "Test Case Runs" ||
        widget.type == "Test Suite Runs" ||
        widget.type == "Test Manual Runs" ||
        widget.type == "Delete Test Scenario Runs" ||
        widget.type == "Delete Test Case Runs" ||
        widget.type == "Delete Test Suite Runs" ||
        widget.type == "Delete Test Manual Runs") {
      result = {
        "status": (statusController.dropDownValue?.name != "Processing" && statusController.dropDownValue?.name != "Error")
            ? statusController.dropDownValue?.value
            : "",
        "statusGt": (statusController.dropDownValue?.name == "Processing") ? statusController.dropDownValue?.value : "",
        "statusLt": (statusController.dropDownValue?.name == "Error") ? statusController.dropDownValue?.value : "",
        "statisticsStatus": statisticsStatusController.dropDownValue?.value,
        "tfTestCaseTfTestComponentId": componentController.dropDownValue?.value,
        "sysUserId": testRunOwnerController.dropDownValue?.value,
        "tfTestCaseTfTestVersionId": versionController.dropDownValue?.value,
        "tfTestCaseTfTestEnvironmentId": environmentController.dropDownValue?.value,
        "tfTestCaseSysUserId": testOwnerController.dropDownValue?.value,
        "tfTestCaseTitleLike": _searchLibrary.text,
        "createDateGt": (startDate.text != "") ? "${DateFormat('dd-MM-yyyy HH:mm').parse(startDate.text).toIso8601String()}+07:00" : "",
        "createDateLt": (endDate.text != "") ? "${DateFormat('dd-MM-yyyy HH:mm').parse(endDate.text).toIso8601String()}+07:00" : "",
        "tfTestCaseNameLike": _nameController.text,
      };
    }
    if (widget.type == "Delete Test Batch Runs") {
      result = {
        "status": statusController.dropDownValue?.value,
        "sysUserId": testRunOwnerController.dropDownValue?.value,
        "tfTestBatchTitleLike": _searchLibrary.text,
        "createDateGt": (startDate.text != "") ? "${DateFormat('dd-MM-yyyy HH:mm').parse(startDate.text).toIso8601String()}+07:00" : "",
        "createDateLt": (endDate.text != "") ? "${DateFormat('dd-MM-yyyy HH:mm').parse(endDate.text).toIso8601String()}+07:00" : "",
        "tfTestBatchNameLike": _nameController.text,
      };
    }
    if (widget.type == "Directory" ||
        widget.type == "Test case batch step" ||
        widget.type == "Test case" ||
        widget.type == "All" ||
        widget.type == "Test suite" ||
        widget.type == "Test scenario" ||
        widget.type == "Api" ||
        widget.type == "Test manual" ||
        widget.type == "Shared steps") {
      result = {
        if (widget.type == "All") "testCaseType": testCaseTypeController.dropDownValue?.value,
        "status": statusController.dropDownValue?.value,
        "labels": labelController.dropDownValue?.value,
        "regressionCandidate": regressionCandidateController.dropDownValue?.value,
        "testPriority": testPriorityController.dropDownValue?.value,
        "tfTestVersionId": versionController.dropDownValue?.value,
        "tfTestEnvironmentId": environmentController.dropDownValue?.value,
        "sysUserId": testOwnerController.dropDownValue?.value,
        "tfTestComponentId": componentController.dropDownValue?.value,
        "titleLike": _searchLibrary.text,
        "nameLike": _nameController.text,
      };
    }
    if (type == "delete") {
      widget.callbackDelete!(result);
    } else if (type == "export") {
      widget.callbackExport!(result);
    } else if (type == "import") {
      widget.callbackImport!();
    } else {
      widget.callbackFilter!(result);
    }
  }

  @override
  void dispose() {
    _searchLibrary.dispose();
    _nameController.dispose();
    startDate.dispose();
    endDate.dispose();
    testCaseTypeController.dispose();
    statusController.dispose();
    statisticsStatusController.dispose();
    versionController.dispose();
    environmentController.dispose();
    componentController.dispose();
    testOwnerController.dispose();
    testRunOwnerController.dispose();
    labelController.dispose();
    testPriorityController.dispose();
    regressionCandidateController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      List<Widget> widgetList = [
        if (widget.type == "All")
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
              child: SizedBox(
                height: 50,
                child: DynamicDropdownSearchClearOption(
                  labelText: multiLanguageString(name: "test_type", defaultValue: "Test type", context: context),
                  controller: testCaseTypeController,
                  hint: multiLanguageString(name: "search_test_type", defaultValue: "Search test type", context: context),
                  items: [
                    {"name": multiLanguageString(name: "test_case", defaultValue: "Test case", context: context), "value": "test_case"},
                    {"name": multiLanguageString(name: "test_scenario", defaultValue: "Test scenario", context: context), "value": "test_scenario"},
                    {"name": multiLanguageString(name: "test_manual", defaultValue: "Test manual", context: context), "value": "manual_test"},
                    {"name": multiLanguageString(name: "test_suite", defaultValue: "Test suite", context: context), "value": "test_suite"},
                    {"name": multiLanguageString(name: "api", defaultValue: "Api", context: context), "value": "test_api"},
                    {"name": multiLanguageString(name: "shared_steps", defaultValue: "Shared steps", context: context), "value": "action"},
                  ].map<DropDownValueModel>((dynamic result) {
                    return DropDownValueModel(
                      value: result["value"],
                      name: result["name"],
                    );
                  }).toList(),
                  maxHeight: 200,
                  onChanged: (dynamic newValue) {
                    if (isSearched && newValue == "") {
                      handleCallBackSearchFunction();
                    }
                    // if (newValue == "") {
                    //   handleCallBackSearchFunction();
                    // }
                  },
                ),
              ),
            ),
          ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
            child: SizedBox(
              height: 50,
              child: DynamicTextField(
                controller: _nameController,
                onComplete: () => handleCallBackSearchFunction(),
                onChanged: (value) {
                  setState(() {
                    _nameController.text;
                  });
                },
                labelText: multiLanguageString(name: "search_test_code", defaultValue: "Search test code", context: context),
                hintText: multiLanguageString(name: "by_code", defaultValue: "By code", context: context),
                suffixIcon: (_nameController.text != "")
                    ? IconButton(
                        onPressed: () {
                          setState(() {
                            _nameController.clear();
                            if (isSearched) {
                              handleCallBackSearchFunction();
                            }
                          });
                        },
                        icon: const Icon(Icons.clear),
                        splashRadius: 20,
                      )
                    : null,
              ),
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
            child: SizedBox(
              height: 50,
              child: DynamicTextField(
                controller: _searchLibrary,
                onComplete: () => handleCallBackSearchFunction(),
                onChanged: (value) {
                  setState(() {
                    _searchLibrary.text;
                  });
                },
                labelText: multiLanguageString(name: "search_test_name", defaultValue: "Search test name", context: context),
                hintText: multiLanguageString(name: "by_name", defaultValue: "By name", context: context),
                suffixIcon: (_searchLibrary.text != "")
                    ? IconButton(
                        onPressed: () {
                          setState(() {
                            _searchLibrary.clear();
                            if (isSearched) {
                              handleCallBackSearchFunction();
                            }
                          });
                        },
                        icon: const Icon(Icons.clear),
                        splashRadius: 20,
                      )
                    : null,
              ),
            ),
          ),
        ),
        if (widget.type != "Delete Test Batch Runs")
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
              child: DynamicDropdownSearchClearOption(
                labelText: multiLanguageString(name: "project_version", defaultValue: "Project version", context: context),
                controller: versionController,
                hint: multiLanguageString(name: "search_version", defaultValue: "Search version", context: context),
                items: _listVersions.map<DropDownValueModel>((dynamic result) {
                  return DropDownValueModel(
                    value: result["id"],
                    name: result["title"],
                  );
                }).toList(),
                maxHeight: 200,
                onChanged: (dynamic newValue) {
                  if (isSearched && newValue == "") {
                    handleCallBackSearchFunction();
                  }
                },
              ),
            ),
          ),
        if (widget.type != "Delete Test Batch Runs")
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
              child: DynamicDropdownSearchClearOption(
                labelText: multiLanguageString(name: "project_environment", defaultValue: "Project environment", context: context),
                controller: environmentController,
                hint: multiLanguageString(name: "search_environment", defaultValue: "Search environment", context: context),
                items: _listEnvironment.map<DropDownValueModel>((dynamic result) {
                  return DropDownValueModel(
                    value: result["id"],
                    name: result["title"],
                  );
                }).toList(),
                maxHeight: 200,
                onChanged: (dynamic newValue) {
                  if (isSearched && newValue == "") {
                    handleCallBackSearchFunction();
                  }
                },
              ),
            ),
          ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
            child: DynamicDropdownSearchClearOption(
              labelText: multiLanguageString(name: "search_status", defaultValue: "Search status", context: context),
              controller: statusController,
              hint: multiLanguageString(name: "search_status", defaultValue: "Search status", context: context),
              items: (widget.type == "Test Scenario Runs" ||
                      widget.type == "Test Case Runs" ||
                      widget.type == "Test Suite Runs" ||
                      widget.type == "Test Manual Runs" ||
                      widget.type == "Delete Test Scenario Runs" ||
                      widget.type == "Delete Test Batch Runs" ||
                      widget.type == "Delete Test Case Runs" ||
                      widget.type == "Delete Test Manual Runs" ||
                      widget.type == "Delete Test Suite Runs")
                  ? {
                      multiLanguageString(name: "pass", defaultValue: "Pass", context: context): 1,
                      multiLanguageString(name: "fail", defaultValue: "Fail", context: context): -1,
                      multiLanguageString(name: "waiting", defaultValue: "Waiting", context: context): 0,
                      multiLanguageString(name: "canceled", defaultValue: "Canceled", context: context): -13,
                      multiLanguageString(name: "error", defaultValue: "Error", context: context): -2,
                      multiLanguageString(name: "processing", defaultValue: "Processing", context: context): 2
                    }.entries.map<DropDownValueModel>((result) {
                      return DropDownValueModel(
                        value: result.value,
                        name: result.key,
                      );
                    }).toList()
                  : {
                      multiLanguageString(name: "draft", defaultValue: "Draft", context: context): 0,
                      multiLanguageString(name: "review", defaultValue: "Review", context: context): 2,
                      multiLanguageString(name: "active", defaultValue: "Active", context: context): 1
                    }.entries.map<DropDownValueModel>((result) {
                      return DropDownValueModel(
                        value: result.value,
                        name: result.key,
                      );
                    }).toList(),
              maxHeight: 200,
              onChanged: (dynamic newValue) {
                if (isSearched && newValue == "") {
                  handleCallBackSearchFunction();
                }
                // if (newValue == "") {
                //   handleCallBackSearchFunction();
                // }
              },
            ),
          ),
        ),
        if (widget.type == "Test Case Runs" ||
            widget.type == "Test Scenario Runs" ||
            widget.type == "Test Manual Runs" ||
            widget.type == "Test Suite Runs" ||
            widget.type == "Delete Test Scenario Runs" ||
            widget.type == "Delete Test Case Runs" ||
            widget.type == "Delete Test Suite Runs" ||
            widget.type == "Delete Test Manual Runs")
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
              child: DynamicDropdownSearchClearOption(
                labelText: multiLanguageString(name: "search_test_run", defaultValue: "Search test run", context: context),
                controller: statisticsStatusController,
                hint: multiLanguageString(name: "search_test_run", defaultValue: "Search test run", context: context),
                items: {
                  multiLanguageString(name: "draft", defaultValue: "Draft", context: context): 0,
                  multiLanguageString(name: "complete", defaultValue: "Complete", context: context): 1,
                }.entries.map<DropDownValueModel>((result) {
                  return DropDownValueModel(
                    value: result.value,
                    name: result.key,
                  );
                }).toList(),
                maxHeight: 200,
                onChanged: (dynamic newValue) {
                  if (isSearched && newValue == "") {
                    handleCallBackSearchFunction();
                  }
                },
              ),
            ),
          ),
        if (widget.type == "Directory" ||
            widget.type == "Test case batch step" ||
            widget.type == "Test case" ||
            widget.type == "All" ||
            widget.type == "Test scenario" ||
            widget.type == "Test manual" ||
            widget.type == "Api" ||
            widget.type == "Test suite" ||
            widget.type == "Shared steps" ||
            widget.type == "Test Scenario Runs" ||
            widget.type == "Test Case Runs" ||
            widget.type == "Test Manual Runs" ||
            widget.type == "Test Suite Runs" ||
            widget.type == "Delete Test Scenario Runs" ||
            widget.type == "Delete Test Case Runs" ||
            widget.type == "Delete Test Suite Runs" ||
            widget.type == "Delete Test Manual Runs")
          Expanded(
            child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                child: DynamicDropdownSearchClearOption(
                  labelText: multiLanguageString(name: "project_module", defaultValue: "Project module", context: context),
                  controller: componentController,
                  hint: multiLanguageString(name: "up_search_module", defaultValue: "Search Module", context: context),
                  items: _listComponents.map<DropDownValueModel>((dynamic result) {
                    return DropDownValueModel(
                      value: result["id"],
                      name: result["title"],
                    );
                  }).toList(),
                  maxHeight: 200,
                  onChanged: (dynamic newValue) {
                    if (isSearched && newValue == "") {
                      handleCallBackSearchFunction();
                    }
                    // if (newValue == "") {
                    //   handleCallBackSearchFunction();
                    // }
                  },
                )),
          ),
        if (widget.type != "Delete Test Batch Runs")
          Expanded(
            child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                child: DynamicDropdownSearchClearOption(
                  labelText: multiLanguageString(name: "test_owner", defaultValue: "Test owner", context: context),
                  controller: testOwnerController,
                  hint: multiLanguageString(name: "search_test_owner", defaultValue: "Search Test owner", context: context),
                  items: _listTestOwner.map<DropDownValueModel>((dynamic result) {
                    return DropDownValueModel(
                      value: result["sysUser"]["id"],
                      name: result["sysUser"]["fullname"],
                    );
                  }).toList(),
                  maxHeight: 200,
                  onChanged: (dynamic newValue) {
                    if (isSearched && newValue == "") {
                      handleCallBackSearchFunction();
                    }
                    // if (newValue == "") {
                    //   handleCallBackSearchFunction();
                    // }
                  },
                )),
          ),
        if (widget.type == "Test Scenario Runs" ||
            widget.type == "Test Case Runs" ||
            widget.type == "Test Suite Runs" ||
            widget.type == "Test Manual Runs" ||
            widget.type == "Delete Test Scenario Runs" ||
            widget.type == "Delete Test Batch Runs" ||
            widget.type == "Delete Test Case Runs" ||
            widget.type == "Delete Test Manual Runs" ||
            widget.type == "Delete Test Suite Runs")
          Expanded(
            child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                child: DynamicDropdownSearchClearOption(
                  onChanged: (dynamic newValue) {
                    if (isSearched && newValue == "") {
                      handleCallBackSearchFunction();
                    }
                    // if (newValue == "") {
                    //   handleCallBackSearchFunction();
                    // }
                  },
                  labelText: multiLanguageString(name: "test_run_owner", defaultValue: "Test run owner", context: context),
                  controller: testRunOwnerController,
                  hint: multiLanguageString(name: "search_test_run_owner", defaultValue: "Search Test run owner", context: context),
                  items: _listTestOwner.map<DropDownValueModel>((dynamic result) {
                    return DropDownValueModel(
                      value: result["sysUser"]["id"],
                      name: result["sysUser"]["fullname"],
                    );
                  }).toList(),
                  maxHeight: 200,
                )),
          ),
        if (widget.type == "Directory" ||
            widget.type == "Test case batch step" ||
            widget.type == "Test case" ||
            widget.type == "All" ||
            widget.type == "Test scenario" ||
            widget.type == "Test manual" ||
            widget.type == "Test suite" ||
            widget.type == "Api" ||
            widget.type == "Shared steps")
          Expanded(
            child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                child: DynamicDropdownSearchClearOption(
                  labelText: multiLanguageString(name: "label", defaultValue: "Label", context: context),
                  controller: labelController,
                  hint: multiLanguageString(name: "search_label", defaultValue: "Search Label", context: context),
                  items: _listLabel.map<DropDownValueModel>((dynamic result) {
                    return DropDownValueModel(
                      value: result["id"],
                      name: result["title"],
                    );
                  }).toList(),
                  onChanged: (dynamic newValue) {
                    if (isSearched && newValue == "") {
                      handleCallBackSearchFunction();
                    }
                    // if (newValue == "") {
                    //   handleCallBackSearchFunction();
                    // }
                  },
                  maxHeight: 200,
                )),
          ),
        if (widget.type == "Directory" ||
            widget.type == "Test case batch step" ||
            widget.type == "Test case" ||
            widget.type == "All" ||
            widget.type == "Test scenario" ||
            widget.type == "Test manual" ||
            widget.type == "Test suite" ||
            widget.type == "Api" ||
            widget.type == "Shared steps")
          Expanded(
            child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                child: DynamicDropdownSearchClearOption(
                  labelText: multiLanguageString(name: "priority", defaultValue: "Priority", context: context),
                  controller: testPriorityController,
                  hint: multiLanguageString(name: "search_priority", defaultValue: "Search Priority", context: context),
                  items: [
                    {"name": multiLanguageString(name: "item_high", defaultValue: "High", context: context), "value": 0},
                    {"name": multiLanguageString(name: "item_medium", defaultValue: "Medium", context: context), "value": 1},
                    {"name": multiLanguageString(name: "item_low", defaultValue: "Low", context: context), "value": 2}
                  ].map<DropDownValueModel>((dynamic result) {
                    return DropDownValueModel(
                      value: result["value"],
                      name: result["name"],
                    );
                  }).toList(),
                  onChanged: (dynamic newValue) {
                    if (isSearched && newValue == "") {
                      handleCallBackSearchFunction();
                    }
                  },
                  maxHeight: 200,
                )),
          ),
        if (widget.type == "Directory" ||
            widget.type == "Test case batch step" ||
            widget.type == "Test case" ||
            widget.type == "All" ||
            widget.type == "Test scenario" ||
            widget.type == "Test manual" ||
            widget.type == "Test suite" ||
            widget.type == "Api" ||
            widget.type == "Shared steps")
          Expanded(
            child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                child: DynamicDropdownSearchClearOption(
                  labelText: multiLanguageString(name: "regression_candidate", defaultValue: "Regression candidate", context: context),
                  controller: regressionCandidateController,
                  hint: multiLanguageString(name: "search_regression_candidate", defaultValue: "Search Regression candidate", context: context),
                  items: [
                    {"name": multiLanguageString(name: "none", defaultValue: "None", context: context), "value": 0},
                    {"name": multiLanguageString(name: "primary", defaultValue: "Primary", context: context), "value": 3},
                    {"name": multiLanguageString(name: "secondary", defaultValue: "Secondary", context: context), "value": 2},
                    {"name": multiLanguageString(name: "tertiary", defaultValue: "Tertiary", context: context), "value": 1},
                  ].map<DropDownValueModel>((dynamic result) {
                    return DropDownValueModel(
                      value: result["value"],
                      name: result["name"],
                    );
                  }).toList(),
                  onChanged: (dynamic newValue) {
                    if (isSearched && newValue == "") {
                      handleCallBackSearchFunction();
                    }
                  },
                  maxHeight: 200,
                )),
          ),
        if (widget.type == "Test Scenario Runs" ||
            widget.type == "Test Case Runs" ||
            widget.type == "Test Suite Runs" ||
            widget.type == "Test Manual Runs" ||
            widget.type == "Delete Test Scenario Runs" ||
            widget.type == "Delete Test Batch Runs" ||
            widget.type == "Delete Test Case Runs" ||
            widget.type == "Delete Test Manual Runs" ||
            widget.type == "Delete Test Suite Runs")
          Expanded(
            child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                child: DynamicTextField(
                  controller: startDate,
                  onChanged: (value) {
                    setState(() {
                      startDate.text;
                    });
                  },
                  labelText: multiLanguageString(name: "create_date_from", defaultValue: "Create date from", context: context),
                  hintText: multiLanguageString(name: "create_date_from", defaultValue: "Create date from", context: context),
                  suffixIcon: (startDate.text != "")
                      ? IconButton(
                          onPressed: () {
                            setState(() {
                              startDate.clear();
                              if (isSearched) {
                                handleCallBackSearchFunction();
                              }
                            });
                          },
                          icon: const Icon(Icons.close),
                          splashRadius: 20,
                        )
                      : const Icon(Icons.calendar_today),
                  readOnly: true,
                  onTap: () {
                    _selectDate(startDate);
                  },
                )),
          ),
        if (widget.type == "Test Scenario Runs" ||
            widget.type == "Test Case Runs" ||
            widget.type == "Test Suite Runs" ||
            widget.type == "Test Manual Runs" ||
            widget.type == "Delete Test Scenario Runs" ||
            widget.type == "Delete Test Batch Runs" ||
            widget.type == "Delete Test Case Runs" ||
            widget.type == "Delete Test Manual Runs" ||
            widget.type == "Delete Test Suite Runs")
          Expanded(
            child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                child: DynamicTextField(
                  controller: endDate,
                  onChanged: (value) {
                    setState(() {
                      endDate.text;
                    });
                  },
                  labelText: multiLanguageString(name: "create_date_to", defaultValue: "Create date to", context: context),
                  hintText: multiLanguageString(name: "create_date_to", defaultValue: "Create date to", context: context),
                  suffixIcon: (endDate.text != "")
                      ? IconButton(
                          onPressed: () {
                            setState(() {
                              endDate.clear();
                              if (isSearched) {
                                handleCallBackSearchFunction();
                              }
                            });
                          },
                          icon: const Icon(Icons.close),
                          splashRadius: 20,
                        )
                      : const Icon(Icons.calendar_today),
                  readOnly: true,
                  onTap: () {
                    _selectDate(endDate);
                  },
                )),
          ),
      ];
      var itemPerRow = 4;
      if (constraints.maxWidth < 600) {
        itemPerRow = 1;
      } else if (constraints.maxWidth < 800) {
        itemPerRow = 2;
      } else if (constraints.maxWidth < 1200) {
        itemPerRow = 3;
      }
      var rowCount = (1.0 * widgetList.length / itemPerRow).ceil();
      var filterWidget = Column(
        children: [
          for (var rowI = 0; rowI < rowCount; rowI++)
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                for (var colI = 0; colI < itemPerRow; colI++)
                  rowI * itemPerRow + colI < widgetList.length ? widgetList[rowI * itemPerRow + colI] : Expanded(child: Container())
              ],
            )
        ],
      );
      return Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              (widget.type == "Test case batch step")
                  ? const SizedBox.shrink()
                  : (widget.countList != null)
                      ? MultiLanguageText(
                          name: "type_with_count",
                          defaultValue: "\$0 (\$1)",
                          variables: [widget.title, "${widget.countList}"],
                          style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500))
                      : MultiLanguageText(
                          name: "type_without_count",
                          defaultValue: "\$0",
                          variables: [widget.title],
                          style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500)),
              if (widget.isDirectory == true)
                SizedBox(
                  height: 40,
                  child: FloatingActionButton(
                    heroTag: null,
                    onPressed: () async {
                      widget.onChangedDirectory!();
                    },
                    tooltip: multiLanguageString(name: "directory", defaultValue: "Directory", context: context),
                    backgroundColor: widget.type == "Directory" ? const Color.fromARGB(176, 125, 141, 231) : const Color.fromRGBO(216, 218, 229, .7),
                    elevation: 0,
                    hoverElevation: 0,
                    child: const Icon(Icons.folder, color: Color.fromRGBO(49, 49, 49, 1)),
                  ),
                )
            ],
          ),
          Row(
            children: [
              MultiLanguageText(
                name: "filter_form",
                defaultValue: "Filter form",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: const Color.fromRGBO(0, 0, 0, 0.8),
                    fontSize: context.fontSizeManager.fontSizeText16,
                    letterSpacing: 2),
              ),
              IconButton(
                onPressed: () {
                  setState(() {
                    isShowFilterForm = !isShowFilterForm;
                  });
                },
                icon: isShowFilterForm ? const Icon(Icons.visibility) : const Icon(Icons.visibility_off),
                splashRadius: 1,
                tooltip: isShowFilterForm
                    ? multiLanguageString(name: "hide_filter_form", defaultValue: "Hide filter form", context: context)
                    : multiLanguageString(name: "show_filter_form", defaultValue: "Show filter form", context: context),
              )
            ],
          ),
          if (isShowFilterForm) filterWidget,
          if (isShowFilterForm)
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                ElevatedButton(
                    onPressed: () {
                      isSearched = true;
                      handleCallBackSearchFunction();
                    },
                    child: const MultiLanguageText(name: "search", defaultValue: "Search", isLowerCase: false)),
                if (widgetList.length > 2)
                  Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: ElevatedButton(
                        onPressed: () {
                          setState(() {
                            _searchLibrary.clear();
                            _nameController.clear();
                            componentController.clearDropDown();
                            testOwnerController.clearDropDown();
                            testCaseTypeController.clearDropDown();
                            statusController.clearDropDown();
                            statisticsStatusController.clearDropDown();
                            testRunOwnerController.clearDropDown();
                            versionController.clearDropDown();
                            environmentController.clearDropDown();
                            labelController.clearDropDown();
                            regressionCandidateController.clearDropDown();
                            testPriorityController.clearDropDown();
                            startDate.clear();
                            endDate.clear();
                            widget.callbackFilter!({});
                          });
                        },
                        child: const MultiLanguageText(name: "reset", defaultValue: "Reset", isLowerCase: false)),
                  ),
                if (widget.type == "Delete Test Scenario Runs" ||
                    widget.type == "Delete Test Batch Runs" ||
                    widget.type == "Delete Test Case Runs" ||
                    widget.type == "Delete Test Manual Runs" ||
                    widget.type == "Delete Test Suite Runs")
                  Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(Colors.red),
                        ),
                        onPressed: () {
                          handleCallBackSearchFunction(type: "delete");
                        },
                        child: const MultiLanguageText(name: "delete", defaultValue: "Delete", isLowerCase: false)),
                  ),
                if (widget.callbackExport != null &&
                    (widget.type == "Test case batch step" ||
                        widget.type == "Directory" ||
                        widget.type == "Test case" ||
                        widget.type == "All" ||
                        widget.type == "Test scenario" ||
                        widget.type == "Api" ||
                        widget.type == "Test manual" ||
                        widget.type == "Test suite" ||
                        widget.type == "Shared steps"))
                  Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(const Color.fromARGB(175, 8, 196, 80)),
                        ),
                        onPressed: () {
                          handleCallBackSearchFunction(type: "export");
                        },
                        child: const MultiLanguageText(name: "export", defaultValue: "Export", isLowerCase: false)),
                  ),
                if (widget.callbackImport != null &&
                    (widget.type == "Test case batch step" ||
                        widget.type == "Directory" ||
                        widget.type == "Test case" ||
                        widget.type == "All" ||
                        widget.type == "Test scenario" ||
                        widget.type == "Api" ||
                        widget.type == "Test manual" ||
                        widget.type == "Test suite" ||
                        widget.type == "Shared steps"))
                  Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(const Color.fromARGB(175, 8, 196, 80)),
                        ),
                        onPressed: () {
                          handleCallBackSearchFunction(type: "import");
                        },
                        child: const MultiLanguageText(name: "import", defaultValue: "Import", isLowerCase: false)),
                  ),
              ],
            )
        ],
      );
    });
  }

  _selectDate(TextEditingController controller) async {
    final DateTime? datePicked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (datePicked != null && mounted) {
      final TimeOfDay? timePicked = await showTimePicker(
        context: context,
        initialTime: TimeOfDay.now(),
      );
      if (timePicked != null) {
        setState(() {
          controller.text =
              "${DateFormat('dd-MM-yyyy').format(datePicked)} ${timePicked.hour.toString().padLeft(2, '0')}:${timePicked.minute.toString().padLeft(2, '0')}";
        });
      }
    }
  }
}
