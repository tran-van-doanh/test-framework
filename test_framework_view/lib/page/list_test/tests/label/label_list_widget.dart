import 'package:flutter/material.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/components/style.dart';

class LabelListWidget extends StatefulWidget {
  final String labelId;
  final VoidCallback? onRemove;
  const LabelListWidget({Key? key, required this.labelId, this.onRemove}) : super(key: key);

  @override
  State<LabelListWidget> createState() => _LabelListWidgetState();
}

class _LabelListWidgetState extends State<LabelListWidget> {
  dynamic label;
  bool isLoad = false;
  @override
  void initState() {
    super.initState();
    getLabel(widget.labelId);
  }

  getLabel(id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-label/$id", context);
    if (mounted && response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        label = response["body"]["result"];
        isLoad = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return isLoad
        ? label != null
            ? Container(
                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                decoration: BoxDecoration(
                  color: Colors.grey.withOpacity(0.5),
                  // color: Colors.grey.withOpacity(0.3),
                  border: const Border(
                    left: BorderSide(color: Colors.grey, width: 4),
                    // left: BorderSide(color: Colors.grey, width: 4),
                  ),
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Text(
                        label["title"],
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                    Row(
                      children: [
                        if (widget.onRemove != null)
                          GestureDetector(
                            onTap: () => widget.onRemove!(),
                            child: const Icon(Icons.close),
                          ),
                      ],
                    )
                  ],
                ),
              )
            : const SizedBox.shrink()
        : const CircularProgressIndicator();
  }
}
