import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/components/dialog/dialog_delete.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/list_test/tests/directory/dialog_option_directory.dart';
import 'package:test_framework_view/type.dart';

class DirectoryItemWidget extends StatefulWidget {
  final String testCaseType;
  final dynamic directory;
  final String prjProjectId;
  final String? parentDirectoryId;
  final Function(String id)? onDeleteDirectory;
  final Function(String id) onClickDirectory;
  final Function(String id)? onPaste;
  const DirectoryItemWidget(
      {Key? key,
      required this.testCaseType,
      this.directory,
      required this.prjProjectId,
      this.parentDirectoryId,
      this.onDeleteDirectory,
      required this.onClickDirectory,
      this.onPaste})
      : super(key: key);

  @override
  State<DirectoryItemWidget> createState() => _DirectoryItemWidgetState();
}

class _DirectoryItemWidgetState extends State<DirectoryItemWidget> {
  List resultListDirectory = [];
  bool isExpand = false;
  var directoryItem = {};

  @override
  initState() {
    super.initState();
    getInitPage();
  }

  getInitPage() {
    setState(() {
      directoryItem = widget.directory;
    });
    getListTfDirectory();
  }

  handleEditTfDirectory(result, id) async {
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-directory/$id", result, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      if (response["body"].containsKey("errorMessage") && mounted) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        await getDirectory(id);
        return true;
      }
    }
    return false;
  }

  getDirectory(id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-directory/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        directoryItem = response["body"]["result"];
      });
    }
  }

  getListTfDirectory() async {
    var findRequestDirectory = {"directoryType": widget.testCaseType, "prjProjectId": widget.prjProjectId, "parentId": directoryItem["id"]};
    var responseDirectory = await httpPost("/test-framework-api/user/test-framework/tf-test-directory/search", findRequestDirectory, context);
    if (responseDirectory.containsKey("body") && responseDirectory["body"] is String == false && mounted) {
      for (var responseDirectory in responseDirectory["body"]["resultList"]) {
        responseDirectory["type"] = "directory";
      }
      setState(() {
        resultListDirectory = responseDirectory["body"]["resultList"];
      });
    }
    return 0;
  }

  handleDeleteDirectory(id) async {
    var response = await httpDelete("/test-framework-api/user/test-framework/tf-test-directory/$id", context);

    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        getListTfDirectory();
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
      }
    }
  }

  handleAddTfDirectory(result, String parentId) async {
    result["parentId"] = parentId;
    var response = await httpPut("/test-framework-api/user/test-framework/tf-test-directory", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
        return false;
      } else {
        Navigator.of(context).pop();
        getListTfDirectory();
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        return true;
      }
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    if (directoryItem.isNotEmpty) {
      return Consumer3<NavigationModel, TestCaseDirectoryModel, LanguageModel>(
        builder: (context, navigationModel, testCaseDirectoryModel, languageModel, child) {
          return Column(
            children: [
              GestureDetector(
                onTap: () => widget.onClickDirectory(directoryItem["id"]),
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minHeight: 45),
                  child: Row(
                    children: [
                      Expanded(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: 30,
                              child: (resultListDirectory.isNotEmpty)
                                  ? GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          isExpand = !isExpand;
                                        });
                                        if (isExpand) {
                                          getListTfDirectory();
                                        }
                                      },
                                      child: Icon(isExpand ? Icons.expand_more : Icons.chevron_right),
                                    )
                                  : const SizedBox.shrink(),
                            ),
                            const Icon(
                              Icons.folder,
                              color: Color.fromRGBO(61, 148, 254, 1),
                              size: 20,
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            Expanded(
                              child: Text(
                                "${directoryItem["title"] ?? ""} ",
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    fontSize: context.fontSizeManager.fontSizeText16,
                                    color: widget.parentDirectoryId == directoryItem["id"] ? Colors.blue : const Color.fromRGBO(49, 49, 49, 1),
                                    decoration: TextDecoration.none,
                                    fontWeight: widget.parentDirectoryId == directoryItem["id"] ? FontWeight.bold : null),
                              ),
                            ),
                          ],
                        ),
                      ),
                      if (widget.onPaste != null && widget.onDeleteDirectory != null)
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).push(
                              createRoute(
                                OptionDirectoryWidget(
                                  directoryType: widget.testCaseType,
                                  type: "create",
                                  title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                                  callbackOptionTest: (result) async {
                                    bool isSuccess = await handleAddTfDirectory(result, directoryItem["id"]);
                                    if (isSuccess && !isExpand) {
                                      setState(() {
                                        isExpand = true;
                                      });
                                    }
                                  },
                                  prjProjectId: widget.prjProjectId,
                                  parentId: directoryItem["id"],
                                ),
                              ),
                            );
                          },
                          child: const Icon(Icons.add),
                        ),
                      if (widget.onPaste != null && widget.onDeleteDirectory != null)
                        PopupMenuButton(
                          offset: const Offset(2, 35),
                          tooltip: '',
                          splashRadius: 10,
                          icon: const Icon(
                            Icons.more_vert,
                          ),
                          itemBuilder: (context) => [
                            PopupMenuItem(
                              padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                              child: ListTile(
                                onTap: () {
                                  Navigator.pop(context);
                                  Navigator.of(context).push(
                                    createRoute(
                                      OptionDirectoryWidget(
                                        directoryType: widget.testCaseType,
                                        type: "edit",
                                        title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                        id: directoryItem["id"],
                                        callbackOptionTest: (result) async {
                                          bool resultFunction = await handleEditTfDirectory(result, directoryItem["id"]);
                                          if (resultFunction && mounted) {
                                            Navigator.of(this.context).pop();
                                            showToast(
                                                context: this.context,
                                                msg: multiLanguageString(
                                                    name: "update_successful", defaultValue: "Update successful", context: this.context),
                                                color: Colors.greenAccent,
                                                icon: const Icon(Icons.done));
                                          }
                                        },
                                        prjProjectId: widget.prjProjectId,
                                      ),
                                    ),
                                  );
                                },
                                contentPadding: const EdgeInsets.all(0),
                                hoverColor: Colors.transparent,
                                title: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                                leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
                              ),
                            ),
                            PopupMenuItem(
                              padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                              child: ListTile(
                                onTap: () {
                                  Navigator.pop(context);
                                  dialogDeleteDirectory(directoryItem);
                                },
                                contentPadding: const EdgeInsets.all(0),
                                hoverColor: Colors.transparent,
                                title: const MultiLanguageText(name: "delete", defaultValue: "Delete"),
                                leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                              ),
                            ),
                            PopupMenuItem(
                              padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                              child: ListTile(
                                onTap: () {
                                  testCaseDirectoryModel.setTestCaseDirectory(directoryItem);
                                  showToast(
                                      context: context,
                                      msg: multiLanguageString(
                                          name: "copy_result_directory_successful",
                                          defaultValue: "Copy '\$0' successful, choose a directory to move",
                                          variables: ["${directoryItem["title"]}"],
                                          context: context),
                                      color: Colors.greenAccent,
                                      icon: const Icon(Icons.done));
                                  Navigator.pop(context);
                                },
                                contentPadding: const EdgeInsets.all(0),
                                hoverColor: Colors.transparent,
                                title: const MultiLanguageText(name: "move_to_directory", defaultValue: "Move to directory"),
                                leading: const Icon(Icons.drive_file_move, color: Color.fromARGB(255, 112, 114, 119)),
                              ),
                            ),
                            if (testCaseDirectoryModel.testCaseDirectory != null &&
                                testCaseDirectoryModel.testCaseDirectory["id"] != directoryItem["id"])
                              PopupMenuItem(
                                padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                                child: ListTile(
                                  onTap: () {
                                    Navigator.pop(context);
                                    widget.onPaste!(directoryItem["id"]);
                                  },
                                  contentPadding: const EdgeInsets.all(0),
                                  hoverColor: Colors.transparent,
                                  title: const MultiLanguageText(name: "paste_to_directory", defaultValue: "Paste to directory"),
                                  leading: const Icon(Icons.file_open, color: Color.fromARGB(255, 112, 114, 119)),
                                ),
                              ),
                          ],
                        ),
                    ],
                  ),
                ),
              ),
              if (isExpand && resultListDirectory.isNotEmpty)
                const SizedBox(
                  height: 10,
                ),
              if (isExpand && resultListDirectory.isNotEmpty)
                Padding(
                  padding: const EdgeInsets.only(left: 25),
                  child: Column(
                    children: [
                      for (var directory in resultListDirectory)
                        GestureDetector(
                          onTap: () {
                            widget.onClickDirectory(directory["id"]);
                          },
                          child: Container(
                            margin: EdgeInsets.only(bottom: resultListDirectory.last != directory ? 10 : 0),
                            child: DirectoryItemWidget(
                              onPaste: (widget.onPaste != null) ? (id) => widget.onPaste!(id) : null,
                              key: Key(directory["id"]),
                              testCaseType: widget.testCaseType,
                              prjProjectId: widget.prjProjectId,
                              directory: directory,
                              parentDirectoryId: widget.parentDirectoryId,
                              onDeleteDirectory: (id) => handleDeleteDirectory(id),
                              onClickDirectory: (id) => widget.onClickDirectory(id),
                            ),
                          ),
                        )
                    ],
                  ),
                )
            ],
          );
        },
      );
    }
    return const SizedBox.shrink();
  }

  dialogDeleteDirectory(selectedItem) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogDelete(
          selectedItem: selectedItem,
          callback: () => widget.onDeleteDirectory!(selectedItem["id"]),
          type: 'test',
        );
      },
    );
  }
}
