import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';

import '../../../../common/custom_state.dart';

class DialogDirectory extends StatefulWidget {
  final Function callbackSelectedDirectory;
  final bool hasNeedSave;
  final String prjProjectId;
  final dynamic directoryPath;
  final String type;
  const DialogDirectory(
      {Key? key,
      required this.callbackSelectedDirectory,
      this.hasNeedSave = false,
      required this.prjProjectId,
      this.directoryPath,
      required this.type})
      : super(key: key);
  @override
  State<DialogDirectory> createState() => _DialogDirectoryState();
}

class _DialogDirectoryState extends CustomState<DialogDirectory> {
  late Future futureListDirectory;
  var listDirectory = [];
  var rowCountListDirectory = 0;
  var currentPageListDirectory = 1;
  var rowPerPageDirectory = 5;
  dynamic selectedDirectory;
  var findRequestDirectory = {};
  String? parentId;
  String? parentName;
  List? directoryPathList;

  @override
  void initState() {
    futureListDirectory = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    findRequestDirectory = {};
    if (widget.directoryPath != null) {
      parentName = widget.directoryPath["title"];
      parentId = widget.directoryPath["parentId"];
      if (widget.directoryPath["parentId"] == null) {
        findRequestDirectory["parentIdIsNull"] = true;
      } else {
        findRequestDirectory["parentId"] = widget.directoryPath["parentId"];
      }
      selectedDirectory = widget.directoryPath["id"];
      await getDirectoryPath();
    } else {
      selectedDirectory = "root";
      findRequestDirectory["parentIdIsNull"] = true;
    }
    await getListTfDirectory(currentPageListDirectory, findRequestDirectory);
    return 0;
  }

  getListTfDirectory(page, findRequest) async {
    if ((page - 1) * rowPerPageDirectory > rowCountListDirectory) {
      page = (1.0 * rowCountListDirectory / rowPerPageDirectory).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequestDirectory = {};
    findRequestDirectory.addAll(findRequest);
    findRequestDirectory["queryOffset"] = (page - 1) * rowPerPageDirectory;
    findRequestDirectory["queryLimit"] = rowPerPageDirectory;
    findRequestDirectory["directoryType"] = widget.type;
    findRequestDirectory["prjProjectId"] = widget.prjProjectId;
    var responseDirectory = await httpPost("/test-framework-api/user/test-framework/tf-test-directory/search", findRequestDirectory, context);
    if (responseDirectory.containsKey("body") && responseDirectory["body"] is String == false) {
      setState(() {
        currentPageListDirectory = page;
        rowCountListDirectory = responseDirectory["body"]["rowCount"];
        listDirectory = responseDirectory["body"]["resultList"];
      });
    }
    return 0;
  }

  getDirectoryPath() async {
    if (parentId != null) {
      var directoryParentResponse = await httpGet("/test-framework-api/user/test-framework/tf-test-directory/$parentId/path", context);
      if (directoryParentResponse.containsKey("body") && directoryParentResponse["body"] is String == false) {
        setState(() {
          directoryPathList = directoryParentResponse["body"]["resultList"];
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: futureListDirectory,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return AlertDialog(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  MultiLanguageText(
                    name: "directory",
                    defaultValue: "Directory",
                    style: Style(context).styleTitleDialog,
                  ),
                  TextButton(
                    onPressed: () => Navigator.pop(context, 'Cancel'),
                    child: const Icon(
                      Icons.close,
                      color: Colors.black,
                    ),
                  )
                ],
              ),
              content: Container(
                decoration: const BoxDecoration(
                  border: Border(
                    top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
                    bottom: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
                  ),
                ),
                width: 1000,
                child: IntrinsicHeight(
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          children: [
                            if (widget.hasNeedSave)
                              Container(
                                margin: const EdgeInsets.only(top: 10),
                                color: Colors.yellow[200],
                                child: MultiLanguageText(
                                  name: "save_test_before",
                                  defaultValue: "Please save test before",
                                  style: Style(context).styleTitleDialog,
                                ),
                              ),
                            Container(
                              margin: const EdgeInsets.only(top: 10),
                              color: Colors.yellow[200],
                              child: MultiLanguageText(
                                name: "select_a_directory",
                                defaultValue: "Select a directory",
                                style: Style(context).styleTitleDialog,
                              ),
                            ),
                            if (directoryPathList != null && directoryPathList!.isNotEmpty) breadCrumbWidget(),
                            headerTableDirectory(),
                            tableDirectoryWidget(),
                            DynamicTablePagging(
                              rowCountListDirectory,
                              currentPageListDirectory,
                              rowPerPageDirectory,
                              pageChangeHandler: (page) {
                                getListTfDirectory(page, findRequestDirectory);
                              },
                              rowPerPageChangeHandler: (value) {
                                setState(() {
                                  rowPerPageDirectory = value;
                                  getListTfDirectory(1, findRequestDirectory);
                                });
                              },
                            ),
                            btnWidget(context),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        });
  }

  Padding btnWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15),
      child: ConstrainedBox(
        constraints: const BoxConstraints(minHeight: 40, minWidth: 120),
        child: ElevatedButton(
          onPressed: (selectedDirectory != null)
              ? () {
                  Navigator.pop(context);
                  widget.callbackSelectedDirectory(selectedDirectory);
                }
              : null,
          child: const MultiLanguageText(
            name: "ok",
            defaultValue: "OK",
            isLowerCase: false,
            style: TextStyle(fontWeight: FontWeight.bold, letterSpacing: 2),
          ),
        ),
      ),
    );
  }

  Column tableDirectoryWidget() {
    return Column(
      children: [
        if (findRequestDirectory["parentIdIsNull"] ?? false)
          Container(
            padding: const EdgeInsets.all(15),
            decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
                bottom: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
                left: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
                right: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
              ),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  flex: 3,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Row(
                      children: [
                        Radio(
                          value: "root",
                          groupValue: selectedDirectory,
                          onChanged: (dynamic value) {
                            setState(() {
                              selectedDirectory = value;
                            });
                          },
                        ),
                        MultiLanguageText(
                          name: "root_repository",
                          defaultValue: "Root respository",
                          maxLines: 3,
                          softWrap: true,
                          style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Center(
                      child: Text(
                        "",
                        maxLines: 3,
                        softWrap: true,
                        style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Center(
                      child: Text(
                        "",
                        maxLines: 3,
                        softWrap: true,
                        style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Center(
                      child: Text(
                        "",
                        maxLines: 3,
                        softWrap: true,
                        style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        for (var elementrepository in listDirectory)
          GestureDetector(
            onTap: () {
              setState(() {
                parentName = elementrepository["title"];
              });
              findRequestDirectory = {};
              parentId = elementrepository["id"];
              findRequestDirectory["parentId"] = elementrepository["id"];
              getListTfDirectory(currentPageListDirectory, findRequestDirectory);
              getDirectoryPath();
            },
            child: Container(
              padding: const EdgeInsets.all(15),
              decoration: const BoxDecoration(
                border: Border(
                  top: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
                  bottom: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
                  left: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
                  right: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
                ),
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    flex: 3,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 5),
                      child: Row(
                        children: [
                          Radio(
                            value: elementrepository["id"],
                            groupValue: selectedDirectory,
                            onChanged: (dynamic value) {
                              setState(() {
                                selectedDirectory = value;
                              });
                            },
                          ),
                          Text(
                            elementrepository["title"] ?? "",
                            maxLines: 3,
                            softWrap: true,
                            style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 5),
                      child: Center(
                        child: Text(
                          elementrepository["description"] ?? "",
                          maxLines: 3,
                          softWrap: true,
                          style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 5),
                      child: Center(
                        child: Text(
                          elementrepository["sysUser"]["fullname"],
                          maxLines: 3,
                          softWrap: true,
                          style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 5),
                      child: Center(
                        child: Text(
                          DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(elementrepository["createDate"]).toLocal()),
                          maxLines: 3,
                          softWrap: true,
                          style: TextStyle(fontSize: context.fontSizeManager.fontSizeText16, color: const Color.fromRGBO(49, 49, 49, 1)),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
      ],
    );
  }

  Container headerTableDirectory() {
    return Container(
      padding: const EdgeInsets.all(15),
      decoration: const BoxDecoration(
        border: Border(
          top: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
          bottom: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 0.75),
          left: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
          right: BorderSide(color: Color.fromARGB(255, 207, 205, 205), width: 1.5),
        ),
      ),
      child: Row(
        children: [
          Expanded(
              flex: 3,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: MultiLanguageText(name: "title", defaultValue: "Title", isLowerCase: false, style: Style(context).styleTextDataColumn),
              )),
          Expanded(
              child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5),
            child: Center(
                child: MultiLanguageText(
                    name: "description", defaultValue: "Description", isLowerCase: false, style: Style(context).styleTextDataColumn)),
          )),
          Expanded(
              child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5),
            child:
                Center(child: MultiLanguageText(name: "owner", defaultValue: "Owner", isLowerCase: false, style: Style(context).styleTextDataColumn)),
          )),
          Expanded(
              child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5),
            child: Center(
                child: MultiLanguageText(
                    name: "create_date", defaultValue: "Create date", isLowerCase: false, style: Style(context).styleTextDataColumn)),
          )),
        ],
      ),
    );
  }

  Row breadCrumbWidget() {
    return Row(
      children: [
        IconButton(
          onPressed: () async {
            findRequestDirectory = {};
            parentId = null;
            findRequestDirectory["parentIdIsNull"] = true;
            directoryPathList = null;
            getListTfDirectory(currentPageListDirectory, findRequestDirectory);
          },
          icon: const FaIcon(FontAwesomeIcons.house, size: 16),
          color: Colors.grey,
          splashRadius: 10,
          tooltip: multiLanguageString(name: "root", defaultValue: "Root", context: context),
        ),
        for (var directoryPath in directoryPathList!)
          RichText(
              text: TextSpan(children: [
            const TextSpan(
              text: " / ",
              style: TextStyle(color: Colors.grey),
            ),
            TextSpan(
                text: "${directoryPath["title"]}",
                style: TextStyle(
                    color: directoryPathList!.last == directoryPath ? Colors.blue : Colors.grey,
                    fontSize: context.fontSizeManager.fontSizeText16,
                    fontWeight: FontWeight.bold),
                recognizer: TapGestureRecognizer()
                  ..onTap = () {
                    findRequestDirectory = {};
                    parentId = directoryPath["id"];
                    findRequestDirectory["parentId"] = directoryPath["id"];
                    getListTfDirectory(currentPageListDirectory, findRequestDirectory);
                    getDirectoryPath();
                  }),
          ]))
      ],
    );
  }
}
