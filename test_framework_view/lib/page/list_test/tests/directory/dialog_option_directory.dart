import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';
import '../../../../api.dart';
import '../../../../common/custom_state.dart';

class OptionDirectoryWidget extends StatefulWidget {
  final String type;
  final String title;
  final String? id;
  final Function? callbackOptionTest;
  final String prjProjectId;
  final String? parentId;
  final String directoryType;
  const OptionDirectoryWidget(
      {Key? key,
      required this.type,
      this.id,
      this.callbackOptionTest,
      required this.prjProjectId,
      this.parentId,
      required this.directoryType,
      required this.title})
      : super(key: key);

  @override
  State<OptionDirectoryWidget> createState() => _OptionDirectoryWidgetState();
}

class _OptionDirectoryWidgetState extends CustomState<OptionDirectoryWidget> {
  late Future future;
  final TextEditingController _desController = TextEditingController();
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  dynamic selectedItem;
  @override
  void initState() {
    future = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    if (widget.id != null) {
      await getSelectedItem(widget.id!);
      await setInitValue();
    }
    return 0;
  }

  getSelectedItem(String id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-directory/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        selectedItem = response["body"]["result"];
      });
    }
  }

  setInitValue() {
    setState(() {
      if (selectedItem != null) {
        _desController.text = selectedItem["description"] ?? "";
        _titleController.text = selectedItem["title"] ?? "";
        _nameController.text = selectedItem["name"] ?? "";
      }
    });
  }

  @override
  void dispose() {
    _desController.dispose();
    _titleController.dispose();
    _nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return FutureBuilder(
        future: future,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return TestFrameworkRootPageWidget(
              child: Column(
                children: [
                  headerWidget(context),
                  const Divider(color: Color.fromRGBO(216, 218, 229, 1)),
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.only(top: 20, bottom: 10),
                      child: Form(
                        key: _formKey,
                        child: SingleChildScrollView(
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                            child: FocusTraversalGroup(
                              policy: OrderedTraversalPolicy(),
                              child: bodyWidget(),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  btnWidget()
                ],
              ),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        },
      );
    });
  }

  Padding btnWidget() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20, top: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ButtonSave(onPressed: () {
            if (_formKey.currentState!.validate()) {
              var result = {};
              if (selectedItem != null) {
                result = selectedItem;
                result["title"] = _titleController.text;
                result["name"] = _nameController.text;
                result["description"] = _desController.text;
              } else {
                result = {
                  "title": _titleController.text,
                  "name": _nameController.text,
                  "description": _desController.text,
                  "status": 1,
                  "prjProjectId": widget.prjProjectId,
                  "directoryType": widget.directoryType,
                  if (widget.parentId != null) "parentId": widget.parentId
                };
              }
              widget.callbackOptionTest!(result);
            }
          }),
          const ButtonCancel()
        ],
      ),
    );
  }

  Column bodyWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "code", defaultValue: "Code", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _nameController,
                  hintText: "...",
                  onChanged: (text) {
                    _formKey.currentState!.validate();
                  },
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "name", defaultValue: "Name", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _titleController,
                  hintText: "...",
                  onChanged: (text) {
                    _formKey.currentState!.validate();
                  },
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "description", defaultValue: "Description", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _desController,
                  hintText: "...",
                  maxline: 3,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Padding headerWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 35, 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "type_directory",
            defaultValue: "\$0 DIRECTORY",
            variables: [widget.title],
            isLowerCase: false,
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
            onPressed: () => Navigator.pop(context, 'cancel'),
            child: const Icon(
              Icons.close,
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }
}
