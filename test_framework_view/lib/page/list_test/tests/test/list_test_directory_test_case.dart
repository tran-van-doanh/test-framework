import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/common/file_download.dart';
import 'package:test_framework_view/components/badge/badge_icon.dart';
import 'package:test_framework_view/components/badge/get_last_run.dart';
import 'package:test_framework_view/components/dialog/dialog_delete.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/editor/editor/editor_page.dart';
import 'package:test_framework_view/page/list_test/tests/directory/dialog_option_directory.dart';
import 'package:test_framework_view/page/list_test/tests/header/header_list_test_widget.dart';
import 'package:test_framework_view/page/list_test/tests/test/dialog_option_test.dart';
import 'package:test_framework_view/page/list_test/tests/directory/directory_item_widget.dart';
import 'package:test_framework_view/type.dart';

import '../../../../api.dart';
import '../../../../common/custom_state.dart';
import '../../../../config.dart';

class ListTestDirectoryTestCase extends StatefulWidget {
  final String type;
  const ListTestDirectoryTestCase({Key? key, required this.type}) : super(key: key);

  @override
  State<ListTestDirectoryTestCase> createState() => _ListTestDirectoryTestCaseState();
}

class _ListTestDirectoryTestCaseState extends CustomState<ListTestDirectoryTestCase> {
  late Future futureListTfDirectoryTestCase;
  List resultListDirectory = [];
  var resultListTestCase = [];
  var rowCountListTestCase = 0;
  var currentPageTestCase = 1;
  var rowPerPageTestCase = 10;
  var findRequestTestCase = {};
  var findRequestDirectory = {};
  var prjProjectId = "";
  bool isViewTypeDirectory = true;
  bool isExpandDirectory = true;
  PlatformFile? fileTestCase;
  PlatformFile? fileContent;
  List? directoryPathList;
  String? parentDirectoryId;
  @override
  void initState() {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    prjProjectId = navigationModel.prjProjectId ?? "";
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Provider.of<TestCaseConfigModel>(context, listen: false).clearProvider();
    });
    futureListTfDirectoryTestCase = getInitPage();
    super.initState();
  }

  getInitPage() async {
    findRequestTestCase["tfTestDirectoryIdIsNull"] = true;
    findRequestDirectory["parentIdIsNull"] = true;
    await getListTfTestCase(currentPageTestCase, findRequestTestCase);
    await getListTfDirectory(findRequestDirectory);
    return 0;
  }

  getDirectoryPathList() async {
    var directoryPathResponse = await httpGet("/test-framework-api/user/test-framework/tf-test-directory/$parentDirectoryId/path", context);
    if (directoryPathResponse.containsKey("body") && directoryPathResponse["body"] is String == false) {
      directoryPathList = directoryPathResponse["body"]["resultList"];
    }
  }

  getListTfTestCase(page, findRequest) async {
    if ((page - 1) * rowPerPageTestCase > rowCountListTestCase) {
      page = (1.0 * rowCountListTestCase / rowPerPageTestCase).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequestTestCase = findRequest;
    if (findRequest["titleLike"] != null && findRequest["titleLike"].isNotEmpty) {
      findRequestTestCase["titleLike"] = "%${findRequest["titleLike"]}%";
    }
    if (findRequest["nameLike"] != null && findRequest["nameLike"].isNotEmpty) {
      findRequestTestCase["nameLike"] = "%${findRequest["nameLike"]}%";
    }
    findRequestTestCase["queryOffset"] = (page - 1) * rowPerPageTestCase;
    findRequestTestCase["queryLimit"] = rowPerPageTestCase;
    findRequestTestCase["testCaseType"] = widget.type;
    findRequestTestCase["prjProjectId"] = prjProjectId;
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-case/search", findRequestTestCase, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPageTestCase = page;
        rowCountListTestCase = response["body"]["rowCount"];
        resultListTestCase = response["body"]["resultList"];
        for (var element in resultListTestCase) {
          element["type"] = "test_scenario";
        }
      });
    }
    return 0;
  }

  getListTfDirectory(findRequest) async {
    var findRequestDirectory = findRequest;
    if (findRequest["titleLike"] != null && findRequest["titleLike"].isNotEmpty) {
      findRequestDirectory["titleLike"] = "%${findRequest["titleLike"]}%";
    }
    findRequestDirectory["directoryType"] = widget.type;
    findRequestDirectory["prjProjectId"] = prjProjectId;
    var responseDirectory = await httpPost("/test-framework-api/user/test-framework/tf-test-directory/search", findRequestDirectory, context);
    if (responseDirectory.containsKey("body") && responseDirectory["body"] is String == false && mounted) {
      for (var responseDirectory in responseDirectory["body"]["resultList"]) {
        responseDirectory["type"] = "directory";
      }
      setState(() {
        resultListDirectory = responseDirectory["body"]["resultList"];
      });
    }
    return 0;
  }

  handleDeleteDirectory(id) async {
    var response = await httpDelete("/test-framework-api/user/test-framework/tf-test-directory/$id", context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        futureListTfDirectoryTestCase = getListTfDirectory(findRequestDirectory);
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
      }
    }
  }

  handleDeleteTestCase(id) async {
    var response = await httpDelete("/test-framework-api/user/test-framework/tf-test-case/$id", context);

    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        futureListTfDirectoryTestCase = getListTfTestCase(currentPageTestCase, findRequestTestCase);
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
      }
    }
  }

  handleAddTfDirectory(result, {String? parentId}) async {
    if (parentId != null) {
      result["parentId"] = parentId;
    }
    var response = await httpPut("/test-framework-api/user/test-framework/tf-test-directory", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        Navigator.of(context).pop();
        futureListTfDirectoryTestCase = getListTfDirectory(findRequestDirectory);
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
      }
    }
  }

  handleAddTfTestCase(result) async {
    var response = await httpPut("/test-framework-api/user/test-framework/tf-test-case", result, context);

    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        Navigator.of(context).pop();
        futureListTfDirectoryTestCase = getListTfTestCase(currentPageTestCase, findRequestTestCase);
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
      }
    }
  }

  handleEditTfDirectory(result, id) async {
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-directory/$id", result, context);

    if (response.containsKey("body") && response["body"] is String == false) {
      if (response["body"].containsKey("errorMessage") && mounted) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        futureListTfDirectoryTestCase = getListTfDirectory(findRequestDirectory);
        return true;
      }
    }
    return false;
  }

  handleEditTfTestCase(result, id) async {
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-case/$id", result, context);

    if (response.containsKey("body") && response["body"] is String == false) {
      if (response["body"].containsKey("errorMessage") && mounted) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        futureListTfDirectoryTestCase = getListTfTestCase(currentPageTestCase, findRequestTestCase);
        return true;
      }
    }
    return false;
  }

  handleDuplicateTfTestCase(result) async {
    var response = await httpPut("/test-framework-api/user/test-framework/tf-test-case", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        Navigator.of(context).pop();
        futureListTfDirectoryTestCase = getListTfTestCase(currentPageTestCase, findRequestTestCase);
        showToast(
            msg: multiLanguageString(name: "duplicate_successful", defaultValue: "Duplicate successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.error),
            context: context);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer3<NavigationModel, TestCaseDirectoryModel, LanguageModel>(
      builder: (context, navigationModel, testCaseDirectoryModel, languageModel, child) {
        return FutureBuilder(
          future: futureListTfDirectoryTestCase,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView(
                children: [
                  NavMenuTabV1(
                    currentUrl: "/test-list/${widget.type}",
                    margin: const EdgeInsets.fromLTRB(50, 20, 50, 0),
                    navigationList: context.testMenuList,
                  ),
                  headerWidget(context),
                  Container(
                    margin: const EdgeInsets.fromLTRB(50, 0, 50, 20),
                    child: SelectionArea(
                      child: Container(
                        alignment: Alignment.topLeft,
                        child: LayoutBuilder(
                          builder: (BuildContext context, BoxConstraints constraints) {
                            return Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                (isViewTypeDirectory && isExpandDirectory)
                                    ? Padding(
                                        padding: const EdgeInsets.only(right: 10),
                                        child: tableDirectoryWidget(context, testCaseDirectoryModel, navigationModel))
                                    : const SizedBox.shrink(),
                                Expanded(
                                  child: tableTestCaseWidget(context, navigationModel, testCaseDirectoryModel),
                                )
                              ],
                            );
                          },
                        ),
                      ),
                    ),
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  Column tableTestCaseWidget(BuildContext context, NavigationModel navigationModel, TestCaseDirectoryModel testCaseDirectoryModel) {
    return Column(
      children: [
        Container(
          alignment: Alignment.centerLeft,
          margin: const EdgeInsets.only(bottom: 10),
          decoration: BoxDecoration(
            border: Border.all(
              color: const Color.fromRGBO(212, 212, 212, 1),
            ),
            color: Colors.grey[100],
          ),
          padding: (directoryPathList != null && directoryPathList!.isNotEmpty) ? const EdgeInsets.fromLTRB(3, 3, 12, 3) : const EdgeInsets.all(9),
          child: (directoryPathList != null && directoryPathList!.isNotEmpty)
              ? breadCrumbWidget(navigationModel)
              : Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        if (!isExpandDirectory)
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                isExpandDirectory = true;
                              });
                            },
                            child: const Icon(Icons.arrow_forward),
                          ),
                        MultiLanguageText(
                          name: "test",
                          defaultValue: "Test",
                          style: Style(context).styleTextDataColumn,
                        ),
                      ],
                    ),
                    addTestWidget(context)
                  ],
                ),
        ),
        if (resultListTestCase.isNotEmpty)
          CustomDataTableWidget(
            columns: [
              Expanded(
                child: MultiLanguageText(
                  name: "code",
                  defaultValue: "Code",
                  isLowerCase: false,
                  style: Style(context).styleTextDataColumn,
                ),
              ),
              Expanded(
                flex: 3,
                child: MultiLanguageText(
                  name: "name",
                  defaultValue: "Name",
                  isLowerCase: false,
                  style: Style(context).styleTextDataColumn,
                ),
              ),
              Expanded(
                child: MultiLanguageText(
                  name: "owner",
                  defaultValue: "Owner",
                  isLowerCase: false,
                  style: Style(context).styleTextDataColumn,
                ),
              ),
              Expanded(
                child: Center(
                  child: MultiLanguageText(
                    name: "create_date",
                    defaultValue: "Create date",
                    isLowerCase: false,
                    style: Style(context).styleTextDataColumn,
                  ),
                ),
              ),
              Expanded(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    MultiLanguageText(
                      name: "status",
                      defaultValue: "Status",
                      isLowerCase: false,
                      style: Style(context).styleTextDataColumn,
                    ),
                    Tooltip(
                      message: multiLanguageString(
                          name: "active_draft_review", defaultValue: "Green = Active\nGrey = Draft\nRed = Review ", context: context),
                      child: const Icon(
                        Icons.info,
                        color: Colors.grey,
                        size: 16,
                      ),
                    ),
                  ],
                ),
              ),
              if (widget.type != "action" && widget.type != "test_api" && widget.type != "test_jdbc")
                Expanded(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      MultiLanguageText(
                        name: "last_run",
                        defaultValue: "Last run",
                        isLowerCase: false,
                        style: Style(context).styleTextDataColumn,
                      ),
                      Tooltip(
                        message: multiLanguageString(
                            name: "pass_fail_waiting_error_processing_canceled",
                            defaultValue: "Green = Pass\nRed = Fail\nOrange = Waiting\nGrey = Error\nBlue = Processing\nBlack = Canceled",
                            context: context),
                        child: const Icon(
                          Icons.info,
                          color: Colors.grey,
                          size: 16,
                        ),
                      ),
                    ],
                  ),
                ),
              Container(
                width: 38,
              ),
            ],
            rows: [
              for (var resultTestCase in resultListTestCase)
                CustomDataRow(
                  cells: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Row(
                          children: [
                            const Icon(Icons.description, color: Color.fromRGBO(61, 148, 254, 1)),
                            const SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: Text(
                                resultTestCase["name"],
                                style: Style(context).styleTextDataCell,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Text(
                          resultTestCase["title"] ?? "",
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: Style(context).styleTextDataCell,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Text(
                          resultTestCase["sysUser"]["fullname"],
                          style: Style(context).styleTextDataCell,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Center(
                          child: Text(
                            DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultTestCase["createDate"]).toLocal()),
                            style: Style(context).styleTextDataCell,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Center(
                          child: Badge3StatusWidget(
                            status: resultTestCase["status"],
                          ),
                        ),
                      ),
                    ),
                    if (widget.type != "action" && widget.type != "test_api" && widget.type != "test_jdbc")
                      Expanded(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            InkWell(
                              hoverColor: Colors.transparent,
                              onTap: () {
                                navigationModel.navigate(
                                  "/qa-platform/project/${navigationModel.prjProjectId}/runs/${widget.type}/${resultTestCase["id"]}",
                                );
                              },
                              child: GetLastTestRun(
                                key: ValueKey(resultTestCase["id"]),
                                idProject: prjProjectId,
                                id: resultTestCase["id"],
                              ),
                            ),
                          ],
                        ),
                      ),
                    PopupMenuButton(
                      offset: const Offset(5, 50),
                      tooltip: '',
                      splashRadius: 10,
                      icon: const Icon(
                        Icons.more_vert,
                      ),
                      itemBuilder: (context) => [
                        if (widget.type != "test_api" && widget.type != "test_case" && widget.type != "test_jdbc" && widget.type != "manual_test")
                          PopupMenuItem(
                            padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                            child: ListTile(
                              onTap: () {
                                Navigator.pop(context);
                                Navigator.of(context).push(
                                  createRoute(
                                    EditorPageRootWidget(
                                      testCaseType: widget.type,
                                      tfTestDirectoryId: parentDirectoryId,
                                      testCaseId: resultTestCase["id"],
                                      callback: () {
                                        futureListTfDirectoryTestCase = getListTfTestCase(currentPageTestCase, findRequestTestCase);
                                      },
                                    ),
                                  ),
                                );
                              },
                              contentPadding: const EdgeInsets.all(0),
                              hoverColor: Colors.transparent,
                              title: const MultiLanguageText(name: "config", defaultValue: "Config"),
                              leading: const Icon(Icons.settings, color: Color.fromARGB(255, 112, 114, 119)),
                            ),
                          ),
                        PopupMenuItem(
                          padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                          child: ListTile(
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.of(context).push(
                                createRoute(
                                  DialogOptionTestWidget(
                                    parentId: parentDirectoryId,
                                    testCaseType: widget.type,
                                    type: "edit",
                                    title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                    selectedItem: resultTestCase,
                                    callbackOptionTest: (result) async {
                                      bool resultFunction = await handleEditTfTestCase(result, resultTestCase["id"]);
                                      if (resultFunction && mounted) {
                                        Navigator.of(context).pop();
                                        showToast(
                                            context: context,
                                            msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
                                            color: Colors.greenAccent,
                                            icon: const Icon(Icons.done));
                                      }
                                    },
                                  ),
                                ),
                              );
                            },
                            contentPadding: const EdgeInsets.all(0),
                            hoverColor: Colors.transparent,
                            title: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                            leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
                          ),
                        ),
                        PopupMenuItem(
                          padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                          child: ListTile(
                            onTap: () {
                              Navigator.pop(context);
                              dialogDeleteTestCase(resultTestCase);
                            },
                            contentPadding: const EdgeInsets.all(0),
                            hoverColor: Colors.transparent,
                            title: const MultiLanguageText(name: "delete", defaultValue: "Delete"),
                            leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                          ),
                        ),
                        PopupMenuItem(
                          padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                          child: ListTile(
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.of(context).push(
                                createRoute(
                                  DialogOptionTestWidget(
                                    parentId: parentDirectoryId,
                                    testCaseType: widget.type,
                                    type: "duplicate",
                                    title: multiLanguageString(name: "duplicate", defaultValue: "Duplicate", context: context),
                                    selectedItem: resultTestCase,
                                    callbackOptionTest: (result) {
                                      handleDuplicateTfTestCase(result);
                                    },
                                  ),
                                ),
                              );
                            },
                            contentPadding: const EdgeInsets.all(0),
                            hoverColor: Colors.transparent,
                            title: const MultiLanguageText(name: "duplicate", defaultValue: "Duplicate"),
                            leading: const Icon(Icons.control_point_duplicate, color: Color.fromARGB(255, 112, 114, 119)),
                          ),
                        ),
                        if (widget.type != "action" && widget.type != "test_api" && widget.type != "test_jdbc")
                          PopupMenuItem(
                            padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                            child: ListTile(
                              onTap: () {
                                if (widget.type == "test_case") {
                                  navigationModel.navigate(
                                      "/qa-platform/project/${navigationModel.prjProjectId}/test-list/test_case/${resultTestCase["id"]}/run/${resultTestCase["parentId"]}");
                                } else {
                                  navigationModel.navigate(
                                      "/qa-platform/project/${navigationModel.prjProjectId}/test-list/${widget.type}/${resultTestCase["id"]}/run");
                                }
                              },
                              contentPadding: const EdgeInsets.all(0),
                              hoverColor: Colors.transparent,
                              title: const MultiLanguageText(name: "run", defaultValue: "RUN", isLowerCase: true),
                              leading: const Icon(Icons.play_arrow, color: Color.fromARGB(255, 112, 114, 119)),
                            ),
                          ),
                        PopupMenuItem(
                          padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                          child: ListTile(
                            onTap: () async {
                              fileContent = await selectFile(['json']);
                              if (fileContent != null) {
                                await uploadFile(
                                    fileContent, "/test-framework-api/user/test-framework-file/tf-test-case/${resultTestCase["id"]}/import-content");
                              }
                              if (mounted) {
                                Navigator.pop(context);
                              }
                            },
                            contentPadding: const EdgeInsets.all(0),
                            hoverColor: Colors.transparent,
                            title: const MultiLanguageText(name: "import", defaultValue: "IMPORT", isLowerCase: true),
                            leading: const Icon(Icons.upload_file, color: Color.fromARGB(255, 112, 114, 119)),
                          ),
                        ),
                        PopupMenuItem(
                          padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                          child: ListTile(
                            onTap: () async {
                              await downloadFile(resultTestCase["id"]);
                              if (mounted) {
                                Navigator.pop(context);
                              }
                            },
                            contentPadding: const EdgeInsets.all(0),
                            hoverColor: Colors.transparent,
                            title: const MultiLanguageText(name: "export", defaultValue: "EXPORT", isLowerCase: true),
                            leading: const Icon(Icons.cloud_download, color: Color.fromARGB(255, 112, 114, 119)),
                          ),
                        ),
                        PopupMenuItem(
                          padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                          child: ListTile(
                            onTap: () {
                              testCaseDirectoryModel.setTestCaseDirectory(resultTestCase);
                              showToast(
                                  context: context,
                                  msg: multiLanguageString(
                                      name: "copy_result_test_case_successful",
                                      defaultValue: "Copy '\$0' successful, choose a directory to move",
                                      variables: ["${resultTestCase["title"]}"],
                                      context: context),
                                  color: Colors.greenAccent,
                                  icon: const Icon(Icons.done));
                              Navigator.pop(context);
                            },
                            contentPadding: const EdgeInsets.all(0),
                            hoverColor: Colors.transparent,
                            title: const MultiLanguageText(name: "move_to_directory", defaultValue: "Move to directory"),
                            leading: const Icon(Icons.drive_file_move, color: Color.fromARGB(255, 112, 114, 119)),
                          ),
                        ),
                      ],
                    ),
                  ],
                  onSelectChanged: () async {
                    if (widget.type != "test_api" && widget.type != "test_case" && widget.type != "test_jdbc" && widget.type != "manual_test") {
                      Navigator.of(context).push(
                        createRoute(
                          EditorPageRootWidget(
                            testCaseType: widget.type,
                            tfTestDirectoryId: parentDirectoryId,
                            testCaseId: resultTestCase["id"],
                            callback: () {
                              futureListTfDirectoryTestCase = getListTfTestCase(currentPageTestCase, findRequestTestCase);
                            },
                          ),
                        ),
                      );
                    } else {
                      Navigator.of(context).push(
                        createRoute(
                          DialogOptionTestWidget(
                            parentId: parentDirectoryId,
                            testCaseType: widget.type,
                            type: "edit",
                            title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                            selectedItem: resultTestCase,
                            callbackOptionTest: (result) async {
                              bool resultFunction = await handleEditTfTestCase(result, resultTestCase["id"]);
                              if (resultFunction && mounted) {
                                Navigator.of(context).pop();
                                showToast(
                                    context: context,
                                    msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
                                    color: Colors.greenAccent,
                                    icon: const Icon(Icons.done));
                              }
                            },
                          ),
                        ),
                      );
                    }
                  },
                ),
            ],
          ),
        DynamicTablePagging(
          rowCountListTestCase,
          currentPageTestCase,
          rowPerPageTestCase,
          pageChangeHandler: (page) {
            setState(() {
              futureListTfDirectoryTestCase = getListTfTestCase(page, findRequestTestCase);
            });
          },
          rowPerPageChangeHandler: (rowPerPage) {
            setState(() {
              rowPerPageTestCase = rowPerPage!;
              futureListTfDirectoryTestCase = getListTfTestCase(1, findRequestTestCase);
            });
          },
        ),
      ],
    );
  }

  OutlinedButton addTestWidget(BuildContext context) {
    return OutlinedButton.icon(
      style: OutlinedButton.styleFrom(
        backgroundColor: const Color.fromARGB(175, 8, 196, 80),
      ),
      icon: const Icon(
        Icons.add,
        color: Color.fromRGBO(49, 49, 49, 1),
      ),
      label: MultiLanguageText(
        name: "new_test",
        defaultValue: "New test",
        isLowerCase: false,
        style: TextStyle(fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
      ),
      onPressed: () {
        if (widget.type != "test_api" && widget.type != "test_case" && widget.type != "test_jdbc" && widget.type != "manual_test") {
          Navigator.of(context).push(
            createRoute(
              DialogOptionTestWidget(
                testCaseType: widget.type,
                type: "create",
                title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                selectedItem: const {},
                callbackOptionTest: (result) async {
                  var response = await httpPut("/test-framework-api/user/test-framework/tf-test-case", result, context);
                  if (response.containsKey("body") && response["body"] is String == false && mounted) {
                    if (response["body"].containsKey("errorMessage")) {
                      showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
                    } else {
                      showToast(
                          context: context,
                          msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
                          color: Colors.greenAccent,
                          icon: const Icon(Icons.done));
                      Navigator.pop(context);
                      Navigator.of(context).push(
                        createRoute(
                          EditorPageRootWidget(
                            testCaseType: widget.type,
                            tfTestDirectoryId: parentDirectoryId,
                            callback: () {
                              futureListTfDirectoryTestCase = getListTfTestCase(currentPageTestCase, findRequestTestCase);
                            },
                            testCaseId: response["body"]["result"],
                          ),
                        ),
                      );
                    }
                  }
                },
                parentId: parentDirectoryId,
              ),
            ),
          );
        } else {
          Navigator.of(context).push(
            createRoute(
              DialogOptionTestWidget(
                parentId: parentDirectoryId,
                testCaseType: widget.type,
                type: "create",
                title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                callbackOptionTest: (result) async {
                  handleAddTfTestCase(result);
                },
                selectedItem: const {},
              ),
            ),
          );
        }
      },
    );
  }

  Container tableDirectoryWidget(BuildContext context, TestCaseDirectoryModel testCaseDirectoryModel, NavigationModel navigationModel) {
    return Container(
      constraints: const BoxConstraints(maxWidth: 350, maxHeight: 560),
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: const Color.fromRGBO(212, 212, 212, 1),
              ),
              color: Colors.grey[100],
            ),
            padding: const EdgeInsets.all(11),
            child: Row(
              children: [
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      MultiLanguageText(
                        name: "directory",
                        defaultValue: "Directory",
                        style: Style(context).styleTextDataColumn,
                      ),
                      Row(
                        children: [
                          if (testCaseDirectoryModel.testCaseDirectory != null &&
                              (testCaseDirectoryModel.testCaseDirectory["tfTestDirectoryId"] != null ||
                                  testCaseDirectoryModel.testCaseDirectory["parentId"] != null))
                            GestureDetector(
                              onTap: () async {
                                onPaste(null, testCaseDirectoryModel, context);
                              },
                              child: const Icon(Icons.content_paste),
                            ),
                          GestureDetector(
                            onTap: () {
                              Navigator.of(context).push(
                                createRoute(
                                  OptionDirectoryWidget(
                                    directoryType: widget.type,
                                    type: "create",
                                    title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                                    callbackOptionTest: (result) {
                                      handleAddTfDirectory(result);
                                    },
                                    prjProjectId: prjProjectId,
                                  ),
                                ),
                              );
                            },
                            child: const Icon(Icons.add),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      isExpandDirectory = false;
                    });
                  },
                  child: const Icon(Icons.arrow_back),
                ),
              ],
            ),
          ),
          Expanded(
            child: Container(
              color: Colors.grey[100],
              margin: const EdgeInsets.only(top: 10),
              child: ListView(
                padding: const EdgeInsets.only(
                  left: 0,
                ),
                children: [
                  for (var resultDirectory in resultListDirectory)
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 5),
                      child: DirectoryItemWidget(
                        key: Key(resultDirectory["id"]),
                        testCaseType: widget.type,
                        prjProjectId: prjProjectId,
                        directory: resultDirectory,
                        parentDirectoryId: parentDirectoryId,
                        onDeleteDirectory: (id) => handleDeleteDirectory(id),
                        onPaste: (id) => onPaste(id, testCaseDirectoryModel, context),
                        onClickDirectory: (id) async {
                          setState(() {
                            parentDirectoryId = id;
                          });
                          getDirectoryPathList();
                          if (findRequestTestCase.containsKey("tfTestDirectoryIdIsNull")) {
                            findRequestTestCase.remove("tfTestDirectoryIdIsNull");
                          }
                          findRequestTestCase["tfTestDirectoryId"] = parentDirectoryId;
                          await getListTfTestCase(currentPageTestCase, findRequestTestCase);
                        },
                      ),
                    ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> onPaste(String? directoryId, TestCaseDirectoryModel testCaseDirectoryModel, BuildContext context) async {
    if (directoryId != null) {
      if (testCaseDirectoryModel.testCaseDirectory["testCaseType"] != null) {
        testCaseDirectoryModel.testCaseDirectory["tfTestDirectoryId"] = directoryId;
        testCaseDirectoryModel.testCaseDirectory["testCaseType"] = widget.type;
      } else {
        testCaseDirectoryModel.testCaseDirectory["parentId"] = directoryId;
        testCaseDirectoryModel.testCaseDirectory["directoryType"] = widget.type;
      }
    } else {
      if (testCaseDirectoryModel.testCaseDirectory["testCaseType"] != null) {
        if (testCaseDirectoryModel.testCaseDirectory.containsKey("tfTestDirectoryId")) {
          testCaseDirectoryModel.testCaseDirectory.remove("tfTestDirectoryId");
        }
        testCaseDirectoryModel.testCaseDirectory["testCaseType"] = widget.type;
      }
      if (testCaseDirectoryModel.testCaseDirectory["directoryType"] != null) {
        if (testCaseDirectoryModel.testCaseDirectory.containsKey("parentId")) {
          testCaseDirectoryModel.testCaseDirectory.remove("parentId");
        }
        testCaseDirectoryModel.testCaseDirectory["directoryType"] = widget.type;
      }
    }
    bool resultFunction = false;
    if (testCaseDirectoryModel.testCaseDirectory["type"] == "test_scenario") {
      resultFunction = await handleEditTfTestCase(testCaseDirectoryModel.testCaseDirectory, testCaseDirectoryModel.testCaseDirectory["id"]);
    }
    if (testCaseDirectoryModel.testCaseDirectory["type"] == "directory") {
      resultFunction = await handleEditTfDirectory(testCaseDirectoryModel.testCaseDirectory, testCaseDirectoryModel.testCaseDirectory["id"]);
    }
    if (resultFunction && mounted) {
      showToast(
          context: context,
          msg: "Move '${testCaseDirectoryModel.testCaseDirectory["title"]}' success",
          color: Colors.greenAccent,
          icon: const Icon(Icons.done));
    }
    testCaseDirectoryModel.setTestCaseDirectory(null);
  }

  Row breadCrumbWidget(NavigationModel navigationModel) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            IconButton(
              onPressed: () async {
                setState(() {
                  parentDirectoryId = null;
                  directoryPathList = null;
                });
                if (findRequestTestCase.containsKey("tfTestDirectoryId")) {
                  findRequestTestCase.remove("tfTestDirectoryId");
                }
                findRequestTestCase["tfTestDirectoryIdIsNull"] = true;
                await getListTfTestCase(currentPageTestCase, findRequestTestCase);
              },
              icon: const FaIcon(FontAwesomeIcons.house, size: 16),
              color: Colors.grey,
              splashRadius: 10,
              tooltip: multiLanguageString(name: "root", defaultValue: "Root", context: context),
            ),
            for (var directoryPath in directoryPathList!)
              RichText(
                  text: TextSpan(children: [
                const TextSpan(
                  text: " / ",
                  style: TextStyle(color: Colors.grey),
                ),
                TextSpan(
                    text: "${directoryPath["title"]}",
                    style: TextStyle(
                        color: directoryPathList!.last == directoryPath ? Colors.blue : Colors.grey,
                        fontSize: context.fontSizeManager.fontSizeText16,
                        fontWeight: FontWeight.bold),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () async {
                        setState(() {
                          parentDirectoryId = directoryPath["id"];
                        });
                        getDirectoryPathList();
                        if (findRequestTestCase.containsKey("tfTestDirectoryIdIsNull")) {
                          findRequestTestCase.remove("tfTestDirectoryIdIsNull");
                        }
                        findRequestTestCase["tfTestDirectoryId"] = parentDirectoryId;
                        await getListTfTestCase(currentPageTestCase, findRequestTestCase);
                      }),
              ]))
          ],
        ),
        addTestWidget(context)
      ],
    );
  }

  Padding headerWidget(BuildContext context) {
    Map<String, String> mapTestCaseType = {
      "test_case": multiLanguageString(name: "map_test_case", defaultValue: "Test case", context: context),
      "test_scenario": multiLanguageString(name: "map_test_scenario", defaultValue: "Test scenario", context: context),
      "manual_test": multiLanguageString(name: "map_test_manual", defaultValue: "Test manual", context: context),
      "test_suite": multiLanguageString(name: "map_test_suite", defaultValue: "Test suite", context: context),
      "test_api": multiLanguageString(name: "map_api", defaultValue: "Api", context: context),
      "test_jdbc": multiLanguageString(name: "map_jdbc", defaultValue: "JDBC", context: context),
      "action": multiLanguageString(name: "map_shared_steps", defaultValue: "Shared steps", context: context),
    };
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: HeaderListTestWidget(
              type: isViewTypeDirectory ? "Directory" : mapTestCaseType[widget.type] ?? "",
              title: mapTestCaseType[widget.type] ?? "",
              isDirectory: true,
              onChangedDirectory: () async {
                onLoading(context);
                setState(() {
                  isViewTypeDirectory = !isViewTypeDirectory;
                });
                if (isViewTypeDirectory) {
                  if (parentDirectoryId != null) {
                    findRequestTestCase["tfTestDirectoryId"] = parentDirectoryId;
                  } else {
                    if (findRequestTestCase.containsKey("tfTestDirectoryId")) {
                      findRequestTestCase.remove("tfTestDirectoryId");
                    }
                    findRequestTestCase["tfTestDirectoryIdIsNull"] = true;
                  }
                } else {
                  if (findRequestTestCase.containsKey("tfTestDirectoryIdIsNull")) {
                    findRequestTestCase.remove("tfTestDirectoryIdIsNull");
                  }
                  if (findRequestTestCase.containsKey("tfTestDirectoryId")) {
                    findRequestTestCase.remove("tfTestDirectoryId");
                  }
                }
                await getListTfTestCase(currentPageTestCase, findRequestTestCase);
                if (mounted) {
                  Navigator.pop(context);
                }
              },
              callbackFilter: (Map text) {
                findRequestDirectory.addAll({"titleLike": text["titleLike"]});
                var newMap = {};
                text.forEach((key, value) {
                  if (value != null && (value is int || value.isNotEmpty)) newMap.addAll({key: value});
                });
                findRequestTestCase = newMap;
                if (newMap.isEmpty) {
                  findRequestTestCase["tfTestDirectoryIdIsNull"] = true;
                }
                getListTfDirectory(findRequestDirectory);
                getListTfTestCase(currentPageTestCase, findRequestTestCase);
              },
              callbackExport: (result) async {
                var newMap = {};
                result.forEach((key, value) {
                  if (value != null && (value is int || value.isNotEmpty)) newMap.addAll({key: value});
                });
                findRequestTestCase = newMap;
                if (newMap.isEmpty) {
                  findRequestTestCase["tfTestDirectoryIdIsNull"] = true;
                }
                var findRequest = findRequestTestCase;
                if (findRequestTestCase["titleLike"] != null && findRequestTestCase["titleLike"].isNotEmpty) {
                  findRequest["titleLike"] = "%${findRequestTestCase["titleLike"]}%";
                }
                if (findRequestTestCase["nameLike"] != null && findRequestTestCase["nameLike"].isNotEmpty) {
                  findRequest["nameLike"] = "%${findRequestTestCase["nameLike"]}%";
                }
                findRequest["testCaseType"] = widget.type;
                findRequest["prjProjectId"] = prjProjectId;
                if (findRequest.containsKey("tfTestDirectoryId")) {
                  findRequest.remove("tfTestDirectoryId");
                }
                if (findRequest.containsKey("queryOffset")) {
                  findRequest.remove("queryOffset");
                }
                if (findRequest.containsKey("queryLimit")) {
                  findRequest.remove("queryLimit");
                }
                File? downloadedFile = await httpDownloadFileWithBodyRequest(
                    context,
                    "/test-framework-api/user/test-framework-file/tf-test-case/export-excel",
                    Provider.of<SecurityModel>(context, listen: false).authorization,
                    'Output.xlsx',
                    findRequest);
                if (downloadedFile != null && mounted) {
                  var snackBar = SnackBar(
                    content: MultiLanguageText(name: "file_downloaded_to", defaultValue: "File downloaded to \$0 .", variables: ["$downloadedFile"]),
                  );
                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                }
              },
              callbackImport: () async {
                fileTestCase = await selectFile(['xlsx']);
                if (fileTestCase != null) {
                  await uploadFile(fileTestCase, "/test-framework-api/user/project-file/prj-project/$prjProjectId/tf-test-case/import-excel");
                }
                getListTfTestCase(currentPageTestCase, findRequestTestCase);
              },
            ),
          ),
        ],
      ),
    );
  }

  uploadFile(PlatformFile? file, String url) async {
    final request = http.MultipartRequest(
      "POST",
      Uri.parse(
        "$baseUrl$url",
      ),
    );
    request.headers.addAll({"Authorization": Provider.of<SecurityModel>(context, listen: false).authorization!});
    request.files.add(http.MultipartFile.fromBytes('file', await readFile(file!), filename: file.name));
    var resp = await request.send();
    var responseDecode = jsonDecode(await resp.stream.bytesToString());
    if (responseDecode.containsKey("errorMessage") && mounted) {
      showToast(context: context, msg: responseDecode["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
    } else if (mounted) {
      showToast(
          context: context,
          msg: multiLanguageString(name: "upload_success", defaultValue: "Upload success", context: context),
          color: Colors.greenAccent,
          icon: const Icon(Icons.done));
    }
  }

  downloadFile(String id) async {
    File? downloadedFile = await httpDownloadFile(context, "/test-framework-api/user/test-framework-file/tf-test-case/$id/export-content",
        Provider.of<SecurityModel>(context, listen: false).authorization, 'Output.zip');
    if (downloadedFile != null && mounted) {
      var snackBar = SnackBar(
        content: MultiLanguageText(name: "file_downloaded_to", defaultValue: "File downloaded to \$0 .", variables: ["$downloadedFile"]),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  dialogDeleteTestCase(selectedItem) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogDelete(
          selectedItem: selectedItem,
          callback: () {
            handleDeleteTestCase(selectedItem["id"]);
          },
          type: 'test',
        );
      },
    );
  }
}
