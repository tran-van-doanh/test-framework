import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/common/row_code_with_tooltip.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/editor/property_widget/root_property_widget.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';
import 'package:test_framework_view/page/list_test/tests/directory/dialog_directory.dart';
import 'package:test_framework_view/page/list_test/tests/test/dialog_option_test_case.dart';
import 'package:test_framework_view/page/list_test/tests/label/label_list_widget.dart';
import 'package:test_framework_view/page/list_test/tests/label/manage_label_widget.dart';
import 'package:test_framework_view/type.dart';
import '../../../../common/dynamic_dropdown_button.dart';
import '../../../../components/toast.dart';
import '../../../../common/custom_state.dart';
import '../../test_api/header_api.dart';
import '../../test_api/multipart_form_api.dart';
import '../../test_api/response_body.dart';

class DialogOptionTestWidget extends StatefulWidget {
  final String testCaseType;
  final String type;
  final String title;
  final dynamic selectedItem;
  final Function? callbackOptionTest;
  final String? parentId;
  final bool isTestCase;
  const DialogOptionTestWidget(
      {Key? key,
      required this.testCaseType,
      required this.type,
      this.selectedItem,
      this.callbackOptionTest,
      this.parentId,
      this.isTestCase = false,
      required this.title})
      : super(key: key);

  @override
  State<DialogOptionTestWidget> createState() => _DialogOptionTestWidgetState();
}

class _DialogOptionTestWidgetState extends State<DialogOptionTestWidget> {
  String testCaseType = "";
  bool isEmptyDirectoryPathList = false;
  @override
  void initState() {
    super.initState();
    testCaseType = widget.testCaseType;
  }

  @override
  Widget build(BuildContext context) {
    if (testCaseType == "test_case") {
      return DialogOptionTestCaseWidget(
        isEmptyDirectoryPathList: isEmptyDirectoryPathList,
        type: widget.type,
        title: widget.title,
        testCaseType: testCaseType,
        selectedItem: widget.selectedItem,
        parentId: widget.parentId,
        callbackOptionTest: widget.callbackOptionTest,
        callbackTestCaseType: (testCaseType) {
          setState(() {
            isEmptyDirectoryPathList = true;
            this.testCaseType = testCaseType;
          });
        },
      );
    }
    return DialogOptionTestOtherWidget(
      isEmptyDirectoryPathList: isEmptyDirectoryPathList,
      type: widget.type,
      title: widget.title,
      testCaseType: testCaseType,
      selectedItem: widget.selectedItem,
      parentId: widget.parentId,
      callbackOptionTest: widget.callbackOptionTest,
      callbackTestCaseType: (testCaseType) {
        setState(() {
          isEmptyDirectoryPathList = true;
          this.testCaseType = testCaseType;
        });
      },
    );
  }
}

class DialogOptionTestOtherWidget extends StatefulWidget {
  final String testCaseType;
  final String type;
  final String title;
  final dynamic selectedItem;
  final Function? callbackOptionTest;
  final Function callbackTestCaseType;
  final String? parentId;
  final bool isEmptyDirectoryPathList;
  const DialogOptionTestOtherWidget({
    Key? key,
    required this.type,
    this.selectedItem,
    this.callbackOptionTest,
    required this.testCaseType,
    this.parentId,
    required this.callbackTestCaseType,
    required this.isEmptyDirectoryPathList,
    required this.title,
  }) : super(key: key);

  @override
  State<DialogOptionTestOtherWidget> createState() => _DialogOptionTestOtherWidgetState();
}

class _DialogOptionTestOtherWidgetState extends CustomState<DialogOptionTestOtherWidget> with TickerProviderStateMixin {
  late Future futureOptionTest;
  final TextEditingController _testsCodeController = TextEditingController();
  final TextEditingController _bodyDataController = TextEditingController();
  final TextEditingController _protocolController = TextEditingController();
  final TextEditingController _hostNameController = TextEditingController();
  final TextEditingController _portNumberController = TextEditingController();
  final TextEditingController _pathController = TextEditingController();
  final TextEditingController _testsDesController = TextEditingController();
  final TextEditingController _testsNameController = TextEditingController();
  final TextEditingController _testsPurposeController = TextEditingController();
  final TextEditingController _testsScenarioController = TextEditingController();
  final TextEditingController _testsPreconditionController = TextEditingController();
  final TextEditingController _testsStepController = TextEditingController();
  final TextEditingController _testsExpectedResultController = TextEditingController();
  final TextEditingController _testsTestDataController = TextEditingController();
  final TextEditingController _testsReviewCommentController = TextEditingController();
  List labelsList = [];
  late TabController tabController;
  String? testCaseType;
  String? jdbcType;
  String? _httpMethod;
  String? _apiType;
  String? _responseBody;
  int? _status = 0;
  int? testPriority = 0;
  String? wfStatusId;
  String? _testsComponent;
  dynamic _testsVersion;
  dynamic _testsEnvironment;
  var _listComponents = [];
  var _listProject = [];
  var _listStatus = [];
  var _listVersions = [];
  var _listEnvironment = [];
  final _formKey = GlobalKey<FormState>();
  var listCheckDuplicateParam = {};
  var listCheckDuplicateOutput = {};
  var listCheckDuplicateClient = {};
  List<dynamic>? sysPermPathList;
  var prjProjectId = "";
  TestCaseConfigModel? testCaseConfigModel;
  List directoryPathList = [];
  List outputFieldList = [];
  List multiPartFormList = [];
  List headerApiList = [];
  List pathParameterList = [];
  ScrollController scrollController = ScrollController();
  @override
  void dispose() {
    _testsCodeController.dispose();
    _bodyDataController.dispose();
    _protocolController.dispose();
    _hostNameController.dispose();
    _portNumberController.dispose();
    _pathController.dispose();
    _testsDesController.dispose();
    _testsNameController.dispose();
    _testsPurposeController.dispose();
    _testsScenarioController.dispose();
    _testsPreconditionController.dispose();
    _testsStepController.dispose();
    _testsExpectedResultController.dispose();
    _testsTestDataController.dispose();
    _testsReviewCommentController.dispose();
    tabController.dispose();
    scrollController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    prjProjectId = Provider.of<NavigationModel>(context, listen: false).prjProjectId ?? "";
    testCaseConfigModel = Provider.of<TestCaseConfigModel>(context, listen: false);
    futureOptionTest = getInfoPage();
    super.initState();
  }

  setDefaultTab() {
    tabController = TabController(
        initialIndex: (testCaseType == "test_api" &&
                testCaseConfigModel?.testCaseConfigMap[testCaseConfigModel?.testCaseIdList.last]["initialIndex"] > 2)
            ? 4
            : (testCaseType == "manual_test" && testCaseConfigModel?.testCaseConfigMap[testCaseConfigModel?.testCaseIdList.last]["initialIndex"] > 1)
                ? 1
                : testCaseConfigModel?.testCaseConfigMap[testCaseConfigModel?.testCaseIdList.last]["initialIndex"],
        length: (testCaseType != "test_api" && testCaseType != "manual_test" && testCaseType != "test_jdbc")
            ? 4
            : (testCaseType == "test_api")
                ? 5
                : 2,
        vsync: this);
  }

  getInfoPage() async {
    if (widget.type == "duplicate") {
      getListProject();
    }
    getListStatus();
    if (!widget.isEmptyDirectoryPathList) {
      if (widget.selectedItem != null && widget.selectedItem["tfTestDirectoryId"] != null) {
        getDirectoryPath(widget.selectedItem["tfTestDirectoryId"]);
      } else if (widget.parentId != null) {
        getDirectoryPath(widget.parentId!);
      }
    }
    await getComponentAndVersion();
    await setInitValue();
    setDefaultTab();
    return 0;
  }

  getDirectoryPath(String id) async {
    var directoryParentResponse = await httpGet("/test-framework-api/user/test-framework/tf-test-directory/$id/path", context);
    if (directoryParentResponse.containsKey("body") && directoryParentResponse["body"] is String == false) {
      setState(() {
        directoryPathList = directoryParentResponse["body"]["resultList"];
      });
    }
  }

  getListProject() async {
    var response = await httpPost("/test-framework-api/user/project/prj-project/search", {"status": 1}, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listProject = response["body"]["resultList"];
      });
    }
  }

  getListStatus() async {
    var response = await httpPost("/test-framework-api/user/workflow/wf-status/search", {"status": 1, "flowTypes": "test_case"}, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listStatus = response["body"]["resultList"];
      });
    }
  }

  setInitValue() async {
    setState(() {
      testCaseType = widget.testCaseType.isNotEmpty ? widget.testCaseType : null;
      var parameterFieldList = [];
      if (!(widget.type == "create")) {
        if (widget.testCaseType == "test_jdbc" && widget.selectedItem != null) {
          jdbcType = widget.selectedItem["content"]["jdbcType"] ?? "";
          pathParameterList = widget.selectedItem["content"]["pathParameterList"] ?? [];
        }
        if (widget.testCaseType == "test_api" && widget.selectedItem != null) {
          _protocolController.text = widget.selectedItem["content"]["protocol"] ?? "";
          _apiType = widget.selectedItem["content"]["apiType"] ?? "";
          _responseBody = widget.selectedItem["content"]["responseBody"] ?? "";
          _hostNameController.text = widget.selectedItem["content"]["hostname"] ?? "";
          _portNumberController.text = widget.selectedItem["content"]["portNumber"] ?? "";
          _pathController.text = widget.selectedItem["content"]["path"] ?? "";
          _httpMethod = widget.selectedItem["content"]["httpMethod"] ?? "";
          headerApiList = widget.selectedItem["content"]["headerList"] ?? [];
          pathParameterList = widget.selectedItem["content"]["pathParameterList"] ?? [];
          if (_apiType == "rawContent") _bodyDataController.text = widget.selectedItem["content"]["bodyData"] ?? "";
          if (_apiType == "multipart-form") multiPartFormList = widget.selectedItem["content"]["multiPartFormList"] ?? [];
        }
        if (widget.type == "duplicate") {
          _testsCodeController.text = "copy_of_${widget.selectedItem["name"]}";
          _testsNameController.text = "copy_of_${widget.selectedItem["title"]}";
        } else {
          _testsCodeController.text = widget.selectedItem["name"] ?? "";
          _testsNameController.text = widget.selectedItem["title"] ?? "";
        }
        _status = widget.selectedItem["status"] ?? 0;
        testPriority = widget.selectedItem["testPriority"] ?? 0;
        wfStatusId = widget.selectedItem["wfStatusId"] ?? (_listStatus.isNotEmpty ? _listStatus[0]["id"] : null);
        _testsComponent = widget.selectedItem["tfTestComponentId"] ?? (_listComponents.isNotEmpty ? _listComponents[0]["id"] : null);
        _testsVersion = widget.selectedItem["tfTestVersion"] ?? (_listVersions.isNotEmpty ? _listVersions[0] : null);
        _testsEnvironment = widget.selectedItem["tfTestEnvironment"] ?? (_listEnvironment.isNotEmpty ? _listEnvironment[0] : null);
        _testsDesController.text = widget.selectedItem["description"] ?? "";
        _testsPurposeController.text = widget.selectedItem["purpose"] ?? "";
        _testsPreconditionController.text = widget.selectedItem["preconditions"] ?? "";
        _testsTestDataController.text = widget.selectedItem["inputs"] ?? "";
        _testsStepController.text = widget.selectedItem["steps"] ?? "";
        _testsExpectedResultController.text = widget.selectedItem["expectedResults"] ?? "";
        labelsList = widget.selectedItem["labelsList"] ?? [];
        if (testCaseConfigModel?.testCaseConfigMap[testCaseConfigModel?.testCaseIdList.last]["setParameterHistory"]) {
          parameterFieldList.addAll(testCaseConfigModel?.testCaseConfigMap[testCaseConfigModel?.testCaseIdList.last]["parameterFieldList"]);
        } else if (widget.selectedItem["content"] != null) {
          outputFieldList = widget.selectedItem["content"]["outputFieldList"] ?? [];
          parameterFieldList.addAll(widget.selectedItem["content"]["parameterFieldList"] ?? []);
        }
        if ((widget.type == "edit" || widget.type == "duplicate") &&
            widget.selectedItem["content"] != null &&
            !(testCaseConfigModel?.testCaseConfigMap[testCaseConfigModel?.testCaseIdList.last]["typeOptionEditor"] ?? false) &&
            !(testCaseConfigModel?.testCaseConfigMap[testCaseConfigModel?.testCaseIdList.last]["setParameterHistory"])) {
          testCaseConfigModel?.testCaseConfigMap[testCaseConfigModel?.testCaseIdList.last]["clientList"] =
              widget.selectedItem["content"]["clientList"] ??
                  [
                    {
                      "name": "default_client",
                      "platform": "Web",
                    }
                  ];
        }
        if (testCaseConfigModel?.testCaseConfigMap[testCaseConfigModel?.testCaseIdList.last]["typeOptionEditor"] ?? false) {
          if (widget.selectedItem["content"] != null) {
            for (var parameterFieldProvider
                in (testCaseConfigModel?.testCaseConfigMap[testCaseConfigModel?.testCaseIdList.last]["parameterFieldList"] ?? [])) {
              if (!parameterFieldList.map((parameterField) => parameterField["name"]).contains(parameterFieldProvider["name"])) {
                parameterFieldList.add({
                  "dataType": parameterFieldProvider["dataType"],
                  "name": parameterFieldProvider["name"],
                  if (parameterFieldProvider["tfRandomTypeId"] != null) "tfRandomTypeId": parameterFieldProvider["tfRandomTypeId"],
                });
              }
            }
          } else {
            for (var parameterFieldProvider
                in (testCaseConfigModel?.testCaseConfigMap[testCaseConfigModel?.testCaseIdList.last]["parameterFieldList"] ?? [])) {
              parameterFieldList.add({
                "dataType": parameterFieldProvider["dataType"],
                "name": parameterFieldProvider["name"],
                if (parameterFieldProvider["tfRandomTypeId"] != null) "tfRandomTypeId": parameterFieldProvider["tfRandomTypeId"],
              });
            }
          }
        }
      } else {
        wfStatusId = _listStatus.isNotEmpty ? _listStatus[0]["id"] : null;
        _testsComponent = _listComponents.isNotEmpty ? _listComponents[0]["id"] : null;
        if (_listVersions.isNotEmpty) {
          var versionItem = _listVersions.firstWhere(
            (element) => element["defaultVersion"] == 1,
            orElse: () => null,
          );
          _testsVersion = versionItem ?? _listVersions[0];
        }
        if (_listEnvironment.isNotEmpty) {
          var environmentItem = _listEnvironment.firstWhere(
            (element) => element["defaultEnvironment"] == 1,
            orElse: () => null,
          );
          _testsEnvironment = environmentItem ?? _listEnvironment[0];
        }
      }
      WidgetsBinding.instance.addPostFrameCallback((_) {
        testCaseConfigModel!.setParameterFieldList(parameterFieldList);
      });
    });
  }

  getComponentAndVersion() async {
    await getComponentList();
    await getVersionList();
    await getEnvironmentList();
  }

  getComponentList() async {
    var findRequest = {"prjProjectId": prjProjectId, "status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-component/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listComponents = response["body"]["resultList"];
      });
    }
  }

  getVersionList() async {
    var findRequest = {"prjProjectId": prjProjectId, "status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-version/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listVersions = response["body"]["resultList"];
      });
    }
  }

  getEnvironmentList() async {
    var findRequest = {"prjProjectId": prjProjectId, "status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-environment/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listEnvironment = response["body"]["resultList"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<TestCaseConfigModel, LanguageModel>(
      builder: (context, testCaseConfigModel, languageModel, child) {
        return FutureBuilder(
          future: futureOptionTest,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return TestFrameworkRootPageWidget(
                child: Container(
                  color: const Color.fromARGB(255, 250, 250, 250),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      headerWidget(testCaseConfigModel, context),
                      Expanded(
                        child: DefaultTabController(
                          length: (testCaseType != "test_api" && testCaseType != "manual_test" && testCaseType != "test_jdbc")
                              ? 4
                              : (testCaseType == "test_api")
                                  ? 5
                                  : 2,
                          initialIndex: (testCaseType == "test_api" &&
                                  testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["initialIndex"] > 2)
                              ? 4
                              : (testCaseType == "manual_test" &&
                                      testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["initialIndex"] > 1)
                                  ? 1
                                  : testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["initialIndex"],
                          child: Form(
                            key: _formKey,
                            child: FocusTraversalGroup(
                              policy: OrderedTraversalPolicy(),
                              child: Column(
                                children: [
                                  tabControlWidget(testCaseConfigModel),
                                  const Divider(color: Color.fromRGBO(216, 218, 229, 1)),
                                  Expanded(
                                    child: Container(
                                      margin: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                                      child: TabBarView(
                                        controller: tabController,
                                        children: [
                                          informationWidget(context),
                                          if (testCaseType == "test_jdbc") jdbcWidget(),
                                          if (testCaseType != "test_api" && testCaseType != "manual_test" && testCaseType != "test_jdbc")
                                            clientWidget(),
                                          if (testCaseType != "test_jdbc") parameterWidget(testCaseConfigModel),
                                          if (testCaseType != "test_api" && testCaseType != "manual_test" && testCaseType != "test_jdbc")
                                            outputWidget(),
                                          if (testCaseType == "test_api") apiHeaderWidget(),
                                          if (testCaseType == "test_api") apiInformationWidget(),
                                          if (testCaseType == "test_api") apiResponseBodyWidget(),
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      btnWidget(testCaseConfigModel, context)
                    ],
                  ),
                ),
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  Padding btnWidget(TestCaseConfigModel testCaseConfigModel, BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20, top: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ButtonSave(
            onPressed: () {
              if (_formKey.currentState!.validate()) {
                listCheckDuplicateParam.clear();
                listCheckDuplicateOutput.clear();
                for (var element in (testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["parameterFieldList"] ?? [])) {
                  listCheckDuplicateParam.putIfAbsent("${element["name"]}", () => element["name"]);
                }
                for (var element in outputFieldList) {
                  listCheckDuplicateOutput.putIfAbsent("${element["name"]}", () => element["name"]);
                }
                int countExists = 1;
                for (var client in testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"] ?? []) {
                  if (testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"]
                          .where((element) => element["name"] == client["name"])
                          .toList()
                          .length >
                      1) {
                    countExists += 1;
                  }
                }
                if (testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["parameterFieldList"].length ==
                        listCheckDuplicateParam.length &&
                    outputFieldList.length == listCheckDuplicateOutput.length &&
                    countExists == 1) {
                  var result = {};
                  if (widget.selectedItem != null && widget.selectedItem.isNotEmpty && widget.type != "duplicate") {
                    result = widget.selectedItem;
                    result["testCaseType"] = testCaseType;
                    result["title"] = _testsNameController.text;
                    result["name"] = _testsCodeController.text;
                    result["description"] = _testsDesController.text;
                    result["tfTestVersionId"] = _testsVersion["id"];
                    result["tfTestEnvironmentId"] = _testsEnvironment["id"];
                    result["tfTestComponentId"] = _testsComponent;
                    result["content"] ??= {};
                    result["content"]["name"] = _testsNameController.text;
                    result["content"]["parameterFieldList"] =
                        (testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["parameterFieldList"] ?? []);
                    if (testCaseType == "test_jdbc") result["content"]["jdbcType"] = jdbcType;
                    if (testCaseType == "test_api") {
                      result["content"]["apiType"] = _apiType;
                      result["content"]["httpMethod"] = _httpMethod;
                      result["content"]["headerList"] = headerApiList;
                      result["content"]["responseBody"] = _responseBody;
                      result["content"]["protocol"] = _protocolController.text;
                      result["content"]["hostname"] = _hostNameController.text;
                      result["content"]["portNumber"] = _portNumberController.text;
                      result["content"]["path"] = _pathController.text;
                    }
                    if (testCaseType == "test_api" || testCaseType == "test_jdbc") result["content"]["pathParameterList"] = pathParameterList;
                    if (testCaseType == "test_api" && _apiType == "rawContent") {
                      result["content"].containsKey("multiPartFormList") && result["content"].remove("multiPartFormList");
                      result["content"]["bodyData"] = _bodyDataController.text;
                    }
                    if (testCaseType == "test_api" && _apiType == "multipart-form") {
                      result["content"].containsKey("bodyData") && result["content"].remove("bodyData");
                      result["content"]["multiPartFormList"] = multiPartFormList;
                    }
                    result["content"]["outputFieldList"] = outputFieldList;
                    if (testCaseType != "test_api" && testCaseType != "manual_test" && testCaseType != "test_jdbc") {
                      result["content"]["clientList"] =
                          testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"] ?? [];
                    }
                    result["prjProjectId"] = prjProjectId;
                    result["purpose"] = _testsPurposeController.text;
                    result["preconditions"] = _testsPreconditionController.text;
                    result["inputs"] = _testsTestDataController.text;
                    result["steps"] = _testsStepController.text;
                    result["expectedResults"] = _testsExpectedResultController.text;
                    result["labelsList"] = labelsList;
                    if (directoryPathList.isNotEmpty) {
                      result["tfTestDirectoryId"] = directoryPathList.last["id"];
                    } else if (result.containsKey("tfTestDirectoryId")) {
                      result.remove("tfTestDirectoryId");
                    }
                    result["status"] = _status;
                    result["testPriority"] = testPriority;
                    result["wfStatusId"] = wfStatusId;
                  } else {
                    result = {
                      "testCaseType": testCaseType,
                      "title": _testsNameController.text,
                      "name": _testsCodeController.text,
                      "description": _testsDesController.text,
                      "tfTestVersionId": _testsVersion["id"],
                      "tfTestEnvironmentId": _testsEnvironment["id"],
                      "tfTestComponentId": _testsComponent,
                      "content": {
                        "name": _testsCodeController.text,
                        "parameterFieldList":
                            (testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["parameterFieldList"] ?? []),
                        if (testCaseType == "test_jdbc") "jdbcType": jdbcType,
                        if (testCaseType == "test_api") "apiType": _apiType,
                        if (testCaseType == "test_api") "httpMethod": _httpMethod,
                        if (testCaseType == "test_api" && _apiType == "rawContent") "bodyData": _bodyDataController.text,
                        if (testCaseType == "test_api") "protocol": _protocolController.text,
                        if (testCaseType == "test_api") "hostname": _hostNameController.text,
                        if (testCaseType == "test_api") "portNumber": _portNumberController.text,
                        if (testCaseType == "test_api") "path": _pathController.text,
                        if (testCaseType == "test_api") "headerList": headerApiList,
                        if (testCaseType == "test_api" || testCaseType == "test_jdbc") "pathParameterList": pathParameterList,
                        if (testCaseType == "test_api") "responseBody": _responseBody,
                        if (testCaseType == "test_api" && _apiType == "multipart-form") "multiPartFormList": multiPartFormList,
                        "outputFieldList": outputFieldList,
                        if (testCaseType != "test_api" && testCaseType != "manual_test" && testCaseType != "test_jdbc")
                          "clientList": testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"] ?? [],
                        if (testCaseType != "test_api" && testCaseType != "manual_test" && testCaseType != "test_jdbc")
                          "bodyList": widget.selectedItem["content"]?["bodyList"] ?? []
                      },
                      "status": _status,
                      "testPriority": testPriority,
                      "wfStatusId": wfStatusId,
                      "prjProjectId": prjProjectId,
                      "purpose": _testsPurposeController.text,
                      "preconditions": _testsPreconditionController.text,
                      "inputs": _testsTestDataController.text,
                      "steps": _testsStepController.text,
                      "expectedResults": _testsExpectedResultController.text,
                      if (directoryPathList.isNotEmpty) "tfTestDirectoryId": directoryPathList.last["id"],
                      "labelsList": labelsList
                    };
                  }
                  widget.callbackOptionTest!(result);
                } else if (countExists == 1) {
                  showToast(
                      context: context,
                      msg: (testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["parameterFieldList"].length !=
                                  listCheckDuplicateParam.length &&
                              outputFieldList.length == listCheckDuplicateOutput.length)
                          ? multiLanguageString(
                              name: "parameter_name_has_been_duplicated", defaultValue: "Parameter name has been duplicated", context: context)
                          : (outputFieldList.length != listCheckDuplicateOutput.length &&
                                  testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["parameterFieldList"].length ==
                                      listCheckDuplicateParam.length)
                              ? multiLanguageString(
                                  name: "output_name_has_been_duplicated", defaultValue: "Output name has been duplicated", context: context)
                              : multiLanguageString(
                                  name: "parameter_and_output_name_has_been_duplicated",
                                  defaultValue: "Parameter and output name has been duplicated",
                                  context: context),
                      color: const Color.fromRGBO(255, 129, 130, 0.4),
                      icon: const Icon(Icons.error));
                } else {
                  showToast(
                      context: context,
                      msg: multiLanguageString(
                          name: "client_name_or_platform_has_been_duplicated",
                          defaultValue: "Client name or platform has been duplicated",
                          context: context),
                      color: const Color.fromRGBO(255, 129, 130, 0.4),
                      icon: const Icon(Icons.error));
                }
              }
            },
          ),
          Container(
            margin: const EdgeInsets.only(left: 5),
            height: 40,
            width: 120,
            child: ElevatedButton(
              onPressed: () {
                testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["parameterFieldList"]
                    .removeWhere((element) => element["name"] == null || element["name"] == "");
                outputFieldList.removeWhere((element) => element["name"] == null || element["name"] == "");
                testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"].removeWhere(
                    (element) => element["name"] == null || element["name"] == "" || element["platform"] == null || element["platform"] == "");
                Navigator.pop(context);
              },
              style: ElevatedButton.styleFrom(
                side: const BorderSide(color: Color.fromRGBO(209, 212, 216, 1)),
                backgroundColor: Colors.white,
                foregroundColor: const Color.fromRGBO(61, 90, 254, 1),
              ),
              child: const MultiLanguageText(
                name: "cancel",
                defaultValue: "Cancel",
                isLowerCase: false,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          )
        ],
      ),
    );
  }

  Column jdbcWidget() {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              flex: 3,
              child: MultiLanguageText(name: "type", defaultValue: "Type", style: Style(context).styleTitleDialog),
            ),
            Expanded(
              flex: 7,
              child: DynamicDropdownButton(
                  borderRadius: 5,
                  value: jdbcType,
                  hint: multiLanguageString(name: "choose_a_type", defaultValue: "Choose a type", context: context),
                  items: [
                    {"value": "query", "name": multiLanguageString(name: "item_query", defaultValue: "Query", context: context)},
                    {
                      "value": "executeUpdate",
                      "name": multiLanguageString(name: "item_execute_update", defaultValue: "Execute Update", context: context)
                    },
                  ].map<DropdownMenuItem<String>>((dynamic result) {
                    return DropdownMenuItem(
                      value: result["value"],
                      child: Text(result["name"]),
                    );
                  }).toList(),
                  onChanged: (newValue) async {
                    setState(() {
                      jdbcType = newValue;
                    });
                  },
                  isRequiredNotEmpty: true),
            ),
          ],
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: DynamicListApiWidget(
              dynamicList: pathParameterList,
              keyForm: _formKey,
              type: multiLanguageString(name: "path_parameter_list", defaultValue: "Path parameter list", context: context)),
        ),
        if (jdbcType == "query")
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: ResponseBodyWidget(responseBodyList: outputFieldList, keyForm: _formKey),
          ),
      ],
    );
  }

  OutputPropertyWidget outputWidget() {
    return OutputPropertyWidget(
      outputFieldList: outputFieldList,
      keyForm: _formKey,
    );
  }

  ClientsPropertyWidget clientWidget() {
    return ClientsPropertyWidget(
      clientList: testCaseConfigModel?.testCaseConfigMap[testCaseConfigModel?.testCaseIdList.last]["clientList"],
      keyForm: _formKey,
    );
  }

  RootPropertyWidget parameterWidget(TestCaseConfigModel testCaseConfigModel) {
    return RootPropertyWidget(
      parameterFieldList: (testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["parameterFieldList"] ?? []),
      keyForm: _formKey,
    );
  }

  DynamicListApiWidget apiHeaderWidget() {
    return DynamicListApiWidget(dynamicList: headerApiList, keyForm: _formKey, type: "Header");
  }

  Column apiResponseBodyWidget() {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              flex: 3,
              child: MultiLanguageText(name: "response_body", defaultValue: "Response body", style: Style(context).styleTitleDialog),
            ),
            Expanded(
              flex: 7,
              child: DynamicDropdownButton(
                  borderRadius: 5,
                  value: _responseBody,
                  hint: multiLanguageString(name: "choose_a_response_body", defaultValue: "Choose a response body", context: context),
                  items: ["none", "text/xml", "application/json"].map<DropdownMenuItem<String>>((String result) {
                    return DropdownMenuItem(
                      value: result,
                      child: Text(result),
                    );
                  }).toList(),
                  onChanged: (newValue) async {
                    setState(() {
                      _responseBody = newValue;
                    });
                  },
                  isRequiredNotEmpty: true),
            ),
          ],
        ),
        if (_responseBody != null && _responseBody != "none")
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: ResponseBodyWidget(responseBodyList: outputFieldList, keyForm: _formKey),
          ),
      ],
    );
  }

  SingleChildScrollView apiInformationWidget() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.only(right: 12),
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.symmetric(vertical: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 3,
                    child: MultiLanguageText(name: "protocol", defaultValue: "Protocol [http]", style: Style(context).styleTitleDialog),
                  ),
                  Expanded(
                    flex: 7,
                    child: DynamicTextField(
                      controller: _protocolController,
                      hintText: "...",
                      isRequiredNotEmpty: true,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 3,
                    child: MultiLanguageText(name: "host_name", defaultValue: "Host name", style: Style(context).styleTitleDialog),
                  ),
                  Expanded(
                    flex: 7,
                    child: DynamicTextField(
                      controller: _hostNameController,
                      hintText: "...",
                      isRequiredNotEmpty: true,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 3,
                    child: MultiLanguageText(name: "port_number", defaultValue: "Port number", style: Style(context).styleTitleDialog),
                  ),
                  Expanded(
                    flex: 7,
                    child: DynamicTextField(
                      controller: _portNumberController,
                      hintText: "...",
                      isRequiredNotEmpty: true,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 3,
                    child: MultiLanguageText(name: "http_method", defaultValue: "HTTP Method", style: Style(context).styleTitleDialog),
                  ),
                  Expanded(
                    flex: 7,
                    child: DynamicDropdownButton(
                        borderRadius: 5,
                        value: _httpMethod,
                        hint: multiLanguageString(name: "choose_a_http_method", defaultValue: "Choose a http method", context: context),
                        items: ["GET", "POST", "PUT", "DELTE", "PATCH"].map<DropdownMenuItem<String>>((String result) {
                          return DropdownMenuItem(
                            value: result,
                            child: Text(result),
                          );
                        }).toList(),
                        onChanged: (newValue) async {
                          setState(() {
                            _httpMethod = newValue;
                          });
                        },
                        isRequiredNotEmpty: true),
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 3,
                    child: MultiLanguageText(name: "path", defaultValue: "Path", style: Style(context).styleTitleDialog),
                  ),
                  Expanded(
                    flex: 7,
                    child: DynamicTextField(
                      controller: _pathController,
                      hintText: "...",
                      isRequiredNotEmpty: true,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 10),
              child: DynamicListApiWidget(
                  dynamicList: pathParameterList,
                  keyForm: _formKey,
                  type: multiLanguageString(name: "path_parameter_list", defaultValue: "Path parameter list", context: context)),
            ),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 10),
              child: Row(
                children: [
                  Expanded(
                    flex: 3,
                    child: MultiLanguageText(name: "request_type", defaultValue: "Request type", style: Style(context).styleTitleDialog),
                  ),
                  Expanded(
                    flex: 7,
                    child: DynamicDropdownButton(
                        borderRadius: 5,
                        value: _apiType,
                        hint: multiLanguageString(name: "choose_a_request_type", defaultValue: "Choose a request type", context: context),
                        items: ["none", "multipart-form", "rawContent"].map<DropdownMenuItem<String>>((String result) {
                          return DropdownMenuItem(
                            value: result,
                            child: Text(result),
                          );
                        }).toList(),
                        onChanged: (newValue) async {
                          setState(() {
                            _apiType = newValue;
                          });
                        },
                        isRequiredNotEmpty: true),
                  ),
                ],
              ),
            ),
            if (_apiType == "multipart-form")
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: MultiPartFormWidget(multiPartFormList: multiPartFormList, keyForm: _formKey),
              ),
            if (_apiType == "rawContent")
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "body_data", defaultValue: "Body data", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicTextField(
                        controller: _bodyDataController,
                        hintText: "...",
                        maxline: 15,
                        minline: 5,
                      ),
                    ),
                  ],
                ),
              ),
          ],
        ),
      ),
    );
  }

  SingleChildScrollView informationWidget(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(bottom: 10),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 3,
                            child: MultiLanguageText(name: "directory", defaultValue: "Directory", style: Style(context).styleTitleDialog),
                          ),
                          Expanded(
                            flex: 7,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                MultiLanguageText(
                                  name: "root",
                                  defaultValue: "Root",
                                  style: Style(context).styleTextDataCell,
                                ),
                                if (directoryPathList.isNotEmpty)
                                  Expanded(
                                    child: Row(
                                      children: [
                                        for (var directoryPath in directoryPathList)
                                          RichText(
                                            text: TextSpan(
                                              children: [
                                                const TextSpan(
                                                  text: " / ",
                                                  style: TextStyle(color: Colors.grey),
                                                ),
                                                TextSpan(
                                                  text: directoryPath != null ? directoryPath["title"] ?? "" : "null",
                                                  style: Style(context).styleTextDataCell,
                                                ),
                                              ],
                                            ),
                                          ),
                                      ],
                                    ),
                                  ),
                                FloatingActionButton.extended(
                                  heroTag: null,
                                  onPressed: () {
                                    showDialog<String>(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return DialogDirectory(
                                          type: testCaseType ?? widget.testCaseType,
                                          prjProjectId: prjProjectId,
                                          directoryPath: (directoryPathList.isNotEmpty) ? directoryPathList.last : null,
                                          callbackSelectedDirectory: (directoryId) {
                                            if (directoryId != "root") {
                                              getDirectoryPath(directoryId);
                                            } else {
                                              setState(() {
                                                directoryPathList = [];
                                              });
                                            }
                                          },
                                        );
                                      },
                                    );
                                  },
                                  icon: const Icon(Icons.folder, color: Colors.white),
                                  label: MultiLanguageText(
                                    name: "directory",
                                    defaultValue: "Directory",
                                    style:
                                        TextStyle(fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: Colors.white),
                                  ),
                                  backgroundColor: Colors.blue,
                                  elevation: 0,
                                  hoverElevation: 0,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 3,
                            child: MultiLanguageText(name: "type", defaultValue: "Type", style: Style(context).styleTitleDialog),
                          ),
                          Expanded(
                            flex: 7,
                            child: DynamicDropdownButton(
                                value: testCaseType,
                                hint: multiLanguageString(name: "choose_a_type", defaultValue: "Choose a type", context: context),
                                items: [
                                  {
                                    "name": multiLanguageString(name: "map_test_case", defaultValue: "Test case", context: context),
                                    "value": "test_case"
                                  },
                                  {
                                    "name": multiLanguageString(name: "map_test_scenario", defaultValue: "Test scenario", context: context),
                                    "value": "test_scenario"
                                  },
                                  {
                                    "name": multiLanguageString(name: "map_test_manual", defaultValue: "manual_test", context: context),
                                    "value": "manual_test"
                                  },
                                  {
                                    "name": multiLanguageString(name: "map_test_suite", defaultValue: "Test suite", context: context),
                                    "value": "test_suite"
                                  },
                                  {"name": multiLanguageString(name: "map_api", defaultValue: "test_api", context: context), "value": "test_api"},
                                  {
                                    "name": multiLanguageString(name: "map_shared_steps", defaultValue: "Shared steps", context: context),
                                    "value": "action"
                                  },
                                  {"name": multiLanguageString(name: "map_jdbc", defaultValue: "test_jdbc", context: context), "value": "test_jdbc"},
                                ].map<DropdownMenuItem<String>>((dynamic result) {
                                  return DropdownMenuItem(
                                    value: result["value"],
                                    child: Text(result["name"]),
                                  );
                                }).toList(),
                                onChanged: (newValue) {
                                  if (newValue == "test_case") {
                                    widget.callbackTestCaseType(newValue);
                                  } else {
                                    setState(() {
                                      testCaseType = newValue!;
                                      directoryPathList = [];
                                    });
                                    setDefaultTab();
                                  }
                                },
                                borderRadius: 5,
                                isRequiredNotEmpty: true),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 3,
                            child: MultiLanguageText(name: "version", defaultValue: "Version", style: Style(context).styleTitleDialog),
                          ),
                          Expanded(
                            flex: 7,
                            child: DynamicDropdownSearchClearOption(
                              hint: multiLanguageString(name: "choose_a_version", defaultValue: "Choose a version", context: context),
                              controller:
                                  SingleValueDropDownController(data: DropDownValueModel(name: _testsVersion["name"], value: _testsVersion["id"])),
                              items: _listVersions.map<DropDownValueModel>((dynamic result) {
                                return DropDownValueModel(
                                  value: result["id"],
                                  name: result["name"],
                                );
                              }).toList(),
                              maxHeight: 200,
                              onChanged: (dynamic newValue) {
                                _formKey.currentState!.validate();
                                setState(() {
                                  _testsVersion = _listVersions.firstWhere((element) => element["id"] == newValue.value);
                                });
                              },
                              isRequiredNotEmpty: true,
                              enabled: widget.type == "create" || widget.type == "duplicate",
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 3,
                            child: MultiLanguageText(name: "environment", defaultValue: "Environment", style: Style(context).styleTitleDialog),
                          ),
                          Expanded(
                            flex: 7,
                            child: DynamicDropdownSearchClearOption(
                              hint: multiLanguageString(name: "choose_an_environment", defaultValue: "Choose an environment", context: context),
                              controller: SingleValueDropDownController(
                                  data: DropDownValueModel(name: _testsEnvironment["name"], value: _testsEnvironment["id"])),
                              items: _listEnvironment.map<DropDownValueModel>((dynamic result) {
                                return DropDownValueModel(
                                  value: result["id"],
                                  name: result["name"],
                                );
                              }).toList(),
                              maxHeight: 200,
                              onChanged: (dynamic newValue) {
                                _formKey.currentState!.validate();
                                setState(() {
                                  _testsEnvironment = _listEnvironment.firstWhere((element) => element["id"] == newValue.value);
                                });
                              },
                              isRequiredNotEmpty: true,
                              enabled: widget.type == "create" || widget.type == "duplicate",
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 3,
                            child: MultiLanguageText(name: "module", defaultValue: "Module", style: Style(context).styleTitleDialog),
                          ),
                          Expanded(
                            flex: 7,
                            child: DynamicDropdownButton(
                              value: _testsComponent,
                              hint: multiLanguageString(name: "choose_a_module", defaultValue: "Choose a module", context: context),
                              items: _listComponents.map<DropdownMenuItem<String>>((dynamic result) {
                                return DropdownMenuItem(
                                  value: result["id"],
                                  child: Text(result["title"]),
                                );
                              }).toList(),
                              onChanged: (newValue) {
                                setState(() {
                                  _testsComponent = newValue!;
                                });
                              },
                              borderRadius: 5,
                              isRequiredNotEmpty: true,
                              enabled: widget.type == "create" || widget.type == "duplicate",
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                child: Column(
                  children: [
                    if (widget.type == "duplicate")
                      Container(
                        margin: const EdgeInsets.only(bottom: 10),
                        child: Row(
                          children: [
                            Expanded(
                              flex: 3,
                              child: MultiLanguageText(name: "project", defaultValue: "Project", style: Style(context).styleTitleDialog),
                            ),
                            Expanded(
                              flex: 7,
                              child: DynamicDropdownButton(
                                  borderRadius: 5,
                                  value: prjProjectId,
                                  hint: multiLanguageString(name: "choose_a_project", defaultValue: "Choose a project", context: context),
                                  items: _listProject.map<DropdownMenuItem<String>>((dynamic result) {
                                    return DropdownMenuItem(
                                      value: result["id"],
                                      child: Text(result["title"]),
                                    );
                                  }).toList(),
                                  onChanged: (newValue) async {
                                    setState(() {
                                      _formKey.currentState!.validate();
                                      prjProjectId = newValue!;
                                      _testsComponent = null;
                                      _testsVersion = null;
                                      _testsEnvironment = null;
                                    });
                                    await getComponentAndVersion();
                                  },
                                  isRequiredNotEmpty: true),
                            ),
                          ],
                        ),
                      ),
                    Container(
                      margin: EdgeInsets.only(top: (widget.type == "duplicate") ? 10 : 0, bottom: 10),
                      child: Row(
                        children: [
                          const Expanded(
                            flex: 3,
                            child: RowCodeWithTooltipWidget(),
                          ),
                          Expanded(
                            flex: 7,
                            child: DynamicTextField(
                              controller: _testsCodeController,
                              hintText: "...",
                              isRequiredNotEmpty: true,
                              isRequiredRegex: true,
                              enabled: widget.type != "edit",
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 3,
                            child: MultiLanguageText(name: "name", defaultValue: "Name", style: Style(context).styleTitleDialog),
                          ),
                          Expanded(
                            flex: 7,
                            child: DynamicTextField(
                              controller: _testsNameController,
                              hintText: "...",
                              isRequiredNotEmpty: true,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 3,
                            child: MultiLanguageText(name: "test_priority", defaultValue: "Test priority", style: Style(context).styleTitleDialog),
                          ),
                          Expanded(
                            flex: 7,
                            child: DynamicDropdownButton(
                                borderRadius: 5,
                                value: testPriority,
                                hint: multiLanguageString(name: "choose_a_priority", defaultValue: "Choose a priority", context: context),
                                items: [
                                  {"name": multiLanguageString(name: "item_high", defaultValue: "High", context: context), "value": 0},
                                  {"name": multiLanguageString(name: "item_medium", defaultValue: "Medium", context: context), "value": 1},
                                  {"name": multiLanguageString(name: "item_low", defaultValue: "Low", context: context), "value": 2}
                                ].map<DropdownMenuItem<int>>((dynamic result) {
                                  return DropdownMenuItem(
                                    value: result["value"],
                                    child: Text(result["name"]),
                                  );
                                }).toList(),
                                onChanged: (newValue) {
                                  setState(() {
                                    testPriority = newValue!;
                                  });
                                },
                                isRequiredNotEmpty: true),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 3,
                            child: MultiLanguageText(name: "status", defaultValue: "Status", style: Style(context).styleTitleDialog),
                          ),
                          Expanded(
                            flex: 7,
                            child: DynamicDropdownButton(
                                borderRadius: 5,
                                value: _status,
                                hint: multiLanguageString(name: "choose_a_status", defaultValue: "Choose a status", context: context),
                                items: [
                                  {"name": multiLanguageString(name: "draft", defaultValue: "Draft", context: context), "value": 0},
                                  {"name": multiLanguageString(name: "active", defaultValue: "Active", context: context), "value": 1},
                                  {"name": multiLanguageString(name: "review", defaultValue: "Review", context: context), "value": 2},
                                ].map<DropdownMenuItem<int>>((dynamic result) {
                                  return DropdownMenuItem(
                                    value: result["value"],
                                    child: Text(result["name"]),
                                  );
                                }).toList(),
                                onChanged: (newValue) {
                                  setState(() {
                                    _status = newValue!;
                                  });
                                },
                                isRequiredNotEmpty: true),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 3,
                            child:
                                MultiLanguageText(name: "workflow_status", defaultValue: "Workflow Status", style: Style(context).styleTitleDialog),
                          ),
                          Expanded(
                            flex: 7,
                            child: DynamicDropdownButton(
                                borderRadius: 5,
                                value: wfStatusId,
                                hint:
                                    multiLanguageString(name: "choose_a_workflow_status", defaultValue: "Choose a workflow status", context: context),
                                items: _listStatus.map<DropdownMenuItem<String>>((dynamic result) {
                                  return DropdownMenuItem(
                                    value: result["id"],
                                    child: Text(result["title"]),
                                  );
                                }).toList(),
                                onChanged: (newValue) async {
                                  setState(() {
                                    wfStatusId = newValue!;
                                  });
                                },
                                isRequiredNotEmpty: true),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MultiLanguageText(name: "purpose", defaultValue: "Purpose", style: Style(context).styleTitleDialog),
                DynamicTextField(
                  controller: _testsPurposeController,
                  hintText: "...",
                  maxline: 5,
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MultiLanguageText(name: "precondition", defaultValue: "Precondition", style: Style(context).styleTitleDialog),
                DynamicTextField(
                  controller: _testsPreconditionController,
                  hintText: "...",
                  maxline: 5,
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MultiLanguageText(name: "test_steps", defaultValue: "Test steps", style: Style(context).styleTitleDialog),
                DynamicTextField(
                  controller: _testsStepController,
                  hintText: "...",
                  maxline: 5,
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MultiLanguageText(name: "expected_result", defaultValue: "Expected Result", style: Style(context).styleTitleDialog),
                DynamicTextField(
                  controller: _testsExpectedResultController,
                  hintText: "...",
                  maxline: 5,
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MultiLanguageText(name: "test_data", defaultValue: "Test data", style: Style(context).styleTitleDialog),
                DynamicTextField(
                  controller: _testsTestDataController,
                  hintText: "...",
                  maxline: 5,
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MultiLanguageText(name: "description", defaultValue: "Description", style: Style(context).styleTitleDialog),
                DynamicTextField(
                  controller: _testsDesController,
                  hintText: "...",
                  maxline: 3,
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    MultiLanguageText(name: "label", defaultValue: "Label", style: Style(context).styleTitleDialog),
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(
                          createRoute(
                            ManageLabelWidget(
                              labelList: labelsList,
                              onSave: (labelsList) {
                                setState(() {
                                  this.labelsList = labelsList;
                                });
                              },
                            ),
                          ),
                        );
                      },
                      child: MultiLanguageText(
                        name: "click_to_manage_label",
                        defaultValue: "Click To Manage Label",
                        style: Style(context).styleTitleDialog,
                      ),
                    )
                  ],
                ),
                const SizedBox(
                  height: 5,
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                      createRoute(
                        ManageLabelWidget(
                          labelList: labelsList,
                          onSave: (labelsList) {
                            setState(() {
                              this.labelsList = labelsList;
                            });
                          },
                        ),
                      ),
                    );
                  },
                  child: Container(
                    alignment: Alignment.centerLeft,
                    constraints: const BoxConstraints(minHeight: 50),
                    padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: const Color.fromRGBO(216, 218, 229, 1),
                      ),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Wrap(
                      children: [
                        for (var label in labelsList)
                          Container(
                            margin: EdgeInsets.only(right: labelsList.last != label ? 10 : 0, bottom: 5, top: 5),
                            child: IntrinsicWidth(
                              child: LabelListWidget(
                                key: UniqueKey(),
                                labelId: label,
                                onRemove: () {
                                  setState(() {
                                    labelsList.remove(label);
                                  });
                                },
                              ),
                            ),
                          )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Container tabControlWidget(TestCaseConfigModel testCaseConfigModel) {
    List<String> listTabItem = [
      multiLanguageString(name: "general_information", defaultValue: "General information", context: context),
      if (testCaseType == "test_jdbc") "test_jdbc",
      if (testCaseType != "test_api" && testCaseType != "manual_test" && testCaseType != "test_jdbc")
        multiLanguageString(name: "clients", defaultValue: "Clients", context: context),
      if (testCaseType != "test_jdbc") multiLanguageString(name: "parameters", defaultValue: "Parameters", context: context),
      if (testCaseType != "test_api" && testCaseType != "manual_test" && testCaseType != "test_jdbc")
        multiLanguageString(name: "output", defaultValue: "Output", context: context),
      if (testCaseType == "test_api") multiLanguageString(name: "header", defaultValue: "Header", context: context),
      if (testCaseType == "test_api") multiLanguageString(name: "request", defaultValue: "Request", context: context),
      if (testCaseType == "test_api") multiLanguageString(name: "response_body", defaultValue: "Response body", context: context),
    ];
    return Container(
      margin: const EdgeInsets.fromLTRB(50, 0, 50, 0),
      child: Row(
        children: [
          IconButton(
            onPressed: () {
              scrollController.animateTo(scrollController.position.pixels - 400,
                  duration: const Duration(milliseconds: 500), curve: Curves.easeInOut);
            },
            icon: const Icon(Icons.arrow_back_ios),
          ),
          Expanded(
            child: Scrollbar(
              controller: scrollController,
              thickness: 0,
              child: SingleChildScrollView(
                controller: scrollController,
                scrollDirection: Axis.horizontal,
                primary: false,
                child: TabBar(
                  controller: tabController,
                  isScrollable: true,
                  physics: const BouncingScrollPhysics(),
                  labelColor: const Color(0xff3d5afe),
                  indicatorColor: const Color(0xff3d5afe),
                  unselectedLabelColor: const Color.fromRGBO(0, 0, 0, 0.8),
                  labelStyle: Style(context).styleTitleDialog,
                  tabs: [
                    for (var tab in listTabItem)
                      Tab(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
                          child: GestureDetector(
                            onTap: () {
                              testCaseConfigModel.setInitialIndex(listTabItem.indexOf(tab));
                              tabController.animateTo(listTabItem.indexOf(tab));
                            },
                            child: Text(tab),
                          ),
                        ),
                      ),
                  ],
                ),
              ),
            ),
          ),
          IconButton(
            onPressed: () {
              scrollController.animateTo(scrollController.position.pixels + 400,
                  duration: const Duration(milliseconds: 500), curve: Curves.easeInOut);
            },
            icon: const Icon(Icons.arrow_forward_ios),
          ),
        ],
      ),
    );
  }

  Padding headerWidget(TestCaseConfigModel testCaseConfigModel, BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 45, 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text("${widget.title.toUpperCase()} ${widget.testCaseType.toUpperCase()}", style: Style(context).styleTitleDialog),
          Padding(
            padding: const EdgeInsets.only(right: 12),
            child: TextButton(
              onPressed: () {
                testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["parameterFieldList"]
                    .removeWhere((element) => element["name"] == null || element["name"] == "");
                outputFieldList.removeWhere((element) => element["name"] == null || element["name"] == "");
                testCaseConfigModel.testCaseConfigMap[testCaseConfigModel.testCaseIdList.last]["clientList"].removeWhere(
                    (element) => element["name"] == null || element["name"] == "" || element["platform"] == null || element["platform"] == "");
                Navigator.pop(context, 'cancel');
              },
              child: const Icon(
                Icons.close,
                color: Colors.black,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
