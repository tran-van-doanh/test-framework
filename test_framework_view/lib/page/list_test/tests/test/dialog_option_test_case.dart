import 'dart:convert';
import 'dart:io';

import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/file_download.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/common/row_code_with_tooltip.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/dynamic_play_form.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/config.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';
import 'package:test_framework_view/page/list_test/tests/directory/dialog_directory.dart';
import 'package:test_framework_view/page/list_test/tests/label/label_list_widget.dart';
import 'package:test_framework_view/page/list_test/tests/label/manage_label_widget.dart';
import 'package:test_framework_view/components/dialog/dialog_choose_test.dart';
import 'package:test_framework_view/type.dart';
import '../../../../common/dynamic_dropdown_button.dart';
import '../../../../common/custom_state.dart';
import 'package:http/http.dart' as http;

class DialogOptionTestCaseWidget extends StatefulWidget {
  final String testCaseType;
  final String type;
  final String title;
  final dynamic selectedItem;
  final Function? callbackOptionTest;
  final String? parentId;
  final Function callbackTestCaseType;
  final bool isEmptyDirectoryPathList;
  const DialogOptionTestCaseWidget({
    Key? key,
    required this.type,
    this.selectedItem,
    this.callbackOptionTest,
    required this.testCaseType,
    this.parentId,
    required this.callbackTestCaseType,
    required this.isEmptyDirectoryPathList,
    required this.title,
  }) : super(key: key);

  @override
  State<DialogOptionTestCaseWidget> createState() => _DialogOptionTestCaseWidgetState();
}

class _DialogOptionTestCaseWidgetState extends CustomState<DialogOptionTestCaseWidget> with TickerProviderStateMixin {
  late Future futureOptionTest;
  final TextEditingController _testsCodeController = TextEditingController();
  final TextEditingController testRunExpectedErrorMessageController = TextEditingController();
  final TextEditingController _testsDesController = TextEditingController();
  final TextEditingController _testsNameController = TextEditingController();
  final TextEditingController _testsPurposeController = TextEditingController();
  final TextEditingController _testsPreconditionController = TextEditingController();
  final TextEditingController _testsStepController = TextEditingController();
  final TextEditingController _testsExpectedResultController = TextEditingController();
  final TextEditingController _testsTestDataController = TextEditingController();
  List labelsList = [];
  List resultClientList = [];
  late TabController tabController;
  int requirementRisk = 1;
  String testRunType = "parallel";
  String? parentTestCaseId;
  String severity = "3";
  String priority = "3";
  int testRunExpectedResult = 1;
  int? _status = 0;
  int? testPriority = 0;
  String? wfStatusId;
  String? _testsComponent;
  dynamic _testsVersion;
  dynamic _testsEnvironment;
  var _listComponents = [];
  var _listStatus = [];
  var _listProject = [];
  var _listVersions = [];
  var _listEnvironment = [];
  final _formKey = GlobalKey<FormState>();
  var prjProjectId = "";
  TestCaseConfigModel? testCaseConfigModel;
  List directoryPathList = [];
  ScrollController scrollController = ScrollController();
  var testCaseSelected = {};
  bool isViewColumn = false;
  List resultparameterFieldList = [];
  List<Map<dynamic, dynamic>> listValueParameter = [];
  PlatformFile? objFile;

  @override
  void dispose() {
    _testsCodeController.dispose();
    testRunExpectedErrorMessageController.dispose();
    _testsDesController.dispose();
    _testsNameController.dispose();
    _testsPurposeController.dispose();
    _testsPreconditionController.dispose();
    _testsStepController.dispose();
    _testsExpectedResultController.dispose();
    _testsTestDataController.dispose();
    tabController.dispose();
    scrollController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    prjProjectId = Provider.of<NavigationModel>(context, listen: false).prjProjectId ?? "";
    testCaseConfigModel = Provider.of<TestCaseConfigModel>(context, listen: false);
    tabController = TabController(initialIndex: 0, length: 2, vsync: this);
    futureOptionTest = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    if (widget.type == "duplicate") {
      getListProject();
    }
    getListStatus();
    if (!widget.isEmptyDirectoryPathList) {
      if (widget.selectedItem != null && widget.selectedItem["tfTestDirectoryId"] != null) {
        getDirectoryPath(widget.selectedItem["tfTestDirectoryId"]);
      } else if (widget.parentId != null) {
        getDirectoryPath(widget.parentId!);
      }
    }
    await getComponentAndVersion();
    await setInitValue();
    return 0;
  }

  getDirectoryPath(String id) async {
    var directoryParentResponse = await httpGet("/test-framework-api/user/test-framework/tf-test-directory/$id/path", context);
    if (directoryParentResponse.containsKey("body") && directoryParentResponse["body"] is String == false) {
      setState(() {
        directoryPathList = directoryParentResponse["body"]["resultList"];
      });
    }
  }

  getListProject() async {
    var response = await httpPost("/test-framework-api/user/project/prj-project/search", {"status": 1}, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listProject = response["body"]["resultList"];
      });
    }
  }

  setInitValue() async {
    setState(() {
      if (widget.type != "create") {
        if (widget.type == "duplicate") {
          _testsCodeController.text = "copy_of_${widget.selectedItem["name"]}";
          _testsNameController.text = "copy_of_${widget.selectedItem["title"]}";
        } else {
          _testsCodeController.text = widget.selectedItem["name"] ?? "";
          _testsNameController.text = widget.selectedItem["title"] ?? "";
        }
        _status = widget.selectedItem["status"] ?? 0;
        testPriority = widget.selectedItem["testPriority"] ?? 0;
        parentTestCaseId = widget.selectedItem["parentId"];
        if (widget.selectedItem["content"] != null) {
          severity = widget.selectedItem["content"]["severity"] ?? "3";
          priority = widget.selectedItem["content"]["priority"] ?? "3";
          testRunExpectedResult = widget.selectedItem["content"]["testRunExpectedResult"] ?? 1;
          testRunExpectedErrorMessageController.text = widget.selectedItem["content"]["testRunExpectedErrorMessage"] ?? "";
          testRunType = widget.selectedItem["content"]["testRunType"] ?? "parallel";
          requirementRisk = widget.selectedItem["content"]["requirementRisk"] ?? 1;
          listValueParameter = <Map<dynamic, dynamic>>[...widget.selectedItem["content"]["testRunData"] ?? []];
        }
        wfStatusId = widget.selectedItem["wfStatusId"] ?? (_listStatus.isNotEmpty ? _listStatus[0]["id"] : null);
        _testsComponent = widget.selectedItem["tfTestComponentId"] ?? (_listComponents.isNotEmpty ? _listComponents[0]["id"] : null);
        _testsVersion = widget.selectedItem["tfTestVersion"] ?? (_listVersions.isNotEmpty ? _listVersions[0] : null);
        _testsEnvironment = widget.selectedItem["tfTestEnvironment"] ?? (_listEnvironment.isNotEmpty ? _listEnvironment[0] : null);
        _testsDesController.text = widget.selectedItem["description"] ?? "";
        _testsPurposeController.text = widget.selectedItem["purpose"] ?? "";
        _testsPreconditionController.text = widget.selectedItem["preconditions"] ?? "";
        _testsTestDataController.text = widget.selectedItem["inputs"] ?? "";
        _testsStepController.text = widget.selectedItem["steps"] ?? "";
        _testsExpectedResultController.text = widget.selectedItem["expectedResults"] ?? "";
        labelsList = widget.selectedItem["labelsList"] ?? [];
      } else {
        wfStatusId = _listStatus.isNotEmpty ? _listStatus[0]["id"] : null;
        _testsComponent = _listComponents.isNotEmpty ? _listComponents[0]["id"] : null;
        if (_listVersions.isNotEmpty) {
          var versionItem = _listVersions.firstWhere(
            (element) => element["defaultVersion"] == 1,
            orElse: () => null,
          );
          _testsVersion = versionItem ?? _listVersions[0];
        }
        if (_listEnvironment.isNotEmpty) {
          var environmentItem = _listEnvironment.firstWhere(
            (element) => element["defaultEnvironment"] == 1,
            orElse: () => null,
          );
          _testsEnvironment = environmentItem ?? _listEnvironment[0];
        }
      }
    });
    if (parentTestCaseId != null) {
      await getTestCase(parentTestCaseId!);
    } else {
      listValueParameter = [
        {"severity": "3", "priority": "3"}
      ];
    }
  }

  Future getTestCase(String testCaseId) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-case/$testCaseId", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        resultClientList = response["body"]["result"]["content"]["clientList"] ?? [];
        testCaseSelected = response["body"]["result"];
        resultparameterFieldList = response["body"]["result"]["content"]["parameterFieldList"] ?? [];
      });
    }
  }

  getComponentAndVersion() async {
    await getComponentList();
    await getVersionList();
    await getEnvironmentList();
  }

  getListStatus() async {
    var response = await httpPost("/test-framework-api/user/workflow/wf-status/search", {"status": 1, "flowTypes": "test_case"}, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listStatus = response["body"]["resultList"];
      });
    }
  }

  getComponentList() async {
    var findRequest = {"prjProjectId": prjProjectId, "status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-component/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listComponents = response["body"]["resultList"];
      });
    }
  }

  getVersionList() async {
    var findRequest = {"prjProjectId": prjProjectId, "status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-version/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listVersions = response["body"]["resultList"];
      });
    }
  }

  getEnvironmentList() async {
    var findRequest = {"prjProjectId": prjProjectId, "status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-environment/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listEnvironment = response["body"]["resultList"];
      });
    }
  }

  downloadFile(id) async {
    File? downloadedFile = await httpDownloadFile(context, "/test-framework-api/user/test-framework-file/tf-test-case/$id/generate-file",
        Provider.of<SecurityModel>(context, listen: false).authorization, 'Output.xlsx');
    if (downloadedFile != null && mounted) {
      var snackBar = SnackBar(
        content: MultiLanguageText(name: "file_downloaded_to", defaultValue: "File downloaded to \$0 .", variables: ["$downloadedFile"]),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  uploadFile() async {
    final request = http.MultipartRequest(
      "POST",
      Uri.parse(
        "$baseUrl/test-framework-api/user/test-framework-file/read-file",
      ),
    );
    request.headers.addAll({"Authorization": Provider.of<SecurityModel>(context, listen: false).authorization!});
    request.files.add(http.MultipartFile.fromBytes('file', await readFile(objFile!), filename: objFile!.name));
    var resp = await request.send();
    var responseDecode = jsonDecode(await resp.stream.bytesToString());
    if (responseDecode.containsKey("errorMessage") && mounted) {
      showToast(context: context, msg: responseDecode["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
    } else if (mounted) {
      showToast(
          context: context,
          msg: multiLanguageString(name: "upload_success", defaultValue: "Upload success", context: context),
          color: Colors.greenAccent,
          icon: const Icon(Icons.done));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<TestCaseConfigModel, LanguageModel>(
      builder: (context, testCaseConfigModel, languageModel, child) {
        return FutureBuilder(
          future: futureOptionTest,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return TestFrameworkRootPageWidget(
                child: Container(
                  color: const Color.fromARGB(255, 250, 250, 250),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      headerWidget(testCaseConfigModel, context),
                      Expanded(
                        child: DefaultTabController(
                          length: 2,
                          initialIndex: 0,
                          child: Form(
                            key: _formKey,
                            child: FocusTraversalGroup(
                              policy: OrderedTraversalPolicy(),
                              child: Column(
                                children: [
                                  tabControlWidget(testCaseConfigModel),
                                  const Divider(color: Color.fromRGBO(216, 218, 229, 1)),
                                  Expanded(
                                    child: Container(
                                      margin: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                                      child: TabBarView(
                                        controller: tabController,
                                        children: [
                                          informationWidget(context),
                                          configWidget(context),
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      btnWidget(testCaseConfigModel, context)
                    ],
                  ),
                ),
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  Padding btnWidget(TestCaseConfigModel testCaseConfigModel, BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20, top: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ButtonSave(
            onPressed: () {
              if (_formKey.currentState!.validate() && parentTestCaseId != null) {
                for (var valueParameter in listValueParameter) {
                  for (var parameterField in resultparameterFieldList) {
                    if (valueParameter[parameterField["name"]] == null) {
                      valueParameter[parameterField["name"]] = "";
                    }
                  }
                }
                var clientListBodyRequest = [];
                for (var element in resultClientList) {
                  var clientNew = Map.from(element);
                  clientNew.remove("profileList");
                  clientListBodyRequest.add(clientNew);
                }
                var result = {};
                if (widget.selectedItem != null && widget.selectedItem.isNotEmpty && widget.type != "duplicate") {
                  result = widget.selectedItem;
                  result["testCaseType"] = "test_case";
                  result["parentId"] = parentTestCaseId;
                  result["title"] = _testsNameController.text;
                  result["name"] = _testsCodeController.text;
                  result["description"] = _testsDesController.text;
                  result["tfTestVersionId"] = _testsVersion["id"];
                  result["tfTestEnvironmentId"] = _testsEnvironment["id"];
                  result["tfTestComponentId"] = _testsComponent;
                  result["prjProjectId"] = prjProjectId;
                  result["purpose"] = _testsPurposeController.text;
                  result["preconditions"] = _testsPreconditionController.text;
                  result["inputs"] = _testsTestDataController.text;
                  result["steps"] = _testsStepController.text;
                  result["expectedResults"] = _testsExpectedResultController.text;
                  result["labelsList"] = labelsList;
                  result["content"] ??= {};
                  result["content"]["name"] = _testsNameController.text;
                  result["content"]["testRunExpectedErrorMessage"] = testRunExpectedErrorMessageController.text;
                  result["content"]["severity"] = severity;
                  result["content"]["priority"] = priority;
                  result["content"]["testRunExpectedResult"] = testRunExpectedResult;
                  result["content"]["testRunData"] = listValueParameter;
                  result["content"]["testRunType"] = testRunType;
                  result["content"]["requirementRisk"] = requirementRisk;
                  if (directoryPathList.isNotEmpty) {
                    result["tfTestDirectoryId"] = directoryPathList.last["id"];
                  } else if (result.containsKey("tfTestDirectoryId")) {
                    result.remove("tfTestDirectoryId");
                  }
                  result["status"] = _status;
                  result["testPriority"] = testPriority;
                  result["wfStatusId"] = wfStatusId;
                } else {
                  result = {
                    "testCaseType": "test_case",
                    "parentId": parentTestCaseId,
                    "title": _testsNameController.text,
                    "name": _testsCodeController.text,
                    "description": _testsDesController.text,
                    "tfTestVersionId": _testsVersion["id"],
                    "tfTestEnvironmentId": _testsEnvironment["id"],
                    "tfTestComponentId": _testsComponent,
                    "content": {
                      "name": _testsNameController.text,
                      "testRunExpectedErrorMessage": testRunExpectedErrorMessageController.text,
                      "severity": severity,
                      "priority": priority,
                      "testRunExpectedResult": testRunExpectedResult,
                      "testRunData": listValueParameter,
                      "testRunType": testRunType,
                      "requirementRisk": requirementRisk,
                    },
                    "status": _status,
                    "testPriority": testPriority,
                    "wfStatusId": wfStatusId,
                    "prjProjectId": prjProjectId,
                    "purpose": _testsPurposeController.text,
                    "preconditions": _testsPreconditionController.text,
                    "inputs": _testsTestDataController.text,
                    "steps": _testsStepController.text,
                    "expectedResults": _testsExpectedResultController.text,
                    if (directoryPathList.isNotEmpty) "tfTestDirectoryId": directoryPathList.last["id"],
                    "labelsList": labelsList
                  };
                }
                widget.callbackOptionTest!(result);
              } else if (parentTestCaseId == null) {
                showToast(
                    context: context,
                    msg: multiLanguageString(name: "please_choose_a_test", defaultValue: "Please choose a test", context: context),
                    color: Colors.red,
                    icon: const Icon(Icons.error));
              }
            },
          ),
          Container(
            margin: const EdgeInsets.only(left: 5),
            height: 40,
            width: 120,
            child: ElevatedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              style: ElevatedButton.styleFrom(
                side: const BorderSide(color: Color.fromRGBO(209, 212, 216, 1)),
                backgroundColor: Colors.white,
                foregroundColor: const Color.fromRGBO(61, 90, 254, 1),
              ),
              child: const MultiLanguageText(
                name: "cancel",
                defaultValue: "Cancel",
                isLowerCase: false,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          )
        ],
      ),
    );
  }

  SingleChildScrollView configWidget(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            margin: const EdgeInsets.only(bottom: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 3,
                  child: MultiLanguageText(name: "test_run_type", defaultValue: "Test Run Type", style: Style(context).styleTitleDialog),
                ),
                Expanded(
                  flex: 7,
                  child: DynamicDropdownButton(
                    value: testRunType,
                    hint: multiLanguageString(name: "choose_a_test_run_type", defaultValue: "Choose a test run type", context: context),
                    items: [
                      {"name": multiLanguageString(name: "sequence", defaultValue: "Sequence", context: context), "value": "sequence"},
                      {"name": multiLanguageString(name: "parallel", defaultValue: "Parallel", context: context), "value": "parallel"}
                    ].map<DropdownMenuItem<String>>((dynamic result) {
                      return DropdownMenuItem(
                        value: result["value"],
                        child: Text(result["name"]),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        _formKey.currentState!.validate();
                        testRunType = newValue!;
                      });
                    },
                    borderRadius: 5,
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 3,
                  child: MultiLanguageText(name: "fail_severity", defaultValue: "Fail severity", style: Style(context).styleTitleDialog),
                ),
                Expanded(
                  flex: 7,
                  child: DynamicDropdownButton(
                      borderRadius: 5,
                      value: severity,
                      hint: multiLanguageString(name: "choose_a_severity", defaultValue: "Choose a severity", context: context),
                      items: [
                        {"value": "1", "name": multiLanguageString(name: "item_critical", defaultValue: "Critical", context: context)},
                        {"value": "2", "name": multiLanguageString(name: "item_major", defaultValue: "Major", context: context)},
                        {"value": "3", "name": multiLanguageString(name: "item_average", defaultValue: "Average", context: context)},
                        {"value": "4", "name": multiLanguageString(name: "item_minor", defaultValue: "Minor", context: context)},
                        {"value": "5", "name": multiLanguageString(name: "item_trivial", defaultValue: "Trivial", context: context)},
                      ].map<DropdownMenuItem<String>>((dynamic result) {
                        return DropdownMenuItem(
                          value: result["value"],
                          child: Text(result["name"]),
                        );
                      }).toList(),
                      onChanged: (newValue) {
                        setState(() {
                          severity = newValue!;
                        });
                      },
                      isRequiredNotEmpty: true),
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 3,
                  child: MultiLanguageText(name: "fail_priority", defaultValue: "Fail priority", style: Style(context).styleTitleDialog),
                ),
                Expanded(
                  flex: 7,
                  child: DynamicDropdownButton(
                      borderRadius: 5,
                      value: priority,
                      hint: multiLanguageString(name: "choose_a_priority", defaultValue: "Choose a priority", context: context),
                      items: [
                        {"value": "1", "name": multiLanguageString(name: "item_highest", defaultValue: "Highest", context: context)},
                        {"value": "2", "name": multiLanguageString(name: "item_high", defaultValue: "High", context: context)},
                        {"value": "3", "name": multiLanguageString(name: "item_medium", defaultValue: "Medium", context: context)},
                        {"value": "4", "name": multiLanguageString(name: "item_low", defaultValue: "Low", context: context)},
                        {"value": "5", "name": multiLanguageString(name: "item_lowest", defaultValue: "Lowest", context: context)},
                      ].map<DropdownMenuItem<String>>((dynamic result) {
                        return DropdownMenuItem(
                          value: result["value"],
                          child: Text(result["name"]),
                        );
                      }).toList(),
                      onChanged: (newValue) {
                        setState(() {
                          priority = newValue!;
                        });
                      },
                      isRequiredNotEmpty: true),
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 3,
                  child: MultiLanguageText(
                      name: "test_run_expected_result", defaultValue: "Test Run Expected Result", style: Style(context).styleTitleDialog),
                ),
                Expanded(
                  flex: 7,
                  child: DynamicDropdownButton(
                      borderRadius: 5,
                      value: testRunExpectedResult,
                      hint: multiLanguageString(
                          name: "choose_a_test_run_expected_result", defaultValue: "Choose a Test Run Expected Result", context: context),
                      items: [
                        {"name": multiLanguageString(name: "item_pass", defaultValue: "Pass", context: context), "value": 1},
                        {"name": multiLanguageString(name: "item_fail", defaultValue: "Fail", context: context), "value": 0},
                      ].map<DropdownMenuItem<int>>((dynamic result) {
                        return DropdownMenuItem(
                          value: result["value"],
                          child: Text(result["name"]),
                        );
                      }).toList(),
                      onChanged: (newValue) {
                        setState(() {
                          testRunExpectedResult = newValue!;
                        });
                      },
                      isRequiredNotEmpty: true),
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 3,
                  child: MultiLanguageText(name: "requirements_risk", defaultValue: "Requirements risk", style: Style(context).styleTitleDialog),
                ),
                Expanded(
                  flex: 7,
                  child: DynamicDropdownButton(
                      borderRadius: 5,
                      value: requirementRisk,
                      hint: multiLanguageString(
                          name: "choose_a_test_run_expected_result", defaultValue: "Choose a Test Run Expected Result", context: context),
                      items: [
                        {"name": multiLanguageString(name: "low", defaultValue: "Low", context: context), "value": 1},
                        {"name": multiLanguageString(name: "medium", defaultValue: "Medium", context: context), "value": 2},
                        {"name": multiLanguageString(name: "high", defaultValue: "High", context: context), "value": 3}
                      ].map<DropdownMenuItem<int>>((dynamic result) {
                        return DropdownMenuItem(
                          value: result["value"],
                          child: Text(result["name"]),
                        );
                      }).toList(),
                      onChanged: (newValue) {
                        setState(() {
                          requirementRisk = newValue!;
                        });
                      },
                      isRequiredNotEmpty: true),
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 3,
                  child: MultiLanguageText(
                      name: "expected_error_message", defaultValue: "Test Run Expected Error Message", style: Style(context).styleTitleDialog),
                ),
                Expanded(
                  flex: 7,
                  child: DynamicTextField(
                    controller: testRunExpectedErrorMessageController,
                    hintText: "...",
                    maxline: 5,
                  ),
                ),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  SizedBox(
                    height: 45,
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          createRoute(
                            DialogChooseTest(
                              prjProjectId: prjProjectId,
                              testCaseType: "test_scenario",
                              callbackSelectedTestCase: (testCaseId) async {
                                setState(() {
                                  parentTestCaseId = testCaseId;
                                });
                                await getTestCase(parentTestCaseId!);
                              },
                            ),
                          ),
                        );
                      },
                      child: const MultiLanguageText(
                        name: "select_test_scenario",
                        defaultValue: "Select test scenario",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  SizedBox(
                    height: 45,
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          createRoute(
                            DialogChooseTest(
                              prjProjectId: prjProjectId,
                              testCaseType: "test_suite",
                              callbackSelectedTestCase: (testCaseId) async {
                                setState(() {
                                  parentTestCaseId = testCaseId;
                                });
                                await getTestCase(parentTestCaseId!);
                              },
                            ),
                          ),
                        );
                      },
                      child: const MultiLanguageText(
                        name: "select_test_suite",
                        defaultValue: "Select test suite",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
              if (parentTestCaseId != null)
                Row(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(right: 10),
                      child: SizedBox(
                        height: 40,
                        child: FloatingActionButton.extended(
                          heroTag: null,
                          onPressed: () async {
                            downloadFile(parentTestCaseId);
                          },
                          icon: const FaIcon(FontAwesomeIcons.fileArrowDown, color: Color.fromRGBO(49, 49, 49, 1)),
                          label: MultiLanguageText(
                            name: "download_excel",
                            defaultValue: "Download Excel",
                            style: TextStyle(
                                fontSize: context.fontSizeManager.fontSizeText13,
                                fontWeight: FontWeight.w600,
                                color: const Color.fromRGBO(49, 49, 49, 1)),
                          ),
                          backgroundColor: const Color.fromRGBO(216, 218, 229, .7),
                          elevation: 0,
                          hoverElevation: 0,
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(right: 10),
                      child: SizedBox(
                        height: 40,
                        child: FloatingActionButton.extended(
                          heroTag: null,
                          onPressed: () async {
                            objFile = await selectFile(['xlsx']);
                            if (objFile != null) {
                              await uploadFile();
                            }
                          },
                          icon: const FaIcon(FontAwesomeIcons.fileArrowUp, color: Color.fromRGBO(49, 49, 49, 1)),
                          label: MultiLanguageText(
                            name: "upload_excel",
                            defaultValue: "Upload Excel",
                            style: TextStyle(
                                fontSize: context.fontSizeManager.fontSizeText13,
                                fontWeight: FontWeight.w600,
                                color: const Color.fromRGBO(49, 49, 49, 1)),
                          ),
                          backgroundColor: const Color.fromRGBO(216, 218, 229, .7),
                          elevation: 0,
                          hoverElevation: 0,
                        ),
                      ),
                    ),
                    isViewColumn
                        ? SizedBox(
                            height: 40,
                            child: FloatingActionButton(
                              heroTag: null,
                              onPressed: () async {
                                setState(() {
                                  isViewColumn = false;
                                });
                              },
                              backgroundColor: const Color.fromARGB(176, 125, 141, 231),
                              elevation: 0,
                              hoverElevation: 0,
                              child: const Icon(Icons.view_column, color: Color.fromRGBO(49, 49, 49, 1)),
                            ),
                          )
                        : SizedBox(
                            height: 40,
                            child: FloatingActionButton(
                              heroTag: null,
                              onPressed: () async {
                                setState(() {
                                  isViewColumn = true;
                                });
                              },
                              backgroundColor: const Color.fromARGB(176, 125, 141, 231),
                              elevation: 0,
                              hoverElevation: 0,
                              child: const Icon(Icons.view_stream, color: Color.fromRGBO(49, 49, 49, 1)),
                            ),
                          ),
                  ],
                ),
            ],
          ),
          if (testCaseSelected.isNotEmpty)
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: MultiLanguageText(
                name: "test_test_case_selected",
                defaultValue: "Test : \$0",
                variables: ["${testCaseSelected["title"]}"],
                style: TextStyle(fontSize: context.fontSizeManager.fontSizeText22, fontWeight: FontWeight.w500),
              ),
            ),
          if (testCaseSelected.isNotEmpty)
            RunBodyParameterWidget(
              isViewColumn: isViewColumn,
              listValueParameter: listValueParameter,
              resultparameterFieldList: resultparameterFieldList,
              key: UniqueKey(),
              type: "test_case",
            ),
        ],
      ),
    );
  }

  SingleChildScrollView informationWidget(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(bottom: 10),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 3,
                            child: MultiLanguageText(name: "directory", defaultValue: "Directory", style: Style(context).styleTitleDialog),
                          ),
                          Expanded(
                            flex: 7,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                MultiLanguageText(
                                  name: "root",
                                  defaultValue: "Root",
                                  style: Style(context).styleTextDataCell,
                                ),
                                if (directoryPathList.isNotEmpty)
                                  Expanded(
                                    child: Row(
                                      children: [
                                        for (var directoryPath in directoryPathList)
                                          RichText(
                                            text: TextSpan(
                                              children: [
                                                const TextSpan(
                                                  text: " / ",
                                                  style: TextStyle(color: Colors.grey),
                                                ),
                                                TextSpan(
                                                  text: directoryPath != null ? directoryPath["title"] ?? "" : "null",
                                                  style: Style(context).styleTextDataCell,
                                                ),
                                              ],
                                            ),
                                          ),
                                      ],
                                    ),
                                  ),
                                FloatingActionButton.extended(
                                  heroTag: null,
                                  onPressed: () {
                                    showDialog<String>(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return DialogDirectory(
                                          type: widget.testCaseType,
                                          prjProjectId: prjProjectId,
                                          directoryPath: (directoryPathList.isNotEmpty) ? directoryPathList.last : null,
                                          callbackSelectedDirectory: (directoryId) {
                                            if (directoryId != "root") {
                                              getDirectoryPath(directoryId);
                                            } else {
                                              setState(() {
                                                directoryPathList = [];
                                              });
                                            }
                                          },
                                        );
                                      },
                                    );
                                  },
                                  icon: const Icon(Icons.folder, color: Colors.white),
                                  label: MultiLanguageText(
                                    name: "directory",
                                    defaultValue: "Directory",
                                    style:
                                        TextStyle(fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: Colors.white),
                                  ),
                                  backgroundColor: Colors.blue,
                                  elevation: 0,
                                  hoverElevation: 0,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 3,
                            child: MultiLanguageText(name: "type", defaultValue: "Type", style: Style(context).styleTitleDialog),
                          ),
                          Expanded(
                            flex: 7,
                            child: DynamicDropdownButton(
                                value: widget.testCaseType,
                                hint: multiLanguageString(name: "choose_a_type", defaultValue: "Choose a type", context: context),
                                items: [
                                  {
                                    "name": multiLanguageString(name: "map_test_case", defaultValue: "Test case", context: context),
                                    "value": "test_case"
                                  },
                                  {
                                    "name": multiLanguageString(name: "map_test_scenario", defaultValue: "Test scenario", context: context),
                                    "value": "test_scenario"
                                  },
                                  {
                                    "name": multiLanguageString(name: "map_test_manual", defaultValue: "Test manual", context: context),
                                    "value": "manual_test"
                                  },
                                  {
                                    "name": multiLanguageString(name: "map_test_suite", defaultValue: "Test suite", context: context),
                                    "value": "test_suite"
                                  },
                                  {"name": multiLanguageString(name: "map_api", defaultValue: "Api", context: context), "value": "test_api"},
                                  {
                                    "name": multiLanguageString(name: "map_shared_steps", defaultValue: "Shared steps", context: context),
                                    "value": "action"
                                  },
                                  {"name": multiLanguageString(name: "map_jdbc", defaultValue: "JDBC", context: context), "value": "test_jdbc"},
                                ].map<DropdownMenuItem<String>>((dynamic result) {
                                  return DropdownMenuItem(
                                    value: result["value"],
                                    child: Text(result["name"]),
                                  );
                                }).toList(),
                                onChanged: (newValue) {
                                  if (newValue != "test_case") {
                                    widget.callbackTestCaseType(newValue);
                                  }
                                },
                                borderRadius: 5,
                                isRequiredNotEmpty: true),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 3,
                            child: MultiLanguageText(name: "version", defaultValue: "Version", style: Style(context).styleTitleDialog),
                          ),
                          Expanded(
                            flex: 7,
                            child: DynamicDropdownSearchClearOption(
                              hint: multiLanguageString(name: "choose_a_version", defaultValue: "Choose a version", context: context),
                              controller:
                                  SingleValueDropDownController(data: DropDownValueModel(name: _testsVersion["name"], value: _testsVersion["id"])),
                              items: _listVersions.map<DropDownValueModel>((dynamic result) {
                                return DropDownValueModel(
                                  value: result["id"],
                                  name: result["name"],
                                );
                              }).toList(),
                              maxHeight: 200,
                              onChanged: (dynamic newValue) {
                                _formKey.currentState!.validate();
                                setState(() {
                                  _testsVersion = _listVersions.firstWhere((element) => element["id"] == newValue.value);
                                });
                              },
                              isRequiredNotEmpty: true,
                              enabled: widget.type == "create" || widget.type == "duplicate",
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 3,
                            child: MultiLanguageText(name: "environment", defaultValue: "Environment", style: Style(context).styleTitleDialog),
                          ),
                          Expanded(
                            flex: 7,
                            child: DynamicDropdownSearchClearOption(
                              hint: multiLanguageString(name: "choose_an_environment", defaultValue: "Choose an environment", context: context),
                              controller: SingleValueDropDownController(
                                  data: DropDownValueModel(name: _testsEnvironment["name"], value: _testsEnvironment["id"])),
                              items: _listEnvironment.map<DropDownValueModel>((dynamic result) {
                                return DropDownValueModel(
                                  value: result["id"],
                                  name: result["name"],
                                );
                              }).toList(),
                              maxHeight: 200,
                              onChanged: (dynamic newValue) {
                                _formKey.currentState!.validate();
                                setState(() {
                                  _testsEnvironment = _listEnvironment.firstWhere((element) => element["id"] == newValue.value);
                                });
                              },
                              isRequiredNotEmpty: true,
                              enabled: widget.type == "create" || widget.type == "duplicate",
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 3,
                            child: MultiLanguageText(name: "module", defaultValue: "Module", style: Style(context).styleTitleDialog),
                          ),
                          Expanded(
                            flex: 7,
                            child: DynamicDropdownButton(
                              value: _testsComponent,
                              hint: multiLanguageString(name: "choose_a_module", defaultValue: "Choose a module", context: context),
                              items: _listComponents.map<DropdownMenuItem<String>>((dynamic result) {
                                return DropdownMenuItem(
                                  value: result["id"],
                                  child: Text(result["title"]),
                                );
                              }).toList(),
                              onChanged: (newValue) {
                                setState(() {
                                  _testsComponent = newValue!;
                                });
                              },
                              borderRadius: 5,
                              isRequiredNotEmpty: true,
                              enabled: widget.type == "create" || widget.type == "duplicate",
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                child: Column(
                  children: [
                    if (widget.type == "duplicate")
                      Container(
                        margin: const EdgeInsets.only(bottom: 10),
                        child: Row(
                          children: [
                            Expanded(
                              flex: 3,
                              child: MultiLanguageText(name: "project", defaultValue: "Project", style: Style(context).styleTitleDialog),
                            ),
                            Expanded(
                              flex: 7,
                              child: DynamicDropdownButton(
                                  borderRadius: 5,
                                  value: prjProjectId,
                                  hint: multiLanguageString(name: "choose_a_project", defaultValue: "Choose a project", context: context),
                                  items: _listProject.map<DropdownMenuItem<String>>((dynamic result) {
                                    return DropdownMenuItem(
                                      value: result["id"],
                                      child: Text(result["title"]),
                                    );
                                  }).toList(),
                                  onChanged: (newValue) async {
                                    setState(() {
                                      _formKey.currentState!.validate();
                                      prjProjectId = newValue!;
                                      _testsComponent = null;
                                      _testsVersion = null;
                                      _testsEnvironment = null;
                                    });
                                    await getComponentAndVersion();
                                  },
                                  isRequiredNotEmpty: true),
                            ),
                          ],
                        ),
                      ),
                    Container(
                      margin: EdgeInsets.only(top: (widget.type == "duplicate") ? 10 : 0, bottom: 10),
                      child: Row(
                        children: [
                          const Expanded(
                            flex: 3,
                            child: RowCodeWithTooltipWidget(),
                          ),
                          Expanded(
                            flex: 7,
                            child: DynamicTextField(
                              controller: _testsCodeController,
                              hintText: "...",
                              isRequiredNotEmpty: true,
                              isRequiredRegex: true,
                              enabled: widget.type != "edit",
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 3,
                            child: MultiLanguageText(name: "name", defaultValue: "Name", style: Style(context).styleTitleDialog),
                          ),
                          Expanded(
                            flex: 7,
                            child: DynamicTextField(
                              controller: _testsNameController,
                              hintText: "...",
                              isRequiredNotEmpty: true,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 3,
                            child: MultiLanguageText(name: "test_priority", defaultValue: "Test priority", style: Style(context).styleTitleDialog),
                          ),
                          Expanded(
                            flex: 7,
                            child: DynamicDropdownButton(
                                borderRadius: 5,
                                value: testPriority,
                                hint: multiLanguageString(name: "choose_a_priority", defaultValue: "Choose a priority", context: context),
                                items: [
                                  {"name": multiLanguageString(name: "item_high", defaultValue: "High", context: context), "value": 0},
                                  {"name": multiLanguageString(name: "item_medium", defaultValue: "Medium", context: context), "value": 1},
                                  {"name": multiLanguageString(name: "item_low", defaultValue: "Low", context: context), "value": 2}
                                ].map<DropdownMenuItem<int>>((dynamic result) {
                                  return DropdownMenuItem(
                                    value: result["value"],
                                    child: Text(result["name"]),
                                  );
                                }).toList(),
                                onChanged: (newValue) {
                                  setState(() {
                                    testPriority = newValue!;
                                  });
                                },
                                isRequiredNotEmpty: true),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 3,
                            child: MultiLanguageText(name: "status", defaultValue: "Status", style: Style(context).styleTitleDialog),
                          ),
                          Expanded(
                            flex: 7,
                            child: DynamicDropdownButton(
                                borderRadius: 5,
                                value: _status,
                                hint: multiLanguageString(name: "choose_a_status", defaultValue: "Choose a status", context: context),
                                items: [
                                  {"name": multiLanguageString(name: "draft", defaultValue: "Draft", context: context), "value": 0},
                                  {"name": multiLanguageString(name: "active", defaultValue: "Active", context: context), "value": 1},
                                  {"name": multiLanguageString(name: "review", defaultValue: "Review", context: context), "value": 2},
                                ].map<DropdownMenuItem<int>>((dynamic result) {
                                  return DropdownMenuItem(
                                    value: result["value"],
                                    child: Text(result["name"]),
                                  );
                                }).toList(),
                                onChanged: (newValue) {
                                  setState(() {
                                    _status = newValue!;
                                  });
                                },
                                isRequiredNotEmpty: true),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 3,
                            child:
                                MultiLanguageText(name: "workflow_status", defaultValue: "Workflow Status", style: Style(context).styleTitleDialog),
                          ),
                          Expanded(
                            flex: 7,
                            child: DynamicDropdownButton(
                                borderRadius: 5,
                                value: wfStatusId,
                                hint:
                                    multiLanguageString(name: "choose_a_workflow_status", defaultValue: "Choose a workflow status", context: context),
                                items: _listStatus.map<DropdownMenuItem<String>>((dynamic result) {
                                  return DropdownMenuItem(
                                    value: result["id"],
                                    child: Text(result["title"]),
                                  );
                                }).toList(),
                                onChanged: (newValue) async {
                                  setState(() {
                                    wfStatusId = newValue!;
                                  });
                                },
                                isRequiredNotEmpty: true),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MultiLanguageText(name: "purpose", defaultValue: "Purpose", style: Style(context).styleTitleDialog),
                DynamicTextField(
                  controller: _testsPurposeController,
                  hintText: "...",
                  maxline: 5,
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MultiLanguageText(name: "precondition", defaultValue: "Precondition", style: Style(context).styleTitleDialog),
                DynamicTextField(
                  controller: _testsPreconditionController,
                  hintText: "...",
                  maxline: 5,
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MultiLanguageText(name: "test_steps", defaultValue: "Test steps", style: Style(context).styleTitleDialog),
                DynamicTextField(
                  controller: _testsStepController,
                  hintText: "...",
                  maxline: 5,
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MultiLanguageText(name: "expected_result", defaultValue: "Expected Result", style: Style(context).styleTitleDialog),
                DynamicTextField(
                  controller: _testsExpectedResultController,
                  hintText: "...",
                  maxline: 5,
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MultiLanguageText(name: "test_data", defaultValue: "Test data", style: Style(context).styleTitleDialog),
                DynamicTextField(
                  controller: _testsTestDataController,
                  hintText: "...",
                  maxline: 5,
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MultiLanguageText(name: "description", defaultValue: "Description", style: Style(context).styleTitleDialog),
                DynamicTextField(
                  controller: _testsDesController,
                  hintText: "...",
                  maxline: 3,
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    MultiLanguageText(name: "label", defaultValue: "Label", style: Style(context).styleTitleDialog),
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(
                          createRoute(
                            ManageLabelWidget(
                              labelList: labelsList,
                              onSave: (labelsList) {
                                setState(() {
                                  this.labelsList = labelsList;
                                });
                              },
                            ),
                          ),
                        );
                      },
                      child: MultiLanguageText(
                        name: "click_to_manage_label",
                        defaultValue: "Click To Manage Label",
                        style: Style(context).styleTitleDialog,
                      ),
                    )
                  ],
                ),
                const SizedBox(
                  height: 5,
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                      createRoute(
                        ManageLabelWidget(
                          labelList: labelsList,
                          onSave: (labelsList) {
                            setState(() {
                              this.labelsList = labelsList;
                            });
                          },
                        ),
                      ),
                    );
                  },
                  child: Container(
                    alignment: Alignment.centerLeft,
                    constraints: const BoxConstraints(minHeight: 50),
                    padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: const Color.fromRGBO(216, 218, 229, 1),
                      ),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Wrap(
                      children: [
                        for (var label in labelsList)
                          Container(
                            margin: EdgeInsets.only(right: labelsList.last != label ? 10 : 0, bottom: 5, top: 5),
                            child: IntrinsicWidth(
                              child: LabelListWidget(
                                key: UniqueKey(),
                                labelId: label,
                                onRemove: () {
                                  setState(() {
                                    labelsList.remove(label);
                                  });
                                },
                              ),
                            ),
                          )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Container tabControlWidget(TestCaseConfigModel testCaseConfigModel) {
    List<String> listTabItem = [
      multiLanguageString(name: "general_information", defaultValue: "General information", context: context),
      multiLanguageString(name: "config", defaultValue: "Config", context: context),
    ];
    return Container(
      margin: const EdgeInsets.fromLTRB(50, 0, 50, 0),
      child: Row(
        children: [
          IconButton(
            onPressed: () {
              scrollController.animateTo(scrollController.position.pixels - 400,
                  duration: const Duration(milliseconds: 500), curve: Curves.easeInOut);
            },
            icon: const Icon(Icons.arrow_back_ios),
          ),
          Expanded(
            child: Scrollbar(
              controller: scrollController,
              thickness: 0,
              child: SingleChildScrollView(
                controller: scrollController,
                scrollDirection: Axis.horizontal,
                primary: false,
                child: TabBar(
                  controller: tabController,
                  isScrollable: true,
                  physics: const BouncingScrollPhysics(),
                  labelColor: const Color(0xff3d5afe),
                  indicatorColor: const Color(0xff3d5afe),
                  unselectedLabelColor: const Color.fromRGBO(0, 0, 0, 0.8),
                  labelStyle: Style(context).styleTitleDialog,
                  tabs: [
                    for (var tab in listTabItem)
                      Tab(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
                          child: GestureDetector(
                            onTap: () {
                              testCaseConfigModel.setInitialIndex(listTabItem.indexOf(tab));
                              tabController.animateTo(listTabItem.indexOf(tab));
                            },
                            child: Text(tab),
                          ),
                        ),
                      ),
                  ],
                ),
              ),
            ),
          ),
          IconButton(
            onPressed: () {
              scrollController.animateTo(scrollController.position.pixels + 400,
                  duration: const Duration(milliseconds: 500), curve: Curves.easeInOut);
            },
            icon: const Icon(Icons.arrow_forward_ios),
          ),
        ],
      ),
    );
  }

  Padding headerWidget(TestCaseConfigModel testCaseConfigModel, BuildContext context) {
    Map<String, String> mapTestCaseType = {
      "test_case": multiLanguageString(name: "map_test_case", defaultValue: "Test case", context: context),
      "test_scenario": multiLanguageString(name: "map_test_scenario", defaultValue: "Test scenario", context: context),
      "manual_test": multiLanguageString(name: "map_test_manual", defaultValue: "Test manual", context: context),
      "test_suite": multiLanguageString(name: "map_test_suite", defaultValue: "Test suite", context: context),
      "test_api": multiLanguageString(name: "map_api", defaultValue: "Api", context: context),
      "test_jdbc": multiLanguageString(name: "map_jdbc", defaultValue: "JDBC", context: context),
      "action": multiLanguageString(name: "map_shared_steps", defaultValue: "Shared steps", context: context),
    };
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 45, 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text("${widget.title.toUpperCase()} ${(mapTestCaseType[widget.testCaseType] ?? "").toUpperCase()}", style: Style(context).styleTitleDialog),
          Padding(
            padding: const EdgeInsets.only(right: 12),
            child: TextButton(
              onPressed: () {
                Navigator.pop(context, 'cancel');
              },
              child: const Icon(
                Icons.close,
                color: Colors.black,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
