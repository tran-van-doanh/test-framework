import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/common/file_download.dart';
import 'package:test_framework_view/components/badge/badge_icon.dart';
import 'package:test_framework_view/components/dialog/dialog_delete.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/editor/editor/editor_page.dart';
import 'package:test_framework_view/page/list_test/tests/header/header_list_test_widget.dart';
import 'package:test_framework_view/page/list_test/tests/test/dialog_option_test.dart';
import 'package:test_framework_view/type.dart';
import '../../../../api.dart';
import '../../../../common/custom_state.dart';
import '../../../../config.dart';

class ListTestAllWidget extends StatefulWidget {
  const ListTestAllWidget({Key? key}) : super(key: key);

  @override
  State<ListTestAllWidget> createState() => _ListTestAllWidgetState();
}

class _ListTestAllWidgetState extends CustomState<ListTestAllWidget> {
  late Future futureListTfDirectoryTestCase;
  var resultListTestCase = [];
  var rowCountListTestCase = 0;
  var currentPageTestCase = 1;
  var rowPerPageTestCase = 10;
  var searchRequest = {};
  var findRequestTestCase = {};
  var prjProjectId = "";
  PlatformFile? fileTestCase;
  PlatformFile? fileContent;
  Map<String, String> mapTestCaseType = {};

  @override
  void initState() {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    prjProjectId = navigationModel.prjProjectId ?? "";
    mapTestCaseType = {
      "test_case": multiLanguageString(name: "map_test_case", defaultValue: "Test case", context: context),
      "test_scenario": multiLanguageString(name: "map_test_scenario", defaultValue: "Test scenario", context: context),
      "manual_test": multiLanguageString(name: "map_test_manual", defaultValue: "Test manual", context: context),
      "test_suite": multiLanguageString(name: "map_test_suite", defaultValue: "Test suite", context: context),
      "test_api": multiLanguageString(name: "map_api", defaultValue: "Api", context: context),
      "test_jdbc": multiLanguageString(name: "map_jdbc", defaultValue: "JDBC", context: context),
      "action": multiLanguageString(name: "map_shared_steps", defaultValue: "Shared steps", context: context),
    };
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Provider.of<TestCaseConfigModel>(context, listen: false).clearProvider();
    });
    futureListTfDirectoryTestCase = getListTfTestCase(currentPageTestCase, findRequestTestCase);

    super.initState();
  }

  getListTfTestCase(page, findRequest) async {
    if ((page - 1) * rowPerPageTestCase > rowCountListTestCase) {
      page = (1.0 * rowCountListTestCase / rowPerPageTestCase).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequestTestCase = findRequest;
    if (findRequest["titleLike"] != null && findRequest["titleLike"].isNotEmpty) {
      findRequestTestCase["titleLike"] = "%${findRequest["titleLike"]}%";
    }
    if (findRequest["nameLike"] != null && findRequest["nameLike"].isNotEmpty) {
      findRequestTestCase["nameLike"] = "%${findRequest["nameLike"]}%";
    }
    findRequestTestCase["queryOffset"] = (page - 1) * rowPerPageTestCase;
    findRequestTestCase["queryLimit"] = rowPerPageTestCase;
    findRequestTestCase["prjProjectId"] = prjProjectId;
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-case/search", findRequestTestCase, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPageTestCase = page;
        rowCountListTestCase = response["body"]["rowCount"];
        resultListTestCase = response["body"]["resultList"];
        for (var element in resultListTestCase) {
          element["type"] = "test_scenario";
        }
      });
    }
    return 0;
  }

  handleDeleteDirectoryTestCase(id) async {
    var response = await httpDelete("/test-framework-api/user/test-framework/tf-test-case/$id", context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        futureListTfDirectoryTestCase = getListTfTestCase(currentPageTestCase, findRequestTestCase);
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
      }
    }
  }

  handleAddTfDirectoryTestCase(result) async {
    var response = await httpPut("/test-framework-api/user/test-framework/tf-test-case", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        Navigator.of(context).pop();
        futureListTfDirectoryTestCase = getListTfTestCase(currentPageTestCase, findRequestTestCase);
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
      }
    }
  }

  handleEditTfDirectoryTestCase(result, id) async {
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-case/$id", result, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      if (response["body"].containsKey("errorMessage") && mounted) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        futureListTfDirectoryTestCase = getListTfTestCase(currentPageTestCase, findRequestTestCase);
        return true;
      }
    }
    return false;
  }

  handleDuplicateTfDirectoryTestCase(result) async {
    var response = await httpPut("/test-framework-api/user/test-framework/tf-test-case", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        Navigator.of(context).pop();
        futureListTfDirectoryTestCase = getListTfTestCase(currentPageTestCase, findRequestTestCase);
        showToast(
            msg: multiLanguageString(name: "duplicate_successful", defaultValue: "Duplicate successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.error),
            context: context);
      }
    }
  }

  bool checkTestCaseType(String testCaseType) {
    if (testCaseType == "test_case" || testCaseType == "manual_test" || testCaseType == "test_api") {
      return true;
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return Consumer3<NavigationModel, TestCaseDirectoryModel, LanguageModel>(
      builder: (context, navigationModel, testCaseDirectoryModel, languageModel, child) {
        return FutureBuilder(
          future: futureListTfDirectoryTestCase,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Stack(
                children: [
                  ListView(
                    children: [
                      NavMenuTabV1(
                        currentUrl: "/test-list/all",
                        margin: const EdgeInsets.fromLTRB(50, 20, 50, 0),
                        navigationList: context.testMenuList,
                      ),
                      headerWidget(context),
                      SingleChildScrollView(
                        child: Container(
                          margin: const EdgeInsets.fromLTRB(50, 0, 50, 50),
                          child: SelectionArea(
                            child: Column(
                              children: [
                                (resultListTestCase.isNotEmpty)
                                    ? tableTestCaseWidget(context, navigationModel, testCaseDirectoryModel)
                                    : const SizedBox.shrink(),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Positioned(bottom: 10, right: 50, child: btnWidget(testCaseDirectoryModel, navigationModel, context)),
                ],
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  SingleChildScrollView btnWidget(TestCaseDirectoryModel testCaseDirectoryModel, NavigationModel navigationModel, BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: [
          Container(
            margin: const EdgeInsets.only(right: 10),
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              onPressed: () {
                Navigator.of(context).push(
                  createRoute(
                    DialogOptionTestWidget(
                      testCaseType: "",
                      type: "create",
                      title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                      callbackOptionTest: (result) async {
                        handleAddTfDirectoryTestCase(result);
                      },
                      selectedItem: const {},
                    ),
                  ),
                );
              },
              icon: const Icon(Icons.note_add, color: Color.fromRGBO(49, 49, 49, 1)),
              label: MultiLanguageText(
                name: "new_test",
                defaultValue: "New Test",
                isLowerCase: false,
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
            ),
          ),
        ],
      ),
    );
  }

  Column tableTestCaseWidget(BuildContext context, NavigationModel navigationModel, TestCaseDirectoryModel testCaseDirectoryModel) {
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.only(top: 10),
          child: CustomDataTableWidget(
            columns: [
              Expanded(
                child: MultiLanguageText(
                  name: "type",
                  defaultValue: "Type",
                  isLowerCase: false,
                  style: Style(context).styleTextDataColumn,
                ),
              ),
              Expanded(
                child: MultiLanguageText(
                  name: "code",
                  defaultValue: "Code",
                  isLowerCase: false,
                  style: Style(context).styleTextDataColumn,
                ),
              ),
              Expanded(
                flex: 3,
                child: MultiLanguageText(
                  name: "name",
                  defaultValue: "Name",
                  isLowerCase: false,
                  style: Style(context).styleTextDataColumn,
                ),
              ),
              // Expanded(
              //   flex: 3,
              //   child: Text(
              //     AppLocalizations.of(
              //             context)!
              //         .description
              //         .toUpperCase(),
              //     style:
              //         Style(context).styleTextDataColumn,
              //   ),
              // ),
              Expanded(
                child: MultiLanguageText(
                  name: "owner",
                  defaultValue: "Owner",
                  isLowerCase: false,
                  style: Style(context).styleTextDataColumn,
                ),
              ),
              Expanded(
                child: Center(
                  child: MultiLanguageText(
                    name: "create_date",
                    defaultValue: "Create date",
                    isLowerCase: false,
                    style: Style(context).styleTextDataColumn,
                  ),
                ),
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    MultiLanguageText(
                      name: "status",
                      defaultValue: "Status",
                      isLowerCase: false,
                      style: Style(context).styleTextDataColumn,
                    ),
                    Tooltip(
                      message: multiLanguageString(
                          name: "active_draft_review", defaultValue: "Green = Active\nGrey = Draft\nRed = Review ", context: context),
                      child: const Icon(
                        Icons.info,
                        color: Colors.grey,
                        size: 16,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                width: 38,
              ),
            ],
            rows: [
              for (var resultTestCase in resultListTestCase)
                CustomDataRow(
                  cells: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Text(
                          mapTestCaseType[resultTestCase["testCaseType"]] ?? "",
                          style: Style(context).styleTextDataCell,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Row(
                          children: [
                            const Icon(Icons.description, color: Color.fromRGBO(61, 148, 254, 1)),
                            const SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: Text(
                                resultTestCase["name"],
                                style: Style(context).styleTextDataCell,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Text(
                          resultTestCase["title"] ?? "",
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: Style(context).styleTextDataCell,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Text(
                          resultTestCase["sysUser"]["fullname"],
                          style: Style(context).styleTextDataCell,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Center(
                          child: Text(
                            DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultTestCase["createDate"]).toLocal()),
                            style: Style(context).styleTextDataCell,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Center(
                          child: Badge3StatusWidget(
                            status: resultTestCase["status"],
                          ),
                        ),
                      ),
                    ),
                    PopupMenuButton(
                      offset: const Offset(5, 50),
                      tooltip: '',
                      splashRadius: 10,
                      icon: const Icon(
                        Icons.more_vert,
                      ),
                      itemBuilder: (context) => [
                        if (!checkTestCaseType(resultTestCase["testCaseType"]))
                          PopupMenuItem(
                            padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                            child: ListTile(
                              onTap: () {
                                Navigator.pop(context);
                                Navigator.of(context).push(
                                  createRoute(
                                    EditorPageRootWidget(
                                      testCaseType: resultTestCase["testCaseType"],
                                      testCaseId: resultTestCase["id"],
                                      callback: () {
                                        futureListTfDirectoryTestCase = getListTfTestCase(currentPageTestCase, findRequestTestCase);
                                      },
                                    ),
                                  ),
                                );
                              },
                              contentPadding: const EdgeInsets.all(0),
                              hoverColor: Colors.transparent,
                              title: const MultiLanguageText(name: "config", defaultValue: "Config"),
                              leading: const Icon(Icons.settings, color: Color.fromARGB(255, 112, 114, 119)),
                            ),
                          ),
                        PopupMenuItem(
                          padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                          child: ListTile(
                            onTap: () async {
                              var responseGetTestApi =
                                  await httpGet("/test-framework-api/user/test-framework/tf-test-case/${resultTestCase["id"]}", context);
                              if (mounted) {
                                Navigator.pop(context);
                                Navigator.of(context).push(
                                  createRoute(
                                    DialogOptionTestWidget(
                                      testCaseType: responseGetTestApi["body"]["result"]["testCaseType"],
                                      type: "edit",
                                      title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                      selectedItem: (responseGetTestApi.containsKey("body") && responseGetTestApi["body"] is String == false)
                                          ? responseGetTestApi["body"]["result"] ?? {}
                                          : {},
                                      callbackOptionTest: (result) async {
                                        bool resultFunction = await handleEditTfDirectoryTestCase(result, resultTestCase["id"]);
                                        if (resultFunction && mounted) {
                                          Navigator.of(context).pop();
                                          showToast(
                                              context: context,
                                              msg:
                                                  multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
                                              color: Colors.greenAccent,
                                              icon: const Icon(Icons.done));
                                        }
                                      },
                                    ),
                                  ),
                                );
                              }
                            },
                            contentPadding: const EdgeInsets.all(0),
                            hoverColor: Colors.transparent,
                            title: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                            leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
                          ),
                        ),
                        PopupMenuItem(
                          padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                          child: ListTile(
                            onTap: () {
                              Navigator.pop(context);
                              dialogDeleteDirectoryTestCase(resultTestCase);
                            },
                            contentPadding: const EdgeInsets.all(0),
                            hoverColor: Colors.transparent,
                            title: const MultiLanguageText(name: "delete", defaultValue: "Delete"),
                            leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                          ),
                        ),
                        PopupMenuItem(
                          padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                          child: ListTile(
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.of(context).push(
                                createRoute(
                                  DialogOptionTestWidget(
                                    testCaseType: resultTestCase["testCaseType"],
                                    type: "duplicate",
                                    title: multiLanguageString(name: "duplicate", defaultValue: "Duplicate", context: context),
                                    selectedItem: resultTestCase,
                                    callbackOptionTest: (result) {
                                      handleDuplicateTfDirectoryTestCase(result);
                                    },
                                  ),
                                ),
                              );
                            },
                            contentPadding: const EdgeInsets.all(0),
                            hoverColor: Colors.transparent,
                            title: const MultiLanguageText(name: "duplicate", defaultValue: "Duplicate"),
                            leading: const Icon(Icons.control_point_duplicate, color: Color.fromARGB(255, 112, 114, 119)),
                          ),
                        ),
                        if (resultTestCase["testCaseType"] != "action" && resultTestCase["testCaseType"] != "test_api")
                          PopupMenuItem(
                            padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                            child: ListTile(
                              onTap: () {
                                navigationModel.navigate(
                                    "/qa-platform/project/${navigationModel.prjProjectId}/test-list/${resultTestCase["testCaseType"]}/${resultTestCase["id"]}/run/${resultTestCase["parentId"]}");
                              },
                              contentPadding: const EdgeInsets.all(0),
                              hoverColor: Colors.transparent,
                              title: const MultiLanguageText(name: "run", defaultValue: "Run"),
                              leading: const Icon(Icons.play_arrow, color: Color.fromARGB(255, 112, 114, 119)),
                            ),
                          ),
                        PopupMenuItem(
                          padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                          child: ListTile(
                            onTap: () async {
                              fileContent = await selectFile(['json']);
                              if (fileContent != null) {
                                await uploadFile(
                                    fileContent, "/test-framework-api/user/test-framework-file/tf-test-case/${resultTestCase["id"]}/import-content");
                              }
                              if (mounted) {
                                Navigator.pop(context);
                              }
                            },
                            contentPadding: const EdgeInsets.all(0),
                            hoverColor: Colors.transparent,
                            title: const MultiLanguageText(name: "import", defaultValue: "Import"),
                            leading: const Icon(Icons.upload_file, color: Color.fromARGB(255, 112, 114, 119)),
                          ),
                        ),
                        PopupMenuItem(
                          padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                          child: ListTile(
                            onTap: () async {
                              await downloadFile(resultTestCase["id"]);
                              if (mounted) {
                                Navigator.pop(context);
                              }
                            },
                            contentPadding: const EdgeInsets.all(0),
                            hoverColor: Colors.transparent,
                            title: const MultiLanguageText(name: "export", defaultValue: "Export"),
                            leading: const Icon(Icons.cloud_download, color: Color.fromARGB(255, 112, 114, 119)),
                          ),
                        ),
                      ],
                    ),
                  ],
                  onSelectChanged: () async {
                    if (checkTestCaseType(resultTestCase["testCaseType"])) {
                      var responseGetTestApi = await httpGet("/test-framework-api/user/test-framework/tf-test-case/${resultTestCase["id"]}", context);
                      if (mounted) {
                        Navigator.of(context).push(
                          createRoute(
                            DialogOptionTestWidget(
                              testCaseType: responseGetTestApi["body"]["result"]["testCaseType"],
                              type: "edit",
                              title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                              selectedItem: (responseGetTestApi.containsKey("body") && responseGetTestApi["body"] is String == false)
                                  ? responseGetTestApi["body"]["result"] ?? {}
                                  : {},
                              callbackOptionTest: (result) async {
                                bool resultFunction = await handleEditTfDirectoryTestCase(result, resultTestCase["id"]);
                                if (resultFunction && mounted) {
                                  Navigator.of(context).pop();
                                  showToast(
                                      context: context,
                                      msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
                                      color: Colors.greenAccent,
                                      icon: const Icon(Icons.done));
                                }
                              },
                            ),
                          ),
                        );
                      }
                    } else {
                      Navigator.of(context).push(
                        createRoute(
                          EditorPageRootWidget(
                            testCaseType: resultTestCase["testCaseType"],
                            testCaseId: resultTestCase["id"],
                            callback: () {
                              futureListTfDirectoryTestCase = getListTfTestCase(currentPageTestCase, findRequestTestCase);
                            },
                          ),
                        ),
                      );
                    }
                  },
                ),
            ],
          ),
        ),
        DynamicTablePagging(
          rowCountListTestCase,
          currentPageTestCase,
          rowPerPageTestCase,
          pageChangeHandler: (page) {
            setState(() {
              futureListTfDirectoryTestCase = getListTfTestCase(page, findRequestTestCase);
            });
          },
          rowPerPageChangeHandler: (rowPerPage) {
            setState(() {
              rowPerPageTestCase = rowPerPage!;
              futureListTfDirectoryTestCase = getListTfTestCase(1, findRequestTestCase);
            });
          },
        ),
      ],
    );
  }

  Padding headerWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: HeaderListTestWidget(
              type: "All",
              title: multiLanguageString(name: "all", defaultValue: "All", context: context),
              callbackFilter: (text) {
                findRequestTestCase = text;
                getListTfTestCase(currentPageTestCase, findRequestTestCase);
              },
              callbackExport: (result) async {
                searchRequest = result;
                var findRequestTestCase = {};
                findRequestTestCase.addAll(searchRequest);
                findRequestTestCase["prjProjectId"] = prjProjectId;
                if (searchRequest["titleLike"] != null && searchRequest["titleLike"].isNotEmpty) {
                  findRequestTestCase["titleLike"] = "%${searchRequest["titleLike"]}%";
                }
                if (searchRequest["nameLike"] != null && searchRequest["nameLike"].isNotEmpty) {
                  findRequestTestCase["nameLike"] = "%${searchRequest["nameLike"]}%";
                }
                if (findRequestTestCase.containsKey("tfTestDirectoryId")) {
                  findRequestTestCase.remove("tfTestDirectoryId");
                }
                if (findRequestTestCase.containsKey("queryOffset")) {
                  findRequestTestCase.remove("queryOffset");
                }
                if (findRequestTestCase.containsKey("queryLimit")) {
                  findRequestTestCase.remove("queryLimit");
                }
                File? downloadedFile = await httpDownloadFileWithBodyRequest(
                    context,
                    "/test-framework-api/user/test-framework-file/tf-test-case/export-excel",
                    Provider.of<SecurityModel>(context, listen: false).authorization,
                    'Output.xlsx',
                    findRequestTestCase);
                if (downloadedFile != null && mounted) {
                  var snackBar = SnackBar(
                    content: MultiLanguageText(name: "file_downloaded_to", defaultValue: "File downloaded to \$0 .", variables: ["$downloadedFile"]),
                  );
                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                }
              },
              callbackImport: () async {
                fileTestCase = await selectFile(['xlsx']);
                if (fileTestCase != null) {
                  uploadFile(fileTestCase, "/test-framework-api/user/project-file/prj-project/$prjProjectId/tf-test-case/import-excel");
                }
              },
            ),
          ),
        ],
      ),
    );
  }

  uploadFile(PlatformFile? file, String url) async {
    final request = http.MultipartRequest(
      "POST",
      Uri.parse(
        "$baseUrl$url",
      ),
    );
    request.headers.addAll({"Authorization": Provider.of<SecurityModel>(context, listen: false).authorization!});
    request.files.add(http.MultipartFile.fromBytes('file', await readFile(file!), filename: file.name));
    var resp = await request.send();
    var responseDecode = jsonDecode(await resp.stream.bytesToString());
    if (responseDecode.containsKey("errorMessage") && mounted) {
      showToast(context: context, msg: responseDecode["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
    } else if (mounted) {
      showToast(
          context: context,
          msg: multiLanguageString(name: "upload_success", defaultValue: "Upload success", context: context),
          color: Colors.greenAccent,
          icon: const Icon(Icons.done));
    }
  }

  downloadFile(String id) async {
    File? downloadedFile = await httpDownloadFile(context, "/test-framework-api/user/test-framework-file/tf-test-case/$id/export-content",
        Provider.of<SecurityModel>(context, listen: false).authorization, 'Output.zip');
    if (downloadedFile != null && mounted) {
      var snackBar = SnackBar(
        content: MultiLanguageText(name: "file_downloaded_to", defaultValue: "File downloaded to \$0 .", variables: ["$downloadedFile"]),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  dialogDeleteDirectoryTestCase(selectedItem) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogDelete(
          selectedItem: selectedItem,
          callback: () {
            handleDeleteDirectoryTestCase(selectedItem["id"]);
          },
          type: 'test',
        );
      },
    );
  }
}
