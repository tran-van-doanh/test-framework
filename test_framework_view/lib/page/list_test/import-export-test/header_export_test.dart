import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import '../../../api.dart';
import '../../../common/custom_state.dart';
import '../../../common/dynamic_dropdown_button.dart';

class HeaderExportTestWidget extends StatefulWidget {
  final Function? callbackFilter;

  const HeaderExportTestWidget({
    Key? key,
    this.callbackFilter,
  }) : super(key: key);

  @override
  State<HeaderExportTestWidget> createState() => _HeaderExportTestWidgetState();
}

class _HeaderExportTestWidgetState extends CustomState<HeaderExportTestWidget> {
  final TextEditingController _searchLibrary = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  SingleValueDropDownController statusController = SingleValueDropDownController();
  SingleValueDropDownController testOwnerController = SingleValueDropDownController();
  SingleValueDropDownController testCaseTypeController = SingleValueDropDownController();

  var prjProjectId = "";
  var _listTestOwner = [];
  bool isShowFilterForm = true;
  bool isSearched = false;
  @override
  void initState() {
    prjProjectId = Provider.of<NavigationModel>(context, listen: false).prjProjectId ?? "";
    getListOwner();
    super.initState();
  }

  getListOwner() async {
    var requestBody = {"prjProjectId": prjProjectId};
    var response = await httpPost("/test-framework-api/user/sys/sys-user-project/search", requestBody, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listTestOwner = response["body"]["resultList"];
      });
    }
  }

  handleCallBackSearchFunction() {
    var result = {
      "testCaseType": testCaseTypeController.dropDownValue?.value,
      "status": statusController.dropDownValue?.value,
      "sysUserId": testOwnerController.dropDownValue?.value,
      "titleLike": _searchLibrary.text,
      "nameLike": _nameController.text,
    };
    widget.callbackFilter!(result);
  }

  @override
  void dispose() {
    _searchLibrary.dispose();
    _nameController.dispose();
    statusController.dispose();
    testCaseTypeController.dispose();
    testOwnerController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      List<Widget> widgetList = [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
            child: SizedBox(
              height: 50,
              child: DynamicDropdownSearchClearOption(
                labelText: multiLanguageString(name: "test_type", defaultValue: "Test type", context: context),
                controller: testCaseTypeController,
                hint: multiLanguageString(name: "search_test_type", defaultValue: "Search test type", context: context),
                items: [
                  {"name": multiLanguageString(name: "map_test_case", defaultValue: "Test case", context: context), "value": "test_case"},
                  {"name": multiLanguageString(name: "map_test_scenario", defaultValue: "Test scenario", context: context), "value": "test_scenario"},
                  {"name": multiLanguageString(name: "map_test_manual", defaultValue: "Test manual", context: context), "value": "manual_test"},
                  {"name": multiLanguageString(name: "map_test_suite", defaultValue: "Test suite", context: context), "value": "test_suite"},
                  {"name": multiLanguageString(name: "map_api", defaultValue: "Api", context: context), "value": "test_api"},
                  {"name": multiLanguageString(name: "map_shared_steps", defaultValue: "Shared steps", context: context), "value": "action"},
                ].map<DropDownValueModel>((dynamic result) {
                  return DropDownValueModel(
                    value: result["value"],
                    name: result["name"],
                  );
                }).toList(),
                maxHeight: 200,
                onChanged: (dynamic newValue) {
                  if (isSearched && newValue == "") {
                    handleCallBackSearchFunction();
                  }
                  // if (newValue == "") {
                  //   handleCallBackSearchFunction();
                  // }
                },
              ),
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
            child: SizedBox(
              height: 50,
              child: DynamicTextField(
                controller: _nameController,
                onComplete: () => handleCallBackSearchFunction(),
                onChanged: (value) {
                  setState(() {
                    _nameController.text;
                  });
                },
                labelText: multiLanguageString(name: "test_code", defaultValue: "Test code", context: context),
                hintText: multiLanguageString(name: "by_code", defaultValue: "By code", context: context),
                suffixIcon: (_nameController.text != "")
                    ? IconButton(
                        onPressed: () {
                          setState(() {
                            _nameController.clear();
                            if (isSearched) {
                              handleCallBackSearchFunction();
                            }
                          });
                        },
                        icon: const Icon(Icons.clear),
                        splashRadius: 20,
                      )
                    : null,
              ),
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
            child: SizedBox(
              height: 50,
              child: DynamicTextField(
                controller: _searchLibrary,
                onComplete: () => handleCallBackSearchFunction(),
                onChanged: (value) {
                  setState(() {
                    _searchLibrary.text;
                  });
                },
                labelText: multiLanguageString(name: "test_name", defaultValue: "Test name", context: context),
                hintText: multiLanguageString(name: "by_name", defaultValue: "By name", context: context),
                suffixIcon: (_searchLibrary.text != "")
                    ? IconButton(
                        onPressed: () {
                          setState(() {
                            _searchLibrary.clear();
                            if (isSearched) {
                              handleCallBackSearchFunction();
                            }
                          });
                        },
                        icon: const Icon(Icons.clear),
                        splashRadius: 20,
                      )
                    : null,
              ),
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
            child: DynamicDropdownSearchClearOption(
              labelText: multiLanguageString(name: "search_status", defaultValue: "Search status", context: context),
              controller: statusController,
              hint: multiLanguageString(name: "search_status", defaultValue: "Search status", context: context),
              items: {
                multiLanguageString(name: "draft", defaultValue: "Draft", context: context): 0,
                multiLanguageString(name: "review", defaultValue: "Review", context: context): 2,
                multiLanguageString(name: "active", defaultValue: "Active", context: context): 1
              }.entries.map<DropDownValueModel>((result) {
                return DropDownValueModel(
                  value: result.value,
                  name: result.key,
                );
              }).toList(),
              maxHeight: 200,
              onChanged: (dynamic newValue) {
                if (isSearched && newValue == "") {
                  handleCallBackSearchFunction();
                }
                // if (newValue == "") {
                //   handleCallBackSearchFunction();
                // }
              },
            ),
          ),
        ),
        Expanded(
          child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
              child: DynamicDropdownSearchClearOption(
                labelText: multiLanguageString(name: "test_owner", defaultValue: "Test owner", context: context),
                controller: testOwnerController,
                hint: multiLanguageString(name: "search_test_owner", defaultValue: "Search Test owner", context: context),
                items: _listTestOwner.map<DropDownValueModel>((dynamic result) {
                  return DropDownValueModel(
                    value: result["sysUser"]["id"],
                    name: result["sysUser"]["fullname"],
                  );
                }).toList(),
                maxHeight: 200,
                onChanged: (dynamic newValue) {
                  if (isSearched && newValue == "") {
                    handleCallBackSearchFunction();
                  }
                  // if (newValue == "") {
                  //   handleCallBackSearchFunction();
                  // }
                },
              )),
        ),
      ];
      var itemPerRow = 4;
      if (constraints.maxWidth < 600) {
        itemPerRow = 1;
      } else if (constraints.maxWidth < 800) {
        itemPerRow = 2;
      } else if (constraints.maxWidth < 1200) {
        itemPerRow = 3;
      }
      var rowCount = (1.0 * widgetList.length / itemPerRow).ceil();
      var filterWidget = Column(
        children: [
          for (var rowI = 0; rowI < rowCount; rowI++)
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                for (var colI = 0; colI < itemPerRow; colI++)
                  rowI * itemPerRow + colI < widgetList.length ? widgetList[rowI * itemPerRow + colI] : Expanded(child: Container())
              ],
            )
        ],
      );
      return Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MultiLanguageText(
            name: "export_multiple_test",
            defaultValue: "Export Multiple Test",
            style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
          ),
          Row(
            children: [
              MultiLanguageText(
                name: "filter_form",
                defaultValue: "Filter form",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: const Color.fromRGBO(0, 0, 0, 0.8),
                    fontSize: context.fontSizeManager.fontSizeText16,
                    letterSpacing: 2),
              ),
              IconButton(
                onPressed: () {
                  setState(() {
                    isShowFilterForm = !isShowFilterForm;
                  });
                },
                icon: isShowFilterForm ? const Icon(Icons.visibility) : const Icon(Icons.visibility_off),
                splashRadius: 1,
                tooltip: isShowFilterForm
                    ? multiLanguageString(name: "hide_filter_form", defaultValue: "Hide filter form", context: context)
                    : multiLanguageString(name: "show_filter_form", defaultValue: "Show filter form", context: context),
              )
            ],
          ),
          if (isShowFilterForm) filterWidget,
          if (isShowFilterForm)
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 20),
                  child: ElevatedButton(
                      onPressed: () {
                        isSearched = true;
                        handleCallBackSearchFunction();
                      },
                      child: const MultiLanguageText(name: "search", defaultValue: "Search", isLowerCase: false)),
                ),
                if (widgetList.length > 2)
                  ElevatedButton(
                      onPressed: () {
                        setState(() {
                          _searchLibrary.clear();
                          _nameController.clear();
                          testOwnerController.clearDropDown();
                          statusController.clearDropDown();
                          testCaseTypeController.clearDropDown();
                          widget.callbackFilter!({});
                        });
                      },
                      child: const MultiLanguageText(name: "reset", defaultValue: "Reset", isLowerCase: false))
              ],
            )
        ],
      );
    });
  }
}
