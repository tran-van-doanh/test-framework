import 'dart:convert';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:http/http.dart' as http;
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/type.dart';
import '../../../api.dart';
import '../../../common/custom_state.dart';
import '../../../config.dart';
import 'dart:io';
import 'package:test_framework_view/common/file_download.dart';
import 'header_export_test.dart';

class ImportExportMultiTestBodyWidget extends StatefulWidget {
  const ImportExportMultiTestBodyWidget({Key? key}) : super(key: key);

  @override
  State<ImportExportMultiTestBodyWidget> createState() => _ImportExportMultiTestBodyWidgetState();
}

class _ImportExportMultiTestBodyWidgetState extends CustomState<ImportExportMultiTestBodyWidget> {
  var resultListTestCase = [];
  var rowCountListTestCase = 0;
  var currentPageTestCase = 1;
  var rowPerPageTestCase = 5;
  var findRequestTestCase = {};
  var prjProjectId = "";
  PlatformFile? fileTestCase;
  List testsExport = [];
  Map<String, String> mapTestCaseType = {};

  @override
  void initState() {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    prjProjectId = navigationModel.prjProjectId ?? "";
    mapTestCaseType = {
      "test_case": multiLanguageString(name: "map_test_case", defaultValue: "Test case", context: context),
      "test_scenario": multiLanguageString(name: "map_test_scenario", defaultValue: "Test scenario", context: context),
      "manual_test": multiLanguageString(name: "map_test_manual", defaultValue: "Test manual", context: context),
      "test_suite": multiLanguageString(name: "map_test_suite", defaultValue: "Test suite", context: context),
      "test_api": multiLanguageString(name: "map_api", defaultValue: "Api", context: context),
      "test_jdbc": multiLanguageString(name: "map_jdbc", defaultValue: "JDBC", context: context),
      "action": multiLanguageString(name: "map_shared_steps", defaultValue: "Shared steps", context: context),
    };
    super.initState();
    search({});
  }

  search(findRequestTestCase) async {
    this.findRequestTestCase = findRequestTestCase;
    await getListTfTestCase(currentPageTestCase);
  }

  getListTfTestCase(page) async {
    if ((page - 1) * rowPerPageTestCase > rowCountListTestCase) {
      page = (1.0 * rowCountListTestCase / rowPerPageTestCase).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * rowPerPageTestCase, "queryLimit": rowPerPageTestCase, "prjProjectId": prjProjectId};
    if (findRequestTestCase["titleLike"] != null && findRequestTestCase["titleLike"].isNotEmpty) {
      findRequest["titleLike"] = "%${findRequestTestCase["titleLike"]}%";
    }
    if (findRequestTestCase["nameLike"] != null && findRequestTestCase["nameLike"].isNotEmpty) {
      findRequest["nameLike"] = "%${findRequestTestCase["nameLike"]}%";
    }
    if (findRequestTestCase["status"] != null) {
      findRequest["status"] = findRequestTestCase["status"];
    }
    if (findRequestTestCase["testCaseType"] != null) {
      findRequest["testCaseType"] = findRequestTestCase["testCaseType"];
    }
    if (findRequestTestCase["sysUserId"] != null && findRequestTestCase["sysUserId"].isNotEmpty) {
      findRequest["sysUserId"] = findRequestTestCase["sysUserId"];
    }
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-case/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPageTestCase = page;
        rowCountListTestCase = response["body"]["rowCount"];
        resultListTestCase = response["body"]["resultList"];
        for (var element in resultListTestCase) {
          element["type"] = "test_scenario";
        }
      });
    }
    return 0;
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, LanguageModel>(
      builder: (context, navigationModel, languageModel, child) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    NavMenuTabV1(
                      currentUrl: "/test-list/import-export",
                      margin: const EdgeInsets.fromLTRB(50, 20, 50, 0),
                      navigationList: context.testMenuList,
                    ),
                    headerImportWidget(context),
                    headerExportWidget(context),
                    SingleChildScrollView(
                      child: Container(
                        margin: const EdgeInsets.fromLTRB(50, 0, 50, 30),
                        child: SelectionArea(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: tableTestCaseWidget("List Test Search", resultListTestCase, testsExport),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                child: tableTestCaseWidget("List Test Export", testsExport, resultListTestCase),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            btnWidget(),
          ],
        );
      },
    );
  }

  Padding btnWidget() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 10, 50, 10),
      child: Align(
        alignment: Alignment.centerRight,
        child: SizedBox(
          height: 40,
          child: FloatingActionButton.extended(
            heroTag: null,
            onPressed: () async {
              var requestBody = {
                "tfTestCaseIdList": [
                  for (var item in testsExport) item["id"],
                ],
              };
              File? downloadedFile = await httpDownloadFileWithBodyRequest(
                  context,
                  "/test-framework-api/user/test-framework-file/tf-test-case/export",
                  Provider.of<SecurityModel>(context, listen: false).authorization,
                  'Output.json',
                  requestBody);
              if (downloadedFile != null && mounted) {
                var snackBar = SnackBar(
                  content: MultiLanguageText(name: "file_downloaded_to", defaultValue: "File downloaded to \$0 .", variables: ["$downloadedFile"]),
                );
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
              }
            },
            icon: const FaIcon(FontAwesomeIcons.fileArrowDown, color: Color.fromRGBO(49, 49, 49, 1), size: 20),
            label: MultiLanguageText(
              name: "export",
              defaultValue: "Export",
              style: TextStyle(
                  fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
            ),
            backgroundColor: const Color.fromARGB(175, 8, 196, 80),
            elevation: 0,
            hoverElevation: 0,
            tooltip: multiLanguageString(name: "export_to_excel", defaultValue: "Export to Excel", context: context),
          ),
        ),
      ),
    );
  }

  Column tableTestCaseWidget(
    String label,
    List<dynamic> list,
    List<dynamic> list1,
  ) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: Style(context).styleTitleDialog,
        ),
        CustomDataTableWidget(
          minWidth: 500,
          columns: [
            Expanded(
              child: MultiLanguageText(
                name: "type",
                defaultValue: "Type",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "code",
                defaultValue: "Code",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              flex: 2,
              child: MultiLanguageText(
                name: "name",
                defaultValue: "Name",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Container(
              width: 56,
            ),
          ],
          rows: [
            for (var item in list)
              CustomDataRow(
                cells: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        mapTestCaseType[item["testCaseType"]] ?? "",
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        item["name"],
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        item["title"] ?? "",
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 56,
                    height: 35,
                    child: (label == "List Test Search" &&
                            !list1.any(
                              (element) => element["id"] == item["id"],
                            ))
                        ? ElevatedButton(
                            onPressed: () {
                              setState(() {
                                list1.add(item);
                              });
                            },
                            child: const Icon(Icons.add),
                          )
                        : (label == "List Test Export")
                            ? ElevatedButton(
                                onPressed: () {
                                  setState(() {
                                    list.remove(item);
                                  });
                                },
                                child: const Icon(Icons.remove),
                              )
                            : const SizedBox.shrink(),
                  ),
                ],
              ),
          ],
        ),
        if (label == "List Test Search")
          DynamicTablePagging(
            rowCountListTestCase,
            currentPageTestCase,
            rowPerPageTestCase,
            pageChangeHandler: (page) {
              getListTfTestCase(page);
            },
            rowPerPageChangeHandler: (rowPerPage) {
              setState(() {
                rowPerPageTestCase = rowPerPage!;
                getListTfTestCase(1);
              });
            },
          ),
      ],
    );
  }

  Column tableTestsExportWidget(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MultiLanguageText(
          name: "list_test_export",
          defaultValue: "List Test Export",
          style: Style(context).styleTitleDialog,
        ),
        CustomDataTableWidget(
          minWidth: 500,
          columns: [
            Expanded(
              child: MultiLanguageText(
                name: "type",
                defaultValue: "Type",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "code",
                defaultValue: "Code",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              flex: 2,
              child: MultiLanguageText(
                name: "name",
                defaultValue: "Name",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Container(
              width: 56,
            ),
          ],
          rows: [
            for (var testExport in testsExport)
              CustomDataRow(
                cells: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        testExport["testCaseType"],
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        testExport["name"],
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        testExport["title"] ?? "",
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 56,
                    height: 35,
                    child: ElevatedButton(
                      onPressed: () {
                        setState(() {
                          testsExport.remove(testExport);
                        });
                      },
                      child: const Icon(Icons.remove),
                    ),
                  ),
                ],
              ),
          ],
        ),
      ],
    );
  }

  Padding headerImportWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 50, 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MultiLanguageText(
            name: "import_multiple_test",
            defaultValue: "Import Multiple Test",
            style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
          ),
          const SizedBox(
            height: 10,
          ),
          ElevatedButton(
            onPressed: () async {
              fileTestCase = await selectFile(['json']);
              if (fileTestCase != null) {
                uploadFile(fileTestCase, "/test-framework-api/user/project-file/prj-project/$prjProjectId/tf-test-case/import");
              }
            },
            child: const MultiLanguageText(name: "import", defaultValue: "Import"),
          ),
        ],
      ),
    );
  }

  Padding headerExportWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 10, 50, 20),
      child: HeaderExportTestWidget(
        callbackFilter: (text) {
          search(text);
        },
      ),
    );
  }

  uploadFile(PlatformFile? file, String url) async {
    final request = http.MultipartRequest(
      "POST",
      Uri.parse(
        "$baseUrl$url",
      ),
    );
    request.headers.addAll({"Authorization": Provider.of<SecurityModel>(context, listen: false).authorization!});
    request.files.add(http.MultipartFile.fromBytes('file', await readFile(file!), filename: file.name));
    var resp = await request.send();
    var responseDecode = jsonDecode(await resp.stream.bytesToString());
    if (responseDecode.containsKey("errorMessage") && mounted) {
      showToast(context: context, msg: responseDecode["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
    } else if (mounted) {
      showToast(
          context: context,
          msg: multiLanguageString(name: "upload_success", defaultValue: "Upload success", context: context),
          color: Colors.greenAccent,
          icon: const Icon(Icons.done));
    }
  }
}
