import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/custom_state.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/badge/badge_icon.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import '../../../../components/toast.dart';

class RunBatchWidget extends StatefulWidget {
  final String id;
  final String prjProjectId;
  final String? testRunid;
  const RunBatchWidget({Key? key, required this.id, required this.prjProjectId, this.testRunid}) : super(key: key);

  @override
  State<RunBatchWidget> createState() => _RunBatchWidgetState();
}

class _RunBatchWidgetState extends CustomState<RunBatchWidget> {
  late Future future;
  var resultTestBatch = {};
  TextEditingController scheduleDate = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  dynamic _testsVersion;
  dynamic _testsEnvironment;
  var _listVersions = [];
  var _listEnvironment = [];
  @override
  void initState() {
    super.initState();
    future = getInitPage();
  }

  getInitPage() async {
    scheduleDate.text = DateFormat('dd-MM-yyyy HH:mm:ss').format(DateTime.now());
    await getVersionList();
    await getEnvironmentList();
    await getTestBatch(widget.id);
    if (widget.testRunid != null) {
      getReRun();
    }
    return 0;
  }

  getReRun() async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-batch-run/${widget.testRunid}", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        // scheduleDate.text =
        //     DateFormat('dd-MM-yyyy HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(response["body"]["result"]["scheduledDate"]).toLocal());
        if (_listVersions.any((element) => element["id"] == response["body"]["result"]["tfTestVersionId"])) {
          _testsVersion = response["body"]["result"]["tfTestVersion"];
        }
        if (_listEnvironment.any((element) => element["id"] == response["body"]["result"]["tfTestEnvironmentId"])) {
          _testsEnvironment = response["body"]["result"]["tfTestEnvironment"];
        }
      });
    }
  }

  getTestBatch(String id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-batch/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        resultTestBatch = response["body"]["result"];
      });
    }
  }

  _selectDate(TextEditingController controller) async {
    final DateTime? datePicked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (datePicked != null && mounted) {
      final TimeOfDay? timePicked = await showTimePicker(
        context: context,
        initialTime: TimeOfDay.now(),
      );
      if (timePicked != null) {
        setState(() {
          controller.text =
              "${DateFormat('dd-MM-yyyy').format(datePicked)} ${timePicked.hour.toString().padLeft(2, '0')}:${timePicked.minute.toString().padLeft(2, '0')}";
        });
      }
    }
  }

  getVersionList() async {
    var findRequest = {"prjProjectId": widget.prjProjectId, "orderBy": "startDate DESC", "status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-version/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listVersions = response["body"]["resultList"];
        if (_listVersions.isNotEmpty) {
          var versionItem = _listVersions.firstWhere(
            (element) => element["defaultVersion"] == 1,
            orElse: () => null,
          );
          _testsVersion = versionItem ?? _listVersions[0];
        }
      });
    }
  }

  getEnvironmentList() async {
    var findRequest = {"prjProjectId": widget.prjProjectId, "orderBy": "startDate DESC", "status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-environment/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listEnvironment = response["body"]["resultList"];
        if (_listEnvironment.isNotEmpty) {
          var environmentItem = _listEnvironment.firstWhere(
            (element) => element["defaultEnvironment"] == 1,
            orElse: () => null,
          );
          _testsEnvironment = environmentItem ?? _listEnvironment[0];
        }
      });
    }
  }

  handlePlayButton() async {
    var requestBody = {
      "scheduledDate": "${DateFormat('dd-MM-yyyy HH:mm').parse(scheduleDate.text).toIso8601String()}+07:00",
      "tfTestBatchId": widget.id,
      "tfTestVersionId": _testsVersion["id"],
      "tfTestEnvironmentId": _testsEnvironment["id"],
      "status": 0
    };
    var response = await httpPut("/test-framework-api/user/test-framework/tf-test-batch-run", requestBody, context);
    if (response.containsKey("body") && response["body"] is String == false && response["body"].containsKey("errorMessage") && mounted) {
      showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      return false;
    } else {
      return true;
    }
  }

  @override
  void dispose() {
    scheduleDate.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, LanguageModel>(
      builder: (context, navigationModel, languageModel, child) {
        return FutureBuilder(
          future: future,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(50, 20, 50, 30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [headerWidget(navigationModel), testBatchInformation(), btnWidget(navigationModel)],
                    ),
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  Widget btnWidget(NavigationModel navigationModel) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          height: 40,
          width: 80,
          child: ElevatedButton(
            onPressed: () async {
              if (_formKey.currentState!.validate()) {
                var bool = await handlePlayButton();
                if (bool) {
                  navigationModel.navigate("/qa-platform/project/${navigationModel.prjProjectId}/runs/test_batch");
                }
              }
            },
            child: const MultiLanguageText(
              name: "run",
              defaultValue: "Run",
              isLowerCase: false,
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
        const SizedBox(
          width: 10,
        ),
        SizedBox(
          height: 40,
          width: 100,
          child: ElevatedButton(
            onPressed: () {
              Navigator.pop(context);
            },
            style: ElevatedButton.styleFrom(
              side: const BorderSide(color: Color.fromRGBO(209, 212, 216, 1)),
              backgroundColor: Colors.white,
              foregroundColor: const Color.fromRGBO(61, 90, 254, 1),
            ),
            child: const MultiLanguageText(
              name: "cancel",
              defaultValue: "Cancel",
              isLowerCase: false,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ],
    );
  }

  Widget testBatchInformation() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "code",
                  defaultValue: "Code",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(130, 130, 130, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
              Expanded(
                flex: 8,
                child: Text(
                  resultTestBatch["name"] ?? "",
                  softWrap: true,
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(49, 49, 49, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "name",
                  defaultValue: "Name",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(130, 130, 130, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
              Expanded(
                flex: 8,
                child: Text(
                  resultTestBatch["title"] ?? "",
                  softWrap: true,
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(49, 49, 49, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "batch_type",
                  defaultValue: "Batch type",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(130, 130, 130, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
              Expanded(
                flex: 8,
                child: Text(
                  resultTestBatch["batchType"],
                  softWrap: true,
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(49, 49, 49, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "owner",
                  defaultValue: "Owner",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(130, 130, 130, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
              Expanded(
                flex: 8,
                child: Row(
                  children: [
                    const Icon(Icons.person),
                    Text(
                      resultTestBatch["sysUser"]["fullname"] ?? "",
                      softWrap: true,
                      style: TextStyle(
                        fontSize: context.fontSizeManager.fontSizeText16,
                        color: const Color.fromRGBO(49, 49, 49, 1),
                        fontWeight: FontWeight.w500,
                        letterSpacing: 1.5,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "create _date",
                  defaultValue: "Create date",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(130, 130, 130, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
              Expanded(
                flex: 8,
                child: (resultTestBatch["createDate"] != null)
                    ? Text(
                        "${DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultTestBatch["createDate"]).toLocal())} ${DateFormat('HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultTestBatch["createDate"]).toLocal())}",
                        softWrap: true,
                        style: TextStyle(
                          fontSize: context.fontSizeManager.fontSizeText16,
                          color: const Color.fromRGBO(49, 49, 49, 1),
                          fontWeight: FontWeight.w500,
                          letterSpacing: 1.5,
                        ),
                      )
                    : const SizedBox.shrink(),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "status",
                  defaultValue: "Status",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(130, 130, 130, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
              Expanded(
                flex: 8,
                child: Row(
                  children: [
                    IntrinsicWidth(
                      child: Badge2StatusSettingWidget(
                        status: resultTestBatch["status"],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          if (resultTestBatch["description"] != null)
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 10,
                ),
                MultiLanguageText(
                  name: "description",
                  defaultValue: "Description",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(49, 49, 49, 1),
                    fontWeight: FontWeight.w600,
                    letterSpacing: 1.5,
                  ),
                ),
                Text(
                  resultTestBatch["description"],
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(49, 49, 49, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ],
            ),
          const SizedBox(
            height: 10,
          ),
          Form(
            key: _formKey,
            child: Row(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                    child: DynamicDropdownSearchClearOption(
                      hint: multiLanguageString(name: "choose_a_version", defaultValue: "Choose a version", context: context),
                      labelText: multiLanguageString(name: "version", defaultValue: "Version", context: context),
                      controller: SingleValueDropDownController(data: DropDownValueModel(name: _testsVersion["name"], value: _testsVersion["id"])),
                      items: _listVersions.map<DropDownValueModel>((dynamic result) {
                        return DropDownValueModel(
                          value: result["id"],
                          name: result["name"],
                        );
                      }).toList(),
                      maxHeight: 200,
                      onChanged: (dynamic newValue) {
                        _formKey.currentState!.validate();
                        setState(() {
                          _testsVersion = _listVersions.firstWhere((element) => element["id"] == newValue.value);
                        });
                      },
                      isRequiredNotEmpty: true,
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                    child: DynamicDropdownSearchClearOption(
                      hint: multiLanguageString(name: "choose_an_environment", defaultValue: "Choose an environment", context: context),
                      labelText: multiLanguageString(name: "environment", defaultValue: "Environment", context: context),
                      controller:
                          SingleValueDropDownController(data: DropDownValueModel(name: _testsEnvironment["name"], value: _testsEnvironment["id"])),
                      items: _listEnvironment.map<DropDownValueModel>((dynamic result) {
                        return DropDownValueModel(
                          value: result["id"],
                          name: result["name"],
                        );
                      }).toList(),
                      maxHeight: 200,
                      onChanged: (dynamic newValue) {
                        _formKey.currentState!.validate();
                        setState(() {
                          _testsEnvironment = _listEnvironment.firstWhere((element) => element["id"] == newValue.value);
                        });
                      },
                      isRequiredNotEmpty: true,
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(0, 8, 0, 8),
                    child: DynamicTextField(
                      controller: scheduleDate,
                      suffixIcon: const Icon(Icons.calendar_today),
                      hintText: multiLanguageString(name: "select_schedule_time", defaultValue: "Select schedule time", context: context),
                      labelText: multiLanguageString(name: "schedule_time", defaultValue: "Schedule time", context: context),
                      readOnly: true,
                      onTap: () {
                        _selectDate(scheduleDate);
                      },
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget headerWidget(NavigationModel navigationModel) {
    return Row(
      children: [
        IconButton(
          onPressed: () {
            navigationModel.pop("/project/${navigationModel.prjProjectId}/test-batch");
          },
          icon: const Icon(Icons.arrow_back),
          padding: EdgeInsets.zero,
          splashRadius: 16,
        ),
        Expanded(
          child: Text(
            resultTestBatch["title"],
            style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
          ),
        ),
      ],
    );
  }
}
