import 'dart:async';

import 'package:flutter/material.dart';

import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/components/badge/badge_icon.dart';
import 'package:test_framework_view/components/badge/get_last_run.dart';
import 'package:test_framework_view/components/dialog/dialog_delete.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/components/header_setting.dart';
import 'package:test_framework_view/type.dart';

import '../../../../../../api.dart';
import '../../../../../../model/model.dart';
import '../../../../../common/custom_state.dart';
import 'option_test_batch_widget.dart';

class TestBatchWidget extends StatefulWidget {
  const TestBatchWidget({Key? key}) : super(key: key);

  @override
  State<TestBatchWidget> createState() => _TestBatchWidgetState();
}

class _TestBatchWidgetState extends CustomState<TestBatchWidget> {
  late Future futureListTestBatch;
  var resultListTestBatch = [];
  var rowCountListTestBatch = 0;
  var currentPage = 1;
  var rowPerPage = 10;
  var searchRequest = {};
  var prjProjectId = "";

  @override
  void initState() {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    prjProjectId = navigationModel.prjProjectId ?? "";
    search({});
    super.initState();
  }

  search(searchRequest) {
    this.searchRequest = searchRequest;
    futureListTestBatch = getListTestBatch(currentPage);
  }

  Future getListTestBatch(page) async {
    if ((page - 1) * rowPerPage > rowCountListTestBatch) {
      page = (1.0 * rowCountListTestBatch / rowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {
      "queryOffset": (page - 1) * rowPerPage,
      "queryLimit": rowPerPage,
      "prjProjectId": prjProjectId,
    };
    if (searchRequest["titleLike"] != null && searchRequest["titleLike"].isNotEmpty) {
      findRequest["titleLike"] = "%${searchRequest["titleLike"]}%";
    }
    if (searchRequest["status"] != null) {
      findRequest["status"] = searchRequest["status"];
    }
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-batch/search", findRequest, context);

    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPage = page;
        rowCountListTestBatch = response["body"]["rowCount"];
        resultListTestBatch = response["body"]["resultList"];
      });
    }
    return 0;
  }

  handleDeleteTestBatch(id) async {
    var response = await httpDelete("/test-framework-api/user/test-framework/tf-test-batch/$id", context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        await getListTestBatch(currentPage);
      }
    }
  }

  handleAddTestBatch(result) async {
    var response = await httpPut("/test-framework-api/user/test-framework/tf-test-batch", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await getListTestBatch(currentPage);
        return response["body"]["result"];
      }
    }
    return false;
  }

  handleEditTestBatch(result, id) async {
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-batch/$id", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await getListTestBatch(currentPage);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, LanguageModel>(
      builder: (context, navigationModel, languageModel, child) {
        return FutureBuilder(
          future: futureListTestBatch,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Stack(
                children: [
                  ListView(
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(50, 20, 50, 50),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            NavMenuTabV1(
                              currentUrl: "/test-list/test_batch",
                              margin: const EdgeInsets.only(bottom: 20),
                              navigationList: context.testMenuList,
                            ),
                            HeaderSettingWidget(
                              type: "Test batch",
                              title: multiLanguageString(name: "test_batch", defaultValue: "Test batch", context: context),
                              countList: rowCountListTestBatch,
                              callbackSearch: (result) {
                                search(result);
                              },
                            ),
                            if (resultListTestBatch.isNotEmpty)
                              SelectionArea(
                                child: Container(
                                  margin: const EdgeInsets.only(top: 30),
                                  child: tableTestBatch(navigationModel),
                                ),
                              ),
                            SelectionArea(
                              child: DynamicTablePagging(
                                rowCountListTestBatch,
                                currentPage,
                                rowPerPage,
                                pageChangeHandler: (page) {
                                  setState(() {
                                    futureListTestBatch = getListTestBatch(page);
                                  });
                                },
                                rowPerPageChangeHandler: (rowPerPage) {
                                  setState(() {
                                    this.rowPerPage = rowPerPage!;
                                    futureListTestBatch = getListTestBatch(1);
                                  });
                                },
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  Positioned(bottom: 10, right: 40, child: btnWidget(navigationModel)),
                ],
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  SingleChildScrollView btnWidget(NavigationModel navigationModel) {
    return SingleChildScrollView(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            margin: const EdgeInsets.only(right: 10),
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              onPressed: () {
                Navigator.of(context).push(
                  createRoute(
                    OptionTestBatchWidget(
                      type: "create",
                      title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                      callbackOptionTestBatch: (result) async {
                        dynamic resultFunction = await handleAddTestBatch(result);
                        if (resultFunction != false) {
                          navigationModel
                              .navigate("/qa-platform/project/${navigationModel.prjProjectId}/test-list/test_batch/$resultFunction/batch-step");
                        }
                      },
                      prjProjectId: prjProjectId,
                    ),
                  ),
                );
              },
              icon: const Icon(Icons.note_add, color: Color.fromRGBO(49, 49, 49, 1)),
              label: MultiLanguageText(
                name: "new_test_batch",
                defaultValue: "New Test Batch",
                isLowerCase: false,
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
            ),
          ),
        ],
      ),
    );
  }

  dialogDeleteTestBatch(selectItem) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogDelete(
          selectedItem: selectItem,
          callback: () {
            handleDeleteTestBatch(selectItem["id"]);
          },
          type: 'TestBatch',
        );
      },
    );
  }

  Widget tableTestBatch(NavigationModel navigationModel) {
    return CustomDataTableWidget(
      columns: [
        Expanded(
          flex: 2,
          child: MultiLanguageText(
            name: "name",
            defaultValue: "Name",
            isLowerCase: false,
            style: Style(context).styleTextDataColumn,
          ),
        ),
        Expanded(
          child: Center(
            child: MultiLanguageText(
              name: "batch_steps",
              defaultValue: "Batch steps",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
        ),
        Expanded(
          child: MultiLanguageText(
            name: "owner",
            defaultValue: "Owner",
            isLowerCase: false,
            style: Style(context).styleTextDataColumn,
          ),
        ),
        Expanded(
          child: MultiLanguageText(
            name: "create_date",
            defaultValue: "Create date",
            isLowerCase: false,
            style: Style(context).styleTextDataColumn,
          ),
        ),
        Expanded(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              MultiLanguageText(
                name: "status",
                defaultValue: "Status",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
              Tooltip(
                message: multiLanguageString(name: "active_inactive", defaultValue: "Green = Active\nRed = Inactive ", context: context),
                child: const Icon(
                  Icons.info,
                  color: Colors.grey,
                  size: 16,
                ),
              ),
            ],
          ),
        ),
        Expanded(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              MultiLanguageText(
                name: "last_run",
                defaultValue: "Last run",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
              Tooltip(
                message: multiLanguageString(
                    name: "run_pass_run_fail_running_new_warning",
                    defaultValue: "Green = Run pass\nRed = Run fail\nOrange = Running\nBlue = New\nYellow = Warning",
                    context: context),
                child: const Icon(
                  Icons.info,
                  color: Colors.grey,
                  size: 16,
                ),
              ),
            ],
          ),
        ),
        Container(
          width: 38,
        ),
      ],
      rows: [
        for (var resultTestBatch in resultListTestBatch)
          CustomDataRow(
            cells: [
              Expanded(
                flex: 2,
                child: Padding(
                  padding: const EdgeInsets.all(5),
                  child: Row(
                    children: [
                      const Icon(Icons.description, color: Color.fromRGBO(61, 148, 254, 1)),
                      const SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: Text(
                          resultTestBatch["title"] ?? "",
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: Style(context).styleTextDataCell,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: GestureDetector(
                    onTap: () {
                      navigationModel
                          .navigate("/qa-platform/project/${navigationModel.prjProjectId}/test-list/test_batch/${resultTestBatch["id"]}/batch-step");
                    },
                    child: QuantityBatchStepWidget(key: Key(resultTestBatch["id"]), tfTestBatchId: resultTestBatch["id"])),
              ),
              Expanded(
                child: Text(
                  resultTestBatch["sysUser"]["fullname"],
                  style: Style(context).styleTextDataCell,
                ),
              ),
              Expanded(
                child: (resultTestBatch["createDate"] != null)
                    ? Text(
                        "${DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultTestBatch["createDate"]).toLocal())} ${DateFormat('HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultTestBatch["createDate"]).toLocal())}",
                        softWrap: true,
                        style: TextStyle(
                          fontSize: context.fontSizeManager.fontSizeText16,
                          color: const Color.fromRGBO(49, 49, 49, 1),
                          fontWeight: FontWeight.w500,
                          letterSpacing: 1.5,
                        ),
                      )
                    : const SizedBox.shrink(),
              ),
              Expanded(
                child: Center(
                  child: Badge2StatusSettingWidget(
                    status: resultTestBatch["status"],
                  ),
                ),
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    InkWell(
                      hoverColor: Colors.transparent,
                      onTap: () {
                        navigationModel.navigate(
                          "/qa-platform/project/${navigationModel.prjProjectId}/runs/test_batch/${resultTestBatch["id"]}",
                        );
                      },
                      child: GetLastTestBatchRun(
                        key: ValueKey(resultTestBatch["id"]),
                        idProject: prjProjectId,
                        id: resultTestBatch["id"],
                      ),
                    ),
                  ],
                ),
              ),
              PopupMenuButton(
                offset: const Offset(5, 50),
                tooltip: '',
                splashRadius: 10,
                itemBuilder: (context) => [
                  PopupMenuItem(
                    padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                    child: ListTile(
                      onTap: () {
                        navigationModel
                            .navigate("/qa-platform/project/${navigationModel.prjProjectId}/test-list/test_batch/${resultTestBatch["id"]}/batch-run");
                      },
                      contentPadding: const EdgeInsets.all(0),
                      hoverColor: Colors.transparent,
                      title: const MultiLanguageText(name: "run", defaultValue: "Run"),
                      leading: const Icon(Icons.play_arrow, color: Color.fromARGB(255, 112, 114, 119)),
                    ),
                  ),
                  PopupMenuItem(
                    padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                    child: ListTile(
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.of(context).push(
                          createRoute(
                            OptionTestBatchWidget(
                              type: "edit",
                              title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                              id: resultTestBatch["id"],
                              callbackOptionTestBatch: (result) {
                                handleEditTestBatch(result, resultTestBatch["id"]);
                              },
                              prjProjectId: prjProjectId,
                            ),
                          ),
                        );
                      },
                      contentPadding: const EdgeInsets.all(0),
                      hoverColor: Colors.transparent,
                      title: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                      leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
                    ),
                  ),
                  PopupMenuItem(
                    padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                    child: ListTile(
                      onTap: () {
                        Navigator.pop(context);
                        dialogDeleteTestBatch(resultTestBatch);
                      },
                      contentPadding: const EdgeInsets.all(0),
                      hoverColor: Colors.transparent,
                      title: const MultiLanguageText(name: "delete", defaultValue: "Delete"),
                      leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                    ),
                  ),
                ],
              ),
            ],
            onSelectChanged: () {
              navigationModel.navigate("/qa-platform/project/${navigationModel.prjProjectId}/test-list/test_batch/${resultTestBatch["id"]}");
            },
          ),
      ],
    );
  }
}

class QuantityBatchStepWidget extends StatefulWidget {
  final String tfTestBatchId;
  const QuantityBatchStepWidget({Key? key, required this.tfTestBatchId}) : super(key: key);

  @override
  State<QuantityBatchStepWidget> createState() => _QuantityBatchStepWidgetState();
}

class _QuantityBatchStepWidgetState extends State<QuantityBatchStepWidget> {
  int rowCount = 0;
  @override
  void initState() {
    super.initState();
    getCount();
  }

  getCount() async {
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-batch-step/search",
        {"tfTestBatchId": widget.tfTestBatchId, "queryLimit": 0, "queryOffset": 0}, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      setState(() {
        rowCount = response["body"]["rowCount"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Icon(
          Icons.description,
          color: Color.fromRGBO(61, 148, 254, 1),
          size: 16,
        ),
        const SizedBox(
          width: 10,
        ),
        Text(
          " $rowCount",
          style: Style(context).styleTextDataCell,
        ),
      ],
    );
  }
}
