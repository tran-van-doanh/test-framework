import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/custom_state.dart';
import 'package:test_framework_view/components/badge/badge_icon.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/list_test/batch/test_batch/option_test_batch_widget.dart';
import '../../../../components/menu_tab.dart';

class SettingTestBatchDetailWidget extends StatefulWidget {
  final String id;
  final String prjProjectId;
  const SettingTestBatchDetailWidget({Key? key, required this.id, required this.prjProjectId}) : super(key: key);

  @override
  State<SettingTestBatchDetailWidget> createState() => _SettingTestBatchDetailWidgetState();
}

class _SettingTestBatchDetailWidgetState extends CustomState<SettingTestBatchDetailWidget> {
  late Future future;
  var resultTestBatch = {};
  @override
  void initState() {
    super.initState();
    future = getInitPage();
  }

  getInitPage() async {
    await getTestBatch(widget.id);
    return 0;
  }

  getTestBatch(String id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-batch/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        resultTestBatch = response["body"]["result"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<NavigationModel>(
      builder: (context, navigationModel, child) {
        return FutureBuilder(
          future: future,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView(
                children: [
                  NavMenuTabV1(
                    currentUrl: "",
                    margin: const EdgeInsets.fromLTRB(50, 20, 50, 0),
                    navigationList: context.testBatchMenuList,
                    preUrl: "/test-list/test_batch/${widget.id}",
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(50, 20, 50, 30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        headerWidget(navigationModel),
                        testBatchInformation(),
                        if (resultTestBatch["testBatchConfig"] != null)
                          BatchStepConfigTableWidget(
                            testBatchConfig: resultTestBatch["testBatchConfig"],
                            isEdit: false,
                          )
                      ],
                    ),
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  Widget testBatchInformation() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: SelectionArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Expanded(
                  flex: 2,
                  child: MultiLanguageText(
                    name: "id",
                    defaultValue: "Id",
                    style: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText16,
                      color: const Color.fromRGBO(130, 130, 130, 1),
                      fontWeight: FontWeight.w500,
                      letterSpacing: 1.5,
                    ),
                  ),
                ),
                Expanded(
                  flex: 8,
                  child: Text(
                    resultTestBatch["id"] ?? "",
                    softWrap: true,
                    style: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText16,
                      color: const Color.fromRGBO(49, 49, 49, 1),
                      fontWeight: FontWeight.w500,
                      letterSpacing: 1.5,
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Expanded(
                  flex: 2,
                  child: MultiLanguageText(
                    name: "code",
                    defaultValue: "Code",
                    style: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText16,
                      color: const Color.fromRGBO(130, 130, 130, 1),
                      fontWeight: FontWeight.w500,
                      letterSpacing: 1.5,
                    ),
                  ),
                ),
                Expanded(
                  flex: 8,
                  child: Text(
                    resultTestBatch["name"] ?? "",
                    softWrap: true,
                    style: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText16,
                      color: const Color.fromRGBO(49, 49, 49, 1),
                      fontWeight: FontWeight.w500,
                      letterSpacing: 1.5,
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Expanded(
                  flex: 2,
                  child: MultiLanguageText(
                    name: "name",
                    defaultValue: "Name",
                    style: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText16,
                      color: const Color.fromRGBO(130, 130, 130, 1),
                      fontWeight: FontWeight.w500,
                      letterSpacing: 1.5,
                    ),
                  ),
                ),
                Expanded(
                  flex: 8,
                  child: Text(
                    resultTestBatch["title"] ?? "",
                    softWrap: true,
                    style: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText16,
                      color: const Color.fromRGBO(49, 49, 49, 1),
                      fontWeight: FontWeight.w500,
                      letterSpacing: 1.5,
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Expanded(
                  flex: 2,
                  child: MultiLanguageText(
                    name: "batch_type",
                    defaultValue: "Batch type",
                    style: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText16,
                      color: const Color.fromRGBO(130, 130, 130, 1),
                      fontWeight: FontWeight.w500,
                      letterSpacing: 1.5,
                    ),
                  ),
                ),
                Expanded(
                  flex: 8,
                  child: Text(
                    resultTestBatch["batchType"],
                    softWrap: true,
                    style: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText16,
                      color: const Color.fromRGBO(49, 49, 49, 1),
                      fontWeight: FontWeight.w500,
                      letterSpacing: 1.5,
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Expanded(
                  flex: 2,
                  child: MultiLanguageText(
                    name: "owner",
                    defaultValue: "Owner",
                    style: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText16,
                      color: const Color.fromRGBO(130, 130, 130, 1),
                      fontWeight: FontWeight.w500,
                      letterSpacing: 1.5,
                    ),
                  ),
                ),
                Expanded(
                  flex: 8,
                  child: Row(
                    children: [
                      const Icon(Icons.person),
                      Text(
                        resultTestBatch["sysUser"]["fullname"] ?? "",
                        softWrap: true,
                        style: TextStyle(
                          fontSize: context.fontSizeManager.fontSizeText16,
                          color: const Color.fromRGBO(49, 49, 49, 1),
                          fontWeight: FontWeight.w500,
                          letterSpacing: 1.5,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Expanded(
                  flex: 2,
                  child: MultiLanguageText(
                    name: "create_date",
                    defaultValue: "Create date",
                    style: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText16,
                      color: const Color.fromRGBO(130, 130, 130, 1),
                      fontWeight: FontWeight.w500,
                      letterSpacing: 1.5,
                    ),
                  ),
                ),
                Expanded(
                  flex: 8,
                  child: (resultTestBatch["createDate"] != null)
                      ? Text(
                          "${DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultTestBatch["createDate"]).toLocal())} ${DateFormat('HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultTestBatch["createDate"]).toLocal())}",
                          softWrap: true,
                          style: TextStyle(
                            fontSize: context.fontSizeManager.fontSizeText16,
                            color: const Color.fromRGBO(49, 49, 49, 1),
                            fontWeight: FontWeight.w500,
                            letterSpacing: 1.5,
                          ),
                        )
                      : const SizedBox.shrink(),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Expanded(
                  flex: 2,
                  child: MultiLanguageText(
                    name: "status",
                    defaultValue: "Status",
                    style: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText16,
                      color: const Color.fromRGBO(130, 130, 130, 1),
                      fontWeight: FontWeight.w500,
                      letterSpacing: 1.5,
                    ),
                  ),
                ),
                Expanded(
                  flex: 8,
                  child: Row(
                    children: [
                      IntrinsicWidth(
                        child: Badge2StatusSettingWidget(
                          status: resultTestBatch["status"],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            if (resultTestBatch["description"] != null)
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 10,
                  ),
                  MultiLanguageText(
                    name: "description",
                    defaultValue: "Description",
                    style: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText16,
                      color: const Color.fromRGBO(49, 49, 49, 1),
                      fontWeight: FontWeight.w600,
                      letterSpacing: 1.5,
                    ),
                  ),
                  Text(
                    resultTestBatch["description"],
                    style: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText16,
                      color: const Color.fromRGBO(49, 49, 49, 1),
                      fontWeight: FontWeight.w500,
                      letterSpacing: 1.5,
                    ),
                  ),
                ],
              ),
          ],
        ),
      ),
    );
  }

  Widget headerWidget(NavigationModel navigationModel) {
    return Row(
      children: [
        IconButton(
          onPressed: () {
            navigationModel.navigate("/qa-platform/project/${navigationModel.prjProjectId}/test-list/test_batch");
          },
          icon: const Icon(Icons.arrow_back),
          padding: EdgeInsets.zero,
          splashRadius: 16,
        ),
        Expanded(
          child: Text(
            resultTestBatch["title"],
            style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
          ),
        ),
      ],
    );
  }
}
