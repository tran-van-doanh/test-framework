import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/common/row_code_with_tooltip.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';
import '../../../../../common/custom_state.dart';
import '../../../../api.dart';

class OptionTestBatchWidget extends StatefulWidget {
  final String type;
  final String title;
  final String prjProjectId;
  final String? id;
  final Function callbackOptionTestBatch;
  const OptionTestBatchWidget({
    Key? key,
    required this.type,
    required this.prjProjectId,
    required this.callbackOptionTestBatch,
    this.id,
    required this.title,
  }) : super(key: key);

  @override
  State<OptionTestBatchWidget> createState() => _OptionTestBatchWidgetState();
}

class _OptionTestBatchWidgetState extends CustomState<OptionTestBatchWidget> {
  late Future future;
  TextEditingController idController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController codeController = TextEditingController();
  TextEditingController desController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  int _status = 0;
  dynamic selectedItem;
  var testBatchConfig = {};

  @override
  void initState() {
    future = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    if (widget.id != null) {
      await getSelectedItem(widget.id!);
      await setInitValue(selectedItem);
    }
    return 0;
  }

  getSelectedItem(String id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-batch/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        selectedItem = response["body"]["result"];
      });
    }
  }

  setInitValue(item) async {
    setState(() {
      _status = selectedItem["status"] ?? 0;
      nameController.text = item["title"] ?? "";
      idController.text = item["id"] ?? "";
      codeController.text = item["name"] ?? "";
      desController.text = item["description"] ?? "";
      testBatchConfig = item["testBatchConfig"] ?? {};
    });
  }

  @override
  void dispose() {
    desController.dispose();
    nameController.dispose();
    idController.dispose();
    codeController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return TestFrameworkRootPageWidget(
        child: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            return FutureBuilder(
              future: future,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: ListView(
                          padding: const EdgeInsets.fromLTRB(50, 30, 50, 10),
                          children: [
                            headerWidget(context),
                            batchTypeCreateForm(),
                          ],
                        ),
                      ),
                      btnWidget(context),
                    ],
                  );
                } else if (snapshot.hasError) {
                  return Text('${snapshot.error}');
                }
                return const Center(child: CircularProgressIndicator());
              },
            );
          },
        ),
      );
    });
  }

  Padding btnWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: 40,
            width: 80,
            child: ElevatedButton(
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  var result = {};
                  if (selectedItem != null) {
                    result = selectedItem;
                    result["description"] = desController.text;
                    result["title"] = nameController.text;
                    result["name"] = codeController.text;
                    result["status"] = _status;
                    result["testBatchConfig"] = testBatchConfig;
                  } else {
                    result = {
                      "prjProjectId": widget.prjProjectId,
                      "description": desController.text,
                      "title": nameController.text,
                      "name": codeController.text,
                      "batchType": "parallel",
                      "status": _status,
                      "testBatchConfig": testBatchConfig,
                    };
                  }
                  widget.callbackOptionTestBatch(result);
                }
              },
              child: const MultiLanguageText(
                name: "ok",
                defaultValue: "Ok",
                isLowerCase: false,
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 10),
            height: 40,
            width: 120,
            child: ElevatedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              style: ElevatedButton.styleFrom(
                side: const BorderSide(color: Color.fromRGBO(209, 212, 216, 1)),
                backgroundColor: Colors.white,
                foregroundColor: const Color.fromRGBO(61, 90, 254, 1),
              ),
              child: const MultiLanguageText(
                name: "cancel",
                defaultValue: "Cancel",
                isLowerCase: false,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          )
        ],
      ),
    );
  }

  Row headerWidget(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        MultiLanguageText(
          name: "type_test_batch",
          defaultValue: "\$0 TEST BATCH",
          variables: [widget.title],
          isLowerCase: false,
          style: Style(context).styleTitleDialog,
        ),
        TextButton(
          onPressed: () => Navigator.pop(context, 'cancel'),
          child: const Icon(
            Icons.close,
            color: Colors.black,
          ),
        ),
      ],
    );
  }

  Widget batchTypeCreateForm() {
    return Form(
      key: _formKey,
      child: Container(
        margin: const EdgeInsets.only(top: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (widget.type != "create")
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: Text("Id", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicTextField(
                        controller: idController,
                        hintText: "...",
                        readOnly: true,
                      ),
                    ),
                  ],
                ),
              ),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Expanded(
                    flex: 3,
                    child: RowCodeWithTooltipWidget(),
                  ),
                  Expanded(
                    flex: 7,
                    child: DynamicTextField(
                      controller: codeController,
                      hintText: "...",
                      isRequiredNotEmpty: true,
                      enabled: widget.type != "edit",
                      isRequiredRegex: true,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 3,
                    child: MultiLanguageText(name: "name", defaultValue: "Name", style: Style(context).styleTitleDialog),
                  ),
                  Expanded(
                    flex: 7,
                    child: DynamicTextField(
                      controller: nameController,
                      hintText: "...",
                      isRequiredNotEmpty: true,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 3,
                    child: MultiLanguageText(name: "status", defaultValue: "Status", style: Style(context).styleTitleDialog),
                  ),
                  Expanded(
                    flex: 7,
                    child: DynamicDropdownButton(
                        borderRadius: 5,
                        value: _status,
                        hint: multiLanguageString(name: "choose_a_status", defaultValue: "Choose a status", context: context),
                        items: [
                          {"name": multiLanguageString(name: "active", defaultValue: "Active", context: context), "value": 1},
                          {"name": multiLanguageString(name: "inactive", defaultValue: "Inactive", context: context), "value": 0},
                        ].map<DropdownMenuItem<int>>((dynamic result) {
                          return DropdownMenuItem(
                            value: result["value"],
                            child: Text(result["name"]),
                          );
                        }).toList(),
                        onChanged: (newValue) {
                          setState(() {
                            _status = newValue!;
                          });
                        },
                        isRequiredNotEmpty: true),
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 3,
                    child: MultiLanguageText(name: "description", defaultValue: "Description", style: Style(context).styleTitleDialog),
                  ),
                  Expanded(
                    flex: 7,
                    child: DynamicTextField(
                      controller: desController,
                      hintText: "...",
                      maxline: 3,
                    ),
                  ),
                ],
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 10,
                ),
                MultiLanguageText(name: "config_batch_results", defaultValue: "Config batch results", style: Style(context).styleTitleDialog),
                BatchStepConfigTableWidget(
                  testBatchConfig: testBatchConfig,
                  isEdit: true,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class BatchStepConfigTableWidget extends StatefulWidget {
  final dynamic testBatchConfig;
  final bool isEdit;
  const BatchStepConfigTableWidget({
    Key? key,
    this.testBatchConfig,
    required this.isEdit,
  }) : super(key: key);

  @override
  State<BatchStepConfigTableWidget> createState() => _BatchStepConfigTableWidgetState();
}

class _BatchStepConfigTableWidgetState extends State<BatchStepConfigTableWidget> {
  List columnList = [
    {"key": "pass Rate", "name": "pass_rate_%", "value": "Pass Rate %"},
    {"key": "warning Rate", "name": "warning_rate_%", "value": "Warning Rate %"},
  ];
  List rowList = [
    {"name": "critical", "value": "Critical"},
    {"name": "major", "value": "Major"},
    {"name": "average", "value": "Average"},
    {"name": "minor", "value": "Minor"},
    {"name": "trivial", "value": "Trivial"},
  ];
  @override
  Widget build(BuildContext context) {
    return CustomDataTableWidget(
      columns: [
        Expanded(
          flex: 1,
          child: MultiLanguageText(
            name: "severity",
            defaultValue: "Severity",
            isLowerCase: false,
            style: Style(context).styleTextDataColumn,
          ),
        ),
        for (var column in columnList)
          Expanded(
            flex: 4,
            child: MultiLanguageText(
              name: column["name"],
              defaultValue: column["value"],
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
      ],
      rows: [
        for (var row in rowList)
          CustomDataRow(
            padding: widget.isEdit ? const EdgeInsets.all(0) : const EdgeInsets.all(10),
            cells: [
              Expanded(
                flex: 1,
                child: Padding(
                  padding: widget.isEdit ? const EdgeInsets.symmetric(horizontal: 10) : const EdgeInsets.symmetric(horizontal: 5),
                  child: MultiLanguageText(
                    name: row["name"],
                    defaultValue: row["value"],
                    isLowerCase: false,
                    style: Style(context).styleTextDataCell,
                  ),
                ),
              ),
              for (var column in columnList)
                Expanded(
                  flex: 4,
                  child: widget.isEdit
                      ? Container(
                          decoration: const BoxDecoration(
                            border: Border(
                              left: BorderSide(color: Color.fromRGBO(196, 200, 223, 1)),
                            ),
                          ),
                          child: DynamicTextField(
                            enabledBorder: const OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.transparent,
                              ),
                            ),
                            inputFormatters: <TextInputFormatter>[
                              FilteringTextInputFormatter.allow(RegExp(r'[0-9]+[.]{0,1}[0-9]*')),
                            ],
                            keyboardType: const TextInputType.numberWithOptions(decimal: true),
                            controller:
                                TextEditingController(text: (widget.testBatchConfig[column["key"].replaceAll(" ", row["value"])] ?? 0).toString()),
                            hintText: multiLanguageString(
                                name: "enter_a_value_for", defaultValue: "Enter a value for \$1", variables: [column["value"]], context: context),
                            onChanged: (text) {
                              widget.testBatchConfig[column["key"].replaceAll(" ", row["value"])] = double.parse(text);
                            },
                          ),
                        )
                      : Text(
                          (widget.testBatchConfig[column["key"].replaceAll(" ", row["value"])] ?? 0).toString(),
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: Style(context).styleTextDataCell,
                        ),
                ),
            ],
          )
      ],
    );
  }
}
