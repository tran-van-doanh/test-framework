import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/custom_state.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/components/badge/badge_icon.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import '../../../../components/style.dart';

class SettingBatchStepDetailWidget extends StatefulWidget {
  final String id;
  final String prjProjectId;
  const SettingBatchStepDetailWidget({Key? key, required this.id, required this.prjProjectId}) : super(key: key);

  @override
  State<SettingBatchStepDetailWidget> createState() => _SettingBatchStepDetailWidgetState();
}

class _SettingBatchStepDetailWidgetState extends CustomState<SettingBatchStepDetailWidget> {
  late Future future;
  var resultBatchStep = {};
  var parameterFieldList = [];
  List<Map<String, String>> listMapPriority = [];
  List<Map<String, String>> listMapSeverity = [];
  @override
  void initState() {
    super.initState();
    future = getInitPage();
    listMapPriority = [
      {"value": "1", "name": multiLanguageString(name: "item_highest", defaultValue: "Highest", context: context)},
      {"value": "2", "name": multiLanguageString(name: "item_high", defaultValue: "High", context: context)},
      {"value": "3", "name": multiLanguageString(name: "item_medium", defaultValue: "Medium", context: context)},
      {"value": "4", "name": multiLanguageString(name: "item_low", defaultValue: "Low", context: context)},
      {"value": "5", "name": multiLanguageString(name: "item_lowest", defaultValue: "Lowest", context: context)},
    ];
    listMapSeverity = [
      {"value": "1", "name": multiLanguageString(name: "item_critical", defaultValue: "Critical", context: context)},
      {"value": "2", "name": multiLanguageString(name: "item_major", defaultValue: "Major", context: context)},
      {"value": "3", "name": multiLanguageString(name: "item_average", defaultValue: "Average", context: context)},
      {"value": "4", "name": multiLanguageString(name: "item_minor", defaultValue: "Minor", context: context)},
      {"value": "5", "name": multiLanguageString(name: "item_trivial", defaultValue: "Trivial", context: context)},
    ];
  }

  getInitPage() async {
    await getBatchStep(widget.id);
    await getTestCase(resultBatchStep["tfTestCase"]["id"]);
    return 0;
  }

  getBatchStep(String id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-batch-step/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        resultBatchStep = response["body"]["result"];
      });
      await getProfile();
    }
  }

  getProfile() async {
    for (var client in resultBatchStep["testRunConfig"]["clientList"]) {
      var response = await httpGet("/test-framework-api/user/test-framework/tf-test-profile/${client['tfTestProfileId']}", context);
      if (response.containsKey("body") && response["body"] is String == false) {
        client["profile"] = response["body"]["result"];
        setState(() {});
      }
    }
  }

  getTestCase(id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-case/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      if (response["body"]["result"]["testCaseType"] == "test_case") {
        getTestCase(response["body"]["result"]["parentId"]);
      } else {
        setState(() {
          parameterFieldList = response["body"]["result"]["content"]["parameterFieldList"] ?? [];
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<NavigationModel>(
      builder: (context, navigationModel, child) {
        return FutureBuilder(
          future: future,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(50, 20, 50, 30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        headerWidget(navigationModel),
                        batchStepInformation(),
                      ],
                    ),
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  Widget profileTable() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MultiLanguageText(
          name: "profile",
          defaultValue: "Profile",
          style: Style(context).styleTitleDialog,
        ),
        const SizedBox(
          height: 5,
        ),
        CustomDataTableWidget(
          columns: [
            Expanded(
              flex: 2,
              child: MultiLanguageText(
                name: "client_name",
                defaultValue: "Client name",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              flex: 2,
              child: MultiLanguageText(
                name: "profile_name",
                defaultValue: "Profile name",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "default",
                defaultValue: "Default",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              flex: 2,
              child: MultiLanguageText(
                name: "operating_system",
                defaultValue: "Operating system",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "browser",
                defaultValue: "Browser",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "resolution",
                defaultValue: "Resolution",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
          ],
          rows: [
            for (var client in resultBatchStep["testRunConfig"]["clientList"])
              CustomDataRow(
                cells: [
                  Expanded(
                    flex: 2,
                    child: Text(
                      client["name"],
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Row(
                        children: [
                          const Icon(Icons.description, color: Color.fromRGBO(61, 148, 254, 1)),
                          const SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Text(
                              client["profile"]["title"] ?? "",
                              maxLines: 3,
                              overflow: TextOverflow.ellipsis,
                              style: Style(context).styleTextDataCell,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Text(
                      client["profile"]["defaultProfile"] == 0 ? "Not default" : "Default",
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Text(
                      client["profile"]["tfOperatingSystem"]["title"],
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 5),
                      child: Text(
                        client["profile"]["tfBrowser"] != null && client["profile"]["tfBrowserVersion"] != null
                            ? ("${client["profile"]["tfBrowser"]["title"]} ( ${client["profile"]["tfBrowserVersion"]["title"]} )")
                            : "",
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Text(
                      "${client["profile"]["screenWidth"]} X ${client["profile"]["screenHeight"]}",
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                ],
              ),
          ],
        ),
        const SizedBox(
          height: 20,
        )
      ],
    );
  }

  Widget testRunDataTable() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 10,
        ),
        MultiLanguageText(
          name: "test_run_data",
          defaultValue: "Test run data",
          style: Style(context).styleTitleDialog,
        ),
        const SizedBox(
          height: 5,
        ),
        CustomDataTableWidget(
          columns: [
            if (resultBatchStep["tfTestCase"]["testCaseType"] == "test_case")
              Expanded(
                child: MultiLanguageText(
                  name: "test_case",
                  defaultValue: "Test case",
                  style: Style(context).styleTextDataColumn,
                ),
              ),
            if (resultBatchStep["tfTestCase"]["testCaseType"] == "test_case")
              Expanded(
                child: MultiLanguageText(
                  name: "severity",
                  defaultValue: "Severity",
                  style: Style(context).styleTextDataColumn,
                ),
              ),
            if (resultBatchStep["tfTestCase"]["testCaseType"] == "test_case")
              Expanded(
                child: MultiLanguageText(
                  name: "priority",
                  defaultValue: "Priority",
                  style: Style(context).styleTextDataColumn,
                ),
              ),
            for (var parameterField in parameterFieldList)
              Expanded(
                child: Text(
                  parameterField["name"],
                  style: Style(context).styleTextDataColumn,
                ),
              ),
          ],
          rows: [
            for (var testRunData in resultBatchStep["testRunData"])
              CustomDataRow(
                cells: [
                  if (resultBatchStep["tfTestCase"]["testCaseType"] == "test_case")
                    Expanded(
                      child: Text(
                        resultBatchStep["tfTestCase"]["name"],
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  if (resultBatchStep["tfTestCase"]["testCaseType"] == "test_case")
                    Expanded(
                      child: Text(
                        "${listMapSeverity.firstWhere((map) => map["value"] == testRunData["severity"], orElse: () => {'name': 'Null'})["name"]}",
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  if (resultBatchStep["tfTestCase"]["testCaseType"] == "test_case")
                    Expanded(
                      child: Text(
                        "${listMapPriority.firstWhere((map) => map["value"] == testRunData["priority"], orElse: () => {'name': 'Null'})["name"]}",
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  for (var parameterField in parameterFieldList)
                    Expanded(
                      child: Text(
                        testRunData[parameterField["name"]] ?? "",
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                ],
              ),
          ],
        ),
        const SizedBox(
          height: 20,
        )
      ],
    );
  }

  Widget batchStepInformation() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "code",
                  defaultValue: "Code",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(130, 130, 130, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
              Expanded(
                flex: 8,
                child: Text(
                  resultBatchStep["name"] ?? "Not Defined",
                  softWrap: true,
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(49, 49, 49, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "name",
                  defaultValue: "Name",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(130, 130, 130, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
              Expanded(
                flex: 8,
                child: Text(
                  resultBatchStep["title"] ?? "Not Defined",
                  softWrap: true,
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(49, 49, 49, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "test_name",
                  defaultValue: "Test name",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(130, 130, 130, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
              Expanded(
                flex: 8,
                child: Text(
                  resultBatchStep["tfTestCase"]["title"] ?? "Not Defined",
                  softWrap: true,
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(49, 49, 49, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "test_run_type",
                  defaultValue: "Test run type",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(130, 130, 130, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
              Expanded(
                flex: 8,
                child: Text(
                  resultBatchStep["testRunType"],
                  softWrap: true,
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(49, 49, 49, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "owner",
                  defaultValue: "Owner",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(130, 130, 130, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
              Expanded(
                flex: 8,
                child: Row(
                  children: [
                    const Icon(Icons.person),
                    Text(
                      resultBatchStep["sysUser"]["fullname"] ?? "",
                      softWrap: true,
                      style: TextStyle(
                        fontSize: context.fontSizeManager.fontSizeText16,
                        color: const Color.fromRGBO(49, 49, 49, 1),
                        fontWeight: FontWeight.w500,
                        letterSpacing: 1.5,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "create_date",
                  defaultValue: "Create date",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(130, 130, 130, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
              Expanded(
                flex: 8,
                child: (resultBatchStep["createDate"] != null)
                    ? Text(
                        "${DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultBatchStep["createDate"]).toLocal())} ${DateFormat('HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultBatchStep["createDate"]).toLocal())}",
                        softWrap: true,
                        style: TextStyle(
                          fontSize: context.fontSizeManager.fontSizeText16,
                          color: const Color.fromRGBO(49, 49, 49, 1),
                          fontWeight: FontWeight.w500,
                          letterSpacing: 1.5,
                        ),
                      )
                    : const SizedBox.shrink(),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "status",
                  defaultValue: "Status",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(130, 130, 130, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
              Expanded(
                flex: 8,
                child: Row(
                  children: [
                    IntrinsicWidth(
                      child: Badge2StatusSettingWidget(
                        status: resultBatchStep["status"],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          if (resultBatchStep["description"] != null)
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 10,
                ),
                MultiLanguageText(
                  name: "description",
                  defaultValue: "Description",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(49, 49, 49, 1),
                    fontWeight: FontWeight.w600,
                    letterSpacing: 1.5,
                  ),
                ),
                Text(
                  resultBatchStep["description"],
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(49, 49, 49, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ],
            ),
          profileTable(),
          if (resultBatchStep["testRunData"] != null && resultBatchStep["testRunData"].isNotEmpty) testRunDataTable()
        ],
      ),
    );
  }

  Widget headerWidget(NavigationModel navigationModel) {
    return Row(
      children: [
        IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(Icons.arrow_back),
          padding: EdgeInsets.zero,
          splashRadius: 16,
        ),
        Expanded(
          child: Text(
            resultBatchStep["title"] ?? "Not Defined",
            style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
          ),
        ),
      ],
    );
  }
}
