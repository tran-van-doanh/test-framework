import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:http/http.dart' as http;
import 'package:test_framework_view/page/guide/sidebar_guide.dart';
import '../../../../api.dart';
import '../../../../common/custom_state.dart';
import '../../../../components/dynamic_play_form.dart';
import '../../../../components/toast.dart';
import '../../../../config.dart';
import '../../../../type.dart';
import '../../../../components/dialog/dialog_choose_test.dart';
import 'package:test_framework_view/common/file_download.dart';

class OptionBatchStepWidget extends StatefulWidget {
  final String type;
  final String title;
  final String prjProjectId;
  final String tfTestBatchId;
  final String? id;
  final Function callbackBatchStep;
  const OptionBatchStepWidget({
    Key? key,
    required this.type,
    required this.prjProjectId,
    required this.callbackBatchStep,
    this.id,
    required this.tfTestBatchId,
    required this.title,
  }) : super(key: key);

  @override
  State<OptionBatchStepWidget> createState() => _OptionBatchStepWidgetState();
}

class _OptionBatchStepWidgetState extends CustomState<OptionBatchStepWidget> {
  late Future futureBatchStep;
  List resultClientList = [];
  String? testCaseNameSelected;
  TextEditingController nameController = TextEditingController();
  TextEditingController codeController = TextEditingController();
  TextEditingController desController = TextEditingController();
  String? testCaseRequiredText;
  String? testCaseId;
  final _formKey = GlobalKey<FormState>();
  int _status = 0;
  bool isViewColumn = false;
  PlatformFile? objFile;
  List<Map<dynamic, dynamic>> listValueParameter = [];
  List resultparameterFieldList = [];
  dynamic selectedItem;
  String testCaseTypeCheck = "";
  bool isFirstSetName = true;
  @override
  void initState() {
    futureBatchStep = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    if (widget.id != null) {
      await getSelectedItem(widget.id!);
      await setInitValue(selectedItem);
    } else {
      listValueParameter = [
        {"severity": "3", "priority": "3"}
      ];
    }
    return 0;
  }

  getSelectedItem(String id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-batch-step/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        selectedItem = response["body"]["result"];
      });
    }
  }

  setInitValue(item) async {
    setState(() {
      _status = selectedItem["status"] ?? 0;
      testCaseId = item["tfTestCase"]["id"];
      nameController.text = item["title"] ?? "";
      codeController.text = item["name"] ?? "";
      desController.text = item["description"] ?? "";
      listValueParameter = item["testRunData"];
    });
    if (testCaseId != null) {
      await getTestCase(testCaseId!);
      for (var client in resultClientList) {
        await getProfileList(client);
        setState(() {
          client["tfTestProfileId"] = selectedItem["testRunConfig"]["clientList"].firstWhere(
            (element) => element["id"] == client["id"],
            orElse: () => {"tfTestProfileId": null},
          )["tfTestProfileId"];
        });
      }
    }
  }

  Future getTestCase(String testCaseId) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-case/$testCaseId", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      if (isFirstSetName) {
        setState(() {
          testCaseNameSelected = response["body"]["result"]["title"];
          isFirstSetName = false;
        });
      }
      if (response["body"]["result"]["testCaseType"] == "test_case") {
        await getTestCase(response["body"]["result"]["parentId"]);
        setState(() {
          testCaseTypeCheck = "test-case";
        });
      } else {
        setState(() {
          resultClientList = response["body"]["result"]["content"]["clientList"] ?? [];
          resultparameterFieldList = response["body"]["result"]["content"]["parameterFieldList"] ?? [];
        });
      }
      setState(() {
        isFirstSetName = true;
      });
    }
  }

  getProfileList(client) async {
    var findRequest = {
      "prjProjectId": Provider.of<NavigationModel>(context, listen: false).prjProjectId,
      "status": 1,
      "platform": client["platform"]
    };
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-profile/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        client["profileList"] = response["body"]["resultList"];
      });
    }
  }

  getProfileDefault(client) async {
    var response = await httpGet(
        "/test-framework-api/user/project/prj-project/${Provider.of<NavigationModel>(context, listen: false).prjProjectId}/tf-test-profile/${client["platform"]}/default",
        context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        client["tfTestProfileId"] = response["body"]["result"]["id"];
      });
    }
  }

  downloadFile(id) async {
    File? downloadedFile = await httpDownloadFile(context, "/test-framework-api/user/test-framework-file/tf-test-case/$id/generate-file",
        Provider.of<SecurityModel>(context, listen: false).authorization, 'Output.xlsx');
    if (downloadedFile != null && mounted) {
      var snackBar = SnackBar(
        content: MultiLanguageText(name: "file_downloaded_to", defaultValue: "File downloaded to \$0 .", variables: ["$downloadedFile"]),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  uploadFile() async {
    final request = http.MultipartRequest(
      "POST",
      Uri.parse(
        "$baseUrl/test-framework-api/user/test-framework-file/read-file",
      ),
    );
    request.headers.addAll({"Authorization": Provider.of<SecurityModel>(context, listen: false).authorization!});
    request.files.add(http.MultipartFile.fromBytes('file', await readFile(objFile!), filename: objFile!.name));
    var resp = await request.send();
    var responseDecode = jsonDecode(await resp.stream.bytesToString());
    if (responseDecode.containsKey("errorMessage") && mounted) {
      showToast(context: context, msg: responseDecode["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
    } else if (mounted) {
      showToast(
          context: context,
          msg: multiLanguageString(name: "upload_success", defaultValue: "Upload success", context: context),
          color: Colors.greenAccent,
          icon: const Icon(Icons.done));
    }
  }

  handleSubmitButton() async {
    for (var valueParameter in listValueParameter) {
      for (var parameterField in resultparameterFieldList) {
        if (valueParameter[parameterField["name"]] == null) {
          valueParameter[parameterField["name"]] = "";
        }
      }
    }
    var clientListBodyRequest = [];
    for (var element in resultClientList) {
      var clientNew = Map.from(element);
      clientNew.remove("profileList");
      clientListBodyRequest.add(clientNew);
    }
    var result = {};
    if (selectedItem != null) {
      result = selectedItem;
      result["tfTestCaseId"] = testCaseId;
      result["description"] = desController.text;
      result["title"] = nameController.text;
      result["name"] = codeController.text;
      result["testRunConfig"]["clientList"] = clientListBodyRequest;
      result["testRunData"] = listValueParameter;
      result["tfTestBatchId"] = widget.tfTestBatchId;
      result["status"] = _status;
    } else {
      result = {
        "prjProjectId": widget.prjProjectId,
        "testRunType": "parallel",
        "description": desController.text,
        "testRunConfig": {"clientList": clientListBodyRequest},
        "tfTestCaseId": testCaseId,
        "title": nameController.text,
        "name": codeController.text,
        "tfTestBatchId": widget.tfTestBatchId,
        "testRunData": listValueParameter,
        "status": _status,
      };
    }
    widget.callbackBatchStep(result);
  }

  @override
  void dispose() {
    desController.dispose();
    nameController.dispose();
    codeController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return TestFrameworkRootPageWidget(
        child: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            return FutureBuilder(
              future: futureBatchStep,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: ListView(
                          padding: const EdgeInsets.fromLTRB(50, 30, 50, 10),
                          children: [
                            headerWidget(context),
                            batchStepForm(),
                          ],
                        ),
                      ),
                      btnWidget(context),
                    ],
                  );
                } else if (snapshot.hasError) {
                  return Text('${snapshot.error}');
                }
                return const Center(child: CircularProgressIndicator());
              },
            );
          },
        ),
      );
    });
  }

  Padding btnWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: 40,
            width: 80,
            child: ElevatedButton(
              onPressed: () {
                if (_formKey.currentState!.validate() && testCaseId != null) {
                  handleSubmitButton();
                } else if (testCaseId == null) {
                  setState(() {
                    testCaseRequiredText = multiLanguageString(name: "please_choose_a_test", defaultValue: "Please choose a test", context: context);
                  });
                }
              },
              child: const MultiLanguageText(
                name: "ok",
                defaultValue: "Ok",
                isLowerCase: false,
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 10),
            height: 40,
            width: 120,
            child: ElevatedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              style: ElevatedButton.styleFrom(
                side: const BorderSide(color: Color.fromRGBO(209, 212, 216, 1)),
                backgroundColor: Colors.white,
                foregroundColor: const Color.fromRGBO(61, 90, 254, 1),
              ),
              child: const MultiLanguageText(
                name: "cancel",
                defaultValue: "Cancel",
                isLowerCase: false,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          )
        ],
      ),
    );
  }

  Row headerWidget(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        MultiLanguageText(
          name: "type_batch_step",
          defaultValue: "\$0 BATCH STEP",
          variables: [widget.title],
          isLowerCase: false,
          style: Style(context).styleTitleDialog,
        ),
        TextButton(
          onPressed: () => Navigator.pop(context, 'cancel'),
          child: const Icon(
            Icons.close,
            color: Colors.black,
          ),
        ),
      ],
    );
  }

  Widget batchStepForm() {
    return Form(
      key: _formKey,
      child: Container(
        margin: const EdgeInsets.only(top: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    SizedBox(
                      height: 45,
                      child: ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).push(
                            createRoute(
                              DialogChooseTest(
                                prjProjectId: widget.prjProjectId,
                                testCaseType: "test_case",
                                callbackSelectedTestCase: (testCaseId) async {
                                  setState(() {
                                    this.testCaseId = testCaseId;
                                    testCaseRequiredText = null;
                                  });
                                  var response = await httpGet("/test-framework-api/user/test-framework/tf-test-case/$testCaseId", context);
                                  if (response.containsKey("body") && response["body"] is String == false) {
                                    if (isFirstSetName) {
                                      setState(() {
                                        testCaseNameSelected = response["body"]["result"]['title'];
                                        isFirstSetName = false;
                                        testCaseTypeCheck = "test-case";
                                      });
                                    }
                                    await getTestCase(response["body"]["result"]["parentId"]);
                                    setState(() {
                                      var testRunData = response["body"]["result"]["content"]["testRunData"];
                                      listValueParameter = <Map<dynamic, dynamic>>[...testRunData ?? []];
                                    });
                                    for (var client in resultClientList) {
                                      await getProfileList(client);
                                      await getProfileDefault(client);
                                    }
                                  }
                                },
                              ),
                            ),
                          );
                        },
                        child: const MultiLanguageText(
                          name: "select_test_case",
                          defaultValue: "Select test case",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    SizedBox(
                      height: 45,
                      child: ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).push(
                            createRoute(
                              DialogChooseTest(
                                prjProjectId: widget.prjProjectId,
                                testCaseType: "test_scenario",
                                callbackSelectedTestCase: (testCaseId) async {
                                  setState(() {
                                    this.testCaseId = testCaseId;
                                    testCaseRequiredText = null;
                                  });
                                  setState(() {
                                    testCaseTypeCheck = "test_scenario";
                                  });
                                  await getTestCase(this.testCaseId!);
                                  for (var client in resultClientList) {
                                    await getProfileList(client);
                                    await getProfileDefault(client);
                                  }
                                },
                              ),
                            ),
                          );
                        },
                        child: const MultiLanguageText(
                          name: "select_test_scenario",
                          defaultValue: "Select test scenario",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    SizedBox(
                      height: 45,
                      child: ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).push(
                            createRoute(
                              DialogChooseTest(
                                prjProjectId: widget.prjProjectId,
                                testCaseType: "test_suite",
                                callbackSelectedTestCase: (testCaseId) async {
                                  setState(() {
                                    this.testCaseId = testCaseId;
                                    testCaseRequiredText = null;
                                  });
                                  setState(() {
                                    testCaseTypeCheck = "test_suite";
                                  });
                                  await getTestCase(this.testCaseId!);
                                  for (var client in resultClientList) {
                                    await getProfileList(client);
                                    await getProfileDefault(client);
                                  }
                                },
                              ),
                            ),
                          );
                        },
                        child: const MultiLanguageText(
                          name: "select_test_suite",
                          defaultValue: "Select test suite",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
                if (testCaseId != null)
                  Row(
                    children: [
                      Container(
                        margin: const EdgeInsets.only(right: 10),
                        child: SizedBox(
                          height: 40,
                          child: FloatingActionButton.extended(
                            heroTag: null,
                            onPressed: () async {
                              downloadFile(testCaseId);
                            },
                            icon: const FaIcon(FontAwesomeIcons.fileArrowDown, color: Color.fromRGBO(49, 49, 49, 1)),
                            label: MultiLanguageText(
                              name: "download_excel",
                              defaultValue: "Download Excel",
                              style: TextStyle(
                                  fontSize: context.fontSizeManager.fontSizeText13,
                                  fontWeight: FontWeight.w600,
                                  color: const Color.fromRGBO(49, 49, 49, 1)),
                            ),
                            backgroundColor: const Color.fromRGBO(216, 218, 229, .7),
                            elevation: 0,
                            hoverElevation: 0,
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(right: 10),
                        child: SizedBox(
                          height: 40,
                          child: FloatingActionButton.extended(
                            heroTag: null,
                            onPressed: () async {
                              objFile = await selectFile(['xlsx']);
                              if (objFile != null) {
                                await uploadFile();
                              }
                            },
                            icon: const FaIcon(FontAwesomeIcons.fileArrowUp, color: Color.fromRGBO(49, 49, 49, 1)),
                            label: MultiLanguageText(
                              name: "upload_excel",
                              defaultValue: "Upload Excel",
                              style: TextStyle(
                                  fontSize: context.fontSizeManager.fontSizeText13,
                                  fontWeight: FontWeight.w600,
                                  color: const Color.fromRGBO(49, 49, 49, 1)),
                            ),
                            backgroundColor: const Color.fromRGBO(216, 218, 229, .7),
                            elevation: 0,
                            hoverElevation: 0,
                          ),
                        ),
                      ),
                      isViewColumn
                          ? SizedBox(
                              height: 40,
                              child: FloatingActionButton(
                                heroTag: null,
                                onPressed: () async {
                                  setState(() {
                                    isViewColumn = false;
                                  });
                                },
                                backgroundColor: const Color.fromARGB(176, 125, 141, 231),
                                elevation: 0,
                                hoverElevation: 0,
                                child: const Icon(Icons.view_column, color: Color.fromRGBO(49, 49, 49, 1)),
                              ),
                            )
                          : SizedBox(
                              height: 40,
                              child: FloatingActionButton(
                                heroTag: null,
                                onPressed: () async {
                                  setState(() {
                                    isViewColumn = true;
                                  });
                                },
                                backgroundColor: const Color.fromARGB(176, 125, 141, 231),
                                elevation: 0,
                                hoverElevation: 0,
                                child: const Icon(Icons.view_stream, color: Color.fromRGBO(49, 49, 49, 1)),
                              ),
                            ),
                    ],
                  )
              ],
            ),
            if (testCaseNameSelected != null)
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: MultiLanguageText(
                  name: "test_test_case_name_selected",
                  defaultValue: "Test : \$0",
                  variables: [testCaseNameSelected!],
                  style: TextStyle(fontSize: context.fontSizeManager.fontSizeText22, fontWeight: FontWeight.w500),
                ),
              ),
            if (testCaseRequiredText != null)
              Text(
                testCaseRequiredText!,
                style: const TextStyle(color: Colors.red),
              ),
            if (testCaseId != null)
              Theme(
                data: ThemeData().copyWith(dividerColor: Colors.transparent),
                child: ExpansionTile(
                  initiallyExpanded: true,
                  tilePadding: const EdgeInsets.all(0),
                  childrenPadding: const EdgeInsets.all(10),
                  title: MultiLanguageText(
                    name: "choose_profile_for_the_clients",
                    defaultValue: "Choose profile for the clients",
                    style: TextStyle(fontSize: context.fontSizeManager.fontSizeText20, fontWeight: FontWeight.w500),
                  ),
                  children: <Widget>[
                    for (var client in resultClientList)
                      if (client["profileList"] != null && client["profileList"].isNotEmpty)
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  child: DynamicDropdownButton(
                                    value: (client["profileList"] != null && client["profileList"].isNotEmpty) ? client["tfTestProfileId"] : null,
                                    hint: multiLanguageString(name: "choose_a_profile", defaultValue: "Choose a profile", context: context),
                                    items: (client["profileList"] as List).map<DropdownMenuItem<String>>((dynamic result) {
                                      return DropdownMenuItem(
                                        value: result["id"],
                                        child: Text(result["title"]),
                                      );
                                    }).toList(),
                                    onChanged: (newValue) {
                                      setState(() {
                                        client["tfTestProfileId"] = newValue!;
                                      });
                                    },
                                    labelText: multiLanguageString(
                                        name: "client_client_name", defaultValue: "Client \$0", variables: ["${client["name"]}"], context: context),
                                    borderRadius: 5,
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            )
                          ],
                        ),
                  ],
                ),
              ),
            const SizedBox(
              height: 10,
            ),
            Column(
              children: [
                Container(
                  alignment: Alignment.centerLeft,
                  padding: const EdgeInsets.all(10),
                  margin: const EdgeInsets.only(bottom: 10),
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: const Color.fromRGBO(212, 212, 212, 1),
                    ),
                    color: Colors.grey[100],
                  ),
                  child: Row(
                    children: [
                      MultiLanguageText(
                        name: "batch_step_information",
                        defaultValue: "Batch step information",
                        isLowerCase: false,
                        style: Style(context).styleTextDataColumn,
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      Tooltip(
                        message: multiLanguageString(name: "metadata_is_not_required", defaultValue: 'Meta data is not required', context: context),
                        child: const Icon(
                          Icons.info,
                          color: Colors.grey,
                          size: 18,
                        ),
                      )
                    ],
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "code", defaultValue: "Code", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicTextField(
                        controller: codeController,
                        hintText: "...",
                        enabled: widget.type != "edit",
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "name", defaultValue: "Name", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicTextField(
                        controller: nameController,
                        hintText: "...",
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "status", defaultValue: "Status", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicDropdownButton(
                        borderRadius: 5,
                        value: _status,
                        hint: multiLanguageString(name: "choose_a_status", defaultValue: "Choose a status", context: context),
                        items: [
                          {"name": multiLanguageString(name: "active", defaultValue: "Active", context: context), "value": 1},
                          {"name": multiLanguageString(name: "inactive", defaultValue: "Inactive", context: context), "value": 0},
                        ].map<DropdownMenuItem<int>>((dynamic result) {
                          return DropdownMenuItem(
                            value: result["value"],
                            child: Text(result["name"]),
                          );
                        }).toList(),
                        onChanged: (newValue) {
                          setState(() {
                            _status = newValue!;
                          });
                        },
                      ),
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MultiLanguageText(name: "description", defaultValue: "Description", style: Style(context).styleTitleDialog),
                DynamicTextField(
                  controller: desController,
                  hintText: "...",
                  maxline: 3,
                ),
              ],
            ),
            if (testCaseNameSelected != null)
              Column(
                children: [
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    padding: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: const Color.fromRGBO(212, 212, 212, 1),
                      ),
                      color: Colors.grey[100],
                    ),
                    child: MultiLanguageText(
                      name: "batch_step_data",
                      defaultValue: "Batch step data",
                      isLowerCase: false,
                      style: Style(context).styleTextDataColumn,
                    ),
                  ),
                  RunBodyParameterWidget(
                    isViewColumn: isViewColumn,
                    listValueParameter: listValueParameter,
                    resultparameterFieldList: resultparameterFieldList,
                    key: UniqueKey(),
                    type: testCaseTypeCheck,
                  ),
                ],
              ),
          ],
        ),
      ),
    );
  }
}
