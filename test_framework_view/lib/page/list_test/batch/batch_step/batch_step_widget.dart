import 'dart:async';

import 'package:flutter/material.dart';

import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/components/badge/badge_icon.dart';
import 'package:test_framework_view/components/dialog/dialog_choose_test_case.dart';
import 'package:test_framework_view/components/dialog/dialog_confirm.dart';
import 'package:test_framework_view/components/dialog/dialog_delete.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/components/header_setting.dart';
import 'package:test_framework_view/type.dart';

import '../../../../../../api.dart';
import '../../../../../../model/model.dart';
import '../../../../../common/custom_state.dart';
import '../../../../components/menu_tab.dart';
import '../batch_step/option_batch_step_widget.dart';

class BatchStepWidget extends StatefulWidget {
  final String testBatchId;

  const BatchStepWidget({Key? key, required this.testBatchId}) : super(key: key);

  @override
  State<BatchStepWidget> createState() => _BatchStepWidgetState();
}

class _BatchStepWidgetState extends CustomState<BatchStepWidget> {
  late Future futureListBatchStep;
  var resultListBatchStep = [];
  var rowCountListBatchStep = 0;
  var currentPage = 1;
  var rowPerPage = 10;
  var searchRequest = {};
  var prjProjectId = "";

  @override
  void initState() {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    prjProjectId = navigationModel.prjProjectId ?? "";
    search({});
    super.initState();
  }

  search(searchRequest) {
    this.searchRequest = searchRequest;
    futureListBatchStep = getListBatchStep(currentPage);
  }

  Future getListBatchStep(page) async {
    if ((page - 1) * rowPerPage > rowCountListBatchStep) {
      page = (1.0 * rowCountListBatchStep / rowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {
      "queryOffset": (page - 1) * rowPerPage,
      "queryLimit": rowPerPage,
      "prjProjectId": prjProjectId,
      "tfTestBatchId": widget.testBatchId
    };
    if (searchRequest["titleLike"] != null && searchRequest["titleLike"].isNotEmpty) {
      findRequest["titleLike"] = "%${searchRequest["titleLike"]}%";
    }
    if (searchRequest["status"] != null) {
      findRequest["status"] = searchRequest["status"];
    }
    if (searchRequest["tfTestCaseRegressionCandidate"] != null) {
      findRequest["tfTestCaseRegressionCandidate"] = searchRequest["tfTestCaseRegressionCandidate"];
    }
    if (searchRequest["tfTestCaseTestPriority"] != null) {
      findRequest["tfTestCaseTestPriority"] = searchRequest["tfTestCaseTestPriority"];
    }
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-batch-step/search", findRequest, context);

    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPage = page;
        rowCountListBatchStep = response["body"]["rowCount"];
        resultListBatchStep = response["body"]["resultList"];
      });
    }
    return 0;
  }

  handleDeleteBatchStep(id) async {
    var response = await httpDelete("/test-framework-api/user/test-framework/tf-test-batch-step/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      if (mounted) {
        if (response["body"].containsKey("errorMessage")) {
          showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
        } else {
          showToast(
              context: context,
              msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
              color: Colors.greenAccent,
              icon: const Icon(Icons.done));
          await getListBatchStep(currentPage);
        }
      }
    }
  }

  handleAddBatchStep(result) async {
    var response = await httpPut("/test-framework-api/user/test-framework/tf-test-batch-step", result, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      if (mounted) {
        if (response["body"].containsKey("errorMessage")) {
          showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
        } else {
          showToast(
              context: context,
              msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
              color: Colors.greenAccent,
              icon: const Icon(Icons.done));
          Navigator.of(context).pop();
          await getListBatchStep(currentPage);
        }
      }
    }
  }

  handleEditBatchStep(result, id) async {
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-batch-step/$id", result, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      if (mounted) {
        if (response["body"].containsKey("errorMessage")) {
          showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
        } else {
          showToast(
              context: context,
              msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
              color: Colors.greenAccent,
              icon: const Icon(Icons.done));
          Navigator.of(context).pop();
          await getListBatchStep(currentPage);
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, LanguageModel>(
      builder: (context, navigationModel, languageModel, child) {
        return FutureBuilder(
          future: futureListBatchStep,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Stack(
                children: [
                  ListView(
                    padding: const EdgeInsets.fromLTRB(50, 20, 50, 50),
                    children: [
                      NavMenuTabV1(
                        currentUrl: "/batch-step",
                        margin: const EdgeInsets.only(bottom: 20),
                        navigationList: context.testBatchMenuList,
                        preUrl: "/test-list/test_batch/${widget.testBatchId}",
                      ),
                      HeaderSettingWidget(
                        type: "Batch Step",
                        title: multiLanguageString(name: "batch_step", defaultValue: "Batch Step", context: context),
                        onBack: () {
                          navigationModel.navigate("/qa-platform/project/${navigationModel.prjProjectId}/test-list/test_batch");
                        },
                        countList: rowCountListBatchStep,
                        callbackSearch: (result) {
                          search(result);
                        },
                        onDisableAll: (result) async {
                          showDialog<String>(
                            context: context,
                            builder: (BuildContext context) {
                              return DialogConfirm(
                                callback: () async {
                                  searchRequest = result;
                                  Map<dynamic, dynamic> findRequest = {
                                    "prjProjectId": prjProjectId,
                                    "tfTestBatchId": widget.testBatchId,
                                  };
                                  findRequest.addAll(searchRequest);
                                  if (searchRequest["titleLike"] != null && searchRequest["titleLike"].isNotEmpty) {
                                    findRequest["titleLike"] = "%${searchRequest["titleLike"]}%";
                                  }
                                  var response =
                                      await httpPost("/test-framework-api/user/test-framework/tf-test-batch-step/disable", findRequest, context);
                                  if (response.containsKey("body") && response["body"] is String == false && mounted) {
                                    if (response["body"].containsKey("errorMessage")) {
                                      showToast(
                                          context: this.context,
                                          msg: response["body"]["errorMessage"],
                                          color: Colors.red,
                                          icon: const Icon(Icons.error));
                                    } else {
                                      futureListBatchStep = getListBatchStep(currentPage);
                                      showToast(
                                          context: this.context,
                                          msg: multiLanguageString(
                                              name: "disable_successful", defaultValue: "Disable successful", context: this.context),
                                          color: Colors.greenAccent,
                                          icon: const Icon(Icons.done));
                                    }
                                  }
                                },
                              );
                            },
                          );
                        },
                      ),
                      SelectionArea(
                        child: Container(
                          margin: const EdgeInsets.only(top: 30),
                          child: tableBatchStep(navigationModel),
                        ),
                      ),
                      SelectionArea(
                        child: DynamicTablePagging(
                          rowCountListBatchStep,
                          currentPage,
                          rowPerPage,
                          pageChangeHandler: (page) {
                            setState(() {
                              futureListBatchStep = getListBatchStep(page);
                            });
                          },
                          rowPerPageChangeHandler: (rowPerPage) {
                            setState(() {
                              this.rowPerPage = rowPerPage!;
                              futureListBatchStep = getListBatchStep(1);
                            });
                          },
                        ),
                      ),
                    ],
                  ),
                  Positioned(bottom: 10, right: 40, child: btnWidget(context)),
                ],
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  SingleChildScrollView btnWidget(BuildContext context) {
    return SingleChildScrollView(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            margin: const EdgeInsets.only(right: 10),
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              onPressed: () {
                Navigator.of(context).push(
                  createRoute(
                    DialogChooseTestCase(
                      prjProjectId: prjProjectId,
                      onCreate: (requestBody) async {
                        var response =
                            await httpPut("/test-framework-api/user/test-framework/tf-test-batch/${widget.testBatchId}/steps", requestBody, context);
                        if (response.containsKey("body") && response["body"] is String == false) {
                          if (mounted) {
                            if (response["body"].containsKey("errorMessage")) {
                              showToast(
                                  context: this.context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
                            } else {
                              showToast(
                                  context: this.context,
                                  msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: this.context),
                                  color: Colors.greenAccent,
                                  icon: const Icon(Icons.done));
                              Navigator.of(this.context).pop();
                              await getListBatchStep(currentPage);
                            }
                          }
                        }
                      },
                    ),
                  ),
                );
              },
              icon: const Icon(Icons.note_add, color: Color.fromRGBO(49, 49, 49, 1)),
              label: MultiLanguageText(
                name: "choose test case",
                defaultValue: "Choose test case",
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
            ),
          ),
          Container(
            margin: const EdgeInsets.only(right: 10),
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              onPressed: () {
                Navigator.of(context).push(
                  createRoute(
                    OptionBatchStepWidget(
                      tfTestBatchId: widget.testBatchId,
                      type: "create",
                      title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                      callbackBatchStep: (result) {
                        handleAddBatchStep(result);
                      },
                      prjProjectId: prjProjectId,
                    ),
                  ),
                );
              },
              icon: const Icon(Icons.note_add, color: Color.fromRGBO(49, 49, 49, 1)),
              label: MultiLanguageText(
                name: "new_batch_Step",
                defaultValue: "NEW BATCH STEP",
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
            ),
          ),
        ],
      ),
    );
  }

  dialogDeleteBatchStep(selectItem) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogDelete(
          selectedItem: selectItem,
          callback: () {
            handleDeleteBatchStep(selectItem["id"]);
          },
          type: 'BatchStep',
        );
      },
    );
  }

  Widget tableBatchStep(NavigationModel navigationModel) {
    return CustomDataTableWidget(
      columns: [
        Expanded(
          flex: 2,
          child: MultiLanguageText(
            name: "name",
            defaultValue: "Name",
            isLowerCase: false,
            style: Style(context).styleTextDataColumn,
          ),
        ),
        Expanded(
          flex: 2,
          child: MultiLanguageText(
            name: "test_name",
            defaultValue: "Test name",
            isLowerCase: false,
            style: Style(context).styleTextDataColumn,
          ),
        ),
        Expanded(
          child: MultiLanguageText(
            name: "test_run_type",
            defaultValue: "Test Run Type",
            isLowerCase: false,
            style: Style(context).styleTextDataColumn,
          ),
        ),
        Expanded(
          child: MultiLanguageText(
            name: "owner",
            defaultValue: "Owner",
            isLowerCase: false,
            style: Style(context).styleTextDataColumn,
          ),
        ),
        Expanded(
          child: MultiLanguageText(
            name: "create_date",
            defaultValue: "Create date",
            isLowerCase: false,
            style: Style(context).styleTextDataColumn,
          ),
        ),
        Expanded(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              MultiLanguageText(
                name: "status",
                defaultValue: "Status",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
              Tooltip(
                message: multiLanguageString(name: "active_inactive", defaultValue: "Green = Active\nRed = Inactive", context: context),
                child: const Icon(
                  Icons.info,
                  color: Colors.grey,
                  size: 16,
                ),
              ),
            ],
          ),
        ),
        Container(
          width: 38,
        ),
      ],
      rows: [
        for (var resultBatchStep in resultListBatchStep)
          CustomDataRow(
            cells: [
              Expanded(
                flex: 2,
                child: Row(
                  children: [
                    const Icon(Icons.description, color: Color.fromRGBO(61, 148, 254, 1)),
                    const SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: Text(
                        resultBatchStep["title"] ?? "",
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 2,
                child: Text(
                  resultBatchStep["tfTestCase"]["title"],
                  style: Style(context).styleTextDataCell,
                ),
              ),
              Expanded(
                child: Text(
                  resultBatchStep["testRunType"],
                  style: Style(context).styleTextDataCell,
                ),
              ),
              Expanded(
                child: Text(
                  resultBatchStep["sysUser"]["fullname"],
                  style: Style(context).styleTextDataCell,
                ),
              ),
              Expanded(
                child: (resultBatchStep["createDate"] != null)
                    ? Text(
                        "${DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultBatchStep["createDate"]).toLocal())} ${DateFormat('HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultBatchStep["createDate"]).toLocal())}",
                        softWrap: true,
                        style: TextStyle(
                          fontSize: context.fontSizeManager.fontSizeText16,
                          color: const Color.fromRGBO(49, 49, 49, 1),
                          fontWeight: FontWeight.w500,
                          letterSpacing: 1.5,
                        ),
                      )
                    : const SizedBox.shrink(),
              ),
              Expanded(
                child: Center(
                  child: Badge2StatusSettingWidget(
                    status: resultBatchStep["status"],
                  ),
                ),
              ),
              PopupMenuButton(
                offset: const Offset(5, 50),
                tooltip: '',
                splashRadius: 10,
                itemBuilder: (context) => [
                  PopupMenuItem(
                    padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                    child: ListTile(
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.of(context).push(
                          createRoute(
                            OptionBatchStepWidget(
                              tfTestBatchId: widget.testBatchId,
                              type: "edit",
                              title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                              id: resultBatchStep["id"],
                              callbackBatchStep: (result) {
                                handleEditBatchStep(result, resultBatchStep["id"]);
                              },
                              prjProjectId: prjProjectId,
                            ),
                          ),
                        );
                      },
                      contentPadding: const EdgeInsets.all(0),
                      hoverColor: Colors.transparent,
                      title: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                      leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
                    ),
                  ),
                  PopupMenuItem(
                    padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                    child: ListTile(
                      onTap: () {
                        Navigator.pop(context);
                        dialogDeleteBatchStep(resultBatchStep);
                      },
                      contentPadding: const EdgeInsets.all(0),
                      hoverColor: Colors.transparent,
                      title: const MultiLanguageText(name: "delete", defaultValue: "Delete"),
                      leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                    ),
                  ),
                ],
              ),
            ],
            onSelectChanged: () {
              navigationModel.navigate(
                  "/qa-platform/project/${navigationModel.prjProjectId}/test-list/test_batch/${widget.testBatchId}/batch-step/${resultBatchStep["id"]}");
            },
          ),
      ],
    );
  }
}
