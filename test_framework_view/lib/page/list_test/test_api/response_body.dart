import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';
import '../../../common/custom_state.dart';
import '../../../common/dynamic_dropdown_button.dart';
import '../../../common/dynamic_text_field.dart';
import '../../../components/style.dart';
import '../../../type.dart';
import '../../editor/property_widget/root_property_widget.dart';
import 'package:test_framework_view/components/dynamic_button.dart';

class ResponseBodyWidget extends StatefulWidget {
  final List responseBodyList;
  final GlobalKey<FormState>? keyForm;

  const ResponseBodyWidget({
    Key? key,
    this.keyForm,
    required this.responseBodyList,
  }) : super(key: key);

  @override
  State<ResponseBodyWidget> createState() => _MultiPartFormWidgetState();
}

class _MultiPartFormWidgetState extends CustomState<ResponseBodyWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MultiLanguageText(name: "output", defaultValue: "Output", style: Style(context).styleTitleDialog),
        const SizedBox(
          height: 10,
        ),
        headerWidget(),
        Container(
          constraints: const BoxConstraints(maxHeight: 500),
          child: SingleChildScrollView(
            child: Column(
              children: [
                for (var i = 0; i < widget.responseBodyList.length; i++) responseBodyItemWidget(i),
              ],
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 5),
          alignment: Alignment.center,
          child: FloatingActionButton.extended(
            heroTag: null,
            icon: const Icon(Icons.add),
            label: const MultiLanguageText(name: "add", defaultValue: "Add"),
            onPressed: () {
              var newItem = {"dataType": "String", "name": "", "responsePath": ""};
              Navigator.of(context).push(
                createRoute(
                  ResponseBodyFormWidget(
                    responseBody: newItem,
                    onSave: () {
                      setState(() {
                        widget.responseBodyList.add(newItem);
                      });
                    },
                  ),
                ),
              );
            },
          ),
        ),
      ],
    );
  }

  Widget responseBodyItemWidget(int i) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: DynamicDropdownButton(
              enabled: false,
              value: widget.responseBodyList[i]["dataType"] ?? "String",
              hint: multiLanguageString(name: "choose_a_data_type", defaultValue: "Choose a dataType", context: context),
              items: const ["String", "Integer", "Long", "Double", "Float", "Date", "DateTime", "File", "RandomValue", "Password"]
                  .map<DropdownMenuItem<String>>((String result) {
                return DropdownMenuItem(
                  value: result,
                  child: Text(result),
                );
              }).toList(),
              onChanged: (newValue) {
                setState(() {
                  widget.responseBodyList[i]["dataType"] = newValue;
                });
              },
              borderRadius: 5,
              isRequiredNotEmpty: true,
            ),
          ),
          Expanded(
            child: Container(
              margin: const EdgeInsets.only(left: 2),
              child: DynamicTextField(
                enabled: false,
                fillColor: Colors.transparent,
                controller: TextEditingController(text: widget.responseBodyList[i]["name"] ?? ""),
                hintText: multiLanguageString(name: "enter_a_name", defaultValue: "Enter a name", context: context),
                onChanged: (text) {
                  widget.responseBodyList[i]["name"] = text;
                },
                isRequiredNotEmpty: true,
                maxline: 1,
              ),
            ),
          ),
          Expanded(
            child: Container(
              margin: const EdgeInsets.only(left: 2),
              child: DynamicTextField(
                enabled: false,
                fillColor: Colors.transparent,
                controller: TextEditingController(text: widget.responseBodyList[i]["responsePath"] ?? ""),
                hintText: multiLanguageString(name: "enter_a_response_path", defaultValue: "Enter a response path", context: context),
                onChanged: (text) {
                  widget.responseBodyList[i]["responsePath"] = text;
                },
                isRequiredNotEmpty: true,
                maxline: 1,
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 5),
            padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: const BorderRadius.all(Radius.circular(5.0)),
            ),
            height: 48,
            child: Row(
              children: [
                Container(
                  width: 45,
                  height: 25,
                  margin: const EdgeInsets.only(right: 7),
                  child: Tooltip(
                    message: multiLanguageString(name: "move_up", defaultValue: "Move up", context: context),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(backgroundColor: Colors.blue, padding: const EdgeInsets.symmetric(horizontal: 11)),
                      onPressed: () {
                        setState(() {
                          if (i > 0) {
                            widget.responseBodyList.insert(i - 1, widget.responseBodyList[i]);
                            widget.responseBodyList.removeAt(i + 1);
                          } else {
                            widget.responseBodyList.insert(widget.responseBodyList.length, widget.responseBodyList[i]);
                            widget.responseBodyList.removeAt(0);
                          }
                        });
                      },
                      child: const Icon(Icons.arrow_upward_outlined, size: 15),
                    ),
                  ),
                ),
                Container(
                  width: 45,
                  height: 25,
                  margin: const EdgeInsets.only(right: 7),
                  child: Tooltip(
                    message: multiLanguageString(name: "move_down", defaultValue: "Move down", context: context),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(backgroundColor: Colors.blue, padding: const EdgeInsets.symmetric(horizontal: 11)),
                      onPressed: () {
                        setState(() {
                          if (i + 1 < widget.responseBodyList.length) {
                            widget.responseBodyList.insert(i + 2, widget.responseBodyList[i]);
                            widget.responseBodyList.removeAt(i);
                          } else {
                            widget.responseBodyList.insert(0, widget.responseBodyList[i]);
                            widget.responseBodyList.removeAt(i + 1);
                          }
                        });
                      },
                      child: const Icon(Icons.arrow_downward_outlined, size: 15),
                    ),
                  ),
                ),
                Container(
                  width: 45,
                  height: 25,
                  margin: const EdgeInsets.only(right: 7),
                  child: Tooltip(
                    message: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(backgroundColor: Colors.grey, padding: const EdgeInsets.symmetric(horizontal: 11)),
                      onPressed: () {
                        Navigator.of(context).push(
                          createRoute(
                            ResponseBodyFormWidget(
                              responseBody: widget.responseBodyList[i],
                              onSave: () {
                                setState(() {
                                  widget.responseBodyList[i];
                                });
                              },
                            ),
                          ),
                        );
                      },
                      child: const Icon(Icons.edit, color: Colors.white, size: 15),
                    ),
                  ),
                ),
                SizedBox(
                  width: 45,
                  height: 25,
                  child: Tooltip(
                    message: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(backgroundColor: Colors.red, padding: const EdgeInsets.symmetric(horizontal: 11)),
                      onPressed: () {
                        setState(() {
                          widget.responseBodyList.remove(widget.responseBodyList[i]);
                        });
                      },
                      child: const Icon(Icons.delete, color: Colors.white, size: 15),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Row headerWidget() {
    return Row(
      children: [
        HeaderItemTable(
          title: multiLanguageString(name: "data_type", defaultValue: "Data Type", context: context),
          flex: 2,
        ),
        HeaderItemTable(
          title: multiLanguageString(name: "name", defaultValue: "Name", context: context),
          flex: 2,
        ),
        HeaderItemTable(
          title: multiLanguageString(name: "response_path", defaultValue: "Response path", context: context),
          flex: 2,
        ),
        Container(
          width: 215,
          alignment: Alignment.center,
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 12),
          decoration: BoxDecoration(
            border: Border.all(width: 1, color: const Color.fromARGB(255, 205, 206, 207), style: BorderStyle.solid),
            color: const Color.fromARGB(255, 222, 223, 224),
          ),
          child: MultiLanguageText(
            name: "option",
            defaultValue: "Option",
            style: TextStyle(fontSize: context.fontSizeManager.fontSizeText14, fontWeight: FontWeight.bold, letterSpacing: 2),
          ),
        ),
      ],
    );
  }
}

class ResponseBodyFormWidget extends StatefulWidget {
  final dynamic responseBody;
  final VoidCallback onSave;
  const ResponseBodyFormWidget({Key? key, this.responseBody, required this.onSave}) : super(key: key);

  @override
  State<ResponseBodyFormWidget> createState() => _ResponseBodyFormWidgetState();
}

class _ResponseBodyFormWidgetState extends CustomState<ResponseBodyFormWidget> {
  TextEditingController nameController = TextEditingController();
  TextEditingController responsePathController = TextEditingController();
  String? dataType;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    setInitValue();
    super.initState();
  }

  setInitValue() {
    setState(() {
      if (widget.responseBody != null) {
        if (widget.responseBody["name"] != null) nameController.text = widget.responseBody["name"];
        if (widget.responseBody["responsePath"] != null) responsePathController.text = widget.responseBody["responsePath"];
        if (widget.responseBody["dataType"] != null) dataType = widget.responseBody["dataType"];
      }
    });
  }

  @override
  void dispose() {
    nameController.dispose();
    responsePathController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return TestFrameworkRootPageWidget(
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              headerWidget(),
              Expanded(
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            const Expanded(
                              child: MultiLanguageText(
                                name: "data_type_adjacent",
                                defaultValue: "DataType",
                                suffiText: " : ",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Color.fromRGBO(87, 94, 117, 1),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 3,
                              child: DynamicDropdownButton(
                                value: dataType,
                                hint: multiLanguageString(name: "choose_a_data_space_type", defaultValue: "Choose a data type", context: context),
                                items: const ["String", "Integer", "Long", "Double", "Float", "Date", "DateTime", "File", "RandomValue", "Password"]
                                    .map<DropdownMenuItem<String>>((String result) {
                                  return DropdownMenuItem(
                                    value: result,
                                    child: Text(result),
                                  );
                                }).toList(),
                                onChanged: (newValue) {
                                  setState(() {
                                    dataType = newValue;
                                  });
                                },
                                borderRadius: 5,
                                isRequiredNotEmpty: true,
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            const Expanded(
                              child: MultiLanguageText(
                                name: "name",
                                defaultValue: "Name",
                                suffiText: " : ",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Color.fromRGBO(87, 94, 117, 1),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 3,
                              child: DynamicTextField(
                                controller: nameController,
                                hintText: multiLanguageString(name: "enter_a_name", defaultValue: "Enter a name", context: context),
                                maxline: 3,
                                minline: 1,
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            const Expanded(
                              child: MultiLanguageText(
                                name: "response_path",
                                defaultValue: "Response path",
                                suffiText: " : ",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Color.fromRGBO(87, 94, 117, 1),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 3,
                              child: DynamicTextField(
                                controller: responsePathController,
                                hintText: multiLanguageString(name: "enter_a_response_path", defaultValue: "Enter a response path", context: context),
                                maxline: 3,
                                minline: 1,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              btnWidget(),
              const SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      );
    });
  }

  Widget btnWidget() {
    return DynamicButton(
      name: "save",
      label: multiLanguageString(name: "save", defaultValue: "Save", isLowerCase: false, context: context),
      onPressed: () {
        if (_formKey.currentState!.validate()) {
          widget.responseBody["dataType"] = dataType;
          widget.responseBody["responsePath"] = responsePathController.text;
          widget.responseBody["name"] = nameController.text;
          Navigator.pop(context);
          widget.onSave();
        }
      },
    );
  }

  Widget headerWidget() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Center(
              child: MultiLanguageText(
                name: "response_body_property",
                defaultValue: "Response body property",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: context.fontSizeManager.fontSizeText20, letterSpacing: 1.5),
              ),
            ),
          ),
          TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: const Icon(
                Icons.close,
                color: Colors.black,
              ))
        ],
      ),
    );
  }
}
