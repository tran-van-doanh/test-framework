import 'package:flutter/material.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import '../../../common/custom_state.dart';
import '../../../common/dynamic_text_field.dart';
import '../../../components/style.dart';
import '../../../type.dart';
import '../../editor/drag&drop_widget/action_widget.dart';
import '../../editor/property_widget/root_property_widget.dart';
import 'multi_form_variable_path.dart';

class DynamicListApiWidget extends StatefulWidget {
  final List dynamicList;
  final String type;
  final GlobalKey<FormState>? keyForm;

  const DynamicListApiWidget({
    Key? key,
    this.keyForm,
    required this.dynamicList,
    required this.type,
  }) : super(key: key);

  @override
  State<DynamicListApiWidget> createState() => _DynamicListApiWidgetState();
}

class _DynamicListApiWidgetState extends CustomState<DynamicListApiWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (widget.type != "Header") Text(widget.type, style: Style(context).styleTitleDialog),
        if (widget.type != "Header")
          const SizedBox(
            height: 10,
          ),
        headerWidget(),
        Container(
          constraints: const BoxConstraints(maxHeight: 500),
          child: SingleChildScrollView(
            child: Column(
              children: [
                for (var i = 0; i < widget.dynamicList.length; i++) headerItemWidget(i),
              ],
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 5),
          alignment: Alignment.center,
          child: FloatingActionButton.extended(
            heroTag: null,
            icon: const Icon(Icons.add),
            label: const MultiLanguageText(name: "add", defaultValue: "Add"),
            onPressed: () {
              var newItem = {"name": "", "variablePath": []};
              Navigator.of(context).push(
                createRoute(
                  MultipartFormVariablePath(
                    multiPartForm: newItem,
                    onSave: () {
                      setState(() {
                        widget.dynamicList.add(newItem);
                      });
                    },
                    typeVariablePath: widget.type,
                  ),
                ),
              );
            },
          ),
        ),
      ],
    );
  }

  Widget headerItemWidget(int i) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: DynamicTextField(
              enabled: false,
              fillColor: Colors.transparent,
              controller: TextEditingController(text: widget.dynamicList[i]["name"] ?? ""),
              onChanged: (text) {
                widget.dynamicList[i]["name"] = text;
              },
              isRequiredNotEmpty: true,
              maxline: 1,
            ),
          ),
          Expanded(
            child: Container(
              margin: const EdgeInsets.only(left: 4),
              padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 10),
              constraints: const BoxConstraints(minHeight: 48),
              decoration: BoxDecoration(
                border: Border.all(color: const Color.fromARGB(255, 83, 85, 88)),
                borderRadius: BorderRadius.circular(3),
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (widget.dynamicList[i]["variablePath"] != null && widget.dynamicList[i]["variablePath"].isNotEmpty)
                    for (var variablePath in widget.dynamicList[i]["variablePath"])
                      VariablePathWidget(
                        isLast: widget.dynamicList[i]["variablePath"].last != variablePath,
                        variablePath: variablePath,
                        colorText: const Color.fromARGB(255, 83, 85, 88),
                        key: UniqueKey(),
                      ),
                ],
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 5),
            padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: const BorderRadius.all(Radius.circular(5.0)),
            ),
            height: 48,
            child: Row(
              children: [
                Container(
                  width: 45,
                  height: 25,
                  margin: const EdgeInsets.only(right: 7),
                  child: Tooltip(
                    message: multiLanguageString(name: "move_up", defaultValue: "Move up", context: context),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(backgroundColor: Colors.blue, padding: const EdgeInsets.symmetric(horizontal: 11)),
                      onPressed: () {
                        setState(() {
                          if (i > 0) {
                            widget.dynamicList.insert(i - 1, widget.dynamicList[i]);
                            widget.dynamicList.removeAt(i + 1);
                          } else {
                            widget.dynamicList.insert(widget.dynamicList.length, widget.dynamicList[i]);
                            widget.dynamicList.removeAt(0);
                          }
                        });
                      },
                      child: const Icon(Icons.arrow_upward_outlined, size: 15),
                    ),
                  ),
                ),
                Container(
                  width: 45,
                  height: 25,
                  margin: const EdgeInsets.only(right: 7),
                  child: Tooltip(
                    message: multiLanguageString(name: "move_down", defaultValue: "Move down", context: context),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(backgroundColor: Colors.blue, padding: const EdgeInsets.symmetric(horizontal: 11)),
                      onPressed: () {
                        setState(() {
                          if (i + 1 < widget.dynamicList.length) {
                            widget.dynamicList.insert(i + 2, widget.dynamicList[i]);
                            widget.dynamicList.removeAt(i);
                          } else {
                            widget.dynamicList.insert(0, widget.dynamicList[i]);
                            widget.dynamicList.removeAt(i + 1);
                          }
                        });
                      },
                      child: const Icon(Icons.arrow_downward_outlined, size: 15),
                    ),
                  ),
                ),
                Container(
                  width: 45,
                  height: 25,
                  margin: const EdgeInsets.only(right: 7),
                  child: Tooltip(
                    message: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(backgroundColor: Colors.grey, padding: const EdgeInsets.symmetric(horizontal: 11)),
                      onPressed: () {
                        Navigator.of(context).push(
                          createRoute(
                            MultipartFormVariablePath(
                              multiPartForm: widget.dynamicList[i],
                              onSave: () {
                                setState(() {
                                  widget.dynamicList[i];
                                });
                              },
                              onCancel: () => setState(() {}),
                              typeVariablePath: "Header",
                            ),
                          ),
                        );
                      },
                      child: const Icon(Icons.edit, color: Colors.white, size: 15),
                    ),
                  ),
                ),
                SizedBox(
                  width: 45,
                  height: 25,
                  child: Tooltip(
                    message: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(backgroundColor: Colors.red, padding: const EdgeInsets.symmetric(horizontal: 11)),
                      onPressed: () {
                        setState(() {
                          widget.dynamicList.remove(widget.dynamicList[i]);
                        });
                      },
                      child: const Icon(Icons.delete, color: Colors.white, size: 15),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Row headerWidget() {
    return Row(
      children: [
        HeaderItemTable(
          title: multiLanguageString(name: "name", defaultValue: "Name", context: context),
          flex: 2,
        ),
        HeaderItemTable(
          title: multiLanguageString(name: "value", defaultValue: "Value", context: context),
          flex: 2,
        ),
        Container(
          width: 215,
          alignment: Alignment.center,
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 12),
          decoration: BoxDecoration(
            border: Border.all(width: 1, color: const Color.fromARGB(255, 205, 206, 207), style: BorderStyle.solid),
            color: const Color.fromARGB(255, 222, 223, 224),
          ),
          child: MultiLanguageText(
            name: "option",
            defaultValue: "Option",
            style: TextStyle(fontSize: context.fontSizeManager.fontSizeText14, fontWeight: FontWeight.bold, letterSpacing: 2),
          ),
        ),
      ],
    );
  }
}
