import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/editor/property_widget/dynamic_variabla_path_widget.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';
import 'package:test_framework_view/type.dart';

import '../../../common/custom_state.dart';
import '../../../common/dynamic_dropdown_button.dart';

class MultipartFormVariablePath extends StatefulWidget {
  final dynamic multiPartForm;
  final String typeVariablePath;
  final VoidCallback onSave;
  final VoidCallback? onCancel;
  const MultipartFormVariablePath({Key? key, this.multiPartForm, required this.onSave, required this.typeVariablePath, this.onCancel})
      : super(key: key);

  @override
  State<MultipartFormVariablePath> createState() => _MultipartFormVariablePathState();
}

class _MultipartFormVariablePathState extends CustomState<MultipartFormVariablePath> {
  List parameterList = [];
  TextEditingController nameController = TextEditingController();
  String? type;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    setInitValue();
    super.initState();
  }

  setInitValue() {
    setState(() {
      if (widget.multiPartForm != null) {
        if (widget.multiPartForm["name"] != null) nameController.text = widget.multiPartForm["name"];
        if (widget.typeVariablePath == "Multi-part form" && widget.multiPartForm["type"] != null) type = widget.multiPartForm["type"];
      }
      if (widget.multiPartForm["variablePath"] != null && widget.multiPartForm["variablePath"].isNotEmpty) {
        parameterList = widget.multiPartForm["variablePath"];
      } else {
        parameterList = [
          {"dataType": "String", "type": "value"}
        ];
      }
    });
  }

  @override
  void dispose() {
    nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return TestFrameworkRootPageWidget(
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              headerWidget(),
              Expanded(
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                    child: Column(
                      children: [
                        if (widget.typeVariablePath == "Multi-part form")
                          Row(
                            children: [
                              const Expanded(
                                child: MultiLanguageText(
                                  name: "type",
                                  defaultValue: "Type",
                                  suffiText: " : ",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Color.fromRGBO(87, 94, 117, 1),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 3,
                                child: DynamicDropdownButton(
                                  value: type,
                                  hint: multiLanguageString(name: "choose_a_type", defaultValue: "Choose a type", context: context),
                                  items: const ["Data", "File"].map<DropdownMenuItem<String>>((String result) {
                                    return DropdownMenuItem(
                                      value: result,
                                      child: Text(result),
                                    );
                                  }).toList(),
                                  onChanged: (newValue) {
                                    setState(() {
                                      type = newValue;
                                    });
                                  },
                                  borderRadius: 5,
                                  isRequiredNotEmpty: true,
                                ),
                              ),
                            ],
                          ),
                        if (widget.typeVariablePath == "Multi-part form")
                          const SizedBox(
                            height: 10,
                          ),
                        Row(
                          children: [
                            const Expanded(
                              child: MultiLanguageText(
                                name: "name",
                                defaultValue: "Name",
                                suffiText: " : ",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Color.fromRGBO(87, 94, 117, 1),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 3,
                              child: SizedBox(
                                child: DynamicTextField(
                                  controller: nameController,
                                  hintText: multiLanguageString(name: "enter_a_name", defaultValue: "Enter a name", context: context),
                                  maxline: 3,
                                  minline: 1,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 20),
                          child: Column(
                            children: [
                              for (var parameter in parameterList) parameterItemWidget(parameter),
                              if (parameterList.isEmpty ||
                                  parameterList[0]["type"] == "variable" ||
                                  parameterList[0]["type"] == "version_variable" ||
                                  parameterList[0]["type"] == "environment_variable" ||
                                  parameterList[0]["type"] == "node_variable")
                                Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 20),
                                  child: FloatingActionButton.extended(
                                    heroTag: null,
                                    icon: const Icon(Icons.add),
                                    label: const MultiLanguageText(name: "add_variable_path", defaultValue: "Add variablePath"),
                                    onPressed: () {
                                      setState(() {
                                        if (parameterList.isNotEmpty) {
                                          parameterList.add({"type": "get"});
                                        } else {
                                          parameterList = [];
                                          parameterList.add({});
                                        }
                                      });
                                    },
                                  ),
                                ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              btnWidget(),
              const SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      );
    });
  }

  Widget btnWidget() {
    return DynamicButton(
      name: "save",
      label: multiLanguageString(name: "save", defaultValue: "Save", isLowerCase: false, context: context),
      onPressed: () {
        if (_formKey.currentState!.validate()) {
          widget.multiPartForm["variablePath"] = parameterList;
          widget.multiPartForm["name"] = nameController.text;
          if (widget.typeVariablePath == "Multi-part form") widget.multiPartForm["type"] = type;
          Navigator.pop(context);
          widget.onSave();
        }
      },
    );
  }

  Widget parameterItemWidget(parameter) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Expanded(
              child: Container(
                margin: parameter != parameterList.last ? const EdgeInsets.only(bottom: 10) : null,
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  border: Border.all(color: const Color.fromRGBO(216, 218, 229, 1)),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Column(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        DynamicVariablePathWidget(
                          parameter,
                          parameterList.indexOf(parameter),
                          [
                            {"value": "value", "name": multiLanguageString(name: "value", defaultValue: "Value", context: context)},
                            {"value": "variable", "name": multiLanguageString(name: "variable", defaultValue: "Variable", context: context)},
                            {
                              "value": "version_variable",
                              "name": multiLanguageString(name: "version_parameter", defaultValue: "Version Parameter", context: context)
                            },
                            {
                              "value": "environment_variable",
                              "name": multiLanguageString(name: "environment_parameter", defaultValue: "Environment Parameter", context: context)
                            },
                            {
                              "value": "node_variable",
                              "name": multiLanguageString(name: "agent_parameter", defaultValue: "Agent Parameter", context: context)
                            },
                          ],
                          dataTypeList,
                          keyForm: _formKey,
                          callback: () {
                            setState(() {});
                          },
                          isRequiredNotEmpty: true,
                          isRequiredRegex: true,
                          onRemove: () => setState(() {
                            parameterList.remove(parameter);
                          }),
                        ),
                        if (parameter != parameterList.last)
                          const SizedBox(
                            height: 20,
                          ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        if (parameter != parameterList.last)
          const SizedBox(
            height: 10,
          )
      ],
    );
  }

  Widget headerWidget() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Center(
              child: MultiLanguageText(
                name: "type_property",
                defaultValue: "\$0 property",
                variables: [widget.typeVariablePath],
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: context.fontSizeManager.fontSizeText20, letterSpacing: 1.5),
              ),
            ),
          ),
          TextButton(
              onPressed: () {
                Navigator.pop(context);
                if (widget.onCancel != null) widget.onCancel!();
              },
              child: const Icon(
                Icons.close,
                color: Colors.black,
              ))
        ],
      ),
    );
  }
}
