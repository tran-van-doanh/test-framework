import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/editor/editor/editor_page.dart';
import 'package:test_framework_view/page/list_test/tests/test/dialog_option_test.dart';
import 'package:test_framework_view/type.dart';

class MyNotification extends StatefulWidget {
  const MyNotification({Key? key}) : super(key: key);

  @override
  State<MyNotification> createState() => _MyNotificationState();
}

class _MyNotificationState extends State<MyNotification> {
  late Future futureListNotification;
  var resultListNotification = [];
  var notificationCurrentPage = 1;
  var notificationRowCount = 0;
  var notificationSearchRequest = {};
  var notificationRowPerPage = 10;

  @override
  void initState() {
    notificationSearch({});
    super.initState();
  }

  notificationSearch(notificationSearchRequest) {
    this.notificationSearchRequest = notificationSearchRequest;
    futureListNotification = notificationPageChange(notificationCurrentPage);
  }

  notificationPageChange(page) async {
    if ((page - 1) * notificationRowPerPage > notificationRowCount) {
      page = (1.0 * notificationRowCount / notificationRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * notificationRowPerPage, "queryLimit": notificationRowPerPage, "status": 1};
    if (notificationSearchRequest["itemType"] != null && notificationSearchRequest["itemType"].isNotEmpty) {
      findRequest["itemType"] = "%${notificationSearchRequest["itemType"]}%";
    }
    var response = await httpPost("/test-framework-api/user/noti/noti-notification/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        notificationCurrentPage = page;
        notificationRowCount = response["body"]["rowCount"];
        resultListNotification = response["body"]["resultList"];
      });
    }
    return 0;
  }

  handleEditTfTestCase(result, id) async {
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-case/$id", result, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      if (response["body"].containsKey("errorMessage") && mounted) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else if (mounted) {
        Navigator.of(context).pop();
        showToast(
            context: context,
            msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
      }
    }
  }

  onReadNoti(id) async {
    var response = await httpPost("/test-framework-api/user/noti/noti-notification/$id/mark-read", {}, context);
    if (response.containsKey("body") && response["body"] is String == false && response["body"].containsKey("errorMessage") && mounted) {
      showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
    }
  }

  onMarkAllRead() async {
    var response = await httpPost("/test-framework-api/user/noti/noti-notification/mark-all-read", {}, context);
    if (response.containsKey("body") && response["body"] is String == false && response["body"].containsKey("errorMessage") && mounted) {
      showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
    }
  }

  onClickNotiTestCase(resultNotification) async {
    if (resultNotification["actionType"] == "comment") {
      Navigator.of(context).push(
        createRoute(
          EditorPageRootWidget(
            isOpenListComment: true,
            testCaseType: resultNotification["itemType"],
            testCaseId: resultNotification["itemId"],
            callback: () {},
          ),
        ),
      );
    } else {
      var responseGetTestCase = await httpGet("/test-framework-api/user/test-framework/tf-test-case/${resultNotification["itemId"]}", context);
      if (mounted) {
        Navigator.of(context).push(
          createRoute(
            DialogOptionTestWidget(
              testCaseType: resultNotification["itemType"],
              type: "edit",
              title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
              selectedItem: (responseGetTestCase.containsKey("body") && responseGetTestCase["body"] is String == false)
                  ? responseGetTestCase["body"]["result"] ?? {}
                  : {},
              callbackOptionTest: (result) {
                handleEditTfTestCase(result, result["id"]);
              },
              parentId: responseGetTestCase["body"]["result"]["tfTestDirectoryId"],
            ),
          ),
        );
      }
    }
  }

  onClickNotiTestRun(itemId, NavigationModel navigationModel) async {
    var responseGetTestRun = await httpGet("/test-framework-api/user/test-framework/tf-test-run/$itemId", context);
    if (responseGetTestRun.containsKey("body") && responseGetTestRun["body"] is String == false) {
      String? type =
          responseGetTestRun["body"]["result"]['tfTestCase'] != null ? responseGetTestRun["body"]["result"]['tfTestCase']['testCaseType'] : null;
      if (type != null) {
        if (responseGetTestRun["body"]["result"]["prjProjectId"] != navigationModel.prjProjectId) {
          navigationModel.setPrjProjectId(responseGetTestRun["body"]["result"]["prjProjectId"]);
        }
        navigationModel.navigate(
            "/qa-platform/project/${responseGetTestRun["body"]["result"]["prjProjectId"]}/runs/$type/test-run/${responseGetTestRun["body"]["result"]["id"]}");
      }
    }
  }

  onClickNotiTestBatchRun(itemId, navigationModel) async {
    var responseGetTestRun = await httpGet("/test-framework-api/user/test-framework/tf-test-batch-run/$itemId", context);
    if (responseGetTestRun.containsKey("body") && responseGetTestRun["body"] is String == false) {
      if (responseGetTestRun["body"]["result"]["prjProjectId"] != navigationModel.prjProjectId) {
        navigationModel.setPrjProjectId(responseGetTestRun["body"]["result"]["prjProjectId"]);
      }
      navigationModel.navigate(
          "/qa-platform/project/${responseGetTestRun["body"]["result"]["prjProjectId"]}/runs/test_batch/test-run/${responseGetTestRun["body"]["result"]["id"]}");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer3<NavigationModel, NotificationModel, LanguageModel>(builder: (context, navigationModel, notificationModel, languageModel, child) {
      return FutureBuilder(
          future: futureListNotification,
          builder: (context, snapshot) {
            return Scaffold(
              appBar: (resultListNotification.isNotEmpty)
                  ? AppBar(
                      automaticallyImplyLeading: false,
                      leading: IconButton(
                        icon: const Icon(Icons.arrow_back),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        tooltip: multiLanguageString(name: "back", defaultValue: "Back", context: context),
                      ),
                      iconTheme: const IconThemeData(color: Colors.black),
                      title: const MultiLanguageText(
                          name: "notification",
                          defaultValue: "Notification",
                          style: TextStyle(color: Colors.black, fontSize: 30, fontWeight: FontWeight.w500)),
                      backgroundColor: Colors.grey[200],
                    )
                  : null,
              body: (resultListNotification.isNotEmpty)
                  ? SingleChildScrollView(
                      child: Column(
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            padding: const EdgeInsets.fromLTRB(150, 50, 150, 20),
                            color: Colors.grey[200],
                            child: Column(
                              children: [
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          const Icon(
                                            Icons.notifications,
                                            color: Colors.lightBlue,
                                            size: 30,
                                          ),
                                          const SizedBox(width: 10),
                                          MultiLanguageText(
                                            name: "notification_count",
                                            defaultValue: "Notification (\$0)",
                                            variables: ["$notificationRowCount"],
                                            style: const TextStyle(fontSize: 30, fontWeight: FontWeight.w500),
                                          ),
                                        ],
                                      ),
                                      GestureDetector(
                                        onTap: () async {
                                          await onMarkAllRead();
                                          notificationPageChange(notificationCurrentPage);
                                        },
                                        child: const MultiLanguageText(
                                          name: "mark_all_as_read",
                                          defaultValue: "Mark all as read",
                                          style: TextStyle(color: Colors.grey, decoration: TextDecoration.underline),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(height: 20),
                                notificationList(navigationModel, notificationModel),
                                DynamicTablePagging(
                                  notificationRowCount,
                                  notificationCurrentPage,
                                  notificationRowPerPage,
                                  pageChangeHandler: (page) {
                                    setState(() {
                                      futureListNotification = notificationPageChange(page);
                                    });
                                  },
                                  rowPerPageChangeHandler: (rowPerPage) {
                                    setState(() {
                                      notificationRowPerPage = rowPerPage!;
                                      futureListNotification = notificationPageChange(1);
                                    });
                                  },
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                  : Stack(
                      children: [
                        Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset("assets/images/no_notification.png"),
                              const MultiLanguageText(
                                name: "no_notificaion_yet",
                                defaultValue: "No notification yet",
                                isLowerCase: false,
                                style: TextStyle(
                                  fontSize: 28,
                                  color: Color.fromRGBO(80, 80, 80, 0.8),
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                              const SizedBox(height: 10),
                              const MultiLanguageText(
                                name: "when_you_get_notifications",
                                defaultValue: "When you get notifications, they'll show up here",
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Color.fromRGBO(120, 120, 120, 0.8),
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Positioned(
                          right: MediaQuery.of(context).size.width / 2 - 90,
                          bottom: 20.0,
                          child: SizedBox(
                            height: 40,
                            child: FloatingActionButton.extended(
                              heroTag: null,
                              onPressed: () {
                                if (navigationModel.product == "qa-platform") {
                                  navigationModel.navigate("/qa-platform/project/${navigationModel.prjProjectId}/insights/dashboard");
                                } else if (navigationModel.product == "resource-pool") {
                                  navigationModel.navigate("/resource-pool/insights");
                                } else if (navigationModel.product == "admin") {
                                  navigationModel.navigate("/admin/dashboard");
                                } else if (navigationModel.product == "workflow") {
                                  navigationModel.navigate("/workflow/status");
                                } else if (navigationModel.product == "prj-360") {
                                  navigationModel.navigate("/prj-360/dashboard");
                                } else {
                                  navigationModel.navigate("/${navigationModel.product}/project/${navigationModel.prjProjectId}/summary");
                                }
                              },
                              icon: const Icon(Icons.home, color: Colors.white),
                              label: MultiLanguageText(
                                name: "go_to_dashboard",
                                defaultValue: "Go to DashBoard",
                                style: TextStyle(fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: Colors.white),
                              ),
                              backgroundColor: const Color.fromRGBO(76, 151, 255, 1),
                              elevation: 0,
                              hoverElevation: 0,
                            ),
                          ),
                        )
                      ],
                    ),
            );
          });
    });
  }

  Widget notificationList(NavigationModel navigationModel, NotificationModel notificationModel) {
    return Column(
      children: [
        for (var resultNotification in resultListNotification)
          GestureDetector(
            onTap: () async {
              if (resultNotification["itemType"] == "test_case") onClickNotiTestCase(resultNotification);
              if (resultNotification["itemType"] == "test_run") onClickNotiTestRun(resultNotification["itemId"], navigationModel);
              if (resultNotification["itemType"] == "test_batch_run") onClickNotiTestBatchRun(resultNotification["itemId"], navigationModel);
              if (resultNotification["readStatus"] == 0) {
                await onReadNoti(resultNotification["id"]);
              }
            },
            child: Container(
              padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
              margin: const EdgeInsets.only(bottom: 5),
              decoration: BoxDecoration(color: Colors.white, border: Border.all(color: Colors.grey.shade300, width: 0.5)),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          resultNotification["title"],
                          style: TextStyle(
                            color: const Color.fromRGBO(0, 0, 0, 0.8),
                            fontWeight: FontWeight.bold,
                            fontSize: context.fontSizeManager.fontSizeText14,
                          ),
                        ),
                        Text(
                          resultNotification["content"],
                          style: TextStyle(
                            color: const Color.fromRGBO(0, 0, 0, 0.8),
                            fontSize: context.fontSizeManager.fontSizeText14,
                          ),
                        ),
                        Text(
                          DateFormat('HH:mm:ss dd-MM-yyyy')
                              .format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultNotification["createDate"]).toLocal()),
                          style: TextStyle(
                            color: const Color.fromRGBO(133, 135, 145, 1),
                            fontSize: context.fontSizeManager.fontSizeText14,
                          ),
                        ),
                      ],
                    ),
                  ),
                  if (resultNotification["readStatus"] == 0)
                    Icon(
                      Icons.fiber_manual_record,
                      color: Colors.blue[900],
                    ),
                  PopupMenuButton(
                    itemBuilder: (BuildContext context) {
                      return [
                        if (resultNotification["readStatus"] == 0)
                          PopupMenuItem(
                            onTap: () async {
                              await onReadNoti(resultNotification["id"]);
                              notificationPageChange(notificationCurrentPage);
                            },
                            child: const Row(children: [
                              Icon(Icons.check),
                              SizedBox(width: 10),
                              MultiLanguageText(name: "mark_as_read", defaultValue: "Mark as read"),
                            ]),
                          ),
                        const PopupMenuItem(
                          child: Row(children: [
                            Icon(Icons.settings),
                            SizedBox(width: 10),
                            MultiLanguageText(name: "settings_notification", defaultValue: "Settings notification"),
                          ]),
                        )
                      ];
                    },
                    tooltip: "",
                  ),
                ],
              ),
            ),
          ),
      ],
    );
  }
}
