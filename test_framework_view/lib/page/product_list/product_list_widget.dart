import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';

class ProductListWidget extends StatelessWidget {
  const ProductListWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<NavigationModel>(builder: (context, navigationModel, child) {
      GestureDetector productItemWidget(String assets, String text, Function() onTapFunc, double? aspectRatio) {
        return GestureDetector(
          onTap: onTapFunc,
          child: Container(
            height: 270,
            width: 300,
            padding: const EdgeInsets.all(20),
            decoration: BoxDecoration(
              border: Border.all(
                color: const Color.fromRGBO(216, 218, 229, 1),
              ),
              borderRadius: BorderRadius.circular(20),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                AspectRatio(
                  aspectRatio: aspectRatio ?? 1.5,
                  child: SvgPicture.asset(
                    assets,
                    colorFilter: const ColorFilter.mode(Colors.blueAccent, BlendMode.srcIn),
                  ),
                ),
                Text(
                  text,
                  style: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText16,
                      color: const Color.fromRGBO(49, 49, 49, 1),
                      letterSpacing: 2,
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
        );
      }

      List productList = [
        {
          "type": "prj-360",
          "name": "Project 360",
          "url": "/prj-360/dashboard",
          "icon": "assets/images/project_360.svg",
          "module": "PROJECT_360",
          "aspectRatio": 2.0
        },
        {
          "type": "resource-pool",
          "name": "Resource Pool",
          "url": "/resource-pool/insights",
          "icon": "assets/images/resource_pool.svg",
          "module": "RESOURCE_POOL"
        },
        {
          "type": "qa-platform",
          "name": "Centralize Automation Testing Suite",
          "url": "/qa-platform/setting/project",
          "icon": "assets/images/logo.svg",
          "module": "TEST_FRAMEWORK"
        },
        {
          "type": "work-mgt",
          "name": "Work Management",
          "url": "/work-mgt/summary",
          "icon": "assets/images/work_mgt.svg",
          "module": "WORK_MANAGEMENT",
        },
        // {
        //   "name": "Content Collaboration",
        //   "icon": "assets/images/content-collaboration.svg",
        //   "module": "CONTENT_COLLABORATION",
        // },
        {
          "type": "workflow",
          "name": "Workflow",
          "url": "/workflow/status",
          "icon": "assets/images/workflow.svg",
          "module": "WORKFLOW",
        },
        {
          "name": "Simulation",
          "icon": "assets/images/simulation.svg",
          "module": "SIMULATION",
        },
        // {
        //   "name": "RPA",
        //   "icon": "assets/images/rpa.svg",
        //   "module": "RPA",
        // },
        // {
        //   "name": "Devices farm",
        //   "icon": "assets/images/devices-farm.svg",
        //   "module": "DEVICES_FARM",
        // },
        {
          "name": "Settings",
          "url": "/setting/role",
          "icon": "assets/images/settings.svg",
        },
        if ((Provider.of<SecurityModel>(context, listen: false).userPermList ?? []).where((permCode) => permCode == "ADMIN").isNotEmpty)
          {"type": "admin", "name": "Administrator", "url": "/admin/dashboard", "icon": "assets/images/admin.svg"},
      ];
      return Scaffold(
        body: ListView(
          padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
          children: [
            Column(
              children: [
                Text(
                  "LLQ QA Platform - Testing Center of Excellence",
                  style: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText36,
                      color: const Color.fromRGBO(49, 49, 49, 1),
                      letterSpacing: 2,
                      fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 20,
                ),
                Wrap(
                  spacing: 30,
                  runSpacing: 30,
                  children: [
                    for (var product in productList)
                      if (Provider.of<SecurityModel>(context, listen: false).hasSysOrganizationModules(product["module"]) == true)
                        productItemWidget(
                          product["icon"]!,
                          product["name"]!,
                          () {
                            if (product["type"] != null) navigationModel.setProduct(product["type"]);
                            if (product["url"] != null) navigationModel.navigate(product["url"]!);
                            navigationModel.setPrjProjectIdWithoutNotify(null);
                          },
                          product["aspectRatio"],
                        ),
                  ],
                ),
              ],
            ),
          ],
        ),
      );
    });
  }
}
