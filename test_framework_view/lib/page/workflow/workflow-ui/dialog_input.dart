import 'package:flutter/material.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';

class DialogConfigWorkFlow extends StatelessWidget {
  final String type;
  final Function callback;
  const DialogConfigWorkFlow({Key? key, required this.type, required this.callback}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController controller = TextEditingController();
    return AlertDialog(
      contentPadding: const EdgeInsets.all(0),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            type,
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
              onPressed: () => Navigator.pop(context, 'Cancel'),
              child: const Icon(
                Icons.close,
                color: Colors.black,
              ))
        ],
      ),
      content: Container(
        decoration: const BoxDecoration(
            border: Border(top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)), bottom: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)))),
        margin: const EdgeInsets.only(top: 20, bottom: 10),
        width: 500,
        padding: const EdgeInsets.all(20),
        child: DynamicTextField(
          autofocus: true,
          controller: controller,
        ),
      ),
      actionsAlignment: MainAxisAlignment.center,
      actions: [
        Container(
          margin: const EdgeInsets.only(right: 5),
          height: 40,
          width: 120,
          child: ElevatedButton(
            onPressed: () {
              callback(controller.text);
              Navigator.of(context).pop();
            },
            child: const MultiLanguageText(
              name: "ok",
              defaultValue: "Ok",
              isLowerCase: false,
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
        const ButtonCancel()
      ],
    );
  }
}
