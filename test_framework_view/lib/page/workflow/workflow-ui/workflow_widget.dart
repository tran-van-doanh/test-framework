import 'package:flutter/material.dart';
import 'package:test_framework_view/common/custom_draggable.dart';
import 'package:test_framework_view/page/workflow/workflow-ui/dialog_input.dart';
import 'package:test_framework_view/page/workflow/workflow-ui/element_painter.dart';
import 'package:test_framework_view/page/workflow/workflow-ui/root_element.dart';
import 'package:test_framework_view/page/workflow/workflow-ui/workflow_role_widget.dart';
import 'package:test_framework_view/type.dart';

class WorkflowWidget extends StatelessWidget {
  const WorkflowWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Row(
        children: [
          SidebarWorkflowWidget(),
          Expanded(
            child: WorkflowEditorWidget(),
          ),
        ],
      ),
    );
  }
}

class SidebarWorkflowWidget extends StatelessWidget {
  const SidebarWorkflowWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 250,
      decoration: const BoxDecoration(color: Colors.white, border: Border(right: BorderSide(width: 1.0, color: Color.fromRGBO(212, 212, 212, 1)))),
      child: ListView(
        padding: const EdgeInsets.all(10),
        children: [
          dragItemWorkflow(WorkFlowTemplate(type: "start", widget: const StartWidget())),
          const SizedBox(height: 10),
          dragItemWorkflow(WorkFlowTemplate(type: "end", widget: const EndWidget())),
          const SizedBox(height: 10),
          dragItemWorkflow(WorkFlowTemplate(type: "process", widget: const ProcessWidget())),
          const SizedBox(height: 10),
          dragItemWorkflow(WorkFlowTemplate(type: "role", widget: const RoleWidget())),
        ],
      ),
    );
  }

  Center dragItemWorkflow(WorkFlowTemplate workFlowStepTemplate) {
    return Center(
      child: CustomDraggable<WorkFlowTemplate>(
        data: workFlowStepTemplate,
        feedback: workFlowStepTemplate.widget,
        childWhenDragging: workFlowStepTemplate.widget,
        child: workFlowStepTemplate.widget,
      ),
    );
  }
}

class WorkflowEditorWidget extends StatefulWidget {
  const WorkflowEditorWidget({
    Key? key,
  }) : super(key: key);

  @override
  State<WorkflowEditorWidget> createState() => _WorkflowEditorWidgetState();
}

class _WorkflowEditorWidgetState extends State<WorkflowEditorWidget> {
  Widget widgetGhost = Container();
  late int oldIndexItem;
  late int newIndexItem;
  int acceptedRoleIndex = -1;
  bool dragAccepted = false;
  bool acceptedRoleLast = false;
  bool acceptedStepLast = false;
  final ScrollController _scrollController = ScrollController();
  List<WorkFlowRole> rollList = [
    WorkFlowRole(id: "1", name: "Owner"),
  ];
  List rollProcessList = [
    {"rollId": "1", "step": WorkFlowStep(id: "1", type: 'start')},
    {"rollId": "1", "step": WorkFlowStep(id: "2", type: 'process', content: 'Mới tạo')},
    {"rollId": "1", "step": WorkFlowStep(id: "3", type: 'process', content: 'Đang xử lý')},
    {"rollId": "1", "step": WorkFlowStep(id: "4", type: 'end')},
  ];
  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      thumbVisibility: true,
      trackVisibility: true,
      controller: _scrollController,
      thickness: 10,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        controller: _scrollController,
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: SizedBox(
            width: 3000,
            height: 2000,
            child: DragTarget<Dragable>(
              builder: (
                BuildContext context,
                List<dynamic> accepted,
                List<dynamic> rejected,
              ) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    for (var i = 0; i < rollList.length; i++)
                      DragTarget<Dragable>(
                        builder: (
                          BuildContext context,
                          List<dynamic> accepted,
                          List<dynamic> rejected,
                        ) {
                          return Column(
                            children: [
                              Text(acceptedRoleIndex.toString()),
                              if (i == acceptedRoleIndex)
                                Opacity(
                                  opacity: 0.5,
                                  child: widgetGhost,
                                ),
                              (i != 0)
                                  ? CustomDraggable(
                                      data: rollList[i],
                                      childWhenDragging: Container(),
                                      feedback: SizedBox(
                                        width: MediaQuery.of(context).size.width - 380,
                                        child: WorkflowRoleWidget(
                                          rollProcessList: rollProcessList,
                                          workFlowRoll: rollList[i],
                                        ),
                                      ),
                                      onDragStarted: () {
                                        oldIndexItem = i;
                                        dragAccepted = false;
                                      },
                                      onDragCompleted: () {
                                        setState(() {
                                          if (dragAccepted) {
                                            if (newIndexItem <= oldIndexItem) {
                                              rollList.removeAt(oldIndexItem + 1);
                                            } else {
                                              rollList.removeAt(oldIndexItem);
                                            }
                                          } else {
                                            rollList.removeAt(oldIndexItem);
                                          }
                                        });
                                      },
                                      child: WorkflowRoleWidget(
                                        rollProcessList: rollProcessList,
                                        workFlowRoll: rollList[i],
                                        acceptedStepLast: acceptedStepLast,
                                      ),
                                    )
                                  : WorkflowRoleWidget(
                                      rollProcessList: rollProcessList,
                                      workFlowRoll: rollList[i],
                                      acceptedStepLast: acceptedStepLast,
                                    )
                            ],
                          );
                        },
                        onWillAccept: (data) {
                          if (data is WorkFlowTemplate) {
                            if (data.type == "role") {
                              acceptedRoleIndex = i;
                              widgetGhost = data.widget;
                            } else {
                              acceptedStepLast = true;
                              widgetGhost = data.widget;
                            }
                          }
                          if (data is WorkFlowRole) {
                            acceptedRoleIndex = i;
                            widgetGhost = WorkflowRoleWidget(
                              rollProcessList: rollProcessList,
                              workFlowRoll: data,
                            );
                          }
                          if (data is WorkFlowStep) {
                            acceptedStepLast = true;
                            widgetGhost = RootElementWorkflowWidget(
                              workFlowStep: data,
                            );
                          }
                          return true;
                        },
                        onLeave: (data) {
                          acceptedRoleIndex = -1;
                          acceptedStepLast = false;
                        },
                        onAccept: (data) {
                          if (data is WorkFlowTemplate) {
                            if (data.type == "role") {
                              newIndexItem = i;
                              dragAccepted = true;
                              WorkFlowRole rollNew = WorkFlowRole(id: "${rollList.length + 1}", name: "");
                              showDialog<String>(
                                context: context,
                                builder: (BuildContext context) {
                                  return DialogConfigWorkFlow(
                                      type: "Role",
                                      callback: (value) {
                                        setState(() {
                                          rollNew.name = value;
                                          rollList.insert(newIndexItem, rollNew);
                                        });
                                      });
                                },
                              );
                            } else {
                              WorkFlowStep stepNew = WorkFlowStep(
                                id: "${rollList.length + 1}",
                                type: data.type,
                              );
                              setState(() {
                                rollProcessList.add(
                                  {"rollId": rollList[i].id, "step": stepNew},
                                );
                              });
                            }
                          }
                          if (data is WorkFlowRole) {
                            newIndexItem = i;
                            dragAccepted = true;
                            setState(() {
                              rollList.insert(newIndexItem, data);
                            });
                          }
                          if (data is WorkFlowStep) {
                            setState(() {
                              rollProcessList.add(
                                {"rollId": rollList[i].id, "step": data},
                              );
                            });
                          }
                          acceptedRoleIndex = -1;
                          acceptedStepLast = false;
                        },
                      ),
                    if (acceptedRoleLast)
                      Opacity(
                        opacity: 0.5,
                        child: widgetGhost,
                      )
                  ],
                );
              },
              onWillAccept: (data) {
                if (data is WorkFlowTemplate && data.type == "role") {
                  acceptedRoleLast = true;
                  widgetGhost = data.widget;
                }
                if (data is WorkFlowRole) {
                  acceptedRoleLast = true;
                  widgetGhost = WorkflowRoleWidget(
                    rollProcessList: rollProcessList,
                    workFlowRoll: data,
                  );
                }
                return true;
              },
              onLeave: (data) {
                acceptedRoleLast = false;
              },
              onAccept: (data) {
                if (data is WorkFlowTemplate && data.type == "role") {
                  WorkFlowRole rollNew = WorkFlowRole(id: "${rollList.length + 1}", name: "");
                  showDialog<String>(
                    context: context,
                    builder: (BuildContext context) {
                      return DialogConfigWorkFlow(
                          type: "Role",
                          callback: (value) {
                            setState(() {
                              rollNew.name = value;
                              rollList.add(rollNew);
                            });
                          });
                    },
                  );
                }
                if (data is WorkFlowRole) {
                  setState(() {
                    rollList.add(data);
                  });
                }
                acceptedRoleLast = false;
              },
            ),
          ),
        ),
      ),
    );
  }
}
