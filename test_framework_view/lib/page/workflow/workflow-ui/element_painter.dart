import 'package:flutter/material.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';

class ArrowLine extends StatelessWidget {
  const ArrowLine({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: ArrowPainter(),
    );
  }
}

class ArrowPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = Colors.black
      ..strokeWidth = 2.0
      ..strokeCap = StrokeCap.round;
    final linePath = Path();
    linePath.moveTo(0, size.height / 2 - 1); // Điểm bắt đầu của đường line
    linePath.lineTo(size.width, size.height / 2 - 1); // Điểm kết thúc của đường line
    linePath.lineTo(size.width, size.height / 2 + 1); // Điểm bắt đầu của đường line
    linePath.lineTo(0, size.height / 2 + 1); // Điểm bắt đầu của đường line
    linePath.close();
    canvas.drawPath(linePath, paint);
    final arrowPath = Path();
    arrowPath.moveTo(size.width - 7.5, size.height / 2 - 7.5); // Điểm đầu của mũi tên
    arrowPath.lineTo(size.width, size.height / 2); // Điểm cuối của mũi tên
    arrowPath.lineTo(size.width - 7.5, size.height / 2 + 7.5); // Điểm cuối của mũi tên
    canvas.drawPath(arrowPath, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

class StartWidget extends StatelessWidget {
  const StartWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 60,
      width: 60,
      child: CustomPaint(
        painter: CirclePainter(radius: 30, color: Colors.green),
        child: Center(
            child: MultiLanguageText(
          name: "start",
          defaultValue: "Start",
          style: Style(context).textDragAndDropWidget,
        )),
      ),
    );
  }
}

class EndWidget extends StatelessWidget {
  const EndWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 60,
      width: 60,
      child: CustomPaint(
        painter: CirclePainter(radius: 30, color: Colors.red),
        child: Center(
            child: MultiLanguageText(
          name: "end",
          defaultValue: "End",
          style: Style(context).textDragAndDropWidget,
        )),
      ),
    );
  }
}

class CirclePainter extends CustomPainter {
  final double radius;
  final Color color;
  CirclePainter({
    required this.radius,
    required this.color,
  });
  @override
  void paint(Canvas canvas, Size size) {
    final center = Offset(size.width / 2, size.height / 2);
    final paint = Paint()
      ..color = color
      ..style = PaintingStyle.fill;
    canvas.drawCircle(center, radius, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

class ProcessWidget extends StatelessWidget {
  const ProcessWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const size = Size(120, 60);
    return SizedBox(
      height: size.height,
      width: size.width,
      child: CustomPaint(
        painter: RectanglePainter(color: Colors.blue),
        child: Center(
            child: MultiLanguageText(
          name: "process",
          defaultValue: "Process",
          style: Style(context).textDragAndDropWidget,
        )),
      ),
    );
  }
}

class RectanglePainter extends CustomPainter {
  final Color color;

  RectanglePainter({required this.color});
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = color
      ..style = PaintingStyle.fill;
    final rect = RRect.fromRectAndRadius(Rect.fromLTWH(0, 0, size.width, size.height), const Radius.circular(8));
    canvas.drawRRect(rect, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

class RoleWidget extends StatelessWidget {
  const RoleWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: const Color.fromRGBO(216, 218, 229, 1)),
      ),
      child: CustomPaint(
        painter: RectanglePainter(color: Colors.white),
        child: Row(
          children: [
            Container(
              alignment: Alignment.centerLeft,
              padding: const EdgeInsets.all(10),
              decoration: const BoxDecoration(
                color: Colors.blueAccent,
              ),
              child: RotatedBox(
                quarterTurns: 1,
                child: MultiLanguageText(
                  name: "role",
                  defaultValue: "Role",
                  isLowerCase: false,
                  style: Style(context).textDragAndDropWidget,
                ),
              ),
            ),
            const VerticalDivider(color: Colors.white),
          ],
        ),
      ),
    );
  }
}
