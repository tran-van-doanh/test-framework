import 'package:flutter/material.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/page/workflow/workflow-ui/root_element.dart';
import 'package:test_framework_view/type.dart';

class WorkflowRoleWidget extends StatefulWidget {
  const WorkflowRoleWidget({
    Key? key,
    required this.rollProcessList,
    required this.workFlowRoll,
    this.acceptedStepLast = false,
  }) : super(key: key);
  final WorkFlowRole workFlowRoll;
  final List rollProcessList;
  final bool acceptedStepLast;
  @override
  State<WorkflowRoleWidget> createState() => _WorkflowRoleWidgetState();
}

class _WorkflowRoleWidgetState extends State<WorkflowRoleWidget> {
  Widget widgetGhost = Container();
  late int oldIndexItem;
  late int newIndexItem;
  int acceptedRoleIndex = -1;
  bool dragAccepted = false;
  bool acceptedRoleLast = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      alignment: Alignment.centerLeft,
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: const Color.fromRGBO(216, 218, 229, 1)),
      ),
      child: Row(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.all(10),
            decoration: const BoxDecoration(
              color: Colors.blueAccent,
            ),
            child: RotatedBox(
              quarterTurns: 1,
              child: Text(
                widget.workFlowRoll.name,
                style: Style(context).textDragAndDropWidget,
              ),
            ),
          ),
          for (var j = 0; j < widget.rollProcessList.length; j++)
            if (widget.rollProcessList[j]["rollId"] == widget.workFlowRoll.id)
              RootElementWorkflowWidget(workFlowStep: widget.rollProcessList[j]["step"]),
        ],
      ),
    );
  }
}
