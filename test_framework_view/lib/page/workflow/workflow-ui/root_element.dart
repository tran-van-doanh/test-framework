import 'package:flutter/material.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/page/workflow/workflow-ui/element_painter.dart';
import 'package:test_framework_view/type.dart';

class RootElementWorkflowWidget extends StatelessWidget {
  final WorkFlowStep workFlowStep;
  const RootElementWorkflowWidget({Key? key, required this.workFlowStep}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (workFlowStep.type == "start") {
      return const StartWidget();
    }
    if (workFlowStep.type == "end") {
      return const EndWidget();
    }
    if (workFlowStep.type == "process") {
      return const ProcessWidget();
    }
    return MultiLanguageText(
      name: "work_flow_step_not_implemented",
      defaultValue: "\$0 not implemented",
      variables: [workFlowStep.type],
      style: TextStyle(fontSize: context.fontSizeManager.fontSizeText14),
    );
  }
}
