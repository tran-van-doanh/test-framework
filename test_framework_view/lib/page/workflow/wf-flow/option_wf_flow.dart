import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/common/row_code_with_tooltip.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';

import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';
import 'package:test_framework_view/type.dart';

import '../../../api.dart';
import '../../../common/custom_state.dart';
import '../../../common/dynamic_dropdown_button.dart';

class OptionWFFlowWidget extends StatefulWidget {
  final String type;
  final String title;
  final String? id;
  final Function? callbackOptionTest;
  const OptionWFFlowWidget({Key? key, required this.type, this.id, this.callbackOptionTest, required this.title}) : super(key: key);

  @override
  State<OptionWFFlowWidget> createState() => _OptionWFFlowWidgetState();
}

class _OptionWFFlowWidgetState extends CustomState<OptionWFFlowWidget> {
  late Future future;
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _desController = TextEditingController();
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _flowConfigController = TextEditingController();
  final TextEditingController _activeFromDateController = TextEditingController();
  final TextEditingController _activeToDateController = TextEditingController();
  int _status = 0;
  String flowType = "test_case";

  final _formKey = GlobalKey<FormState>();
  dynamic selectedItem;
  @override
  void initState() {
    future = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    if (widget.id != null) {
      await getSelectedItem(widget.id!);
      await setInitValue();
    }
    return 0;
  }

  getSelectedItem(String id) async {
    var response = await httpGet("/test-framework-api/user/workflow/wf-flow/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        selectedItem = response["body"]["result"];
      });
    }
  }

  setInitValue() {
    setState(() {
      if (selectedItem != null) {
        _status = selectedItem["status"] ?? 0;
        flowType = selectedItem["flowType"] ?? "test_case";
        _desController.text = selectedItem["description"] ?? "";
        _titleController.text = selectedItem["title"] ?? "";
        _nameController.text = selectedItem["name"] ?? "";
        _flowConfigController.text = jsonEncode(selectedItem["flowConfig"] ?? {});
        if (selectedItem["activeFromDate"] != null) {
          _activeFromDateController.text =
              DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(selectedItem["activeFromDate"]).toLocal());
        }
        if (selectedItem["activeToDate"] != null) {
          _activeToDateController.text =
              DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(selectedItem["activeToDate"]).toLocal());
        }
      }
    });
  }

  _selectDate(TextEditingController controller) async {
    final DateTime? datePicked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (datePicked != null) {
      setState(() {
        controller.text = DateFormat('dd-MM-yyyy').format(datePicked);
      });
    }
  }

  @override
  void dispose() {
    _nameController.dispose();
    _desController.dispose();
    _titleController.dispose();
    _flowConfigController.dispose();
    _activeFromDateController.dispose();
    _activeToDateController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, LanguageModel>(
      builder: (context, navigationModel, languageModel, child) {
        return FutureBuilder(
          future: future,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return TestFrameworkRootPageWidget(
                child: Column(
                  children: [
                    headerWidget(context),
                    const Divider(color: Color.fromRGBO(216, 218, 229, 1)),
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.only(top: 20, bottom: 10),
                        child: Form(
                          key: _formKey,
                          child: SingleChildScrollView(
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                              child: FocusTraversalGroup(
                                policy: OrderedTraversalPolicy(),
                                child: bodyWidget(context),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    btnWidget()
                  ],
                ),
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  Padding btnWidget() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20, top: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ButtonSave(onPressed: () {
            if (_formKey.currentState!.validate()) {
              var result = {};
              if (selectedItem != null) {
                result = selectedItem;
                result["name"] = _nameController.text;
                result["title"] = _titleController.text;
                result["description"] = _desController.text;
                result["status"] = _status;
                result["flowType"] = flowType;
                result["flowConfig"] = jsonDecode(_flowConfigController.text.isEmpty ? "{}" : _flowConfigController.text);
                if (_activeFromDateController.text.isNotEmpty) {
                  result["activeFromDate"] = dateFormatTimeZone(DateFormat('dd-MM-yyyy'), _activeFromDateController.text);
                }
                if (_activeToDateController.text.isNotEmpty) {
                  result["activeToDate"] = dateFormatTimeZone(DateFormat('dd-MM-yyyy'), _activeToDateController.text);
                }
              } else {
                result = {
                  "title": _titleController.text,
                  "name": _nameController.text,
                  "description": _desController.text,
                  "status": _status,
                  "flowType": flowType,
                  "flowConfig": jsonDecode(_flowConfigController.text.isEmpty ? "{}" : _flowConfigController.text),
                  if (_activeFromDateController.text.isNotEmpty)
                    "activeFromDate": dateFormatTimeZone(DateFormat('dd-MM-yyyy'), _activeFromDateController.text),
                  if (_activeToDateController.text.isNotEmpty)
                    "activeToDate": dateFormatTimeZone(DateFormat('dd-MM-yyyy'), _activeToDateController.text),
                };
              }
              widget.callbackOptionTest!(result);
            }
          }),
          const ButtonCancel()
        ],
      ),
    );
  }

  Column bodyWidget(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.only(bottom: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "type", defaultValue: "Type", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                    borderRadius: 5,
                    value: flowType,
                    items: [
                      {"name": "Test case", "value": "test_case"},
                    ].map<DropdownMenuItem<String>>((dynamic result) {
                      return DropdownMenuItem(
                        value: result["value"],
                        child: Text(result["name"]),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        flowType = newValue!;
                      });
                    },
                    isRequiredNotEmpty: true),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Expanded(
                flex: 3,
                child: RowCodeWithTooltipWidget(),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  enabled: widget.type != "edit",
                  controller: _nameController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                  isRequiredRegex: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "name", defaultValue: "Name", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _titleController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "status", defaultValue: "Status", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                    borderRadius: 5,
                    value: _status,
                    hint: multiLanguageString(name: "choose_a_status", defaultValue: "Choose a status", context: context),
                    items: [
                      {"name": multiLanguageString(name: "active", defaultValue: "Active", context: context), "value": 1},
                      {"name": multiLanguageString(name: "inactive", defaultValue: "Inactive", context: context), "value": 0},
                    ].map<DropdownMenuItem<int>>((dynamic result) {
                      return DropdownMenuItem(
                        value: result["value"],
                        child: Text(result["name"]),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        _status = newValue!;
                      });
                    },
                    isRequiredNotEmpty: true),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "description", defaultValue: "Description", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _desController,
                  hintText: "...",
                  maxline: 3,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "config", defaultValue: "Config", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _flowConfigController,
                  hintText: "...",
                  maxline: 10,
                  minline: 3,
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "from_date", defaultValue: "From date", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _activeFromDateController,
                  suffixIcon: const Icon(Icons.calendar_today),
                  hintText: multiLanguageString(name: "select_from_date", defaultValue: "Select from date", context: context),
                  labelText: multiLanguageString(name: "from_date", defaultValue: "From date", context: context),
                  readOnly: true,
                  onTap: () {
                    _selectDate(_activeFromDateController);
                  },
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "to_date", defaultValue: "To date", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _activeToDateController,
                  suffixIcon: const Icon(Icons.calendar_today),
                  hintText: multiLanguageString(name: "select_to_date", defaultValue: "Select to date", context: context),
                  labelText: multiLanguageString(name: "to_date", defaultValue: "To date", context: context),
                  readOnly: true,
                  onTap: () {
                    _selectDate(_activeToDateController);
                  },
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Padding headerWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 35, 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "type_flow",
            defaultValue: "\$0 FLOW",
            variables: [widget.title],
            isLowerCase: false,
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
            onPressed: () => Navigator.pop(context, 'cancel'),
            child: const Icon(
              Icons.close,
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }
}
