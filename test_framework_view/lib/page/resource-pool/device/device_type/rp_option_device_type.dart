import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/custom_state.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/common/row_code_with_tooltip.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';

import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';

class RpOptionDeviceTypeWidget extends StatefulWidget {
  final String type;
  final String title;
  final String? id;
  final Function? callbackOptionTest;
  final String prjProjectId;
  const RpOptionDeviceTypeWidget({Key? key, required this.type, this.id, this.callbackOptionTest, required this.prjProjectId, required this.title})
      : super(key: key);

  @override
  State<RpOptionDeviceTypeWidget> createState() => _RpOptionDeviceTypeWidgetState();
}

class _RpOptionDeviceTypeWidgetState extends CustomState<RpOptionDeviceTypeWidget> {
  late Future future;
  final TextEditingController _codeController = TextEditingController();
  final TextEditingController _desController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _deviceTypeConfigController = TextEditingController();
  int _status = 0;
  final _formKey = GlobalKey<FormState>();
  dynamic selectedItem;
  @override
  void initState() {
    future = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    if (widget.id != null) {
      await getSelectedItem(widget.id!);
      await setInitValue();
    }
    return 0;
  }

  getSelectedItem(String id) async {
    var response = await httpGet("/test-framework-api/user/sys/sys-device-type/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        selectedItem = response["body"]["result"];
      });
    }
  }

  setInitValue() {
    setState(() {
      if (selectedItem != null) {
        _status = selectedItem["status"] ?? 0;
        _desController.text = selectedItem["description"] ?? "";
        _nameController.text = selectedItem["name"];
        _codeController.text = selectedItem["code"];
        if (selectedItem["deviceTypeConfig"] != null) {
          _deviceTypeConfigController.text = jsonEncode(selectedItem["deviceTypeConfig"]);
        } else {
          _deviceTypeConfigController.text = "{}";
        }
      }
    });
  }

  @override
  void dispose() {
    _codeController.dispose();
    _desController.dispose();
    _nameController.dispose();
    _deviceTypeConfigController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return FutureBuilder(
        future: future,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return TestFrameworkRootPageWidget(
              child: Column(
                children: [
                  headerWidget(context),
                  const Divider(color: Color.fromRGBO(216, 218, 229, 1)),
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.only(top: 20, bottom: 10),
                      child: Form(
                        key: _formKey,
                        child: SingleChildScrollView(
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                            child: FocusTraversalGroup(
                              policy: OrderedTraversalPolicy(),
                              child: bodyWidget(context),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  btnWidget()
                ],
              ),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        },
      );
    });
  }

  Padding btnWidget() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20, top: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ButtonSave(onPressed: () {
            if (_formKey.currentState!.validate()) {
              var result = {};
              if (selectedItem != null) {
                result = selectedItem;
                result["code"] = _codeController.text;
                result["name"] = _nameController.text;
                result["prjProjectId"] = widget.prjProjectId;
                result["description"] = _desController.text;
                result["status"] = _status;
                result["deviceTypeConfig"] = jsonDecode(_deviceTypeConfigController.text.isEmpty ? "{}" : _deviceTypeConfigController.text);
              } else {
                result = {
                  "code": _codeController.text,
                  "name": _nameController.text,
                  "description": _desController.text,
                  "status": _status,
                  "prjProjectId": widget.prjProjectId,
                  "deviceTypeConfig": jsonDecode(_deviceTypeConfigController.text.isEmpty ? "{}" : _deviceTypeConfigController.text),
                };
              }
              widget.callbackOptionTest!(result);
            }
          }),
          const ButtonCancel()
        ],
      ),
    );
  }

  Column bodyWidget(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Expanded(
                flex: 3,
                child: RowCodeWithTooltipWidget(),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  enabled: widget.type != "edit",
                  controller: _codeController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                  isRequiredRegex: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "name", defaultValue: "Name", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _nameController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "status", defaultValue: "Status", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                    borderRadius: 5,
                    value: _status,
                    hint: multiLanguageString(name: "choose_a_status", defaultValue: "Choose a status", context: context),
                    items: [
                      {"name": multiLanguageString(name: "active", defaultValue: "Active", context: context), "value": 1},
                      {"name": multiLanguageString(name: "inactive", defaultValue: "Inactive", context: context), "value": 0},
                    ].map<DropdownMenuItem<int>>((dynamic result) {
                      return DropdownMenuItem(
                        value: result["value"],
                        child: Text(result["name"]),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        _status = newValue!;
                      });
                    },
                    isRequiredNotEmpty: true),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "description", defaultValue: "Description", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _desController,
                  hintText: "...",
                  maxline: 3,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "config", defaultValue: "Config", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _deviceTypeConfigController,
                  hintText: "...",
                  maxline: 3,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Padding headerWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 35, 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "type_project_device_type",
            defaultValue: "\$0 PROJECT DEVICE TYPE",
            variables: [widget.title],
            isLowerCase: false,
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
            onPressed: () => Navigator.pop(context, 'cancel'),
            child: const Icon(
              Icons.close,
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }
}
