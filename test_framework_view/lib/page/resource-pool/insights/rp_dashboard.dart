import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/components/dashboard/dynamic_dashboard.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';

class RPDashBoardPageWidget extends StatefulWidget {
  const RPDashBoardPageWidget({Key? key}) : super(key: key);

  @override
  State<RPDashBoardPageWidget> createState() => _RPDashBoardPageWidgetState();
}

class _RPDashBoardPageWidgetState extends State<RPDashBoardPageWidget> {
  List dashboardList = [];
  String? dashboardSelectedId;
  Map<dynamic, dynamic> requestBody = {};
  @override
  void initState() {
    super.initState();
    getInfoPage();
  }

  getInfoPage() async {
    await getDashboardList();
    if (dashboardList.isNotEmpty) {
      setState(() {
        dashboardSelectedId = dashboardList[0]["id"];
      });
    }
  }

  getDashboardList() async {
    var findRequest = {"dashboardType": "resource_pool", "status": 1};
    for (var key in requestBody.keys) {
      if (requestBody[key] != null && requestBody[key].isNotEmpty) {
        findRequest[key] = requestBody[key];
      }
    }
    var response = await httpPost("/test-framework-api/user/report/rpt-dashboard/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false && response["body"]["resultList"].isNotEmpty) {
      setState(() {
        dashboardList = response["body"]["resultList"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return ListView(
        padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 30),
            child: MultiLanguageText(
              name: "dashboard",
              defaultValue: "Dashboard",
              style: TextStyle(
                fontSize: context.fontSizeManager.fontSizeText30,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          Row(
            children: [
              if (dashboardList.length > 1)
                Expanded(
                  child: DynamicDropdownButton(
                    value: dashboardSelectedId,
                    hint: multiLanguageString(name: "choose_a_dashboard", defaultValue: "Choose a dashboard", context: context),
                    items: dashboardList.map<DropdownMenuItem<String>>((dynamic result) {
                      return DropdownMenuItem(
                        value: result["id"],
                        child: Text(result["title"]),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        dashboardSelectedId = newValue!;
                      });
                    },
                    borderRadius: 5,
                  ),
                ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          if (dashboardSelectedId != null)
            DynamicDashboardWidget(
                key: Key("${dashboardSelectedId!}:${jsonEncode(requestBody)}"),
                dashboardType: "resource_pool",
                dashboardSelectedId: dashboardSelectedId!,
                requestBody: requestBody),
        ],
      );
    });
  }
}
