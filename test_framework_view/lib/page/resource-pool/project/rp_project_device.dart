import 'dart:async';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/custom_state.dart';

import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/components/badge/badge_icon.dart';
import 'package:test_framework_view/components/dialog/dialog_delete.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/components/header_setting.dart';
import 'package:test_framework_view/page/resource-pool/project/rp_manage_project_device.dart';
import 'package:test_framework_view/type.dart';

import '../../../../api.dart';
import '../../../../model/model.dart';

class RpProjectDeviceWidget extends StatefulWidget {
  final String prjProjectId;
  const RpProjectDeviceWidget({Key? key, required this.prjProjectId}) : super(key: key);

  @override
  State<RpProjectDeviceWidget> createState() => _RpProjectDeviceWidgetState();
}

class _RpProjectDeviceWidgetState extends CustomState<RpProjectDeviceWidget> {
  late Future futureListDevice;
  var resultListDevice = [];
  var rowCountListDevice = 0;
  var currentPage = 1;
  var rowPerPage = 10;
  var searchRequest = {};
  @override
  void initState() {
    search({});
    super.initState();
  }

  search(searchRequest) {
    this.searchRequest = searchRequest;
    futureListDevice = getListDevice(currentPage);
  }

  Future getListDevice(page) async {
    if ((page - 1) * rowPerPage > rowCountListDevice) {
      page = (1.0 * rowCountListDevice / rowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {
      "queryOffset": (page - 1) * rowPerPage,
      "queryLimit": rowPerPage,
      "prjProjectId": widget.prjProjectId,
    };
    if (searchRequest["titleLike"] != null && searchRequest["titleLike"].isNotEmpty) {
      findRequest["titleLike"] = "%${searchRequest["titleLike"]}%";
    }
    if (searchRequest["status"] != null) {
      findRequest["status"] = searchRequest["status"];
    }
    var response = await httpPost("/test-framework-api/user/sys/sys-device-project/search", findRequest, context);

    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPage = page;
        rowCountListDevice = response["body"]["rowCount"];
        resultListDevice = response["body"]["resultList"];
      });
    }
    return 0;
  }

  handleDeleteDevice(id) async {
    var response = await httpDelete("/test-framework-api/user/sys/sys-device-project/$id", context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        await getListDevice(currentPage);
      }
    }
  }

  handleAddDevice(sysDeviceId) async {
    var result = {
      "sysDeviceId": sysDeviceId,
      "prjProjectId": widget.prjProjectId,
    };
    var response = await httpPut("/test-framework-api/user/sys/sys-device-project", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        await getListDevice(currentPage);
      }
    }
  }

  // handleEditDevice(result, id) async {
  //   var response = await httpPost("/test-framework-api/user/sys/sys-device-project/$id", result, context);
  //   if (response.containsKey("body") && response["body"] is String == false && mounted) {
  //     if (response["body"].containsKey("errorMessage")) {
  //       showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
  //     } else {
  //       showToast(
  //           context: context,
  //           msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
  //           color: Colors.greenAccent,
  //           icon: const Icon(Icons.done));
  //       Navigator.of(context).pop();
  //       await getListDevice(currentPage);
  //     }
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, LanguageModel>(
      builder: (context, navigationModel, languageModel, child) {
        return FutureBuilder(
          future: futureListDevice,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Column(
                children: [
                  Expanded(
                    child: ListView(
                      children: [
                        HeaderSettingWidget(
                          type: "Device",
                          title: multiLanguageString(name: "device", defaultValue: "Device", context: context),
                          countList: rowCountListDevice,
                          callbackSearch: (result) {
                            search(result);
                          },
                        ),
                        if (resultListDevice.isNotEmpty) tableWidget(context),
                        SelectionArea(
                          child: DynamicTablePagging(
                            rowCountListDevice,
                            currentPage,
                            rowPerPage,
                            pageChangeHandler: (page) {
                              setState(() {
                                futureListDevice = getListDevice(page);
                              });
                            },
                            rowPerPageChangeHandler: (rowPerPage) {
                              setState(() {
                                this.rowPerPage = rowPerPage!;
                                futureListDevice = getListDevice(1);
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  btnWidget(context),
                ],
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  Padding btnWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            margin: const EdgeInsets.only(right: 10),
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              onPressed: () {
                Navigator.of(context).push(
                  createRoute(
                    ManageProjectDeviceWidget(
                      resultListDevice: resultListDevice,
                      onSave: (resultListDeviceNew) async {
                        List addeds =
                            resultListDeviceNew.where((element) => !resultListDevice.any((obj) => obj["sysDevice"]["id"] == element["id"])).toList();
                        List removeds =
                            resultListDevice.where((element) => !resultListDeviceNew.any((obj) => obj["id"] == element["sysDevice"]["id"])).toList();
                        for (var added in addeds) {
                          handleAddDevice(added["id"]);
                        }
                        for (var removed in removeds) {
                          handleDeleteDevice(removed["id"]);
                        }
                      },
                    ),
                  ),
                );
              },
              icon: const Icon(Icons.note_add, color: Color.fromRGBO(49, 49, 49, 1)),
              label: MultiLanguageText(
                name: "new_device",
                defaultValue: "New Device",
                isLowerCase: false,
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
            ),
          ),
        ],
      ),
    );
  }

  SelectionArea tableWidget(BuildContext context) {
    return SelectionArea(
      child: Container(
        margin: const EdgeInsets.only(top: 30),
        child: CustomDataTableWidget(
          columns: [
            Expanded(
              flex: 3,
              child: MultiLanguageText(
                name: "code",
                defaultValue: "Code",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              flex: 3,
              child: MultiLanguageText(
                name: "name",
                defaultValue: "Name",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              flex: 3,
              child: MultiLanguageText(
                name: "description",
                defaultValue: "Description",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  MultiLanguageText(
                    name: "status",
                    defaultValue: "Status",
                    isLowerCase: false,
                    style: Style(context).styleTextDataColumn,
                  ),
                  Tooltip(
                    message: multiLanguageString(name: "active_inactive", defaultValue: "Green = Active\nRed = Inactive", context: context),
                    child: const Icon(
                      Icons.info,
                      color: Colors.grey,
                      size: 16,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: 38,
            ),
          ],
          rows: [
            for (var resultDevice in resultListDevice)
              CustomDataRow(
                cells: [
                  Expanded(
                    flex: 3,
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Row(
                        children: [
                          const Icon(Icons.description, color: Color.fromRGBO(61, 148, 254, 1)),
                          const SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Text(
                              resultDevice["sysDevice"]["code"] ?? "",
                              overflow: TextOverflow.ellipsis,
                              style: Style(context).styleTextDataCell,
                              maxLines: 3,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Text(
                      resultDevice["sysDevice"]["name"] ?? "",
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Text(
                      resultDevice["sysDevice"]["description"] ?? "",
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                  Expanded(
                    child: Center(
                      child: Badge2StatusSettingWidget(
                        status: resultDevice["sysDevice"]["status"],
                      ),
                    ),
                  ),
                  PopupMenuButton(
                    offset: const Offset(5, 50),
                    tooltip: '',
                    splashRadius: 10,
                    itemBuilder: (context) => [
                      // PopupMenuItem(
                      //   padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      //   child: ListTile(
                      //     onTap: () {
                      //       Navigator.pop(context);
                      //       Navigator.of(context).push(
                      //         createRoute(
                      //           RpOptionDeviceWidget(
                      //             type: "edit",
                      //             title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                      //             id: resultDevice["id"],
                      //             callbackOptionTest: (result) {
                      //               handleEditDevice(result, resultDevice["id"]);
                      //             },
                      //           ),
                      //         ),
                      //       );
                      //     },
                      //     contentPadding: const EdgeInsets.all(0),
                      //     hoverColor: Colors.transparent,
                      //     title: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                      //     leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
                      //   ),
                      // ),
                      PopupMenuItem(
                        padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                        child: ListTile(
                          onTap: () {
                            Navigator.pop(context);
                            dialogDeleteDevice(resultDevice);
                          },
                          contentPadding: const EdgeInsets.all(0),
                          hoverColor: Colors.transparent,
                          title: const MultiLanguageText(name: "delete", defaultValue: "Delete"),
                          leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
          ],
        ),
      ),
    );
  }

  dialogDeleteDevice(selectItem) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogDelete(
          selectedItem: selectItem,
          callback: () {
            handleDeleteDevice(selectItem["id"]);
          },
          type: 'device',
        );
      },
    );
  }
}
