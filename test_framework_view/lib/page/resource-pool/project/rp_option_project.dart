import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/row_code_with_tooltip.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/page/resource-pool/project/rp_project_device.dart';
import 'package:test_framework_view/page/resource-pool/project/rp_project_user.dart';

import '../../../common/custom_state.dart';

class RPOptionProjectWidget extends StatefulWidget {
  final String type;
  final String title;
  final String? id;
  final Function? callbackOptionTest;
  const RPOptionProjectWidget({Key? key, required this.type, this.id, this.callbackOptionTest, required this.title}) : super(key: key);

  @override
  State<RPOptionProjectWidget> createState() => _RPOptionProjectWidgetState();
}

class _RPOptionProjectWidgetState extends CustomState<RPOptionProjectWidget> with TickerProviderStateMixin {
  late Future future;
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _desController = TextEditingController();
  final TextEditingController _startDateController = TextEditingController();
  final TextEditingController _endDateController = TextEditingController();
  int _status = 0;
  String? autoCreateProfile;
  final _formKey = GlobalKey<FormState>();
  dynamic selectedItem;
  String? projectType;
  List listProjectType = [];
  ScrollController scrollController = ScrollController();
  late TabController tabController;
  @override
  void initState() {
    tabController = TabController(length: 3, vsync: this);
    future = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    await getListProjectType();
    if (widget.id != null) {
      await getSelectedItem(widget.id!);
      await setInitValue();
    }
    return 0;
  }

  getSelectedItem(String id) async {
    var response = await httpGet("/test-framework-api/user/project/prj-project/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        selectedItem = response["body"]["result"];
      });
    }
  }

  _selectDate(TextEditingController controller) async {
    final DateTime? datePicked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (datePicked != null) {
      setState(() {
        controller.text = DateFormat('dd-MM-yyyy').format(datePicked);
      });
    }
  }

  getListProjectType() async {
    var findRequest = {"status": 1};
    var response = await httpPost("/test-framework-api/user/project/prj-project-type/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        listProjectType = response["body"]["resultList"];
      });
    }
  }

  setInitValue() {
    setState(() {
      if (selectedItem != null) {
        _status = selectedItem["status"] ?? 0;
        _nameController.text = selectedItem["name"] ?? "";
        _titleController.text = selectedItem["title"] ?? "";
        _desController.text = selectedItem["description"] ?? "";
        projectType = selectedItem["prjProjectTypeId"];
        _startDateController.text = selectedItem["startDate"] != null
            ? DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(selectedItem["startDate"]).toLocal())
            : DateFormat('dd-MM-yyyy').format(DateTime.now());
        _endDateController.text = selectedItem["endDate"] != null
            ? DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(selectedItem["endDate"]).toLocal())
            : DateFormat('dd-MM-yyyy').format(DateTime.now());
      }
    });
  }

  @override
  void dispose() {
    _nameController.dispose();
    _titleController.dispose();
    _desController.dispose();
    _startDateController.dispose();
    _endDateController.dispose();
    scrollController.dispose();
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: future,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Scaffold(
              body: Column(
            children: [
              headerWidget(context),
              Expanded(
                child: DefaultTabController(
                  length: 3,
                  child: Form(
                    key: _formKey,
                    child: FocusTraversalGroup(
                      policy: OrderedTraversalPolicy(),
                      child: Column(
                        children: [
                          tabControlWidget(),
                          const Divider(color: Color.fromRGBO(216, 218, 229, 1)),
                          Expanded(
                            child: Container(
                              margin: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                              child: TabBarView(
                                controller: tabController,
                                children: [
                                  generalInformation(context),
                                  selectedItem != null ? RPProjectUserWidget(prjProjectId: selectedItem["id"]) : const SizedBox.shrink(),
                                  selectedItem != null ? RpProjectDeviceWidget(prjProjectId: selectedItem["id"]) : const SizedBox.shrink(),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              btnWidget()
            ],
          ));
        } else if (snapshot.hasError) {
          return Text('${snapshot.error}');
        }
        return const Center(child: CircularProgressIndicator());
      },
    );
  }

  Container tabControlWidget() {
    List<String> listTabItem = [
      multiLanguageString(name: "general_information", defaultValue: "General information", context: context),
      multiLanguageString(name: "user", defaultValue: "User", context: context),
      multiLanguageString(name: "device", defaultValue: "Device", context: context),
    ];
    return Container(
      margin: const EdgeInsets.fromLTRB(50, 0, 50, 0),
      child: Row(
        children: [
          IconButton(
            onPressed: () {
              scrollController.animateTo(scrollController.position.pixels - 400,
                  duration: const Duration(milliseconds: 500), curve: Curves.easeInOut);
            },
            icon: const Icon(Icons.arrow_back_ios),
          ),
          Expanded(
            child: Scrollbar(
              controller: scrollController,
              thickness: 0,
              child: SingleChildScrollView(
                controller: scrollController,
                scrollDirection: Axis.horizontal,
                primary: false,
                child: TabBar(
                  controller: tabController,
                  isScrollable: true,
                  physics: const BouncingScrollPhysics(),
                  labelColor: const Color(0xff3d5afe),
                  indicatorColor: const Color(0xff3d5afe),
                  unselectedLabelColor: const Color.fromRGBO(0, 0, 0, 0.8),
                  labelStyle: Style(context).styleTitleDialog,
                  tabs: [
                    for (var tab in listTabItem)
                      Tab(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
                          child: GestureDetector(
                            onTap: () {
                              tabController.animateTo(listTabItem.indexOf(tab));
                            },
                            child: Text(tab),
                          ),
                        ),
                      ),
                  ],
                ),
              ),
            ),
          ),
          IconButton(
            onPressed: () {
              scrollController.animateTo(scrollController.position.pixels + 400,
                  duration: const Duration(milliseconds: 500), curve: Curves.easeInOut);
            },
            icon: const Icon(Icons.arrow_forward_ios),
          ),
        ],
      ),
    );
  }

  Padding btnWidget() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20, top: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ButtonSave(onPressed: () {
            if (_formKey.currentState!.validate()) {
              var result = {};
              if (selectedItem != null) {
                result = selectedItem;
                result["tfProjectTypeId"] = projectType;
                result["name"] = _nameController.text;
                result["title"] = _titleController.text;
                result["description"] = _desController.text;
                result["status"] = _status;
                result["startDate"] = "${DateFormat('dd-MM-yyyy').parse(_startDateController.text).toIso8601String()}+07:00";
                result["endDate"] = "${DateFormat('dd-MM-yyyy').parse(_endDateController.text).toIso8601String()}+07:00";
              } else {
                result = {
                  "tfProjectTypeId": projectType,
                  "name": _nameController.text,
                  "title": _titleController.text,
                  "description": _desController.text,
                  "status": _status,
                  if (_startDateController.text.isNotEmpty)
                    "startDate": "${DateFormat('dd-MM-yyyy').parse(_startDateController.text).toIso8601String()}+07:00",
                  if (_endDateController.text.isNotEmpty)
                    "endDate": "${DateFormat('dd-MM-yyyy').parse(_endDateController.text).toIso8601String()}+07:00",
                };
              }
              widget.callbackOptionTest!(result);
            }
          }),
          const ButtonCancel()
        ],
      ),
    );
  }

  SingleChildScrollView generalInformation(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 3,
                  child: MultiLanguageText(name: "project_type", defaultValue: "Project type", style: Style(context).styleTitleDialog),
                ),
                Expanded(
                  flex: 7,
                  child: DynamicDropdownButton(
                    value: projectType,
                    hint: multiLanguageString(name: "choose_a_project_type", defaultValue: "Choose a project type", context: context),
                    items: listProjectType.map<DropdownMenuItem<String>>((dynamic result) {
                      return DropdownMenuItem(
                        value: result["id"],
                        child: Text(result["title"]),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        projectType = newValue!;
                      });
                    },
                    borderRadius: 5,
                    isRequiredNotEmpty: true,
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Expanded(
                  flex: 3,
                  child: RowCodeWithTooltipWidget(),
                ),
                Expanded(
                  flex: 7,
                  child: DynamicTextField(
                    controller: _nameController,
                    hintText: "...",
                    isRequiredNotEmpty: true,
                    enabled: widget.type != "edit",
                    isRequiredRegex: true,
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 3,
                  child: MultiLanguageText(name: "name", defaultValue: "Name", style: Style(context).styleTitleDialog),
                ),
                Expanded(
                  flex: 7,
                  child: DynamicTextField(
                    controller: _titleController,
                    hintText: "...",
                    isRequiredNotEmpty: true,
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 3,
                  child: MultiLanguageText(name: "description", defaultValue: "Description", style: Style(context).styleTitleDialog),
                ),
                Expanded(
                  flex: 7,
                  child: DynamicTextField(
                    controller: _desController,
                    hintText: "...",
                    maxline: 3,
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 3,
                  child: MultiLanguageText(name: "status", defaultValue: "Status", style: Style(context).styleTitleDialog),
                ),
                Expanded(
                  flex: 7,
                  child: DynamicDropdownButton(
                    value: _status,
                    hint: multiLanguageString(name: "choose_a_status", defaultValue: "Choose a status", context: context),
                    items: [
                      {"name": "Active", "value": 1},
                      {"name": "Inactive", "value": 0},
                    ].map<DropdownMenuItem<int>>((dynamic result) {
                      return DropdownMenuItem(
                        value: result["value"],
                        child: Text(result["name"]),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        _status = newValue!;
                      });
                    },
                    borderRadius: 5,
                    isRequiredNotEmpty: true,
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 3,
                  child: MultiLanguageText(name: "start_date", defaultValue: "Start date", style: Style(context).styleTitleDialog),
                ),
                Expanded(
                  flex: 7,
                  child: DynamicTextField(
                    controller: _startDateController,
                    suffixIcon: const Icon(Icons.calendar_today),
                    hintText: multiLanguageString(name: "select_start_date", defaultValue: "Select start date", context: context),
                    labelText: multiLanguageString(name: "start_date", defaultValue: "Start date", context: context),
                    readOnly: true,
                    onTap: () {
                      _selectDate(_startDateController);
                    },
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 3,
                  child: MultiLanguageText(name: "end_date", defaultValue: "End date", style: Style(context).styleTitleDialog),
                ),
                Expanded(
                  flex: 7,
                  child: DynamicTextField(
                    controller: _endDateController,
                    suffixIcon: const Icon(Icons.calendar_today),
                    hintText: multiLanguageString(name: "select_end_date", defaultValue: "Select end date", context: context),
                    labelText: multiLanguageString(name: "end_date", defaultValue: "End date", context: context),
                    readOnly: true,
                    onTap: () {
                      _selectDate(_endDateController);
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Padding headerWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 35, 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "type_project",
            defaultValue: "\$0 PROJECT",
            variables: [widget.title],
            isLowerCase: false,
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
            onPressed: () => Navigator.pop(context, 'cancel'),
            child: const Icon(
              Icons.close,
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }
}
