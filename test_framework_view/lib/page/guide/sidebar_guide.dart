import 'package:flutter/material.dart';
import 'package:test_framework_view/components/header_application.dart';
import 'package:test_framework_view/components/navigation.dart';
import 'package:test_framework_view/page/document/side_bar_document.dart';

class TestFrameworkRootPageWidget extends StatefulWidget {
  final Widget child;
  const TestFrameworkRootPageWidget({Key? key, required this.child}) : super(key: key);

  @override
  State<TestFrameworkRootPageWidget> createState() => _TestFrameworkRootPageWidgetState();
}

class _TestFrameworkRootPageWidgetState extends State<TestFrameworkRootPageWidget> {
  bool isOpenGuide = false;
  handleOpenGuide() {
    setState(() {
      isOpenGuide = !isOpenGuide;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(60),
        child: HeaderApplication(
          onClickGuide: handleOpenGuide,
        ),
      ),
      body: Row(
        children: [
          const NavigationWidget(),
          Expanded(child: widget.child),
          if (isOpenGuide)
            Container(
              decoration: const BoxDecoration(
                  border: Border(
                    left: BorderSide(
                      color: Color.fromRGBO(0, 0, 0, 0.1),
                    ),
                  ),
                  color: Color.fromARGB(255, 235, 242, 255)),
              child: SideBarDocumentWidget(
                documentType: "guide",
                onClose: handleOpenGuide,
              ),
            ),
        ],
      ),
    );
  }
}
