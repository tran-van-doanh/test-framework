import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/editor/element_config/element_repository.dart';
import 'package:test_framework_view/page/editor/element_config/web_element.dart';
import 'package:test_framework_view/page/editor/property_widget/root_property_widget.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';
import 'package:test_framework_view/type.dart';
import '../../common/custom_state.dart';
import '../editor/action_button_editor/dialog_analyze.dart';

class OptionElementWidget extends StatefulWidget {
  final String type;
  final String title;
  final String? id;
  final Function? callbackOptionTest;
  final String prjProjectId;
  final String? elementTemplateId;
  final bool isStartDebug;
  const OptionElementWidget(
      {Key? key,
      required this.type,
      this.id,
      this.callbackOptionTest,
      required this.prjProjectId,
      this.elementTemplateId,
      required this.isStartDebug,
      required this.title})
      : super(key: key);

  @override
  State<OptionElementWidget> createState() => _OptionElementWidgetState();
}

class _OptionElementWidgetState extends CustomState<OptionElementWidget> {
  late Future future;
  final TextEditingController _desController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  Map<dynamic, dynamic> belowOfElement = {};
  Map<dynamic, dynamic> middleOfElement = {};
  Map<dynamic, dynamic> aboveOfElement = {};
  Map<dynamic, dynamic> rightOfElement = {};
  Map<dynamic, dynamic> leftOfElement = {};
  Map<dynamic, dynamic> centerOfElement = {};
  List xpathList = [];
  final _formKey = GlobalKey<FormState>();
  int _status = 1;
  var content = {};
  var listElementsTemplate = [];
  String? elementTemplate;
  var resultAttributeList = [];
  bool isChooseElementTemplate = false;
  List repositoryPathList = [];
  dynamic selectedItem;
  String? repositoryId;
  @override
  void initState() {
    future = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    repositoryId = Provider.of<ElementRepositoryModel>(context, listen: false).selectRepositoryId;
    if (widget.id != null) {
      await getSelectedItem(widget.id!);
    }
    await setInitValue();
    if (repositoryId != null || selectedItem != null) {
      getRepositoryPath(repositoryId ?? selectedItem["tfTestElementRepositoryId"]);
    }
    await getListElementTemplate(widget.prjProjectId);
    return 0;
  }

  getSelectedItem(String id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-element/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        selectedItem = response["body"]["result"];
      });
    }
  }

  setInitValue() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      xpathList = [
        {"name": "", "xpath": ""}
      ];
      if (selectedItem != null) {
        if (selectedItem["tfFindElementTemplateId"] != null && selectedItem["tfFindElementTemplateId"] != "") {
          elementTemplate = selectedItem["tfFindElementTemplateId"];
          isChooseElementTemplate = true;
          await getElementTemplate(selectedItem["tfFindElementTemplateId"]);
          for (var resultAttribute in resultAttributeList) {
            if (resultAttribute["name"] != "position") {
              resultAttribute["value"] = selectedItem["content"][resultAttribute["name"]];
            }
          }
        }
        _desController.text = selectedItem["description"] ?? "";
        _nameController.text = selectedItem["name"] ?? "";
        if (selectedItem["content"]["xpathList"] != null) {
          xpathList = selectedItem["content"]["xpathList"];
        }
        if (selectedItem["content"]["tfTestElementAboveOfId"] != null) {
          await getAboveOfElement(selectedItem["content"]["tfTestElementAboveOfId"]);
        }
        if (selectedItem["content"]["tfTestElementBelowOfId"] != null) {
          await getBelowOfElement(selectedItem["content"]["tfTestElementBelowOfId"]);
        }
        if (selectedItem["content"]["tfTestElementCenterOfId"] != null) {
          await getCenterOfElement(selectedItem["content"]["tfTestElementCenterOfId"]);
        }
        if (selectedItem["content"]["tfTestElementLeftOfId"] != null) {
          await getLeftOfElement(selectedItem["content"]["tfTestElementLeftOfId"]);
        }
        if (selectedItem["content"]["tfTestElementMiddleOfId"] != null) {
          await getMiddleOfElement(selectedItem["content"]["tfTestElementMiddleOfId"]);
        }
        if (selectedItem["content"]["tfTestElementRightOfId"] != null) {
          await getRightOfElement(selectedItem["content"]["tfTestElementRightOfId"]);
        }
        _status = selectedItem["status"] ?? 1;
      } else if (widget.elementTemplateId != null && widget.elementTemplateId != "") {
        elementTemplate = widget.elementTemplateId;
        isChooseElementTemplate = true;
        await getElementTemplate(elementTemplate);
      }
      setState(() {});
    });
  }

  getAboveOfElement(elementId) async {
    var responseElement = await httpGet("/test-framework-api/user/test-framework/tf-test-element/$elementId", context);
    if (responseElement.containsKey("body") && responseElement["body"] is String == false && mounted) {
      var repositoryParentResponse = await httpGet(
          "/test-framework-api/user/test-framework/tf-test-element-repository/${responseElement["body"]["result"]["tfTestElementRepositoryId"]}/path",
          context);
      if (repositoryParentResponse.containsKey("body") && repositoryParentResponse["body"] is String == false) {
        setState(() {
          aboveOfElement["id"] = responseElement["body"]["result"]["id"];
          aboveOfElement["name"] = responseElement["body"]["result"]["name"];
          aboveOfElement["directoryPathList"] = repositoryParentResponse["body"]["resultList"];
        });
      }
    }
  }

  getBelowOfElement(elementId) async {
    var responseElement = await httpGet("/test-framework-api/user/test-framework/tf-test-element/$elementId", context);
    if (responseElement.containsKey("body") && responseElement["body"] is String == false && mounted) {
      var repositoryParentResponse = await httpGet(
          "/test-framework-api/user/test-framework/tf-test-element-repository/${responseElement["body"]["result"]["tfTestElementRepositoryId"]}/path",
          context);
      if (repositoryParentResponse.containsKey("body") && repositoryParentResponse["body"] is String == false) {
        setState(() {
          belowOfElement["id"] = responseElement["body"]["result"]["id"];
          belowOfElement["name"] = responseElement["body"]["result"]["name"];
          belowOfElement["directoryPathList"] = repositoryParentResponse["body"]["resultList"];
        });
      }
    }
  }

  getCenterOfElement(elementId) async {
    var responseElement = await httpGet("/test-framework-api/user/test-framework/tf-test-element/$elementId", context);
    if (responseElement.containsKey("body") && responseElement["body"] is String == false && mounted) {
      var repositoryParentResponse = await httpGet(
          "/test-framework-api/user/test-framework/tf-test-element-repository/${responseElement["body"]["result"]["tfTestElementRepositoryId"]}/path",
          context);
      if (repositoryParentResponse.containsKey("body") && repositoryParentResponse["body"] is String == false) {
        setState(() {
          centerOfElement["id"] = responseElement["body"]["result"]["id"];
          centerOfElement["name"] = responseElement["body"]["result"]["name"];
          centerOfElement["directoryPathList"] = repositoryParentResponse["body"]["resultList"];
        });
      }
    }
  }

  getLeftOfElement(elementId) async {
    var responseElement = await httpGet("/test-framework-api/user/test-framework/tf-test-element/$elementId", context);
    if (responseElement.containsKey("body") && responseElement["body"] is String == false && mounted) {
      var repositoryParentResponse = await httpGet(
          "/test-framework-api/user/test-framework/tf-test-element-repository/${responseElement["body"]["result"]["tfTestElementRepositoryId"]}/path",
          context);
      if (repositoryParentResponse.containsKey("body") && repositoryParentResponse["body"] is String == false) {
        setState(() {
          leftOfElement["id"] = responseElement["body"]["result"]["id"];
          leftOfElement["name"] = responseElement["body"]["result"]["name"];
          leftOfElement["directoryPathList"] = repositoryParentResponse["body"]["resultList"];
        });
      }
    }
  }

  getMiddleOfElement(elementId) async {
    var responseElement = await httpGet("/test-framework-api/user/test-framework/tf-test-element/$elementId", context);
    if (responseElement.containsKey("body") && responseElement["body"] is String == false && mounted) {
      var repositoryParentResponse = await httpGet(
          "/test-framework-api/user/test-framework/tf-test-element-repository/${responseElement["body"]["result"]["tfTestElementRepositoryId"]}/path",
          context);
      if (repositoryParentResponse.containsKey("body") && repositoryParentResponse["body"] is String == false) {
        setState(() {
          middleOfElement["id"] = responseElement["body"]["result"]["id"];
          middleOfElement["name"] = responseElement["body"]["result"]["name"];
          middleOfElement["directoryPathList"] = repositoryParentResponse["body"]["resultList"];
        });
      }
    }
  }

  getRightOfElement(elementId) async {
    var responseElement = await httpGet("/test-framework-api/user/test-framework/tf-test-element/$elementId", context);
    if (responseElement.containsKey("body") && responseElement["body"] is String == false && mounted) {
      var repositoryParentResponse = await httpGet(
          "/test-framework-api/user/test-framework/tf-test-element-repository/${responseElement["body"]["result"]["tfTestElementRepositoryId"]}/path",
          context);
      if (repositoryParentResponse.containsKey("body") && repositoryParentResponse["body"] is String == false) {
        setState(() {
          rightOfElement["id"] = responseElement["body"]["result"]["id"];
          rightOfElement["name"] = responseElement["body"]["result"]["name"];
          rightOfElement["directoryPathList"] = repositoryParentResponse["body"]["resultList"];
        });
      }
    }
  }

  getListElementTemplate(prjProjectId) async {
    var projectResponse = await httpGet("/test-framework-api/user/project/prj-project/$prjProjectId", context);
    if (projectResponse.containsKey("body") && projectResponse["body"] is String == false && mounted) {
      var findRequest = {"prjProjectId": prjProjectId, "status": 1, "prjProjectTypeId": projectResponse["body"]["result"]["prjProjectTypeId"]};
      var response = await httpPost("/test-framework-api/user/test-framework/tf-find-element-template/search", findRequest, context);
      if (response.containsKey("body") && response["body"] is String == false) {
        setState(() {
          listElementsTemplate = response["body"]["resultList"];
        });
      }
    }
  }

  getElementTemplate(elementId) async {
    var responseElementTemplate = await httpGet("/test-framework-api/user/test-framework/tf-find-element-template/$elementId", context);
    if (responseElementTemplate.containsKey("body") && responseElementTemplate["body"] is String == false) {
      setState(() {
        resultAttributeList = responseElementTemplate["body"]["result"]["content"]["resultAttributeList"] ?? [];
      });
    }
  }

  getRepositoryPath(String id) async {
    var repositoryParentResponse = await httpGet("/test-framework-api/user/test-framework/tf-test-element-repository/$id/path", context);
    if (repositoryParentResponse.containsKey("body") && repositoryParentResponse["body"] is String == false) {
      setState(() {
        repositoryPathList = repositoryParentResponse["body"]["resultList"];
      });
    }
  }

  @override
  void dispose() {
    _desController.dispose();
    _nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return FutureBuilder(
        future: future,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return TestFrameworkRootPageWidget(
              child: Column(
                children: [
                  headerWidget(context),
                  const Divider(color: Color.fromRGBO(216, 218, 229, 1)),
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.only(top: 20, bottom: 10),
                      child: Form(
                        key: _formKey,
                        child: SingleChildScrollView(
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                            child: FocusTraversalGroup(
                              policy: OrderedTraversalPolicy(),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin: const EdgeInsets.symmetric(vertical: 10),
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Expanded(
                                          flex: 3,
                                          child: MultiLanguageText(
                                              name: "create_element_repository", defaultValue: "Repository", style: Style(context).styleTitleDialog),
                                        ),
                                        Expanded(
                                          flex: 7,
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              (repositoryPathList.isNotEmpty)
                                                  ? Expanded(
                                                      child: Row(
                                                        children: [
                                                          for (var directoryPath in repositoryPathList)
                                                            RichText(
                                                              text: TextSpan(
                                                                children: [
                                                                  TextSpan(
                                                                    text: directoryPath["title"],
                                                                    style: Style(context).styleTextDataCell,
                                                                  ),
                                                                  if (repositoryPathList.last != directoryPath)
                                                                    const TextSpan(
                                                                      text: " / ",
                                                                      style: TextStyle(color: Colors.grey),
                                                                    ),
                                                                ],
                                                              ),
                                                            ),
                                                        ],
                                                      ),
                                                    )
                                                  : const Expanded(
                                                      child: SizedBox.shrink(),
                                                    ),
                                              // if (widget.type == "create")
                                              FloatingActionButton.extended(
                                                heroTag: null,
                                                onPressed: () {
                                                  showDialog<String>(
                                                    context: context,
                                                    builder: (BuildContext context) {
                                                      return DialogAnalyze(
                                                        onClickContinue: (repositoryId, client, requestBody, context) async {
                                                          Navigator.pop(context);
                                                          this.repositoryId = repositoryId;
                                                          getRepositoryPath(repositoryId);
                                                        },
                                                        title: multiLanguageString(name: "repository", defaultValue: "Repository", context: context),
                                                      );
                                                    },
                                                  );
                                                },
                                                icon: const Icon(Icons.folder, color: Colors.white),
                                                label: MultiLanguageText(
                                                  name: "repository",
                                                  defaultValue: "Repository",
                                                  style: TextStyle(
                                                      fontSize: context.fontSizeManager.fontSizeText13,
                                                      fontWeight: FontWeight.w600,
                                                      color: Colors.white),
                                                ),
                                                backgroundColor: Colors.blue,
                                                elevation: 0,
                                                hoverElevation: 0,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: const EdgeInsets.symmetric(vertical: 10),
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Expanded(
                                          flex: 3,
                                          child: MultiLanguageText(
                                              name: "create_element_template",
                                              defaultValue: "Element Template",
                                              style: Style(context).styleTitleDialog),
                                        ),
                                        Expanded(
                                          flex: 7,
                                          child: DynamicDropdownButton(
                                            borderRadius: 5,
                                            value: elementTemplate,
                                            hint: multiLanguageString(
                                                name: "choose_an_element_template", defaultValue: "Choose an element template", context: context),
                                            items: listElementsTemplate.map<DropdownMenuItem<String>>((dynamic result) {
                                              return DropdownMenuItem(
                                                value: result["id"],
                                                child: Text(
                                                  result["title"],
                                                ),
                                              );
                                            }).toList(),
                                            onChanged: (newValue) async {
                                              setState(() {
                                                elementTemplate = newValue!;
                                                isChooseElementTemplate = true;
                                              });
                                              await getElementTemplate(newValue);
                                            },
                                            isRequiredNotEmpty: true,
                                            enabled: !(widget.type == "edit" || widget.type == "duplicate"),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  if (isChooseElementTemplate)
                                    Container(
                                      margin: const EdgeInsets.symmetric(vertical: 10),
                                      child: Row(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Expanded(
                                            flex: 3,
                                            child: MultiLanguageText(name: "name", defaultValue: "Name", style: Style(context).styleTitleDialog),
                                          ),
                                          Expanded(
                                            flex: 7,
                                            child: DynamicTextField(
                                              controller: _nameController,
                                              hintText: "...",
                                              isRequiredNotEmpty: true,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  if (resultAttributeList.isNotEmpty)
                                    for (var resultAttribute in resultAttributeList)
                                      if (resultAttribute["name"] != "position")
                                        Container(
                                          margin: const EdgeInsets.symmetric(vertical: 10),
                                          child: Row(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Expanded(
                                                flex: 3,
                                                child: Text(resultAttribute["name"], style: Style(context).styleTitleDialog),
                                              ),
                                              Expanded(
                                                flex: 7,
                                                child: DynamicTextField(
                                                  controller: TextEditingController(text: resultAttribute["value"]),
                                                  hintText: "...",
                                                  onChanged: (text) {
                                                    resultAttribute["value"] = text;
                                                  },
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                  propertyFindElement(name: "below_of", title: "Below of", element: belowOfElement),
                                  propertyFindElement(name: "middle_of", title: "Middle of", element: middleOfElement),
                                  propertyFindElement(name: "above_of", title: "Above of", element: aboveOfElement),
                                  propertyFindElement(name: "right_of", title: "Right of", element: rightOfElement),
                                  propertyFindElement(name: "center_of", title: "Center of", element: centerOfElement),
                                  propertyFindElement(name: "left_of", title: "Left of", element: leftOfElement),
                                  Container(
                                    margin: const EdgeInsets.symmetric(vertical: 10),
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Expanded(
                                          flex: 3,
                                          child: MultiLanguageText(
                                              name: "create_element_description",
                                              defaultValue: "Description",
                                              style: Style(context).styleTitleDialog),
                                        ),
                                        Expanded(
                                          flex: 7,
                                          child: DynamicTextField(
                                            controller: _desController,
                                            hintText: "...",
                                            maxline: 3,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: const EdgeInsets.symmetric(vertical: 10),
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Expanded(
                                          flex: 3,
                                          child: MultiLanguageText(
                                              name: "create_element_status", defaultValue: "Status", style: Style(context).styleTitleDialog),
                                        ),
                                        Expanded(
                                          flex: 7,
                                          child: DynamicDropdownButton(
                                              borderRadius: 5,
                                              value: _status,
                                              hint: multiLanguageString(name: "choose_a_status", defaultValue: "Choose a status", context: context),
                                              items: [
                                                {"name": multiLanguageString(name: "active", defaultValue: "Active", context: context), "value": 1},
                                                {
                                                  "name": multiLanguageString(name: "inactive", defaultValue: "Inactive", context: context),
                                                  "value": 0
                                                },
                                              ].map<DropdownMenuItem<int>>((dynamic result) {
                                                return DropdownMenuItem(
                                                  value: result["value"],
                                                  child: Text(result["name"]),
                                                );
                                              }).toList(),
                                              onChanged: (newValue) {
                                                setState(() {
                                                  _status = newValue!;
                                                });
                                              },
                                              isRequiredNotEmpty: true),
                                        ),
                                      ],
                                    ),
                                  ),
                                  if (isChooseElementTemplate)
                                    Column(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.symmetric(vertical: 10),
                                          child: Row(
                                            children: [
                                              HeaderItemTable(title: multiLanguageString(name: "name", defaultValue: "Name", context: context)),
                                              HeaderItemTable(title: multiLanguageString(name: "xpath", defaultValue: "Xpath", context: context)),
                                            ],
                                          ),
                                        ),
                                        for (var xpath in xpathList) xpathItemWidget(xpath),
                                        Container(
                                          margin: const EdgeInsets.symmetric(vertical: 5),
                                          child: FloatingActionButton.extended(
                                            heroTag: null,
                                            icon: const Icon(Icons.add),
                                            label: const MultiLanguageText(name: "new_xpath", defaultValue: "New xpath"),
                                            onPressed: () {
                                              setState(() {
                                                xpathList.add({"name": "", "xpath": ""});
                                              });
                                            },
                                          ),
                                        ),
                                      ],
                                    )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20, top: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ButtonSave(onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            var result = {};
                            if (selectedItem != null && selectedItem.isNotEmpty && widget.type != "duplicate") {
                              result = selectedItem;
                              result["description"] = _desController.text;
                              result["name"] = _nameController.text;
                              result["status"] = _status;
                              result["tfFindElementTemplateId"] = elementTemplate;
                              result["tfTestElementRepositoryId"] = repositoryId;
                              result["content"]["xpathList"] = xpathList;
                              result["content"]["tfTestElementAboveOfId"] = aboveOfElement["id"];
                              result["content"]["tfTestElementAboveOfXpathName"] = aboveOfElement["name"];
                              result["content"]["tfTestElementBelowOfId"] = belowOfElement["id"];
                              result["content"]["tfTestElementBelowOfXpathName"] = belowOfElement["name"];
                              result["content"]["tfTestElementMiddleOfId"] = middleOfElement["id"];
                              result["content"]["tfTestElementMiddleOfXpathName"] = middleOfElement["name"];
                              result["content"]["tfTestElementRightOfId"] = rightOfElement["id"];
                              result["content"]["tfTestElementRightOfXpathName"] = rightOfElement["name"];
                              result["content"]["tfTestElementCenterOfId"] = centerOfElement["id"];
                              result["content"]["tfTestElementCenterOfXpathName"] = centerOfElement["name"];
                              result["content"]["tfTestElementLeftOfId"] = leftOfElement["id"];
                              result["content"]["tfTestElementLeftOfXpathName"] = leftOfElement["name"];
                              for (var resultAttribute in resultAttributeList) {
                                if (resultAttribute["name"] != "position") result["content"][resultAttribute["name"]] = resultAttribute["value"];
                              }
                            } else {
                              result = {
                                "description": _desController.text,
                                "name": _nameController.text,
                                "tfTestElementRepositoryId": repositoryId,
                                "status": _status,
                                "prjProjectId": widget.prjProjectId,
                                "tfFindElementTemplateId": elementTemplate,
                                "content": {
                                  "xpathList": xpathList,
                                  "tfTestElementAboveOfId": aboveOfElement["id"],
                                  "tfTestElementAboveOfXpathName": aboveOfElement["name"],
                                  "tfTestElementBelowOfId": belowOfElement["id"],
                                  "tfTestElementBelowOfXpathName": belowOfElement["name"],
                                  "tfTestElementMiddleOfId": middleOfElement["id"],
                                  "tfTestElementMiddleOfXpathName": middleOfElement["name"],
                                  "tfTestElementRightOfId": rightOfElement["id"],
                                  "tfTestElementRightOfXpathName": rightOfElement["name"],
                                  "tfTestElementCenterOfId": centerOfElement["id"],
                                  "tfTestElementCenterOfXpathName": centerOfElement["name"],
                                  "tfTestElementLeftOfId": leftOfElement["id"],
                                  "tfTestElementLeftOfXpathName": leftOfElement["name"],
                                  for (var resultAttribute in resultAttributeList)
                                    if (resultAttribute["name"] != "position") resultAttribute["name"]: resultAttribute["value"],
                                }
                              };
                            }
                            widget.callbackOptionTest!(result);
                          }
                        }),
                        const ButtonCancel()
                      ],
                    ),
                  )
                ],
              ),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        },
      );
    });
  }

  Row xpathItemWidget(xpath) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(right: 2),
            child: DynamicTextField(
              controller: TextEditingController(text: xpath["name"] ?? ""),
              hintText: multiLanguageString(name: "enter_a_name", defaultValue: "Enter a name", context: context),
              onChanged: (text) {
                xpath["name"] = text;
              },
              isRequiredNotEmpty: true,
            ),
          ),
        ),
        Expanded(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 2),
                  child: DynamicTextField(
                    controller: TextEditingController(text: xpath["xpath"] ?? ""),
                    hintText: multiLanguageString(name: "enter_a_xpath", defaultValue: "Enter a xpath", context: context),
                    onChanged: (text) {
                      xpath["xpath"] = text;
                    },
                    isRequiredNotEmpty: true,
                  ),
                ),
              ),
              if (widget.isStartDebug)
                Container(
                  margin: const EdgeInsets.only(left: 10),
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(backgroundColor: const Color.fromARGB(255, 255, 63, 49), fixedSize: const Size(130, 47)),
                      onPressed: () {
                        Navigator.of(context).push(
                          createRoute(
                            WebElementConfigWidget((xpathCallback, element) {
                              setState(() {
                                xpath["xpath"] = xpathCallback["xpath"];
                                if (xpath["name"].length == 0) {
                                  xpath["name"] = xpathCallback["name"];
                                }
                                for (var resultAttribute in resultAttributeList) {
                                  resultAttribute["value"] = element[resultAttribute["name"]];
                                }
                              });
                            }, elementTemplate: elementTemplate),
                          ),
                        );
                      },
                      child: const MultiLanguageText(name: "find_element", defaultValue: "Find element")),
                ),
              Container(
                margin: const EdgeInsets.only(left: 10),
                height: 46,
                child: Tooltip(
                  message: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(backgroundColor: Colors.red, padding: const EdgeInsets.symmetric(horizontal: 11)),
                    onPressed: () {
                      setState(() {
                        xpathList.remove(xpath);
                      });
                    },
                    child: const Icon(Icons.delete, color: Colors.white),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Padding headerWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 35, 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "type_element",
            defaultValue: "\$0 ELEMENT",
            variables: [widget.title],
            isLowerCase: false,
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
            onPressed: () => Navigator.pop(context, 'cancel'),
            child: const Icon(
              Icons.close,
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }

  Widget propertyFindElement({required String name, required String title, dynamic element}) {
    String value = "";
    if (element["directoryPathList"] != null && element["directoryPathList"].isNotEmpty) {
      for (var item in element["directoryPathList"]) {
        value += item["title"] + " / ";
      }
    }
    value += element["name"] ?? "";
    return StatefulBuilder(
      builder: (BuildContext context, StateSetter setState) {
        return Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: name, defaultValue: title, style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: Row(
                  children: [
                    Expanded(
                      child: DynamicTextField(
                        controller: TextEditingController(text: value),
                        hintText: multiLanguageString(name: "find_element", defaultValue: "Find element", context: context),
                        readOnly: true,
                        suffixIcon: element["id"] != null
                            ? GestureDetector(
                                onTap: () {
                                  setState(() {
                                    element["id"] = null;
                                    value = '';
                                  });
                                },
                                child: const Icon(
                                  Icons.close,
                                ),
                              )
                            : null,
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 10),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(backgroundColor: const Color.fromARGB(255, 255, 63, 49), fixedSize: const Size(130, 47)),
                        onPressed: () {
                          Navigator.of(context).push(
                            createRoute(
                              ElementRepositoryConfigWidget((parentId, valueCallback, xpath, name) async {
                                element["id"] = valueCallback["id"];
                                var repositoryParentResponse = await httpGet(
                                    "/test-framework-api/user/test-framework/tf-test-element-repository/${valueCallback["tfTestElementRepositoryId"]}/path",
                                    context);
                                if (repositoryParentResponse.containsKey("body") && repositoryParentResponse["body"] is String == false) {
                                  setState(() {
                                    element["directoryPathList"] = repositoryParentResponse["body"]["resultList"];
                                    setState(() {
                                      value = '';
                                      if (element["directoryPathList"] != null && element["directoryPathList"].isNotEmpty) {
                                        for (var item in element["directoryPathList"]) {
                                          value += item["title"] + " / ";
                                        }
                                      }
                                      value += valueCallback["name"];
                                    });
                                  });
                                }
                              }),
                            ),
                          );
                        },
                        child: const MultiLanguageText(name: "create_element_find_element", defaultValue: "Find element"),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
