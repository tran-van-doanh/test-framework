import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';
import '../../api.dart';
import '../../common/custom_state.dart';

class OptionElementRepositoryWidget extends StatefulWidget {
  final String type;
  final String title;
  final String? id;
  final Function? callbackOptionTest;
  final String prjProjectId;
  const OptionElementRepositoryWidget({
    Key? key,
    required this.type,
    this.id,
    this.callbackOptionTest,
    required this.prjProjectId,
    required this.title,
  }) : super(key: key);

  @override
  State<OptionElementRepositoryWidget> createState() => _OptionElementRepositoryWidgetState();
}

class _OptionElementRepositoryWidgetState extends CustomState<OptionElementRepositoryWidget> {
  final TextEditingController _desController = TextEditingController();
  final TextEditingController _titleController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  int _status = 1;
  dynamic selectedItem;
  late Future future;

  @override
  void initState() {
    future = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    if (widget.id != null) {
      await getSelectedItem(widget.id!);
      await setInitValue();
    }
    return 0;
  }

  getSelectedItem(String id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-element-repository/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        selectedItem = response["body"]["result"];
      });
    }
  }

  setInitValue() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      setState(() {
        if (selectedItem != null) {
          _desController.text = selectedItem["description"] ?? "";
          _titleController.text = selectedItem["title"] ?? "";
          _status = selectedItem["status"] ?? 1;
        }
      });
    });
  }

  @override
  void dispose() {
    _desController.dispose();
    _titleController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return FutureBuilder(
        future: future,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return TestFrameworkRootPageWidget(
              child: Column(
                children: [
                  headerWidget(context),
                  const Divider(color: Color.fromRGBO(216, 218, 229, 1)),
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.only(top: 20, bottom: 10),
                      child: Form(
                        key: _formKey,
                        child: SingleChildScrollView(
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                            child: FocusTraversalGroup(
                              policy: OrderedTraversalPolicy(),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin: const EdgeInsets.symmetric(vertical: 10),
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Expanded(
                                          flex: 3,
                                          child: MultiLanguageText(
                                              name: "create_element_repository_name", defaultValue: "Name", style: Style(context).styleTitleDialog),
                                        ),
                                        Expanded(
                                          flex: 7,
                                          child: DynamicTextField(
                                            controller: _titleController,
                                            hintText: "...",
                                            isRequiredNotEmpty: true,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: const EdgeInsets.symmetric(vertical: 10),
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Expanded(
                                          flex: 3,
                                          child: MultiLanguageText(
                                              name: "create_element_repository_description",
                                              defaultValue: "Description",
                                              style: Style(context).styleTitleDialog),
                                        ),
                                        Expanded(
                                          flex: 7,
                                          child: DynamicTextField(
                                            controller: _desController,
                                            hintText: "...",
                                            maxline: 3,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: const EdgeInsets.symmetric(vertical: 10),
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Expanded(
                                          flex: 3,
                                          child: MultiLanguageText(
                                              name: "create_element_repository_status",
                                              defaultValue: "Status",
                                              style: Style(context).styleTitleDialog),
                                        ),
                                        Expanded(
                                          flex: 7,
                                          child: DynamicDropdownButton(
                                              borderRadius: 5,
                                              value: _status,
                                              hint: multiLanguageString(name: "choose_a_status", defaultValue: "Choose a status", context: context),
                                              items: [
                                                {"name": multiLanguageString(name: "active", defaultValue: "Active", context: context), "value": 1},
                                                {
                                                  "name": multiLanguageString(name: "inactive", defaultValue: "Inactive", context: context),
                                                  "value": 0
                                                },
                                              ].map<DropdownMenuItem<int>>((dynamic result) {
                                                return DropdownMenuItem(
                                                  value: result["value"],
                                                  child: Text(result["name"]),
                                                );
                                              }).toList(),
                                              onChanged: (newValue) {
                                                setState(() {
                                                  _status = newValue!;
                                                });
                                              },
                                              isRequiredNotEmpty: true),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20, top: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ButtonSave(onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            var result = {};
                            if (selectedItem != null) {
                              result = selectedItem;
                              result["title"] = _titleController.text;
                              result["description"] = _desController.text;
                              result["status"] = _status;
                            } else {
                              result = {
                                "title": _titleController.text,
                                "description": _desController.text,
                                "status": _status,
                                "prjProjectId": widget.prjProjectId,
                              };
                            }
                            widget.callbackOptionTest!(result);
                          }
                        }),
                        const ButtonCancel()
                      ],
                    ),
                  )
                ],
              ),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        },
      );
    });
  }

  Padding headerWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 35, 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "type_element_repository",
            defaultValue: "\$0 ELEMENT REPOSITORY",
            variables: [widget.title],
            isLowerCase: false,
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
            onPressed: () => Navigator.pop(context, 'cancel'),
            child: const Icon(
              Icons.close,
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }
}
