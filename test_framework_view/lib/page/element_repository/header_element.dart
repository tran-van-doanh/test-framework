import 'dart:async';

import 'package:flutter/material.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';

import '../../common/custom_state.dart';

class HeaderElementWidget extends StatefulWidget {
  final Function? callbackFilter;
  final String? dx;
  final String? dy;
  final String? selectElementId;
  final bool isShowFilterForm;
  final VoidCallback onClickFilterForm;
  final String prjProjectId;
  const HeaderElementWidget({
    Key? key,
    this.callbackFilter,
    this.dx,
    this.dy,
    this.isShowFilterForm = true,
    required this.onClickFilterForm,
    required this.prjProjectId,
    this.selectElementId,
  }) : super(key: key);

  @override
  State<HeaderElementWidget> createState() => _HeaderElementWidgetState();
}

class _HeaderElementWidgetState extends CustomState<HeaderElementWidget> {
  final TextEditingController elementTemplateSearchController = TextEditingController();
  final TextEditingController dxController = TextEditingController();
  final TextEditingController dyController = TextEditingController();
  Timer? _debounceTimer;
  String selectElementId = "";
  String? projectType;
  List<dynamic> listProjectType = [
    {"id": null, "title": "All"}
  ];
  List<dynamic> listElementsTemplate = [
    {"id": "", "title": "All Template"}
  ];
  @override
  void initState() {
    getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    await getListProjectType();
    await getCurrentProjectType();
    await getListElementTemplate();
    if (widget.selectElementId != null) {
      setState(() {
        selectElementId = widget.selectElementId!;
      });
    }
  }

  getCurrentProjectType() async {
    var projectResponse = await httpGet("/test-framework-api/user/project/prj-project/${widget.prjProjectId}", context);
    if (projectResponse.containsKey("body") && projectResponse["body"] is String == false) {
      projectType = projectResponse["body"]["result"]["prjProjectTypeId"];
    }
  }

  handleCallBackSearchFunction({String? dx, String? dy}) {
    var result = {
      "nameLike": elementTemplateSearchController.text,
      "tfFindElementTemplateId": selectElementId,
      "dx": dx,
      "dy": dy,
    };
    widget.callbackFilter!(result);
  }

  getListProjectType() async {
    var findRequest = {"prjProjectId": widget.prjProjectId, "status": 1};
    var response = await httpPost("/test-framework-api/user/project/prj-project-type/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        listProjectType.addAll(response["body"]["resultList"]);
      });
    }
  }

  getListElementTemplate() async {
    var findRequest = {"prjProjectId": widget.prjProjectId, "status": 1, "prjProjectTypeId": projectType};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-find-element-template/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        listElementsTemplate = [
          {"id": "", "title": "All Template"}
        ];
        listElementsTemplate.addAll(response["body"]["resultList"]);
      });
    }
  }

  @override
  void dispose() {
    _debounceTimer?.cancel();
    elementTemplateSearchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    dxController.text = widget.dx ?? "";
    dyController.text = widget.dy ?? "";
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      List<Widget> widgetList = [
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
          child: SizedBox(
            height: 50,
            child: DynamicTextField(
              controller: elementTemplateSearchController,
              hintText: multiLanguageString(name: "by_element_name", defaultValue: "By element name", context: context),
              onChanged: (value) {
                setState(() {
                  elementTemplateSearchController.text;
                });
                if (_debounceTimer?.isActive ?? false) {
                  _debounceTimer?.cancel();
                }

                _debounceTimer = Timer(const Duration(milliseconds: 500), () {
                  handleCallBackSearchFunction(dx: dxController.text, dy: dyController.text);
                });
              },
              suffixIcon: (elementTemplateSearchController.text != "")
                  ? IconButton(
                      onPressed: () {
                        setState(() {
                          elementTemplateSearchController.clear();
                        });
                        handleCallBackSearchFunction(dx: dxController.text, dy: dyController.text);
                      },
                      icon: const Icon(Icons.clear),
                      splashRadius: 20,
                    )
                  : null,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
          child: SizedBox(
            height: 50,
            child: DynamicDropdownButton(
              borderRadius: 5,
              value: projectType,
              hint: multiLanguageString(name: "choose_a_project_type", defaultValue: "Choose a project type", context: context),
              items: listProjectType.map<DropdownMenuItem<String>>((dynamic result) {
                return DropdownMenuItem(
                  value: result["id"],
                  child: Text(
                    result["title"],
                  ),
                );
              }).toList(),
              onChanged: (newValue) async {
                setState(() {
                  projectType = newValue;
                  selectElementId = "";
                });
                getListElementTemplate();
                handleCallBackSearchFunction();
              },
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
          child: SizedBox(
            height: 50,
            child: DynamicDropdownButton(
              borderRadius: 5,
              value: selectElementId,
              hint: multiLanguageString(name: "choose_an_element_template", defaultValue: "Choose an element template", context: context),
              items: listElementsTemplate.map<DropdownMenuItem<String>>((dynamic result) {
                return DropdownMenuItem(
                  value: result["id"],
                  child: Text(
                    result["title"],
                  ),
                );
              }).toList(),
              onChanged: (newValue) async {
                setState(() {
                  selectElementId = newValue;
                });
                handleCallBackSearchFunction();

                // await getElementTemplate(newValue);
                // await getListTfElement(1, findRequestElement);
              },
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
          child: SizedBox(
            height: 50,
            child: DynamicTextField(
              controller: dxController,
              hintText: multiLanguageString(name: "by_position_x", defaultValue: "By position x", context: context),
              keyboardType: TextInputType.number,
              onChanged: (value) {
                if (_debounceTimer?.isActive ?? false) {
                  _debounceTimer?.cancel();
                }
                _debounceTimer = Timer(const Duration(milliseconds: 500), () {
                  handleCallBackSearchFunction(dx: value, dy: widget.dy);
                });
              },
              suffixIcon: (widget.dx != null && widget.dx != "")
                  ? IconButton(
                      onPressed: () {
                        handleCallBackSearchFunction(dx: null, dy: widget.dy);
                      },
                      icon: const Icon(Icons.clear),
                      splashRadius: 20,
                    )
                  : null,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
          child: SizedBox(
            height: 50,
            child: DynamicTextField(
              controller: dyController,
              hintText: multiLanguageString(name: "by_position_y", defaultValue: "By position y", context: context),
              keyboardType: TextInputType.number,
              onChanged: (value) {
                if (_debounceTimer?.isActive ?? false) {
                  _debounceTimer?.cancel();
                }
                _debounceTimer = Timer(const Duration(milliseconds: 500), () {
                  handleCallBackSearchFunction(dx: widget.dx, dy: value);
                });
              },
              suffixIcon: (widget.dy != null && widget.dy != "")
                  ? IconButton(
                      onPressed: () {
                        handleCallBackSearchFunction(dx: widget.dx, dy: null);
                      },
                      icon: const Icon(Icons.clear),
                      splashRadius: 20,
                    )
                  : null,
            ),
          ),
        ),
      ];
      var itemPerRow = 4;
      if (constraints.maxWidth < 600) {
        itemPerRow = 1;
      } else if (constraints.maxWidth < 800) {
        itemPerRow = 2;
      } else if (constraints.maxWidth < 1200) {
        itemPerRow = 3;
      }
      var rowCount = (1.0 * widgetList.length / itemPerRow).ceil();
      var filterWidget = Column(
        children: [
          for (var rowI = 0; rowI < rowCount; rowI++)
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                for (var colI = 0; colI < itemPerRow; colI++)
                  rowI * itemPerRow + colI < widgetList.length ? Expanded(child: widgetList[rowI * itemPerRow + colI]) : Expanded(child: Container())
              ],
            )
        ],
      );

      return Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              MultiLanguageText(
                name: "filter_by_element",
                defaultValue: "Filter by element",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: const Color.fromRGBO(0, 0, 0, 0.8),
                    fontSize: context.fontSizeManager.fontSizeText16,
                    letterSpacing: 2),
              ),
              IconButton(
                onPressed: () {
                  widget.onClickFilterForm();
                },
                icon: widget.isShowFilterForm ? const Icon(Icons.visibility) : const Icon(Icons.visibility_off),
                splashRadius: 1,
                tooltip: widget.isShowFilterForm
                    ? multiLanguageString(name: "hide_filter_form", defaultValue: "Hide filter form", context: context)
                    : multiLanguageString(name: "show_filter_form", defaultValue: "Show filter form", context: context),
              )
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (widget.isShowFilterForm) filterWidget,
              if (widgetList.length > 2)
                ElevatedButton(
                  onPressed: () {
                    setState(() {
                      elementTemplateSearchController.clear();
                      dxController.clear();
                      dyController.clear();
                      selectElementId = "";
                      handleCallBackSearchFunction();
                    });
                  },
                  child: const MultiLanguageText(name: "reset", defaultValue: "Reset", isLowerCase: false),
                ),
            ],
          ),
        ],
      );
    });
  }
}
