import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';

import '../../common/custom_state.dart';

class HeaderRepositoryWidget extends StatefulWidget {
  final int countList;
  final String? directoryParentName;
  final Function? callbackFilter;
  final bool isShowFilterFormRepository;
  const HeaderRepositoryWidget({
    Key? key,
    required this.countList,
    this.directoryParentName,
    this.callbackFilter,
    required this.isShowFilterFormRepository,
  }) : super(key: key);

  @override
  State<HeaderRepositoryWidget> createState() => _HeaderRepositoryWidgetState();
}

class _HeaderRepositoryWidgetState extends CustomState<HeaderRepositoryWidget> {
  final TextEditingController _searchLibrary = TextEditingController();
  bool isShowFilterForm = true;
  bool isSearched = false;
  handleCallBackSearchFunction() {
    var result = {
      if (_searchLibrary.text.isNotEmpty) "titleLike": _searchLibrary.text,
    };
    widget.callbackFilter!(result);
  }

  @override
  void dispose() {
    _searchLibrary.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      List<Widget> widgetList = [
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
          child: SizedBox(
            height: 50,
            child: DynamicTextField(
              controller: _searchLibrary,
              onComplete: () => handleCallBackSearchFunction(),
              onChanged: (value) {
                setState(() {
                  _searchLibrary.text;
                });
              },
              labelText:
                  multiLanguageString(name: "search_element_repository_name", defaultValue: "Search element repository name", context: context),
              hintText: multiLanguageString(name: "by_element_repository_name", defaultValue: "By element repository name", context: context),
              suffixIcon: (_searchLibrary.text != "")
                  ? IconButton(
                      onPressed: () {
                        setState(() {
                          _searchLibrary.clear();
                          if (isSearched) {
                            handleCallBackSearchFunction();
                          }
                        });
                      },
                      icon: const Icon(Icons.clear),
                      splashRadius: 20,
                    )
                  : null,
            ),
          ),
        ),
      ];
      var itemPerRow = 4;
      if (constraints.maxWidth < 600) {
        itemPerRow = 1;
      } else if (constraints.maxWidth < 800) {
        itemPerRow = 2;
      } else if (constraints.maxWidth < 1200) {
        itemPerRow = 3;
      }
      var rowCount = (1.0 * widgetList.length / itemPerRow).ceil();
      var filterWidget = Column(
        children: [
          for (var rowI = 0; rowI < rowCount; rowI++)
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                for (var colI = 0; colI < itemPerRow; colI++)
                  rowI * itemPerRow + colI < widgetList.length ? Expanded(child: widgetList[rowI * itemPerRow + colI]) : Expanded(child: Container())
              ],
            )
        ],
      );
      Widget buttons = Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 20),
            child: ElevatedButton(
              onPressed: () {
                isSearched = true;
                handleCallBackSearchFunction();
              },
              child: const MultiLanguageText(name: "search", defaultValue: "Search", isLowerCase: false),
            ),
          ),
          if (widgetList.length > 2)
            ElevatedButton(
              onPressed: () {
                setState(() {
                  _searchLibrary.clear();
                  widget.callbackFilter!({});
                });
              },
              child: const MultiLanguageText(name: "reset", defaultValue: "Reset", isLowerCase: false),
            ),
        ],
      );
      return Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              if (widget.directoryParentName != null)
                IconButton(
                  onPressed: () {
                    Provider.of<NavigationModel>(context, listen: false)
                        .pop("/project/${Provider.of<NavigationModel>(context, listen: false).prjProjectId}/element-repository");
                  },
                  icon: const Icon(Icons.arrow_back),
                  padding: EdgeInsets.zero,
                  splashRadius: 16,
                ),
              Expanded(
                child: MultiLanguageText(
                  name: "element_repository",
                  defaultValue: "${widget.directoryParentName != null ? "${widget.directoryParentName} " : ""}Element Repository (\$0)",
                  variables: ["${widget.countList}"],
                  style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
                ),
              ),
            ],
          ),
          if (widget.isShowFilterFormRepository)
            Row(
              children: [
                MultiLanguageText(
                  name: "filter_form",
                  defaultValue: "Filter form",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: const Color.fromRGBO(0, 0, 0, 0.8),
                      fontSize: context.fontSizeManager.fontSizeText16,
                      letterSpacing: 2),
                ),
                IconButton(
                  onPressed: () {
                    setState(() {
                      isShowFilterForm = !isShowFilterForm;
                    });
                  },
                  icon: isShowFilterForm ? const Icon(Icons.visibility) : const Icon(Icons.visibility_off),
                  splashRadius: 1,
                  tooltip: isShowFilterForm
                      ? multiLanguageString(name: "hide_filter_form", defaultValue: "Hide filter form", context: context)
                      : multiLanguageString(name: "show_filter_form", defaultValue: "Show filter form", context: context),
                )
              ],
            ),
          if (isShowFilterForm && widget.isShowFilterFormRepository)
            (widgetList.length <= 2)
                ? Wrap(
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: [
                      for (var widget in widgetList) SizedBox(width: 450, child: widget),
                      IntrinsicWidth(
                        child: buttons,
                      )
                    ],
                  )
                : Column(
                    children: [
                      filterWidget,
                      buttons,
                    ],
                  ),
        ],
      );
    });
  }
}
