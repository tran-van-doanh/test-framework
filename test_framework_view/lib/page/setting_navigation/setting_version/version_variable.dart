import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dynamic_play_form.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';

import 'package:test_framework_view/type.dart';

import '../../../common/custom_state.dart';

class VersionVariableBody extends StatefulWidget {
  final String versionId;
  const VersionVariableBody({Key? key, required this.versionId}) : super(key: key);

  @override
  State<VersionVariableBody> createState() => _VersionVariableBodyState();
}

class _VersionVariableBodyState extends CustomState<VersionVariableBody> {
  late Future futureParameterListPlay;
  var resultparameterFieldList = [];
  var versionResult = {};
  bool isViewColumn = false;

  final _formKey = GlobalKey<FormState>();
  var prjProjectId = "";

  @override
  void initState() {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    prjProjectId = navigationModel.prjProjectId ?? "";
    futureParameterListPlay = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    await getParameterFieldList();
    await getVersionVariable();
    return 0;
  }

  getParameterFieldList() async {
    var response = await httpGet("/test-framework-api/user/project/prj-project/$prjProjectId", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        if (response["body"]["result"]["projectConfig"] != null) {
          resultparameterFieldList = response["body"]["result"]["projectConfig"]["parameterFieldList"] ?? [];
        }
      });
    }
  }

  getVersionVariable() async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-version/${widget.versionId}", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        versionResult = response["body"]["result"];
        versionResult["versionConfig"] = versionResult["versionConfig"] ?? {};
      });
    }
  }

  handleUpdateButton(navigationModel) async {
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-version/${widget.versionId}", versionResult, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        navigationModel.navigate("/qa-platform/project/${navigationModel.prjProjectId}/setting/version");
        showToast(
            context: context,
            msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, LanguageModel>(builder: (context, navigationModel, languageModel, child) {
      return FutureBuilder(
        future: futureParameterListPlay,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                NavMenuTabV1(
                  currentUrl: "/setting/version",
                  margin: const EdgeInsets.fromLTRB(50, 20, 50, 0),
                  navigationList: context.settingMenuList,
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(50, 30, 50, 10),
                      child: Column(
                        children: [
                          headerWidget(navigationModel),
                          resultparameterFieldList.isNotEmpty
                              ? Form(
                                  key: _formKey,
                                  child: Container(
                                    margin: const EdgeInsets.only(top: 30),
                                    child: Column(
                                      children: [
                                        Row(
                                          children: [
                                            Expanded(
                                              child: Center(
                                                child: TableViewVersionWidget(
                                                  isViewColumn: isViewColumn,
                                                  resultparameterFieldList:
                                                      resultparameterFieldList.where((element) => element["variableType"] == "version_variable"),
                                                  versionConfig: versionResult["versionConfig"],
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        const SizedBox(
                                          height: 20,
                                        ),
                                        SizedBox(
                                          height: 40,
                                          child: ElevatedButton(
                                            onPressed: () async {
                                              if (_formKey.currentState!.validate()) {
                                                await handleUpdateButton(navigationModel);
                                              }
                                            },
                                            child: const MultiLanguageText(
                                              name: "update",
                                              defaultValue: "Update",
                                              isLowerCase: false,
                                              style: TextStyle(color: Colors.white),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              : Container(
                                  margin: const EdgeInsets.only(top: 30),
                                  child: Column(
                                    children: [
                                      MultiLanguageText(
                                        name: "not_parameter_field",
                                        defaultValue: "Not parameterField !!",
                                        style: TextStyle(fontSize: context.fontSizeManager.fontSizeText20, fontWeight: FontWeight.bold),
                                      ),
                                      Container(
                                        margin: const EdgeInsets.only(top: 10),
                                        height: 40,
                                        child: ElevatedButton(
                                          onPressed: () async {
                                            navigationModel.navigate("/qa-platform/project/$prjProjectId/setting/parameter");
                                          },
                                          child: const MultiLanguageText(
                                            name: "edit_project_parameter",
                                            defaultValue: "Go to Edit project parameter",
                                            style: TextStyle(color: Colors.white),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        },
      );
    });
  }

  Row headerWidget(NavigationModel navigationModel) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            IconButton(
              onPressed: () {
                navigationModel.navigate("/qa-platform/project/${navigationModel.prjProjectId}/setting/version");
              },
              icon: const Icon(Icons.arrow_back),
              padding: EdgeInsets.zero,
              splashRadius: 16,
            ),
            MultiLanguageText(
              name: "enter_list_parameter",
              defaultValue: "Enter list parameter",
              style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
            )
          ],
        ),
        Row(
          children: [
            isViewColumn
                ? SizedBox(
                    height: 40,
                    child: FloatingActionButton(
                      heroTag: null,
                      onPressed: () async {
                        setState(() {
                          isViewColumn = false;
                        });
                      },
                      backgroundColor: const Color.fromARGB(176, 125, 141, 231),
                      elevation: 0,
                      hoverElevation: 0,
                      child: const Icon(Icons.view_column, color: Color.fromRGBO(49, 49, 49, 1)),
                    ),
                  )
                : SizedBox(
                    height: 40,
                    child: FloatingActionButton(
                      heroTag: null,
                      onPressed: () async {
                        setState(() {
                          isViewColumn = true;
                        });
                      },
                      backgroundColor: const Color.fromARGB(176, 125, 141, 231),
                      elevation: 0,
                      hoverElevation: 0,
                      child: const Icon(Icons.view_stream, color: Color.fromRGBO(49, 49, 49, 1)),
                    ),
                  ),
          ],
        )
      ],
    );
  }
}

class TableViewVersionWidget extends StatefulWidget {
  final bool isViewColumn;
  final dynamic resultparameterFieldList;
  final dynamic versionConfig;
  const TableViewVersionWidget({Key? key, required this.isViewColumn, this.resultparameterFieldList, this.versionConfig}) : super(key: key);

  @override
  State<TableViewVersionWidget> createState() => _TableViewVersionWidgetState();
}

class _TableViewVersionWidgetState extends CustomState<TableViewVersionWidget> {
  @override
  Widget build(BuildContext context) {
    return (!widget.isViewColumn)
        ? SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: DataTable(
              columnSpacing: 5,
              horizontalMargin: 0,
              columns: [
                for (var paramColumn in widget.resultparameterFieldList)
                  DataColumn(
                    label: Container(
                      constraints: const BoxConstraints(minWidth: 300),
                      alignment: Alignment.center,
                      child: Text(
                        paramColumn["name"],
                        maxLines: 1,
                        style: Style(context).styleTextDataColumn,
                      ),
                    ),
                  ),
              ],
              rows: [
                DataRow(
                  cells: [
                    for (var parameterField in widget.resultparameterFieldList)
                      DataCell(
                        Row(
                          children: [
                            Expanded(
                              child: SizedBox(
                                width: 300,
                                child: DynamicTextField(
                                  controller: TextEditingController(text: widget.versionConfig[parameterField["name"]] ?? ""),
                                  hintText: multiLanguageString(
                                      name: "enter_a_value_for",
                                      defaultValue: "Enter a value for \$0",
                                      variables: ["${parameterField["name"]}"],
                                      context: context),
                                  onChanged: (text) {
                                    widget.versionConfig[parameterField["name"]] = text;
                                  },
                                ),
                              ),
                            ),
                            if (parameterField["dataType"] == "File")
                              ElevatedButton(
                                  onPressed: () {
                                    Navigator.of(context).push(
                                      createRoute(
                                        FindFileWidget((String valueCallback) {
                                          setState(() {
                                            widget.versionConfig[parameterField["name"]] = valueCallback;
                                          });
                                        }),
                                      ),
                                    );
                                  },
                                  child: const MultiLanguageText(name: "select_file", defaultValue: "Select file")),
                          ],
                        ),
                      ),
                  ],
                ),
              ],
            ),
          )
        : Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  for (var paramColumn in widget.resultparameterFieldList)
                    Container(
                      padding: const EdgeInsets.only(left: 10, right: 10),
                      alignment: Alignment.centerLeft,
                      width: 300,
                      height: 56,
                      child: Tooltip(
                        message: paramColumn["name"],
                        child: Text(
                          paramColumn["name"],
                          maxLines: 1,
                          style: Style(context).styleTextDataColumn,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    )
                ],
              ),
              Scrollbar(
                thumbVisibility: true,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    primary: true,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            for (var parameterField in widget.resultparameterFieldList)
                              IntrinsicWidth(
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: SizedBox(
                                        width: 300,
                                        child: DynamicTextField(
                                          controller: TextEditingController(text: widget.versionConfig[parameterField["name"]] ?? ""),
                                          hintText: multiLanguageString(
                                              name: "enter_a_value_for",
                                              defaultValue: "Enter a value for \$0",
                                              variables: ["${parameterField["name"]}"],
                                              context: context),
                                          onChanged: (text) {
                                            widget.versionConfig[parameterField["name"]] = text;
                                          },
                                        ),
                                      ),
                                    ),
                                    if (parameterField["dataType"] == "File")
                                      ElevatedButton(
                                          onPressed: () {
                                            Navigator.of(context).push(
                                              createRoute(
                                                FindFileWidget((String valueCallback) {
                                                  setState(() {
                                                    widget.versionConfig[parameterField["name"]] = valueCallback;
                                                  });
                                                }),
                                              ),
                                            );
                                          },
                                          child: const MultiLanguageText(name: "select_file", defaultValue: "Select file")),
                                  ],
                                ),
                              )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          );
  }
}
