import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/common/row_code_with_tooltip.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';

import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';
import '../../../api.dart';
import '../../../common/custom_state.dart';
import '../../../common/dynamic_dropdown_button.dart';

class OptionVersionWidget extends StatefulWidget {
  final String type;
  final String title;
  final String? id;
  final Function? callbackOptionTest;
  final String prjProjectId;
  const OptionVersionWidget({Key? key, required this.type, this.id, this.callbackOptionTest, required this.prjProjectId, required this.title})
      : super(key: key);

  @override
  State<OptionVersionWidget> createState() => _OptionVersionWidgetState();
}

class _OptionVersionWidgetState extends CustomState<OptionVersionWidget> {
  late Future future;
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _desController = TextEditingController();
  final TextEditingController _titleController = TextEditingController();
  int _status = 0;
  int defaultVersion = 0;
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _startDateController = TextEditingController();
  final TextEditingController _endDateController = TextEditingController();
  dynamic selectedItem;
  @override
  void initState() {
    future = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    if (widget.id != null) {
      await getSelectedItem(widget.id!);
      await setInitValue();
    }
    return 0;
  }

  getSelectedItem(String id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-version/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        selectedItem = response["body"]["result"];
      });
    }
  }

  setInitValue() {
    setState(() {
      if (selectedItem != null) {
        _status = selectedItem["status"] ?? 0;
        _desController.text = selectedItem["description"] ?? "";
        _titleController.text = selectedItem["title"];
        _nameController.text = selectedItem["name"];
        defaultVersion = selectedItem["defaultVersion"] ?? 0;
        _startDateController.text = DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(selectedItem["startDate"]).toLocal());
        _endDateController.text = DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(selectedItem["endDate"]).toLocal());
      }
    });
  }

  _selectDate(TextEditingController controller) async {
    final DateTime? datePicked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (datePicked != null) {
      setState(() {
        controller.text = DateFormat('dd-MM-yyyy').format(datePicked);
        _formKey.currentState!.validate();
      });
    }
  }

  @override
  void dispose() {
    _nameController.dispose();
    _desController.dispose();
    _titleController.dispose();
    _startDateController.dispose();
    _endDateController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, LanguageModel>(
      builder: (context, navigationModel, languageModel, child) {
        return FutureBuilder(
          future: future,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return TestFrameworkRootPageWidget(
                child: Column(
                  children: [
                    headerWidget(context),
                    const Divider(color: Color.fromRGBO(216, 218, 229, 1)),
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.only(top: 20, bottom: 10),
                        child: Form(
                          key: _formKey,
                          child: SingleChildScrollView(
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                              child: FocusTraversalGroup(
                                policy: OrderedTraversalPolicy(),
                                child: bodyWidget(context),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    btnWidget()
                  ],
                ),
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  Padding btnWidget() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20, top: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ButtonSave(onPressed: () {
            if (_formKey.currentState!.validate()) {
              var result = {};
              if (selectedItem != null) {
                result = selectedItem;
                result["name"] = _nameController.text;
                result["title"] = _titleController.text;
                result["prjProjectId"] = widget.prjProjectId;
                result["description"] = _desController.text;
                result["status"] = _status;
                result["defaultVersion"] = defaultVersion;
                result["startDate"] = "${DateFormat('dd-MM-yyyy').parse(_startDateController.text).toIso8601String()}+07:00";
                result["endDate"] = "${DateFormat('dd-MM-yyyy').parse(_endDateController.text).toIso8601String()}+07:00";
              } else {
                result = {
                  "name": _nameController.text,
                  "title": _titleController.text,
                  "prjProjectId": widget.prjProjectId,
                  "description": _desController.text,
                  "status": _status,
                  "defaultVersion": defaultVersion,
                  "startDate": "${DateFormat('dd-MM-yyyy').parse(_startDateController.text).toIso8601String()}+07:00",
                  "endDate": "${DateFormat('dd-MM-yyyy').parse(_endDateController.text).toIso8601String()}+07:00",
                };
              }
              widget.callbackOptionTest!(result);
            }
          }),
          const ButtonCancel()
        ],
      ),
    );
  }

  Column bodyWidget(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Expanded(
                flex: 3,
                child: RowCodeWithTooltipWidget(),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _nameController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "name", defaultValue: "Name", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _titleController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "default_version", defaultValue: "Default version", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                  flex: 7,
                  child: DynamicDropdownButton(
                      borderRadius: 5,
                      value: defaultVersion,
                      hint: multiLanguageString(name: "is_default_version", defaultValue: "Is default version ?", context: context),
                      items: [
                        {"name": multiLanguageString(name: "item_default", defaultValue: "Default", context: context), "value": 1},
                        {"name": multiLanguageString(name: "item_not_default", defaultValue: "Not Default", context: context), "value": 0},
                      ].map<DropdownMenuItem<int>>((dynamic result) {
                        return DropdownMenuItem(
                          value: result["value"],
                          child: Text(result["name"]),
                        );
                      }).toList(),
                      onChanged: (newValue) {
                        setState(() {
                          defaultVersion = newValue!;
                        });
                      },
                      isRequiredNotEmpty: true)),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "status", defaultValue: "Status", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                    borderRadius: 5,
                    value: _status,
                    hint: multiLanguageString(name: "choose_a_status", defaultValue: "Choose a status", context: context),
                    items: [
                      {"name": multiLanguageString(name: "active", defaultValue: "Active", context: context), "value": 1},
                      {"name": multiLanguageString(name: "inactive", defaultValue: "Inactive", context: context), "value": 0},
                    ].map<DropdownMenuItem<int>>((dynamic result) {
                      return DropdownMenuItem(
                        value: result["value"],
                        child: Text(result["name"]),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        _status = newValue!;
                      });
                    },
                    isRequiredNotEmpty: true),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "start_date", defaultValue: "Start date", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _startDateController,
                  suffixIcon: const Icon(Icons.calendar_today),
                  hintText: multiLanguageString(name: "select_start_date", defaultValue: "Select start date", context: context),
                  labelText: multiLanguageString(name: "start_date", defaultValue: "Start date", context: context),
                  readOnly: true,
                  onTap: () {
                    _selectDate(_startDateController);
                  },
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "end_date", defaultValue: "End date", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _endDateController,
                  suffixIcon: const Icon(Icons.calendar_today),
                  hintText: multiLanguageString(name: "select_end_date", defaultValue: "Select end date", context: context),
                  labelText: multiLanguageString(name: "end_date", defaultValue: "End date", context: context),
                  readOnly: true,
                  onTap: () {
                    _selectDate(_endDateController);
                  },
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "description", defaultValue: "Description", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _desController,
                  hintText: "...",
                  maxline: 3,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Padding headerWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 35, 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "type_project_version",
            defaultValue: "\$0 PROJECT VERSION",
            variables: [widget.title],
            isLowerCase: false,
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
            onPressed: () => Navigator.pop(context, 'cancel'),
            child: const Icon(
              Icons.close,
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }
}
