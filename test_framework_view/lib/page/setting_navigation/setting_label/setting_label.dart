import 'dart:async';

import 'package:flutter/material.dart';

import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/components/badge/badge_icon.dart';
import 'package:test_framework_view/components/dialog/dialog_delete.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/components/header_setting.dart';
import 'package:test_framework_view/page/setting_navigation/setting_label/option_label.dart';
import 'package:test_framework_view/type.dart';

import '../../../../api.dart';
import '../../../../model/model.dart';
import '../../../common/custom_state.dart';

class SettingLabelWidget extends StatefulWidget {
  const SettingLabelWidget({Key? key}) : super(key: key);

  @override
  State<SettingLabelWidget> createState() => _SettingLabelWidgetState();
}

class _SettingLabelWidgetState extends CustomState<SettingLabelWidget> {
  late Future futureListLabel;
  var resultListLabel = [];
  var rowCountListLabel = 0;
  var currentPage = 1;
  var rowPerPage = 10;
  var searchRequest = {};
  var prjProjectId = "";

  @override
  void initState() {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    prjProjectId = navigationModel.prjProjectId ?? "";
    search({});
    super.initState();
  }

  search(searchRequest) {
    this.searchRequest = searchRequest;
    futureListLabel = getListLabel(currentPage);
  }

  Future getListLabel(page) async {
    if ((page - 1) * rowPerPage > rowCountListLabel) {
      page = (1.0 * rowCountListLabel / rowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {
      "queryOffset": (page - 1) * rowPerPage,
      "queryLimit": rowPerPage,
      "prjProjectId": prjProjectId,
    };
    if (searchRequest["titleLike"] != null && searchRequest["titleLike"].isNotEmpty) {
      findRequest["titleLike"] = "%${searchRequest["titleLike"]}%";
    }

    if (searchRequest["status"] != null) {
      findRequest["status"] = searchRequest["status"];
    }
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-label/search", findRequest, context);

    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPage = page;
        rowCountListLabel = response["body"]["rowCount"];
        resultListLabel = response["body"]["resultList"];
      });
    }
    return 0;
  }

  handleDeleteLabel(id) async {
    var response = await httpDelete("/test-framework-api/user/test-framework/tf-test-label/$id", context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        await getListLabel(currentPage);
      }
    }
  }

  handleAddLabel(result) async {
    var response = await httpPut("/test-framework-api/user/test-framework/tf-test-label", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await getListLabel(currentPage);
      }
    }
  }

  handleEditLabel(result, id) async {
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-label/$id", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await getListLabel(currentPage);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, LanguageModel>(
      builder: (context, navigationModel, languageModel, child) {
        return FutureBuilder(
          future: futureListLabel,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Stack(
                children: [
                  ListView(
                    children: [
                      NavMenuTabV1(
                        currentUrl: "/setting/label",
                        margin: const EdgeInsets.fromLTRB(50, 20, 50, 0),
                        navigationList: context.settingMenuList,
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(50, 20, 50, 50),
                        child: Column(
                          children: [
                            HeaderSettingWidget(
                              type: "Label",
                              title: multiLanguageString(name: "label", defaultValue: "Label", context: context),
                              countList: rowCountListLabel,
                              callbackSearch: (result) {
                                search(result);
                              },
                            ),
                            if (resultListLabel.isNotEmpty) tableWidget(context),
                            SelectionArea(
                              child: DynamicTablePagging(
                                rowCountListLabel,
                                currentPage,
                                rowPerPage,
                                pageChangeHandler: (page) {
                                  setState(() {
                                    futureListLabel = getListLabel(page);
                                  });
                                },
                                rowPerPageChangeHandler: (rowPerPage) {
                                  setState(() {
                                    this.rowPerPage = rowPerPage!;
                                    futureListLabel = getListLabel(1);
                                  });
                                },
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  Positioned(bottom: 10, right: 40, child: btnWidget(context)),
                ],
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  SingleChildScrollView btnWidget(BuildContext context) {
    return SingleChildScrollView(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            margin: const EdgeInsets.only(right: 10),
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              onPressed: () {
                Navigator.of(context).push(
                  createRoute(
                    OptionLabelWidget(
                      type: "create",
                      title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                      callbackOptionTest: (result) {
                        handleAddLabel(result);
                      },
                      prjProjectId: prjProjectId,
                    ),
                  ),
                );
              },
              icon: const Icon(Icons.note_add, color: Color.fromRGBO(49, 49, 49, 1)),
              label: MultiLanguageText(
                name: "new_label",
                defaultValue: "New Label",
                isLowerCase: false,
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
            ),
          ),
        ],
      ),
    );
  }

  SelectionArea tableWidget(BuildContext context) {
    return SelectionArea(
      child: Container(
        margin: const EdgeInsets.only(top: 30),
        child: CustomDataTableWidget(
          columns: [
            Expanded(
              flex: 3,
              child: MultiLanguageText(
                name: "name",
                defaultValue: "Name",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              flex: 3,
              child: MultiLanguageText(
                name: "description",
                defaultValue: "Description",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "create_date",
                defaultValue: "Create date",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "owner",
                defaultValue: "Owner",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  MultiLanguageText(
                    name: "status",
                    defaultValue: "Status",
                    isLowerCase: false,
                    style: Style(context).styleTextDataColumn,
                  ),
                  Tooltip(
                    message: multiLanguageString(name: "active_inactive", defaultValue: "Green = Active\nRed = Inactive", context: context),
                    child: const Icon(
                      Icons.info,
                      color: Colors.grey,
                      size: 16,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: 38,
            ),
          ],
          rows: [
            for (var resultLabel in resultListLabel)
              CustomDataRow(
                cells: [
                  Expanded(
                    flex: 3,
                    child: Row(
                      children: [
                        const Icon(Icons.description, color: Color.fromRGBO(61, 148, 254, 1)),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Text(
                            resultLabel["title"] ?? "",
                            style: Style(context).styleTextDataCell,
                            maxLines: 3,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Text(
                      resultLabel["description"] ?? "",
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                  Expanded(
                    child: Text(
                      resultLabel["createDate"] != null
                          ? DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultLabel["createDate"]).toLocal())
                          : "",
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                  Expanded(
                    child: Text(
                      resultLabel["sysUser"]["fullname"],
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                  Expanded(
                    child: Center(
                      child: Badge2StatusSettingWidget(
                        status: resultLabel["status"],
                      ),
                    ),
                  ),
                  PopupMenuButton(
                    offset: const Offset(5, 50),
                    tooltip: '',
                    splashRadius: 10,
                    itemBuilder: (context) => [
                      PopupMenuItem(
                        padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                        child: ListTile(
                          onTap: () {
                            Navigator.pop(context);
                            Navigator.of(context).push(
                              createRoute(
                                OptionLabelWidget(
                                  type: "edit",
                                  title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                  id: resultLabel["id"],
                                  callbackOptionTest: (result) {
                                    handleEditLabel(result, resultLabel["id"]);
                                  },
                                  prjProjectId: prjProjectId,
                                ),
                              ),
                            );
                          },
                          contentPadding: const EdgeInsets.all(0),
                          hoverColor: Colors.transparent,
                          title: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                          leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
                        ),
                      ),
                      PopupMenuItem(
                        padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                        child: ListTile(
                          onTap: () {
                            Navigator.pop(context);

                            dialogDeleteLabel(resultLabel, resultLabel["id"]);
                          },
                          contentPadding: const EdgeInsets.all(0),
                          hoverColor: Colors.transparent,
                          title: const MultiLanguageText(name: "delete", defaultValue: "Delete"),
                          leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
          ],
        ),
      ),
    );
  }

  dialogDeleteLabel(selectItem, id) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogDelete(
          selectedItem: selectItem,
          callback: () {
            handleDeleteLabel(id);
          },
          type: 'test',
        );
      },
    );
  }
}
