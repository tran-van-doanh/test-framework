import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/common/file_download.dart';
import 'package:test_framework_view/components/dialog/dialog_delete.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/config.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/components/header_setting.dart';
import 'package:test_framework_view/page/setting_navigation/setting_user/option_permission_user.dart';
import 'package:test_framework_view/page/setting_navigation/setting_user/option_role_user.dart';
import 'package:test_framework_view/type.dart';

import '../../../api.dart';
import '../../../common/custom_state.dart';

class SettingUserWidget extends StatefulWidget {
  const SettingUserWidget({Key? key}) : super(key: key);

  @override
  State<SettingUserWidget> createState() => _SettingUserWidgetState();
}

class _SettingUserWidgetState extends CustomState<SettingUserWidget> {
  final TextEditingController _nameController = TextEditingController();
  late Future futureListUser;
  var resultListUser = [];
  var rowCountListUser = 0;
  var currentPage = 1;
  var rowPerPage = 10;
  var searchRequest = {};
  var prjProjectId = "";
  PlatformFile? objFile;

  @override
  void initState() {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    prjProjectId = navigationModel.prjProjectId ?? "";
    search({});
    super.initState();
  }

  search(searchRequest) {
    this.searchRequest = searchRequest;
    futureListUser = getListUser(currentPage);
  }

  Future getListUser(page) async {
    if ((page - 1) * rowPerPage > rowCountListUser) {
      page = (1.0 * rowCountListUser / rowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {
      "queryOffset": (page - 1) * rowPerPage,
      "queryLimit": rowPerPage,
      "prjProjectId": prjProjectId,
    };
    if (searchRequest["fullnameLike"] != null && searchRequest["fullnameLike"].isNotEmpty) {
      findRequest["fullnameLike"] = "%${searchRequest["fullnameLike"]}%";
    }
    if (searchRequest["emailLike"] != null && searchRequest["emailLike"].isNotEmpty) {
      findRequest["emailLike"] = "%${searchRequest["emailLike"]}%";
    }
    var response = await httpPost("/test-framework-api/user/sys/sys-user-project/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPage = page;
        rowCountListUser = response["body"]["rowCount"];
        resultListUser = response["body"]["resultList"];
      });
    }
    return 0;
  }

  String getFirstChar(nameValue) {
    return nameValue[0];
  }

  handleDeleteUser(id) async {
    var response = await httpDelete("/test-framework-api/user/sys/sys-user-project/$id", context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        await getListUser(currentPage);
      }
    }
  }

  handleAddUser() async {
    var findRequestSearchUser = {"email": _nameController.text};
    var response = await httpPost("/test-framework-api/user/sys/sys-user/search", findRequestSearchUser, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"]["resultList"].isNotEmpty) {
        var findRequest = {"sysUserId": response["body"]["resultList"][0]["id"], "prjProjectId": prjProjectId};
        var response1 = await httpPut("/test-framework-api/user/sys/sys-user-project", findRequest, context);
        if (mounted) {
          if (response1.containsKey("body") && response1["body"] is String == false) {
            showToast(
                context: context,
                msg: multiLanguageString(name: "add_member_successful", defaultValue: "Add member successful", context: context),
                color: Colors.greenAccent,
                icon: const Icon(Icons.done));
            await getListUser(currentPage);
          } else {
            showToast(
                context: context,
                msg: multiLanguageString(name: "fail", defaultValue: "Fail", context: context),
                color: Colors.greenAccent,
                icon: const Icon(Icons.done));
          }
        }
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "name", defaultValue: "Member '\$0' not exist !!!", variables: [_nameController.text], context: context),
            color: Colors.red,
            icon: const Icon(Icons.error));
      }
    }
  }

  handleEditUser(result, id) async {
    var response = await httpPost("/test-framework-api/user/sys/sys-user-project/$id", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await getListUser(currentPage);
      }
    }
  }

  uploadFile() async {
    final request = http.MultipartRequest(
      "POST",
      Uri.parse(
        "$baseUrl/test-framework-api/user/system-file/prj-project/$prjProjectId/sys-user-project/import-excel",
      ),
    );
    request.headers.addAll({"Authorization": Provider.of<SecurityModel>(context, listen: false).authorization!});
    request.files.add(http.MultipartFile.fromBytes('file', await readFile(objFile!), filename: objFile!.name));
    var resp = await request.send();
    var responseDecode = jsonDecode(await resp.stream.bytesToString());
    if (responseDecode.containsKey("errorMessage") && mounted) {
      showToast(context: context, msg: responseDecode["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
    } else if (mounted) {
      showToast(
          context: context,
          msg: multiLanguageString(name: "upload_success", defaultValue: "Upload success", context: context),
          color: Colors.greenAccent,
          icon: const Icon(Icons.done));
    }
  }

  @override
  void dispose() {
    _nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, LanguageModel>(
      builder: (context, navigationModel, languageModel, child) {
        return FutureBuilder(
          future: futureListUser,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Stack(
                children: [
                  ListView(
                    children: [
                      NavMenuTabV1(
                        currentUrl: "/setting/teammate",
                        margin: const EdgeInsets.fromLTRB(50, 20, 50, 0),
                        navigationList: context.settingMenuList,
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(50, 20, 50, 50),
                        child: Column(
                          children: [
                            HeaderSettingWidget(
                              type: "Teammate",
                              title: multiLanguageString(name: "teammate", defaultValue: "Teammate", context: context),
                              countList: rowCountListUser,
                              callbackSearch: (result) {
                                search(result);
                              },
                            ),
                            tableWidget(),
                            SelectionArea(
                              child: DynamicTablePagging(
                                rowCountListUser,
                                currentPage,
                                rowPerPage,
                                pageChangeHandler: (page) {
                                  setState(() {
                                    futureListUser = getListUser(page);
                                  });
                                },
                                rowPerPageChangeHandler: (rowPerPage) {
                                  setState(() {
                                    this.rowPerPage = rowPerPage!;
                                    futureListUser = getListUser(1);
                                  });
                                },
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  Positioned(bottom: 10, right: 40, child: btnWidget(context)),
                ],
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  SingleChildScrollView btnWidget(BuildContext context) {
    return SingleChildScrollView(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            margin: const EdgeInsets.only(right: 10),
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              onPressed: () {
                dialogInviteTeammmates(context);
              },
              icon: const Icon(Icons.note_add, color: Color.fromRGBO(49, 49, 49, 1)),
              label: MultiLanguageText(
                name: "teammate",
                defaultValue: "Teammate",
                isLowerCase: false,
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
            ),
          ),
          Container(
            margin: const EdgeInsets.only(right: 10),
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              onPressed: () async {
                var requestBody = {
                  "prjProjectId": prjProjectId,
                };
                File? downloadedFile = await httpDownloadFileWithBodyRequest(
                    context,
                    "/test-framework-api/user/system-file/sys-user-project/export-excel",
                    Provider.of<SecurityModel>(context, listen: false).authorization,
                    'Output.xlsx',
                    requestBody);
                if (downloadedFile != null && mounted) {
                  var snackBar = SnackBar(
                    content: MultiLanguageText(name: "file_downloaded_to", defaultValue: "File downloaded to \$0 .", variables: ["$downloadedFile"]),
                  );
                  ScaffoldMessenger.of(this.context).showSnackBar(snackBar);
                }
              },
              icon: const FaIcon(FontAwesomeIcons.fileArrowDown, color: Color.fromRGBO(49, 49, 49, 1), size: 20),
              label: MultiLanguageText(
                name: "export",
                defaultValue: "Export",
                isLowerCase: false,
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
              tooltip: multiLanguageString(name: "export_to_excel", defaultValue: "Export to Excel", context: context),
            ),
          ),
          SizedBox(
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              onPressed: () async {
                objFile = await selectFile(['xlsx']);
                uploadFile();
              },
              icon: const FaIcon(FontAwesomeIcons.fileArrowUp, color: Color.fromRGBO(49, 49, 49, 1), size: 20),
              label: MultiLanguageText(
                name: "import",
                defaultValue: "Import",
                isLowerCase: false,
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
              tooltip: multiLanguageString(name: "import_to_excel", defaultValue: "Import to Excel", context: context),
            ),
          ),
        ],
      ),
    );
  }

  SelectionArea tableWidget() {
    return SelectionArea(
      child: Container(
        margin: const EdgeInsets.only(top: 30),
        child: CustomDataTableWidget(
          columns: const [],
          rows: [
            for (var resultUser in resultListUser)
              CustomDataRow(
                cells: [
                  Expanded(
                    flex: 3,
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Row(
                        children: [
                          Container(
                            width: 50,
                            height: 50,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(25),
                              color: const Color(0xff7986cb),
                            ),
                            child: Center(
                                child:
                                    Text(getFirstChar(resultUser["sysUser"]["fullname"]).toUpperCase(), style: const TextStyle(color: Colors.white))),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(resultUser["sysUser"]["fullname"] ?? "",
                                    style: TextStyle(color: const Color(0xff313131), fontSize: context.fontSizeManager.fontSizeText14)),
                                Text(resultUser["sysUser"]["email"] ?? "",
                                    style: TextStyle(color: const Color(0xff858791), fontSize: context.fontSizeManager.fontSizeText14))
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  PopupMenuButton(
                    offset: const Offset(5, 50),
                    tooltip: '',
                    splashRadius: 10,
                    itemBuilder: (context) => [
                      PopupMenuItem(
                        padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                        child: ListTile(
                          onTap: () {
                            Navigator.pop(context);
                            dialogEditPermUser(resultUser);
                          },
                          contentPadding: const EdgeInsets.all(0),
                          hoverColor: Colors.transparent,
                          title: const MultiLanguageText(name: "edit_permission_user", defaultValue: "Edit permission user"),
                          leading: const Icon(Icons.settings, color: Color.fromARGB(255, 112, 114, 119)),
                        ),
                      ),
                      PopupMenuItem(
                        padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                        child: ListTile(
                          onTap: () {
                            Navigator.pop(context);
                            dialogEditRoleUser(resultUser);
                          },
                          contentPadding: const EdgeInsets.all(0),
                          hoverColor: Colors.transparent,
                          title: const MultiLanguageText(name: "edit_role_user", defaultValue: "Edit role user"),
                          leading: const Icon(Icons.settings, color: Color.fromARGB(255, 112, 114, 119)),
                        ),
                      ),
                      PopupMenuItem(
                        padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                        child: ListTile(
                          onTap: () {
                            Navigator.pop(context);
                            dialogDeleteUser(resultUser);
                          },
                          contentPadding: const EdgeInsets.all(0),
                          hoverColor: Colors.transparent,
                          title: const MultiLanguageText(name: "delete_user", defaultValue: "Delete user"),
                          leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
          ],
        ),
      ),
    );
  }

  Future<String?> dialogInviteTeammmates(BuildContext context) {
    final formInviteKey = GlobalKey<FormState>();
    _nameController.clear();
    return showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: const EdgeInsets.all(0),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MultiLanguageText(
                name: "invite_teammates",
                defaultValue: "Invite Teammates",
                isLowerCase: false,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: const Color.fromRGBO(0, 0, 0, 0.8),
                    fontSize: context.fontSizeManager.fontSizeText16,
                    letterSpacing: 2),
              ),
              TextButton(
                  onPressed: () => Navigator.pop(context, 'Cancel'),
                  child: const Icon(
                    Icons.close,
                    color: Colors.black,
                  ))
            ],
          ),
          content: Container(
            padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
            decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
                bottom: BorderSide(
                  color: Color.fromRGBO(216, 218, 229, 1),
                ),
              ),
            ),
            margin: const EdgeInsets.only(top: 20, bottom: 10),
            constraints: const BoxConstraints(minWidth: 600),
            child: SingleChildScrollView(
              child: Form(
                key: formInviteKey,
                child: DynamicTextField(
                  controller: _nameController,
                  hintText: multiLanguageString(name: "enter_teammate_email", defaultValue: "Enter teammate's email ", context: context),
                  onChanged: (text) {
                    formInviteKey.currentState!.validate();
                  },
                  isRequiredNotEmpty: true,
                ),
              ),
            ),
          ),
          actionsAlignment: MainAxisAlignment.center,
          actions: [
            ButtonSave(
              onPressed: () async {
                if (formInviteKey.currentState!.validate()) {
                  onLoading(context);
                  await handleAddUser();
                  if (mounted) {
                    Navigator.pop(this.context);
                    Navigator.of(this.context).pop();
                  }
                }
              },
            ),
            const ButtonCancel()
          ],
        );
      },
    );
  }

  dialogEditPermUser(selectedItem) {
    showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          return OptionPermissionUserWidget(
              type: "edit",
              title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
              selectedItem: selectedItem,
              callbackOptionTest: (result) {
                handleEditUser(result, selectedItem["id"]);
              });
        });
  }

  dialogEditRoleUser(selectedItem) {
    showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          return OptionRoleUserWidget(
              type: "edit",
              title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
              selectedItem: selectedItem,
              callbackOptionTest: (result) {
                handleEditUser(result, selectedItem["id"]);
              });
        });
  }

  dialogDeleteUser(selectedItem) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogDelete(
          selectedItem: selectedItem,
          callback: () {
            handleDeleteUser(selectedItem["id"]);
          },
          type: 'user',
        );
      },
    );
  }
}
