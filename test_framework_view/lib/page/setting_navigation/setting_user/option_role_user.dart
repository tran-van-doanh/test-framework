import 'package:flutter/material.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/custom_state.dart';

import 'package:test_framework_view/common/dynamic_form.dart';
import 'package:test_framework_view/common/tree.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';

class OptionRoleUserWidget extends StatefulWidget {
  final String type;
  final String title;
  final dynamic selectedItem;
  final Function? callbackOptionTest;
  const OptionRoleUserWidget({Key? key, required this.type, this.selectedItem, this.callbackOptionTest, required this.title}) : super(key: key);

  @override
  State<OptionRoleUserWidget> createState() => _OptionRoleUserWidgetState();
}

class _OptionRoleUserWidgetState extends CustomState<OptionRoleUserWidget> {
  TreeData? sysRoleTreeData;
  var sysUserProject = {};
  bool isSelectAll = false;

  @override
  void initState() {
    getListRoleUser();
    super.initState();
  }

  getListRoleUser() async {
    var sysRoleFindRequest = {"itemType": "project", "status": 1};
    var sysRoleFindResponse = await httpPost("/test-framework-api/user/sys/sys-item-role/search", sysRoleFindRequest, context);
    if (sysRoleFindResponse.containsKey("body") && sysRoleFindResponse["body"] is String == false) {
      var sysRoleList = sysRoleFindResponse["body"]["resultList"];
      List<TreeNode> treeNodeList = [];
      for (var sysRole in sysRoleList) {
        treeNodeList.add(TreeNode(
          key: sysRole["id"],
          // parentKey: sysRole["parentId"],
          item: sysRole,
        ));
      }
      setState(() {
        sysRoleTreeData = TreeData(treeNodeList);
      });
    }
    if (mounted) {
      var sysUserProjectGetResponse = await httpGet("/test-framework-api/user/sys/sys-user-project/${widget.selectedItem["id"]}", context);
      if (sysUserProjectGetResponse.containsKey("body") && sysUserProjectGetResponse["body"] is String == false) {
        setState(() {
          sysUserProject = sysUserProjectGetResponse["body"]["result"];
        });
      }
    }
    if (sysRoleTreeData!.getAllNode().map((e) => e.key).every((element) => (sysUserProject["sysRoleIdList"] ?? []).contains(element))) {
      setState(() {
        isSelectAll = true;
      });
    } else {
      setState(() {
        isSelectAll = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: const EdgeInsets.all(0),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "type_edit_role_user",
            defaultValue: "\$0 role user",
            variables: [widget.title],
            isLowerCase: false,
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Icon(
                Icons.close,
                color: Colors.black,
              ))
        ],
      ),
      content: Container(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
        decoration: const BoxDecoration(
          border: Border(
            top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
            bottom: BorderSide(
              color: Color.fromRGBO(216, 218, 229, 1),
            ),
          ),
        ),
        margin: const EdgeInsets.only(top: 20, bottom: 10),
        constraints: const BoxConstraints(minWidth: 600),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                child: MultiLanguageText(name: "role", defaultValue: "Role", style: Style(context).styleTitleDialog),
              ),
              if (sysRoleTreeData != null)
                MyTreeMultipleSelect(
                    selectedValue: sysUserProject["sysRoleIdList"] ?? [],
                    treeData: sysRoleTreeData!,
                    getTreeNodeLabel: (treeNode) => "${treeNode.item["code"]} - ${treeNode.item["name"]}",
                    onChanged: (value) {
                      sysUserProject["sysRoleIdList"] = value;
                    })
            ],
          ),
        ),
      ),
      actionsAlignment: MainAxisAlignment.center,
      actions: [
        Container(
          margin: const EdgeInsets.only(right: 5),
          height: 40,
          child: ElevatedButton(
            onPressed: () {
              setState(() {
                if (isSelectAll) {
                  sysUserProject["sysRoleIdList"] = [];
                } else {
                  sysUserProject["sysRoleIdList"] = [];
                  for (var element in sysRoleTreeData!.getAllNode()) {
                    sysUserProject["sysRoleIdList"].add(element.key);
                  }
                }
                isSelectAll = !isSelectAll;
              });
            },
            child: isSelectAll
                ? const MultiLanguageText(
                    name: "unselect_all",
                    defaultValue: "Unselect all",
                    isLowerCase: false,
                    style: TextStyle(color: Colors.white),
                  )
                : const MultiLanguageText(
                    name: "select_all",
                    defaultValue: "Select all",
                    isLowerCase: false,
                    style: TextStyle(color: Colors.white),
                  ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(right: 5),
          height: 40,
          width: 120,
          child: ElevatedButton(
            onPressed: () {
              var result = widget.selectedItem;
              result["sysRoleIdList"] = sysUserProject["sysRoleIdList"];
              widget.callbackOptionTest!(result);
            },
            child: const MultiLanguageText(
              name: "ok",
              defaultValue: "Ok",
              isLowerCase: false,
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 5),
          height: 40,
          width: 120,
          child: ElevatedButton(
            onPressed: () {
              Navigator.pop(context);
            },
            style: ElevatedButton.styleFrom(
                side: const BorderSide(color: Color.fromRGBO(209, 212, 216, 1)),
                backgroundColor: Colors.white,
                foregroundColor: const Color.fromRGBO(61, 90, 254, 1)),
            child: const MultiLanguageText(
              name: "cancel",
              defaultValue: "Cancel",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ],
    );
  }
}
