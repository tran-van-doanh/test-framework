import 'package:flutter/material.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/custom_state.dart';

import 'package:test_framework_view/common/dynamic_form.dart';
import 'package:test_framework_view/common/tree.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';

class OptionPermissionUserWidget extends StatefulWidget {
  final String type;
  final String title;
  final dynamic selectedItem;
  final Function? callbackOptionTest;
  const OptionPermissionUserWidget({Key? key, required this.type, this.selectedItem, this.callbackOptionTest, required this.title}) : super(key: key);

  @override
  State<OptionPermissionUserWidget> createState() => _OptionPermissionUserWidgetState();
}

class _OptionPermissionUserWidgetState extends CustomState<OptionPermissionUserWidget> {
  TreeData? sysPermTreeData;
  var sysUserProject = {};
  bool isSelectAll = false;

  @override
  void initState() {
    getListPermUser();
    super.initState();
  }

  getListPermUser() async {
    var sysPermFindRequest = {"permType": "project", "status": 1};
    var sysPermFindResponse = await httpPost("/test-framework-api/user/sys/sys-perm/search", sysPermFindRequest, context);
    if (sysPermFindResponse.containsKey("body") && sysPermFindResponse["body"] is String == false) {
      var sysPermList = sysPermFindResponse["body"]["resultList"];
      List<TreeNode> treeNodeList = [];
      for (var sysPerm in sysPermList) {
        treeNodeList.add(TreeNode(key: sysPerm["id"], parentKey: sysPerm["parentId"], item: sysPerm));
      }
      setState(() {
        sysPermTreeData = TreeData(treeNodeList);
      });
    }
    if (mounted) {
      var sysUserProjectGetResponse = await httpGet("/test-framework-api/user/sys/sys-user-project/${widget.selectedItem["id"]}", context);
      if (sysUserProjectGetResponse.containsKey("body") && sysUserProjectGetResponse["body"] is String == false) {
        setState(() {
          sysUserProject = sysUserProjectGetResponse["body"]["result"];
        });
      }
    }
    if (sysPermTreeData!.getAllNode().map((e) => e.key).every((element) => (sysUserProject["sysPermIdList"] ?? []).contains(element))) {
      setState(() {
        isSelectAll = true;
      });
    } else {
      setState(() {
        isSelectAll = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: const EdgeInsets.all(0),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "type_permission_user",
            defaultValue: "\$0 permission user",
            variables: [widget.title],
            isLowerCase: false,
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Icon(
                Icons.close,
                color: Colors.black,
              ))
        ],
      ),
      content: Container(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
        decoration: const BoxDecoration(
          border: Border(
            top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
            bottom: BorderSide(
              color: Color.fromRGBO(216, 218, 229, 1),
            ),
          ),
        ),
        margin: const EdgeInsets.only(top: 20, bottom: 10),
        constraints: const BoxConstraints(minWidth: 600),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                child: MultiLanguageText(name: "permission", defaultValue: "Permission", style: Style(context).styleTitleDialog),
              ),
              if (sysPermTreeData != null)
                MyTreeMultipleSelect(
                    selectedValue: sysUserProject["sysPermIdList"] ?? [],
                    treeData: sysPermTreeData!,
                    getTreeNodeLabel: (treeNode) => "${treeNode.item["code"]} - ${treeNode.item["name"]}",
                    onChanged: (value) {
                      sysUserProject["sysPermIdList"] = value;
                    })
            ],
          ),
        ),
      ),
      actionsAlignment: MainAxisAlignment.center,
      actions: [
        Container(
          margin: const EdgeInsets.only(right: 5),
          height: 40,
          child: ElevatedButton(
            onPressed: () {
              setState(() {
                if (isSelectAll) {
                  sysUserProject["sysPermIdList"] = [];
                } else {
                  sysUserProject["sysPermIdList"] = [];
                  for (var element in sysPermTreeData!.getAllNode()) {
                    sysUserProject["sysPermIdList"].add(element.key);
                  }
                }
                isSelectAll = !isSelectAll;
              });
            },
            child: isSelectAll
                ? const MultiLanguageText(
                    name: "unselect_all",
                    defaultValue: "Unselect all",
                    isLowerCase: false,
                    style: TextStyle(color: Colors.white),
                  )
                : const MultiLanguageText(
                    name: "select_all",
                    defaultValue: "Select all",
                    isLowerCase: false,
                    style: TextStyle(color: Colors.white),
                  ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(right: 5),
          height: 40,
          width: 120,
          child: ElevatedButton(
            onPressed: () {
              var result = widget.selectedItem;
              result["sysPermIdList"] = sysUserProject["sysPermIdList"];
              widget.callbackOptionTest!(result);
            },
            child: const MultiLanguageText(
              name: "ok",
              defaultValue: "Ok",
              isLowerCase: false,
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 5),
          height: 40,
          width: 120,
          child: ElevatedButton(
            onPressed: () {
              Navigator.pop(context);
            },
            style: ElevatedButton.styleFrom(
                side: const BorderSide(color: Color.fromRGBO(209, 212, 216, 1)),
                backgroundColor: Colors.white,
                foregroundColor: const Color.fromRGBO(61, 90, 254, 1)),
            child: const MultiLanguageText(
              name: "cancel",
              defaultValue: "Cancel",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ],
    );
  }
}
