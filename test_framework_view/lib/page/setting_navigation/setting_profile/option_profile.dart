import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';
import '../../../common/custom_state.dart';
import '../../../common/dynamic_dropdown_button.dart';

class OptionProfileWidget extends StatefulWidget {
  final String type;
  final String title;
  final String? id;
  final Function? callbackOptionTest;
  final String prjProjectId;
  const OptionProfileWidget({Key? key, required this.type, this.id, this.callbackOptionTest, required this.prjProjectId, required this.title})
      : super(key: key);

  @override
  State<OptionProfileWidget> createState() => _OptionProfileWidgetState();
}

class _OptionProfileWidgetState extends CustomState<OptionProfileWidget> {
  late Future future;
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _udidController = TextEditingController();
  final TextEditingController _appController = TextEditingController();
  final TextEditingController _desController = TextEditingController();
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController screenWidthController = TextEditingController();
  final TextEditingController screenHeightController = TextEditingController();
  dynamic prjProject;
  int _status = 0;
  String? tfOperatingSystem;
  String? tfBrowser;
  int defaultProfile = 0;
  String? platform;
  String? headless;
  String? recorderDisabled;
  String? imageDisabled;
  String? tfBrowserVersion;
  List operatingSystemList = [];
  List browserList = [];
  List browserVersionList = [];
  final _formKey = GlobalKey<FormState>();
  String? prjProjectId;
  dynamic selectedItem;
  @override
  void initState() {
    future = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    prjProject =
        await Provider.of<NavigationModel>(context, listen: false).getPrjProject(Provider.of<SecurityModel>(context, listen: false).authorization);
    if (widget.id != null) {
      await getSelectedItem(widget.id!);
      await setInitValue();
    }
    if (platform != null) {
      await getListOperatingSystem();
    }
    if (tfOperatingSystem != null) await getListBrowser(tfOperatingSystem);
    if (tfBrowser != null) await getListBrowserVersion(tfBrowser);
    return 0;
  }

  getSelectedItem(String id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-profile/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        selectedItem = response["body"]["result"];
      });
    }
  }

  setInitValue() {
    setState(() {
      if (selectedItem != null) {
        _status = selectedItem["status"] ?? 0;
        _desController.text = selectedItem["description"] ?? "";
        _titleController.text = selectedItem["title"] ?? "";
        _nameController.text = selectedItem["name"] ?? "";
        _udidController.text = selectedItem["udid"] ?? "";
        _appController.text = selectedItem["app"] ?? "";
        screenWidthController.text = selectedItem["screenWidth"].toString();
        screenHeightController.text = selectedItem["screenHeight"].toString();
        tfOperatingSystem = selectedItem["tfOperatingSystemId"];
        tfBrowser = selectedItem["tfBrowserId"];
        tfBrowserVersion = selectedItem["tfBrowserVersionId"];
        headless = selectedItem["headless"] ? "True" : "False";
        recorderDisabled = selectedItem["recorderDisabled"] ? "True" : "False";
        imageDisabled = selectedItem["imageDisabled"] ? "True" : "False";
        prjProjectId = selectedItem["prjProjectId"];
        platform = selectedItem["platform"];
        defaultProfile = selectedItem["defaultProfile"] ?? 0;
      }
    });
  }

  getListOperatingSystem() async {
    var searchRequest = {"platform": platform};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-operating-system/search", searchRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        operatingSystemList = response["body"]["resultList"];
      });
    }
  }

  getListBrowser(tfOperatingSystemId) async {
    var response = await httpPost("/test-framework-api/user/test-framework/tf-browser/search", {"tfOperatingSystemId": tfOperatingSystemId}, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        browserList = response["body"]["resultList"];
      });
    }
  }

  getListBrowserVersion(tfBrowserId) async {
    var response = await httpPost("/test-framework-api/user/test-framework/tf-browser-version/search", {"tfBrowserId": tfBrowserId}, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        browserVersionList = response["body"]["resultList"];
      });
    }
  }

  @override
  void dispose() {
    _nameController.dispose();
    _udidController.dispose();
    _appController.dispose();
    _desController.dispose();
    _titleController.dispose();
    screenWidthController.dispose();
    screenHeightController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return FutureBuilder(
        future: future,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return TestFrameworkRootPageWidget(
              child: Column(
                children: [
                  headerWidget(context),
                  const Divider(color: Color.fromRGBO(216, 218, 229, 1)),
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.only(top: 20, bottom: 10),
                      child: Form(
                        key: _formKey,
                        child: SingleChildScrollView(
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                            child: FocusTraversalGroup(
                              policy: OrderedTraversalPolicy(),
                              child: bodyWidget(context),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  btnWidget()
                ],
              ),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        },
      );
    });
  }

  Padding btnWidget() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20, top: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ButtonSave(onPressed: () {
            if (_formKey.currentState!.validate()) {
              var result = {};
              if (selectedItem != null) {
                result = selectedItem;
                result["name"] = _nameController.text;
                result["udid"] = _udidController.text;
                result["app"] = _appController.text;
                result["title"] = _titleController.text;
                result["prjProjectId"] = widget.prjProjectId;
                result["description"] = _desController.text;
                result["screenWidth"] = screenWidthController.text;
                result["screenHeight"] = screenHeightController.text;
                result["tfOperatingSystemId"] = tfOperatingSystem;
                result["tfBrowserId"] = tfBrowser;
                result["platform"] = platform;
                result["tfBrowserVersionId"] = tfBrowserVersion;
                result["defaultProfile"] = defaultProfile;
                result["headless"] = !(headless == "False");
                result["recorderDisabled"] = !(recorderDisabled == "False");
                result["imageDisabled"] = !(imageDisabled == "False");
                result["status"] = _status;
              } else {
                result = {
                  "title": _titleController.text,
                  "name": _nameController.text,
                  "udid": _udidController.text,
                  "app": _appController.text,
                  "description": _desController.text,
                  "screenWidth": screenWidthController.text,
                  "screenHeight": screenHeightController.text,
                  "tfOperatingSystemId": tfOperatingSystem,
                  "tfBrowserId": tfBrowser,
                  "platform": platform,
                  "tfBrowserVersionId": tfBrowserVersion,
                  "defaultProfile": defaultProfile,
                  "headless": !(headless == "False"),
                  "recorderDisabled": !(recorderDisabled == "False"),
                  "imageDisabled": !(imageDisabled == "False"),
                  "status": _status,
                  "prjProjectId": widget.prjProjectId
                };
              }
              widget.callbackOptionTest!(result);
            }
          }),
          const ButtonCancel()
        ],
      ),
    );
  }

  Column bodyWidget(BuildContext context) {
    var securityModel = Provider.of<SecurityModel>(context, listen: false);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "platform", defaultValue: "Platform", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                  flex: 7,
                  child: DynamicDropdownButton(
                      borderRadius: 5,
                      value: platform,
                      hint: multiLanguageString(name: "choose_a_platform", defaultValue: "Choose a platform", context: context),
                      items: (securityModel.sysOrganizationPlatformsList ?? []).map<DropdownMenuItem<String>>((String result) {
                        return DropdownMenuItem(
                          value: result,
                          child: Text(result),
                        );
                      }).toList(),
                      onChanged: (newValue) async {
                        setState(() {
                          platform = newValue!;
                          tfOperatingSystem = null;
                          tfBrowser = null;
                          tfBrowserVersion = null;
                          browserVersionList = [];
                        });
                        await getListOperatingSystem();
                      },
                      isRequiredNotEmpty: true)),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "code", defaultValue: "Code", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  enabled: widget.type != "edit",
                  controller: _nameController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                  isRequiredRegex: false,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "name", defaultValue: "Name", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _titleController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "app", defaultValue: "App", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _appController,
                  hintText: "...",
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "udid", defaultValue: "Udid", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _udidController,
                  hintText: "...",
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "status", defaultValue: "Status", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                    borderRadius: 5,
                    value: _status,
                    hint: multiLanguageString(name: "choose_a_status", defaultValue: "Choose a status", context: context),
                    items: [
                      {"name": multiLanguageString(name: "active", defaultValue: "Active", context: context), "value": 1},
                      {"name": multiLanguageString(name: "inactive", defaultValue: "Inactive", context: context), "value": 0},
                    ].map<DropdownMenuItem<int>>((dynamic result) {
                      return DropdownMenuItem(
                        value: result["value"],
                        child: Text(result["name"]),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        _status = newValue!;
                      });
                    },
                    isRequiredNotEmpty: true),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "screen_width", defaultValue: "Screen Width", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  keyboardType: TextInputType.number,
                  controller: screenWidthController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "screen_height", defaultValue: "Screen Height", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  keyboardType: TextInputType.number,
                  controller: screenHeightController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "default_profile", defaultValue: "Default profile", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                  flex: 7,
                  child: DynamicDropdownButton(
                      borderRadius: 5,
                      value: defaultProfile,
                      hint: multiLanguageString(name: "is_default_profile", defaultValue: "Is default profile ?", context: context),
                      items: [
                        {"name": multiLanguageString(name: "item_default", defaultValue: "Default", context: context), "value": 1},
                        {"name": multiLanguageString(name: "item_not_default", defaultValue: "Not Default", context: context), "value": 0},
                      ].map<DropdownMenuItem<int>>((dynamic result) {
                        return DropdownMenuItem(
                          value: result["value"],
                          child: Text(result["name"]),
                        );
                      }).toList(),
                      onChanged: (newValue) {
                        setState(() {
                          defaultProfile = newValue!;
                        });
                      },
                      isRequiredNotEmpty: true)),
            ],
          ),
        ),
        if (operatingSystemList.isNotEmpty)
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 3,
                  child: MultiLanguageText(name: "operating_system", defaultValue: "Operating system", style: Style(context).styleTitleDialog),
                ),
                Expanded(
                  flex: 7,
                  child: DynamicDropdownButton(
                    value: tfOperatingSystem,
                    hint: multiLanguageString(name: "choose_an_operating_system", defaultValue: "Choose an operating system", context: context),
                    items: operatingSystemList.map<DropdownMenuItem<String>>((dynamic result) {
                      return DropdownMenuItem(
                        value: result["id"],
                        child: Text(result["title"]),
                      );
                    }).toList(),
                    onChanged: (newValue) async {
                      setState(() {
                        tfOperatingSystem = newValue!;
                        tfBrowser = null;
                        tfBrowserVersion = null;
                        browserVersionList = [];
                      });
                      if (platform == "Web") {
                        await getListBrowser(tfOperatingSystem);
                      }
                    },
                    borderRadius: 5,
                    isRequiredNotEmpty: true,
                  ),
                ),
              ],
            ),
          ),
        if (tfOperatingSystem != null && platform == "Web")
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 3,
                  child: MultiLanguageText(name: "browser", defaultValue: "Browser", style: Style(context).styleTitleDialog),
                ),
                Expanded(
                    flex: 7,
                    child: DynamicDropdownButton(
                      value: tfBrowser,
                      hint: multiLanguageString(name: "choose_a_browser", defaultValue: "Choose a browser", context: context),
                      items: browserList.map<DropdownMenuItem<String>>((dynamic result) {
                        return DropdownMenuItem(
                          value: result["id"],
                          child: Text(result["title"]),
                        );
                      }).toList(),
                      onChanged: (newValue) {
                        setState(() {
                          tfBrowser = newValue!;
                          tfBrowserVersion = null;
                          getListBrowserVersion(tfBrowser);
                        });
                      },
                      borderRadius: 5,
                      isRequiredNotEmpty: browserList.isNotEmpty,
                    )),
              ],
            ),
          ),
        if (tfBrowser != null)
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 3,
                  child: MultiLanguageText(name: "browser_version", defaultValue: "Browser version", style: Style(context).styleTitleDialog),
                ),
                Expanded(
                    flex: 7,
                    child: DynamicDropdownButton(
                      value: tfBrowserVersion,
                      hint: multiLanguageString(name: "choose_a_browser_version", defaultValue: "Choose a browser version", context: context),
                      items: browserVersionList.map<DropdownMenuItem<String>>((dynamic result) {
                        return DropdownMenuItem(
                          value: result["id"],
                          child: Text(result["title"]),
                        );
                      }).toList(),
                      onChanged: (newValue) {
                        setState(() {
                          tfBrowserVersion = newValue!;
                        });
                      },
                      borderRadius: 5,
                      isRequiredNotEmpty: true,
                    )),
              ],
            ),
          ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "headless", defaultValue: "Headless", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                    borderRadius: 5,
                    value: headless ?? "True",
                    items: const ["True", "False"].map<DropdownMenuItem<String>>((String result) {
                      return DropdownMenuItem(
                        value: result,
                        child: Text(result),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        headless = newValue!;
                      });
                    },
                    isRequiredNotEmpty: true),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "recorder_disabled", defaultValue: "Recorder disabled", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                    borderRadius: 5,
                    value: recorderDisabled ?? "True",
                    items: const ["True", "False"].map<DropdownMenuItem<String>>((String result) {
                      return DropdownMenuItem(
                        value: result,
                        child: Text(result),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        recorderDisabled = newValue!;
                      });
                    },
                    isRequiredNotEmpty: true),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "image_disabled", defaultValue: "Image disabled", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                  flex: 7,
                  child: DynamicDropdownButton(
                      borderRadius: 5,
                      value: imageDisabled ?? "True",
                      items: const ["True", "False"].map<DropdownMenuItem<String>>((String result) {
                        return DropdownMenuItem(
                          value: result,
                          child: Text(result),
                        );
                      }).toList(),
                      onChanged: (newValue) {
                        setState(() {
                          imageDisabled = newValue!;
                        });
                      },
                      isRequiredNotEmpty: true)),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "description", defaultValue: "Description", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _desController,
                  hintText: "...",
                  maxline: 3,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Padding headerWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 35, 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "type_project_profile",
            defaultValue: "\$0 PROJECT PROFILE",
            variables: [widget.title],
            isLowerCase: false,
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
            onPressed: () => Navigator.pop(context, 'cancel'),
            child: const Icon(
              Icons.close,
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }
}
