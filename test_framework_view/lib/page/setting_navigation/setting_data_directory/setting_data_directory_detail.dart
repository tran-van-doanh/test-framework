import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/custom_state.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/components/badge/badge_icon.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import '../../../components/style.dart';

class SettingDataDirectoryDetailWidget extends StatefulWidget {
  final String id;
  final String prjProjectId;
  const SettingDataDirectoryDetailWidget({Key? key, required this.id, required this.prjProjectId}) : super(key: key);

  @override
  State<SettingDataDirectoryDetailWidget> createState() => _SettingDataDirectoryDetailWidgetState();
}

class _SettingDataDirectoryDetailWidgetState extends CustomState<SettingDataDirectoryDetailWidget> {
  late Future future;
  var resultDataDirectory = {};
  var resultDataFile = [];
  var rowCount = 0;
  var currentPage = 1;
  var rowPerPage = 10;
  @override
  void initState() {
    super.initState();
    future = getInitPage();
  }

  getInitPage() async {
    await getDataDirectory(widget.id);
    await getDataFile(widget.id, currentPage);
    return 0;
  }

  getDataFile(String id, page) async {
    if ((page - 1) * rowPerPage > rowCount) {
      page = (1.0 * rowCount / rowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {
      "tfTestDataDirectoryId": id,
      "prjProjectId": widget.prjProjectId,
      "queryOffset": (page - 1) * rowPerPage,
      "queryLimit": rowPerPage
    };
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-data-file/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPage = page;
        rowCount = response["body"]["rowCount"];
        resultDataFile = response["body"]["resultList"];
      });
    }
    return 0;
  }

  getDataDirectory(String id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-data-directory/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        resultDataDirectory = response["body"]["result"];
      });
      await getClient();
    }
  }

  getClient() async {
    for (var client in resultDataDirectory["testRunConfig"]["clientList"]) {
      var response = await httpGet("/test-framework-api/user/test-framework/tf-test-profile/${client['tfTestProfileId']}", context);
      if (response.containsKey("body") && response["body"] is String == false) {
        client["profile"] = response["body"]["result"];
        setState(() {});
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, LanguageModel>(
      builder: (context, navigationModel, languageModel, child) {
        return FutureBuilder(
          future: future,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(50, 20, 50, 30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        headerWidget(navigationModel),
                        dataDirectoryInformation(),
                        clientTable(),
                        if (resultDataFile.isNotEmpty) dataFileTable(),
                        DynamicTablePagging(
                          rowCount,
                          currentPage,
                          rowPerPage,
                          pageChangeHandler: (page) {
                            setState(() {
                              future = getDataFile(widget.id, page);
                            });
                          },
                          rowPerPageChangeHandler: (rowPerPage) {
                            setState(() {
                              this.rowPerPage = rowPerPage!;
                              future = getDataFile(widget.id, 1);
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  Widget clientTable() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MultiLanguageText(
          name: "profile",
          defaultValue: "Profile",
          style: Style(context).styleTitleDialog,
        ),
        const SizedBox(
          height: 5,
        ),
        CustomDataTableWidget(
          columns: [
            Expanded(
              flex: 2,
              child: MultiLanguageText(
                name: "client_name",
                defaultValue: "Client name",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              flex: 2,
              child: MultiLanguageText(
                name: "profile_name",
                defaultValue: "Profile name",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "default",
                defaultValue: "Default",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              flex: 2,
              child: MultiLanguageText(
                name: "operating_system",
                defaultValue: "Operating system",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "browser",
                defaultValue: "Browser",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "resolution",
                defaultValue: "Resolution",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
          ],
          rows: [
            for (var client in resultDataDirectory["testRunConfig"]["clientList"])
              CustomDataRow(
                cells: [
                  Expanded(
                    flex: 2,
                    child: Text(
                      client["name"],
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Row(
                        children: [
                          const Icon(Icons.description, color: Color.fromRGBO(61, 148, 254, 1)),
                          const SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Text(
                              client["profile"]["title"] ?? "",
                              maxLines: 3,
                              overflow: TextOverflow.ellipsis,
                              style: Style(context).styleTextDataCell,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Text(
                      client["profile"]["defaultProfile"] == 0 ? "Not default" : "Default",
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Text(
                      client["profile"]["tfOperatingSystem"]["title"],
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 5),
                      child: Text(
                        client["profile"]["tfBrowser"] != null && client["profile"]["tfBrowserVersion"] != null
                            ? ("${client["profile"]["tfBrowser"]["title"]} ( ${client["profile"]["tfBrowserVersion"]["title"]} )")
                            : "",
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Text(
                      "${client["profile"]["screenWidth"]} X ${client["profile"]["screenHeight"]}",
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                ],
              ),
          ],
        ),
        const SizedBox(
          height: 20,
        )
      ],
    );
  }

  Widget dataFileTable() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MultiLanguageText(
          name: "data_file",
          defaultValue: "Data file",
          style: Style(context).styleTitleDialog,
        ),
        const SizedBox(
          height: 5,
        ),
        CustomDataTableWidget(
          columns: [
            Expanded(
              flex: 2,
              child: MultiLanguageText(
                name: "file_path",
                defaultValue: "File path",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              flex: 2,
              child: MultiLanguageText(
                name: "description",
                defaultValue: "Description",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "create_date",
                defaultValue: "Create date",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  MultiLanguageText(
                    name: "status",
                    defaultValue: "Status",
                    isLowerCase: false,
                    style: Style(context).styleTextDataColumn,
                  ),
                  Tooltip(
                    message: multiLanguageString(
                        name: "new_reading_result_finished_error",
                        defaultValue: "Grey = New\nBlue = Reading\nOrange = Result\nGreen = Finished\nRed = Error",
                        context: context),
                    child: const Icon(
                      Icons.info,
                      color: Colors.grey,
                      size: 16,
                    ),
                  ),
                ],
              ),
            ),
          ],
          rows: [
            for (var dataFile in resultDataFile)
              CustomDataRow(
                cells: [
                  Expanded(
                    flex: 2,
                    child: Text(
                      dataFile["filePath"],
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Text(
                      dataFile["description"] ?? "",
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                  Expanded(
                    child: (dataFile["createDate"] != null)
                        ? Text(
                            "${DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(dataFile["createDate"]).toLocal())} ${DateFormat('HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(dataFile["createDate"]).toLocal())}",
                            softWrap: true,
                            style: TextStyle(
                              fontSize: context.fontSizeManager.fontSizeText16,
                              color: const Color.fromRGBO(49, 49, 49, 1),
                              fontWeight: FontWeight.w500,
                              letterSpacing: 1.5,
                            ),
                          )
                        : const SizedBox.shrink(),
                  ),
                  Expanded(
                    child: Center(
                      child: Badge5StatusDataFileWidget(
                        status: dataFile["status"],
                      ),
                    ),
                  ),
                ],
              ),
          ],
        ),
        const SizedBox(
          height: 20,
        )
      ],
    );
  }

  Widget dataDirectoryInformation() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "code",
                  defaultValue: "Code",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(130, 130, 130, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
              Expanded(
                flex: 8,
                child: Text(
                  resultDataDirectory["name"] ?? "",
                  softWrap: true,
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(49, 49, 49, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "name",
                  defaultValue: "Name",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(130, 130, 130, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
              Expanded(
                flex: 8,
                child: Text(
                  resultDataDirectory["title"] ?? "",
                  softWrap: true,
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(49, 49, 49, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "directory_path",
                  defaultValue: "Directory path",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(130, 130, 130, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
              Expanded(
                flex: 8,
                child: Text(
                  resultDataDirectory["directoryPath"] ?? "",
                  softWrap: true,
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(49, 49, 49, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "test_case",
                  defaultValue: "Test case",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(130, 130, 130, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
              Expanded(
                flex: 8,
                child: Text(
                  resultDataDirectory["tfTestCase"]["title"] ?? "",
                  softWrap: true,
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(49, 49, 49, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "run_owner",
                  defaultValue: "Run owner",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(130, 130, 130, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
              Expanded(
                flex: 8,
                child: Row(
                  children: [
                    const Icon(Icons.person),
                    Text(
                      resultDataDirectory["sysUser"]["fullname"] ?? "",
                      softWrap: true,
                      style: TextStyle(
                        fontSize: context.fontSizeManager.fontSizeText16,
                        color: const Color.fromRGBO(49, 49, 49, 1),
                        fontWeight: FontWeight.w500,
                        letterSpacing: 1.5,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "create_date",
                  defaultValue: "Create date",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(130, 130, 130, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
              Expanded(
                flex: 8,
                child: (resultDataDirectory["createDate"] != null)
                    ? Text(
                        "${DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultDataDirectory["createDate"]).toLocal())} ${DateFormat('HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultDataDirectory["createDate"]).toLocal())}",
                        softWrap: true,
                        style: TextStyle(
                          fontSize: context.fontSizeManager.fontSizeText16,
                          color: const Color.fromRGBO(49, 49, 49, 1),
                          fontWeight: FontWeight.w500,
                          letterSpacing: 1.5,
                        ),
                      )
                    : const SizedBox.shrink(),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "status",
                  defaultValue: "Status",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(130, 130, 130, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
              Expanded(
                flex: 8,
                child: Row(
                  children: [
                    IntrinsicWidth(
                      child: Badge2StatusSettingWidget(
                        status: resultDataDirectory["status"],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "version",
                  defaultValue: "Version",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(130, 130, 130, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
              Expanded(
                flex: 8,
                child: Text(
                  resultDataDirectory["tfTestVersion"]["title"] ?? "",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(49, 49, 49, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
            ],
          ),
          if (resultDataDirectory["description"] != null)
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 10,
                ),
                MultiLanguageText(
                  name: "description",
                  defaultValue: "Description",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(49, 49, 49, 1),
                    fontWeight: FontWeight.w600,
                    letterSpacing: 1.5,
                  ),
                ),
                Text(
                  resultDataDirectory["description"],
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(49, 49, 49, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ],
            ),
        ],
      ),
    );
  }

  Widget headerWidget(NavigationModel navigationModel) {
    return Row(
      children: [
        IconButton(
          onPressed: () {
            navigationModel.pop("/project/${navigationModel.prjProjectId}/data-directory");
          },
          icon: const Icon(Icons.arrow_back),
          padding: EdgeInsets.zero,
          splashRadius: 16,
        ),
        Expanded(
          child: Text(
            resultDataDirectory["title"],
            style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
          ),
        ),
      ],
    );
  }
}
