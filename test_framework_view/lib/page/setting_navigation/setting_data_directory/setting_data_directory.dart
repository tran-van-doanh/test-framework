import 'dart:async';

import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/components/badge/badge_icon.dart';
import 'package:test_framework_view/components/dialog/dialog_delete.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/components/header_setting.dart';
import 'package:test_framework_view/type.dart';

import '../../../../api.dart';
import '../../../../model/model.dart';
import '../../../common/custom_state.dart';
import 'option_data_directory.dart';

class SettingDataDirectoryWidget extends StatefulWidget {
  const SettingDataDirectoryWidget({Key? key}) : super(key: key);

  @override
  State<SettingDataDirectoryWidget> createState() => _SettingDataDirectoryWidgetState();
}

class _SettingDataDirectoryWidgetState extends CustomState<SettingDataDirectoryWidget> {
  late Future futureListDataDirectory;
  var resultListDataDirectory = [];
  var rowCountListDataDirectory = 0;
  var currentPage = 1;
  var rowPerPage = 10;
  var searchRequest = {};
  var prjProjectId = "";

  @override
  void initState() {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    prjProjectId = navigationModel.prjProjectId ?? "";
    search({});
    super.initState();
  }

  search(searchRequest) {
    this.searchRequest = searchRequest;
    futureListDataDirectory = getListDataDirectory(currentPage);
  }

  Future getListDataDirectory(page) async {
    if ((page - 1) * rowPerPage > rowCountListDataDirectory) {
      page = (1.0 * rowCountListDataDirectory / rowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {
      "queryOffset": (page - 1) * rowPerPage,
      "queryLimit": rowPerPage,
      "prjProjectId": prjProjectId,
    };
    if (searchRequest["titleLike"] != null && searchRequest["titleLike"].isNotEmpty) {
      findRequest["titleLike"] = "%${searchRequest["titleLike"]}%";
    }
    if (searchRequest["status"] != null) {
      findRequest["status"] = searchRequest["status"];
    }
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-data-directory/search", findRequest, context);

    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPage = page;
        rowCountListDataDirectory = response["body"]["rowCount"];
        resultListDataDirectory = response["body"]["resultList"];
      });
    }
    return 0;
  }

  handleDeleteDataDirectory(id) async {
    var response = await httpDelete("/test-framework-api/user/test-framework/tf-test-data-directory/$id", context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        await getListDataDirectory(currentPage);
      }
    }
  }

  handleAddDataDirectory(result) async {
    var response = await httpPut("/test-framework-api/user/test-framework/tf-test-data-directory", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await getListDataDirectory(currentPage);
      }
    }
  }

  handleEditDataDirectory(result, id) async {
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-data-directory/$id", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await getListDataDirectory(currentPage);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, LanguageModel>(
      builder: (context, navigationModel, languageModel, child) {
        return FutureBuilder(
          future: futureListDataDirectory,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Stack(
                children: [
                  ListView(
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(50, 20, 50, 50),
                        child: Column(
                          children: [
                            HeaderSettingWidget(
                              type: "Data Directory",
                              title: multiLanguageString(name: "data_directory", defaultValue: "Data Directory", context: context),
                              countList: rowCountListDataDirectory,
                              callbackSearch: (result) {
                                search(result);
                              },
                            ),
                            if (resultListDataDirectory.isNotEmpty)
                              SelectionArea(
                                child: Container(
                                  margin: const EdgeInsets.only(top: 30),
                                  child: tableDataDirectory(navigationModel),
                                ),
                              ),
                            SelectionArea(
                              child: DynamicTablePagging(
                                rowCountListDataDirectory,
                                currentPage,
                                rowPerPage,
                                pageChangeHandler: (page) {
                                  setState(() {
                                    futureListDataDirectory = getListDataDirectory(page);
                                  });
                                },
                                rowPerPageChangeHandler: (rowPerPage) {
                                  setState(() {
                                    this.rowPerPage = rowPerPage!;
                                    futureListDataDirectory = getListDataDirectory(1);
                                  });
                                },
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  Positioned(bottom: 10, right: 40, child: btnWidget(context)),
                ],
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  SingleChildScrollView btnWidget(BuildContext context) {
    return SingleChildScrollView(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            margin: const EdgeInsets.only(right: 10),
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              onPressed: () {
                Navigator.of(context).push(
                  createRoute(
                    OptionDataDirectoryWidget(
                      type: "create",
                      title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                      callbackOptionDataDirectory: (result) {
                        handleAddDataDirectory(result);
                      },
                      prjProjectId: prjProjectId,
                    ),
                  ),
                );
              },
              icon: const Icon(Icons.note_add, color: Color.fromRGBO(49, 49, 49, 1)),
              label: MultiLanguageText(
                name: "new_data_directory",
                defaultValue: "New Data Directory",
                isLowerCase: false,
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
            ),
          ),
        ],
      ),
    );
  }

  dialogDeleteDataDirectory(selectItem) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogDelete(
          selectedItem: selectItem,
          callback: () {
            handleDeleteDataDirectory(selectItem["id"]);
          },
          type: 'DataDirectory',
        );
      },
    );
  }

  Widget tableDataDirectory(NavigationModel navigationModel) {
    return CustomDataTableWidget(
      columns: [
        Expanded(
          flex: 2,
          child: MultiLanguageText(
            name: "name",
            defaultValue: "Name",
            isLowerCase: false,
            style: Style(context).styleTextDataColumn,
          ),
        ),
        Expanded(
          flex: 2,
          child: MultiLanguageText(
            name: "test_case",
            defaultValue: "Test Case",
            isLowerCase: false,
            style: Style(context).styleTextDataColumn,
          ),
        ),
        Expanded(
          child: MultiLanguageText(
            name: "version",
            defaultValue: "Version",
            isLowerCase: false,
            style: Style(context).styleTextDataColumn,
          ),
        ),
        Expanded(
          child: MultiLanguageText(
            name: "type",
            defaultValue: "Type",
            isLowerCase: false,
            style: Style(context).styleTextDataColumn,
          ),
        ),
        Expanded(
          child: MultiLanguageText(
            name: "owner",
            defaultValue: "Owner",
            isLowerCase: false,
            style: Style(context).styleTextDataColumn,
          ),
        ),
        Expanded(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              MultiLanguageText(
                name: "status",
                defaultValue: "Status",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
              Tooltip(
                message: multiLanguageString(name: "active_inactive", defaultValue: "Green = Active\nRed = Inactive", context: context),
                child: const Icon(
                  Icons.info,
                  color: Colors.grey,
                  size: 16,
                ),
              ),
            ],
          ),
        ),
        Container(
          width: 38,
        ),
      ],
      rows: [
        for (var resultDataDirectory in resultListDataDirectory)
          CustomDataRow(
            cells: [
              Expanded(
                flex: 2,
                child: Padding(
                  padding: const EdgeInsets.all(5),
                  child: Row(
                    children: [
                      const Icon(Icons.description, color: Color.fromRGBO(61, 148, 254, 1)),
                      const SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: Text(
                          resultDataDirectory["title"] ?? "",
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: Style(context).styleTextDataCell,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 2,
                child: Text(
                  resultDataDirectory["tfTestCase"]["title"],
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                  style: Style(context).styleTextDataCell,
                ),
              ),
              Expanded(
                child: Text(
                  resultDataDirectory["tfTestVersion"]["title"],
                  style: Style(context).styleTextDataCell,
                ),
              ),
              Expanded(
                child: Text(
                  resultDataDirectory["testRunType"],
                  style: Style(context).styleTextDataCell,
                ),
              ),
              Expanded(
                child: Text(
                  resultDataDirectory["sysUser"]["fullname"],
                  style: Style(context).styleTextDataCell,
                ),
              ),
              Expanded(
                child: Center(
                  child: Badge2StatusSettingWidget(
                    status: resultDataDirectory["status"],
                  ),
                ),
              ),
              PopupMenuButton(
                offset: const Offset(5, 50),
                tooltip: '',
                splashRadius: 10,
                itemBuilder: (context) => [
                  PopupMenuItem(
                    padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                    child: ListTile(
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.of(context).push(
                          createRoute(
                            OptionDataDirectoryWidget(
                              type: "edit",
                              title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                              id: resultDataDirectory["id"],
                              callbackOptionDataDirectory: (result) {
                                handleEditDataDirectory(result, resultDataDirectory["id"]);
                              },
                              prjProjectId: prjProjectId,
                            ),
                          ),
                        );
                      },
                      contentPadding: const EdgeInsets.all(0),
                      hoverColor: Colors.transparent,
                      title: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                      leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
                    ),
                  ),
                  PopupMenuItem(
                    padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                    child: ListTile(
                      onTap: () {
                        Navigator.pop(context);
                        dialogDeleteDataDirectory(resultDataDirectory);
                      },
                      contentPadding: const EdgeInsets.all(0),
                      hoverColor: Colors.transparent,
                      title: const MultiLanguageText(name: "delete", defaultValue: "Delete"),
                      leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                    ),
                  ),
                ],
              ),
            ],
            onSelectChanged: () {
              navigationModel.navigate("/qa-platform/project/${navigationModel.prjProjectId}/data-directory/${resultDataDirectory["id"]}");
            },
          ),
      ],
    );
  }
}
