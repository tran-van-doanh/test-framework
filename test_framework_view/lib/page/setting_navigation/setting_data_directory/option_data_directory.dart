import 'dart:async';
import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/common/row_code_with_tooltip.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';

import '../../../api.dart';
import '../../../common/custom_state.dart';
import '../../../type.dart';
import '../../../components/dialog/dialog_choose_test.dart';

class OptionDataDirectoryWidget extends StatefulWidget {
  final String type;
  final String title;
  final String prjProjectId;
  final String? id;
  final Function callbackOptionDataDirectory;
  const OptionDataDirectoryWidget({
    Key? key,
    required this.type,
    required this.prjProjectId,
    required this.callbackOptionDataDirectory,
    this.id,
    required this.title,
  }) : super(key: key);

  @override
  State<OptionDataDirectoryWidget> createState() => _OptionDataDirectoryWidgetState();
}

class _OptionDataDirectoryWidgetState extends CustomState<OptionDataDirectoryWidget> {
  late Future futureParameterListPlay;
  List resultClientList = [];
  var testCaseSelected = {};
  // TextEditingController scheduleDate = TextEditingController();
  TextEditingController directoryPathController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController codeController = TextEditingController();
  TextEditingController desController = TextEditingController();
  String? testCaseRequiredText;
  dynamic _testsVersion;
  dynamic _testsEnvironment;
  String? testCaseId;
  String? testRunType = "parallel";
  var _listVersions = [];
  var _listEnvironment = [];
  final _formKey = GlobalKey<FormState>();
  int _status = 0;
  dynamic selectedItem;
  @override
  void initState() {
    futureParameterListPlay = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    await getVersionList();
    await getEnvironmentList();
    if (widget.id != null) {
      await getSelectedItem(widget.id!);
      await setInitValue(selectedItem);
    }
    return 0;
  }

  getSelectedItem(String id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-data-directory/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        selectedItem = response["body"]["result"];
      });
    }
  }

  setInitValue(item) async {
    setState(() {
      _status = selectedItem["status"] ?? 0;
      testCaseId = item["tfTestCase"]["id"];
      if (_listVersions.any((element) => element["id"] == item["tfTestVersionId"])) {
        _testsVersion = item["tfTestVersion"];
      }
      if (_listEnvironment.any((element) => element["id"] == item["tfTestEnvironmentId"])) {
        _testsEnvironment = item["tfTestEnvironment"];
      }
      testRunType = item["testRunType"];
      directoryPathController.text = item["directoryPath"] ?? "";
      nameController.text = item["title"] ?? "";
      codeController.text = item["name"] ?? "";
      desController.text = item["description"] ?? "";
    });
    if (testCaseId != null) {
      await getClientList(testCaseId!);
      for (var client in resultClientList) {
        await getProfileList(client);
        setState(() {
          client["tfTestProfileId"] = selectedItem["testRunConfig"]["clientList"][resultClientList.indexOf(client)]["tfTestProfileId"];
        });
      }
    }
  }

  Future getClientList(String testCaseId) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-test-case/$testCaseId", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        resultClientList = response["body"]["result"]["content"]["clientList"] ?? [];
        testCaseSelected = response["body"]["result"];
      });
    }
  }

  getVersionList() async {
    var findRequest = {"prjProjectId": Provider.of<NavigationModel>(context, listen: false).prjProjectId, "orderBy": "startDate DESC", "status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-version/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listVersions = response["body"]["resultList"];
        if (_listVersions.isNotEmpty) {
          var versionItem = _listVersions.firstWhere(
            (element) => element["defaultVersion"] == 1,
            orElse: () => null,
          );
          _testsVersion = versionItem ?? _listVersions[0];
        }
      });
    }
  }

  getEnvironmentList() async {
    var findRequest = {"prjProjectId": Provider.of<NavigationModel>(context, listen: false).prjProjectId, "orderBy": "startDate DESC", "status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-environment/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listEnvironment = response["body"]["resultList"];
        if (_listEnvironment.isNotEmpty) {
          var environmentItem = _listEnvironment.firstWhere(
            (element) => element["defaultEnvironment"] == 1,
            orElse: () => null,
          );
          _testsEnvironment = environmentItem ?? _listEnvironment[0];
        }
      });
    }
  }

  getProfileList(client) async {
    var findRequest = {
      "prjProjectId": Provider.of<NavigationModel>(context, listen: false).prjProjectId,
      "status": 1,
      "platform": client["platform"]
    };
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-profile/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        client["profileList"] = response["body"]["resultList"];
      });
    }
  }

  getProfileDefault(client) async {
    var response = await httpGet(
        "/test-framework-api/user/project/prj-project/${Provider.of<NavigationModel>(context, listen: false).prjProjectId}/tf-test-profile/${client["platform"]}/default",
        context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        client["tfTestProfileId"] = response["body"]["result"]["id"];
      });
    }
  }

  handlePlayButton() async {
    var clientListBodyRequest = [];
    for (var element in resultClientList) {
      var clientNew = Map.from(element);
      clientNew.remove("profileList");
      clientListBodyRequest.add(clientNew);
    }
    var result = {};
    if (selectedItem != null) {
      result = selectedItem;
      result["tfTestVersionId"] = _testsVersion["id"];
      result["tfTestEnvironmentId"] = _testsEnvironment["id"];
      result["tfTestCaseId"] = testCaseId;
      result["description"] = desController.text;
      result["testRunType"] = testRunType;
      result["directoryPath"] = directoryPathController.text;
      result["title"] = nameController.text;
      result["name"] = codeController.text;
      result["testRunConfig"]["clientList"] = clientListBodyRequest;
      result["status"] = _status;
    } else {
      result = {
        "createDate": "${DateTime.now().toIso8601String()}+07:00",
        "prjProjectId": widget.prjProjectId,
        "tfTestVersionId": _testsVersion["id"],
        "tfTestEnvironmentId": _testsEnvironment["id"],
        "testRunType": testRunType,
        "description": desController.text,
        "testRunConfig": {"clientList": clientListBodyRequest},
        "tfTestCaseId": testCaseId,
        "directoryPath": directoryPathController.text,
        "title": nameController.text,
        "name": codeController.text,
        "status": _status,
      };
    }
    widget.callbackOptionDataDirectory(result);
  }

  @override
  void dispose() {
    // scheduleDate.dispose();
    desController.dispose();
    directoryPathController.dispose();
    nameController.dispose();
    codeController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return FutureBuilder(
        future: futureParameterListPlay,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return TestFrameworkRootPageWidget(
              child: LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
                return FutureBuilder(
                  future: futureParameterListPlay,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: SingleChildScrollView(
                              child: IntrinsicHeight(
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(50, 30, 50, 10),
                                  child: Column(
                                    children: [
                                      headerWidget(context),
                                      dataDirectoryCreateForm(),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          btnWidget(context),
                        ],
                      );
                    } else if (snapshot.hasError) {
                      return Text('${snapshot.error}');
                    }
                    return const Center(child: CircularProgressIndicator());
                  },
                );
              }),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        },
      );
    });
  }

  Padding btnWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: 40,
            width: 80,
            child: ElevatedButton(
              onPressed: () {
                if (_formKey.currentState!.validate() && testCaseId != null) {
                  handlePlayButton();
                } else if (testCaseId == null) {
                  setState(() {
                    testCaseRequiredText = multiLanguageString(name: "please_choose_a_test", defaultValue: "Please choose a test", context: context);
                  });
                }
              },
              child: const MultiLanguageText(
                name: "ok",
                defaultValue: "Ok",
                isLowerCase: false,
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 10),
            height: 40,
            width: 120,
            child: ElevatedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              style: ElevatedButton.styleFrom(
                side: const BorderSide(color: Color.fromRGBO(209, 212, 216, 1)),
                backgroundColor: Colors.white,
                foregroundColor: const Color.fromRGBO(61, 90, 254, 1),
              ),
              child: const MultiLanguageText(
                name: "cancel",
                defaultValue: "Cancel",
                isLowerCase: false,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          )
        ],
      ),
    );
  }

  Row headerWidget(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        MultiLanguageText(
          name: "type_data_directory",
          defaultValue: "\$0 DATA DIRECTORY",
          variables: [widget.title],
          isLowerCase: false,
          style: Style(context).styleTitleDialog,
        ),
        TextButton(
          onPressed: () => Navigator.pop(context, 'cancel'),
          child: const Icon(
            Icons.close,
            color: Colors.black,
          ),
        ),
      ],
    );
  }

  Widget dataDirectoryCreateForm() {
    return Expanded(
      child: Form(
        key: _formKey,
        child: Container(
          margin: const EdgeInsets.only(top: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  SizedBox(
                    height: 45,
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          createRoute(
                            DialogChooseTest(
                              prjProjectId: widget.prjProjectId,
                              testCaseType: "test_scenario",
                              callbackSelectedTestCase: (testCaseId) async {
                                setState(() {
                                  this.testCaseId = testCaseId;
                                  testCaseRequiredText = null;
                                });
                                await getClientList(this.testCaseId!);
                                for (var client in resultClientList) {
                                  await getProfileList(client);
                                  await getProfileDefault(client);
                                }
                              },
                            ),
                          ),
                        );
                      },
                      child: const MultiLanguageText(
                        name: "select_test_scenario",
                        defaultValue: "Select test scenario",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  SizedBox(
                    height: 45,
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          createRoute(
                            DialogChooseTest(
                              prjProjectId: widget.prjProjectId,
                              testCaseType: "test_suite",
                              callbackSelectedTestCase: (testCaseId) async {
                                setState(() {
                                  this.testCaseId = testCaseId;
                                  testCaseRequiredText = null;
                                });
                                await getClientList(this.testCaseId!);
                                for (var client in resultClientList) {
                                  await getProfileList(client);
                                  await getProfileDefault(client);
                                }
                              },
                            ),
                          ),
                        );
                      },
                      child: const MultiLanguageText(
                        name: "select_test_suite",
                        defaultValue: "Select test suite",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
              if (testCaseSelected.isNotEmpty)
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: MultiLanguageText(
                    name: "test_test_case_selected",
                    defaultValue: "Test : \$0",
                    variables: ["${testCaseSelected["title"]}"],
                    style: TextStyle(fontSize: context.fontSizeManager.fontSizeText22, fontWeight: FontWeight.w500),
                  ),
                ),
              if (testCaseRequiredText != null)
                Text(
                  testCaseRequiredText!,
                  style: const TextStyle(color: Colors.red),
                ),
              const SizedBox(
                height: 10,
              ),
              if (testCaseId != null)
                Theme(
                  data: ThemeData().copyWith(dividerColor: Colors.transparent),
                  child: ExpansionTile(
                    initiallyExpanded: true,
                    tilePadding: const EdgeInsets.all(0),
                    childrenPadding: const EdgeInsets.all(10),
                    title: MultiLanguageText(
                      name: "choose_profile_for_the_clients",
                      defaultValue: "Choose profile for the clients",
                      style: TextStyle(fontSize: context.fontSizeManager.fontSizeText20, fontWeight: FontWeight.w500),
                    ),
                    children: <Widget>[
                      for (var client in resultClientList)
                        if (client["profileList"] != null && client["profileList"].isNotEmpty)
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                    child: DynamicDropdownButton(
                                      value: (client["profileList"] != null && client["profileList"].isNotEmpty) ? client["tfTestProfileId"] : null,
                                      hint: multiLanguageString(name: "choose_a_profile", defaultValue: "Choose a profile", context: context),
                                      items: (client["profileList"] as List).map<DropdownMenuItem<String>>((dynamic result) {
                                        return DropdownMenuItem(
                                          value: result["id"],
                                          child: Text(result["title"]),
                                        );
                                      }).toList(),
                                      onChanged: (newValue) {
                                        setState(() {
                                          client["tfTestProfileId"] = newValue!;
                                        });
                                      },
                                      labelText: multiLanguageString(
                                          name: "client_client_name", defaultValue: "Client \$0", variables: ["${client["name"]}"], context: context),
                                      borderRadius: 5,
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(
                                height: 10,
                              )
                            ],
                          ),
                    ],
                  ),
                ),
              const SizedBox(
                height: 10,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                      child: DynamicDropdownSearchClearOption(
                        hint: multiLanguageString(name: "choose_a_version", defaultValue: "Choose a version", context: context),
                        labelText: multiLanguageString(name: "version", defaultValue: "Version", context: context),
                        controller: SingleValueDropDownController(data: DropDownValueModel(name: _testsVersion["name"], value: _testsVersion["id"])),
                        items: _listVersions.map<DropDownValueModel>((dynamic result) {
                          return DropDownValueModel(
                            value: result["id"],
                            name: result["name"],
                          );
                        }).toList(),
                        maxHeight: 200,
                        onChanged: (dynamic newValue) {
                          _formKey.currentState!.validate();
                          setState(() {
                            _testsVersion = _listVersions.firstWhere((element) => element["id"] == newValue.value);
                          });
                        },
                        isRequiredNotEmpty: true,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                      child: DynamicDropdownSearchClearOption(
                        hint: multiLanguageString(name: "choose_an_environment", defaultValue: "Choose an environment", context: context),
                        controller:
                            SingleValueDropDownController(data: DropDownValueModel(name: _testsEnvironment["name"], value: _testsEnvironment["id"])),
                        items: _listEnvironment.map<DropDownValueModel>((dynamic result) {
                          return DropDownValueModel(
                            value: result["id"],
                            name: result["name"],
                          );
                        }).toList(),
                        maxHeight: 200,
                        onChanged: (dynamic newValue) {
                          _formKey.currentState!.validate();
                          setState(() {
                            _testsEnvironment = _listEnvironment.firstWhere((element) => element["id"] == newValue.value);
                          });
                        },
                        isRequiredNotEmpty: true,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                      child: DynamicDropdownButton(
                        value: testRunType,
                        hint: multiLanguageString(name: "choose_a_test_run_type", defaultValue: "Choose a test run type", context: context),
                        items: [
                          {"label": multiLanguageString(name: "sequence", defaultValue: "Sequence", context: context), "value": "sequence"},
                          {"label": multiLanguageString(name: "parallel", defaultValue: "Parallel", context: context), "value": "parallel"}
                        ].map<DropdownMenuItem<String>>((dynamic result) {
                          return DropdownMenuItem(
                            value: result["value"],
                            child: Text(result["label"]),
                          );
                        }).toList(),
                        onChanged: (newValue) {
                          setState(() {
                            _formKey.currentState!.validate();
                            testRunType = newValue!;
                          });
                        },
                        labelText: multiLanguageString(name: "test_run_type", defaultValue: "Test run type", context: context),
                        borderRadius: 5,
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Expanded(
                      flex: 3,
                      child: RowCodeWithTooltipWidget(),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicTextField(
                        controller: codeController,
                        hintText: "...",
                        isRequiredNotEmpty: true,
                        enabled: widget.type != "edit",
                        isRequiredRegex: true,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "name", defaultValue: "Name", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicTextField(
                        controller: nameController,
                        hintText: "...",
                        isRequiredNotEmpty: true,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "directory_path", defaultValue: "Directory path", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicTextField(
                        controller: directoryPathController,
                        hintText: "...",
                        isRequiredNotEmpty: true,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "status", defaultValue: "Status", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicDropdownButton(
                          borderRadius: 5,
                          value: _status,
                          hint: multiLanguageString(name: "choose_a_status", defaultValue: "Choose a status", context: context),
                          items: [
                            {"name": multiLanguageString(name: "active", defaultValue: "Active", context: context), "value": 1},
                            {"name": multiLanguageString(name: "inactive", defaultValue: "Inactive", context: context), "value": 0},
                          ].map<DropdownMenuItem<int>>((dynamic result) {
                            return DropdownMenuItem(
                              value: result["value"],
                              child: Text(result["name"]),
                            );
                          }).toList(),
                          onChanged: (newValue) {
                            setState(() {
                              _status = newValue!;
                            });
                          },
                          isRequiredNotEmpty: true),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "description", defaultValue: "Description", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicTextField(
                        controller: desController,
                        hintText: "...",
                        maxline: 3,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
