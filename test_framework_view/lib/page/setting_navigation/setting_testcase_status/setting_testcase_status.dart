import 'dart:async';

import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/components/badge/badge_icon.dart';
import 'package:test_framework_view/components/dialog/dialog_delete.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/components/header_setting.dart';
import 'package:test_framework_view/type.dart';

import '../../../../api.dart';
import '../../../../model/model.dart';
import '../../../common/custom_state.dart';
import 'option_testcase_status.dart';

class SettingTestCaseStatusWidget extends StatefulWidget {
  const SettingTestCaseStatusWidget({Key? key}) : super(key: key);

  @override
  State<SettingTestCaseStatusWidget> createState() => _SettingTestCaseStatusWidgetState();
}

class _SettingTestCaseStatusWidgetState extends CustomState<SettingTestCaseStatusWidget> {
  late Future futureListTestCaseStatus;
  var resultListTestCaseStatus = [];
  var rowCountListTestCaseStatus = 0;
  var currentPage = 1;
  var rowPerPage = 10;
  var searchRequest = {};
  var prjProjectId = "";

  @override
  void initState() {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    prjProjectId = navigationModel.prjProjectId ?? "";
    search({});
    super.initState();
  }

  search(searchRequest) {
    this.searchRequest = searchRequest;
    futureListTestCaseStatus = getListTestCaseStatus(currentPage);
  }

  Future getListTestCaseStatus(page) async {
    if ((page - 1) * rowPerPage > rowCountListTestCaseStatus) {
      page = (1.0 * rowCountListTestCaseStatus / rowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {
      "queryOffset": (page - 1) * rowPerPage,
      "queryLimit": rowPerPage,
      "prjProjectId": prjProjectId,
    };
    if (searchRequest["titleLike"] != null && searchRequest["titleLike"].isNotEmpty) {
      findRequest["titleLike"] = "%${searchRequest["titleLike"]}%";
    }
    if (searchRequest["status"] != null) {
      findRequest["status"] = searchRequest["status"];
    }
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-case-status/search", findRequest, context);

    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPage = page;
        rowCountListTestCaseStatus = response["body"]["rowCount"];
        resultListTestCaseStatus = response["body"]["resultList"];
      });
    }
    return 0;
  }

  handleDeleteTestCaseStatus(id) async {
    var response = await httpDelete("/test-framework-api/user/test-framework/tf-test-case-status/$id", context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        await getListTestCaseStatus(currentPage);
      }
    }
  }

  handleAddTestCaseStatus(result) async {
    var response = await httpPut("/test-framework-api/user/test-framework/tf-test-case-status", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await getListTestCaseStatus(currentPage);
      }
    }
  }

  handleEditTestCaseStatus(result, id) async {
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-case-status/$id", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await getListTestCaseStatus(currentPage);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<NavigationModel>(
      builder: (context, navigationModel, child) {
        return FutureBuilder(
          future: futureListTestCaseStatus,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Column(
                children: [
                  Expanded(
                    child: ListView(
                      children: [
                        NavMenuTabV1(
                          currentUrl: "/setting/testcase-status",
                          margin: const EdgeInsets.fromLTRB(50, 20, 50, 0),
                          navigationList: context.settingMenuList,
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
                          child: Column(
                            children: [
                              HeaderSettingWidget(
                                type: "Test Case Status",
                                title: multiLanguageString(name: "test_case_status", defaultValue: "Test Case Status", context: context),
                                countList: rowCountListTestCaseStatus,
                                callbackSearch: (result) {
                                  search(result);
                                },
                              ),
                              tableWidget(context),
                              SelectionArea(
                                child: DynamicTablePagging(
                                  rowCountListTestCaseStatus,
                                  currentPage,
                                  rowPerPage,
                                  pageChangeHandler: (page) {
                                    setState(() {
                                      futureListTestCaseStatus = getListTestCaseStatus(page);
                                    });
                                  },
                                  rowPerPageChangeHandler: (rowPerPage) {
                                    setState(() {
                                      this.rowPerPage = rowPerPage!;
                                      futureListTestCaseStatus = getListTestCaseStatus(1);
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  btnWidget(context),
                ],
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  Padding btnWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 40, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            margin: const EdgeInsets.only(right: 10),
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              onPressed: () {
                Navigator.of(context).push(
                  createRoute(
                    OptionTestCaseStatusWidget(
                      type: "create",
                      title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                      callbackOptionTest: (result) {
                        handleAddTestCaseStatus(result);
                      },
                      prjProjectId: prjProjectId,
                    ),
                  ),
                );
              },
              icon: const Icon(Icons.note_add, color: Color.fromRGBO(49, 49, 49, 1)),
              label: MultiLanguageText(
                name: "new_test_case_status",
                defaultValue: "New Test Case Status",
                isLowerCase: false,
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
            ),
          ),
        ],
      ),
    );
  }

  SelectionArea tableWidget(BuildContext context) {
    return SelectionArea(
      child: Container(
        margin: const EdgeInsets.only(top: 30),
        child: CustomDataTableWidget(
          columns: [
            Expanded(
              flex: 2,
              child: MultiLanguageText(
                name: "name",
                defaultValue: "Name",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              flex: 2,
              child: MultiLanguageText(
                name: "description",
                defaultValue: "Description",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "bug_tracker",
                defaultValue: "Bug tracker",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  MultiLanguageText(
                    name: "status",
                    defaultValue: "Status",
                    isLowerCase: false,
                    style: Style(context).styleTextDataColumn,
                  ),
                  Tooltip(
                    message: multiLanguageString(name: "active_inactive", defaultValue: "Green = Active\nRed = Inactive", context: context),
                    child: const Icon(
                      Icons.info,
                      color: Colors.grey,
                      size: 16,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: 38,
            ),
          ],
          rows: [
            for (var resultTestCaseStatus in resultListTestCaseStatus)
              CustomDataRow(
                cells: [
                  Expanded(
                    flex: 2,
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Row(
                        children: [
                          const Icon(Icons.description, color: Color.fromRGBO(61, 148, 254, 1)),
                          const SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Text(
                              resultTestCaseStatus["title"] ?? "",
                              maxLines: 3,
                              overflow: TextOverflow.ellipsis,
                              style: Style(context).styleTextDataCell,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Text(
                      resultTestCaseStatus["description"] ?? "",
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                  Expanded(
                    child: Text(
                      resultTestCaseStatus["bugTracker"].toString(),
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                  Expanded(
                    child: Center(
                      child: Badge2StatusSettingWidget(
                        status: resultTestCaseStatus["status"],
                      ),
                    ),
                  ),
                  PopupMenuButton(
                    offset: const Offset(5, 50),
                    tooltip: '',
                    splashRadius: 10,
                    itemBuilder: (context) => [
                      PopupMenuItem(
                        padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                        child: ListTile(
                          onTap: () {
                            Navigator.pop(context);
                            Navigator.of(context).push(
                              createRoute(
                                OptionTestCaseStatusWidget(
                                  type: "edit",
                                  title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                  id: resultTestCaseStatus["id"],
                                  callbackOptionTest: (result) {
                                    handleEditTestCaseStatus(result, resultTestCaseStatus["id"]);
                                  },
                                  prjProjectId: prjProjectId,
                                ),
                              ),
                            );
                          },
                          contentPadding: const EdgeInsets.all(0),
                          hoverColor: Colors.transparent,
                          title: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                          leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
                        ),
                      ),
                      PopupMenuItem(
                        padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                        child: ListTile(
                          onTap: () {
                            Navigator.pop(context);
                            dialogDeleteTestCaseStatus(resultTestCaseStatus);
                          },
                          contentPadding: const EdgeInsets.all(0),
                          hoverColor: Colors.transparent,
                          title: const MultiLanguageText(name: "delete", defaultValue: "Delete"),
                          leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
          ],
        ),
      ),
    );
  }

  dialogDeleteTestCaseStatus(selectItem) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogDelete(
          selectedItem: selectItem,
          callback: () {
            handleDeleteTestCaseStatus(selectItem["id"]);
          },
          type: 'TestCaseStatus',
        );
      },
    );
  }
}
