import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/custom_state.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';

class SettingParameterWidget extends StatefulWidget {
  final String prjProjectId;
  const SettingParameterWidget({Key? key, required this.prjProjectId}) : super(key: key);

  @override
  State<SettingParameterWidget> createState() => _SettingParameterWidgetState();
}

class _SettingParameterWidgetState extends CustomState<SettingParameterWidget> {
  // var parameterFieldList = [];
  final _formKey = GlobalKey<FormState>();
  var listCheckDuplicate = {};
  dynamic selectedItem;
  bool isLoad = false;
  @override
  void initState() {
    super.initState();
    getInitPage();
  }

  getInitPage() async {
    isLoad = false;
    var response = await httpGet("/test-framework-api/user/project/prj-project/${widget.prjProjectId}", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        if (response["body"]["result"]["projectConfig"] != null && response["body"]["result"]["projectConfig"]["parameterFieldList"] != null) {
          for (var parameterField in response["body"]["result"]["projectConfig"]["parameterFieldList"]) {
            parameterField["variableType"] ??= "version_variable";
          }
        }
        selectedItem = response["body"]["result"];
        isLoad = true;
      });
    }
  }

  handleEditProject(result, prjProjectId) async {
    var response = await httpPost("/test-framework-api/user/project/prj-project/$prjProjectId", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        getInitPage();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return isLoad
          ? Scaffold(
              body: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    NavMenuTabV1(
                      currentUrl: "/setting/parameter",
                      margin: const EdgeInsets.fromLTRB(50, 20, 50, 0),
                      navigationList: context.settingMenuList,
                    ),
                    headerWidget(context),
                    headerTableWidget(),
                    Expanded(
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                          child: Column(
                            children: [
                              if (selectedItem["projectConfig"] != null)
                                for (var parameterField in selectedItem["projectConfig"]["parameterFieldList"] ?? [])
                                  parameterFieldWidget(parameterField),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      alignment: Alignment.center,
                      child: FloatingActionButton.extended(
                        heroTag: null,
                        icon: const Icon(Icons.add),
                        label: const MultiLanguageText(name: "new_parameter", defaultValue: "New parameter"),
                        onPressed: () {
                          setState(() {
                            selectedItem["projectConfig"] ??= {};
                            selectedItem["projectConfig"]["parameterFieldList"] ??= [];
                            selectedItem["projectConfig"]["parameterFieldList"].add({"variableType": "version_variable", "dataType": "String"});
                          });
                        },
                      ),
                    ),
                    Center(
                      child: DynamicButton(
                        name: "save",
                        label: multiLanguageString(name: "save", defaultValue: "Save", isLowerCase: false, context: context),
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            listCheckDuplicate.clear();
                            if (selectedItem["projectConfig"] != null) {
                              for (var element in selectedItem["projectConfig"]["parameterFieldList"]) {
                                listCheckDuplicate.putIfAbsent("${element["name"]}", () => element["name"]);
                              }
                              if (selectedItem["projectConfig"]["parameterFieldList"].length == listCheckDuplicate.length) {
                                handleEditProject(selectedItem, selectedItem["id"]);
                              } else {
                                showToast(
                                    context: context,
                                    msg: multiLanguageString(
                                        name: "name_has_been_duplicated", defaultValue: "Name has been duplicated", context: context),
                                    color: const Color.fromRGBO(255, 129, 130, 0.4),
                                    icon: const Icon(Icons.error));
                              }
                            }
                          }
                        },
                      ),
                    ),
                    const SizedBox(height: 20),
                  ],
                ),
              ),
            )
          : const Center(child: CircularProgressIndicator());
    });
  }

  Container parameterFieldWidget(parameterField) {
    return Container(
      margin: const EdgeInsets.only(bottom: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: DynamicDropdownButton(
              value: parameterField["variableType"],
              hint: multiLanguageString(name: "choose_a_variable_type", defaultValue: "Choose a variableType", context: context),
              items: [
                {
                  "value": "version_variable",
                  "name": multiLanguageString(name: "version_variable", defaultValue: "Version variable", context: context)
                },
                {
                  "value": "environment_variable",
                  "name": multiLanguageString(name: "environment_variable", defaultValue: "Environment variable", context: context)
                },
                {"value": "node_variable", "name": multiLanguageString(name: "agent_variable", defaultValue: "Agent variable", context: context)}
              ].map<DropdownMenuItem<String>>((dynamic result) {
                return DropdownMenuItem(
                  value: result["value"],
                  child: Text(result["name"]),
                );
              }).toList(),
              onChanged: (newValue) {
                setState(() {
                  _formKey.currentState!.validate();
                  parameterField["variableType"] = newValue;
                });
              },
              isRequiredNotEmpty: true,
              borderRadius: 5,
            ),
          ),
          // Expanded(
          //   child: DynamicDropdownButton(
          //     value: parameterField["dataType"],
          //     hint: multiLanguageString(name: "choose_a_data_type", defaultValue: "Choose a dataType", context: context),
          //     items: const ["String"].map<DropdownMenuItem<String>>((String result) {
          //       return DropdownMenuItem(
          //         value: result,
          //         child: Text(result),
          //       );
          //     }).toList(),
          //     onChanged: (newValue) {
          //       setState(() {
          //         _formKey.currentState!.validate();
          //         parameterField["dataType"] = newValue;
          //       });
          //     },
          //     isRequiredNotEmpty: true,
          //
          //     borderRadius: 5,
          //   ),
          // ),
          Expanded(
            flex: 2,
            child: Row(
              children: [
                Expanded(
                  child: DynamicTextField(
                    controller: TextEditingController(text: parameterField["name"] ?? ""),
                    hintText: multiLanguageString(name: "enter_a_name", defaultValue: "Enter a name", context: context),
                    onChanged: (text) {
                      (_formKey.currentState!.validate());
                      parameterField["name"] = text;
                    },
                    isRequiredNotEmpty: true,
                    isRequiredRegex: true,
                  ),
                ),
                InkWell(
                    onTap: () {
                      setState(() {
                        selectedItem["projectConfig"]["parameterFieldList"].remove(parameterField);
                      });
                    },
                    child: const Icon(
                      Icons.remove_circle,
                      color: Colors.red,
                    ))
              ],
            ),
          ),
        ],
      ),
    );
  }

  Padding headerTableWidget() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 0, 50, 10),
      child: Row(
        children: [
          Expanded(
            child: Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 12),
              decoration: BoxDecoration(
                border: Border.all(width: 1, color: const Color.fromARGB(255, 205, 206, 207), style: BorderStyle.solid),
                color: const Color.fromARGB(255, 222, 223, 224),
              ),
              child: MultiLanguageText(
                name: "variable_type",
                defaultValue: "VariableType",
                style: TextStyle(fontSize: context.fontSizeManager.fontSizeText14, fontWeight: FontWeight.bold, letterSpacing: 2),
              ),
            ),
          ),
          // Expanded(
          //   child: Container(
          //     alignment: Alignment.center,
          //     padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 12),
          //     decoration: BoxDecoration(
          //       border: Border.all(width: 1, color: const Color.fromARGB(255, 205, 206, 207), style: BorderStyle.solid),
          //       color: const Color.fromARGB(255, 222, 223, 224),
          //     ),
          //     child: MultiLanguageText(
          //       name: "data_type_adjacent",
          //       defaultValue: "DataType",
          //       style: TextStyle(fontSize: context.fontSizeManager.fontSizeText14, fontWeight: FontWeight.bold, letterSpacing: 2),
          //     ),
          //   ),
          // ),
          Expanded(
            flex: 2,
            child: Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 12),
              decoration: BoxDecoration(
                color: const Color.fromARGB(255, 222, 223, 224),
                border: Border.all(width: 1, color: const Color.fromARGB(255, 205, 206, 207), style: BorderStyle.solid),
              ),
              margin: const EdgeInsets.only(right: 25),
              child: MultiLanguageText(
                name: "name",
                defaultValue: "Name",
                style: TextStyle(fontSize: context.fontSizeManager.fontSizeText14, fontWeight: FontWeight.bold, letterSpacing: 2),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Padding headerWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
      child: MultiLanguageText(
        name: "project_parameters",
        defaultValue: "Project Parameters",
        style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
      ),
    );
  }
}
