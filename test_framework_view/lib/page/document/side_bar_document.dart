import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/tree.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';

import '../../common/custom_state.dart';

class SideBarDocumentWidget extends StatefulWidget {
  final String? nameSideBarItem;
  final VoidCallback? onClose;
  final String documentType;
  const SideBarDocumentWidget({Key? key, this.nameSideBarItem, this.onClose, required this.documentType}) : super(key: key);

  @override
  State<SideBarDocumentWidget> createState() => _SideBarDocumentWidgetState();
}

class _SideBarDocumentWidgetState extends CustomState<SideBarDocumentWidget> {
  int selectedIndex = 0;
  TreeData? docDocumentTreeData;
  @override
  void initState() {
    getListDocDocument();
    super.initState();
  }

  getListDocDocument() async {
    var docDocumentFindResponse =
        await httpPost("/test-framework-api/guest/doc/doc-document/search", {"status": 1, "documentType": widget.documentType}, context);
    if (docDocumentFindResponse.containsKey("body") && docDocumentFindResponse["body"] is String == false) {
      var docDocumentList = docDocumentFindResponse["body"]["resultList"];
      List<TreeNode> treeNodeList = [];
      for (var docDocument in docDocumentList) {
        treeNodeList.add(TreeNode(key: docDocument["id"], parentKey: docDocument["parentId"], item: docDocument));
      }
      setState(() {
        docDocumentTreeData = TreeData(treeNodeList);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        border: Border(
          right: BorderSide(
            color: Color.fromRGBO(0, 0, 0, 0.1),
          ),
        ),
      ),
      width: 350,
      child: Column(
        children: [
          if (widget.onClose != null)
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  MultiLanguageText(
                    name: "test_framework_guide",
                    defaultValue: "Test Framework Guide",
                    isLowerCase: false,
                    style: Style(context).styleTitleDialog,
                  ),
                  TextButton(
                    onPressed: widget.onClose,
                    child: const Icon(
                      Icons.close,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
            ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  if (docDocumentTreeData != null)
                    for (var docDocumentTreeNode in docDocumentTreeData!.rootList)
                      SideBarItemWidget(
                        hasParent: false,
                        isExpand: true,
                        docDocumentTreeNode: docDocumentTreeNode,
                        nameSideBarItem: widget.nameSideBarItem,
                      ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class SideBarItemWidget extends StatefulWidget {
  final TreeNode? docDocumentTreeNode;
  final bool hasParent;
  final bool isExpand;
  final String? nameSideBarItem;
  const SideBarItemWidget({Key? key, this.hasParent = true, this.isExpand = false, this.docDocumentTreeNode, this.nameSideBarItem}) : super(key: key);

  @override
  State<SideBarItemWidget> createState() => _SideBarItemWidgetState();
}

class _SideBarItemWidgetState extends CustomState<SideBarItemWidget> {
  bool isExpand = false;

  @override
  void initState() {
    isExpand = widget.isExpand;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextButton(
          onPressed: () {
            if (widget.docDocumentTreeNode!.children.isEmpty) {
              Provider.of<NavigationModel>(context, listen: false).navigate("/qa-platform/docs/${widget.docDocumentTreeNode!.item["name"]}");
            } else {
              setState(() {
                isExpand = !isExpand;
              });
            }
          },
          style: ButtonStyle(
            foregroundColor: MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states) {
              if (states.contains(MaterialState.hovered)) return Colors.blue;
              return widget.hasParent ? const Color.fromRGBO(122, 131, 142, 1) : const Color.fromRGBO(27, 35, 49, 1);
            }),
          ),
          child: Container(
            margin: !widget.hasParent ? const EdgeInsets.symmetric(vertical: 10, horizontal: 0) : null,
            padding: !widget.hasParent
                ? const EdgeInsets.symmetric(vertical: 5, horizontal: 10)
                : (widget.docDocumentTreeNode!.hasChildren)
                    ? const EdgeInsets.only(left: 7)
                    : null,
            child: Row(
              children: [
                if (widget.docDocumentTreeNode!.children.isNotEmpty)
                  Icon(
                    isExpand ? Icons.expand_more : Icons.navigate_next,
                    size: 16,
                  ),
                Expanded(
                  child: Text(
                    widget.docDocumentTreeNode!.item["title"],
                    style: widget.hasParent
                        ? TextStyle(
                            fontSize: context.fontSizeManager.fontSizeText14,
                            fontWeight: FontWeight.w400,
                            color: widget.docDocumentTreeNode!.item["name"] == widget.nameSideBarItem ? Colors.blue : null,
                          )
                        : TextStyle(
                            fontSize: context.fontSizeManager.fontSizeText16,
                            letterSpacing: 1.5,
                            fontWeight: FontWeight.w600,
                          ),
                  ),
                )
              ],
            ),
          ),
        ),
        if (isExpand)
          for (var docDocumentTreeNodeChild in widget.docDocumentTreeNode!.children)
            Container(
              margin: const EdgeInsets.only(bottom: 5),
              padding: const EdgeInsets.only(left: 40),
              child: SideBarItemWidget(
                docDocumentTreeNode: docDocumentTreeNodeChild,
                nameSideBarItem: widget.nameSideBarItem,
                isExpand: true,
              ),
            ),
      ],
    );
  }
}
