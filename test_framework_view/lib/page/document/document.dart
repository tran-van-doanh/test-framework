import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/header_application.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/document/side_bar_document.dart';

import '../../common/custom_state.dart';

class DocumentWidget extends StatefulWidget {
  final String? nameSideBarItem;
  const DocumentWidget({Key? key, this.nameSideBarItem}) : super(key: key);

  @override
  State<DocumentWidget> createState() => _DocumentWidgetState();
}

class _DocumentWidgetState extends CustomState<DocumentWidget> {
  String? content;
  bool isExpandSideBar = false;
  @override
  void initState() {
    super.initState();
    getContentDocument();
  }

  getContentDocument() async {
    var findRequest = {"name": widget.nameSideBarItem, "status": 1, "documentType": "document"};
    var response = await httpPost("/test-framework-api/guest/doc/doc-document/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      if (response["body"]["resultList"].isNotEmpty) {
        setState(() {
          content = response["body"]["resultList"][0]["content"];
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    if (content == null) {
      return const Center(child: CircularProgressIndicator());
    }
    return Consumer2<NavigationModel, SideBarDocumentModel>(builder: (context, navigationModel, sideBarDocumentModel, child) {
      return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
        if (constraints.maxWidth >= 1200) {
          isExpandSideBar = false;
        }
        return Scaffold(
          appBar: AppBar(
            leading: constraints.maxWidth < 1200
                ? IconButton(
                    onPressed: () {
                      setState(() {
                        isExpandSideBar = !isExpandSideBar;
                      });
                    },
                    icon: Icon(
                      Icons.menu,
                      color: isExpandSideBar ? Colors.blue : Colors.black,
                    ),
                  )
                : const SizedBox.shrink(),
            title: Row(
              children: [
                InkWell(
                  hoverColor: Colors.transparent,
                  onTap: () {
                    navigationModel.navigate("/qa-platform/project/${navigationModel.prjProjectId}/insights/dashboard");
                  },
                  child: SvgPicture.asset(
                    'assets/images/logo.svg',
                    colorFilter: const ColorFilter.mode(Colors.black, BlendMode.srcIn),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.symmetric(vertical: 3, horizontal: 8),
                  decoration: BoxDecoration(
                    color: const Color.fromRGBO(195, 205, 255, 1),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Text(
                    "DOCS",
                    style: TextStyle(color: const Color.fromRGBO(89, 114, 254, 1), fontSize: context.fontSizeManager.fontSizeText12),
                  ),
                ),
                Expanded(
                  child: Center(
                    child: Text(
                      "LLQ QA Platform Documentation",
                      style: TextStyle(
                          fontSize: context.fontSizeManager.fontSizeText20,
                          fontWeight: FontWeight.w600,
                          letterSpacing: 1.5,
                          color: const Color.fromRGBO(56, 66, 72, 1)),
                    ),
                  ),
                ),
              ],
            ),
            toolbarHeight: 60,
            titleTextStyle:
                TextStyle(color: const Color.fromRGBO(49, 49, 49, 1), fontSize: context.fontSizeManager.fontSizeText16, fontWeight: FontWeight.w500),
            backgroundColor: Colors.white,
            elevation: 2,
            actions: [
              Container(
                padding: const EdgeInsets.symmetric(vertical: 10),
                width: 200,
                child: TextField(
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(),
                    hintText: multiLanguageString(name: "search", defaultValue: "Search", context: context),
                    prefixIcon: const Icon(Icons.search),
                    contentPadding: const EdgeInsets.all(0),
                  ),
                  readOnly: true,
                  onTap: () {
                    dialogSearchDocument();
                  },
                ),
              ),
              const UserMenuButtonWidget(),
            ],
          ),
          body: Row(
            children: [
              if (constraints.maxWidth >= 1200 || isExpandSideBar)
                SideBarDocumentWidget(nameSideBarItem: widget.nameSideBarItem, documentType: "document"),
              Expanded(
                child: Center(
                  child: Markdown(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    selectable: true,
                    data: '''
$content
''',
                  ),
                ),
              ),
            ],
          ),
        );
      });
    });
  }

  dialogSearchDocument() {
    showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          return const DialogSearchDocumentWidget();
        });
  }
}

class DialogSearchDocumentWidget extends StatefulWidget {
  const DialogSearchDocumentWidget({Key? key}) : super(key: key);

  @override
  State<DialogSearchDocumentWidget> createState() => _DialogSearchDocumentWidgetState();
}

class _DialogSearchDocumentWidgetState extends CustomState<DialogSearchDocumentWidget> {
  List listSearchDocument = [];
  search(titleLike) async {
    Map<dynamic, dynamic> findRequest = {"status": 1};
    if (titleLike != null && titleLike.isNotEmpty) {
      findRequest["titleLike"] = "%$titleLike%";
    }
    var response = await httpPost("/test-framework-api/guest/doc/doc-document/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        listSearchDocument = response["body"]["resultList"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: const EdgeInsets.all(0),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "search_document",
            defaultValue: "Search Document",
            isLowerCase: false,
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
              onPressed: () => Navigator.pop(context, 'Cancel'),
              child: const Icon(
                Icons.close,
                color: Colors.black,
              ))
        ],
      ),
      content: Container(
        decoration: const BoxDecoration(
            border: Border(top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)), bottom: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)))),
        margin: const EdgeInsets.only(top: 20, bottom: 10),
        height: 600,
        width: 850,
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            DynamicTextField(
              autofocus: true,
              prefixIcon: const Icon(Icons.search),
              onChanged: (text) {
                search(text);
              },
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    for (var searchDocument in listSearchDocument)
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                          Provider.of<NavigationModel>(context, listen: false).navigate("qa-platform/docs/${searchDocument["name"]}");
                        },
                        child: ListTile(
                          title: Text(searchDocument["title"]),
                          subtitle: Text(searchDocument["description"]),
                          hoverColor: Colors.grey,
                        ),
                      )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
