import 'dart:convert';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/config.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/type.dart';

import 'package:test_framework_view/common/file_download.dart';
import 'package:http/http.dart' as http;

class AdminConfigWidget extends StatefulWidget {
  const AdminConfigWidget({Key? key}) : super(key: key);

  @override
  State<AdminConfigWidget> createState() => _AdminConfigWidgetState();
}

class _AdminConfigWidgetState extends State<AdminConfigWidget> {
  var prjProjectId = "";
  PlatformFile? fileTestCase;
  @override
  void initState() {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    prjProjectId = navigationModel.prjProjectId ?? "";
    super.initState();
  }

  uploadFile(PlatformFile? file, String url) async {
    final request = http.MultipartRequest(
      "POST",
      Uri.parse(
        "$baseUrl$url",
      ),
    );
    request.headers.addAll({"Authorization": Provider.of<SecurityModel>(context, listen: false).authorization!});
    request.files.add(http.MultipartFile.fromBytes('file', await readFile(file!), filename: file.name));
    var resp = await request.send();
    var responseDecode = jsonDecode(await resp.stream.bytesToString());
    if (responseDecode.containsKey("errorMessage") && mounted) {
      showToast(context: context, msg: responseDecode["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
    } else if (mounted) {
      showToast(
          context: context,
          msg: multiLanguageString(name: "upload_success", defaultValue: "Upload success", context: context),
          color: Colors.greenAccent,
          icon: const Icon(Icons.done));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return ListView(
        padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 30),
            child: MultiLanguageText(
              name: "config",
              defaultValue: "Config",
              style: TextStyle(
                fontSize: context.fontSizeManager.fontSizeText30,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              onPressed: () async {
                fileTestCase = await selectFile(['json']);
                if (fileTestCase != null) {
                  await uploadFile(fileTestCase, "/test-framework-api/admin/global-config-file/import");
                }
              },
              icon: const FaIcon(FontAwesomeIcons.fileArrowUp, color: Color.fromRGBO(49, 49, 49, 1), size: 20),
              label: MultiLanguageText(
                name: "import_a_file",
                defaultValue: "Import a File",
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            alignment: Alignment.centerLeft,
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              onPressed: () async {
                File? downloadedFile = await httpDownloadFile(context, "/test-framework-api/admin/global-config-file/export",
                    Provider.of<SecurityModel>(context, listen: false).authorization, 'Output.json');
                if (downloadedFile != null && mounted) {
                  var snackBar = SnackBar(
                    content: MultiLanguageText(name: "file_downloaded_to", defaultValue: "File downloaded to \$0 .", variables: ["$downloadedFile"]),
                  );
                  ScaffoldMessenger.of(this.context).showSnackBar(snackBar);
                }
              },
              icon: const FaIcon(FontAwesomeIcons.fileArrowDown, color: Color.fromRGBO(49, 49, 49, 1), size: 20),
              label: MultiLanguageText(
                name: "export_a_file",
                defaultValue: "Export a File",
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
            ),
          ),
        ],
      );
    });
  }
}
