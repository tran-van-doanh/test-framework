import 'package:flutter/material.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';

class EmptyPageWidget extends StatelessWidget {
  const EmptyPageWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        MultiLanguageText(
          name: "no_result_found",
          defaultValue: "No result found",
          style: TextStyle(
            color: const Color.fromRGBO(61, 90, 254, 1),
            fontSize: context.fontSizeManager.fontSizeText40,
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        const MultiLanguageText(
          name: "couldnt_find",
          defaultValue: "We couldn't find what you searched for.",
          style: TextStyle(color: Color.fromRGBO(133, 135, 145, 1)),
        ),
        const MultiLanguageText(
          name: "try_searching_again",
          defaultValue: "Try searching again.",
          style: TextStyle(color: Color.fromRGBO(133, 135, 145, 1)),
        )
      ],
    );
  }
}
