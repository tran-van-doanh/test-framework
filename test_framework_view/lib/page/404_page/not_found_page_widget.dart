import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';

class NotFoundPageWidget extends StatelessWidget {
  const NotFoundPageWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<NavigationModel>(builder: (context, navigationModel, child) {
      return Material(
        child: Stack(
          children: [
            Center(child: Image.asset("/images/404.png")),
            Positioned(
              right: MediaQuery.of(context).size.width / 2 - 90,
              bottom: 20.0,
              child: SizedBox(
                height: 40,
                child: FloatingActionButton.extended(
                  heroTag: null,
                  onPressed: () {
                    if (navigationModel.product == "qa-platform") {
                      navigationModel.navigate("/qa-platform/project/${navigationModel.prjProjectId}/insights/dashboard");
                    } else if (navigationModel.product == "resource-pool") {
                      navigationModel.navigate("/resource-pool/insights");
                    } else if (navigationModel.product == "admin") {
                      navigationModel.navigate("/admin/dashboard");
                    } else if (navigationModel.product == "workflow") {
                      navigationModel.navigate("/workflow/status");
                    } else if (navigationModel.product == "prj-360") {
                      navigationModel.navigate("/prj-360/dashboard");
                    } else {
                      navigationModel.navigate("/${navigationModel.product}/project/${navigationModel.prjProjectId}/summary");
                    }
                  },
                  icon: const Icon(Icons.home, color: Colors.white),
                  label: MultiLanguageText(
                    name: "go_to_dashboard",
                    defaultValue: "Go to DashBoard",
                    style: TextStyle(fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: Colors.white),
                  ),
                  backgroundColor: const Color.fromRGBO(76, 151, 255, 1),
                  elevation: 0,
                  hoverElevation: 0,
                ),
              ),
            ),
          ],
        ),
      );
    });
  }
}
