import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/common/file_download.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';
import 'package:test_framework_view/page/list_test/tests/directory/dialog_directory.dart';
import 'package:test_framework_view/type.dart';

import '../../common/custom_state.dart';
import '../../components/dynamic_button.dart';

class DialogOptionFile extends StatefulWidget {
  final String type;
  final String title;
  final String? id;
  final Function? callbackOptionTest;
  final String? tfTestDirectoryId;

  const DialogOptionFile({Key? key, required this.type, this.id, this.callbackOptionTest, required this.title, this.tfTestDirectoryId})
      : super(key: key);

  @override
  State<DialogOptionFile> createState() => _DialogOptionFileState();
}

class _DialogOptionFileState extends CustomState<DialogOptionFile> {
  late Future future;
  final TextEditingController _fileDesController = TextEditingController();
  final TextEditingController _fileTitleController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  PlatformFile? objFile;
  String? errorFile;
  String? fileName;
  var prjProjectId = "";
  List directoryPathList = [];
  var _listProject = [];
  int _status = 0;
  dynamic selectedItem;
  @override
  void initState() {
    prjProjectId = Provider.of<NavigationModel>(context, listen: false).prjProjectId ?? "";
    future = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    if (widget.type == "duplicate") {
      getListProject();
    }
    if (widget.id != null) {
      await getSelectedItem(widget.id!);
      await setInitValue();
    }
    if (selectedItem != null && selectedItem["tfTestDirectoryId"] != null) {
      getDirectoryPath(selectedItem["tfTestDirectoryId"]);
    } else if (widget.tfTestDirectoryId != null) {
      getDirectoryPath(widget.tfTestDirectoryId!);
    }

    return 0;
  }

  getDirectoryPath(String id) async {
    var directoryParentResponse = await httpGet("/test-framework-api/user/test-framework/tf-test-directory/$id/path", context);
    if (directoryParentResponse.containsKey("body") && directoryParentResponse["body"] is String == false) {
      setState(() {
        directoryPathList = directoryParentResponse["body"]["resultList"];
      });
    }
  }

  getListProject() async {
    var response = await httpPost("/test-framework-api/user/project/prj-project/search", {"status": 1}, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listProject = response["body"]["resultList"];
      });
    }
  }

  getSelectedItem(String id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-file/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        selectedItem = response["body"]["result"];
      });
    }
  }

  setInitValue() {
    setState(() {
      if (selectedItem != null) {
        _status = selectedItem["status"] ?? 0;
        _fileDesController.text = selectedItem["description"] ?? "";
        _fileTitleController.text = selectedItem["title"];
        fileName = selectedItem["fileName"];
      }
    });
  }

  @override
  void dispose() {
    _fileDesController.dispose();
    _fileTitleController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return FutureBuilder(
        future: future,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return TestFrameworkRootPageWidget(
              child: Column(
                children: [
                  headerWidget(context),
                  const Divider(color: Color.fromRGBO(216, 218, 229, 1)),
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.only(top: 20, bottom: 10),
                      child: Form(
                        key: _formKey,
                        child: SingleChildScrollView(
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                            child: FocusTraversalGroup(
                              policy: OrderedTraversalPolicy(),
                              child: bodyWidget(context),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  btnWidget(context)
                ],
              ),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        },
      );
    });
  }

  Padding btnWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20, top: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ButtonSave(onPressed: () {
            if (_formKey.currentState!.validate() && fileName != null) {
              var result = {};
              if (selectedItem != null) {
                result = selectedItem;
                result["title"] = _fileTitleController.text;
                result["description"] = _fileDesController.text;
                result["status"] = _status;
                if (objFile?.bytes != null) {
                  result["value"] = objFile?.bytes;
                }
                if (objFile?.name != null) {
                  result["fileName"] = objFile?.name;
                }
                if (objFile?.name != null && objFile?.bytes != null) {
                  result["isChangeFile"] = true;
                }
                if (directoryPathList.isNotEmpty) {
                  result["tfTestDirectoryId"] = directoryPathList.last["id"];
                } else if (result.containsKey("tfTestDirectoryId")) {
                  result.remove("tfTestDirectoryId");
                }
              } else {
                result = {
                  "title": _fileTitleController.text,
                  "description": _fileDesController.text,
                  "status": _status,
                  "prjProjectId": prjProjectId,
                  "sysUserId": Provider.of<SecurityModel>(context, listen: false).userLoginCurren["id"],
                  "value": objFile?.bytes,
                  if (directoryPathList.isNotEmpty) "tfTestDirectoryId": directoryPathList.last["id"],
                  "fileName": objFile?.name,
                };
              }
              widget.callbackOptionTest!(result);
            } else {
              setState(() {
                errorFile = multiLanguageString(name: "please_choose_a_file", defaultValue: "Please choose a file", context: context);
              });
            }
          }),
          const ButtonCancel()
        ],
      ),
    );
  }

  Column bodyWidget(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (widget.type == "duplicate")
          Container(
            margin: const EdgeInsets.only(bottom: 10),
            child: Row(
              children: [
                Expanded(
                  flex: 3,
                  child: MultiLanguageText(name: "project", defaultValue: "Project", style: Style(context).styleTitleDialog),
                ),
                Expanded(
                  flex: 7,
                  child: DynamicDropdownButton(
                      borderRadius: 5,
                      value: prjProjectId,
                      hint: multiLanguageString(name: "choose_a_project", defaultValue: "Choose a project", context: context),
                      items: _listProject.map<DropdownMenuItem<String>>((dynamic result) {
                        return DropdownMenuItem(
                          value: result["id"],
                          child: Text(result["title"]),
                        );
                      }).toList(),
                      onChanged: (newValue) async {
                        setState(() {
                          _formKey.currentState!.validate();
                          prjProjectId = newValue!;
                        });
                      },
                      isRequiredNotEmpty: true),
                ),
              ],
            ),
          ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "directory", defaultValue: "Directory", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    MultiLanguageText(
                      name: "root",
                      defaultValue: "Root",
                      style: Style(context).styleTextDataCell,
                    ),
                    if (directoryPathList.isNotEmpty)
                      Expanded(
                        child: Row(
                          children: [
                            for (var directoryPath in directoryPathList)
                              RichText(
                                text: TextSpan(
                                  children: [
                                    const TextSpan(
                                      text: " / ",
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                    TextSpan(
                                      text: directoryPath != null ? directoryPath["title"] ?? "" : "null",
                                      style: Style(context).styleTextDataCell,
                                    ),
                                  ],
                                ),
                              ),
                          ],
                        ),
                      ),
                    FloatingActionButton.extended(
                      heroTag: null,
                      onPressed: () {
                        showDialog<String>(
                          context: context,
                          builder: (BuildContext context) {
                            return DialogDirectory(
                              type: "file",
                              prjProjectId: prjProjectId,
                              directoryPath: (directoryPathList.isNotEmpty) ? directoryPathList.last : null,
                              callbackSelectedDirectory: (directoryId) {
                                if (directoryId != "root") {
                                  getDirectoryPath(directoryId);
                                } else {
                                  setState(() {
                                    directoryPathList = [];
                                  });
                                }
                              },
                            );
                          },
                        );
                      },
                      icon: const Icon(Icons.folder, color: Colors.white),
                      label: MultiLanguageText(
                        name: "directory",
                        defaultValue: "Directory",
                        style: TextStyle(fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: Colors.white),
                      ),
                      backgroundColor: Colors.blue,
                      elevation: 0,
                      hoverElevation: 0,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "name", defaultValue: "Name", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _fileTitleController,
                  hintText: "...",
                  onChanged: (text) {
                    _formKey.currentState!.validate();
                  },
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "status", defaultValue: "Status", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                    borderRadius: 5,
                    value: _status,
                    hint: multiLanguageString(name: "choose_a_status", defaultValue: "Choose a status", context: context),
                    items: [
                      {"name": multiLanguageString(name: "active", defaultValue: "Active", context: context), "value": 1},
                      {"name": multiLanguageString(name: "inactive", defaultValue: "Inactive", context: context), "value": 0},
                    ].map<DropdownMenuItem<int>>((dynamic result) {
                      return DropdownMenuItem(
                        value: result["value"],
                        child: Text(result["name"]),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        _status = newValue!;
                      });
                    },
                    isRequiredNotEmpty: true),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "description", defaultValue: "Description", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _fileDesController,
                  hintText: "...",
                  onChanged: (text) {
                    _formKey.currentState!.validate();
                  },
                  maxline: 3,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "file", defaultValue: "File", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    if (fileName != null) Text(fileName!),
                    if (errorFile != null)
                      Text(
                        errorFile!,
                        style: TextStyle(color: Colors.red, fontSize: context.fontSizeManager.fontSizeText16),
                      )
                  ],
                ),
              ),
            ],
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FloatingActionButton.extended(
              heroTag: null,
              onPressed: () async {
                objFile = await selectFile(['xlsx']);
                if (objFile != null) {
                  setState(() {
                    fileName = objFile!.name;
                    errorFile = null;
                  });
                }
              },
              icon: const Icon(Icons.cloud_upload, color: Colors.white),
              label: MultiLanguageText(
                name: "choose_file",
                defaultValue: "Choose file",
                style: TextStyle(fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: Colors.white),
              ),
              backgroundColor: Colors.redAccent,
              elevation: 0,
              hoverElevation: 0,
            ),
            if (widget.type == "edit")
              Padding(
                padding: const EdgeInsets.only(left: 20),
                child: FloatingActionButton.extended(
                  heroTag: null,
                  onPressed: () async {
                    File? downloadedFile = await httpDownloadFile(context, "/test-framework-api/user/test-framework-file/tf-file/${widget.id}",
                        Provider.of<SecurityModel>(context, listen: false).authorization, 'Output.xlsx');
                    if (downloadedFile != null && mounted) {
                      var snackBar = SnackBar(
                        content:
                            MultiLanguageText(name: "file_downloaded_to", defaultValue: "File downloaded to \$0 .", variables: ["$downloadedFile"]),
                      );
                      ScaffoldMessenger.of(this.context).showSnackBar(snackBar);
                    }
                  },
                  icon: const Icon(Icons.cloud_download, color: Colors.white),
                  label: MultiLanguageText(
                    name: "download_file",
                    defaultValue: "Download file",
                    style: TextStyle(fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: Colors.white),
                  ),
                  backgroundColor: Colors.blueAccent,
                  elevation: 0,
                  hoverElevation: 0,
                ),
              ),
          ],
        )
      ],
    );
  }

  Padding headerWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 35, 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(widget.title.toUpperCase(), style: Style(context).styleTitleDialog),
          TextButton(
            onPressed: () => Navigator.pop(context, 'cancel'),
            child: const Icon(
              Icons.close,
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }
}
