import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/components/badge/badge_icon.dart';
import 'package:test_framework_view/components/dialog/dialog_delete.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/config.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/file/dialog_option_file.dart';
import 'package:test_framework_view/page/file/header_file.dart';
import 'package:test_framework_view/page/list_test/tests/directory/dialog_option_directory.dart';
import 'package:test_framework_view/page/list_test/tests/directory/directory_item_widget.dart';
import 'package:test_framework_view/type.dart';

import '../../../../api.dart';
import '../../../../common/custom_state.dart';

class ListDirectoryFile extends StatefulWidget {
  final String type;
  const ListDirectoryFile({Key? key, required this.type}) : super(key: key);

  @override
  State<ListDirectoryFile> createState() => _ListDirectoryFileState();
}

class _ListDirectoryFileState extends CustomState<ListDirectoryFile> {
  late Future futureListTfDirectoryFile;
  List resultListDirectory = [];
  var resultListFile = [];
  var rowCountListFile = 0;
  var currentPageFile = 1;
  var rowPerPageFile = 10;
  var findRequestFile = {};
  var findRequestDirectory = {};
  var prjProjectId = "";
  bool isViewTypeDirectory = true;
  bool isExpandDirectory = true;
  List? directoryPathList;
  String? parentDirectoryId;
  @override
  void initState() {
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    prjProjectId = navigationModel.prjProjectId ?? "";
    futureListTfDirectoryFile = getInitPage();
    super.initState();
  }

  getInitPage() async {
    findRequestFile["tfTestDirectoryIdIsNull"] = true;
    findRequestDirectory["parentIdIsNull"] = true;
    await getListTfFile(currentPageFile, findRequestFile);
    await getListTfDirectory(findRequestDirectory);
    return 0;
  }

  getDirectoryPathList() async {
    var directoryPathResponse = await httpGet("/test-framework-api/user/test-framework/tf-test-directory/$parentDirectoryId/path", context);
    if (directoryPathResponse.containsKey("body") && directoryPathResponse["body"] is String == false) {
      directoryPathList = directoryPathResponse["body"]["resultList"];
    }
  }

  getListTfFile(page, findRequest) async {
    if ((page - 1) * rowPerPageFile > rowCountListFile) {
      page = (1.0 * rowCountListFile / rowPerPageFile).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequestFile = findRequest;
    if (findRequest["titleLike"] != null && findRequest["titleLike"].isNotEmpty) {
      findRequestFile["titleLike"] = "%${findRequest["titleLike"]}%";
    }
    findRequestFile["queryOffset"] = (page - 1) * rowPerPageFile;
    findRequestFile["queryLimit"] = rowPerPageFile;
    findRequestFile["prjProjectId"] = prjProjectId;
    var response = await httpPost("/test-framework-api/user/test-framework/tf-file/search", findRequestFile, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPageFile = page;
        rowCountListFile = response["body"]["rowCount"];
        resultListFile = response["body"]["resultList"];
        for (var element in resultListFile) {
          element["type"] = "file";
        }
      });
    }
    return 0;
  }

  getListTfDirectory(findRequest) async {
    var findRequestDirectory = findRequest;
    if (findRequest["titleLike"] != null && findRequest["titleLike"].isNotEmpty) {
      findRequestDirectory["titleLike"] = "%${findRequest["titleLike"]}%";
    }
    findRequestDirectory["directoryType"] = widget.type;
    findRequestDirectory["prjProjectId"] = prjProjectId;
    var responseDirectory = await httpPost("/test-framework-api/user/test-framework/tf-test-directory/search", findRequestDirectory, context);
    if (responseDirectory.containsKey("body") && responseDirectory["body"] is String == false && mounted) {
      for (var responseDirectory in responseDirectory["body"]["resultList"]) {
        responseDirectory["type"] = "directory";
      }
      setState(() {
        resultListDirectory = responseDirectory["body"]["resultList"];
      });
    }
    return 0;
  }

  handleDeleteDirectory(id) async {
    var response = await httpDelete("/test-framework-api/user/test-framework/tf-test-directory/$id", context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        futureListTfDirectoryFile = getListTfDirectory(findRequestDirectory);
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
      }
    }
  }

  handleDeleteFile(id) async {
    var response = await httpDelete("/test-framework-api/user/test-framework/tf-file/$id", context);

    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        futureListTfDirectoryFile = getListTfFile(currentPageFile, findRequestFile);
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
      }
    }
  }

  handleAddTfDirectory(result, {String? parentId}) async {
    if (parentId != null) {
      result["parentId"] = parentId;
    }
    var response = await httpPut("/test-framework-api/user/test-framework/tf-test-directory", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        Navigator.of(context).pop();
        futureListTfDirectoryFile = getListTfDirectory(findRequestDirectory);
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
      }
    }
  }

  handleAddTfFile(result) async {
    var response = await httpPut("/test-framework-api/user/test-framework/tf-file", result, context);

    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        uploadFile(response["body"]["result"], result["value"], result["fileName"]);
        Navigator.of(context).pop();
        futureListTfDirectoryFile = getListTfFile(currentPageFile, findRequestFile);
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
      }
    }
  }

  uploadFile(String fileId, value, filename) async {
    final request = http.MultipartRequest(
      "POST",
      Uri.parse(
        "$baseUrl/test-framework-api/user/test-framework-file/tf-file/$fileId",
      ),
    );
    request.headers.addAll({"Authorization": Provider.of<SecurityModel>(context, listen: false).authorization!});
    request.files.add(http.MultipartFile.fromBytes('file', value, filename: filename));
    var resp = await request.send();
    var responseDecode = jsonDecode(await resp.stream.bytesToString());
    if (responseDecode.containsKey("errorMessage") && mounted) {
      showToast(context: context, msg: responseDecode["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
    }
  }

  handleEditTfDirectory(result, id) async {
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-directory/$id", result, context);

    if (response.containsKey("body") && response["body"] is String == false) {
      if (response["body"].containsKey("errorMessage") && mounted) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        futureListTfDirectoryFile = getListTfDirectory(findRequestDirectory);
        return true;
      }
    }
    return false;
  }

  handleEditTfFile(result, id) async {
    var response = await httpPost("/test-framework-api/user/test-framework/tf-file/$id", result, context);

    if (response.containsKey("body") && response["body"] is String == false) {
      if (response["body"].containsKey("errorMessage") && mounted) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        if (result["isChangeFile"] != null && result["isChangeFile"]) {
          uploadFile(id, result["value"], result["fileName"]);
        }
        futureListTfDirectoryFile = getListTfFile(currentPageFile, findRequestFile);
        return true;
      }
    }
    return false;
  }

  handleDuplicateTfFile(result) async {
    var response = await httpPut("/test-framework-api/user/test-framework/tf-file", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        Navigator.of(context).pop();
        futureListTfDirectoryFile = getListTfFile(currentPageFile, findRequestFile);
        showToast(
            msg: multiLanguageString(name: "duplicate_successful", defaultValue: "Duplicate successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.error),
            context: context);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer3<NavigationModel, TestCaseDirectoryModel, LanguageModel>(
      builder: (context, navigationModel, testCaseDirectoryModel, languageModel, child) {
        return FutureBuilder(
          future: futureListTfDirectoryFile,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView(
                children: [
                  headerWidget(context),
                  Container(
                    margin: const EdgeInsets.fromLTRB(50, 0, 50, 20),
                    child: SelectionArea(
                      child: Container(
                        alignment: Alignment.topLeft,
                        child: LayoutBuilder(
                          builder: (BuildContext context, BoxConstraints constraints) {
                            return Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                (isViewTypeDirectory && isExpandDirectory)
                                    ? Padding(
                                        padding: const EdgeInsets.only(right: 10),
                                        child: tableDirectoryWidget(context, testCaseDirectoryModel, navigationModel))
                                    : const SizedBox.shrink(),
                                Expanded(
                                  child: tableFileWidget(context, navigationModel, testCaseDirectoryModel),
                                )
                              ],
                            );
                          },
                        ),
                      ),
                    ),
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  Column tableFileWidget(BuildContext context, NavigationModel navigationModel, TestCaseDirectoryModel testCaseDirectoryModel) {
    return Column(
      children: [
        Container(
          alignment: Alignment.centerLeft,
          margin: const EdgeInsets.only(bottom: 10),
          decoration: BoxDecoration(
            border: Border.all(
              color: const Color.fromRGBO(212, 212, 212, 1),
            ),
            color: Colors.grey[100],
          ),
          padding: (directoryPathList != null && directoryPathList!.isNotEmpty) ? const EdgeInsets.fromLTRB(3, 3, 12, 3) : const EdgeInsets.all(9),
          child: (directoryPathList != null && directoryPathList!.isNotEmpty)
              ? breadCrumbWidget(navigationModel)
              : Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        if (!isExpandDirectory)
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                isExpandDirectory = true;
                              });
                            },
                            child: const Icon(Icons.arrow_forward),
                          ),
                        MultiLanguageText(
                          name: "file",
                          defaultValue: "File",
                          style: Style(context).styleTextDataColumn,
                        ),
                      ],
                    ),
                    addTestWidget(context)
                  ],
                ),
        ),
        if (resultListFile.isNotEmpty)
          CustomDataTableWidget(
            columns: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(
                  name: "name",
                  defaultValue: "Name",
                  isLowerCase: false,
                  style: Style(context).styleTextDataColumn,
                ),
              ),
              Expanded(
                child: MultiLanguageText(
                  name: "owner",
                  defaultValue: "Owner",
                  isLowerCase: false,
                  style: Style(context).styleTextDataColumn,
                ),
              ),
              Expanded(
                child: Center(
                  child: MultiLanguageText(
                    name: "create_date",
                    defaultValue: "Create date",
                    isLowerCase: false,
                    style: Style(context).styleTextDataColumn,
                  ),
                ),
              ),
              Expanded(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    MultiLanguageText(
                      name: "status",
                      defaultValue: "Status",
                      isLowerCase: false,
                      style: Style(context).styleTextDataColumn,
                    ),
                    Tooltip(
                      message: multiLanguageString(name: "active_inactive", defaultValue: "Green = Active\nRed = Inactive", context: context),
                      child: const Icon(
                        Icons.info,
                        color: Colors.grey,
                        size: 16,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                width: 38,
              ),
            ],
            rows: [
              for (var resultFile in resultListFile)
                CustomDataRow(
                  cells: [
                    Expanded(
                      flex: 3,
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Row(
                          children: [
                            const Icon(Icons.description, color: Color.fromRGBO(61, 148, 254, 1)),
                            const SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: Text(
                                resultFile["title"],
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                                style: Style(context).styleTextDataCell,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Text(
                          resultFile["sysUser"]["fullname"],
                          style: Style(context).styleTextDataCell,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Center(
                          child: Text(
                            DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultFile["createDate"]).toLocal()),
                            style: Style(context).styleTextDataCell,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Center(
                          child: Badge2StatusSettingWidget(
                            status: resultFile["status"],
                          ),
                        ),
                      ),
                    ),
                    PopupMenuButton(
                      offset: const Offset(5, 50),
                      tooltip: '',
                      splashRadius: 10,
                      icon: const Icon(
                        Icons.more_vert,
                      ),
                      itemBuilder: (context) => [
                        PopupMenuItem(
                          padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                          child: ListTile(
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.of(context).push(
                                createRoute(
                                  DialogOptionFile(
                                    tfTestDirectoryId: parentDirectoryId,
                                    type: "edit",
                                    title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                    id: resultFile["id"],
                                    callbackOptionTest: (result) async {
                                      bool resultFunction = await handleEditTfFile(result, resultFile["id"]);
                                      if (resultFunction && mounted) {
                                        Navigator.of(this.context).pop();
                                        showToast(
                                            context: this.context,
                                            msg: multiLanguageString(
                                                name: "update_successful", defaultValue: "Update successful", context: this.context),
                                            color: Colors.greenAccent,
                                            icon: const Icon(Icons.done));
                                      }
                                    },
                                  ),
                                ),
                              );
                            },
                            contentPadding: const EdgeInsets.all(0),
                            hoverColor: Colors.transparent,
                            title: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                            leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
                          ),
                        ),
                        PopupMenuItem(
                          padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                          child: ListTile(
                            onTap: () {
                              Navigator.pop(context);
                              dialogDeleteFile(resultFile);
                            },
                            contentPadding: const EdgeInsets.all(0),
                            hoverColor: Colors.transparent,
                            title: const MultiLanguageText(name: "delete", defaultValue: "Delete"),
                            leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                          ),
                        ),
                        PopupMenuItem(
                          padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                          child: ListTile(
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.of(context).push(
                                createRoute(
                                  DialogOptionFile(
                                    tfTestDirectoryId: parentDirectoryId,
                                    type: "edit",
                                    title: multiLanguageString(name: "duplicate", defaultValue: "Duplicate", context: context),
                                    id: resultFile["id"],
                                    callbackOptionTest: (result) async {
                                      handleDuplicateTfFile(result);
                                    },
                                  ),
                                ),
                              );
                            },
                            contentPadding: const EdgeInsets.all(0),
                            hoverColor: Colors.transparent,
                            title: const MultiLanguageText(name: "duplicate", defaultValue: "Duplicate"),
                            leading: const Icon(Icons.control_point_duplicate, color: Color.fromARGB(255, 112, 114, 119)),
                          ),
                        ),
                        PopupMenuItem(
                          padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                          child: ListTile(
                            onTap: () {
                              testCaseDirectoryModel.setTestCaseDirectory(resultFile);
                              showToast(
                                  context: context,
                                  msg: multiLanguageString(
                                      name: "copy_result_test_case_successful",
                                      defaultValue: "Copy '\$0' successful, choose a directory to move",
                                      variables: ["${resultFile["title"]}"],
                                      context: context),
                                  color: Colors.greenAccent,
                                  icon: const Icon(Icons.done));
                              Navigator.pop(context);
                            },
                            contentPadding: const EdgeInsets.all(0),
                            hoverColor: Colors.transparent,
                            title: const MultiLanguageText(name: "move_to_directory", defaultValue: "Move to directory"),
                            leading: const Icon(Icons.drive_file_move, color: Color.fromARGB(255, 112, 114, 119)),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
            ],
          ),
        DynamicTablePagging(
          rowCountListFile,
          currentPageFile,
          rowPerPageFile,
          pageChangeHandler: (page) {
            setState(() {
              futureListTfDirectoryFile = getListTfFile(page, findRequestFile);
            });
          },
          rowPerPageChangeHandler: (rowPerPage) {
            setState(() {
              rowPerPageFile = rowPerPage!;
              futureListTfDirectoryFile = getListTfFile(1, findRequestFile);
            });
          },
        ),
      ],
    );
  }

  OutlinedButton addTestWidget(BuildContext context) {
    return OutlinedButton.icon(
      style: OutlinedButton.styleFrom(
        backgroundColor: const Color.fromARGB(175, 8, 196, 80),
      ),
      icon: const Icon(
        Icons.add,
        color: Color.fromRGBO(49, 49, 49, 1),
      ),
      label: MultiLanguageText(
        name: "new_file",
        defaultValue: "New file",
        isLowerCase: false,
        style: TextStyle(fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
      ),
      onPressed: () {
        Navigator.of(context).push(
          createRoute(
            DialogOptionFile(
              tfTestDirectoryId: parentDirectoryId,
              type: "create",
              title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
              callbackOptionTest: (result) async {
                handleAddTfFile(result);
              },
            ),
          ),
        );
      },
    );
  }

  Container tableDirectoryWidget(BuildContext context, TestCaseDirectoryModel testCaseDirectoryModel, NavigationModel navigationModel) {
    return Container(
      constraints: const BoxConstraints(maxWidth: 350, maxHeight: 560),
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: const Color.fromRGBO(212, 212, 212, 1),
              ),
              color: Colors.grey[100],
            ),
            padding: const EdgeInsets.all(11),
            child: Row(
              children: [
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      MultiLanguageText(
                        name: "directory",
                        defaultValue: "Directory",
                        style: Style(context).styleTextDataColumn,
                      ),
                      Row(
                        children: [
                          if (testCaseDirectoryModel.testCaseDirectory != null &&
                              (testCaseDirectoryModel.testCaseDirectory["tfTestDirectoryId"] != null ||
                                  testCaseDirectoryModel.testCaseDirectory["parentId"] != null))
                            GestureDetector(
                              onTap: () async {
                                onPaste(null, testCaseDirectoryModel, context);
                              },
                              child: const Icon(Icons.content_paste),
                            ),
                          GestureDetector(
                            onTap: () {
                              Navigator.of(context).push(
                                createRoute(
                                  OptionDirectoryWidget(
                                    directoryType: widget.type,
                                    type: "create",
                                    title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                                    callbackOptionTest: (result) {
                                      handleAddTfDirectory(result);
                                    },
                                    prjProjectId: prjProjectId,
                                  ),
                                ),
                              );
                            },
                            child: const Icon(Icons.add),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      isExpandDirectory = false;
                    });
                  },
                  child: const Icon(Icons.arrow_back),
                ),
              ],
            ),
          ),
          Expanded(
            child: Container(
              color: Colors.grey[100],
              margin: const EdgeInsets.only(top: 10),
              child: ListView(
                padding: const EdgeInsets.only(
                  left: 0,
                ),
                children: [
                  for (var resultDirectory in resultListDirectory)
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 5),
                      child: DirectoryItemWidget(
                        key: Key(resultDirectory["id"]),
                        testCaseType: widget.type,
                        prjProjectId: prjProjectId,
                        directory: resultDirectory,
                        parentDirectoryId: parentDirectoryId,
                        onDeleteDirectory: (id) => handleDeleteDirectory(id),
                        onPaste: (id) => onPaste(id, testCaseDirectoryModel, context),
                        onClickDirectory: (id) async {
                          setState(() {
                            parentDirectoryId = id;
                          });
                          getDirectoryPathList();
                          if (findRequestFile.containsKey("tfTestDirectoryIdIsNull")) {
                            findRequestFile.remove("tfTestDirectoryIdIsNull");
                          }
                          findRequestFile["tfTestDirectoryId"] = parentDirectoryId;
                          await getListTfFile(currentPageFile, findRequestFile);
                        },
                      ),
                    ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> onPaste(String? directoryId, TestCaseDirectoryModel testCaseDirectoryModel, BuildContext context) async {
    if (directoryId != null) {
      if (testCaseDirectoryModel.testCaseDirectory["directoryType"] != null) {
        testCaseDirectoryModel.testCaseDirectory["parentId"] = directoryId;
        testCaseDirectoryModel.testCaseDirectory["directoryType"] = widget.type;
      } else {
        testCaseDirectoryModel.testCaseDirectory["tfTestDirectoryId"] = directoryId;
      }
    } else {
      if (testCaseDirectoryModel.testCaseDirectory["directoryType"] != null) {
        if (testCaseDirectoryModel.testCaseDirectory.containsKey("parentId")) {
          testCaseDirectoryModel.testCaseDirectory.remove("parentId");
        }
        testCaseDirectoryModel.testCaseDirectory["directoryType"] = widget.type;
      } else if (testCaseDirectoryModel.testCaseDirectory.containsKey("tfTestDirectoryId")) {
        testCaseDirectoryModel.testCaseDirectory.remove("tfTestDirectoryId");
      }
    }
    bool resultFunction = false;
    if (testCaseDirectoryModel.testCaseDirectory["type"] == "file") {
      resultFunction = await handleEditTfFile(testCaseDirectoryModel.testCaseDirectory, testCaseDirectoryModel.testCaseDirectory["id"]);
    }
    if (testCaseDirectoryModel.testCaseDirectory["type"] == "directory") {
      resultFunction = await handleEditTfDirectory(testCaseDirectoryModel.testCaseDirectory, testCaseDirectoryModel.testCaseDirectory["id"]);
    }
    if (resultFunction && mounted) {
      showToast(
          context: this.context,
          msg: "Move '${testCaseDirectoryModel.testCaseDirectory["title"]}' success",
          color: Colors.greenAccent,
          icon: const Icon(Icons.done));
    }
    testCaseDirectoryModel.setTestCaseDirectory(null);
  }

  Row breadCrumbWidget(NavigationModel navigationModel) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            IconButton(
              onPressed: () async {
                setState(() {
                  parentDirectoryId = null;
                  directoryPathList = null;
                });
                if (findRequestFile.containsKey("tfTestDirectoryId")) {
                  findRequestFile.remove("tfTestDirectoryId");
                }
                findRequestFile["tfTestDirectoryIdIsNull"] = true;
                await getListTfFile(currentPageFile, findRequestFile);
              },
              icon: const FaIcon(FontAwesomeIcons.house, size: 16),
              color: Colors.grey,
              splashRadius: 10,
              tooltip: multiLanguageString(name: "root", defaultValue: "Root", context: context),
            ),
            for (var directoryPath in directoryPathList!)
              RichText(
                  text: TextSpan(children: [
                const TextSpan(
                  text: " / ",
                  style: TextStyle(color: Colors.grey),
                ),
                TextSpan(
                    text: "${directoryPath["title"]}",
                    style: TextStyle(
                        color: directoryPathList!.last == directoryPath ? Colors.blue : Colors.grey,
                        fontSize: context.fontSizeManager.fontSizeText16,
                        fontWeight: FontWeight.bold),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () async {
                        setState(() {
                          parentDirectoryId = directoryPath["id"];
                        });
                        getDirectoryPathList();
                        if (findRequestFile.containsKey("tfTestDirectoryIdIsNull")) {
                          findRequestFile.remove("tfTestDirectoryIdIsNull");
                        }
                        findRequestFile["tfTestDirectoryId"] = parentDirectoryId;
                        await getListTfFile(currentPageFile, findRequestFile);
                      }),
              ]))
          ],
        ),
        addTestWidget(context)
      ],
    );
  }

  Padding headerWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
      child: HeaderListFileWidget(
        type: isViewTypeDirectory ? "Directory" : "File",
        title: multiLanguageString(name: "file", defaultValue: "File", context: context),
        countList: rowCountListFile,
        isDirectory: true,
        onChangedDirectory: () async {
          onLoading(context);
          setState(() {
            isViewTypeDirectory = !isViewTypeDirectory;
          });
          if (isViewTypeDirectory) {
            if (parentDirectoryId != null) {
              findRequestFile["tfTestDirectoryId"] = parentDirectoryId;
            } else {
              if (findRequestFile.containsKey("tfTestDirectoryId")) {
                findRequestFile.remove("tfTestDirectoryId");
              }
              findRequestFile["tfTestDirectoryIdIsNull"] = true;
            }
          } else {
            if (findRequestFile.containsKey("tfTestDirectoryIdIsNull")) {
              findRequestFile.remove("tfTestDirectoryIdIsNull");
            }
            if (findRequestFile.containsKey("tfTestDirectoryId")) {
              findRequestFile.remove("tfTestDirectoryId");
            }
          }
          await getListTfFile(currentPageFile, findRequestFile);
          if (mounted) {
            Navigator.pop(this.context);
          }
        },
        callbackFilter: (result) {
          findRequestDirectory.addAll({"titleLike": result["titleLike"]});
          var newMap = {};
          result.forEach((key, value) {
            if (value != null && (value is int || value.isNotEmpty)) newMap.addAll({key: value});
          });
          findRequestFile = newMap;
          if (newMap.isEmpty) {
            findRequestFile["tfTestDirectoryIdIsNull"] = true;
          }
          getListTfDirectory(findRequestDirectory);
          getListTfFile(currentPageFile, findRequestFile);
        },
      ),
    );
  }

  dialogDeleteFile(selectedItem) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogDelete(
          selectedItem: selectedItem,
          callback: () {
            handleDeleteFile(selectedItem["id"]);
          },
          type: 'file',
        );
      },
    );
  }
}
