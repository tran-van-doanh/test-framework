import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import '../../../../api.dart';
import '../../../../common/custom_state.dart';
import '../../../../common/dynamic_dropdown_button.dart';

class HeaderListFileWidget extends StatefulWidget {
  final String type;
  final int? countList;
  final String title;
  final Function? onChangedDirectory;
  final bool? isDirectory;
  final Function? callbackFilter;

  const HeaderListFileWidget({
    Key? key,
    required this.type,
    this.countList,
    this.callbackFilter,
    required this.title,
    this.onChangedDirectory,
    this.isDirectory,
  }) : super(key: key);

  @override
  State<HeaderListFileWidget> createState() => _HeaderListFileWidgetState();
}

class _HeaderListFileWidgetState extends CustomState<HeaderListFileWidget> {
  final TextEditingController _nameController = TextEditingController();
  SingleValueDropDownController statusController = SingleValueDropDownController();
  SingleValueDropDownController testOwnerController = SingleValueDropDownController();

  var prjProjectId = "";
  var _listTestOwner = [];
  bool isShowFilterForm = true;
  bool isSearched = false;
  getListOwner() async {
    var requestBody = {"prjProjectId": prjProjectId};
    var response = await httpPost("/test-framework-api/user/sys/sys-user-project/search", requestBody, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      _listTestOwner = response["body"]["resultList"];
    }
  }

  @override
  void initState() {
    prjProjectId = Provider.of<NavigationModel>(context, listen: false).prjProjectId ?? "";
    getInfoFilter();
    super.initState();
  }

  getInfoFilter() {
    getListOwner();
  }

  handleCallBackSearchFunction({String type = "search"}) {
    var result = {};
    if (widget.type == "Directory" || widget.type == "File") {
      result = {
        "status": statusController.dropDownValue?.value,
        "sysUserId": testOwnerController.dropDownValue?.value,
        "titleLike": _nameController.text,
      };
    }
    widget.callbackFilter!(result);
  }

  @override
  void dispose() {
    _nameController.dispose();
    statusController.dispose();
    testOwnerController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      List<Widget> widgetList = [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
            child: SizedBox(
              height: 50,
              child: DynamicTextField(
                controller: _nameController,
                onComplete: () => handleCallBackSearchFunction(),
                onChanged: (value) {
                  setState(() {
                    _nameController.text;
                  });
                },
                labelText: multiLanguageString(name: "search_test_name", defaultValue: "Search test name", context: context),
                hintText: multiLanguageString(name: "by_name", defaultValue: "By name", context: context),
                suffixIcon: (_nameController.text != "")
                    ? IconButton(
                        onPressed: () {
                          setState(() {
                            _nameController.clear();
                            if (isSearched) {
                              handleCallBackSearchFunction();
                            }
                          });
                        },
                        icon: const Icon(Icons.clear),
                        splashRadius: 20,
                      )
                    : null,
              ),
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
            child: DynamicDropdownSearchClearOption(
              labelText: multiLanguageString(name: "search_status", defaultValue: "Search status", context: context),
              controller: statusController,
              hint: multiLanguageString(name: "search_status", defaultValue: "Search status", context: context),
              items: {
                multiLanguageString(name: "inactive", defaultValue: "Inactive", context: context): 0,
                multiLanguageString(name: "active", defaultValue: "Active", context: context): 1
              }.entries.map<DropDownValueModel>((result) {
                return DropDownValueModel(
                  value: result.value,
                  name: result.key,
                );
              }).toList(),
              maxHeight: 200,
              onChanged: (dynamic newValue) {
                if (isSearched && newValue == "") {
                  handleCallBackSearchFunction();
                }
                // if (newValue == "") {
                //   handleCallBackSearchFunction();
                // }
              },
            ),
          ),
        ),
        if (widget.type != "Delete Test Batch Runs")
          Expanded(
            child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                child: DynamicDropdownSearchClearOption(
                  labelText: multiLanguageString(name: "test_owner", defaultValue: "Test owner", context: context),
                  controller: testOwnerController,
                  hint: multiLanguageString(name: "search_test_owner", defaultValue: "Search Test owner", context: context),
                  items: _listTestOwner.map<DropDownValueModel>((dynamic result) {
                    return DropDownValueModel(
                      value: result["sysUser"]["id"],
                      name: result["sysUser"]["fullname"],
                    );
                  }).toList(),
                  maxHeight: 200,
                  onChanged: (dynamic newValue) {
                    if (isSearched && newValue == "") {
                      handleCallBackSearchFunction();
                    }
                    // if (newValue == "") {
                    //   handleCallBackSearchFunction();
                    // }
                  },
                )),
          ),
      ];
      var itemPerRow = 4;
      if (constraints.maxWidth < 600) {
        itemPerRow = 1;
      } else if (constraints.maxWidth < 800) {
        itemPerRow = 2;
      } else if (constraints.maxWidth < 1200) {
        itemPerRow = 3;
      }
      var rowCount = (1.0 * widgetList.length / itemPerRow).ceil();
      var filterWidget = Column(
        children: [
          for (var rowI = 0; rowI < rowCount; rowI++)
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                for (var colI = 0; colI < itemPerRow; colI++)
                  rowI * itemPerRow + colI < widgetList.length ? widgetList[rowI * itemPerRow + colI] : Expanded(child: Container())
              ],
            )
        ],
      );
      return Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              (widget.type == "File")
                  ? MultiLanguageText(
                      name: "type_with_count",
                      defaultValue: "\$0 (\$1)",
                      variables: [widget.title, "${widget.countList}"],
                      style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500))
                  : MultiLanguageText(
                      name: "type_without_count",
                      defaultValue: "\$0",
                      variables: [widget.title],
                      style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500)),
              if (widget.isDirectory == true)
                SizedBox(
                  height: 40,
                  child: FloatingActionButton(
                    heroTag: null,
                    onPressed: () async {
                      widget.onChangedDirectory!();
                    },
                    tooltip: multiLanguageString(name: "directory", defaultValue: "Directory", context: context),
                    backgroundColor: widget.type == "Directory" ? const Color.fromARGB(176, 125, 141, 231) : const Color.fromRGBO(216, 218, 229, .7),
                    elevation: 0,
                    hoverElevation: 0,
                    child: const Icon(Icons.folder, color: Color.fromRGBO(49, 49, 49, 1)),
                  ),
                )
            ],
          ),
          Row(
            children: [
              MultiLanguageText(
                name: "filter_form",
                defaultValue: "Filter form",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: const Color.fromRGBO(0, 0, 0, 0.8),
                    fontSize: context.fontSizeManager.fontSizeText16,
                    letterSpacing: 2),
              ),
              IconButton(
                onPressed: () {
                  setState(() {
                    isShowFilterForm = !isShowFilterForm;
                  });
                },
                icon: isShowFilterForm ? const Icon(Icons.visibility) : const Icon(Icons.visibility_off),
                splashRadius: 1,
                tooltip: isShowFilterForm
                    ? multiLanguageString(name: "hide_filter_form", defaultValue: "Hide filter form", context: context)
                    : multiLanguageString(name: "show_filter_form", defaultValue: "Show filter form", context: context),
              )
            ],
          ),
          if (isShowFilterForm) filterWidget,
          if (isShowFilterForm)
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                ElevatedButton(
                    onPressed: () {
                      isSearched = true;
                      handleCallBackSearchFunction();
                    },
                    child: const MultiLanguageText(name: "search", defaultValue: "Search", isLowerCase: false)),
                if (widgetList.length > 2)
                  Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: ElevatedButton(
                        onPressed: () {
                          setState(() {
                            _nameController.clear();
                            testOwnerController.clearDropDown();
                            statusController.clearDropDown();
                            widget.callbackFilter!({});
                          });
                        },
                        child: const MultiLanguageText(name: "reset", defaultValue: "Reset", isLowerCase: false)),
                  ),
              ],
            )
        ],
      );
    });
  }
}
