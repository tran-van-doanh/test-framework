import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/common/row_code_with_tooltip.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';

import '../../../../api.dart';
import '../../../../common/custom_state.dart';
import '../../../../common/dynamic_dropdown_button.dart';

class OptionChartWidget extends StatefulWidget {
  final String type;
  final String title;
  final String? id;
  final Function? callbackOptionTest;
  final String? reportType;
  const OptionChartWidget({Key? key, required this.type, this.id, this.callbackOptionTest, required this.title, this.reportType}) : super(key: key);

  @override
  State<OptionChartWidget> createState() => _OptionChartWidgetState();
}

class _OptionChartWidgetState extends CustomState<OptionChartWidget> {
  late Future future;
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _desController = TextEditingController();
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _reportDisplayConfigController = TextEditingController();
  final TextEditingController _reportQueryConfigController = TextEditingController();
  int _status = 0;
  String? reportType;
  final _formKey = GlobalKey<FormState>();
  dynamic selectedItem;
  @override
  void initState() {
    future = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    if (widget.id != null) {
      await getSelectedItem(widget.id!);
    }
    await setInitValue();
    return 0;
  }

  getSelectedItem(String id) async {
    var response = await httpGet("/test-framework-api/admin/report/rpt-report/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        selectedItem = response["body"]["result"];
      });
    }
  }

  setInitValue() {
    setState(() {
      if (selectedItem != null) {
        reportType = selectedItem["reportType"];
        _status = selectedItem["status"] ?? 0;
        _desController.text = selectedItem["description"] ?? "";
        _titleController.text = selectedItem["title"] ?? "";
        _nameController.text = selectedItem["name"] ?? "";
        _reportDisplayConfigController.text = jsonEncode(selectedItem["reportDisplayConfig"] ?? {});
        _reportQueryConfigController.text = jsonEncode(selectedItem["reportQueryConfig"] ?? {});
      } else if (widget.reportType != null) {
        reportType = widget.reportType;
      }
    });
  }

  @override
  void dispose() {
    _nameController.dispose();
    _desController.dispose();
    _titleController.dispose();
    _reportDisplayConfigController.dispose();
    _reportQueryConfigController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, LanguageModel>(
      builder: (context, navigationModel, languageModel, child) {
        return FutureBuilder(
          future: future,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return TestFrameworkRootPageWidget(
                child: Column(
                  children: [
                    headerWidget(context),
                    const Divider(color: Color.fromRGBO(216, 218, 229, 1)),
                    Expanded(
                      child: Container(
                        margin: const EdgeInsets.only(top: 20, bottom: 10),
                        child: Form(
                          key: _formKey,
                          child: SingleChildScrollView(
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                              child: FocusTraversalGroup(
                                policy: OrderedTraversalPolicy(),
                                child: bodyWidget(context),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    btnWidget()
                  ],
                ),
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  Padding btnWidget() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20, top: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ButtonSave(onPressed: () {
            if (_formKey.currentState!.validate()) {
              var result = {};
              if (selectedItem != null) {
                result = selectedItem;
                result["name"] = _nameController.text;
                result["title"] = _titleController.text;
                result["description"] = _desController.text;
                result["status"] = _status;
                result["reportType"] = reportType;
                result["reportDisplayConfig"] = jsonDecode(_reportDisplayConfigController.text.isEmpty ? "{}" : _reportDisplayConfigController.text);
                result["reportQueryConfig"] = jsonDecode(_reportQueryConfigController.text.isEmpty ? "{}" : _reportQueryConfigController.text);
              } else {
                result = {
                  "title": _titleController.text,
                  "name": _nameController.text,
                  "description": _desController.text,
                  "status": _status,
                  "reportType": reportType,
                  "reportDisplayConfig": jsonDecode(_reportDisplayConfigController.text.isEmpty ? "{}" : _reportDisplayConfigController.text),
                  "reportQueryConfig": jsonDecode(_reportQueryConfigController.text.isEmpty ? "{}" : _reportQueryConfigController.text),
                };
              }
              widget.callbackOptionTest!(result);
            }
          }),
          const ButtonCancel()
        ],
      ),
    );
  }

  Column bodyWidget(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "dashboard_type", defaultValue: "Chart Type", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                    borderRadius: 5,
                    value: reportType,
                    hint: multiLanguageString(name: "choose_a_type", defaultValue: "Choose a type", context: context),
                    items: [
                      {"name": multiLanguageString(name: "data_sheet", defaultValue: "Data Sheet", context: context), "value": "dataSheet"},
                      {"name": multiLanguageString(name: "chart", defaultValue: "Chart", context: context), "value": "chart"},
                      {"name": multiLanguageString(name: "dataTable", defaultValue: "Data Table", context: context), "value": "dataTable"},
                      {"name": multiLanguageString(name: "data", defaultValue: "Data", context: context), "value": "data"},
                    ].map<DropdownMenuItem<String>>((dynamic result) {
                      return DropdownMenuItem(
                        value: result["value"],
                        child: Text(result["name"]),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        reportType = newValue!;
                      });
                    },
                    isRequiredNotEmpty: true),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Expanded(
                flex: 3,
                child: RowCodeWithTooltipWidget(),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  enabled: widget.type != "edit",
                  controller: _nameController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                  isRequiredRegex: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "name", defaultValue: "Name", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _titleController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "status", defaultValue: "Status", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                    borderRadius: 5,
                    value: _status,
                    hint: multiLanguageString(name: "choose_a_status", defaultValue: "Choose a status", context: context),
                    items: [
                      {"name": multiLanguageString(name: "active", defaultValue: "Active", context: context), "value": 1},
                      {"name": multiLanguageString(name: "inactive", defaultValue: "Inactive", context: context), "value": 0},
                    ].map<DropdownMenuItem<int>>((dynamic result) {
                      return DropdownMenuItem(
                        value: result["value"],
                        child: Text(result["name"]),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        _status = newValue!;
                      });
                    },
                    isRequiredNotEmpty: true),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "description", defaultValue: "Description", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _desController,
                  hintText: "...",
                  maxline: 3,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "report_display_config", defaultValue: "ReportDisplay Config", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _reportDisplayConfigController,
                  hintText: "...",
                  maxline: 10,
                  minline: 3,
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: Row(
                  children: [
                    MultiLanguageText(name: "report_query_config", defaultValue: "Report Query Config", style: Style(context).styleTitleDialog),
                  ],
                ),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _reportQueryConfigController,
                  hintText: "...",
                  maxline: 10,
                  minline: 3,
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Padding headerWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 35, 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "type_report",
            defaultValue: "\$0 report",
            variables: [widget.title],
            isLowerCase: false,
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
            onPressed: () => Navigator.pop(context, 'cancel'),
            child: const Icon(
              Icons.close,
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }
}
