import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/components/dashboard/dynamic_report.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/insights/report/option_chart_widget.dart';
import 'package:test_framework_view/page/insights/report/report_item_widget.dart';
import 'package:test_framework_view/type.dart';

class DynamicReportPageWidget extends StatefulWidget {
  final String? reportPageType;
  const DynamicReportPageWidget({Key? key, this.reportPageType}) : super(key: key);

  @override
  State<DynamicReportPageWidget> createState() => _DynamicReportPageWidgetState();
}

class _DynamicReportPageWidgetState extends State<DynamicReportPageWidget> {
  List reportList = [];
  String? reportSelectedId;
  String reportType = "dataSheet";
  @override
  void initState() {
    super.initState();
    getInfoPage();
  }

  getInfoPage() async {
    var findRequest = {if (widget.reportPageType != null) "status": 1, "reportType": reportType};
    var response = await httpPost("/test-framework-api/user/report/rpt-report/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        reportList = response["body"]["resultList"];
        if (reportList.isNotEmpty) {
          reportSelectedId = reportList[0]["id"];
        }
      });
    }
  }

  handleAddReport(result) async {
    var response = await httpPut("/test-framework-api/admin/report/rpt-report", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        getInfoPage();
      }
    }
  }

  handleEditReport(result, id) async {
    var response = await httpPost("/test-framework-api/admin/report/rpt-report/$id", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage") && mounted) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "edit_successful", defaultValue: "Edit successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        getInfoPage();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return Column(
        children: [
          Expanded(
            child: ListView(
              padding: const EdgeInsets.fromLTRB(50, 20, 50, 0),
              children: [
                (widget.reportPageType == "qa_platform")
                    ? NavMenuTabV1(
                        currentUrl: "/insights/reports",
                        navigationList: context.insightsMenuList,
                        margin: const EdgeInsets.only(bottom: 20),
                      )
                    : const SizedBox.shrink(),
                Padding(
                  padding: const EdgeInsets.only(bottom: 30),
                  child: MultiLanguageText(
                    name: widget.reportPageType == null ? "chart" : "report",
                    defaultValue: widget.reportPageType == null ? "Chart" : "Report",
                    style: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText30,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      constraints: const BoxConstraints(maxWidth: 350, maxHeight: 560),
                      child: Column(
                        children: [
                          DynamicDropdownButton(
                            value: reportType,
                            hint: multiLanguageString(name: "choose_a_report_type", defaultValue: "Choose a report type", context: context),
                            items: [
                              if (widget.reportPageType == null)
                                {"name": multiLanguageString(name: "all", defaultValue: "All", context: context), "value": ""},
                              {"name": multiLanguageString(name: "data_sheet", defaultValue: "Data Sheet", context: context), "value": "dataSheet"},
                              {"name": multiLanguageString(name: "data_table", defaultValue: "Data Table", context: context), "value": "dataTable"},
                              {"name": multiLanguageString(name: "chart", defaultValue: "Chart", context: context), "value": "chart"},
                              if (widget.reportPageType == null)
                                {"name": multiLanguageString(name: "data", defaultValue: "Data", context: context), "value": "data"},
                            ].map<DropdownMenuItem<String>>((dynamic result) {
                              return DropdownMenuItem(
                                value: result["value"],
                                child: Text(result["name"]),
                              );
                            }).toList(),
                            onChanged: (newValue) {
                              setState(() {
                                reportType = newValue!;
                              });
                              getInfoPage();
                            },
                            borderRadius: 5,
                          ),
                          Expanded(
                            child: Container(
                              color: Colors.grey[100],
                              margin: const EdgeInsets.only(top: 10),
                              child: ListView(
                                padding: const EdgeInsets.only(
                                  left: 13,
                                ),
                                children: [
                                  for (var report in reportList)
                                    Container(
                                      margin: const EdgeInsets.symmetric(vertical: 5),
                                      child: ReportItemWidget(
                                        key: Key(report["id"]),
                                        reportPageType: widget.reportPageType,
                                        report: report,
                                        reportSelectedId: reportSelectedId,
                                        onEditReport: (result) {
                                          handleEditReport(result, result["id"]);
                                        },
                                        onClickReport: (id) async {
                                          setState(() {
                                            reportSelectedId = id;
                                          });
                                        },
                                      ),
                                    ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    if (reportSelectedId != null)
                      Expanded(
                        child: DynamicReportWidget(
                            key: Key(reportSelectedId!),
                            requestBody: const {},
                            parameters: const {},
                            reportSelectedId: reportSelectedId!,
                            isShowDownload: true),
                      ),
                  ],
                )
              ],
            ),
          ),
          if (widget.reportPageType == null)
            Container(
              alignment: Alignment.centerRight,
              margin: const EdgeInsets.fromLTRB(50, 20, 50, 20),
              height: 40,
              child: FloatingActionButton.extended(
                heroTag: null,
                onPressed: () {
                  Navigator.of(context).push(
                    createRoute(
                      OptionChartWidget(
                        type: "create",
                        reportType: reportType,
                        title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                        callbackOptionTest: (result) {
                          handleAddReport(result);
                        },
                      ),
                    ),
                  );
                },
                icon: const Icon(Icons.note_add, color: Color.fromRGBO(49, 49, 49, 1)),
                label: MultiLanguageText(
                  name: "new_chart",
                  defaultValue: "New chart",
                  isLowerCase: false,
                  style: TextStyle(
                      fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
                ),
                backgroundColor: const Color.fromARGB(175, 8, 196, 80),
                elevation: 0,
                hoverElevation: 0,
              ),
            ),
        ],
      );
    });
  }
}
