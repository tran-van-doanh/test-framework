import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/insights/report/option_chart_widget.dart';
import 'package:test_framework_view/page/insights/rp_query_config/rp_query_config.dart';
import 'package:test_framework_view/type.dart';

class ReportItemWidget extends StatefulWidget {
  final dynamic report;
  final String? reportSelectedId;
  final String? reportPageType;
  final Function(String id) onClickReport;
  final Function(dynamic result) onEditReport;
  const ReportItemWidget({
    Key? key,
    this.report,
    required this.onClickReport,
    required this.reportSelectedId,
    required this.onEditReport,
    this.reportPageType,
  }) : super(key: key);

  @override
  State<ReportItemWidget> createState() => _ReportItemWidgetState();
}

class _ReportItemWidgetState extends State<ReportItemWidget> {
  var reportItem = {};

  @override
  initState() {
    super.initState();
    getInitPage();
  }

  getInitPage() {
    setState(() {
      reportItem = widget.report;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (reportItem.isNotEmpty) {
      return Consumer2<NavigationModel, LanguageModel>(
        builder: (context, navigationModel, languageModel, child) {
          return GestureDetector(
            onTap: () => widget.onClickReport(reportItem["id"]),
            child: ConstrainedBox(
              constraints: const BoxConstraints(minHeight: 45),
              child: Row(
                children: [
                  Expanded(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Icon(
                          Icons.bar_chart,
                          color: Color.fromRGBO(61, 148, 254, 1),
                          size: 20,
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Expanded(
                          child: Text(
                            "${reportItem["title"] ?? ""} ",
                            maxLines: 3,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                fontSize: context.fontSizeManager.fontSizeText16,
                                color: widget.reportSelectedId == reportItem["id"] ? Colors.blue : const Color.fromRGBO(49, 49, 49, 1),
                                decoration: TextDecoration.none,
                                fontWeight: widget.reportSelectedId == reportItem["id"] ? FontWeight.bold : null),
                          ),
                        ),
                      ],
                    ),
                  ),
                  if (widget.reportPageType == null)
                    PopupMenuButton(
                      offset: const Offset(2, 35),
                      tooltip: '',
                      splashRadius: 10,
                      icon: const Icon(
                        Icons.more_vert,
                      ),
                      itemBuilder: (context) => [
                        PopupMenuItem(
                          padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                          child: ListTile(
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.of(context).push(
                                createRoute(
                                  OptionChartWidget(
                                    id: reportItem["id"],
                                    type: "edit",
                                    title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                    callbackOptionTest: (result) {
                                      widget.onEditReport(result);
                                    },
                                  ),
                                ),
                              );
                            },
                            contentPadding: const EdgeInsets.all(0),
                            hoverColor: Colors.transparent,
                            title: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                            leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
                          ),
                        ),
                        PopupMenuItem(
                          padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                          child: ListTile(
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.of(context).push(
                                createRoute(
                                  ReportQueryConfigWidget(
                                    id: reportItem["id"],
                                    callbackReportQuery: (reportQueryConfig) {
                                      widget.report["reportQueryConfig"] = reportQueryConfig;
                                      widget.onEditReport(widget.report);
                                    },
                                  ),
                                ),
                              );
                            },
                            contentPadding: const EdgeInsets.all(0),
                            hoverColor: Colors.transparent,
                            title: const MultiLanguageText(name: "edit_report_query", defaultValue: "Edit report query"),
                            leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
                          ),
                        ),
                      ],
                    ),
                ],
              ),
            ),
          );
        },
      );
    }
    return const SizedBox.shrink();
  }
}
