import 'package:flutter/material.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/page/insights/rp_query_config/qr_value_edit.dart';

class QueryTableColumnOrValueEditWidget extends StatefulWidget {
  final dynamic queryColumn;
  final List<Map<String, String>> alias;
  final int check;
  final int level;
  final String type;
  final Function callback;
  const QueryTableColumnOrValueEditWidget(
      {Key? key,
      required this.queryColumn,
      required this.alias,
      required this.check,
      required this.level,
      required this.type,
      required this.callback})
      : super(key: key);

  @override
  State<QueryTableColumnOrValueEditWidget> createState() => _QueryTableColumnOrValueEditWidgetState();
}

class _QueryTableColumnOrValueEditWidgetState extends State<QueryTableColumnOrValueEditWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
                child: Padding(
                    padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                    child: TextField(
                      controller: TextEditingController(text: widget.queryColumn["format"] ?? ""),
                      onChanged: (value) {
                        widget.queryColumn["format"] = value;
                      },
                      decoration: InputDecoration(
                        label: const MultiLanguageText(name: "format", defaultValue: "Format"),
                        hintText: multiLanguageString(name: "format", defaultValue: "Format", context: context),
                        border: const OutlineInputBorder(),
                      ),
                    )))
          ],
        ),
        Row(
          children: [
            Expanded(
                child: Padding(
                    padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                    child: DynamicDropdownButton(
                        borderRadius: 5,
                        value: (widget.queryColumn["columnType"].toString() != "null") ? widget.queryColumn["columnType"].toString() : null,
                        labelText: "columnType",
                        items: ["Column", "Value"].map<DropdownMenuItem<String>>((dynamic result) {
                          return DropdownMenuItem(
                            value: result,
                            child: Text(result),
                          );
                        }).toList(),
                        onChanged: (newValue) async {
                          String? columnName;
                          String value = "";
                          String? isList;
                          bool isNull = false;
                          String? valueType;
                          String? ignoreNull;
                          setState(() {
                            if (widget.queryColumn["columnType"] != newValue) {
                              setState(() {
                                widget.queryColumn["columnType"] = newValue;
                                if (widget.queryColumn["columnType"] == "Column") {
                                  widget.queryColumn.remove("queryValue");
                                  widget.queryColumn["queryTableColumn"] = {"columnName": columnName, "queryTable": {}};
                                }
                                if (widget.queryColumn["columnType"] == "Value") {
                                  widget.queryColumn.remove("queryTableColumn");
                                  widget.queryColumn["queryValue"] = {
                                    "value": value,
                                    "isList": isList,
                                    "isNull": isNull,
                                    "valueType": valueType,
                                    "ignoreNull": ignoreNull,
                                  };
                                }
                              });
                            }
                          });
                        },
                        isRequiredNotEmpty: true)))
          ],
        ),
        Row(
          children: [
            Expanded(
                child: Padding(
                    padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                    child: TextField(
                      controller: TextEditingController(text: widget.queryColumn["columnAlias"] ?? ""),
                      onChanged: (value) {
                        widget.queryColumn["columnAlias"] = value;
                      },
                      decoration: InputDecoration(
                        label: const MultiLanguageText(name: "column_alias", defaultValue: "Column alias"),
                        hintText: multiLanguageString(name: "column_alias", defaultValue: "Column alias", context: context),
                        border: const OutlineInputBorder(),
                      ),
                    )))
          ],
        ),
        if (widget.queryColumn["columnType"] == "Column")
          Padding(
            padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  "Query table column",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                ),
                const SizedBox(height: 10),
                QueryTableColumn(
                    queryTableColumn: widget.queryColumn["queryTableColumn"], alias: widget.alias, check: widget.check, level: widget.level),
              ],
            ),
          ),
        if (widget.queryColumn["columnType"] == "Value")
          Padding(
            padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  "Query value",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                ),
                const SizedBox(height: 10),
                QueryValueColumn(query: widget.queryColumn),
              ],
            ),
          ),
        if (widget.type == "queryColumnElse" || widget.type == "queryColumnThen" || widget.type == "querySumColumn")
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                onPressed: () {
                  setState(() {
                    widget.callback(widget.queryColumn);
                    Navigator.pop(context);
                  });
                },
                child: const MultiLanguageText(name: "close", defaultValue: "Close"),
              )
            ],
          ),
      ],
    );
  }
}
