import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';
import 'package:test_framework_view/page/insights/rp_query_config/qr_column_list_edit.dart';
import 'package:test_framework_view/page/insights/rp_query_config/qr_condition_list_edit.dart';
import 'package:test_framework_view/page/insights/rp_query_config/qr_from_list_edit.dart';
import 'package:test_framework_view/page/insights/rp_query_config/qr_group_by_edit.dart';
import 'package:test_framework_view/page/insights/rp_query_config/qr_order_by_edit.dart';
import 'package:test_framework_view/page/insights/rp_query_config/qr_table_column_edit.dart';

class ReportQueryConfigWidget extends StatefulWidget {
  final String? id;
  final Function callbackReportQuery;
  const ReportQueryConfigWidget({Key? key, this.id, required this.callbackReportQuery}) : super(key: key);

  @override
  State<ReportQueryConfigWidget> createState() => _ReportQueryConfigWidgetState();
}

class _ReportQueryConfigWidgetState extends State<ReportQueryConfigWidget> {
  late Future future;
  dynamic selectedItem;
  var reportQueryConfig = {};

  @override
  void initState() {
    future = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    if (widget.id != null) {
      await getSelectedItem(widget.id!);
      await setInitValue();
    }
    return 0;
  }

  getSelectedItem(String id) async {
    var response = await httpGet("/test-framework-api/admin/report/rpt-report/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        selectedItem = response["body"]["result"];
      });
    }
  }

  setInitValue() {
    setState(() {
      if (selectedItem != null) {
        reportQueryConfig = selectedItem["reportQueryConfig"] ?? {};
      }
    });
  }

  List<Map<String, String>> createAliasList1() {
    Set<String> uniqueAliasSet = {};
    List<Map<String, String>> aliasList = [];

    for (var queryFrom in reportQueryConfig["queryFromList"] ?? []) {
      if (queryFrom.containsKey("queryTable") && queryFrom["queryTable"] != null) {
        if (!uniqueAliasSet.contains(queryFrom["queryTable"]["alias"])) {
          aliasList.add({
            "alias": queryFrom["queryTable"]["alias"],
            "tableName": queryFrom["queryTable"]["tableName"],
          });
          uniqueAliasSet.add(queryFrom["queryTable"]["alias"]);
        }
      }
      if (queryFrom.containsKey("queryJoin") && queryFrom["queryJoin"] != null) {
        if (queryFrom["queryJoin"]["queryTable2"] != null) {
          if (!uniqueAliasSet.contains(queryFrom["queryJoin"]["queryTable2"]["alias"])) {
            aliasList.add({
              "alias": queryFrom["queryJoin"]["queryTable2"]["alias"],
              "tableName": queryFrom["queryJoin"]["queryTable2"]["tableName"],
            });
            uniqueAliasSet.add(queryFrom["queryJoin"]["queryTable2"]["alias"]);
          }
        } else {
          if (!uniqueAliasSet.contains(queryFrom["queryJoin"]["queryTable"]["alias"])) {
            aliasList.add({
              "alias": queryFrom["queryJoin"]["queryTable"]["alias"],
              "tableName": queryFrom["queryJoin"]["queryTable"]["tableName"],
            });
            uniqueAliasSet.add(queryFrom["queryJoin"]["queryTable"]["alias"]);
          }
        }
      }
    }
    return aliasList;
  }

  List<Map<String, String>> createAliasList2() {
    Set<String> uniqueAliasSet = {};
    List<Map<String, String>> aliasList = [];

    for (var queryFromList in reportQueryConfig["queryFromList"] ?? []) {
      if (queryFromList.containsKey("queryTable") && queryFromList["queryTable"] != null) {
        if (queryFromList["queryTable"].containsKey("querySelect") && queryFromList["queryTable"]["querySelect"] != null) {
          if (queryFromList["queryTable"]["querySelect"].containsKey("queryFromList") &&
              queryFromList["queryTable"]["querySelect"]["queryFromList"].isNotEmpty) {
            for (var queryFrom in queryFromList["queryTable"]["querySelect"]["queryFromList"] ?? []) {
              if (queryFrom.containsKey("queryTable") && queryFrom["queryTable"] != null) {
                if (!uniqueAliasSet.contains(queryFrom["queryTable"]["alias"])) {
                  aliasList.add({
                    "alias": queryFrom["queryTable"]["alias"],
                    "tableName": queryFrom["queryTable"]["tableName"],
                  });
                  uniqueAliasSet.add(queryFrom["queryTable"]["alias"]);
                }
              }
              if (queryFrom.containsKey("queryJoin") && queryFrom["queryJoin"] != null) {
                if (queryFrom["queryJoin"]["queryTable2"] != null) {
                  if (!uniqueAliasSet.contains(queryFrom["queryJoin"]["queryTable2"]["alias"])) {
                    aliasList.add({
                      "alias": queryFrom["queryJoin"]["queryTable2"]["alias"],
                      "tableName": queryFrom["queryJoin"]["queryTable2"]["tableName"],
                    });
                    uniqueAliasSet.add(queryFrom["queryJoin"]["queryTable2"]["alias"]);
                  }
                } else {
                  if (!uniqueAliasSet.contains(queryFrom["queryJoin"]["queryTable"]["alias"])) {
                    aliasList.add({
                      "alias": queryFrom["queryJoin"]["queryTable"]["alias"],
                      "tableName": queryFrom["queryJoin"]["queryTable"]["tableName"],
                    });
                    uniqueAliasSet.add(queryFrom["queryJoin"]["queryTable"]["alias"]);
                  }
                }
              }
            }
          }
        }
      }
    }
    return aliasList;
  }

  List<Map<String, String>> createAliasList3() {
    Set<String> uniqueAliasSet = {};
    List<Map<String, String>> aliasList = [];

    for (var queryFromList in reportQueryConfig["queryFromList"] ?? []) {
      if (queryFromList.containsKey("queryTable") && queryFromList["queryTable"] != null) {
        if (queryFromList["queryTable"].containsKey("querySelect") && queryFromList["queryTable"]["querySelect"] != null) {
          if (queryFromList["queryTable"]["querySelect"].containsKey("queryFromList") &&
              queryFromList["queryTable"]["querySelect"]["queryFromList"].isNotEmpty) {
            for (var queryFromList2 in queryFromList["queryTable"]["querySelect"]["queryFromList"] ?? []) {
              if (queryFromList2["queryTable"].containsKey("querySelect") && queryFromList2["queryTable"]["querySelect"] != null) {
                for (var queryFrom in queryFromList2["queryTable"]["querySelect"]["queryFromList"] ?? []) {
                  if (queryFrom.containsKey("queryTable") && queryFrom["queryTable"] != null) {
                    if (!uniqueAliasSet.contains(queryFrom["queryTable"]["alias"])) {
                      aliasList.add({
                        "alias": queryFrom["queryTable"]["alias"],
                        "tableName": queryFrom["queryTable"]["tableName"],
                      });
                      uniqueAliasSet.add(queryFrom["queryTable"]["alias"]);
                    }
                  }
                  if (queryFrom.containsKey("queryJoin") && queryFrom["queryJoin"] != null) {
                    if (queryFrom["queryJoin"]["queryTable2"] != null) {
                      if (!uniqueAliasSet.contains(queryFrom["queryJoin"]["queryTable2"]["alias"])) {
                        aliasList.add({
                          "alias": queryFrom["queryJoin"]["queryTable2"]["alias"],
                          "tableName": queryFrom["queryJoin"]["queryTable2"]["tableName"],
                        });
                        uniqueAliasSet.add(queryFrom["queryJoin"]["queryTable2"]["alias"]);
                      }
                    } else {
                      if (!uniqueAliasSet.contains(queryFrom["queryJoin"]["queryTable"]["alias"])) {
                        aliasList.add({
                          "alias": queryFrom["queryJoin"]["queryTable"]["alias"],
                          "tableName": queryFrom["queryJoin"]["queryTable"]["tableName"],
                        });
                        uniqueAliasSet.add(queryFrom["queryJoin"]["queryTable"]["alias"]);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    return aliasList;
  }

  List<Map<String, String>> createAliasListOutSide2() {
    Set<String> uniqueAliasSet = {};
    List<Map<String, String>> aliasList = [];

    for (var queryFrom in reportQueryConfig["queryFromList"] ?? []) {
      if (queryFrom.containsKey("queryTable") && queryFrom["queryTable"] != null) {
        if (!uniqueAliasSet.contains(queryFrom["queryTable"]["alias"])) {
          aliasList.add({
            "alias": queryFrom["queryTable"]["alias"],
          });
          uniqueAliasSet.add(queryFrom["queryTable"]["alias"]);
        }
      }
    }
    return aliasList;
  }

  List<Map<String, String>> createAliasListOutSide3() {
    Set<String> uniqueAliasSet = {};
    List<Map<String, String>> aliasList = [];

    for (var queryFromList in reportQueryConfig["queryFromList"] ?? []) {
      if (queryFromList.containsKey("queryTable") && queryFromList["queryTable"] != null) {
        if (queryFromList["queryTable"].containsKey("querySelect") && queryFromList["queryTable"]["querySelect"] != null) {
          if (queryFromList["queryTable"]["querySelect"].containsKey("queryFromList") &&
              queryFromList["queryTable"]["querySelect"]["queryFromList"].isNotEmpty) {
            for (var queryFrom in queryFromList["queryTable"]["querySelect"]["queryFromList"] ?? []) {
              if (queryFrom.containsKey("queryTable") && queryFrom["queryTable"] != null) {
                if (!uniqueAliasSet.contains(queryFrom["queryTable"]["alias"])) {
                  aliasList.add({
                    "alias": queryFrom["queryTable"]["alias"],
                  });
                  uniqueAliasSet.add(queryFrom["queryTable"]["alias"]);
                }
              }
            }
          }
        }
      }
    }
    return aliasList;
  }

  void updateTableNameForAlias(alias, newTableName) {
    void updateTableAlias(table, alias, newTableName) {
      if (table != null && table["alias"] == alias) {
        table["tableName"] = newTableName;
      }
    }

    void processQueryFrom(queryFrom) {
      if (checkQuerySelect() == 1) {
        updateTableAlias(queryFrom["queryTable"], alias, newTableName);
        updateTableAlias(queryFrom["queryJoin"]?["queryTable"], alias, newTableName);

        var queryConditionList = queryFrom["queryJoin"]?["queryConditionGroup"]?["queryConditionList"];
        queryConditionList?.forEach((condition) {
          updateTableAlias(condition["queryColumn1"]?["queryTableColumn"]?["queryTable"], alias, newTableName);
          updateTableAlias(condition["queryColumn2"]?["queryTableColumn"]?["queryTable"], alias, newTableName);
        });
      }
      if (checkQuerySelect() == 2) {
        var queryFromList = queryFrom["queryTable"]?["querySelect"]?["queryFromList"];
        queryFromList?.forEach((queryFrom2) {
          updateTableAlias(queryFrom2["queryTable"], alias, newTableName);
          updateTableAlias(queryFrom2["queryJoin"]?["queryTable"], alias, newTableName);

          var queryConditionList = queryFrom2["queryJoin"]?["queryConditionGroup"]?["queryConditionList"];
          queryConditionList?.forEach((condition2) {
            updateTableAlias(condition2["queryColumn1"]?["queryTableColumn"]?["queryTable"], alias, newTableName);
            updateTableAlias(condition2["queryColumn2"]?["queryTableColumn"]?["queryTable"], alias, newTableName);
          });
        });

        var queryColumnList = queryFrom["queryTable"]?["querySelect"]?["queryColumnList"];
        queryColumnList?.forEach((queryColumn2) {
          if (queryColumn2["queryTableColumn"] != null) {
            updateTableAlias(queryColumn2["queryTableColumn"]["queryTable"], alias, newTableName);
          }
          if (queryColumn2["queryCase"] != null) {
            queryColumn2["queryCase"]["queryCaseWhenList"]?.forEach((queryCaseWhen) {
              queryCaseWhen["queryConditionGroup"]["queryConditionList"]?.forEach((condition3) {
                updateTableAlias(condition3["queryColumn1"]?["queryTableColumn"]?["queryTable"], alias, newTableName);
                updateTableAlias(condition3["queryColumn2"]?["queryTableColumn"]?["queryTable"], alias, newTableName);
              });
            });
          }
        });

        var queryGroupByList = queryFrom["queryTable"]?["querySelect"]?["queryGroupByList"];
        queryGroupByList?.forEach((queryGroupBy2) {
          updateTableAlias(queryGroupBy2["queryTableColumn"]["queryTable"], alias, newTableName);
        });
        var queryOrderByList = queryFrom["queryTable"]?["querySelect"]?["queryOrderByList"];
        queryOrderByList?.forEach((queryOrderBy2) {
          updateTableAlias(queryOrderBy2["queryColumn"]["queryTableColumn"]["queryTable"], alias, newTableName);
        });
        var queryConditionList = queryFrom["queryTable"]?["querySelect"]?["queryConditionGroup"]?["queryConditionList"];
        queryConditionList?.forEach((condition4) {
          updateTableAlias(condition4["queryColumn1"]?["queryTableColumn"]?["queryTable"], alias, newTableName);
          updateTableAlias(condition4["queryColumn2"]?["queryTableColumn"]?["queryTable"], alias, newTableName);
        });
      }
      if (checkQuerySelect() == 3) {
        var queryFromList = queryFrom["queryTable"]?["querySelect"]?["queryFromList"];
        queryFromList?.forEach((queryFrom2) {
          var queryFromList2 = queryFrom2["queryTable"]?["querySelect"]?["queryFromList"];
          queryFromList2?.forEach((queryFrom3) {
            updateTableAlias(queryFrom3["queryTable"], alias, newTableName);
            updateTableAlias(queryFrom3["queryJoin"]?["queryTable"], alias, newTableName);

            var queryConditionList = queryFrom3["queryJoin"]?["queryConditionGroup"]?["queryConditionList"];
            queryConditionList?.forEach((condition) {
              updateTableAlias(condition["queryColumn1"]?["queryTableColumn"]?["queryTable"], alias, newTableName);
              updateTableAlias(condition["queryColumn2"]?["queryTableColumn"]?["queryTable"], alias, newTableName);
            });
          });

          var queryColumnList2 = queryFrom2["queryTable"]?["querySelect"]?["queryColumnList"];
          queryColumnList2?.forEach((queryColumn) {
            if (queryColumn["queryTableColumn"] != null) {
              updateTableAlias(queryColumn["queryTableColumn"]["queryTable"], alias, newTableName);
            }
            if (queryColumn["queryCase"] != null) {
              queryColumn["queryCase"]["queryCaseWhenList"]?.forEach((queryCaseWhen) {
                queryCaseWhen["queryConditionGroup"]["queryConditionList"]?.forEach((condition2) {
                  updateTableAlias(condition2["queryColumn1"]?["queryTableColumn"]?["queryTable"], alias, newTableName);
                  updateTableAlias(condition2["queryColumn2"]?["queryTableColumn"]?["queryTable"], alias, newTableName);
                });
              });
            }
          });

          var queryGroupByList2 = queryFrom2["queryTable"]?["querySelect"]?["queryGroupByList"];
          queryGroupByList2?.forEach((queryGroupBy) {
            updateTableAlias(queryGroupBy["queryTableColumn"]["queryTable"], alias, newTableName);
          });
          var queryOrderByList2 = queryFrom2["queryTable"]?["querySelect"]?["queryOrderByList"];
          queryOrderByList2?.forEach((queryOrderBy) {
            updateTableAlias(queryOrderBy["queryColumn"]["queryTableColumn"]["queryTable"], alias, newTableName);
          });
          var queryConditionList2 = queryFrom2["queryTable"]?["querySelect"]?["queryConditionGroup"]?["queryConditionList"];
          queryConditionList2?.forEach((condition3) {
            updateTableAlias(condition3["queryColumn1"]?["queryTableColumn"]?["queryTable"], alias, newTableName);
            updateTableAlias(condition3["queryColumn2"]?["queryTableColumn"]?["queryTable"], alias, newTableName);
          });
        });
      }
    }

    void processQueryColumn(queryColumn) {
      if (queryColumn["queryTableColumn"] != null) {
        updateTableAlias(queryColumn["queryTableColumn"]["queryTable"], alias, newTableName);
      }
      if (queryColumn["queryCase"] != null) {
        queryColumn["queryCase"]["queryCaseWhenList"]?.forEach((queryCaseWhen) {
          queryCaseWhen["queryConditionGroup"]["queryConditionList"]?.forEach((condition) {
            updateTableAlias(condition["queryColumn1"]?["queryTableColumn"]?["queryTable"], alias, newTableName);
            updateTableAlias(condition["queryColumn2"]?["queryTableColumn"]?["queryTable"], alias, newTableName);
          });
        });
      }
    }

    void processQueryGroupBy(queryGroupBy) {
      updateTableAlias(queryGroupBy["queryTableColumn"]["queryTable"], alias, newTableName);
    }

    void processQueryOrderBy(queryOrderBy) {
      updateTableAlias(queryOrderBy["queryColumn"]["queryTableColumn"]["queryTable"], alias, newTableName);
    }

    void processCondition(condition) {
      updateTableAlias(condition["queryColumn1"]?["queryTableColumn"]?["queryTable"], alias, newTableName);
      updateTableAlias(condition["queryColumn2"]?["queryTableColumn"]?["queryTable"], alias, newTableName);
    }

    reportQueryConfig["queryFromList"]?.forEach(processQueryFrom);
    reportQueryConfig["queryColumnList"]?.forEach(processQueryColumn);
    reportQueryConfig["queryGroupByList"]?.forEach(processQueryGroupBy);
    reportQueryConfig["queryOrderByList"]?.forEach(processQueryOrderBy);
    reportQueryConfig["queryConditionGroup"]?["queryConditionList"]?.forEach(processCondition);
  }

  int checkQuerySelect() {
    if (reportQueryConfig["queryFromList"] != null) {
      for (var queryFrom in reportQueryConfig["queryFromList"] ?? []) {
        var queryTable = queryFrom["queryTable"];
        if (queryTable != null && queryTable.containsKey("querySelect")) {
          for (var queryFrom2 in queryTable["querySelect"]["queryFromList"] ?? []) {
            var queryTable2 = queryFrom2["queryTable"];
            if (queryTable2 != null && queryTable2.containsKey("querySelect")) {
              return 3;
            }
          }
          return 2;
        }
      }
    }
    return 1;
  }

  showEditDialog(data, type, level) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return SingleChildScrollView(
            child: Dialog(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                child: Row(mainAxisSize: MainAxisSize.max, children: [
                  Expanded(
                      child: Column(
                    children: [
                      if (type == "queryColumnList")
                        QueryColumnListEditWidget(
                          queryColumn: data,
                          alias: (checkQuerySelect() == 3 && level == 1)
                              ? createAliasListOutSide2()
                              : (checkQuerySelect() == 3 && level == 2)
                                  ? createAliasListOutSide3()
                                  : (checkQuerySelect() == 3 && level == 3)
                                      ? createAliasList3()
                                      : (checkQuerySelect() == 2 && level == 1)
                                          ? createAliasListOutSide2()
                                          : (checkQuerySelect() == 2 && level == 2)
                                              ? createAliasList2()
                                              : createAliasList1(),
                          level: level,
                          check: checkQuerySelect(),
                          callbackColumn: (newData) {
                            setState(() {
                              data = newData;
                            });
                          },
                        ),
                      if (type == "queryColumnElse" || type == "queryColumnThen")
                        QueryTableColumnOrValueEditWidget(
                          queryColumn: data,
                          alias: (checkQuerySelect() == 3 && level == 1)
                              ? createAliasListOutSide2()
                              : (checkQuerySelect() == 3 && level == 2)
                                  ? createAliasListOutSide3()
                                  : (checkQuerySelect() == 3 && level == 3)
                                      ? createAliasList3()
                                      : (checkQuerySelect() == 2 && level == 1)
                                          ? createAliasListOutSide2()
                                          : (checkQuerySelect() == 2 && level == 2)
                                              ? createAliasList2()
                                              : createAliasList1(),
                          level: level,
                          check: checkQuerySelect(),
                          type: type,
                          callback: (newData) {
                            setState(() {
                              data = newData;
                            });
                          },
                        ),
                      if (type == "queryFromList")
                        QueryFromListEditWidget(
                          queryFrom: data,
                          updateAlias: (newAlias, newTableName) {
                            setState(() {
                              updateTableNameForAlias(newAlias, newTableName);
                            });
                          },
                          onClose: (newData) {
                            setState(() {
                              data = newData;
                            });
                          },
                        ),
                      if (type == "fromAlias") fromAlias(data),
                      if (type == "groupType") groupTypeDialog(data),
                      if (type == "queryCondition")
                        QueryConditionListEditWidget(
                          queryCondition: data,
                          alias: (checkQuerySelect() == 3 && level == 1)
                              ? createAliasListOutSide2()
                              : (checkQuerySelect() == 3 && level == 2)
                                  ? createAliasListOutSide3()
                                  : (checkQuerySelect() == 3 && level == 3)
                                      ? createAliasList3()
                                      : (checkQuerySelect() == 2 && level == 1)
                                          ? createAliasListOutSide2()
                                          : (checkQuerySelect() == 2 && level == 2)
                                              ? createAliasList2()
                                              : createAliasList1(),
                          level: level,
                          check: checkQuerySelect(),
                          callbackQueryCondition: (newData) {
                            setState(() {
                              data = newData;
                            });
                          },
                        ),
                      if (type == "queryGroupByList")
                        QueryGroupByEditWidget(
                          queryGroupBy: data,
                          alias: (checkQuerySelect() == 3 && level == 1)
                              ? createAliasListOutSide2()
                              : (checkQuerySelect() == 3 && level == 2)
                                  ? createAliasListOutSide3()
                                  : (checkQuerySelect() == 3 && level == 3)
                                      ? createAliasList3()
                                      : (checkQuerySelect() == 2 && level == 1)
                                          ? createAliasListOutSide2()
                                          : (checkQuerySelect() == 2 && level == 2)
                                              ? createAliasList2()
                                              : createAliasList1(),
                          level: level,
                          check: checkQuerySelect(),
                          callbackGroupBy: (newData) {
                            setState(() {
                              data = newData;
                            });
                          },
                        ),
                      if (type == "queryOrderByList")
                        QueryOrderByEditWidget(
                          queryOrderBy: data,
                          alias: (checkQuerySelect() == 3 && level == 1)
                              ? createAliasListOutSide2()
                              : (checkQuerySelect() == 3 && level == 2)
                                  ? createAliasListOutSide3()
                                  : (checkQuerySelect() == 3 && level == 3)
                                      ? createAliasList3()
                                      : (checkQuerySelect() == 2 && level == 1)
                                          ? createAliasListOutSide2()
                                          : (checkQuerySelect() == 2 && level == 2)
                                              ? createAliasList2()
                                              : createAliasList1(),
                          level: level,
                          check: checkQuerySelect(),
                          callback: (newData) {
                            setState(() {
                              data = newData;
                            });
                          },
                        ),
                    ],
                  ))
                ]),
              ),
            ),
          );
        });
  }

  Column fromAlias(data) {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
                child: Padding(
                    padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                    child: TextField(
                      controller: TextEditingController(text: data["queryTable"]["alias"]),
                      onChanged: (value) {
                        data["queryTable"]["alias"] = value;
                      },
                      decoration: InputDecoration(
                        label: const MultiLanguageText(name: "alias", defaultValue: "Alias"),
                        hintText: multiLanguageString(name: "alias", defaultValue: "Alias", context: context),
                        border: const OutlineInputBorder(),
                      ),
                    )))
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                setState(() {
                  Navigator.pop(context, true);
                });
              },
              child: const MultiLanguageText(name: "close", defaultValue: "Close"),
            )
          ],
        )
      ],
    );
  }

  Column groupTypeDialog(data) {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
                child: Padding(
                    padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                    child: DynamicDropdownButton(
                        borderRadius: 5,
                        value: data["groupType"],
                        hint: "",
                        labelText: multiLanguageString(name: "group_type", defaultValue: "Group type", context: context),
                        items: ["AND", "OR"].map<DropdownMenuItem<String>>((dynamic result) {
                          return DropdownMenuItem(
                            value: result,
                            child: Text(result),
                          );
                        }).toList(),
                        onChanged: (newValue) async {
                          data["groupType"] = newValue;
                        },
                        isRequiredNotEmpty: true)))
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                setState(() {
                  Navigator.pop(context, true);
                });
              },
              child: const MultiLanguageText(name: "close", defaultValue: "Close"),
            )
          ],
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(
      builder: (context, languageModel, child) {
        return FutureBuilder(
          future: future,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return TestFrameworkRootPageWidget(
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            headerWidget(context),
                            queryColumnListWidget(reportQueryConfig, 4, 1),
                            queryFromListWidget(reportQueryConfig, 4, 1),
                            queryConditionGroupWidget(reportQueryConfig, 4, 1),
                            queryGroupByListWidget(reportQueryConfig, 4, 1),
                            queryOrderByListWidget(reportQueryConfig, 4, 1),
                          ],
                        ),
                      ),
                    ),
                    Positioned(bottom: 0, left: 0, right: 0, child: btnWidget())
                  ],
                ),
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  Padding headerWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 35, 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "edit_report_query",
            defaultValue: "Edit report query",
            isLowerCase: false,
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
            onPressed: () => Navigator.pop(context, 'cancel'),
            child: const Icon(
              Icons.close,
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }

  Padding btnWidget() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20, top: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ButtonSave(onPressed: () {
            widget.callbackReportQuery(reportQueryConfig);
          }),
          const ButtonCancel()
        ],
      ),
    );
  }

  Widget queryColumnListWidget(dynamic reportQueryConfig, number, int level) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          child: RichText(
            text: TextSpan(
              style: const TextStyle(color: Colors.black),
              children: [
                TextSpan(text: " " * number),
                const TextSpan(text: "SELECT"),
                TextSpan(text: " " * 2),
                TextSpan(
                  text: multiLanguageString(name: "add", defaultValue: "Add", context: context),
                  style: const TextStyle(color: Colors.green),
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      reportQueryConfig["queryColumnList"] ??= [];
                      setState(() {
                        String? format;
                        String? columnAlias;
                        String? columnName;
                        reportQueryConfig["queryColumnList"].add({
                          "format": format,
                          "columnType": "Column",
                          "columnAlias": columnAlias,
                          "queryTableColumn": {"columnName": columnName, "queryTable": {}}
                        });
                      });
                    },
                ),
              ],
            ),
          ),
        ),
        for (var queryColumn in reportQueryConfig["queryColumnList"] ?? [])
          Column(
            children: [
              (queryColumn["columnType"] == "Sum")
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        if (queryColumn["queryCase"] != null)
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 5, bottom: 5),
                                child: RichText(
                                  text: TextSpan(
                                    style: const TextStyle(color: Colors.black),
                                    children: [
                                      TextSpan(text: " " * (number + 4)),
                                      const TextSpan(text: "- Sum ("),
                                      TextSpan(text: " " * 2),
                                      TextSpan(
                                        text: multiLanguageString(name: "edit", defaultValue: "edit", context: context),
                                        style: const TextStyle(color: Colors.green),
                                        recognizer: TapGestureRecognizer()
                                          ..onTap = () {
                                            setState(() {
                                              showEditDialog(queryColumn, "queryColumnList", level);
                                            });
                                          },
                                      ),
                                      TextSpan(text: " " * 2),
                                      TextSpan(
                                          text: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                                          style: const TextStyle(color: Colors.green),
                                          recognizer: TapGestureRecognizer()
                                            ..onTap = () {
                                              setState(() {
                                                reportQueryConfig["queryColumnList"].remove(queryColumn);
                                              });
                                            }),
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 5, bottom: 5),
                                child: RichText(
                                  text: TextSpan(
                                    style: const TextStyle(color: Colors.black),
                                    children: [
                                      TextSpan(text: " " * (number + 8)),
                                      const TextSpan(text: "Case"),
                                      TextSpan(text: " " * 2),
                                      TextSpan(
                                        text: multiLanguageString(name: "add", defaultValue: "Add", context: context),
                                        style: const TextStyle(color: Colors.green),
                                        recognizer: TapGestureRecognizer()
                                          ..onTap = () {
                                            queryColumn["queryCase"]["queryCaseWhenList"] ??= [];
                                            setState(() {
                                              String? format;
                                              String? columnType;
                                              String? columnAlias;
                                              String? operation;
                                              String? ignoreNull;
                                              String? columnName;
                                              String? groupType;
                                              queryColumn["queryCase"]["queryCaseWhenList"].add({
                                                "queryColumnThen": {"format": format, "columnType": columnType, "queryValue": {}},
                                                "queryConditionGroup": {
                                                  "groupType": groupType,
                                                  "queryConditionList": [
                                                    {
                                                      "operation": operation,
                                                      "ignoreNull": ignoreNull,
                                                      "queryColumn1": {
                                                        "format": format,
                                                        "columnType": columnType,
                                                        "columnAlias": columnAlias,
                                                        "queryTableColumn": {"columnName": columnName, "queryTable": {}}
                                                      },
                                                      "queryColumn2": {
                                                        "format": format,
                                                        "columnType": columnType,
                                                        "columnAlias": columnAlias,
                                                        "queryTableColumn": {"columnName": columnName, "queryTable": {}}
                                                      }
                                                    }
                                                  ]
                                                }
                                              });
                                            });
                                          },
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              queryCaseWhenWidget(queryColumn["queryCase"], (number + 12), level),
                              if (queryColumn["queryCase"]["queryColumnElse"] != null)
                                Padding(
                                  padding: const EdgeInsets.only(top: 5, bottom: 5),
                                  child: RichText(
                                    text: TextSpan(
                                      style: const TextStyle(color: Colors.black),
                                      children: [
                                        TextSpan(text: " " * (number + 12)),
                                        TextSpan(
                                            text: (queryColumn["queryCase"]["queryColumnElse"]["queryValue"] != null)
                                                ? "Else ${queryColumn["queryCase"]["queryColumnElse"]["queryValue"]["value"] ?? ""}"
                                                : "Else ${queryColumn["queryCase"]["queryColumnElse"]["queryTableColumn"]["columnName"] ?? ""}"),
                                        TextSpan(text: " " * 2),
                                        TextSpan(
                                          text: multiLanguageString(name: "edit", defaultValue: "edit", context: context),
                                          style: const TextStyle(color: Colors.green),
                                          recognizer: TapGestureRecognizer()
                                            ..onTap = () {
                                              setState(() {
                                                showEditDialog(queryColumn["queryCase"]["queryColumnElse"], "queryColumnElse", level);
                                              });
                                            },
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              Padding(
                                padding: const EdgeInsets.only(top: 5, bottom: 5),
                                child: RichText(
                                  text: TextSpan(
                                    style: const TextStyle(color: Colors.black),
                                    children: [
                                      TextSpan(text: " " * (number + 8)),
                                      const TextSpan(text: "End"),
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 5, bottom: 5),
                                child: RichText(
                                  text: TextSpan(
                                    style: const TextStyle(color: Colors.black),
                                    children: [
                                      TextSpan(text: " " * (number + 4)),
                                      TextSpan(text: ") As ${queryColumn["columnAlias"]}"),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        if (queryColumn["queryColumn"] != null)
                          Padding(
                            padding: const EdgeInsets.only(top: 5, bottom: 5),
                            child: RichText(
                              text: TextSpan(
                                style: const TextStyle(color: Colors.black),
                                children: [
                                  TextSpan(text: " " * (number + 4)),
                                  TextSpan(
                                      text:
                                          "- Sum (${(queryColumn["queryColumn"]["queryTableColumn"] != null) ? queryColumn["queryColumn"]["queryTableColumn"]["columnName"] : queryColumn["queryColumn"]["queryValue"]["value"]}) As ${queryColumn["columnAlias"] ?? ""}"),
                                  TextSpan(text: " " * 2),
                                  TextSpan(
                                    text: multiLanguageString(name: "edit", defaultValue: "edit", context: context),
                                    style: const TextStyle(color: Colors.green),
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        setState(() {
                                          showEditDialog(queryColumn, "queryColumnList", level);
                                        });
                                      },
                                  ),
                                  TextSpan(text: " " * 2),
                                  TextSpan(
                                      text: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                                      style: const TextStyle(color: Colors.green),
                                      recognizer: TapGestureRecognizer()
                                        ..onTap = () {
                                          setState(() {
                                            reportQueryConfig["queryColumnList"].remove(queryColumn);
                                          });
                                        }),
                                ],
                              ),
                            ),
                          ),
                      ],
                    )
                  : (queryColumn["queryCase"] != null && queryColumn["columnType"] == "Case")
                      ? Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(top: 5, bottom: 5),
                              child: RichText(
                                text: TextSpan(
                                  style: const TextStyle(color: Colors.black),
                                  children: [
                                    TextSpan(text: " " * (number + 4)),
                                    const TextSpan(text: "- Case"),
                                    TextSpan(text: " " * 2),
                                    TextSpan(
                                      text: multiLanguageString(name: "add", defaultValue: "Add", context: context),
                                      style: const TextStyle(color: Colors.green),
                                      recognizer: TapGestureRecognizer()
                                        ..onTap = () {
                                          queryColumn["queryCase"]["queryCaseWhenList"] ??= [];
                                          setState(() {
                                            String? format;
                                            String? columnType;
                                            String? columnAlias;
                                            String? operation;
                                            String? ignoreNull;
                                            String? columnName;
                                            String? groupType;
                                            queryColumn["queryCase"]["queryCaseWhenList"].add({
                                              "queryColumnThen": {"format": format, "columnType": columnType, "queryValue": {}},
                                              "queryConditionGroup": {
                                                "groupType": groupType,
                                                "queryConditionList": [
                                                  {
                                                    "operation": operation,
                                                    "ignoreNull": ignoreNull,
                                                    "queryColumn1": {
                                                      "format": format,
                                                      "columnType": columnType,
                                                      "columnAlias": columnAlias,
                                                      "queryTableColumn": {"columnName": columnName, "queryTable": {}}
                                                    },
                                                    "queryColumn2": {
                                                      "format": format,
                                                      "columnType": columnType,
                                                      "columnAlias": columnAlias,
                                                      "queryTableColumn": {"columnName": columnName, "queryTable": {}}
                                                    }
                                                  }
                                                ]
                                              }
                                            });
                                          });
                                        },
                                    ),
                                    TextSpan(text: " " * 2),
                                    TextSpan(
                                      text: multiLanguageString(name: "edit", defaultValue: "edit", context: context),
                                      style: const TextStyle(color: Colors.green),
                                      recognizer: TapGestureRecognizer()
                                        ..onTap = () {
                                          setState(() {
                                            showEditDialog(queryColumn, "queryColumnList", level);
                                          });
                                        },
                                    ),
                                    TextSpan(text: " " * 2),
                                    TextSpan(
                                        text: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                                        style: const TextStyle(color: Colors.green),
                                        recognizer: TapGestureRecognizer()
                                          ..onTap = () {
                                            setState(() {
                                              reportQueryConfig["queryColumnList"].remove(queryColumn);
                                            });
                                          }),
                                  ],
                                ),
                              ),
                            ),
                            queryCaseWhenWidget(queryColumn["queryCase"], (number + 8), level),
                            if (queryColumn["queryCase"]["queryColumnElse"] != null)
                              Padding(
                                padding: const EdgeInsets.only(top: 5, bottom: 5),
                                child: RichText(
                                  text: TextSpan(
                                    style: const TextStyle(color: Colors.black),
                                    children: [
                                      TextSpan(text: " " * (number + 8)),
                                      TextSpan(
                                          text: (queryColumn["queryCase"]["queryColumnElse"]["queryValue"] != null)
                                              ? "Else ${queryColumn["queryCase"]["queryColumnElse"]["queryValue"]["value"] ?? ""}"
                                              : "Else ${queryColumn["queryCase"]["queryColumnElse"]["queryTableColumn"]["columnName"] ?? ""}"),
                                      TextSpan(text: " " * 2),
                                      TextSpan(
                                        text: multiLanguageString(name: "edit", defaultValue: "edit", context: context),
                                        style: const TextStyle(color: Colors.green),
                                        recognizer: TapGestureRecognizer()
                                          ..onTap = () {
                                            setState(() {
                                              showEditDialog(queryColumn["queryCase"]["queryColumnElse"], "queryColumnElse", level);
                                            });
                                          },
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            Padding(
                              padding: const EdgeInsets.only(top: 5, bottom: 5),
                              child: RichText(
                                text: TextSpan(
                                  style: const TextStyle(color: Colors.black),
                                  children: [
                                    TextSpan(text: " " * (number + 4)),
                                    TextSpan(text: "End As ${queryColumn["columnAlias"]}"),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        )
                      : (queryColumn["queryTableColumn"] != null)
                          ? Column(
                              children: [
                                if (queryColumn["columnType"] == "Column")
                                  Padding(
                                    padding: const EdgeInsets.only(top: 5, bottom: 5),
                                    child: RichText(
                                      text: TextSpan(style: const TextStyle(color: Colors.black), children: [
                                        TextSpan(text: " " * (number + 4)),
                                        TextSpan(
                                            text:
                                                "- ${queryColumn["queryTableColumn"]["queryTable"]["tableName"] ?? queryColumn["queryTableColumn"]["queryTable"]["alias"]}.${queryColumn["queryTableColumn"]["columnName"]} as ${queryColumn["columnAlias"] ?? ""}"),
                                        TextSpan(text: " " * 2),
                                        TextSpan(
                                          text: multiLanguageString(name: "edit", defaultValue: "edit", context: context),
                                          style: const TextStyle(color: Colors.green),
                                          recognizer: TapGestureRecognizer()
                                            ..onTap = () {
                                              setState(() {
                                                showEditDialog(queryColumn, "queryColumnList", level);
                                              });
                                            },
                                        ),
                                        TextSpan(text: " " * 2),
                                        TextSpan(
                                          text: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                                          style: const TextStyle(color: Colors.green),
                                          recognizer: TapGestureRecognizer()
                                            ..onTap = () {
                                              setState(() {
                                                reportQueryConfig["queryColumnList"].remove(queryColumn);
                                              });
                                            },
                                        ),
                                      ]),
                                    ),
                                  ),
                                if (queryColumn["columnType"] == "Avg" ||
                                    queryColumn["columnType"] == "Min" ||
                                    queryColumn["columnType"] == "Max" ||
                                    queryColumn["columnType"] == "Count")
                                  Padding(
                                    padding: const EdgeInsets.only(top: 5, bottom: 5),
                                    child: RichText(
                                      text: TextSpan(style: const TextStyle(color: Colors.black), children: [
                                        TextSpan(text: " " * (number + 4)),
                                        TextSpan(
                                            text:
                                                "- ${queryColumn["columnType"]} (${queryColumn["queryTableColumn"]["queryTable"]["tableName"] ?? queryColumn["queryTableColumn"]["queryTable"]["alias"]}.${queryColumn["queryTableColumn"]["columnName"] ?? ""}) as ${queryColumn["columnAlias"] ?? ""}"),
                                        TextSpan(text: " " * 2),
                                        TextSpan(
                                          text: multiLanguageString(name: "edit", defaultValue: "edit", context: context),
                                          style: const TextStyle(color: Colors.green),
                                          recognizer: TapGestureRecognizer()
                                            ..onTap = () {
                                              setState(() {
                                                showEditDialog(queryColumn, "queryColumnList", level);
                                              });
                                            },
                                        ),
                                        TextSpan(text: " " * 2),
                                        TextSpan(
                                          text: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                                          style: const TextStyle(color: Colors.green),
                                          recognizer: TapGestureRecognizer()
                                            ..onTap = () {
                                              setState(() {
                                                reportQueryConfig["queryColumnList"].remove(queryColumn);
                                              });
                                            },
                                        ),
                                      ]),
                                    ),
                                  ),
                                if (queryColumn["columnType"] == "TruncDate")
                                  Padding(
                                    padding: const EdgeInsets.only(top: 5, bottom: 5),
                                    child: RichText(
                                      text: TextSpan(style: const TextStyle(color: Colors.black), children: [
                                        TextSpan(text: " " * (number + 4)),
                                        TextSpan(
                                            text:
                                                "- ${queryColumn["columnType"]} (${queryColumn["queryTableColumn"]["queryTable"]["tableName"] ?? queryColumn["queryTableColumn"]["queryTable"]["alias"]}.${queryColumn["queryTableColumn"]["columnName"] ?? ""}) as ${queryColumn["columnAlias"] ?? ""}"),
                                        TextSpan(text: "  format: ${queryColumn["format"] ?? ""}"),
                                        TextSpan(text: " " * 2),
                                        TextSpan(
                                          text: multiLanguageString(name: "edit", defaultValue: "edit", context: context),
                                          style: const TextStyle(color: Colors.green),
                                          recognizer: TapGestureRecognizer()
                                            ..onTap = () {
                                              setState(() {
                                                showEditDialog(queryColumn, "queryColumnList", level);
                                              });
                                            },
                                        ),
                                        TextSpan(text: " " * 2),
                                        TextSpan(
                                          text: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                                          style: const TextStyle(color: Colors.green),
                                          recognizer: TapGestureRecognizer()
                                            ..onTap = () {
                                              setState(() {
                                                reportQueryConfig["queryColumnList"].remove(queryColumn);
                                              });
                                            },
                                        ),
                                      ]),
                                    ),
                                  ),
                              ],
                            )
                          : Padding(
                              padding: const EdgeInsets.only(top: 5, bottom: 5),
                              child: RichText(
                                text: TextSpan(
                                  style: const TextStyle(color: Colors.black),
                                  children: [
                                    TextSpan(text: " " * (number + 4)),
                                    TextSpan(text: "- ${queryColumn["columnType"]} as ${queryColumn["columnAlias"]}"),
                                    TextSpan(text: " " * 2),
                                    TextSpan(
                                        text: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                        style: const TextStyle(color: Colors.green),
                                        recognizer: TapGestureRecognizer()
                                          ..onTap = () {
                                            showEditDialog(queryColumn, "queryColumnList", level);
                                          }),
                                    TextSpan(text: " " * 2),
                                    TextSpan(
                                        text: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                                        style: const TextStyle(color: Colors.green),
                                        recognizer: TapGestureRecognizer()
                                          ..onTap = () {
                                            setState(() {
                                              reportQueryConfig["queryColumnList"].remove(queryColumn);
                                            });
                                          }),
                                  ],
                                ),
                              ),
                            ),
            ],
          ),
        const SizedBox(height: 10),
      ],
    );
  }

  Widget queryCaseWhenWidget(dynamic queryCase, int number, int level) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        for (var queryCaseWhen in queryCase["queryCaseWhenList"] ?? [])
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 5, bottom: 5),
                child: RichText(
                  text: TextSpan(
                    style: const TextStyle(color: Colors.black),
                    children: [
                      TextSpan(text: " " * number),
                      TextSpan(text: "When ${queryCaseWhen["queryConditionGroup"]["groupType"]}"),
                      TextSpan(text: " " * 2),
                      TextSpan(
                          text: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                          style: const TextStyle(color: Colors.green),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              showEditDialog(queryCaseWhen["queryConditionGroup"], "groupType", level);
                            }),
                      TextSpan(text: " " * 2),
                      TextSpan(
                        text: multiLanguageString(name: "add", defaultValue: "Add", context: context),
                        style: const TextStyle(color: Colors.green),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            queryCaseWhen["queryConditionGroup"]["queryConditionList"] ??= [];
                            setState(() {
                              String? operation;
                              bool? ignoreNull;
                              String? format;
                              String? columnAlias;
                              String? columnName;
                              String value = "";
                              bool? isList;
                              bool isNull = false;
                              String? valueType;
                              queryCaseWhen["queryConditionGroup"]["queryConditionList"].add({
                                "operation": operation,
                                "ignoreNull": ignoreNull,
                                "queryColumn1": {
                                  "format": format,
                                  "columnType": "Column",
                                  "columnAlias": columnAlias,
                                  "queryTableColumn": {"columnName": columnName, "queryTable": {}}
                                },
                                "queryColumn2": {
                                  "format": format,
                                  "columnType": "Value",
                                  "columnAlias": columnAlias,
                                  "queryValue": {"value": value, "isList": isList, "isNull": isNull, "valueType": valueType, "ignoreNull": ignoreNull}
                                }
                              });
                            });
                          },
                      ),
                      TextSpan(text: " " * 2),
                      TextSpan(
                          text: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                          style: const TextStyle(color: Colors.green),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              setState(() {
                                queryCase["queryCaseWhenList"].remove(queryCaseWhen);
                              });
                            }),
                    ],
                  ),
                ),
              ),
              queryConditionList(queryCaseWhen["queryConditionGroup"], number + 4, level),
              if (queryCaseWhen["queryColumnThen"] != null)
                Padding(
                  padding: const EdgeInsets.only(top: 5, bottom: 5),
                  child: RichText(
                    text: TextSpan(style: const TextStyle(color: Colors.black), children: [
                      TextSpan(text: " " * number),
                      TextSpan(
                          text: (queryCaseWhen["queryColumnThen"]["queryValue"] != null)
                              ? "Then: ${queryCaseWhen["queryColumnThen"]["queryValue"]["value"] ?? ""}"
                              : "Then: ${queryCaseWhen["queryColumnThen"]["queryTableColumn"]["columnName"] ?? ""}"),
                      TextSpan(text: " " * 2),
                      TextSpan(
                        text: multiLanguageString(name: "edit", defaultValue: "edit", context: context),
                        style: const TextStyle(color: Colors.green),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            setState(() {
                              showEditDialog(queryCaseWhen["queryColumnThen"], "queryColumnThen", level);
                            });
                          },
                      ),
                      TextSpan(text: " " * 2),
                      TextSpan(
                          text: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                          style: const TextStyle(color: Colors.green),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              setState(() {
                                queryCaseWhen.remove("queryColumnThen");
                              });
                            }),
                    ]),
                  ),
                ),
            ],
          ),
      ],
    );
  }

  Widget queryFromListWidget(dynamic reportQueryConfig, int number, int level) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          child: RichText(
            text: TextSpan(
              style: const TextStyle(color: Colors.black),
              children: [
                TextSpan(text: " " * number),
                const TextSpan(text: "FROM"),
                TextSpan(text: " " * 2),
                TextSpan(
                  text: multiLanguageString(name: "add", defaultValue: "Add", context: context),
                  style: const TextStyle(color: Colors.green),
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      setState(() {
                        reportQueryConfig["queryFromList"] ??= [];
                        reportQueryConfig["queryFromList"].add({"queryTable": {}});
                      });
                    },
                ),
              ],
            ),
          ),
        ),
        for (var queryFrom in reportQueryConfig["queryFromList"] ?? [])
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (queryFrom["queryJoin"] != null)
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 5, bottom: 5),
                      child: RichText(
                        text: TextSpan(
                          style: const TextStyle(color: Colors.black),
                          children: [
                            TextSpan(text: " " * (number + 4)),
                            TextSpan(text: "- ${queryFrom["queryJoin"]["joinType"] ?? ""} "),
                            TextSpan(
                                text: (queryFrom["queryJoin"]["queryTable2"] != null)
                                    ? "${queryFrom["queryJoin"]["queryTable2"]["schemaName"] != null && queryFrom["queryJoin"]["queryTable2"]["schemaName"].isNotEmpty ? "${queryFrom["queryJoin"]["queryTable2"]["schemaName"]}." : ""}${queryFrom["queryJoin"]["queryTable2"]["tableName"]} as ${queryFrom["queryJoin"]["queryTable2"]["alias"]}"
                                    : "${queryFrom["queryJoin"]["queryTable"]["schemaName"] != null && queryFrom["queryJoin"]["queryTable"]["schemaName"].isNotEmpty ? "${queryFrom["queryJoin"]["queryTable"]["schemaName"]}." : ""}${queryFrom["queryJoin"]["queryTable"]["tableName"]} as ${queryFrom["queryJoin"]["queryTable"]["alias"]}"),
                            TextSpan(text: " " * 2),
                            TextSpan(
                                text: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                style: const TextStyle(color: Colors.green),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    showEditDialog(queryFrom, "queryFromList", level);
                                  }),
                            TextSpan(text: " " * 2),
                            TextSpan(
                                text: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                                style: const TextStyle(color: Colors.green),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    setState(() {
                                      reportQueryConfig["queryFromList"].remove(queryFrom);
                                    });
                                  }),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 5, bottom: 5),
                      child: RichText(
                        text: TextSpan(
                          style: const TextStyle(color: Colors.black),
                          children: [
                            TextSpan(text: " " * (number + 4)),
                            TextSpan(text: "ON ${queryFrom["queryJoin"]["queryConditionGroup"]["groupType"] ?? ""}"),
                            TextSpan(text: " " * 2),
                            TextSpan(
                              text: multiLanguageString(name: "edit", defaultValue: "edit", context: context),
                              style: const TextStyle(color: Colors.green),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  setState(() {
                                    showEditDialog(queryFrom["queryJoin"]["queryConditionGroup"], "groupType", level);
                                  });
                                },
                            ),
                            TextSpan(text: " " * 2),
                            TextSpan(
                              text: multiLanguageString(name: "add", defaultValue: "Add", context: context),
                              style: const TextStyle(color: Colors.green),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  queryFrom["queryJoin"]["queryConditionGroup"]["queryConditionList"] ??= [];
                                  setState(() {
                                    String? operation;
                                    String? ignoreNull;
                                    String? format;
                                    String? columnAlias;
                                    String? columnName;
                                    queryFrom["queryJoin"]["queryConditionGroup"]["queryConditionList"].add({
                                      "operation": operation,
                                      "ignoreNull": ignoreNull,
                                      "queryColumn1": {
                                        "format": format,
                                        "columnType": "Column",
                                        "columnAlias": columnAlias,
                                        "queryTableColumn": {"columnName": columnName, "queryTable": {}}
                                      },
                                      "queryColumn2": {
                                        "format": format,
                                        "columnType": "Column",
                                        "columnAlias": columnAlias,
                                        "queryTableColumn": {"columnName": columnName, "queryTable": {}}
                                      }
                                    });
                                  });
                                },
                            ),
                            TextSpan(text: " " * 2),
                            TextSpan(
                                text: multiLanguageString(name: "clear", defaultValue: "Clear", context: context),
                                style: const TextStyle(color: Colors.green),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    setState(() {
                                      queryFrom["queryJoin"]["queryConditionGroup"].clear();
                                    });
                                  }),
                          ],
                        ),
                      ),
                    ),
                    queryConditionList(queryFrom["queryJoin"]["queryConditionGroup"], number + 8, level),
                  ],
                ),
              if (queryFrom["queryTable"] != null && !queryFrom["queryTable"].containsKey("querySelect"))
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 5, bottom: 5),
                      child: RichText(
                        text: TextSpan(
                          style: const TextStyle(color: Colors.black),
                          children: [
                            TextSpan(text: " " * (number + 4)),
                            TextSpan(
                                text:
                                    "- ${queryFrom["queryTable"]["schemaName"] != null && queryFrom["queryTable"]["schemaName"].isNotEmpty ? "${queryFrom["queryTable"]["schemaName"]}." : ""}${queryFrom["queryTable"]["tableName"]} as ${queryFrom["queryTable"]["alias"]}"),
                            TextSpan(text: " " * 2),
                            TextSpan(
                                text: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                style: const TextStyle(color: Colors.green),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    showEditDialog(queryFrom, "queryFromList", level);
                                  }),
                            TextSpan(text: " " * 2),
                            TextSpan(
                                text: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                                style: const TextStyle(color: Colors.green),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    setState(() {
                                      reportQueryConfig["queryFromList"].remove(queryFrom);
                                    });
                                  }),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              if (queryFrom["queryTable"] != null && queryFrom["queryTable"].containsKey("querySelect"))
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    queryColumnListWidget(queryFrom["queryTable"]["querySelect"], (number + 4), level + 1),
                    queryFromListWidget(queryFrom["queryTable"]["querySelect"], (number + 8), level + 1),
                    queryConditionGroupWidget(queryFrom["queryTable"]["querySelect"], (number + 8), level + 1),
                    queryGroupByListWidget(queryFrom["queryTable"]["querySelect"], (number + 8), level + 1),
                    queryOrderByListWidget(queryFrom["queryTable"]["querySelect"], (number + 8), level + 1),
                    Padding(
                      padding: const EdgeInsets.only(top: 5, bottom: 5),
                      child: RichText(
                        text: TextSpan(
                          style: const TextStyle(color: Colors.black),
                          children: [
                            TextSpan(text: " " * number),
                            TextSpan(text: "As ${queryFrom["queryTable"]["alias"]}"),
                            TextSpan(text: " " * 2),
                            TextSpan(
                                text: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                style: const TextStyle(color: Colors.green),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    showEditDialog(queryFrom, "fromAlias", level);
                                  }),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
            ],
          ),
        const SizedBox(height: 10),
      ],
    );
  }

  Widget queryConditionGroupWidget(dynamic reportQueryConfig, int number, int level) {
    reportQueryConfig["queryConditionGroup"] ??= {};
    return Container(
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(5)),
      padding: const EdgeInsets.only(top: 5, bottom: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 5, bottom: 5),
            child: RichText(
              text: TextSpan(
                style: const TextStyle(color: Colors.black),
                children: [
                  TextSpan(text: " " * number),
                  TextSpan(text: "WHERE ${reportQueryConfig["queryConditionGroup"]["groupType"] ?? ""}"),
                  TextSpan(text: " " * 2),
                  TextSpan(
                    text: multiLanguageString(name: "edit", defaultValue: "edit", context: context),
                    style: const TextStyle(color: Colors.green),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        setState(() {
                          showEditDialog(reportQueryConfig["queryConditionGroup"], "groupType", level);
                        });
                      },
                  ),
                  TextSpan(text: " " * 2),
                  TextSpan(
                    text: multiLanguageString(name: "add", defaultValue: "Add", context: context),
                    style: const TextStyle(color: Colors.green),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        reportQueryConfig["queryConditionGroup"]["queryConditionList"] ??= [];
                        setState(() {
                          String? operation;
                          String? ignoreNull;
                          String? format;
                          String? columnAlias;
                          String? columnName;
                          reportQueryConfig["queryConditionGroup"]["queryConditionList"].add({
                            "operation": operation,
                            "ignoreNull": ignoreNull,
                            "queryColumn1": {
                              "format": format,
                              "columnType": "Column",
                              "columnAlias": columnAlias,
                              "queryTableColumn": {"columnName": columnName, "queryTable": {}}
                            },
                            "queryColumn2": {
                              "format": format,
                              "columnType": "Column",
                              "columnAlias": columnAlias,
                              "queryTableColumn": {"columnName": columnName, "queryTable": {}}
                            }
                          });
                        });
                      },
                  ),
                  TextSpan(text: " " * 2),
                  TextSpan(
                      text: multiLanguageString(name: "clear", defaultValue: "Clear", context: context),
                      style: const TextStyle(color: Colors.green),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          setState(() {
                            reportQueryConfig["queryConditionGroup"].clear();
                          });
                        }),
                ],
              ),
            ),
          ),
          queryConditionList(reportQueryConfig["queryConditionGroup"], number + 4, level),
          const SizedBox(height: 10),
        ],
      ),
    );
  }

  Widget queryGroupByListWidget(dynamic reportQueryConfig, int number, int level) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          child: RichText(
            text: TextSpan(
              style: const TextStyle(color: Colors.black),
              children: [
                TextSpan(text: " " * number),
                const TextSpan(text: "GROUP BY"),
                TextSpan(text: " " * 2),
                TextSpan(
                  text: multiLanguageString(name: "add", defaultValue: "Add", context: context),
                  style: const TextStyle(color: Colors.green),
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      reportQueryConfig["queryGroupByList"] ??= [];
                      setState(() {
                        String? columnName;
                        String? format;
                        String? columnAlias;
                        reportQueryConfig["queryGroupByList"].add({
                          "format": format,
                          "columnType": "Column",
                          "columnAlias": columnAlias,
                          "queryTableColumn": {"columnName": columnName, "queryTable": {}}
                        });
                      });
                    },
                ),
              ],
            ),
          ),
        ),
        for (var queryGroupBy in reportQueryConfig["queryGroupByList"] ?? [])
          Padding(
            padding: const EdgeInsets.only(top: 5, bottom: 5),
            child: RichText(
              text: TextSpan(
                style: const TextStyle(color: Colors.black),
                children: [
                  TextSpan(text: " " * (number + 4)),
                  TextSpan(
                      text:
                          "- ${queryGroupBy["queryTableColumn"]["queryTable"]["tableName"] ?? queryGroupBy["queryTableColumn"]["queryTable"]["alias"]}.${queryGroupBy["queryTableColumn"]["columnName"]}"),
                  TextSpan(text: " " * 2),
                  TextSpan(
                      text: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                      style: const TextStyle(color: Colors.green),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          showEditDialog(queryGroupBy, "queryGroupByList", level);
                        }),
                  TextSpan(text: " " * 2),
                  TextSpan(
                      text: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                      style: const TextStyle(color: Colors.green),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          setState(() {
                            reportQueryConfig["queryGroupByList"].remove(queryGroupBy);
                          });
                        }),
                ],
              ),
            ),
          ),
        const SizedBox(height: 10),
      ],
    );
  }

  Widget queryOrderByListWidget(dynamic reportQueryConfig, int number, int level) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          child: RichText(
            text: TextSpan(
              style: const TextStyle(color: Colors.black),
              children: [
                TextSpan(text: " " * number),
                const TextSpan(text: "ORDER BY"),
                TextSpan(text: " " * 2),
                TextSpan(
                  text: multiLanguageString(name: "add", defaultValue: "Add", context: context),
                  style: const TextStyle(color: Colors.green),
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      reportQueryConfig["queryOrderByList"] ??= [];
                      setState(() {
                        String? orderByText;
                        String? orderDirection;
                        String? columnName;
                        String? format;
                        String? columnAlias;
                        reportQueryConfig["queryOrderByList"].add({
                          "orderByText": orderByText,
                          "orderDirection": orderDirection,
                          "queryColumn": {
                            "format": format,
                            "columnType": "Column",
                            "columnAlias": columnAlias,
                            "queryTableColumn": {"columnName": columnName, "queryTable": {}}
                          }
                        });
                      });
                    },
                ),
              ],
            ),
          ),
        ),
        for (var queryOrderBy in reportQueryConfig["queryOrderByList"] ?? [])
          (queryOrderBy["queryColumn"]["queryTableColumn"] != null)
              ? Padding(
                  padding: const EdgeInsets.only(top: 5, bottom: 5),
                  child: RichText(
                    text: TextSpan(
                      style: const TextStyle(color: Colors.black),
                      children: [
                        TextSpan(text: " " * (number + 4)),
                        TextSpan(
                            text:
                                "- ${queryOrderBy["queryColumn"]["queryTableColumn"]["queryTable"]["tableName"] ?? queryOrderBy["queryColumn"]["queryTableColumn"]["queryTable"]["alias"]}.${queryOrderBy["queryColumn"]["queryTableColumn"]["columnName"]} ${queryOrderBy["orderDirection"]}"),
                        TextSpan(text: " " * 2),
                        TextSpan(
                            text: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                            style: const TextStyle(color: Colors.green),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                showEditDialog(queryOrderBy, "queryOrderByList", level);
                              }),
                        TextSpan(text: " " * 2),
                        TextSpan(
                            text: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                            style: const TextStyle(color: Colors.green),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                setState(() {
                                  reportQueryConfig["queryOrderByList"].remove(queryOrderBy);
                                });
                              }),
                      ],
                    ),
                  ),
                )
              : Padding(
                  padding: const EdgeInsets.only(top: 5, bottom: 5),
                  child: RichText(
                    text: TextSpan(
                      style: const TextStyle(color: Colors.black),
                      children: [
                        TextSpan(text: " " * (number + 4)),
                        TextSpan(text: "- ${queryOrderBy["queryColumn"]["columnAlias"]} ${queryOrderBy["orderDirection"]}"),
                        TextSpan(text: " " * 2),
                        TextSpan(
                            text: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                            style: const TextStyle(color: Colors.green),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                showEditDialog(queryOrderBy, "queryOrderByList", level);
                              }),
                        TextSpan(text: " " * 2),
                        TextSpan(
                            text: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                            style: const TextStyle(color: Colors.green),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                setState(() {
                                  reportQueryConfig["queryOrderByList"].remove(queryOrderBy);
                                });
                              }),
                      ],
                    ),
                  ),
                ),
      ],
    );
  }

  Widget queryConditionList(dynamic queryConditionGroup, int number, int level) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        for (var queryCondition in queryConditionGroup["queryConditionList"] ?? [])
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (queryCondition["queryColumn1"]["queryTableColumn"] != null)
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    if (queryCondition["queryColumn2"]["queryTableColumn"] != null)
                      Padding(
                        padding: const EdgeInsets.only(top: 5, bottom: 5),
                        child: RichText(
                          text: TextSpan(style: const TextStyle(color: Colors.black), children: [
                            TextSpan(text: " " * number),
                            TextSpan(
                                text:
                                    "- ${queryCondition["queryColumn1"]["queryTableColumn"]["queryTable"]["tableName"] ?? queryCondition["queryColumn1"]["queryTableColumn"]["queryTable"]["alias"]}.${queryCondition["queryColumn1"]["queryTableColumn"]["columnName"]}"),
                            TextSpan(text: " ${queryCondition["operation"]} "),
                            TextSpan(
                                text:
                                    "${queryCondition["queryColumn2"]["queryTableColumn"]["queryTable"]["tableName"]}.${queryCondition["queryColumn2"]["queryTableColumn"]["columnName"]}"),
                            TextSpan(text: " " * 2),
                            TextSpan(
                              text: multiLanguageString(name: "edit", defaultValue: "edit", context: context),
                              style: const TextStyle(color: Colors.green),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  setState(() {
                                    showEditDialog(queryCondition, "queryCondition", level);
                                  });
                                },
                            ),
                            TextSpan(text: " " * 2),
                            TextSpan(
                                text: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                                style: const TextStyle(color: Colors.green),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    setState(() {
                                      queryConditionGroup["queryConditionList"].remove(queryCondition);
                                    });
                                  }),
                          ]),
                        ),
                      ),
                    if (queryCondition["queryColumn2"]["queryValue"] != null)
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          if (queryCondition["queryColumn2"]["queryValue"]["value"] != null)
                            Padding(
                              padding: const EdgeInsets.only(top: 5, bottom: 5),
                              child: RichText(
                                text: TextSpan(
                                  style: const TextStyle(color: Colors.black),
                                  children: [
                                    TextSpan(text: " " * number),
                                    TextSpan(
                                        text:
                                            "- ${queryCondition["queryColumn1"]["queryTableColumn"]["queryTable"]["tableName"] ?? queryCondition["queryColumn1"]["queryTableColumn"]["queryTable"]["alias"]}.${queryCondition["queryColumn1"]["queryTableColumn"]["columnName"]}"),
                                    TextSpan(text: " ${queryCondition["operation"]} "),
                                    TextSpan(text: "${queryCondition["queryColumn2"]["queryValue"]["value"]}"),
                                    TextSpan(text: " " * 2),
                                    TextSpan(
                                      text: multiLanguageString(name: "edit", defaultValue: "edit", context: context),
                                      style: const TextStyle(color: Colors.green),
                                      recognizer: TapGestureRecognizer()
                                        ..onTap = () {
                                          setState(() {
                                            showEditDialog(queryCondition, "queryCondition", level);
                                          });
                                        },
                                    ),
                                    TextSpan(text: " " * 2),
                                    TextSpan(
                                        text: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                                        style: const TextStyle(color: Colors.green),
                                        recognizer: TapGestureRecognizer()
                                          ..onTap = () {
                                            setState(() {
                                              queryConditionGroup["queryConditionList"].remove(queryCondition);
                                            });
                                          }),
                                  ],
                                ),
                              ),
                            ),
                          if (queryCondition["queryColumn2"]["queryValue"]["parameterName"] != null)
                            Padding(
                              padding: const EdgeInsets.only(top: 5, bottom: 5),
                              child: RichText(
                                text: TextSpan(
                                  style: const TextStyle(color: Colors.black),
                                  children: [
                                    TextSpan(text: " " * number),
                                    TextSpan(
                                        text:
                                            "- ${queryCondition["queryColumn1"]["queryTableColumn"]["queryTable"]["tableName"] ?? queryCondition["queryColumn1"]["queryTableColumn"]["queryTable"]["alias"]}.${queryCondition["queryColumn1"]["queryTableColumn"]["columnName"]}"),
                                    TextSpan(text: " ${queryCondition["operation"]} "),
                                    TextSpan(text: "${queryCondition["queryColumn2"]["queryValue"]["parameterName"]}"),
                                    TextSpan(text: " " * 2),
                                    TextSpan(
                                      text: multiLanguageString(name: "edit", defaultValue: "edit", context: context),
                                      style: const TextStyle(color: Colors.green),
                                      recognizer: TapGestureRecognizer()
                                        ..onTap = () {
                                          setState(() {
                                            showEditDialog(queryCondition, "queryCondition", level);
                                          });
                                        },
                                    ),
                                    TextSpan(text: " " * 2),
                                    TextSpan(
                                        text: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                                        style: const TextStyle(color: Colors.green),
                                        recognizer: TapGestureRecognizer()
                                          ..onTap = () {
                                            setState(() {
                                              queryConditionGroup["queryConditionList"].remove(queryCondition);
                                            });
                                          }),
                                  ],
                                ),
                              ),
                            ),
                        ],
                      ),
                  ],
                ),
              if (queryCondition["queryColumn1"]["queryValue"] != null)
                Column(
                  children: [
                    if (queryCondition["queryColumn1"]["queryValue"]["value"] != null)
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          if (queryCondition["queryColumn2"]["queryTableColumn"] != null)
                            Padding(
                              padding: const EdgeInsets.only(top: 5, bottom: 5),
                              child: RichText(
                                text: TextSpan(style: const TextStyle(color: Colors.black), children: [
                                  TextSpan(text: " " * number),
                                  TextSpan(text: "- ${queryCondition["queryColumn1"]["queryValue"]["value"]}"),
                                  TextSpan(text: " ${queryCondition["operation"]} "),
                                  TextSpan(
                                      text:
                                          "${queryCondition["queryColumn2"]["queryTableColumn"]["queryTable"]["tableName"]}.${queryCondition["queryColumn2"]["queryTableColumn"]["columnName"]}"),
                                  TextSpan(text: " " * 2),
                                  TextSpan(
                                    text: multiLanguageString(name: "edit", defaultValue: "edit", context: context),
                                    style: const TextStyle(color: Colors.green),
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        setState(() {
                                          showEditDialog(queryCondition, "queryCondition", level);
                                        });
                                      },
                                  ),
                                  TextSpan(text: " " * 2),
                                  TextSpan(
                                      text: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                                      style: const TextStyle(color: Colors.green),
                                      recognizer: TapGestureRecognizer()
                                        ..onTap = () {
                                          setState(() {
                                            queryConditionGroup["queryConditionList"].remove(queryCondition);
                                          });
                                        }),
                                ]),
                              ),
                            ),
                          if (queryCondition["queryColumn2"]["queryValue"] != null)
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                if (queryCondition["queryColumn2"]["queryValue"]["value"] != null)
                                  Padding(
                                    padding: const EdgeInsets.only(top: 5, bottom: 5),
                                    child: RichText(
                                      text: TextSpan(
                                        style: const TextStyle(color: Colors.black),
                                        children: [
                                          TextSpan(text: " " * number),
                                          TextSpan(text: "- ${queryCondition["queryColumn1"]["queryValue"]["value"]}"),
                                          TextSpan(text: " ${queryCondition["operation"]} "),
                                          TextSpan(text: "${queryCondition["queryColumn2"]["queryValue"]["value"]}"),
                                          TextSpan(text: " " * 2),
                                          TextSpan(
                                            text: multiLanguageString(name: "edit", defaultValue: "edit", context: context),
                                            style: const TextStyle(color: Colors.green),
                                            recognizer: TapGestureRecognizer()
                                              ..onTap = () {
                                                setState(() {
                                                  showEditDialog(queryCondition, "queryCondition", level);
                                                });
                                              },
                                          ),
                                          TextSpan(text: " " * 2),
                                          TextSpan(
                                              text: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                                              style: const TextStyle(color: Colors.green),
                                              recognizer: TapGestureRecognizer()
                                                ..onTap = () {
                                                  setState(() {
                                                    queryConditionGroup["queryConditionList"].remove(queryCondition);
                                                  });
                                                }),
                                        ],
                                      ),
                                    ),
                                  ),
                                if (queryCondition["queryColumn2"]["queryValue"]["parameterName"] != null)
                                  Padding(
                                    padding: const EdgeInsets.only(top: 5, bottom: 5),
                                    child: RichText(
                                      text: TextSpan(
                                        style: const TextStyle(color: Colors.black),
                                        children: [
                                          TextSpan(text: " " * number),
                                          TextSpan(text: "- ${queryCondition["queryColumn1"]["queryValue"]["value"]}"),
                                          TextSpan(text: " ${queryCondition["operation"]} "),
                                          TextSpan(text: "${queryCondition["queryColumn2"]["queryValue"]["parameterName"]}"),
                                          TextSpan(text: " " * 2),
                                          TextSpan(
                                            text: multiLanguageString(name: "edit", defaultValue: "edit", context: context),
                                            style: const TextStyle(color: Colors.green),
                                            recognizer: TapGestureRecognizer()
                                              ..onTap = () {
                                                setState(() {
                                                  showEditDialog(queryCondition, "queryCondition", level);
                                                });
                                              },
                                          ),
                                          TextSpan(text: " " * 2),
                                          TextSpan(
                                              text: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                                              style: const TextStyle(color: Colors.green),
                                              recognizer: TapGestureRecognizer()
                                                ..onTap = () {
                                                  setState(() {
                                                    queryConditionGroup["queryConditionList"].remove(queryCondition);
                                                  });
                                                }),
                                        ],
                                      ),
                                    ),
                                  ),
                              ],
                            ),
                        ],
                      ),
                    if (queryCondition["queryColumn1"]["queryValue"]["parameterName"] != null)
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          if (queryCondition["queryColumn2"]["queryTableColumn"] != null)
                            Padding(
                              padding: const EdgeInsets.only(top: 5, bottom: 5),
                              child: RichText(
                                text: TextSpan(style: const TextStyle(color: Colors.black), children: [
                                  TextSpan(text: " " * number),
                                  TextSpan(text: "- ${queryCondition["queryColumn1"]["queryValue"]["parameterName"]}"),
                                  TextSpan(text: " ${queryCondition["operation"]} "),
                                  TextSpan(
                                      text:
                                          "${queryCondition["queryColumn2"]["queryTableColumn"]["queryTable"]["tableName"]}.${queryCondition["queryColumn2"]["queryTableColumn"]["columnName"]}"),
                                  TextSpan(text: " " * 2),
                                  TextSpan(
                                    text: multiLanguageString(name: "edit", defaultValue: "edit", context: context),
                                    style: const TextStyle(color: Colors.green),
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        setState(() {
                                          showEditDialog(queryCondition, "queryCondition", level);
                                        });
                                      },
                                  ),
                                  TextSpan(text: " " * 2),
                                  TextSpan(
                                      text: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                                      style: const TextStyle(color: Colors.green),
                                      recognizer: TapGestureRecognizer()
                                        ..onTap = () {
                                          setState(() {
                                            queryConditionGroup["queryConditionList"].remove(queryCondition);
                                          });
                                        }),
                                ]),
                              ),
                            ),
                          if (queryCondition["queryColumn2"]["queryValue"] != null)
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                if (queryCondition["queryColumn2"]["queryValue"]["value"] != null)
                                  Padding(
                                    padding: const EdgeInsets.only(top: 5, bottom: 5),
                                    child: RichText(
                                      text: TextSpan(
                                        style: const TextStyle(color: Colors.black),
                                        children: [
                                          TextSpan(text: " " * number),
                                          TextSpan(text: "- ${queryCondition["queryColumn1"]["queryValue"]["parameterName"]}"),
                                          TextSpan(text: " ${queryCondition["operation"]} "),
                                          TextSpan(text: "${queryCondition["queryColumn2"]["queryValue"]["value"]}"),
                                          TextSpan(text: " " * 2),
                                          TextSpan(
                                            text: multiLanguageString(name: "edit", defaultValue: "edit", context: context),
                                            style: const TextStyle(color: Colors.green),
                                            recognizer: TapGestureRecognizer()
                                              ..onTap = () {
                                                setState(() {
                                                  showEditDialog(queryCondition, "queryCondition", level);
                                                });
                                              },
                                          ),
                                          TextSpan(text: " " * 2),
                                          TextSpan(
                                              text: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                                              style: const TextStyle(color: Colors.green),
                                              recognizer: TapGestureRecognizer()
                                                ..onTap = () {
                                                  setState(() {
                                                    queryConditionGroup["queryConditionList"].remove(queryCondition);
                                                  });
                                                }),
                                        ],
                                      ),
                                    ),
                                  ),
                                if (queryCondition["queryColumn2"]["queryValue"]["parameterName"] != null)
                                  Padding(
                                    padding: const EdgeInsets.only(top: 5, bottom: 5),
                                    child: RichText(
                                      text: TextSpan(
                                        style: const TextStyle(color: Colors.black),
                                        children: [
                                          TextSpan(text: " " * number),
                                          TextSpan(text: "- ${queryCondition["queryColumn1"]["queryValue"]["parameterName"]}"),
                                          TextSpan(text: " ${queryCondition["operation"]} "),
                                          TextSpan(text: "${queryCondition["queryColumn2"]["queryValue"]["parameterName"]}"),
                                          TextSpan(text: " " * 2),
                                          TextSpan(
                                            text: multiLanguageString(name: "edit", defaultValue: "edit", context: context),
                                            style: const TextStyle(color: Colors.green),
                                            recognizer: TapGestureRecognizer()
                                              ..onTap = () {
                                                setState(() {
                                                  showEditDialog(queryCondition, "queryCondition", level);
                                                });
                                              },
                                          ),
                                          TextSpan(text: " " * 2),
                                          TextSpan(
                                              text: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                                              style: const TextStyle(color: Colors.green),
                                              recognizer: TapGestureRecognizer()
                                                ..onTap = () {
                                                  setState(() {
                                                    queryConditionGroup["queryConditionList"].remove(queryCondition);
                                                  });
                                                }),
                                        ],
                                      ),
                                    ),
                                  ),
                              ],
                            ),
                        ],
                      ),
                  ],
                ),
            ],
          ),
      ],
    );
  }
}
