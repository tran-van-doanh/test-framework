import 'package:flutter/material.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/page/insights/rp_query_config/qr_table_column_edit.dart';

class QueryConditionListEditWidget extends StatefulWidget {
  final dynamic queryCondition;
  final List<Map<String, String>> alias;
  final int check;
  final int level;
  final Function callbackQueryCondition;
  const QueryConditionListEditWidget(
      {Key? key, required this.queryCondition, required this.alias, required this.check, required this.level, required this.callbackQueryCondition})
      : super(key: key);

  @override
  State<QueryConditionListEditWidget> createState() => _QueryConditionListEditWidgetState();
}

class _QueryConditionListEditWidgetState extends State<QueryConditionListEditWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
                child: Padding(
                    padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                    child: DynamicDropdownButton(
                        borderRadius: 5,
                        value: widget.queryCondition["operation"],
                        hint: "",
                        labelText: multiLanguageString(name: "operation", defaultValue: "Operation", context: context),
                        items: ["=", ">", "<", ">=", "<=", "<>"].map<DropdownMenuItem<String>>((dynamic result) {
                          return DropdownMenuItem(
                            value: result,
                            child: Text(result),
                          );
                        }).toList(),
                        onChanged: (newValue) async {
                          widget.queryCondition["operation"] = newValue;
                        },
                        isRequiredNotEmpty: true)))
          ],
        ),
        Row(
          children: [
            Expanded(
                child: Padding(
                    padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                    child: DynamicDropdownButton(
                        borderRadius: 5,
                        value: (widget.queryCondition["ignoreNull"].toString() != "null") ? widget.queryCondition["ignoreNull"].toString() : null,
                        labelText: "ignoreNull",
                        items: ["true", "false"].map<DropdownMenuItem<String>>((dynamic result) {
                          return DropdownMenuItem(
                            value: result,
                            child: Text(result),
                          );
                        }).toList(),
                        onChanged: (newValue) async {
                          setState(() {
                            setState(() {
                              if (newValue == "true") {
                                widget.queryCondition["ignoreNull"] = true;
                              } else {
                                widget.queryCondition["ignoreNull"] = false;
                              }
                            });
                          });
                        },
                        isRequiredNotEmpty: true)))
          ],
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      "Query column 1",
                      style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                    ),
                    const SizedBox(height: 10),
                    QueryTableColumnOrValueEditWidget(
                      queryColumn: widget.queryCondition["queryColumn1"],
                      alias: widget.alias,
                      check: widget.check,
                      level: widget.level,
                      type: "condition",
                      callback: (queryColumn1) {
                        widget.queryCondition["queryColumn1"] = queryColumn1;
                        widget.callbackQueryCondition(widget.queryCondition["queryColumn1"]);
                      },
                    )
                  ],
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      "Query column 2",
                      style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                    ),
                    const SizedBox(height: 10),
                    QueryTableColumnOrValueEditWidget(
                      queryColumn: widget.queryCondition["queryColumn2"],
                      alias: widget.alias,
                      check: widget.check,
                      level: widget.level,
                      type: "condition",
                      callback: (queryColumn2) {
                        widget.queryCondition["queryColumn2"] = queryColumn2;
                        widget.callbackQueryCondition(widget.queryCondition["queryColumn2"]);
                      },
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                setState(() {
                  widget.callbackQueryCondition(widget.queryCondition);
                  Navigator.pop(context, true);
                });
              },
              child: const MultiLanguageText(name: "close", defaultValue: "Close"),
            )
          ],
        )
      ],
    );
  }
}
