import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';

class QueryOrderByEditWidget extends StatefulWidget {
  final dynamic queryOrderBy;
  final List<Map<String, String>> alias;
  final int check;
  final int level;
  final Function callback;
  const QueryOrderByEditWidget(
      {Key? key, required this.queryOrderBy, required this.alias, required this.check, required this.level, required this.callback})
      : super(key: key);

  @override
  State<QueryOrderByEditWidget> createState() => _QueryOrderByEditWidgetState();
}

class _QueryOrderByEditWidgetState extends State<QueryOrderByEditWidget> {
  var listAlias = [];
  var listTableName = [];
  var listColumnName = [];
  @override
  void initState() {
    if ((widget.check == 1) || (widget.check == 2 && widget.level == 2) || (widget.check == 3 && widget.level == 3)) {
      getListColumnName(widget.queryOrderBy["queryColumn"]["queryTableColumn"]["queryTable"]["schemaName"],
          widget.queryOrderBy["queryColumn"]["queryTableColumn"]["queryTable"]["tableName"]);
    }
    listAlias = widget.alias;
    super.initState();
  }

  getListColumnName(schemaName, tableName) async {
    var response = await httpGet(
        (widget.queryOrderBy["queryColumn"]["queryTableColumn"]["queryTable"]["schemaName"] != null)
            ? "/test-framework-api/admin/report/db/table/$schemaName/$tableName/column"
            : "/test-framework-api/admin/report/db/table/$tableName/column",
        context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        listColumnName = response["body"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
                child: Padding(
              padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
              child: TextField(
                controller: TextEditingController(text: widget.queryOrderBy["orderByText"] ?? ""),
                onChanged: (value) {
                  widget.queryOrderBy["orderByText"] = value;
                },
                decoration: InputDecoration(
                  label: const MultiLanguageText(name: "order_by_text", defaultValue: "Order by text"),
                  hintText: multiLanguageString(name: "order_by_text", defaultValue: "Order by text", context: context),
                  border: const OutlineInputBorder(),
                ),
              ),
            ))
          ],
        ),
        Row(
          children: [
            Expanded(
                child: Padding(
              padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
              child: DynamicDropdownButton(
                  borderRadius: 5,
                  value: widget.queryOrderBy["queryColumn"]["queryTableColumn"]["queryTable"]["alias"],
                  labelText: multiLanguageString(name: "alias", defaultValue: "Alias", context: context),
                  items: listAlias.map<DropdownMenuItem<String>>((dynamic result) {
                    return DropdownMenuItem(
                      value: result["alias"],
                      child: Text(result["alias"]),
                    );
                  }).toList(),
                  onChanged: (newValue) async {
                    String? tableName = listAlias.firstWhere((element) => element["alias"] == newValue)["tableName"];
                    if (widget.queryOrderBy["queryColumn"]["queryTableColumn"]["queryTable"]["alias"] != newValue) {
                      setState(() {
                        widget.queryOrderBy["queryColumn"]["queryTableColumn"]["queryTable"]["alias"] = newValue;
                        widget.queryOrderBy["queryColumn"]["queryTableColumn"]["queryTable"]["tableName"] = tableName;
                        widget.queryOrderBy["queryColumn"]["queryTableColumn"]["columnName"] = null;
                        getListColumnName(widget.queryOrderBy["queryColumn"]["queryTableColumn"]["queryTable"]["schemaName"],
                            widget.queryOrderBy["queryColumn"]["queryTableColumn"]["queryTable"]["tableName"]);
                      });
                    }
                  },
                  isRequiredNotEmpty: true),
            ))
          ],
        ),
        ((widget.check == 1) || (widget.check == 2 && widget.level == 2) || (widget.check == 3 && widget.level == 3))
            ? Row(
                children: [
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                    child: DynamicDropdownSearchClearOption(
                      labelText: multiLanguageString(name: "column_name", defaultValue: "Column name", context: context),
                      controller: SingleValueDropDownController(
                          data: DropDownValueModel(
                              name: widget.queryOrderBy["queryColumn"]["queryTableColumn"]["columnName"] ?? "",
                              value: widget.queryOrderBy["queryColumn"]["queryTableColumn"]["columnName"])),
                      items: listColumnName.map<DropDownValueModel>((dynamic result) {
                        return DropDownValueModel(
                          value: result["columnName"],
                          name: result["columnName"],
                        );
                      }).toList(),
                      maxHeight: 200,
                      onChanged: (dynamic newValue) {
                        setState(() {
                          widget.queryOrderBy["queryColumn"]["queryTableColumn"]["columnName"] = newValue.value;
                        });
                      },
                    ),
                  ))
                ],
              )
            : Row(
                children: [
                  Expanded(
                      child: Padding(
                          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                          child: TextField(
                            controller: TextEditingController(text: widget.queryOrderBy["queryColumn"]["queryTableColumn"]["columnName"]),
                            onChanged: (value) {
                              widget.queryOrderBy["queryColumn"]["queryTableColumn"]["columnName"] = value;
                            },
                            decoration: InputDecoration(
                              label: const MultiLanguageText(name: "column_name", defaultValue: "Column name"),
                              hintText: multiLanguageString(name: "column_name", defaultValue: "Column name", context: context),
                              border: const OutlineInputBorder(),
                            ),
                          )))
                ],
              ),
        Row(
          children: [
            Expanded(
                child: Padding(
                    padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                    child: DynamicDropdownButton(
                        borderRadius: 5,
                        value: widget.queryOrderBy["orderDirection"],
                        hint: "",
                        labelText: multiLanguageString(name: "order_direction", defaultValue: "Order direction", context: context),
                        items: ["ASC", "DESC"].map<DropdownMenuItem<String>>((dynamic result) {
                          return DropdownMenuItem(
                            value: result,
                            child: Text(result),
                          );
                        }).toList(),
                        onChanged: (newValue) async {
                          widget.queryOrderBy["orderDirection"] = newValue;
                        },
                        isRequiredNotEmpty: true)))
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                setState(() {
                  widget.callback(widget.queryOrderBy);
                  Navigator.pop(context);
                });
              },
              child: const MultiLanguageText(name: "close", defaultValue: "Close"),
            )
          ],
        ),
      ],
    );
  }
}
