import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';

class QueryGroupByEditWidget extends StatefulWidget {
  final dynamic queryGroupBy;
  final List<Map<String, String>> alias;
  final int check;
  final int level;
  final Function callbackGroupBy;
  const QueryGroupByEditWidget(
      {Key? key, required this.queryGroupBy, required this.alias, required this.check, required this.level, required this.callbackGroupBy})
      : super(key: key);

  @override
  State<QueryGroupByEditWidget> createState() => _QueryGroupByEditWidgetState();
}

class _QueryGroupByEditWidgetState extends State<QueryGroupByEditWidget> {
  var listAlias = [];
  var listColumnName = [];
  @override
  void initState() {
    if ((widget.check == 1) || (widget.check == 2 && widget.level == 2) || (widget.check == 3 && widget.level == 3)) {
      getListColumnName(
          widget.queryGroupBy["queryTableColumn"]["queryTable"]["schemaName"], widget.queryGroupBy["queryTableColumn"]["queryTable"]["tableName"]);
    }
    listAlias = widget.alias;
    super.initState();
  }

  getListColumnName(schemaName, tableName) async {
    var response = await httpGet(
        (widget.queryGroupBy["queryTableColumn"]["queryTable"]["schemaName"] != null)
            ? "/test-framework-api/admin/report/db/table/$schemaName/$tableName/column"
            : "/test-framework-api/admin/report/db/table/$tableName/column",
        context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        listColumnName = response["body"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
                child: Padding(
              padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
              child: DynamicDropdownButton(
                  borderRadius: 5,
                  value: widget.queryGroupBy["queryTableColumn"]["queryTable"]["alias"],
                  hint: "",
                  labelText: multiLanguageString(name: "alias", defaultValue: "Alias", context: context),
                  items: listAlias.map<DropdownMenuItem<String>>((dynamic result) {
                    return DropdownMenuItem(
                      value: result["alias"],
                      child: Text(result["alias"]),
                    );
                  }).toList(),
                  onChanged: (newValue) async {
                    String? tableName = listAlias.firstWhere((element) => element["alias"] == newValue)["tableName"];
                    if (widget.queryGroupBy["queryTableColumn"]["queryTable"]["alias"] != newValue) {
                      setState(() {
                        widget.queryGroupBy["queryTableColumn"]["queryTable"]["alias"] = newValue;
                        widget.queryGroupBy["queryTableColumn"]["queryTable"]["tableName"] = tableName;
                        widget.queryGroupBy["queryTableColumn"]["columnName"] = null;
                        getListColumnName(widget.queryGroupBy["queryTableColumn"]["queryTable"]["schemaName"],
                            widget.queryGroupBy["queryTableColumn"]["queryTable"]["tableName"]);
                      });
                    }
                  },
                  isRequiredNotEmpty: true),
            ))
          ],
        ),
        ((widget.check == 1) || (widget.check == 2 && widget.level == 2) || (widget.check == 3 && widget.level == 3))
            ? Row(
                children: [
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                    child: DynamicDropdownSearchClearOption(
                      labelText: multiLanguageString(name: "column_name", defaultValue: "Column name", context: context),
                      controller: SingleValueDropDownController(
                          data: DropDownValueModel(
                              name: widget.queryGroupBy["queryTableColumn"]["columnName"] ?? "",
                              value: widget.queryGroupBy["queryTableColumn"]["columnName"])),
                      items: listColumnName.map<DropDownValueModel>((dynamic result) {
                        return DropDownValueModel(
                          value: result["columnName"],
                          name: result["columnName"],
                        );
                      }).toList(),
                      maxHeight: 200,
                      onChanged: (dynamic newValue) {
                        setState(() {
                          widget.queryGroupBy["queryTableColumn"]["columnName"] = newValue.value;
                        });
                      },
                    ),
                  ))
                ],
              )
            : Row(
                children: [
                  Expanded(
                      child: Padding(
                          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                          child: TextField(
                            controller: TextEditingController(text: widget.queryGroupBy["queryTableColumn"]["columnName"]),
                            onChanged: (value) {
                              widget.queryGroupBy["queryTableColumn"]["columnName"] = value;
                            },
                            decoration: InputDecoration(
                              label: const MultiLanguageText(name: "column_name", defaultValue: "Column name"),
                              hintText: multiLanguageString(name: "column_name", defaultValue: "Column name", context: context),
                              border: const OutlineInputBorder(),
                            ),
                          )))
                ],
              ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                setState(() {
                  widget.callbackGroupBy(widget.queryGroupBy);
                  Navigator.pop(context);
                });
              },
              child: const MultiLanguageText(name: "close", defaultValue: "Close"),
            )
          ],
        ),
      ],
    );
  }
}
