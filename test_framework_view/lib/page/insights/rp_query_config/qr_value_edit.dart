import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';

class QueryTableColumn extends StatefulWidget {
  final dynamic queryTableColumn;
  final List<Map<String, String>> alias;
  final int check;
  final int level;
  const QueryTableColumn({Key? key, required this.queryTableColumn, required this.alias, required this.check, required this.level}) : super(key: key);

  @override
  State<QueryTableColumn> createState() => _QueryTableColumnState();
}

class _QueryTableColumnState extends State<QueryTableColumn> {
  var listAlias = [];
  var listColumnName = [];

  @override
  initState() {
    if ((widget.check == 1) || (widget.check == 2 && widget.level == 2) || (widget.check == 3 && widget.level == 3)) {
      getListColumnName(widget.queryTableColumn["queryTable"]["schemaName"], widget.queryTableColumn["queryTable"]["tableName"]);
    }
    listAlias = widget.alias;
    super.initState();
  }

  getListColumnName(schemaName, tableName) async {
    var response = await httpGet(
        (widget.queryTableColumn["queryTable"]["schemaName"] != null)
            ? "/test-framework-api/admin/report/db/table/$schemaName/$tableName/column"
            : "/test-framework-api/admin/report/db/table/$tableName/column",
        context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        listColumnName = response["body"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
                child: Padding(
              padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
              child: DynamicDropdownButton(
                  borderRadius: 5,
                  value: widget.queryTableColumn["queryTable"]["alias"],
                  hint: "",
                  labelText: multiLanguageString(name: "alias", defaultValue: "Alias", context: context),
                  items: listAlias.map<DropdownMenuItem<String>>((dynamic result) {
                    return DropdownMenuItem(
                      value: result["alias"],
                      child: Text(result["alias"]),
                    );
                  }).toList(),
                  onChanged: (newValue) async {
                    String? tableName = listAlias.firstWhere((element) => element["alias"] == newValue)["tableName"];
                    if (widget.queryTableColumn["queryTable"]["alias"] != newValue) {
                      setState(() {
                        widget.queryTableColumn["queryTable"]["alias"] = newValue;
                        widget.queryTableColumn["queryTable"]["tableName"] = tableName;
                        getListColumnName(widget.queryTableColumn["queryTable"]["schemaName"], widget.queryTableColumn["queryTable"]["tableName"]);
                        widget.queryTableColumn["columnName"] = null;
                      });
                    }
                  },
                  isRequiredNotEmpty: true),
            ))
          ],
        ),
        ((widget.check == 1) || (widget.check == 2 && widget.level == 2) || (widget.check == 3 && widget.level == 3))
            ? Row(
                children: [
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                    child: DynamicDropdownSearchClearOption(
                      labelText: multiLanguageString(name: "column_name", defaultValue: "Column name", context: context),
                      controller: SingleValueDropDownController(
                          data: DropDownValueModel(name: widget.queryTableColumn["columnName"] ?? "", value: widget.queryTableColumn["columnName"])),
                      items: listColumnName.map<DropDownValueModel>((dynamic result) {
                        return DropDownValueModel(
                          value: result["columnName"],
                          name: result["columnName"],
                        );
                      }).toList(),
                      maxHeight: 200,
                      onChanged: (dynamic newValue) {
                        setState(() {
                          widget.queryTableColumn["columnName"] = newValue.value;
                        });
                      },
                    ),
                  ))
                ],
              )
            : Row(
                children: [
                  Expanded(
                      child: Padding(
                          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                          child: TextField(
                            controller: TextEditingController(text: widget.queryTableColumn["columnName"]),
                            onChanged: (value) {
                              widget.queryTableColumn["columnName"] = value;
                            },
                            decoration: InputDecoration(
                              label: const MultiLanguageText(name: "column_name", defaultValue: "Column name"),
                              hintText: multiLanguageString(name: "column_name", defaultValue: "Column name", context: context),
                              border: const OutlineInputBorder(),
                            ),
                          )))
                ],
              ),
      ],
    );
  }
}

class QueryValueColumn extends StatefulWidget {
  final dynamic query;
  const QueryValueColumn({Key? key, required this.query}) : super(key: key);

  @override
  State<QueryValueColumn> createState() => _QueryValueColumnState();
}

class _QueryValueColumnState extends State<QueryValueColumn> {
  String type = "";
  @override
  void initState() {
    type = (widget.query["queryValue"]["value"] != null) ? "value" : "parameterName";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
                child: Padding(
                    padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                    child: DynamicDropdownButton(
                        borderRadius: 5,
                        value: type,
                        labelText: "Type",
                        items: ["value", "parameterName"].map<DropdownMenuItem<String>>((dynamic result) {
                          return DropdownMenuItem(
                            value: result,
                            child: Text(result),
                          );
                        }).toList(),
                        onChanged: (newValue) async {
                          String? value = "";
                          String? isList;
                          String? valueType;
                          String? ignoreNull;
                          bool isNull = false;
                          String? parameterName = "";
                          setState(() {
                            if (type != newValue) {
                              setState(() {
                                type = newValue;
                                if (type == "parameterName") {
                                  widget.query["queryValue"].remove("value");
                                  widget.query["queryValue"].remove("isNull");
                                  widget.query["queryValue"] = {
                                    "isList": isList,
                                    "valueType": valueType,
                                    "ignoreNull": ignoreNull,
                                    "parameterName": parameterName
                                  };
                                }
                                if (type == "value") {
                                  widget.query["queryValue"].remove("parameterName");
                                  widget.query["queryValue"] = {
                                    "value": value,
                                    "isList": isList,
                                    "isNull": isNull,
                                    "valueType": valueType,
                                    "ignoreNull": ignoreNull,
                                  };
                                }
                              });
                            }
                          });
                        },
                        isRequiredNotEmpty: true)))
          ],
        ),
        (widget.query["queryValue"]["value"] != null)
            ? Row(
                children: [
                  Expanded(
                      child: Padding(
                          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                          child: TextField(
                            controller: TextEditingController(text: widget.query["queryValue"]["value"].toString()),
                            onChanged: (value) {
                              widget.query["queryValue"]["value"] = value;
                            },
                            decoration: InputDecoration(
                              label: const MultiLanguageText(name: "value", defaultValue: "Value"),
                              hintText: multiLanguageString(name: "value", defaultValue: "Value", context: context),
                              border: const OutlineInputBorder(),
                            ),
                          )))
                ],
              )
            : Container(),
        Row(
          children: [
            Expanded(
                child: Padding(
              padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
              child: DynamicDropdownButton(
                  borderRadius: 5,
                  value: (widget.query["queryValue"]["isList"].toString() != "null") ? widget.query["queryValue"]["isList"].toString() : null,
                  labelText: "isList",
                  items: ["true", "false"].map<DropdownMenuItem<String>>((dynamic result) {
                    return DropdownMenuItem(
                      value: result,
                      child: Text(result),
                    );
                  }).toList(),
                  onChanged: (newValue) async {
                    setState(() {
                      setState(() {
                        if (newValue == "true") {
                          widget.query["queryValue"]["isList"] = true;
                        } else {
                          widget.query["queryValue"]["isList"] = false;
                        }
                      });
                    });
                  },
                  isRequiredNotEmpty: true),
            ))
          ],
        ),
        (widget.query["queryValue"]["isNull"] != null)
            ? Row(
                children: [
                  Expanded(
                      child: Padding(
                          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                          child: DynamicDropdownButton(
                              borderRadius: 5,
                              value: (widget.query["queryValue"]["isNull"].toString() != "null")
                                  ? widget.query["queryValue"]["isNull"].toString()
                                  : null,
                              labelText: "isNull",
                              items: ["true", "false"].map<DropdownMenuItem<String>>((dynamic result) {
                                return DropdownMenuItem(
                                  value: result,
                                  child: Text(result),
                                );
                              }).toList(),
                              onChanged: (newValue) async {
                                setState(() {
                                  setState(() {
                                    if (newValue == "true") {
                                      widget.query["queryValue"]["isNull"] = true;
                                    } else {
                                      widget.query["queryValue"]["isNull"] = false;
                                    }
                                  });
                                });
                              },
                              isRequiredNotEmpty: true)))
                ],
              )
            : Container(),
        Row(
          children: [
            Expanded(
                child: Padding(
                    padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                    child: TextField(
                      controller: TextEditingController(text: widget.query["queryValue"]["valueType"] ?? ""),
                      onChanged: (value) {
                        widget.query["queryValue"]["valueType"] = value;
                      },
                      decoration: InputDecoration(
                        label: const MultiLanguageText(name: "value_type", defaultValue: "Value type"),
                        hintText: multiLanguageString(name: "value_type", defaultValue: "Value type", context: context),
                        border: const OutlineInputBorder(),
                      ),
                    )))
          ],
        ),
        Row(
          children: [
            Expanded(
                child: Padding(
                    padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                    child: DynamicDropdownButton(
                        borderRadius: 5,
                        value: (widget.query["queryValue"]["ignoreNull"].toString() != "null")
                            ? widget.query["queryValue"]["ignoreNull"].toString()
                            : null,
                        labelText: "ignoreNull",
                        items: ["true", "false"].map<DropdownMenuItem<String>>((dynamic result) {
                          return DropdownMenuItem(
                            value: result,
                            child: Text(result),
                          );
                        }).toList(),
                        onChanged: (newValue) async {
                          setState(() {
                            setState(() {
                              if (newValue == "true") {
                                widget.query["queryValue"]["ignoreNull"] = true;
                              } else {
                                widget.query["queryValue"]["ignoreNull"] = false;
                              }
                            });
                          });
                        },
                        isRequiredNotEmpty: true)))
          ],
        ),
        (widget.query["queryValue"]["parameterName"] != null)
            ? Row(
                children: [
                  Expanded(
                      child: Padding(
                          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                          child: TextField(
                            controller: TextEditingController(text: widget.query["queryValue"]["parameterName"] ?? ""),
                            onChanged: (value) {
                              widget.query["queryValue"]["parameterName"] = value;
                            },
                            decoration: InputDecoration(
                              label: const MultiLanguageText(name: "parameter_name", defaultValue: "Parameter name"),
                              hintText: multiLanguageString(name: "parameter_name", defaultValue: "Parameter name", context: context),
                              border: const OutlineInputBorder(),
                            ),
                          )))
                ],
              )
            : Container(),
      ],
    );
  }
}
