import 'package:flutter/material.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/page/insights/rp_query_config/qr_value_edit.dart';

class QueryColumnListEditWidget extends StatefulWidget {
  final dynamic queryColumn;
  final List<Map<String, String>> alias;
  final int check;
  final int level;
  final Function callbackColumn;
  const QueryColumnListEditWidget(
      {Key? key, required this.queryColumn, required this.alias, required this.check, required this.level, required this.callbackColumn})
      : super(key: key);

  @override
  State<QueryColumnListEditWidget> createState() => _QueryColumnListEditState();
}

class _QueryColumnListEditState extends State<QueryColumnListEditWidget> {
  String sumType = "";
  @override
  void initState() {
    if (widget.queryColumn["queryCase"] != null) {
      sumType = "caseWhen";
    }
    if (widget.queryColumn["queryColumn"] != null) {
      sumType = "Column";
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (widget.queryColumn["columnType"] == "TruncDate")
          Row(
            children: [
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                child: TextField(
                  controller: TextEditingController(text: widget.queryColumn["format"] ?? ""),
                  onChanged: (value) {
                    widget.queryColumn["format"] = value;
                  },
                  decoration: InputDecoration(
                    label: const MultiLanguageText(name: "format", defaultValue: "Format"),
                    hintText: multiLanguageString(name: "format", defaultValue: "Format", context: context),
                    border: const OutlineInputBorder(),
                  ),
                ),
              ))
            ],
          ),
        Row(
          children: [
            Expanded(
                child: Padding(
                    padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                    child: DynamicDropdownButton(
                        borderRadius: 5,
                        value: (widget.queryColumn["columnType"].toString() != "null") ? widget.queryColumn["columnType"].toString() : null,
                        labelText: "Select column type",
                        items: ["CountOne", "TruncDate", "Column", "Sum", "Case", "Count", "Avg", "Min", "Max"]
                            .map<DropdownMenuItem<String>>((dynamic result) {
                          return DropdownMenuItem(
                            value: result,
                            child: Text(result),
                          );
                        }).toList(),
                        onChanged: (newValue) async {
                          setState(() {
                            if (widget.queryColumn["columnType"] != newValue) {
                              setState(() {
                                String? format;
                                String? columnType;
                                String? columnAlias;
                                String? operation;
                                bool? ignoreNull;
                                String? columnName;
                                String value = "";
                                bool? isList;
                                bool isNull = false;
                                String? valueType;
                                String? groupType;
                                widget.queryColumn["columnType"] = newValue;
                                if (widget.queryColumn["columnType"] == "Sum" || widget.queryColumn["columnType"] == "Case") {
                                  widget.queryColumn.remove("queryTableColumn");
                                  widget.queryColumn["queryCase"] = {
                                    "queryColumnElse": {
                                      "format": format,
                                      "columnType": "Value",
                                      "columnAlias": columnAlias,
                                      "queryValue": {
                                        "value": value,
                                        "isList": isList,
                                        "isNull": isNull,
                                        "valueType": valueType,
                                        "ignoreNull": ignoreNull
                                      }
                                    },
                                    "queryCaseWhenList": [
                                      {
                                        "queryColumnThen": {
                                          "format": format,
                                          "columnType": "Value",
                                          "queryValue": {
                                            "value": value,
                                            "isList": isList,
                                            "isNull": isNull,
                                            "valueType": valueType,
                                            "ignoreNull": ignoreNull
                                          }
                                        },
                                        "queryConditionGroup": {
                                          "groupType": groupType,
                                          "queryConditionList": [
                                            {
                                              "operation": operation,
                                              "ignoreNull": ignoreNull,
                                              "queryColumn1": {
                                                "format": format,
                                                "columnType": columnType,
                                                "columnAlias": columnAlias,
                                                "queryTableColumn": {"columnName": columnName, "queryTable": {}}
                                              },
                                              "queryColumn2": {
                                                "format": format,
                                                "columnType": "Value",
                                                "columnAlias": columnAlias,
                                                "queryValue": {
                                                  "value": "",
                                                  "isList": isList,
                                                  "isNull": isNull,
                                                  "valueType": valueType,
                                                  "ignoreNull": ignoreNull
                                                }
                                              }
                                            }
                                          ]
                                        }
                                      }
                                    ],
                                  };
                                }
                                if (widget.queryColumn["columnType"] == "Column" ||
                                    widget.queryColumn["columnType"] == "Avg" ||
                                    widget.queryColumn["columnType"] == "Min" ||
                                    widget.queryColumn["columnType"] == "Max" ||
                                    widget.queryColumn["columnType"] == "Count" ||
                                    widget.queryColumn["columnType"] == "TruncDate") {
                                  widget.queryColumn.remove("queryCase");
                                  widget.queryColumn["queryTableColumn"] = {"columnName": columnName, "queryTable": {}};
                                }
                                if (widget.queryColumn["columnType"] == "CountOne") {
                                  widget.queryColumn.remove("format");
                                  widget.queryColumn.remove("columnAlias");
                                  widget.queryColumn.remove("queryTableColumn");
                                }
                              });
                            }
                            if (widget.queryColumn["queryCase"] != null) {
                              sumType = "caseWhen";
                            }
                            if (widget.queryColumn["queryColumn"] != null) {
                              sumType = "Column";
                            }
                          });
                        },
                        isRequiredNotEmpty: true)))
          ],
        ),
        if (widget.queryColumn["columnType"] == "Sum")
          Column(
            children: [
              Row(
                children: [
                  Expanded(
                      child: Padding(
                          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                          child: DynamicDropdownButton(
                              borderRadius: 5,
                              value: sumType,
                              labelText: "sumType",
                              items: ["Column", "caseWhen"].map<DropdownMenuItem<String>>((dynamic result) {
                                return DropdownMenuItem(
                                  value: result,
                                  child: Text(result),
                                );
                              }).toList(),
                              onChanged: (newValue) async {
                                String? format;
                                String? columnAlias;
                                String? columnName;
                                String? value = "";
                                String? isList;
                                bool isNull = false;
                                String? valueType;
                                String? ignoreNull;
                                setState(() {
                                  if (sumType != newValue) {
                                    setState(() {
                                      sumType = newValue;
                                      if (sumType == "Column") {
                                        widget.queryColumn.remove("queryCase");
                                        widget.queryColumn["queryColumn"] = {
                                          "format": format,
                                          "columnType": "Column",
                                          "columnAlias": columnAlias,
                                          "queryTableColumn": {"columnName": columnName, "queryTable": {}}
                                        };
                                      }
                                      if (sumType == "caseWhen") {
                                        widget.queryColumn.remove("queryColumn");
                                        widget.queryColumn["queryCase"] = {
                                          "queryColumnElse": {
                                            "format": format,
                                            "columnType": "Value",
                                            "queryValue": {
                                              "value": value,
                                              "isList": isList,
                                              "isNull": isNull,
                                              "valueType": valueType,
                                              "ignoreNull": ignoreNull
                                            },
                                            "columnAlias": columnAlias
                                          },
                                          "queryCaseWhenList": [
                                            {
                                              "queryColumnThen": {
                                                "format": format,
                                                "columnType": "Value",
                                                "queryValue": {
                                                  "value": value,
                                                  "isList": isList,
                                                  "isNull": isNull,
                                                  "valueType": valueType,
                                                  "ignoreNull": ignoreNull
                                                },
                                                "columnAlias": columnAlias
                                              },
                                              "queryConditionGroup": {}
                                            }
                                          ]
                                        };
                                      }
                                    });
                                  }
                                });
                              },
                              isRequiredNotEmpty: true)))
                ],
              ),
              if (sumType == "Column")
                Padding(
                  padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Query column",
                        style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                      ),
                      Row(
                        children: [
                          Expanded(
                              child: Padding(
                                  padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                                  child: TextField(
                                    controller: TextEditingController(text: widget.queryColumn["queryColumn"]["format"] ?? ""),
                                    onChanged: (value) {
                                      widget.queryColumn["queryColumn"]["format"] = value;
                                    },
                                    decoration: InputDecoration(
                                      label: const MultiLanguageText(name: "format", defaultValue: "Format"),
                                      hintText: multiLanguageString(name: "format", defaultValue: "Format", context: context),
                                      border: const OutlineInputBorder(),
                                    ),
                                  )))
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                              child: Padding(
                                  padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                                  child: DynamicDropdownButton(
                                      borderRadius: 5,
                                      value: (widget.queryColumn["queryColumn"]["columnType"].toString() != "null")
                                          ? widget.queryColumn["queryColumn"]["columnType"].toString()
                                          : null,
                                      labelText: "columnType",
                                      items: ["Column", "Value"].map<DropdownMenuItem<String>>((dynamic result) {
                                        return DropdownMenuItem(
                                          value: result,
                                          child: Text(result),
                                        );
                                      }).toList(),
                                      onChanged: (newValue) async {
                                        String? columnName;
                                        String? value = "";
                                        String? isList;
                                        bool isNull = false;
                                        String? valueType;
                                        String? ignoreNull;
                                        setState(() {
                                          if (widget.queryColumn["queryColumn"]["columnType"] != newValue) {
                                            setState(() {
                                              widget.queryColumn["queryColumn"]["columnType"] = newValue;
                                              if (widget.queryColumn["queryColumn"]["columnType"] == "Column") {
                                                widget.queryColumn["queryColumn"].remove("queryValue");
                                                widget.queryColumn["queryColumn"]["queryTableColumn"] = {"columnName": columnName, "queryTable": {}};
                                              }
                                              if (widget.queryColumn["queryColumn"]["columnType"] == "Value") {
                                                widget.queryColumn["queryColumn"].remove("queryTableColumn");
                                                widget.queryColumn["queryColumn"]["queryValue"] = {
                                                  "value": value,
                                                  "isList": isList,
                                                  "isNull": isNull,
                                                  "valueType": valueType,
                                                  "ignoreNull": ignoreNull
                                                };
                                              }
                                            });
                                          }
                                        });
                                      },
                                      isRequiredNotEmpty: true)))
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                              child: Padding(
                                  padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                                  child: TextField(
                                    controller: TextEditingController(text: widget.queryColumn["queryColumn"]["columnAlias"] ?? ""),
                                    onChanged: (value) {
                                      widget.queryColumn["queryColumn"]["columnAlias"] = value;
                                    },
                                    decoration: InputDecoration(
                                      label: const MultiLanguageText(name: "column_alias", defaultValue: "Column alias"),
                                      hintText: multiLanguageString(name: "column_alias", defaultValue: "Column alias", context: context),
                                      border: const OutlineInputBorder(),
                                    ),
                                  )))
                        ],
                      ),
                      if (widget.queryColumn["queryColumn"]["columnType"] == "Column")
                        Padding(
                          padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                "Query table column",
                                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                              ),
                              const SizedBox(height: 10),
                              QueryTableColumn(
                                  queryTableColumn: widget.queryColumn["queryColumn"]["queryTableColumn"],
                                  alias: widget.alias,
                                  check: widget.check,
                                  level: widget.level),
                            ],
                          ),
                        ),
                      if (widget.queryColumn["queryColumn"]["columnType"] == "Value")
                        Padding(
                          padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                "Query value",
                                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                              ),
                              const SizedBox(height: 10),
                              QueryValueColumn(query: widget.queryColumn["queryColumn"]),
                            ],
                          ),
                        ),
                    ],
                  ),
                ),
            ],
          ),
        if (widget.queryColumn["columnType"] == "Column" ||
            widget.queryColumn["columnType"] == "Avg" ||
            widget.queryColumn["columnType"] == "Min" ||
            widget.queryColumn["columnType"] == "Max" ||
            widget.queryColumn["columnType"] == "Count" ||
            widget.queryColumn["columnType"] == "TruncDate")
          Padding(
            padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  "Query table column",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                ),
                const SizedBox(height: 10),
                QueryTableColumn(
                    queryTableColumn: widget.queryColumn["queryTableColumn"], alias: widget.alias, check: widget.check, level: widget.level),
              ],
            ),
          ),
        Row(
          children: [
            Expanded(
                child: Padding(
              padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
              child: TextField(
                controller: TextEditingController(text: widget.queryColumn["columnAlias"] ?? ""),
                onChanged: (value) {
                  widget.queryColumn["columnAlias"] = value;
                },
                decoration: InputDecoration(
                  label: const MultiLanguageText(name: "column_alias", defaultValue: "Column alias"),
                  hintText: multiLanguageString(name: "column_alias", defaultValue: "Column alias", context: context),
                  border: const OutlineInputBorder(),
                ),
              ),
            ))
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                setState(() {
                  widget.callbackColumn(widget.queryColumn);
                  Navigator.pop(context);
                });
              },
              child: const MultiLanguageText(name: "close", defaultValue: "Close"),
            )
          ],
        ),
      ],
    );
  }
}
