import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';

class QueryFromListEditWidget extends StatefulWidget {
  final dynamic queryFrom;
  final Function updateAlias;
  final Function onClose;
  const QueryFromListEditWidget({Key? key, required this.queryFrom, required this.updateAlias, required this.onClose}) : super(key: key);

  @override
  State<QueryFromListEditWidget> createState() => _QueryFromListEditWidgetState();
}

class _QueryFromListEditWidgetState extends State<QueryFromListEditWidget> {
  String? alias;
  String? tableName;
  var listSchema = [];
  var listTable = [];
  var listTableWithoutSchema = [];
  String? _queryFromType;

  @override
  initState() {
    getInitPage();
    super.initState();
  }

  getInitPage() {
    setState(() {
      if (widget.queryFrom["queryTable"] != null && widget.queryFrom["queryTable"].containsKey("querySelect")) {
        _queryFromType = "querySelect";
      } else if (widget.queryFrom["queryJoin"] != null) {
        _queryFromType = "queryJoin";
      } else {
        _queryFromType = "queryTable";
      }
    });
    getListSchema();
    getListTableWithoutSchema();
  }

  getListSchema() async {
    var response = await httpGet("/test-framework-api/admin/report/db/schema", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        listSchema = response["body"];
      });
    }
  }

  getListTable(schemaName) async {
    var response = await httpGet("/test-framework-api/admin/report/db/schema/$schemaName/table", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        listTable = response["body"];
      });
    }
  }

  getListTableWithoutSchema() async {
    var response = await httpGet("/test-framework-api/admin/report/db/table", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        listTableWithoutSchema = response["body"];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
                child: Padding(
                    padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                    child: DynamicDropdownButton(
                      value: _queryFromType,
                      labelText: multiLanguageString(name: "select_a_type", defaultValue: "Select a type", context: context),
                      items: [
                        {"label": multiLanguageString(name: "query_table", defaultValue: "queryTable", context: context), "value": "queryTable"},
                        {"label": multiLanguageString(name: "query_join", defaultValue: "queryJoin", context: context), "value": "queryJoin"},
                        {"label": multiLanguageString(name: "query_select", defaultValue: "querySelect", context: context), "value": "querySelect"}
                      ].map<DropdownMenuItem<String>>((dynamic result) {
                        return DropdownMenuItem(
                          value: result["value"],
                          child: Text(result["label"]),
                        );
                      }).toList(),
                      onChanged: (newValue) {
                        if (_queryFromType != newValue) {
                          setState(() {
                            _queryFromType = newValue;
                            String? joinType;
                            if (_queryFromType == "queryTable") {
                              widget.queryFrom.remove("queryJoin");
                              widget.queryFrom["queryTable"] = {};
                            }
                            if (_queryFromType == "queryJoin") {
                              widget.queryFrom.remove("queryTable");
                              widget.queryFrom["queryJoin"] = {"joinType": joinType, "queryTable": {}, "queryConditionGroup": {}};
                            }
                            if (_queryFromType == "querySelect") {
                              widget.queryFrom.remove("queryJoin");
                              widget.queryFrom["queryTable"]["querySelect"] = {
                                "queryFromList": [],
                                "queryColumnList": [],
                                "queryConditionGroup": {},
                                "queryGroupByList": [],
                                "queryOrderByList": []
                              };
                            }
                          });
                        }
                      },
                      borderRadius: 5,
                      isRequiredNotEmpty: true,
                    )))
          ],
        ),
        if (_queryFromType == "queryTable")
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                    child: DynamicDropdownButton(
                        borderRadius: 5,
                        value: widget.queryFrom["queryTable"]["schemaName"],
                        labelText: multiLanguageString(name: "schema_name", defaultValue: "Schema name", context: context),
                        items: <DropdownMenuItem<String>>[
                          const DropdownMenuItem(
                            value: null,
                            child: Text("None"),
                          ),
                          ...listSchema.map<DropdownMenuItem<String>>((dynamic result) {
                            return DropdownMenuItem(
                              value: result,
                              child: Text(result),
                            );
                          }),
                        ],
                        onChanged: (newValue) async {
                          if (widget.queryFrom["queryTable"]["schemaName"] != newValue) {
                            setState(() {
                              widget.queryFrom["queryTable"]["schemaName"] = newValue;
                              widget.queryFrom["queryTable"]["tableName"] = null;
                              getListTable(widget.queryFrom["queryTable"]["schemaName"]);
                            });
                          }
                        },
                        isRequiredNotEmpty: true),
                  ))
                ],
              ),
              (widget.queryFrom["queryTable"]["schemaName"] != null)
                  ? Row(
                      children: [
                        Expanded(
                            child: Padding(
                          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                          child: DynamicDropdownSearchClearOption(
                            labelText: multiLanguageString(name: "table_name", defaultValue: "Table name", context: context),
                            controller: SingleValueDropDownController(
                                data: DropDownValueModel(
                                    name: widget.queryFrom["queryTable"]["tableName"] ?? "", value: widget.queryFrom["queryTable"]["tableName"])),
                            items: listTable.map<DropDownValueModel>((dynamic result) {
                              return DropDownValueModel(
                                value: result["tableName"],
                                name: result["tableName"],
                              );
                            }).toList(),
                            maxHeight: 200,
                            onChanged: (dynamic newValue) {
                              setState(() {
                                widget.queryFrom["queryTable"]["tableName"] = newValue.value;
                                widget.queryFrom["queryTable"]["alias"] = widget.queryFrom["queryTable"]["tableName"];
                                tableName = widget.queryFrom["queryTable"]["tableName"];
                              });
                            },
                          ),
                        ))
                      ],
                    )
                  : Row(
                      children: [
                        Expanded(
                            child: Padding(
                          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                          child: DynamicDropdownSearchClearOption(
                            labelText: multiLanguageString(name: "table_name", defaultValue: "Table name", context: context),
                            controller: SingleValueDropDownController(
                                data: DropDownValueModel(
                                    name: widget.queryFrom["queryTable"]["tableName"] ?? "", value: widget.queryFrom["queryTable"]["tableName"])),
                            items: listTableWithoutSchema.map<DropDownValueModel>((dynamic result) {
                              return DropDownValueModel(
                                value: result["tableName"],
                                name: result["tableName"],
                              );
                            }).toList(),
                            maxHeight: 200,
                            onChanged: (dynamic newValue) {
                              setState(() {
                                widget.queryFrom["queryTable"]["tableName"] = newValue.value;
                                widget.queryFrom["queryTable"]["alias"] = widget.queryFrom["queryTable"]["tableName"];
                                tableName = widget.queryFrom["queryTable"]["tableName"];
                              });
                            },
                          ),
                        ))
                      ],
                    ),
              Row(
                children: [
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                    child: TextField(
                      controller: TextEditingController(text: widget.queryFrom["queryTable"]["alias"] ?? ""),
                      onChanged: (value) {
                        widget.queryFrom["queryTable"]["alias"] = value;
                        alias = widget.queryFrom["queryTable"]["alias"];
                      },
                      decoration: InputDecoration(
                        label: const MultiLanguageText(name: "alias", defaultValue: "Alias"),
                        hintText: multiLanguageString(name: "alias", defaultValue: "Alias", context: context),
                        border: const OutlineInputBorder(),
                      ),
                    ),
                  ))
                ],
              ),
            ],
          ),
        if (_queryFromType == "queryJoin")
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Expanded(
                      child: Padding(
                          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                          child: TextField(
                            controller: TextEditingController(text: widget.queryFrom["queryJoin"]["joinType"] ?? ""),
                            onChanged: (value) {
                              widget.queryFrom["queryJoin"]["joinType"] = value;
                            },
                            decoration: InputDecoration(
                              label: const MultiLanguageText(name: "join_type", defaultValue: "Join type"),
                              hintText: multiLanguageString(name: "join_type", defaultValue: "Join type", context: context),
                              border: const OutlineInputBorder(),
                            ),
                          )))
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                          child: Padding(
                        padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                        child: DynamicDropdownButton(
                            borderRadius: 5,
                            value: widget.queryFrom["queryJoin"]["queryTable"]["schemaName"],
                            labelText: multiLanguageString(name: "schema_name", defaultValue: "Schema name", context: context),
                            items: <DropdownMenuItem<String>>[
                              const DropdownMenuItem(
                                value: null,
                                child: Text("None"),
                              ),
                              ...listSchema.map<DropdownMenuItem<String>>((dynamic result) {
                                return DropdownMenuItem(
                                  value: result,
                                  child: Text(result),
                                );
                              }),
                            ],
                            onChanged: (newValue) async {
                              if (widget.queryFrom["queryJoin"]["queryTable"]["schemaName"] != newValue) {
                                setState(() {
                                  widget.queryFrom["queryJoin"]["queryTable"]["schemaName"] = newValue;
                                  widget.queryFrom["queryJoin"]["queryTable"]["tableName"] = null;
                                  getListTable(widget.queryFrom["queryJoin"]["queryTable"]["schemaName"]);
                                });
                              }
                            },
                            isRequiredNotEmpty: true),
                      ))
                    ],
                  ),
                  (widget.queryFrom["queryJoin"]["queryTable"]["schemaName"] != null)
                      ? Row(
                          children: [
                            Expanded(
                                child: Padding(
                              padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                              child: DynamicDropdownSearchClearOption(
                                labelText: multiLanguageString(name: "table_name", defaultValue: "Table name", context: context),
                                controller: SingleValueDropDownController(
                                    data: DropDownValueModel(
                                        name: widget.queryFrom["queryJoin"]["queryTable"]["tableName"] ?? "",
                                        value: widget.queryFrom["queryJoin"]["queryTable"]["tableName"])),
                                items: listTable.map<DropDownValueModel>((dynamic result) {
                                  return DropDownValueModel(
                                    value: result["tableName"],
                                    name: result["tableName"],
                                  );
                                }).toList(),
                                maxHeight: 200,
                                onChanged: (dynamic newValue) {
                                  setState(() {
                                    widget.queryFrom["queryJoin"]["queryTable"]["tableName"] = newValue.value;
                                    widget.queryFrom["queryJoin"]["queryTable"]["alias"] = widget.queryFrom["queryJoin"]["queryTable"]["tableName"];
                                    tableName = widget.queryFrom["queryJoin"]["queryTable"]["tableName"];
                                  });
                                },
                              ),
                            ))
                          ],
                        )
                      : Row(
                          children: [
                            Expanded(
                                child: Padding(
                              padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                              child: DynamicDropdownSearchClearOption(
                                labelText: multiLanguageString(name: "table_name", defaultValue: "Table name", context: context),
                                controller: SingleValueDropDownController(
                                    data: DropDownValueModel(
                                        name: widget.queryFrom["queryJoin"]["queryTable"]["tableName"] ?? "",
                                        value: widget.queryFrom["queryJoin"]["queryTable"]["tableName"])),
                                items: listTableWithoutSchema.map<DropDownValueModel>((dynamic result) {
                                  return DropDownValueModel(
                                    value: result["tableName"],
                                    name: result["tableName"],
                                  );
                                }).toList(),
                                maxHeight: 200,
                                onChanged: (dynamic newValue) {
                                  setState(() {
                                    widget.queryFrom["queryJoin"]["queryTable"]["tableName"] = newValue.value;
                                    widget.queryFrom["queryJoin"]["queryTable"]["alias"] = widget.queryFrom["queryJoin"]["queryTable"]["tableName"];
                                    tableName = widget.queryFrom["queryJoin"]["queryTable"]["tableName"];
                                  });
                                },
                              ),
                            ))
                          ],
                        ),
                  Row(
                    children: [
                      Expanded(
                          child: Padding(
                        padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                        child: TextField(
                          controller: TextEditingController(text: widget.queryFrom["queryJoin"]["queryTable"]["alias"] ?? ""),
                          onChanged: (value) {
                            widget.queryFrom["queryJoin"]["queryTable"]["alias"] = value;
                            alias = widget.queryFrom["queryJoin"]["queryTable"]["alias"];
                          },
                          decoration: InputDecoration(
                            label: const MultiLanguageText(name: "alias", defaultValue: "Alias"),
                            hintText: multiLanguageString(name: "alias", defaultValue: "Alias", context: context),
                            border: const OutlineInputBorder(),
                          ),
                        ),
                      ))
                    ],
                  ),
                ],
              ),
            ],
          ),
        if (_queryFromType == "querySelect")
          Row(
            children: [
              Expanded(
                  child: Padding(
                      padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                      child: TextField(
                        controller: TextEditingController(text: widget.queryFrom["queryTable"]["alias"] ?? ""),
                        onChanged: (value) {
                          widget.queryFrom["queryTable"]["alias"] = value;
                        },
                        decoration: InputDecoration(
                          label: const MultiLanguageText(name: "alias", defaultValue: "Alias"),
                          hintText: multiLanguageString(name: "alias", defaultValue: "Alias", context: context),
                          border: const OutlineInputBorder(),
                        ),
                      )))
            ],
          ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                setState(() {
                  widget.updateAlias(alias, tableName);
                  widget.onClose(widget.queryFrom);
                  Navigator.pop(context);
                });
              },
              child: const MultiLanguageText(name: "close", defaultValue: "Close"),
            )
          ],
        ),
      ],
    );
  }
}
