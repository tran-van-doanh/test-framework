import 'dart:convert';

import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/components/dashboard/dynamic_dashboard.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';

class QAPlatformDashBoardPageWidget extends StatefulWidget {
  const QAPlatformDashBoardPageWidget({Key? key}) : super(key: key);

  @override
  State<QAPlatformDashBoardPageWidget> createState() => _QAPlatformDashBoardPageWidgetState();
}

class _QAPlatformDashBoardPageWidgetState extends State<QAPlatformDashBoardPageWidget> {
  SingleValueDropDownController versionController = SingleValueDropDownController();
  SingleValueDropDownController environmentController = SingleValueDropDownController();
  var prjProjectId = "";
  var _listVersions = [];
  var _listEnvironment = [];
  bool isSearched = false;
  List dashboardList = [];
  String? dashboardSelectedId;
  Map<dynamic, dynamic> requestBody = {};
  @override
  void initState() {
    prjProjectId = Provider.of<NavigationModel>(context, listen: false).prjProjectId ?? "";
    super.initState();
    getInfoPage();
  }

  getInfoPage() async {
    getVersionList();
    getEnvironmentList();
    await getDashboardList();
    if (dashboardList.isNotEmpty) {
      setState(() {
        dashboardSelectedId = dashboardList[0]["id"];
      });
    }
  }

  getDashboardList() async {
    var findRequest = {"dashboardType": "qa_platform", "status": 1};
    for (var key in requestBody.keys) {
      if (requestBody[key] != null && requestBody[key].isNotEmpty) {
        findRequest[key] = requestBody[key];
      }
    }
    var response = await httpPost("/test-framework-api/user/report/rpt-dashboard/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false && response["body"]["resultList"].isNotEmpty) {
      setState(() {
        dashboardList = response["body"]["resultList"];
      });
    }
  }

  getVersionList() async {
    var findRequest = {"prjProjectId": prjProjectId, "status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-version/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listVersions = response["body"]["resultList"];
      });
    }
    return _listVersions;
  }

  getEnvironmentList() async {
    var findRequest = {"prjProjectId": prjProjectId, "status": 1};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-test-environment/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        _listEnvironment = response["body"]["resultList"];
      });
    }
    return _listEnvironment;
  }

  @override
  void dispose() {
    versionController.dispose();
    environmentController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageModel>(builder: (context, languageModel, child) {
      return ListView(
        padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
        children: [
          NavMenuTabV1(
            currentUrl: "/insights/dashboard",
            navigationList: context.insightsMenuList,
            margin: const EdgeInsets.only(bottom: 20),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 30),
            child: headerWidget(),
          ),
          Row(
            children: [
              if (dashboardList.length > 1)
                Expanded(
                  child: DynamicDropdownButton(
                    value: dashboardSelectedId,
                    hint: multiLanguageString(name: "choose_a_dashboard", defaultValue: "Choose a dashboard", context: context),
                    items: dashboardList.map<DropdownMenuItem<String>>((dynamic result) {
                      return DropdownMenuItem(
                        value: result["id"],
                        child: Text(result["title"]),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        dashboardSelectedId = newValue!;
                      });
                    },
                    borderRadius: 5,
                  ),
                ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          if (dashboardSelectedId != null)
            DynamicDashboardWidget(
                key: Key("${dashboardSelectedId!}:${jsonEncode(requestBody)}"),
                dashboardType: "qa_platform",
                dashboardSelectedId: dashboardSelectedId!,
                requestBody: requestBody),
        ],
      );
    });
  }

  Column headerWidget() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MultiLanguageText(
            name: "dashboard",
            defaultValue: "Dashboard",
            style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500)),
        Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                child: DynamicDropdownSearchClearOption(
                  labelText: multiLanguageString(name: "project_version", defaultValue: "Project version", context: context),
                  controller: versionController,
                  hint: multiLanguageString(name: "search_version", defaultValue: "Search version", context: context),
                  items: _listVersions.map<DropDownValueModel>((dynamic result) {
                    return DropDownValueModel(
                      value: result["id"],
                      name: result["title"],
                    );
                  }).toList(),
                  maxHeight: 200,
                  onChanged: (dynamic newValue) {
                    if (isSearched && newValue == "") {
                      setRequestBody();
                    }
                  },
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                child: DynamicDropdownSearchClearOption(
                  labelText: multiLanguageString(name: "project_environment", defaultValue: "Project environment", context: context),
                  controller: environmentController,
                  hint: multiLanguageString(name: "search_environment", defaultValue: "Search environment", context: context),
                  items: _listEnvironment.map<DropDownValueModel>((dynamic result) {
                    return DropDownValueModel(
                      value: result["id"],
                      name: result["title"],
                    );
                  }).toList(),
                  maxHeight: 200,
                  onChanged: (dynamic newValue) {
                    if (isSearched && newValue == "") {
                      setRequestBody();
                    }
                  },
                ),
              ),
            ),
          ],
        ),
        ElevatedButton(
            onPressed: () {
              isSearched = true;
              setRequestBody();
            },
            child: const MultiLanguageText(name: "search", defaultValue: "Search", isLowerCase: false))
      ],
    );
  }

  void setRequestBody() {
    setState(() {
      requestBody = {
        if (versionController.dropDownValue != null) "tfTestVersionId": versionController.dropDownValue?.value,
        if (environmentController.dropDownValue != null) "tfTestEnvironmentId": environmentController.dropDownValue?.value,
      };
    });
  }
}
