import 'dart:convert';

import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/common/row_code_with_tooltip.dart';
import 'package:test_framework_view/components/dynamic_button.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/page/editor/property_widget/root_property_widget.dart';

import '../../../common/custom_state.dart';

class DialogOptionProjectWidget extends StatefulWidget {
  final String type;
  final String title;
  final String? id;
  final Function? callbackOptionTest;
  const DialogOptionProjectWidget({Key? key, required this.type, this.id, this.callbackOptionTest, required this.title}) : super(key: key);

  @override
  State<DialogOptionProjectWidget> createState() => _DialogOptionProjectWidgetState();
}

class _DialogOptionProjectWidgetState extends CustomState<DialogOptionProjectWidget> {
  late Future future;
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _desController = TextEditingController();
  final TextEditingController _mantisProjectIdController = TextEditingController();
  final TextEditingController _mantisDefaultFieldsController = TextEditingController();
  final TextEditingController _startDateController = TextEditingController();
  final TextEditingController _endDateController = TextEditingController();
  String? _bugTrackerType;
  String? _jiraIssueCreateType = "MANUAL";
  Map<String, dynamic> jiraIssuetype = {};
  Map<String, dynamic> jiraProject = {};
  String? _mantisIssueCreateType = "MANUAL";
  String? _llqIssueCreateType = "MANUAL";
  int _status = 0;
  String? autoCreateProfile;
  final _formKey = GlobalKey<FormState>();
  dynamic selectedItem;
  String? projectType;
  String? jiraBugTrackerAccountId;
  String? mantisBugTrackerAccountId;
  List listProjectType = [];
  List listBugTrackerAccount = [];
  List listJiraProject = [];
  List listJiraIssuetype = [];
  List jiraFieldList = [];
  Map<String, dynamic> fieldConfig = {};
  Map<dynamic, dynamic> jiraDefaultFields = {};
  @override
  void initState() {
    future = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    getListProjectType();
    if (widget.id != null) {
      await getSelectedItem(widget.id!);
      setInitValue();
      if ((_bugTrackerType == "JIRA" || _bugTrackerType == "MANTIS")) {
        getListBugTrackerAccount();
      }
      if (_bugTrackerType == "JIRA" && jiraBugTrackerAccountId != null) {
        getListJiraProject();
      }
      if (_bugTrackerType == "JIRA" && jiraProject["id"] != null) {
        getListJiraIssuetype();
      }
      if (_bugTrackerType == "JIRA" && jiraIssuetype["id"] != null) {
        getListJiraField();
      }
    }
    return 0;
  }

  getSelectedItem(String id) async {
    var response = await httpGet("/test-framework-api/user/project/prj-project/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        selectedItem = response["body"]["result"];
      });
    }
  }

  _selectDate(TextEditingController controller) async {
    final DateTime? datePicked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (datePicked != null) {
      setState(() {
        controller.text = DateFormat('dd-MM-yyyy').format(datePicked);
      });
    }
  }

  getListProjectType() async {
    var findRequest = {"status": 1};
    var response = await httpPost("/test-framework-api/user/project/prj-project-type/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        listProjectType = response["body"]["resultList"] ?? [];
      });
    }
  }

  getListBugTrackerAccount() async {
    var findRequest = {"bugTrackerType": _bugTrackerType};
    var response = await httpPost("/test-framework-api/user/test-framework/tf-bug-tracker-account/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        listBugTrackerAccount = response["body"]["resultList"] ?? [];
      });
    }
  }

  getListJiraProject() async {
    var response = await httpGet("/test-framework-api/user/jira/$jiraBugTrackerAccountId/project", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        listJiraProject = response["body"]["resultList"] ?? [];
      });
    }
  }

  getListJiraIssuetype() async {
    var response = await httpGet("/test-framework-api/user/jira/$jiraBugTrackerAccountId/project/${jiraProject["id"]}/issue-type", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        listJiraIssuetype = response["body"]["resultList"] ?? [];
      });
    }
  }

  getListJiraField() async {
    var response = await httpGet(
        "/test-framework-api/user/jira/$jiraBugTrackerAccountId/project/${jiraProject["id"]}/issue-type/${jiraIssuetype["id"]}/field", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        jiraFieldList = response["body"]["resultList"] ?? [];
      });
    }
  }

  setInitValue() {
    setState(() {
      if (selectedItem != null) {
        _bugTrackerType = selectedItem["bugTrackerType"];
        _status = selectedItem["status"] ?? 0;
        autoCreateProfile = selectedItem["autoCreateProfile"].toString();
        _nameController.text = selectedItem["name"] ?? "";
        _titleController.text = selectedItem["title"] ?? "";
        _desController.text = selectedItem["description"] ?? "";
        projectType = selectedItem["prjProjectTypeId"];
        if (selectedItem["projectConfig"] != null && selectedItem["projectConfig"]["jira"] != null) {
          jiraBugTrackerAccountId = selectedItem["projectConfig"]["jira"]["tfBugTrackerAccountId"];
          if (selectedItem["projectConfig"]["jira"]["defaultFields"] != null && selectedItem["projectConfig"]["jira"]["defaultFields"].isNotEmpty) {
            jiraDefaultFields = selectedItem["projectConfig"]["jira"]["defaultFields"];
            jiraProject = selectedItem["projectConfig"]["jira"]["defaultFields"]["project"];
            jiraIssuetype = selectedItem["projectConfig"]["jira"]["defaultFields"]["issuetype"];
          }
          _jiraIssueCreateType = selectedItem["projectConfig"]["jira"]["issueCreateType"] ?? "MANUAL";
          fieldConfig = selectedItem["projectConfig"]["jira"]["fieldConfig"] ?? {};
        }
        if (selectedItem["projectConfig"] != null && selectedItem["projectConfig"]["mantis"] != null) {
          mantisBugTrackerAccountId = selectedItem["projectConfig"]["mantis"]["tfBugTrackerAccountId"];
          _mantisProjectIdController.text = selectedItem["projectConfig"]["mantis"]["projectId"] ?? "";
          if (selectedItem["projectConfig"]["mantis"]["defaultFields"] != null &&
              selectedItem["projectConfig"]["mantis"]["defaultFields"].isNotEmpty) {
            _mantisDefaultFieldsController.text = jsonEncode(selectedItem["projectConfig"]["mantis"]["defaultFields"]);
          } else {
            _mantisDefaultFieldsController.text = "{}";
          }
          _mantisIssueCreateType = selectedItem["projectConfig"]["mantis"]["issueCreateType"] ?? "MANUAL";
        }
        if (selectedItem["projectConfig"] != null && selectedItem["projectConfig"]["llq"] != null) {
          _llqIssueCreateType = selectedItem["projectConfig"]["llq"]["issueCreateType"] ?? "MANUAL";
        }
        _startDateController.text = selectedItem["startDate"] != null
            ? DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(selectedItem["startDate"]).toLocal())
            : DateFormat('dd-MM-yyyy').format(DateTime.now());
        _endDateController.text = selectedItem["endDate"] != null
            ? DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(selectedItem["endDate"]).toLocal())
            : DateFormat('dd-MM-yyyy').format(DateTime.now());
      }
    });
  }

  @override
  void dispose() {
    _nameController.dispose();
    _titleController.dispose();
    _desController.dispose();
    _mantisProjectIdController.dispose();
    _mantisDefaultFieldsController.dispose();
    _startDateController.dispose();
    _endDateController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: future,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Scaffold(
              body: Column(
            children: [
              headerWidget(context),
              const Divider(color: Color.fromRGBO(216, 218, 229, 1)),
              Expanded(
                child: Container(
                  margin: const EdgeInsets.only(top: 20, bottom: 10),
                  child: Form(
                    key: _formKey,
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                        child: bodyWidget(context),
                      ),
                    ),
                  ),
                ),
              ),
              btnWidget()
            ],
          ));
        } else if (snapshot.hasError) {
          return Text('${snapshot.error}');
        }
        return const Center(child: CircularProgressIndicator());
      },
    );
  }

  Padding btnWidget() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20, top: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ButtonSave(onPressed: () {
            if (_formKey.currentState!.validate()) {
              var result = {};
              if (selectedItem != null) {
                result = selectedItem;
                result["prjProjectTypeId"] = projectType;
                result["name"] = _nameController.text;
                result["title"] = _titleController.text;
                result["description"] = _desController.text;
                result["bugTrackerType"] = _bugTrackerType;
                result["status"] = _status;
                result["autoCreateProfile"] = (autoCreateProfile == "true");
                result["startDate"] = "${DateFormat('dd-MM-yyyy').parse(_startDateController.text).toIso8601String()}+07:00";
                result["endDate"] = "${DateFormat('dd-MM-yyyy').parse(_endDateController.text).toIso8601String()}+07:00";
                result["projectConfig"] ??= {};
                result["projectConfig"]["jira"] = {
                  "tfBugTrackerAccountId": jiraBugTrackerAccountId,
                  "issueCreateType": _jiraIssueCreateType,
                  "fieldConfig": fieldConfig,
                  "defaultFields": jiraDefaultFields,
                };
                result["projectConfig"]["jira"]["defaultFields"]["project"] = jiraProject;
                result["projectConfig"]["jira"]["defaultFields"]["issuetype"] = jiraIssuetype;
                result["projectConfig"]["mantis"] = {
                  "tfBugTrackerAccountId": mantisBugTrackerAccountId,
                  "projectId": _mantisProjectIdController.text,
                  "issueCreateType": _mantisIssueCreateType,
                  "defaultFields": jsonDecode(_mantisDefaultFieldsController.text.isEmpty ? "{}" : _mantisDefaultFieldsController.text),
                };
                result["projectConfig"]["llq"] = {
                  "issueCreateType": _llqIssueCreateType,
                };
              } else {
                result = {
                  "prjProjectTypeId": projectType,
                  "name": _nameController.text,
                  "title": _titleController.text,
                  "description": _desController.text,
                  "bugTrackerType": _bugTrackerType,
                  "status": _status,
                  "autoCreateProfile": (autoCreateProfile == "true"),
                  if (_startDateController.text.isNotEmpty)
                    "startDate": "${DateFormat('dd-MM-yyyy').parse(_startDateController.text).toIso8601String()}+07:00",
                  if (_endDateController.text.isNotEmpty)
                    "endDate": "${DateFormat('dd-MM-yyyy').parse(_endDateController.text).toIso8601String()}+07:00",
                  "projectConfig": {
                    "jira": {
                      "tfBugTrackerAccountId": jiraBugTrackerAccountId,
                      "issueCreateType": _jiraIssueCreateType,
                      "fieldConfig": fieldConfig,
                      "defaultFields": jiraDefaultFields,
                    },
                    "mantis": {
                      "tfBugTrackerAccountId": mantisBugTrackerAccountId,
                      "mantisProjectId": _mantisProjectIdController.text,
                      "issueCreateType": _mantisIssueCreateType,
                      "defaultFields": jsonDecode(_mantisDefaultFieldsController.text.isEmpty ? "{}" : _mantisDefaultFieldsController.text)
                    },
                    "llq": {
                      "issueCreateType": _llqIssueCreateType,
                    }
                  }
                };
                result["projectConfig"]["jira"]["defaultFields"]["project"] = jiraProject;
                result["projectConfig"]["jira"]["defaultFields"]["issuetype"] = jiraIssuetype;
              }
              widget.callbackOptionTest!(result);
            }
          }),
          const ButtonCancel()
        ],
      ),
    );
  }

  Column bodyWidget(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "project_type", defaultValue: "Project type", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                  value: projectType,
                  hint: multiLanguageString(name: "choose_a_project_type", defaultValue: "Choose a project type", context: context),
                  items: listProjectType.map<DropdownMenuItem<String>>((dynamic result) {
                    return DropdownMenuItem(
                      value: result["id"],
                      child: Text(result["title"]),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      projectType = newValue!;
                    });
                  },
                  borderRadius: 5,
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Expanded(
                flex: 3,
                child: RowCodeWithTooltipWidget(),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _nameController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                  readOnly: widget.type == "edit",
                  isRequiredRegex: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "name", defaultValue: "Name", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _titleController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "description", defaultValue: "Description", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _desController,
                  hintText: "...",
                  maxline: 3,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "auto_create_profile", defaultValue: "Auto create profile", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                  value: autoCreateProfile ?? "false",
                  hint: multiLanguageString(name: "choose_a_status", defaultValue: "Choose a status", context: context),
                  items: ["false", "true"].map<DropdownMenuItem<String>>((String result) {
                    return DropdownMenuItem(
                      value: result,
                      child: Text(result),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      autoCreateProfile = newValue!;
                    });
                  },
                  borderRadius: 5,
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "status", defaultValue: "Status", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                  value: _status,
                  hint: multiLanguageString(name: "choose_a_status", defaultValue: "Choose a status", context: context),
                  items: [
                    {"name": multiLanguageString(name: "active", defaultValue: "Active", context: context), "value": 1},
                    {"name": multiLanguageString(name: "inactive", defaultValue: "Inactive", context: context), "value": 0},
                  ].map<DropdownMenuItem<int>>((dynamic result) {
                    return DropdownMenuItem(
                      value: result["value"],
                      child: Text(result["name"]),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      _status = newValue!;
                    });
                  },
                  borderRadius: 5,
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "start_date", defaultValue: "Start date", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _startDateController,
                  suffixIcon: const Icon(Icons.calendar_today),
                  hintText: multiLanguageString(name: "select_start_date", defaultValue: "Select start date", context: context),
                  labelText: multiLanguageString(name: "start_date", defaultValue: "Start date", context: context),
                  readOnly: true,
                  isRequiredNotEmpty: true,
                  onTap: () {
                    _selectDate(_startDateController);
                  },
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "end_date", defaultValue: "End date", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _endDateController,
                  suffixIcon: const Icon(Icons.calendar_today),
                  hintText: multiLanguageString(name: "select_end_date", defaultValue: "Select end date", context: context),
                  labelText: multiLanguageString(name: "end_date", defaultValue: "End date", context: context),
                  readOnly: true,
                  isRequiredNotEmpty: true,
                  onTap: () {
                    _selectDate(_endDateController);
                  },
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "bug_tracker_type", defaultValue: "Bug tracker type", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                  value: _bugTrackerType ?? "None",
                  hint: multiLanguageString(name: "bug_tracker_type", defaultValue: "Bug tracker type", context: context),
                  items: [
                    {"value": "None", "label": "None"},
                    {"value": "LLQ", "label": "LLQ Work management"},
                    {"value": "JIRA", "label": "Jira"},
                    {"value": "MANTIS", "label": "Mantis"}
                  ].map<DropdownMenuItem<String>>((dynamic result) {
                    return DropdownMenuItem(
                      value: result["value"],
                      child: Text(result["label"]),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    if (_bugTrackerType != newValue) {
                      setState(() {
                        _bugTrackerType = newValue!;
                        jiraBugTrackerAccountId = null;
                        mantisBugTrackerAccountId = null;
                      });
                      if ((_bugTrackerType == "JIRA" || _bugTrackerType == "MANTIS")) {
                        getListBugTrackerAccount();
                      }
                    }
                  },
                  borderRadius: 5,
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        if (_bugTrackerType == "JIRA")
          Column(
            children: [
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child:
                          MultiLanguageText(name: "bug_tracker_account", defaultValue: "Bug tracker account", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicDropdownButton(
                        value: jiraBugTrackerAccountId,
                        hint:
                            multiLanguageString(name: "choose_a_bug_tracker_account", defaultValue: "Choose a bug tracker account", context: context),
                        items: listBugTrackerAccount.map<DropdownMenuItem<String>>((dynamic result) {
                          return DropdownMenuItem(
                            value: result["id"],
                            child: Text(result["username"]),
                          );
                        }).toList(),
                        onChanged: (newValue) {
                          if (jiraBugTrackerAccountId != newValue) {
                            setState(() {
                              jiraBugTrackerAccountId = newValue!;
                              jiraProject.clear();
                            });
                            getListJiraProject();
                          }
                        },
                        borderRadius: 5,
                        isRequiredNotEmpty: true,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "issue_create_type", defaultValue: "Issue create type", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicDropdownButton(
                        value: _jiraIssueCreateType,
                        hint: multiLanguageString(name: "issue_create_type", defaultValue: "issue create type", context: context),
                        items: ["AUTO", "MANUAL"].map<DropdownMenuItem<String>>((String result) {
                          return DropdownMenuItem(
                            value: result,
                            child: Text(result),
                          );
                        }).toList(),
                        onChanged: (newValue) {
                          setState(() {
                            // _formKey.currentState!.validate();
                            _jiraIssueCreateType = newValue!;
                          });
                        },
                        borderRadius: 5,
                        isRequiredNotEmpty: true,
                      ),
                    ),
                  ],
                ),
              ),
              if (jiraBugTrackerAccountId != null)
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 10),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 3,
                        child: MultiLanguageText(name: "jira_project", defaultValue: "Jira project", style: Style(context).styleTitleDialog),
                      ),
                      Expanded(
                        flex: 7,
                        child: DynamicDropdownSearchClearOption(
                          hint: multiLanguageString(name: "jira_project", defaultValue: "Jira project", context: context),
                          controller: SingleValueDropDownController(data: DropDownValueModel(name: jiraProject["name"], value: jiraProject["id"])),
                          items: listJiraProject.map<DropDownValueModel>((dynamic result) {
                            return DropDownValueModel(
                              value: result["id"],
                              name: result["name"],
                            );
                          }).toList(),
                          maxHeight: 200,
                          onChanged: (dynamic newValue) {
                            if (jiraProject["id"] != newValue) {
                              setState(() {
                                jiraProject = listJiraProject.firstWhere((element) => element["id"] == newValue);
                                jiraIssuetype.clear();
                              });
                              getListJiraIssuetype();
                            }
                          },
                          isRequiredNotEmpty: true,
                        ),
                      ),
                    ],
                  ),
                ),
              if (jiraProject["id"] != null)
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 10),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 3,
                        child: MultiLanguageText(name: "jira_issue_type", defaultValue: "Jira issue type", style: Style(context).styleTitleDialog),
                      ),
                      Expanded(
                        flex: 7,
                        child: DynamicDropdownButton(
                          value: jiraIssuetype["id"],
                          hint: multiLanguageString(name: "jira_issue_type", defaultValue: "Jira issue type", context: context),
                          items: listJiraIssuetype.map<DropdownMenuItem<String>>((dynamic result) {
                            return DropdownMenuItem(
                              value: result["id"],
                              child: Text(result["name"]),
                            );
                          }).toList(),
                          onChanged: (newValue) {
                            if (jiraIssuetype["id"] != newValue) {
                              setState(() {
                                jiraIssuetype = listJiraIssuetype.firstWhere((element) => element["id"] == newValue);
                              });
                              getListJiraField();
                            }
                          },
                          borderRadius: 5,
                          isRequiredNotEmpty: true,
                        ),
                      ),
                    ],
                  ),
                ),
              if (jiraIssuetype["id"] != null && jiraFieldList.isNotEmpty) jiraFieldListTableWidget(),
            ],
          ),
        if (_bugTrackerType == "MANTIS")
          Column(
            children: [
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child:
                          MultiLanguageText(name: "bug_tracker_account", defaultValue: "Bug tracker account", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicDropdownButton(
                        value: mantisBugTrackerAccountId,
                        hint:
                            multiLanguageString(name: "choose_a_bug_tracker_account", defaultValue: "Choose a bug tracker account", context: context),
                        items: listBugTrackerAccount.map<DropdownMenuItem<String>>((dynamic result) {
                          return DropdownMenuItem(
                            value: result["id"],
                            child: Text(result["username"]),
                          );
                        }).toList(),
                        onChanged: (newValue) {
                          mantisBugTrackerAccountId = newValue!;
                        },
                        borderRadius: 5,
                        isRequiredNotEmpty: true,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "issue_create_type", defaultValue: "Issue create type", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicDropdownButton(
                        value: _mantisIssueCreateType,
                        hint: multiLanguageString(name: "issue_create_type", defaultValue: "issue create type", context: context),
                        items: ["AUTO", "MANUAL"].map<DropdownMenuItem<String>>((String result) {
                          return DropdownMenuItem(
                            value: result,
                            child: Text(result),
                          );
                        }).toList(),
                        onChanged: (newValue) {
                          setState(() {
                            // _formKey.currentState!.validate();
                            _mantisIssueCreateType = newValue!;
                          });
                        },
                        borderRadius: 5,
                        isRequiredNotEmpty: true,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "mantis_project_id", defaultValue: "Mantis project id", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicTextField(
                        controller: _mantisProjectIdController,
                        hintText: "...",
                        onChanged: (text) {
                          // _formKey.currentState!.validate();
                        },
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(
                          name: "mantis_default_fields", defaultValue: "Mantis default fields", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicTextField(
                        controller: _mantisDefaultFieldsController,
                        hintText: "...",
                        onChanged: (text) {
                          // _formKey.currentState!.validate();
                        },
                        maxline: 10,
                        minline: 3,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        if (_bugTrackerType == "LLQ")
          Column(
            children: [
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: MultiLanguageText(name: "issue_create_type", defaultValue: "Issue create type", style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicDropdownButton(
                        value: _llqIssueCreateType,
                        hint: multiLanguageString(name: "issue_create_type", defaultValue: "issue create type", context: context),
                        items: ["AUTO", "MANUAL"].map<DropdownMenuItem<String>>((String result) {
                          return DropdownMenuItem(
                            value: result,
                            child: Text(result),
                          );
                        }).toList(),
                        onChanged: (newValue) {
                          setState(() {
                            // _formKey.currentState!.validate();
                            _llqIssueCreateType = newValue!;
                          });
                        },
                        borderRadius: 5,
                        isRequiredNotEmpty: true,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
      ],
    );
  }

  Padding headerWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 35, 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "type_project",
            defaultValue: "\$0 PROJECT",
            variables: [widget.title],
            isLowerCase: false,
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
            onPressed: () => Navigator.pop(context, 'cancel'),
            child: const Icon(
              Icons.close,
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }

  Column jiraFieldListTableWidget() {
    return Column(
      children: [
        Row(
          children: [
            HeaderItemTable(flex: 2, title: multiLanguageString(name: "name", defaultValue: "Name", context: context)),
            HeaderItemTable(title: multiLanguageString(name: "required", defaultValue: "Required", context: context)),
            HeaderItemTable(title: multiLanguageString(name: "default", defaultValue: "Default", context: context)),
            HeaderItemTable(flex: 2, title: multiLanguageString(name: "field", defaultValue: "Field", context: context)),
            HeaderItemTable(flex: 2, title: multiLanguageString(name: "value_map", defaultValue: "Value map", context: context)),
          ],
        ),
        for (var jiraField in jiraFieldList)
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 2.5),
            child: Row(
              children: [
                Expanded(
                  flex: 2,
                  child: Text(
                    jiraField["name"] ?? "",
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                Expanded(
                  child: Center(
                      child: Text(
                    jiraField["required"] ? "Required" : "Not required",
                    style: Style(context).styleTextDataCell,
                  )),
                ),
                Expanded(
                  child: Center(
                      child: Text(
                    jiraField["hasDefaultValue"] ? "Default" : "Not default",
                    style: Style(context).styleTextDataCell,
                  )),
                ),
                Expanded(
                  flex: 2,
                  child: DynamicDropdownButton(
                    value: fieldConfig[jiraField["fieldId"]] != null ? fieldConfig[jiraField["fieldId"]]["qaPlatformField"] : null,
                    hint: multiLanguageString(name: "choose_a_field", defaultValue: "Choose a field", context: context),
                    items: [
                      {"value": "summary", "name": multiLanguageString(name: "summary", defaultValue: "Summary", context: context)},
                      {"value": "description", "name": multiLanguageString(name: "description", defaultValue: "Description", context: context)},
                      {"value": "tfTestVersion", "name": multiLanguageString(name: "version", defaultValue: "Version", context: context)},
                      {"value": "tfTestComponent", "name": multiLanguageString(name: "component", defaultValue: "Component", context: context)},
                      {"value": "tfTestEnvironment", "name": multiLanguageString(name: "environment", defaultValue: "Environment", context: context)},
                      {"value": "dataSeverity", "name": multiLanguageString(name: "severity", defaultValue: "Severity", context: context)},
                      {"value": "dataPriority", "name": multiLanguageString(name: "priority", defaultValue: "Priority", context: context)},
                      {"value": "createUser", "name": multiLanguageString(name: "create_user", defaultValue: "Create User", context: context)},
                      {"value": "tfTestCase", "name": multiLanguageString(name: "tf_test_case", defaultValue: "Test Case", context: context)},
                      {
                        "value": "tfTestDirectory",
                        "name": multiLanguageString(name: "tf_test_directory", defaultValue: "Test Directory", context: context)
                      },
                    ].map<DropdownMenuItem<String>>((dynamic result) {
                      return DropdownMenuItem(
                        value: result["value"],
                        child: Text(result["name"]),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        fieldConfig[jiraField["fieldId"]] ??= {};
                        fieldConfig[jiraField["fieldId"]]["qaPlatformField"] = newValue;
                      });
                    },
                    suffixIcon: (fieldConfig[jiraField["fieldId"]] != null && fieldConfig[jiraField["fieldId"]]["qaPlatformField"] != null)
                        ? IconButton(
                            onPressed: () {
                              setState(() {
                                fieldConfig.remove(jiraField["fieldId"]);
                              });
                            },
                            icon: const Icon(Icons.close),
                            splashRadius: 20,
                          )
                        : null,
                    borderRadius: 5,
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            if (jiraDefaultFields[jiraField["fieldId"]] != null && jiraDefaultFields[jiraField["fieldId"]]["displayName"] != null)
                              Text(
                                "User : ${jiraDefaultFields[jiraField["fieldId"]]["displayName"]}",
                                style: Style(context).styleTextDataCell,
                              ),
                            if ((fieldConfig[jiraField["fieldId"]] != null && fieldConfig[jiraField["fieldId"]]["valueMap"] != null))
                              Text(
                                viewTextValueMap(fieldConfig[jiraField["fieldId"]]["valueMap"]),
                                style: Style(context).styleTextDataCell,
                              ),
                          ],
                        ),
                      ),
                      Wrap(
                        spacing: 10,
                        children: [
                          if (jiraField["allowedValues"] != null &&
                              jiraField["allowedValues"].isNotEmpty &&
                              ((fieldConfig[jiraField["fieldId"]] != null &&
                                  fieldConfig[jiraField["fieldId"]]["qaPlatformField"] != null &&
                                  (fieldConfig[jiraField["fieldId"]]["qaPlatformField"] == "dataSeverity" ||
                                      fieldConfig[jiraField["fieldId"]]["qaPlatformField"] == "dataPriority"))))
                            ElevatedButton(
                              onPressed: () {
                                fieldConfig[jiraField["fieldId"]] ??= {};
                                showDialog<String>(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return jiraValueMapWidget(
                                        fieldConfig[jiraField["fieldId"]]["valueMap"],
                                        fieldConfig[jiraField["fieldId"]]["qaPlatformField"],
                                        jiraField["allowedValues"],
                                        (valueMap) {
                                          fieldConfig[jiraField["fieldId"]]["valueMap"] ??= {};
                                          setState(() {
                                            fieldConfig[jiraField["fieldId"]]["valueMap"] = valueMap;
                                          });
                                        },
                                      );
                                    });
                              },
                              child: const MultiLanguageText(
                                name: "map",
                                defaultValue: "Map",
                                isLowerCase: false,
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          if (jiraField["schema"]["type"] == "user")
                            ElevatedButton(
                              onPressed: () async {
                                var response = await httpGet("/test-framework-api/user/jira/$jiraBugTrackerAccountId/user", context);
                                if (response.containsKey("body") && response["body"] is String == false && mounted) {
                                  showDialog<String>(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return jiraDefaultValueWidget(
                                          jiraDefaultFields[jiraField["fieldId"]],
                                          response["body"]["resultList"] ?? [],
                                          (user) {
                                            setState(() {
                                              Map<dynamic, dynamic> newMap = {jiraField["fieldId"]: user};
                                              jiraDefaultFields.addAll(Map<String, dynamic>.from(newMap));
                                            });
                                          },
                                        );
                                      });
                                }
                              },
                              child: const MultiLanguageText(
                                name: "default",
                                defaultValue: "Default",
                                isLowerCase: false,
                                style: TextStyle(color: Colors.white),
                              ),
                            )
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
      ],
    );
  }

  AlertDialog jiraValueMapWidget(valueMapInit, qaPlatformField, List allowedValues, Function onSave) {
    Map<String, dynamic> valueMap = {};
    if (valueMapInit != null && valueMapInit.isNotEmpty) valueMap = Map.from(valueMapInit);
    List valueMapList = [];
    if (qaPlatformField == "dataSeverity") {
      valueMapList = [
        "Critical",
        "Major",
        "Average",
        "Minor",
        "Trivial",
      ];
    }
    if (qaPlatformField == "dataPriority") {
      valueMapList = [
        "Highest",
        "High",
        "Medium",
        "Low",
        "Lowest",
      ];
    }
    return AlertDialog(
      contentPadding: const EdgeInsets.all(0),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "value_map",
            defaultValue: "Value map",
            isLowerCase: false,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                color: const Color.fromRGBO(0, 0, 0, 0.8),
                fontSize: context.fontSizeManager.fontSizeText16,
                letterSpacing: 2),
          ),
          TextButton(
              onPressed: () => Navigator.pop(context, 'Cancel'),
              child: const Icon(
                Icons.close,
                color: Colors.black,
              ))
        ],
      ),
      content: Container(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
        decoration: const BoxDecoration(
          border: Border(
            top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
            bottom: BorderSide(
              color: Color.fromRGBO(216, 218, 229, 1),
            ),
          ),
        ),
        margin: const EdgeInsets.only(top: 20, bottom: 10),
        constraints: const BoxConstraints(minWidth: 600),
        child: SingleChildScrollView(
          child: Column(
            children: [
              for (var valueMapItem in valueMapList)
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 3,
                      child: Text(valueMapItem, style: Style(context).styleTitleDialog),
                    ),
                    Expanded(
                      flex: 7,
                      child: DynamicDropdownButton(
                        value: valueMap[valueMapItem] != null ? valueMap[valueMapItem]["id"] : null,
                        hint: multiLanguageString(name: "choose_a_value", defaultValue: "Choose a value", context: context),
                        items: allowedValues.map<DropdownMenuItem<dynamic>>((dynamic result) {
                          return DropdownMenuItem(
                            value: result["id"],
                            child: Text("${result["name"] ?? ""}${result["value"] ?? ""}"),
                          );
                        }).toList(),
                        onChanged: (newValue) {
                          setState(() {
                            valueMap[valueMapItem] = allowedValues.firstWhere((element) => element["id"] == newValue);
                          });
                        },
                        borderRadius: 5,
                      ),
                    ),
                  ],
                ),
            ],
          ),
        ),
      ),
      actionsAlignment: MainAxisAlignment.center,
      actions: [
        ButtonSave(
          onPressed: () {
            onSave(valueMap);
            Navigator.pop(context);
          },
        ),
        const ButtonCancel()
      ],
    );
  }

  AlertDialog jiraDefaultValueWidget(userSelected, List userList, Function onSave) {
    var user = {};
    if (userSelected != null && userSelected.isNotEmpty) {
      user = Map.from(userSelected);
    }
    return AlertDialog(
      contentPadding: const EdgeInsets.all(0),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "default_value",
            defaultValue: "Default value",
            isLowerCase: false,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                color: const Color.fromRGBO(0, 0, 0, 0.8),
                fontSize: context.fontSizeManager.fontSizeText16,
                letterSpacing: 2),
          ),
          TextButton(
              onPressed: () => Navigator.pop(context, 'Cancel'),
              child: const Icon(
                Icons.close,
                color: Colors.black,
              ))
        ],
      ),
      content: Container(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
        decoration: const BoxDecoration(
          border: Border(
            top: BorderSide(color: Color.fromRGBO(216, 218, 229, 1)),
            bottom: BorderSide(
              color: Color.fromRGBO(216, 218, 229, 1),
            ),
          ),
        ),
        margin: const EdgeInsets.only(top: 20, bottom: 10),
        constraints: const BoxConstraints(minWidth: 600),
        child: SingleChildScrollView(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "user", defaultValue: "User", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownSearchClearOption(
                  hint: multiLanguageString(name: "choose_an_user", defaultValue: "Choose an user", context: context),
                  controller: SingleValueDropDownController(data: DropDownValueModel(name: user["displayName"], value: user["accountId"])),
                  items: userList.map<DropDownValueModel>((dynamic result) {
                    return DropDownValueModel(
                      value: result["accountId"],
                      name: result["displayName"],
                    );
                  }).toList(),
                  maxHeight: 200,
                  onChanged: (dynamic newValue) {
                    setState(() {
                      user = userList.firstWhere((element) => element["accountId"] == newValue.value);
                    });
                  },
                ),
              ),
            ],
          ),
        ),
      ),
      actionsAlignment: MainAxisAlignment.center,
      actions: [
        ButtonSave(
          onPressed: () {
            onSave(user);
            Navigator.pop(context);
          },
        ),
        const ButtonCancel()
      ],
    );
  }

  String viewTextValueMap(inputMap) {
    String formattedText = '';
    inputMap.forEach((key, value) {
      formattedText += '$key: ${value['name']}, ';
    });
    formattedText = formattedText.substring(0, formattedText.length - 2);
    return formattedText;
  }
}
