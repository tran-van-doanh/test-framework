import 'package:flutter/material.dart';

import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/components/badge/badge_icon.dart';
import 'package:test_framework_view/components/dialog/dialog_delete.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/resource-pool/project/rp_option_project.dart';
import 'package:test_framework_view/page/work_mgt/setting_project_work_mgt/setting_project_work_mgt.dart';
import 'package:test_framework_view/page/setting_project/project/option_project.dart';
import 'package:test_framework_view/type.dart';
import '../../../common/custom_state.dart';

class SettingProjectWidget extends StatefulWidget {
  final String type;
  const SettingProjectWidget({Key? key, required this.type}) : super(key: key);

  @override
  State<SettingProjectWidget> createState() => _SettingProjectWidgetState();
}

class _SettingProjectWidgetState extends CustomState<SettingProjectWidget> {
  late Future futureprjProjectList;
  var prjProjectSearchRequest = {};
  var prjProjectRowCount = 0;
  var prjProjectCurrentPage = 1;
  var prjProjectResultList = [];
  var prjProjectRowPerPage = 10;
  var prjProjectStatusMap = {1: "Active", 0: "Inactive"};

  @override
  void initState() {
    prjProjectSearch({});
    super.initState();
  }

  prjProjectSearch(prjProjectSearchRequest) {
    this.prjProjectSearchRequest = prjProjectSearchRequest;
    futureprjProjectList = prjProjectPageChange(prjProjectCurrentPage);
  }

  prjProjectPageChange(page) async {
    if ((page - 1) * prjProjectRowPerPage > prjProjectRowCount) {
      page = (1.0 * prjProjectRowCount / prjProjectRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * prjProjectRowPerPage, "queryLimit": prjProjectRowPerPage};
    if (prjProjectSearchRequest["nameLike"] != null && prjProjectSearchRequest["nameLike"].isNotEmpty) {
      findRequest["nameLike"] = "%${prjProjectSearchRequest["nameLike"]}%";
    }
    var response = await httpPost("/test-framework-api/user/project/prj-project/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        prjProjectCurrentPage = page;
        prjProjectRowCount = response["body"]["rowCount"];
        prjProjectResultList = response["body"]["resultList"];
      });
    }
    return 0;
  }

  handleDeleteProject(prjProject) async {
    var response = await httpDelete("/test-framework-api/user/project/prj-project/${prjProject["id"]}", context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        await prjProjectPageChange(prjProjectCurrentPage);
      }
    }
  }

  handleAddProject(result) async {
    var response = await httpPut("/test-framework-api/user/project/prj-project", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await prjProjectPageChange(prjProjectCurrentPage);
        return true;
      }
    }
    return false;
  }

  handleEditProject(result, prjProject) async {
    var response = await httpPost("/test-framework-api/user/project/prj-project/${prjProject["id"]}", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Provider.of<NavigationModel>(context, listen: false).setIsRequiredGetListProject(true);
        Navigator.of(context).pop();
        await prjProjectPageChange(prjProjectCurrentPage);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer2<NavigationModel, LanguageModel>(builder: (context, navigationModel, languageModel, child) {
      return FutureBuilder(
        future: futureprjProjectList,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Scaffold(
              body: Column(
                children: [
                  Expanded(
                    child: ListView(
                      children: [
                        if (widget.type != "resource-pool")
                          OrganizationNavMenuTab(
                            margin: const EdgeInsets.fromLTRB(45, 20, 30, 0),
                            navigationList: widget.type == "qa-platform"
                                ? context.organizationSettingsMenuList
                                : widget.type == "work-mgt"
                                    ? context.workManagementSettingsMenuList
                                    : [],
                          ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  MultiLanguageText(
                                    name: "projects_count",
                                    defaultValue: "Projects ( \$0 )",
                                    variables: ["$prjProjectRowCount"],
                                    style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
                                  ),
                                ],
                              ),
                              tableWidget(context, navigationModel),
                              DynamicTablePagging(
                                prjProjectRowCount,
                                prjProjectCurrentPage,
                                prjProjectRowPerPage,
                                pageChangeHandler: (page) {
                                  setState(() {
                                    futureprjProjectList = prjProjectPageChange(page);
                                  });
                                },
                                rowPerPageChangeHandler: (rowPerPage) {
                                  setState(() {
                                    prjProjectRowPerPage = rowPerPage!;
                                    futureprjProjectList = prjProjectPageChange(1);
                                  });
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  btnWidget(context, navigationModel),
                ],
              ),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        },
      );
    });
  }

  Padding btnWidget(BuildContext context, NavigationModel navigationModel) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 40, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          SizedBox(
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              icon: const IconTheme(
                data: IconThemeData(color: Color.fromRGBO(49, 49, 49, 1)),
                child: Icon(Icons.add),
              ),
              label: MultiLanguageText(
                name: "new_project",
                defaultValue: "New Project",
                isLowerCase: false,
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
              onPressed: () {
                Navigator.of(context).push(
                  createRoute(
                    (widget.type == "resource-pool")
                        ? RPOptionProjectWidget(
                            type: "create",
                            title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                            callbackOptionTest: (result) async {
                              bool resultFunction = await handleAddProject(result);
                              if (resultFunction && mounted) {
                                Provider.of<SecurityModel>(this.context, listen: false).reloadUserInfo();
                              }
                            })
                        : (widget.type == "work-mgt")
                            ? SettingProjectWorkMGTWidget(
                                type: "create",
                                title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                                callbackOptionTest: (result) async {
                                  bool resultFunction = await handleAddProject(result);
                                  if (resultFunction && mounted) {
                                    Provider.of<SecurityModel>(this.context, listen: false).reloadUserInfo();
                                  }
                                })
                            : DialogOptionProjectWidget(
                                type: "create",
                                title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                                callbackOptionTest: (result) async {
                                  bool resultFunction = await handleAddProject(result);
                                  if (resultFunction && mounted) {
                                    Provider.of<SecurityModel>(this.context, listen: false).reloadUserInfo();
                                  }
                                }),
                  ),
                );
              },
            ),
          ),
          const SizedBox(width: 10),
          Container(
            margin: const EdgeInsets.only(right: 10),
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              icon: const IconTheme(
                data: IconThemeData(color: Color.fromRGBO(49, 49, 49, 1)),
                child: Icon(Icons.logout),
              ),
              label: MultiLanguageText(
                name: "logout",
                defaultValue: "Logout",
                isLowerCase: false,
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
              onPressed: () async {
                var securityModel = Provider.of<SecurityModel>(context, listen: false);
                var webElementModel = Provider.of<WebElementModel>(context, listen: false);
                var elementRepositoryModel = Provider.of<ElementRepositoryModel>(context, listen: false);
                var debugStatusModel = Provider.of<DebugStatusModel>(context, listen: false);
                var testCaseConfigModel = Provider.of<TestCaseConfigModel>(context, listen: false);
                var testCaseDirectoryModel = Provider.of<TestCaseDirectoryModel>(context, listen: false);
                var sideBarDocumentModel = Provider.of<SideBarDocumentModel>(context, listen: false);
                var playTestCaseModel = Provider.of<PlayTestCaseModel>(context, listen: false);
                await securityModel.clearProvider();
                await navigationModel.clearProvider();
                await webElementModel.clearProvider();
                await elementRepositoryModel.clearProvider();
                await debugStatusModel.clearProvider();
                await testCaseConfigModel.clearProvider();
                await testCaseDirectoryModel.clearProvider();
                await sideBarDocumentModel.clearProvider();
                await playTestCaseModel.clearProvider();
              },
            ),
          ),
        ],
      ),
    );
  }

  Container tableWidget(BuildContext context, NavigationModel navigationModel) {
    return Container(
      margin: const EdgeInsets.only(top: 30),
      child: CustomDataTableWidget(
        columns: [
          Expanded(
            flex: 2,
            child: MultiLanguageText(
              name: "name",
              defaultValue: "Name",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            flex: 2,
            child: MultiLanguageText(
              name: "description",
              defaultValue: "Description",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            child: MultiLanguageText(
              name: "create_date",
              defaultValue: "Create date",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            child: MultiLanguageText(
              name: "owner",
              defaultValue: "Owner",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            child: Center(
              child: MultiLanguageText(
                name: "status",
                defaultValue: "Status",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
          ),
          Container(
            width: 38,
          ),
        ],
        rows: [
          for (var prjProject in prjProjectResultList)
            CustomDataRow(
              cells: [
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.all(5),
                    child: Row(
                      children: [
                        const Icon(Icons.description, color: Color.fromRGBO(61, 148, 254, 1)),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Text(
                            prjProject["title"] ?? "",
                            maxLines: 3,
                            overflow: TextOverflow.ellipsis,
                            style: Style(context).styleTextDataCell,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text(
                    prjProject["description"] ?? "",
                    style: Style(context).styleTextDataCell,
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                Expanded(
                  child: Text(
                    DateFormat('dd-MM-yyyy').format(DateTime.parse(prjProject["createDate"])),
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                Expanded(
                  child: Text(
                    prjProject["sysUser"]["fullname"],
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                Expanded(
                  child: Center(
                    child: Badge2StatusSettingWidget(
                      status: prjProject["status"],
                    ),
                  ),
                ),
                PopupMenuButton(
                  offset: const Offset(5, 50),
                  tooltip: '',
                  splashRadius: 10,
                  icon: const Icon(
                    Icons.more_vert,
                  ),
                  itemBuilder: (context) => [
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.of(context).push(
                            createRoute(
                              (widget.type == "resource-pool")
                                  ? RPOptionProjectWidget(
                                      type: "edit",
                                      title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                      id: prjProject["id"],
                                      callbackOptionTest: (result) {
                                        handleEditProject(result, prjProject);
                                      })
                                  : (widget.type == "work-mgt")
                                      ? SettingProjectWorkMGTWidget(
                                          type: "edit",
                                          title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                          id: prjProject["id"],
                                          callbackOptionTest: (result) {
                                            handleEditProject(result, prjProject);
                                          })
                                      : DialogOptionProjectWidget(
                                          type: "edit",
                                          title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                          id: prjProject["id"],
                                          callbackOptionTest: (result) {
                                            handleEditProject(result, prjProject);
                                          }),
                            ),
                          );
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                        leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          dialogDeleteProject(prjProject);
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "delete", defaultValue: "Delete"),
                        leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                  ],
                ),
              ],
              onSelectChanged: () {
                if (widget.type != "resource-pool") {
                  Provider.of<ElementRepositoryModel>(context, listen: false).clearProvider();
                  navigationModel.setPrjProjectId(prjProject["id"]);
                  navigationModel.setPrjProject(prjProject);
                  if (widget.type == "qa-platform") {
                    navigationModel.navigate("/qa-platform/project/${prjProject["id"]}/insights/dashboard");
                  } else if (widget.type == "work-mgt") {
                    navigationModel.navigate("/work-mgt/project/${prjProject["id"]}/summary");
                  }
                }
              },
            ),
        ],
      ),
    );
  }

  dialogDeleteProject(prjProject) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogDelete(
          selectedItem: prjProject,
          callback: () {
            handleDeleteProject(prjProject);
          },
          type: 'project',
        );
      },
    );
  }
}
