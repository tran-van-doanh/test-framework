import 'package:flutter/material.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/row_code_with_tooltip.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';

import '../../../api.dart';
import '../../../common/custom_state.dart';
import '../../../components/dynamic_button.dart';

class DialogOptionRandomTypeWidget extends StatefulWidget {
  final String type;
  final String title;
  final String? id;
  final Function? callbackOptionTest;
  const DialogOptionRandomTypeWidget({Key? key, required this.type, this.id, this.callbackOptionTest, required this.title}) : super(key: key);

  @override
  State<DialogOptionRandomTypeWidget> createState() => _DialogOptionRandomTypeWidgetState();
}

class _DialogOptionRandomTypeWidgetState extends CustomState<DialogOptionRandomTypeWidget> {
  late Future future;
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _desController = TextEditingController();
  final TextEditingController _randomPrefixController = TextEditingController();
  final TextEditingController _randomSubfixController = TextEditingController();
  final TextEditingController _validCharactersController = TextEditingController();
  final TextEditingController _regexExpressionController = TextEditingController();
  final TextEditingController _randomLengthController = TextEditingController();
  int _status = 0;
  int randomUniqueValue = 0;
  int randomInList = 0;
  final _formKey = GlobalKey<FormState>();
  dynamic selectedItem;

  @override
  void initState() {
    future = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    if (widget.id != null) {
      await getSelectedItem(widget.id!);
      await setInitValue();
    }
    return 0;
  }

  getSelectedItem(String id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-random-type/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        selectedItem = response["body"]["result"];
      });
    }
  }

  setInitValue() {
    setState(() {
      if (selectedItem != null) {
        _status = selectedItem["status"] ?? 0;
        randomUniqueValue = selectedItem["randomUniqueValue"] ?? 0;
        randomInList = selectedItem["randomInList"] ?? 0;
        _nameController.text = selectedItem["name"] ?? "";
        _titleController.text = selectedItem["title"] ?? "";
        _randomPrefixController.text = selectedItem["randomPrefix"] ?? "";
        _randomSubfixController.text = selectedItem["randomSubfix"] ?? "";
        _validCharactersController.text = selectedItem["validCharacters"] ?? "";
        _regexExpressionController.text = selectedItem["regexExpression"] ?? "";
        _randomLengthController.text = selectedItem["randomLength"].toString();
        _desController.text = selectedItem["description"] ?? "";
      }
    });
  }

  @override
  void dispose() {
    _nameController.dispose();
    _titleController.dispose();
    _randomPrefixController.dispose();
    _randomSubfixController.dispose();
    _validCharactersController.dispose();
    _regexExpressionController.dispose();
    _randomLengthController.dispose();
    _desController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: future,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Scaffold(
            body: Column(
              children: [
                headerWidget(context),
                const Divider(color: Color.fromRGBO(216, 218, 229, 1)),
                Expanded(
                  child: Container(
                    margin: const EdgeInsets.only(top: 20, bottom: 10),
                    child: Form(
                      key: _formKey,
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                          child: bodyWidget(context),
                        ),
                      ),
                    ),
                  ),
                ),
                btnWidget()
              ],
            ),
          );
        } else if (snapshot.hasError) {
          return Text('${snapshot.error}');
        }
        return const Center(child: CircularProgressIndicator());
      },
    );
  }

  Padding btnWidget() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20, top: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ButtonSave(
            onPressed: () {
              if (_formKey.currentState!.validate()) {
                var result = {};
                if (selectedItem != null) {
                  result = selectedItem;
                  result["name"] = _nameController.text;
                  result["title"] = _titleController.text;
                  result["randomPrefix"] = _randomPrefixController.text;
                  result["randomSubfix"] = _randomSubfixController.text;
                  result["validCharacters"] = _validCharactersController.text;
                  result["regexExpression"] = _regexExpressionController.text;
                  result["randomLength"] = _randomLengthController.text;
                  result["description"] = _desController.text;
                  result["status"] = _status;
                  result["randomUniqueValue"] = randomUniqueValue;
                  result["randomInList"] = randomInList;
                } else {
                  result = {
                    "name": _nameController.text,
                    "title": _titleController.text,
                    "randomPrefix": _randomPrefixController.text,
                    "randomSubfix": _randomSubfixController.text,
                    "validCharacters": _validCharactersController.text,
                    "regexExpression": _regexExpressionController.text,
                    "randomLength": _randomLengthController.text,
                    "description": _desController.text,
                    "status": _status,
                    "randomUniqueValue": randomUniqueValue,
                    "randomInList": randomInList,
                  };
                }
                widget.callbackOptionTest!(result);
              }
            },
          ),
          const ButtonCancel()
        ],
      ),
    );
  }

  Column bodyWidget(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
            const Expanded(flex: 3, child: RowCodeWithTooltipWidget()),
            Expanded(
              flex: 7,
              child: DynamicTextField(
                controller: _nameController,
                hintText: "...",
                isRequiredNotEmpty: true,
                isRequiredRegex: true,
                enabled: widget.type != "edit",
              ),
            ),
          ]),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Expanded(flex: 3, child: MultiLanguageText(name: "name", defaultValue: "Name", style: Style(context).styleTitleDialog)),
            Expanded(
              flex: 7,
              child: DynamicTextField(
                controller: _titleController,
                hintText: "...",
                isRequiredNotEmpty: true,
              ),
            ),
          ]),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Expanded(flex: 3, child: MultiLanguageText(name: "description", defaultValue: "Description", style: Style(context).styleTitleDialog)),
            Expanded(
              flex: 7,
              child: DynamicTextField(
                controller: _desController,
                hintText: "...",
                maxline: 3,
              ),
            ),
          ]),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Expanded(flex: 3, child: MultiLanguageText(name: "status", defaultValue: "Status", style: Style(context).styleTitleDialog)),
            Expanded(
              flex: 7,
              child: DynamicDropdownButton(
                value: _status,
                hint: multiLanguageString(name: "choose_a_status", defaultValue: "Choose a status", context: context),
                items: [
                  {"name": multiLanguageString(name: "active", defaultValue: "Active", context: context), "value": 1},
                  {"name": multiLanguageString(name: "inactive", defaultValue: "Inactive", context: context), "value": 0},
                ].map<DropdownMenuItem<int>>((dynamic result) {
                  return DropdownMenuItem(
                    value: result["value"],
                    child: Text(result["name"]),
                  );
                }).toList(),
                onChanged: (newValue) {
                  setState(() {
                    _status = newValue!;
                  });
                },
                borderRadius: 5,
                isRequiredNotEmpty: true,
              ),
            ),
          ]),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Expanded(flex: 3, child: MultiLanguageText(name: "random_prefix", defaultValue: "Random prefix", style: Style(context).styleTitleDialog)),
            Expanded(
              flex: 7,
              child: DynamicTextField(
                controller: _randomPrefixController,
                hintText: "...",
              ),
            ),
          ]),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Expanded(flex: 3, child: MultiLanguageText(name: "random_suffix", defaultValue: "Random suffix", style: Style(context).styleTitleDialog)),
            Expanded(
              flex: 7,
              child: DynamicTextField(
                controller: _randomSubfixController,
                hintText: "...",
              ),
            ),
          ]),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Expanded(
                flex: 3,
                child: MultiLanguageText(name: "valid_characters", defaultValue: "Valid characters", style: Style(context).styleTitleDialog)),
            Expanded(
              flex: 7,
              child: DynamicTextField(
                controller: _validCharactersController,
                hintText: "...",
              ),
            ),
          ]),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Expanded(
                flex: 3,
                child: MultiLanguageText(name: "regex_expression", defaultValue: "Regex expression", style: Style(context).styleTitleDialog)),
            Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _regexExpressionController,
                  hintText: "...",
                )),
          ]),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Expanded(flex: 3, child: MultiLanguageText(name: "unique_value", defaultValue: "Unique Value", style: Style(context).styleTitleDialog)),
            Expanded(
              flex: 7,
              child: DynamicDropdownButton(
                value: randomUniqueValue,
                hint: multiLanguageString(name: "choose_a_random_unique_value", defaultValue: "Choose a random unique value", context: context),
                items: [
                  {"name": multiLanguageString(name: "item_unique_value", defaultValue: "Unique value", context: context), "value": 1},
                  {"name": multiLanguageString(name: "item_not_unique_value", defaultValue: "Not unique value", context: context), "value": 0},
                ].map<DropdownMenuItem<int>>((dynamic result) {
                  return DropdownMenuItem(
                    value: result["value"],
                    child: Text(result["name"]),
                  );
                }).toList(),
                onChanged: (newValue) {
                  setState(() {
                    randomUniqueValue = newValue!;
                  });
                },
                borderRadius: 5,
                isRequiredNotEmpty: true,
              ),
            ),
          ]),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                  flex: 3, child: MultiLanguageText(name: "random_in_list", defaultValue: "Random in list", style: Style(context).styleTitleDialog)),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                  value: randomInList,
                  hint: multiLanguageString(name: "choose_an_option", defaultValue: "Choose an option", context: context),
                  items: [
                    {"name": multiLanguageString(name: "item_in_list", defaultValue: "In list", context: context), "value": 1},
                    {"name": multiLanguageString(name: "item_not_in_list", defaultValue: "Not in list", context: context), "value": 0},
                  ].map<DropdownMenuItem<int>>((dynamic result) {
                    return DropdownMenuItem(
                      value: result["value"],
                      child: Text(result["name"]),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      randomInList = newValue!;
                    });
                  },
                  borderRadius: 5,
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                  flex: 3, child: MultiLanguageText(name: "random_length", defaultValue: "Random length", style: Style(context).styleTitleDialog)),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _randomLengthController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                  keyboardType: TextInputType.number,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Padding headerWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 35, 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "type_random_type",
            defaultValue: "\$0 RANDOM TYPE",
            variables: [widget.title],
            isLowerCase: false,
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Icon(
                Icons.close,
                color: Colors.black,
              ))
        ],
      ),
    );
  }
}
