import 'package:flutter/material.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/row_code_with_tooltip.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';

import '../../../../common/custom_state.dart';
import '../../../../components/dynamic_button.dart';

class DialogOptionRandomValueWidget extends StatefulWidget {
  final String type;
  final String title;
  final String? id;
  final Function? callbackOptionTest;
  final String tfRandomTypeId;
  const DialogOptionRandomValueWidget(
      {Key? key, required this.type, this.id, this.callbackOptionTest, required this.tfRandomTypeId, required this.title})
      : super(key: key);

  @override
  State<DialogOptionRandomValueWidget> createState() => _DialogOptionRandomValueWidgetState();
}

class _DialogOptionRandomValueWidgetState extends CustomState<DialogOptionRandomValueWidget> {
  late Future future;
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _desController = TextEditingController();
  final TextEditingController _valueController = TextEditingController();
  int _status = 0;
  String? _randomTypeId;
  final _formKey = GlobalKey<FormState>();
  var listRandomType = [];
  dynamic selectedItem;
  @override
  void initState() {
    future = getInfoPage();
    super.initState();
  }

  getInfoPage() async {
    await getListRandomType();
    if (widget.id != null) {
      await getSelectedItem(widget.id!);
    }
    await setInitValue();
    return 0;
  }

  getSelectedItem(String id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-random-value/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        selectedItem = response["body"]["result"];
      });
    }
  }

  getListRandomType() async {
    var searchRequest = {
      // "status": 1
    };
    var tfRandomTypeFindResponse = await httpPost("/test-framework-api/user/test-framework/tf-random-type/search", searchRequest, context);
    if (tfRandomTypeFindResponse.containsKey("body") && tfRandomTypeFindResponse["body"] is String == false) {
      setState(() {
        listRandomType = tfRandomTypeFindResponse["body"]["resultList"];
      });
    }
  }

  setInitValue() {
    setState(() {
      if (selectedItem != null) {
        _status = selectedItem["status"] ?? 0;
        _randomTypeId = selectedItem["tfRandomTypeId"];
        _nameController.text = selectedItem["name"] ?? "";
        _desController.text = selectedItem["description"] ?? "";
        _valueController.text = selectedItem["value"] ?? "";
      } else {
        _randomTypeId = widget.tfRandomTypeId;
      }
    });
  }

  @override
  void dispose() {
    _nameController.dispose();
    _desController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: future,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Scaffold(
            body: Column(
              children: [
                headerWidget(context),
                const Divider(color: Color.fromRGBO(216, 218, 229, 1)),
                Expanded(
                  child: Container(
                    margin: const EdgeInsets.only(top: 20, bottom: 10),
                    child: Form(
                      key: _formKey,
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                          child: bodyWidget(context),
                        ),
                      ),
                    ),
                  ),
                ),
                btnWidget()
              ],
            ),
          );
        } else if (snapshot.hasError) {
          return Text('${snapshot.error}');
        }
        return const Center(child: CircularProgressIndicator());
      },
    );
  }

  Padding btnWidget() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20, top: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ButtonSave(onPressed: () {
            if (_formKey.currentState!.validate()) {
              var result = {};
              if (selectedItem != null) {
                result = selectedItem;
                result["tfRandomTypeId"] = _randomTypeId;
                result["name"] = _nameController.text;
                result["description"] = _desController.text;
                result["value"] = _valueController.text;
                result["status"] = _status;
              } else {
                result = {
                  "tfRandomTypeId": _randomTypeId,
                  "name": _nameController.text,
                  "description": _desController.text,
                  "value": _valueController.text,
                  "status": _status,
                };
              }
              widget.callbackOptionTest!(result);
            }
          }),
          const ButtonCancel()
        ],
      ),
    );
  }

  Column bodyWidget(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(flex: 3, child: MultiLanguageText(name: "random_type", defaultValue: "Random type", style: Style(context).styleTitleDialog)),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                  value: _randomTypeId,
                  hint: multiLanguageString(name: "choose_a_value_type", defaultValue: "Choose a value type", context: context),
                  items: listRandomType.map<DropdownMenuItem<String>>(
                    (dynamic result) {
                      return DropdownMenuItem(
                        value: result["id"],
                        child: Text(result["title"]),
                      );
                    },
                  ).toList(),
                  onChanged: (newValue) {
                    setState(
                      () {
                        _formKey.currentState!.validate();
                        _randomTypeId = newValue!;
                      },
                    );
                  },
                  borderRadius: 5,
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Expanded(flex: 3, child: RowCodeWithTooltipWidget()),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _nameController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                  isRequiredRegex: true,
                  enabled: widget.type != "edit",
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(flex: 3, child: MultiLanguageText(name: "value", defaultValue: "Value", style: Style(context).styleTitleDialog)),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _valueController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(flex: 3, child: MultiLanguageText(name: "description", defaultValue: "Description", style: Style(context).styleTitleDialog)),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _desController,
                  hintText: "...",
                  maxline: 3,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Expanded(flex: 3, child: MultiLanguageText(name: "status", defaultValue: "Status", style: Style(context).styleTitleDialog)),
            Expanded(
              flex: 7,
              child: DynamicDropdownButton(
                value: _status,
                hint: multiLanguageString(name: "choose_a_status", defaultValue: "Choose a status", context: context),
                items: [
                  {"name": multiLanguageString(name: "active", defaultValue: "Active", context: context), "value": 1},
                  {"name": multiLanguageString(name: "inactive", defaultValue: "Inactive", context: context), "value": 0},
                ].map<DropdownMenuItem<int>>((dynamic result) {
                  return DropdownMenuItem(
                    value: result["value"],
                    child: Text(result["name"]),
                  );
                }).toList(),
                onChanged: (newValue) {
                  setState(() {
                    _status = newValue!;
                  });
                },
                borderRadius: 5,
                isRequiredNotEmpty: true,
              ),
            ),
          ]),
        ),
      ],
    );
  }

  Padding headerWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 35, 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "type_random_value",
            defaultValue: "\$0 RANDOM VALUE",
            variables: [widget.title],
            isLowerCase: false,
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
            onPressed: () => Navigator.pop(context),
            child: const Icon(
              Icons.close,
              color: Colors.black,
            ),
          )
        ],
      ),
    );
  }
}
