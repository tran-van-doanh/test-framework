import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/components/dialog/dialog_delete.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/setting_project/random_type/random_value/option_random_value.dart';

import '../../../../common/custom_state.dart';
import '../../../../type.dart';

class SettingRandomValue extends StatefulWidget {
  final String tfRandomTypeId;
  const SettingRandomValue({Key? key, required this.tfRandomTypeId}) : super(key: key);

  @override
  State<SettingRandomValue> createState() => _SettingRandomValueState();
}

class _SettingRandomValueState extends CustomState<SettingRandomValue> {
  late Future futuretfRandomValueList;
  var tfRandomValueSearchRequest = {};
  var tfRandomValueRowCount = 0;
  var tfRandomValueCurrentPage = 1;
  var tfRandomValueResultList = [];
  var tfRandomValueRowPerPage = 10;
  var tfRandomValueStatusMap = {1: "Active", 0: "Inactive"};

  @override
  void initState() {
    tfRandomValueSearch({});
    super.initState();
  }

  tfRandomValueSearch(tfRandomValueSearchRequest) {
    this.tfRandomValueSearchRequest = tfRandomValueSearchRequest;
    futuretfRandomValueList = tfRandomValuePageChange(tfRandomValueCurrentPage);
  }

  tfRandomValuePageChange(page) async {
    if ((page - 1) * tfRandomValueRowPerPage > tfRandomValueRowCount) {
      page = (1.0 * tfRandomValueRowCount / tfRandomValueRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {
      "queryOffset": (page - 1) * tfRandomValueRowPerPage,
      "queryLimit": tfRandomValueRowPerPage,
      "tfRandomTypeId": widget.tfRandomTypeId
    };
    if (tfRandomValueSearchRequest["nameLike"] != null && tfRandomValueSearchRequest["nameLike"].isNotEmpty) {
      findRequest["nameLike"] = "%${tfRandomValueSearchRequest["nameLike"]}%";
    }
    if (tfRandomValueSearchRequest["tfValueTypeId"] != null && tfRandomValueSearchRequest["tfValueTypeId"].isNotEmpty) {
      findRequest["tfValueTypeId"] = tfRandomValueSearchRequest["tfValueTypeId"];
    }
    var response = await httpPost("/test-framework-api/user/test-framework/tf-random-value/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        tfRandomValueCurrentPage = page;
        tfRandomValueRowCount = response["body"]["rowCount"];
        tfRandomValueResultList = response["body"]["resultList"];
      });
    }
    return 0;
  }

  handleDeleteRandomValue(tfRandomValue) async {
    var response = await httpDelete("/test-framework-api/user/test-framework/tf-random-value/${tfRandomValue["id"]}", context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        await tfRandomValuePageChange(tfRandomValueCurrentPage);
      }
    }
  }

  handleAddRandomValue(result) async {
    var response = await httpPut("/test-framework-api/user/test-framework/tf-random-value", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await tfRandomValuePageChange(tfRandomValueCurrentPage);
      }
    }
  }

  handleEditRandomValue(result, tfRandomValue) async {
    var response = await httpPost("/test-framework-api/user/test-framework/tf-random-value/${tfRandomValue["id"]}", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await tfRandomValuePageChange(tfRandomValueCurrentPage);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<NavigationModel>(builder: (context, navigationModel, child) {
      return FutureBuilder(
        future: futuretfRandomValueList,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Scaffold(
              body: Column(
                children: [
                  Expanded(
                    child: ListView(
                      children: [
                        OrganizationNavMenuTab(
                          margin: const EdgeInsets.fromLTRB(45, 20, 30, 0),
                          navigationList: context.organizationSettingsMenuList,
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      IconButton(
                                        onPressed: () {
                                          Navigator.pop(context);
                                        },
                                        icon: const Icon(Icons.arrow_back),
                                        padding: EdgeInsets.zero,
                                        splashRadius: 16,
                                      ),
                                      MultiLanguageText(
                                        name: "system_random_value_count",
                                        defaultValue: "System Random Value ( \$0 )",
                                        variables: ["$tfRandomValueRowCount"],
                                        style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              tableWidget(),
                              DynamicTablePagging(
                                tfRandomValueRowCount,
                                tfRandomValueCurrentPage,
                                tfRandomValueRowPerPage,
                                pageChangeHandler: (page) {
                                  setState(() {
                                    futuretfRandomValueList = tfRandomValuePageChange(page);
                                  });
                                },
                                rowPerPageChangeHandler: (rowPerPage) {
                                  setState(() {
                                    tfRandomValueRowPerPage = rowPerPage!;
                                    futuretfRandomValueList = tfRandomValuePageChange(1);
                                  });
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  btnWidget(context),
                ],
              ),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        },
      );
    });
  }

  Padding btnWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 40, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            margin: const EdgeInsets.only(right: 10),
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              icon: const IconTheme(
                data: IconThemeData(color: Color.fromRGBO(49, 49, 49, 1)),
                child: Icon(Icons.add),
              ),
              label: MultiLanguageText(
                name: "new_random_value",
                defaultValue: "New Random Value",
                isLowerCase: false,
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
              onPressed: () {
                Navigator.of(context).push(
                  createRoute(
                    DialogOptionRandomValueWidget(
                        type: "create",
                        title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                        tfRandomTypeId: widget.tfRandomTypeId,
                        callbackOptionTest: (result) {
                          handleAddRandomValue(result);
                        }),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Container tableWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 30),
      child: CustomDataTableWidget(
        columns: [
          Expanded(
            flex: 2,
            child: MultiLanguageText(
              name: "code",
              defaultValue: "Code",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            flex: 2,
            child: MultiLanguageText(
              name: "description",
              defaultValue: "Description",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            flex: 2,
            child: MultiLanguageText(
              name: "value",
              defaultValue: "Value",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            child: MultiLanguageText(
              name: "status",
              defaultValue: "Status",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          const SizedBox(
            width: 38,
          ),
        ],
        rows: [
          for (var tfRandomValue in tfRandomValueResultList)
            CustomDataRow(
              cells: [
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.all(5),
                    child: Row(
                      children: [
                        const Icon(Icons.description, color: Color.fromRGBO(61, 148, 254, 1)),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Text(
                            tfRandomValue["name"] ?? "",
                            overflow: TextOverflow.ellipsis,
                            style: Style(context).styleTextDataCell,
                            maxLines: 3,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text(
                    tfRandomValue["description"] ?? "",
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text(
                    tfRandomValue["value"] ?? "",
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                Expanded(
                  child: Text(
                    tfRandomValueStatusMap[tfRandomValue["status"]] ?? "Not defined",
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                PopupMenuButton(
                  offset: const Offset(5, 50),
                  tooltip: '',
                  splashRadius: 10,
                  icon: const Icon(
                    Icons.more_vert,
                  ),
                  itemBuilder: (context) => [
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          dialogDeleteRandomValue(tfRandomValue);
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "delete", defaultValue: "Delete"),
                        leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.of(context).push(
                            createRoute(
                              DialogOptionRandomValueWidget(
                                type: "edit",
                                title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                id: tfRandomValue["id"],
                                callbackOptionTest: (result) {
                                  handleEditRandomValue(result, tfRandomValue);
                                },
                                tfRandomTypeId: widget.tfRandomTypeId,
                              ),
                            ),
                          );
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                        leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                  ],
                ),
              ],
            ),
        ],
      ),
    );
  }

  dialogDeleteRandomValue(tfRandomValue) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogDelete(
          selectedItem: tfRandomValue,
          callback: () {
            handleDeleteRandomValue(tfRandomValue);
          },
          type: 'random_value',
        );
      },
    );
  }
}
