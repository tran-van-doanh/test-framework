import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/components/dialog/dialog_delete.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/setting_project/random_type/option_random_type.dart';
import 'package:test_framework_view/page/setting_project/random_type/random_value/setting_random_value.dart';
import 'package:test_framework_view/type.dart';

import '../../../common/custom_state.dart';

class SettingRandomTypeWidget extends StatefulWidget {
  const SettingRandomTypeWidget({Key? key}) : super(key: key);

  @override
  State<SettingRandomTypeWidget> createState() => _SettingRandomTypeWidgetState();
}

class _SettingRandomTypeWidgetState extends CustomState<SettingRandomTypeWidget> {
  late Future futuretfRandomTypeList;
  var tfRandomTypeSearchRequest = {};
  var tfRandomTypeRowCount = 0;
  var tfRandomTypeCurrentPage = 1;
  var tfRandomTypeResultList = [];
  var tfRandomTypeRowPerPage = 10;
  var tfRandomTypeStatusMap = {1: "Active", 0: "Inactive"};

  @override
  void initState() {
    tfRandomTypeSearch({});
    super.initState();
  }

  tfRandomTypeSearch(tfRandomTypeSearchRequest) {
    this.tfRandomTypeSearchRequest = tfRandomTypeSearchRequest;
    futuretfRandomTypeList = tfRandomTypePageChange(tfRandomTypeCurrentPage);
  }

  tfRandomTypePageChange(page) async {
    if ((page - 1) * tfRandomTypeRowPerPage > tfRandomTypeRowCount) {
      page = (1.0 * tfRandomTypeRowCount / tfRandomTypeRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * tfRandomTypeRowPerPage, "queryLimit": tfRandomTypeRowPerPage};
    if (tfRandomTypeSearchRequest["nameLike"] != null && tfRandomTypeSearchRequest["nameLike"].isNotEmpty) {
      findRequest["nameLike"] = "%${tfRandomTypeSearchRequest["nameLike"]}%";
    }
    var response = await httpPost("/test-framework-api/user/test-framework/tf-random-type/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        tfRandomTypeCurrentPage = page;
        tfRandomTypeRowCount = response["body"]["rowCount"];
        tfRandomTypeResultList = response["body"]["resultList"];
      });
    }
    return 0;
  }

  handleDeleteRandomType(resultRandomType) async {
    var response = await httpDelete("/test-framework-api/user/test-framework/tf-random-type/${resultRandomType["id"]}", context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        await tfRandomTypePageChange(tfRandomTypeCurrentPage);
      }
    }
  }

  handleDeleteAllRandomType(id) async {
    var response = await httpDelete("/test-framework-api/user/test-framework/tf-random-type/$id/tf-random-value", context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
      }
    }
  }

  handleAddRandomType(result) async {
    var response = await httpPut("/test-framework-api/user/test-framework/tf-random-type", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await tfRandomTypePageChange(tfRandomTypeCurrentPage);
      }
    }
  }

  handleEditRandomType(result, resultRandomType) async {
    var response = await httpPost("/test-framework-api/user/test-framework/tf-random-type/${resultRandomType["id"]}", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await tfRandomTypePageChange(tfRandomTypeCurrentPage);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<NavigationModel>(builder: (context, navigationModel, child) {
      return FutureBuilder(
        future: futuretfRandomTypeList,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Scaffold(
              body: Column(
                children: [
                  Expanded(
                    child: ListView(
                      children: [
                        OrganizationNavMenuTab(
                          margin: const EdgeInsets.fromLTRB(45, 20, 30, 0),
                          navigationList: context.organizationSettingsMenuList,
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  MultiLanguageText(
                                    name: "random_type_count",
                                    defaultValue: "Random Type ( \$0 )",
                                    variables: ["$tfRandomTypeRowCount"],
                                    style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
                                  ),
                                ],
                              ),
                              tableWidget(),
                              DynamicTablePagging(
                                tfRandomTypeRowCount,
                                tfRandomTypeCurrentPage,
                                tfRandomTypeRowPerPage,
                                pageChangeHandler: (page) {
                                  setState(() {
                                    futuretfRandomTypeList = tfRandomTypePageChange(page);
                                  });
                                },
                                rowPerPageChangeHandler: (rowPerPage) {
                                  setState(() {
                                    tfRandomTypeRowPerPage = rowPerPage!;
                                    futuretfRandomTypeList = tfRandomTypePageChange(1);
                                  });
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  btnWidget(context),
                ],
              ),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        },
      );
    });
  }

  Padding btnWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 40, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            margin: const EdgeInsets.only(right: 10),
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              icon: const IconTheme(
                data: IconThemeData(color: Color.fromRGBO(49, 49, 49, 1)),
                child: Icon(Icons.add),
              ),
              label: MultiLanguageText(
                name: "new_random_type",
                defaultValue: "New Random Type",
                isLowerCase: false,
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
              onPressed: () {
                Navigator.of(context).push(
                  createRoute(DialogOptionRandomTypeWidget(
                      type: "create",
                      title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                      callbackOptionTest: (result) {
                        handleAddRandomType(result);
                      })),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Container tableWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 30),
      child: CustomDataTableWidget(
        columns: [
          Expanded(
            flex: 3,
            child: MultiLanguageText(
              name: "code",
              defaultValue: "Code",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            flex: 3,
            child: MultiLanguageText(
              name: "name",
              defaultValue: "Name",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            flex: 3,
            child: MultiLanguageText(
              name: "description",
              defaultValue: "Description",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            child: MultiLanguageText(
              name: "status",
              defaultValue: "Status",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          const SizedBox(
            width: 38,
          ),
        ],
        rows: [
          for (var resultRandomType in tfRandomTypeResultList)
            CustomDataRow(
              cells: [
                Expanded(
                  flex: 3,
                  child: Padding(
                    padding: const EdgeInsets.all(5),
                    child: Row(
                      children: [
                        const Icon(Icons.description, color: Color.fromRGBO(61, 148, 254, 1)),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Text(
                            resultRandomType["name"] ?? "",
                            overflow: TextOverflow.ellipsis,
                            style: Style(context).styleTextDataCell,
                            maxLines: 3,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: Text(
                    resultRandomType["title"] ?? "",
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: Text(
                    resultRandomType["description"] ?? "",
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                Expanded(
                  child: Text(
                    tfRandomTypeStatusMap[resultRandomType["status"]] ?? "Not defined",
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                PopupMenuButton(
                  offset: const Offset(5, 50),
                  tooltip: '',
                  splashRadius: 10,
                  icon: const Icon(
                    Icons.more_vert,
                  ),
                  itemBuilder: (context) => [
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.of(context).push(
                            createRoute(DialogOptionRandomTypeWidget(
                                type: "edit",
                                title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                id: resultRandomType["id"],
                                callbackOptionTest: (result) {
                                  handleEditRandomType(result, resultRandomType);
                                })),
                          );
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                        leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.of(context).push(
                            createRoute(
                              SettingRandomValue(tfRandomTypeId: resultRandomType["id"]),
                            ),
                          );
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "random_value", defaultValue: "Random value"),
                        leading: const Icon(Icons.settings, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          dialogDeleteRandomType(resultRandomType, "delete");
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "delete", defaultValue: "Delete"),
                        leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          dialogDeleteRandomType(resultRandomType, "delete_all");
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "delete_all_value", defaultValue: "Delete all value"),
                        leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                  ],
                ),
              ],
            ),
        ],
      ),
    );
  }

  dialogDeleteRandomType(resultRandomType, type) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogDelete(
          selectedItem: resultRandomType,
          callback: () {
            if (type == "delete") {
              handleDeleteRandomType(resultRandomType);
            } else if (type == "delete_all") {
              handleDeleteAllRandomType(resultRandomType["id"]);
            }
          },
          type: 'random_type',
        );
      },
    );
  }
}
