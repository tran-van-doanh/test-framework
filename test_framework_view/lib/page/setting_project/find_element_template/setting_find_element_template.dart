import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/components/dialog/dialog_delete.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/setting_project/find_element_template/option_find_element_template.dart';
import 'package:test_framework_view/type.dart';

import '../../../common/custom_state.dart';

class SettingFindElementTemplateWidget extends StatefulWidget {
  final String prjProjectTypeId;
  const SettingFindElementTemplateWidget({Key? key, required this.prjProjectTypeId}) : super(key: key);

  @override
  State<SettingFindElementTemplateWidget> createState() => _SettingFindElementTemplateWidgetState();
}

class _SettingFindElementTemplateWidgetState extends CustomState<SettingFindElementTemplateWidget> {
  late Future futurefindElementTemplateList;
  var findElementTemplateSearchRequest = {};
  var findElementTemplateRowCount = 0;
  var findElementTemplateCurrentPage = 1;
  var findElementTemplateResultList = [];
  var findElementTemplateRowPerPage = 10;
  var findElementTemplateStatusMap = {1: "Active", 0: "Inactive"};

  @override
  void initState() {
    findElementTemplateSearch({});
    super.initState();
  }

  findElementTemplateSearch(findElementTemplateSearchRequest) {
    this.findElementTemplateSearchRequest = findElementTemplateSearchRequest;
    futurefindElementTemplateList = findElementTemplatePageChange(findElementTemplateCurrentPage);
  }

  findElementTemplatePageChange(page) async {
    if ((page - 1) * findElementTemplateRowPerPage > findElementTemplateRowCount) {
      page = (1.0 * findElementTemplateRowCount / findElementTemplateRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {
      "queryOffset": (page - 1) * findElementTemplateRowPerPage,
      "queryLimit": findElementTemplateRowPerPage,
      "prjProjectTypeId": widget.prjProjectTypeId
    };
    if (findElementTemplateSearchRequest["titleLike"] != null && findElementTemplateSearchRequest["titleLike"].isNotEmpty) {
      findRequest["titleLike"] = "%${findElementTemplateSearchRequest["titleLike"]}%";
    }
    var response = await httpPost("/test-framework-api/user/test-framework/tf-find-element-template/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        findElementTemplateCurrentPage = page;
        findElementTemplateRowCount = response["body"]["rowCount"];
        findElementTemplateResultList = response["body"]["resultList"];
      });
    }
    return 0;
  }

  handleDeleteFindElementTemplate(findElementTemplate) async {
    var response = await httpDelete("/test-framework-api/user/test-framework/tf-find-element-template/${findElementTemplate["id"]}", context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        await findElementTemplatePageChange(findElementTemplateCurrentPage);
      }
    }
  }

  handleAddFindElementTemplate(result) async {
    var response = await httpPut("/test-framework-api/user/test-framework/tf-find-element-template", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await findElementTemplatePageChange(findElementTemplateCurrentPage);
      }
    }
  }

  handleEditFindElementTemplate(result, findElementTemplate) async {
    var response = await httpPost("/test-framework-api/user/test-framework/tf-find-element-template/${findElementTemplate["id"]}", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await findElementTemplatePageChange(findElementTemplateCurrentPage);
      }
    }
  }

  handleDuplicateFindElementTemplate(result) async {
    var response = await httpPut("/test-framework-api/user/test-framework/tf-find-element-template", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await findElementTemplatePageChange(findElementTemplateCurrentPage);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<NavigationModel>(builder: (context, navigationModel, child) {
      return FutureBuilder(
        future: futurefindElementTemplateList,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Scaffold(
              body: Column(
                children: [
                  Expanded(
                    child: ListView(
                      children: [
                        OrganizationNavMenuTab(
                          margin: const EdgeInsets.fromLTRB(45, 20, 30, 0),
                          navigationList: context.organizationSettingsMenuList,
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
                          child: Column(
                            children: [
                              headerWidget(context),
                              tableWidget(),
                              DynamicTablePagging(
                                findElementTemplateRowCount,
                                findElementTemplateCurrentPage,
                                findElementTemplateRowPerPage,
                                pageChangeHandler: (page) {
                                  setState(() {
                                    futurefindElementTemplateList = findElementTemplatePageChange(page);
                                  });
                                },
                                rowPerPageChangeHandler: (rowPerPage) {
                                  setState(() {
                                    findElementTemplateRowPerPage = rowPerPage!;
                                    futurefindElementTemplateList = findElementTemplatePageChange(1);
                                  });
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  btnWidget(context),
                ],
              ),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        },
      );
    });
  }

  Padding btnWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 40, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            margin: const EdgeInsets.only(right: 10),
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              icon: const IconTheme(
                data: IconThemeData(color: Color.fromRGBO(49, 49, 49, 1)),
                child: Icon(Icons.add),
              ),
              label: MultiLanguageText(
                name: "new_element_template",
                defaultValue: "New Element Template",
                isLowerCase: false,
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
              onPressed: () {
                Navigator.of(context).push(
                  createRoute(DialogOptionFindElementTemplateWidget(
                    type: "create",
                    title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                    callbackOptionTest: (result) {
                      handleAddFindElementTemplate(result);
                    },
                    prjProjectTypeId: widget.prjProjectTypeId,
                  )),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Container tableWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 30),
      child: CustomDataTableWidget(
        columns: [
          Expanded(
            flex: 2,
            child: MultiLanguageText(
              name: "name",
              defaultValue: "Name",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            flex: 2,
            child: MultiLanguageText(
              name: "description",
              defaultValue: "Description",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            child: MultiLanguageText(
              name: "project_type",
              defaultValue: "Project Type",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            child: MultiLanguageText(
              name: "status",
              defaultValue: "Status",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          const SizedBox(
            width: 38,
          ),
        ],
        rows: [
          for (var findElementTemplate in findElementTemplateResultList)
            CustomDataRow(
              cells: [
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.all(5),
                    child: Row(
                      children: [
                        const Icon(Icons.description, color: Color.fromRGBO(61, 148, 254, 1)),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Text(
                            findElementTemplate["title"] ?? "",
                            overflow: TextOverflow.ellipsis,
                            maxLines: 3,
                            style: Style(context).styleTextDataCell,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text(
                    findElementTemplate["description"] ?? "",
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                Expanded(
                  child: Text(
                    findElementTemplate["prjProjectType"]["title"] ?? "",
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                Expanded(
                  child: Text(
                    findElementTemplateStatusMap[findElementTemplate["status"]] ?? "Not defined",
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                PopupMenuButton(
                  offset: const Offset(5, 50),
                  tooltip: '',
                  splashRadius: 10,
                  icon: const Icon(
                    Icons.more_vert,
                  ),
                  itemBuilder: (context) => [
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.of(context).push(
                            createRoute(
                              DialogOptionFindElementTemplateWidget(
                                type: "edit",
                                title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                id: findElementTemplate["id"],
                                callbackOptionTest: (result) {
                                  handleEditFindElementTemplate(result, findElementTemplate);
                                },
                                prjProjectTypeId: widget.prjProjectTypeId,
                              ),
                            ),
                          );
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                        leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          dialogDeleteFindElementTemplate(findElementTemplate);
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "delete", defaultValue: "Delete"),
                        leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.of(context).push(
                            createRoute(
                              DialogOptionFindElementTemplateWidget(
                                type: "duplicate",
                                title: multiLanguageString(name: "duplicate", defaultValue: "Duplicate", context: context),
                                id: findElementTemplate["id"],
                                callbackOptionTest: (result) {
                                  handleDuplicateFindElementTemplate(result);
                                },
                                prjProjectTypeId: widget.prjProjectTypeId,
                              ),
                            ),
                          );
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "duplicate", defaultValue: "Duplicate"),
                        leading: const Icon(Icons.control_point_duplicate, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                  ],
                ),
              ],
            ),
        ],
      ),
    );
  }

  Row headerWidget(BuildContext context) {
    return Row(
      children: [
        IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(Icons.arrow_back),
          padding: EdgeInsets.zero,
          splashRadius: 16,
        ),
        MultiLanguageText(
          name: "element_template_count",
          defaultValue: "Element Template ( \$0)",
          variables: ["$findElementTemplateRowCount"],
          style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
        ),
      ],
    );
  }

  dialogDeleteFindElementTemplate(findElementTemplate) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogDelete(
          selectedItem: findElementTemplate,
          callback: () {
            handleDeleteFindElementTemplate(findElementTemplate);
          },
          type: 'find_element_template',
        );
      },
    );
  }
}
