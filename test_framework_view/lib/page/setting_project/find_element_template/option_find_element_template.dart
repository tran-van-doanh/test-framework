import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_dropdown_button.dart';
import 'package:test_framework_view/common/dynamic_form.dart';
import 'package:test_framework_view/common/row_code_with_tooltip.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/model/model.dart';

import '../../../common/custom_state.dart';
import '../../../components/dynamic_button.dart';

class DialogOptionFindElementTemplateWidget extends StatefulWidget {
  final String type;
  final String title;
  final String? id;
  final String prjProjectTypeId;
  final Function? callbackOptionTest;
  const DialogOptionFindElementTemplateWidget(
      {Key? key, required this.type, this.id, this.callbackOptionTest, required this.prjProjectTypeId, required this.title})
      : super(key: key);

  @override
  State<DialogOptionFindElementTemplateWidget> createState() => _DialogOptionFindElementTemplateWidgetState();
}

class _DialogOptionFindElementTemplateWidgetState extends CustomState<DialogOptionFindElementTemplateWidget> {
  late Future future;
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  int _status = 0;
  String? projectType;
  List projectTypeList = [];
  final _formKey = GlobalKey<FormState>();
  var content = {};
  String? platform;
  dynamic selectedItem;
  @override
  void initState() {
    future = getInfoPage();
    super.initState();
  }

  @override
  void dispose() {
    _nameController.dispose();
    _titleController.dispose();
    _descriptionController.dispose();
    super.dispose();
  }

  getInfoPage() async {
    await getListProjectType();
    if (widget.id != null) {
      await getSelectedItem(widget.id!);
    }
    await setInitValue();
    return 0;
  }

  getSelectedItem(String id) async {
    var response = await httpGet("/test-framework-api/user/test-framework/tf-find-element-template/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        selectedItem = response["body"]["result"];
      });
    }
  }

  getListProjectType() async {
    var findRequest = {};
    var response = await httpPost("/test-framework-api/user/project/prj-project-type/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        projectTypeList = response["body"]["resultList"];
      });
    }
  }

  setInitValue() {
    setState(() {
      if (selectedItem != null) {
        _status = selectedItem["status"] ?? 0;
        if (widget.type == "duplicate") {
          _nameController.text = "copy_of_${selectedItem["name"]}";
          _titleController.text = "copy_of_${selectedItem["title"]}";
        } else {
          _nameController.text = selectedItem["name"] ?? "";
          _titleController.text = selectedItem["title"] ?? "";
        }
        _descriptionController.text = selectedItem["description"] ?? "";
        projectType = selectedItem["prjProjectTypeId"];
        content = selectedItem["content"] ?? {"type": "findElements"};
        platform = selectedItem["platform"];
      } else {
        var projectTypeSearch = projectTypeList.firstWhere((element) => element["id"] == widget.prjProjectTypeId);
        projectType = projectTypeSearch["id"];
        platform = projectTypeSearch["platform"];
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: future,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Scaffold(
              body: Column(
            children: [
              headerWidget(context),
              const Divider(color: Color.fromRGBO(216, 218, 229, 1)),
              Expanded(
                child: Container(
                  margin: const EdgeInsets.only(top: 20, bottom: 10),
                  child: Form(
                    key: _formKey,
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
                        child: bodyWidget(),
                      ),
                    ),
                  ),
                ),
              ),
              btnWidget()
            ],
          ));
        } else if (snapshot.hasError) {
          return Text('${snapshot.error}');
        }
        return const Center(child: CircularProgressIndicator());
      },
    );
  }

  Padding btnWidget() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20, top: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ButtonSave(onPressed: () {
            if (_formKey.currentState!.validate()) {
              var result = {};
              if (selectedItem != null && selectedItem.isNotEmpty && widget.type != "duplicate") {
                result = selectedItem;
                result["name"] = _nameController.text;
                result["title"] = _titleController.text;
                result["prjProjectTypeId"] = projectType;
                result["description"] = _descriptionController.text;
                result["status"] = _status;
                result["content"] = content;
                result["platform"] = platform;
              } else {
                result = {
                  "name": _nameController.text,
                  "title": _titleController.text,
                  "prjProjectTypeId": projectType,
                  "content": content,
                  "description": _descriptionController.text,
                  "status": _status,
                  "platform": platform,
                };
              }
              widget.callbackOptionTest!(result);
            }
          }),
          const ButtonCancel()
        ],
      ),
    );
  }

  Column bodyWidget() {
    var securityModel = Provider.of<SecurityModel>(context, listen: false);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "project_type", defaultValue: "Project type", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                  value: projectType,
                  hint: multiLanguageString(name: "choose_a_project_type", defaultValue: "Choose a project type", context: context),
                  items: projectTypeList.map<DropdownMenuItem<String>>((dynamic result) {
                    return DropdownMenuItem(
                      value: result["id"],
                      child: Text(result["title"]),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      projectType = newValue!;
                    });
                  },
                  borderRadius: 5,
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "platform", defaultValue: "Platform", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                    borderRadius: 5,
                    value: platform,
                    hint: multiLanguageString(name: "choose_a_platform", defaultValue: "Choose a platform", context: context),
                    items: (securityModel.sysOrganizationPlatformsList ?? []).map<DropdownMenuItem<String>>((String result) {
                      return DropdownMenuItem(
                        value: result,
                        child: Text(result),
                      );
                    }).toList(),
                    onChanged: (newValue) async {
                      setState(() {
                        platform = newValue!;
                      });
                    },
                    isRequiredNotEmpty: true),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Expanded(
                flex: 3,
                child: RowCodeWithTooltipWidget(),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _nameController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                  enabled: widget.type != "edit",
                  isRequiredRegex: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "name", defaultValue: "Name", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicTextField(
                  controller: _titleController,
                  hintText: "...",
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 3,
                  child: MultiLanguageText(name: "description", defaultValue: "Description", style: Style(context).styleTitleDialog),
                ),
                Expanded(
                  flex: 7,
                  child: DynamicTextField(
                    controller: _descriptionController,
                    hintText: "...",
                    maxline: 3,
                  ),
                ),
              ],
            )),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "status", defaultValue: "Status", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: DynamicDropdownButton(
                  value: _status,
                  hint: multiLanguageString(name: "choose_a_status", defaultValue: "Choose a status", context: context),
                  items: [
                    {"name": multiLanguageString(name: "active", defaultValue: "Active", context: context), "value": 1},
                    {"name": multiLanguageString(name: "inactive", defaultValue: "Inactive", context: context), "value": 0},
                  ].map<DropdownMenuItem<int>>((dynamic result) {
                    return DropdownMenuItem(
                      value: result["value"],
                      child: Text(result["name"]),
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() {
                      _status = newValue!;
                    });
                  },
                  borderRadius: 5,
                  isRequiredNotEmpty: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: MultiLanguageText(name: "config", defaultValue: "Config", style: Style(context).styleTitleDialog),
              ),
              Expanded(
                flex: 7,
                child: TfFindElementTemplateContentWidget(
                  content: content,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Padding headerWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 20, 35, 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MultiLanguageText(
            name: "type_element_template",
            defaultValue: "\$0 ELEMENT TEMPLATE",
            variables: [widget.title],
            isLowerCase: false,
            style: Style(context).styleTitleDialog,
          ),
          TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Icon(
                Icons.close,
                color: Colors.black,
              ))
        ],
      ),
    );
  }
}

class TfFindElementTemplateContentWidget extends StatefulWidget {
  final dynamic content;
  const TfFindElementTemplateContentWidget({required this.content, Key? key}) : super(key: key);

  @override
  State<TfFindElementTemplateContentWidget> createState() => _TfFindElementTemplateContentWidgetState();
}

class _TfFindElementTemplateContentWidgetState extends CustomState<TfFindElementTemplateContentWidget> {
  showEditDialog(parentList, data, type) {
    if (type == "elementPath") {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext dialogContext) {
          return Dialog(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Expanded(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Expanded(
                                child: Padding(
                                    padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                                    child: TextField(
                                      controller: TextEditingController(text: data["from"] ?? ""),
                                      onChanged: (value) {
                                        data["from"] = value;
                                      },
                                      decoration: InputDecoration(
                                        label: const MultiLanguageText(name: "from", defaultValue: "from"),
                                        hintText: multiLanguageString(name: "from_input", defaultValue: "From input", context: context),
                                        border: const OutlineInputBorder(),
                                      ),
                                    )))
                          ],
                        ),
                        Row(
                          children: [
                            Expanded(
                                child: Padding(
                                    padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                                    child: TextField(
                                      controller: TextEditingController(text: data["name"] ?? ""),
                                      onChanged: (value) {
                                        data["name"] = value;
                                      },
                                      decoration: InputDecoration(
                                        label: const MultiLanguageText(name: "name", defaultValue: "Name"),
                                        hintText: multiLanguageString(name: "name_input", defaultValue: "Name input", context: context),
                                        border: const OutlineInputBorder(),
                                      ),
                                    )))
                          ],
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                                child: TextField(
                                  controller: TextEditingController(text: data["xpath"] ?? ""),
                                  onChanged: (value) {
                                    data["xpath"] = value;
                                  },
                                  decoration: InputDecoration(
                                    label: const MultiLanguageText(name: "xpath", defaultValue: "Xpath"),
                                    hintText: multiLanguageString(name: "xpath_input", defaultValue: "Xpath input", context: context),
                                    border: const OutlineInputBorder(),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ElevatedButton(
                              onPressed: () {
                                Navigator.pop(dialogContext, true);
                                setState(() {});
                              },
                              child: const MultiLanguageText(name: "close", defaultValue: "Close"),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      );
    }
    if (type == "resultXpath") {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext dialogContext) {
          return Dialog(
              child: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                    child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                            child: Padding(
                                padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                                child: TextField(
                                  controller: TextEditingController(text: data["name"] ?? ""),
                                  onChanged: (value) {
                                    data["name"] = value;
                                  },
                                  decoration: InputDecoration(
                                    label: const MultiLanguageText(name: "name", defaultValue: "Name"),
                                    hintText: multiLanguageString(name: "name_input", defaultValue: "Name input", context: context),
                                    border: const OutlineInputBorder(),
                                  ),
                                )))
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                          onPressed: () {
                            Navigator.pop(dialogContext, true);
                            setState(() {});
                          },
                          child: const MultiLanguageText(name: "close", defaultValue: "Close"),
                        )
                      ],
                    )
                  ],
                ))
              ],
            ),
          ));
        },
      );
    }
    if (type == "xpath") {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext dialogContext) {
          return Dialog(
              child: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                    child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                            child: Padding(
                                padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                                child: TextField(
                                  controller: TextEditingController(text: data["path"] ?? ""),
                                  onChanged: (value) {
                                    data["path"] = value;
                                  },
                                  decoration: InputDecoration(
                                    label: const MultiLanguageText(name: "path", defaultValue: "Path"),
                                    hintText: multiLanguageString(name: "path_input", defaultValue: "Path input", context: context),
                                    border: const OutlineInputBorder(),
                                  ),
                                )))
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                          onPressed: () {
                            Navigator.pop(dialogContext, true);
                            setState(() {});
                          },
                          child: const MultiLanguageText(name: "close", defaultValue: "Close"),
                        )
                      ],
                    )
                  ],
                ))
              ],
            ),
          ));
        },
      );
    }
    if (type == "variable") {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext dialogContext) {
          return Dialog(
              child: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                    child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                            child: Padding(
                                padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                                child: TextField(
                                  controller: TextEditingController(text: data["name"] ?? ""),
                                  onChanged: (value) {
                                    data["name"] = value;
                                  },
                                  decoration: InputDecoration(
                                    label: const MultiLanguageText(name: "name", defaultValue: "Name"),
                                    hintText: multiLanguageString(name: "name_input", defaultValue: "Name input", context: context),
                                    border: const OutlineInputBorder(),
                                  ),
                                )))
                      ],
                    ),
                    MyDropdown(
                        label: multiLanguageString(name: "type", defaultValue: "Type", context: context),
                        hintText: multiLanguageString(name: "type_input", defaultValue: "Type input", context: context),
                        selectedValue: data["type"] ?? "value",
                        optionList: const [
                          {"value": "value", "label": "value"},
                          {"value": "contains", "label": "contains"}
                        ],
                        onChanged: (value) {
                          data["type"] = value;
                        }),
                    Row(
                      children: [
                        Expanded(
                            child: Padding(
                                padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                                child: TextField(
                                  controller: TextEditingController(text: data["elementName"] ?? ""),
                                  onChanged: (value) {
                                    data["elementName"] = value;
                                  },
                                  decoration: InputDecoration(
                                    label: const MultiLanguageText(name: "element", defaultValue: "Element"),
                                    hintText: multiLanguageString(name: "element_input", defaultValue: "Element input", context: context),
                                    border: const OutlineInputBorder(),
                                  ),
                                )))
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                            child: Padding(
                                padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                                child: TextField(
                                  controller: TextEditingController(text: data["attributeName"] ?? ""),
                                  onChanged: (value) {
                                    data["attributeName"] = value;
                                  },
                                  decoration: InputDecoration(
                                    label: const MultiLanguageText(name: "attribute", defaultValue: "Attribute"),
                                    hintText: multiLanguageString(name: "attribute_input", defaultValue: "Attribute input", context: context),
                                    border: const OutlineInputBorder(),
                                  ),
                                )))
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                          onPressed: () {
                            Navigator.pop(dialogContext, true);
                            setState(() {});
                          },
                          child: const MultiLanguageText(name: "close", defaultValue: "Close"),
                        )
                      ],
                    )
                  ],
                ))
              ],
            ),
          ));
        },
      );
    }
    if (type == "resultAttribute") {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext dialogContext) {
          return Dialog(
              child: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                    child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                            child: Padding(
                                padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                                child: TextField(
                                  controller: TextEditingController(text: data["name"] ?? ""),
                                  onChanged: (value) {
                                    data["name"] = value;
                                  },
                                  decoration: InputDecoration(
                                    label: const MultiLanguageText(name: "name", defaultValue: "Name"),
                                    hintText: multiLanguageString(name: "name_input", defaultValue: "Name input", context: context),
                                    border: const OutlineInputBorder(),
                                  ),
                                )))
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                            child: Padding(
                                padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                                child: TextField(
                                  controller: TextEditingController(text: data["elementName"] ?? ""),
                                  onChanged: (value) {
                                    data["elementName"] = value;
                                  },
                                  decoration: InputDecoration(
                                    label: const MultiLanguageText(name: "element", defaultValue: "Element"),
                                    hintText: multiLanguageString(name: "element_input", defaultValue: "Element input", context: context),
                                    border: const OutlineInputBorder(),
                                  ),
                                )))
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                            child: Padding(
                                padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                                child: TextField(
                                  controller: TextEditingController(text: data["attributeName"] ?? ""),
                                  onChanged: (value) {
                                    data["attributeName"] = value;
                                  },
                                  decoration: InputDecoration(
                                    label: const MultiLanguageText(name: "attribute", defaultValue: "Attribute"),
                                    hintText: multiLanguageString(name: "attribute_input", defaultValue: "Attribute input", context: context),
                                    border: const OutlineInputBorder(),
                                  ),
                                )))
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                          onPressed: () {
                            Navigator.pop(dialogContext, true);
                            setState(() {});
                          },
                          child: const MultiLanguageText(name: "close", defaultValue: "Close"),
                        )
                      ],
                    )
                  ],
                ))
              ],
            ),
          ));
        },
      );
    }
    if (type == "testElementAttribute") {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext dialogContext) {
          return Dialog(
              child: Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                    child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                            child: Padding(
                                padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                                child: TextField(
                                  controller: TextEditingController(text: data["elementName"] ?? ""),
                                  onChanged: (value) {
                                    data["elementName"] = value;
                                  },
                                  decoration: InputDecoration(
                                    label: const MultiLanguageText(name: "element", defaultValue: "Element"),
                                    hintText: multiLanguageString(name: "element_input", defaultValue: "Element input", context: context),
                                    border: const OutlineInputBorder(),
                                  ),
                                )))
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                            child: Padding(
                                padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                                child: TextField(
                                  controller: TextEditingController(text: data["attributeName"] ?? ""),
                                  onChanged: (value) {
                                    data["attributeName"] = value;
                                  },
                                  decoration: InputDecoration(
                                    label: const MultiLanguageText(name: "attribute", defaultValue: "Attribute"),
                                    hintText: multiLanguageString(name: "attribute_input", defaultValue: "Attribute input", context: context),
                                    border: const OutlineInputBorder(),
                                  ),
                                )))
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                          onPressed: () {
                            Navigator.pop(dialogContext, true);
                            setState(() {});
                          },
                          child: const MultiLanguageText(name: "close", defaultValue: "Close"),
                        )
                      ],
                    )
                  ],
                ))
              ],
            ),
          ));
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          color: const Color.fromARGB(255, 199, 199, 199),
        ),
        borderRadius: BorderRadius.circular(5),
      ),
      padding: const EdgeInsets.only(top: 10, bottom: 10),
      child: Row(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 5, bottom: 5),
                child: RichText(
                  text: TextSpan(
                    style: const TextStyle(color: Colors.black),
                    children: [
                      TextSpan(text: " " * 4),
                      const TextSpan(text: "Element path list"),
                      TextSpan(text: " " * 2),
                      TextSpan(
                        text: multiLanguageString(name: "add", defaultValue: "Add", context: context),
                        style: const TextStyle(color: Colors.green),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            if (widget.content["elementPathList"] == null) {
                              widget.content["elementPathList"] = [];
                            }
                            widget.content["elementPathList"].add({});
                            setState(() {});
                          },
                      ),
                    ],
                  ),
                ),
              ),
              for (var elementPath in widget.content["elementPathList"] ?? [])
                Padding(
                  padding: const EdgeInsets.only(top: 5, bottom: 5),
                  child: RichText(
                    text: TextSpan(
                      style: const TextStyle(color: Colors.black),
                      children: [
                        TextSpan(text: " " * 8),
                        TextSpan(text: "- ${elementPath["name"]} = "),
                        TextSpan(
                            text: elementPath["from"] != null && elementPath["from"].isNotEmpty
                                ? "${elementPath["from"]} => ${elementPath["xpath"] ?? ""} "
                                : "${elementPath["xpath"] ?? ""}"),
                        TextSpan(text: " " * 2),
                        TextSpan(
                            text: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                            style: const TextStyle(color: Colors.green),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                showEditDialog(widget.content["elementPathList"], elementPath, "elementPath");
                              }),
                        TextSpan(text: " " * 2),
                        TextSpan(
                            text: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                            style: const TextStyle(color: Colors.green),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                widget.content["elementPathList"].remove(elementPath);
                                setState(() {});
                              })
                      ],
                    ),
                  ),
                ),
              Padding(
                padding: const EdgeInsets.only(top: 5, bottom: 5),
                child: RichText(
                  text: TextSpan(
                    style: const TextStyle(color: Colors.black),
                    children: [
                      TextSpan(text: " " * 4),
                      const TextSpan(text: "Result xpath list"),
                      TextSpan(text: " " * 2),
                      TextSpan(
                          text: multiLanguageString(name: "add", defaultValue: "Add", context: context),
                          style: const TextStyle(color: Colors.green),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              if (widget.content["resultXpathList"] == null) {
                                widget.content["resultXpathList"] = [];
                              }
                              widget.content["resultXpathList"].add({});
                              setState(() {});
                            })
                    ],
                  ),
                ),
              ),
              for (var resultXpath in widget.content["resultXpathList"] ?? [])
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: RichText(
                        text: TextSpan(
                          style: const TextStyle(color: Colors.black),
                          children: [
                            TextSpan(text: " " * 8),
                            TextSpan(text: "- ${resultXpath["name"]}"),
                            TextSpan(text: " " * 2),
                            TextSpan(
                                text: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                style: const TextStyle(color: Colors.green),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    showEditDialog(widget.content["resultXpathList"], resultXpath, "resultXpath");
                                  }),
                            TextSpan(text: " " * 2),
                            TextSpan(
                                text: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                                style: const TextStyle(color: Colors.green),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    widget.content["resultXpathList"].remove(resultXpath);
                                    setState(() {});
                                  })
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: RichText(
                        text: TextSpan(
                          style: const TextStyle(color: Colors.black),
                          children: [
                            TextSpan(text: " " * 12),
                            const TextSpan(text: "Xpath list"),
                            TextSpan(text: " " * 2),
                            TextSpan(
                                text: multiLanguageString(name: "add", defaultValue: "Add", context: context),
                                style: const TextStyle(color: Colors.green),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    if (resultXpath["xpathList"] == null) {
                                      resultXpath["xpathList"] = [];
                                    }
                                    resultXpath["xpathList"].add({});
                                    setState(() {});
                                  })
                          ],
                        ),
                      ),
                    ),
                    for (var xpath in resultXpath["xpathList"] ?? [])
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            RichText(
                                text: TextSpan(style: const TextStyle(color: Colors.black), children: [
                              TextSpan(text: " " * 16),
                              TextSpan(text: "- ${xpath["path"]}"),
                              TextSpan(text: " " * 2),
                              TextSpan(
                                  text: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                  style: const TextStyle(color: Colors.green),
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () {
                                      showEditDialog(resultXpath["xpathList"], xpath, "xpath");
                                    }),
                              TextSpan(text: " " * 2),
                              TextSpan(
                                  text: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                                  style: const TextStyle(color: Colors.green),
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () {
                                      resultXpath["xpathList"].remove(xpath);
                                      setState(() {});
                                    })
                            ])),
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: RichText(
                                  text: TextSpan(style: const TextStyle(color: Colors.black), children: [
                                TextSpan(text: " " * 20),
                                const TextSpan(text: "Xpath variable list"),
                                TextSpan(text: " " * 2),
                                TextSpan(
                                    text: multiLanguageString(name: "add", defaultValue: "Add", context: context),
                                    style: const TextStyle(color: Colors.green),
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        if (xpath["variableList"] == null) {
                                          xpath["variableList"] = [];
                                        }
                                        xpath["variableList"].add({"type": "value"});
                                        setState(() {});
                                      })
                              ])),
                            ),
                            for (var variable in xpath["variableList"] ?? [])
                              Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: RichText(
                                  text: TextSpan(
                                    style: const TextStyle(color: Colors.black),
                                    children: [
                                      TextSpan(text: " " * 24),
                                      TextSpan(text: "- ${variable["name"]} = ${variable["elementName"]}.${variable["attributeName"]}"),
                                      TextSpan(text: " " * 2),
                                      TextSpan(
                                          text: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                          style: const TextStyle(color: Colors.green),
                                          recognizer: TapGestureRecognizer()
                                            ..onTap = () {
                                              showEditDialog(xpath["variableList"], variable, "variable");
                                            }),
                                      TextSpan(text: " " * 2),
                                      TextSpan(
                                        text: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                                        style: const TextStyle(color: Colors.green),
                                        recognizer: TapGestureRecognizer()
                                          ..onTap = () {
                                            xpath["variableList"].remove(variable);
                                            setState(() {});
                                          },
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                          ],
                        ),
                      )
                  ],
                ),
              Padding(
                padding: const EdgeInsets.only(top: 5, bottom: 5),
                child: RichText(
                  text: TextSpan(
                    style: const TextStyle(color: Colors.black),
                    children: [
                      TextSpan(text: " " * 4),
                      const TextSpan(text: "Result attribute list"),
                      TextSpan(text: " " * 2),
                      TextSpan(
                        text: multiLanguageString(name: "add", defaultValue: "Add", context: context),
                        style: const TextStyle(color: Colors.green),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            if (widget.content["resultAttributeList"] == null) {
                              widget.content["resultAttributeList"] = [];
                            }
                            widget.content["resultAttributeList"].add({});
                            setState(() {});
                          },
                      ),
                    ],
                  ),
                ),
              ),
              for (var resultAttribute in widget.content["resultAttributeList"] ?? [])
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: RichText(
                    text: TextSpan(
                      style: const TextStyle(color: Colors.black),
                      children: [
                        TextSpan(text: " " * 8),
                        TextSpan(text: "- ${resultAttribute["name"]} = ${resultAttribute["elementName"]}.${resultAttribute["attributeName"]}"),
                        TextSpan(text: " " * 2),
                        TextSpan(
                            text: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                            style: const TextStyle(color: Colors.green),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                showEditDialog(widget.content["resultAttributeList"], resultAttribute, "resultAttribute");
                              }),
                        TextSpan(text: " " * 2),
                        TextSpan(
                          text: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                          style: const TextStyle(color: Colors.green),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              widget.content["resultAttributeList"].remove(resultAttribute);
                              setState(() {});
                            },
                        ),
                      ],
                    ),
                  ),
                ),
              Padding(
                padding: const EdgeInsets.only(top: 5, bottom: 5),
                child: RichText(
                  text: TextSpan(
                    style: const TextStyle(color: Colors.black),
                    children: [
                      TextSpan(text: " " * 4),
                      const TextSpan(text: "Test Element attribute list"),
                      TextSpan(text: " " * 2),
                      TextSpan(
                        text: multiLanguageString(name: "add", defaultValue: "Add", context: context),
                        style: const TextStyle(color: Colors.green),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            if (widget.content["testElementAttributeList"] == null) {
                              widget.content["testElementAttributeList"] = [];
                            }
                            widget.content["testElementAttributeList"].add({});
                            setState(() {});
                          },
                      ),
                    ],
                  ),
                ),
              ),
              for (var testElementAttribute in widget.content["testElementAttributeList"] ?? [])
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: RichText(
                    text: TextSpan(
                      style: const TextStyle(color: Colors.black),
                      children: [
                        TextSpan(text: " " * 8),
                        TextSpan(text: "- ${testElementAttribute["elementName"]}.${testElementAttribute["attributeName"]}"),
                        TextSpan(text: " " * 2),
                        TextSpan(
                            text: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                            style: const TextStyle(color: Colors.green),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                showEditDialog(widget.content["testElementAttributeList"], testElementAttribute, "testElementAttribute");
                              }),
                        TextSpan(text: " " * 2),
                        TextSpan(
                          text: multiLanguageString(name: "delete", defaultValue: "Delete", context: context),
                          style: const TextStyle(color: Colors.green),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              widget.content["testElementAttributeList"].remove(testElementAttribute);
                              setState(() {});
                            },
                        ),
                      ],
                    ),
                  ),
                ),
            ],
          ),
        ],
      ),
    );
  }
}
