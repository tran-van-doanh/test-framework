import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/custom_state.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import '../../../components/badge/badge_icon.dart';
import '../../../components/menu_tab.dart';
import '../../../components/style.dart';

class SysNodeDetailWidget extends StatefulWidget {
  final String id;
  const SysNodeDetailWidget({
    Key? key,
    required this.id,
  }) : super(key: key);

  @override
  State<SysNodeDetailWidget> createState() => _SysNodeDetailWidgetState();
}

class _SysNodeDetailWidgetState extends CustomState<SysNodeDetailWidget> {
  late Future future;
  var resultNodeInstance = [];
  var resultSysNode = {};
  var rowCount = 0;
  var currentPage = 1;
  var rowPerPage = 10;
  @override
  void initState() {
    super.initState();
    future = getInitPage();
  }

  getInitPage() async {
    await getSysNode(widget.id);
    await getListNodeInstance(widget.id, currentPage);
    return 0;
  }

  getSysNode(String id) async {
    var response = await httpGet("/test-framework-api/user/sys/sys-node/$id", context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        resultSysNode = response["body"]["result"];
      });
    }
    return 0;
  }

  getListNodeInstance(String parentId, page) async {
    if ((page - 1) * rowPerPage > rowCount) {
      page = (1.0 * rowCount / rowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"sysNodeId": parentId, "queryOffset": (page - 1) * rowPerPage, "queryLimit": rowPerPage};
    var response = await httpPost("/test-framework-api/user/sys/sys-node-instance/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        currentPage = page;
        rowCount = response["body"]["rowCount"];
        resultNodeInstance = response["body"]["resultList"];
      });
    }
    return 0;
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<NavigationModel>(
      builder: (context, navigationModel, child) {
        return FutureBuilder(
          future: future,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Scaffold(
                  body: Column(
                children: [
                  Expanded(
                    child: ListView(
                      children: [
                        OrganizationNavMenuTab(
                          margin: const EdgeInsets.fromLTRB(45, 20, 30, 0),
                          navigationList: context.organizationSettingsMenuList,
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(50, 20, 50, 30),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              header(navigationModel),
                              testRunInformation(),
                              (resultNodeInstance.isNotEmpty) ? nodeInstanceTable(navigationModel) : const SizedBox.shrink(),
                              DynamicTablePagging(
                                rowCount,
                                currentPage,
                                rowPerPage,
                                pageChangeHandler: (page) {
                                  setState(() {
                                    future = getListNodeInstance(widget.id, page);
                                  });
                                },
                                rowPerPageChangeHandler: (rowPerPage) {
                                  setState(() {
                                    this.rowPerPage = rowPerPage!;
                                    future = getListNodeInstance(widget.id, 1);
                                  });
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ));
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  Widget nodeInstanceTable(NavigationModel navigationModel) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MultiLanguageText(
          name: "agent_instance",
          defaultValue: "Agent instance",
          style: Style(context).styleTitleDialog,
        ),
        const SizedBox(
          height: 5,
        ),
        CustomDataTableWidget(
          columns: [
            Expanded(
              child: MultiLanguageText(
                name: "type",
                defaultValue: "Type",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "operating_system",
                defaultValue: "Operating system",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "platform",
                defaultValue: "Platform",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "browser",
                defaultValue: "Browser",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "resolution",
                defaultValue: "Resolution",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "create_date",
                defaultValue: "Create date",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: MultiLanguageText(
                name: "last_run",
                defaultValue: "Last run",
                isLowerCase: false,
                style: Style(context).styleTextDataColumn,
              ),
            ),
            Expanded(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  MultiLanguageText(
                    name: "status",
                    defaultValue: "Status",
                    isLowerCase: false,
                    style: Style(context).styleTextDataColumn,
                  ),
                  Tooltip(
                    message: multiLanguageString(name: "active_inactive", defaultValue: "Green = Active\nRed = Inactive", context: context),
                    child: const Icon(
                      Icons.info,
                      color: Colors.grey,
                      size: 16,
                    ),
                  ),
                ],
              ),
            ),
          ],
          rows: [
            for (var nodeInstance in resultNodeInstance)
              CustomDataRow(
                cells: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        nodeInstance["nodeType"],
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        nodeInstance["tfOperatingSystem"]["title"],
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        nodeInstance["tfOperatingSystem"]["platform"],
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        nodeInstance["tfBrowser"] != null && nodeInstance["tfBrowserVersion"] != null
                            ? ("${nodeInstance["tfBrowser"]["title"]} ( ${nodeInstance["tfBrowserVersion"]["title"]} )")
                            : "",
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        "${nodeInstance["screenWidth"]} X ${nodeInstance["screenHeight"]}",
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        nodeInstance["createDate"] != null
                            ? "${DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(nodeInstance["createDate"]).toLocal())} ${DateFormat('HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(nodeInstance["createDate"]).toLocal())}"
                            : "",
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Text(
                        nodeInstance["lastRunDate"] != null
                            ? "${DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(nodeInstance["lastRunDate"]).toLocal())} ${DateFormat('HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(nodeInstance["lastRunDate"]).toLocal())}"
                            : "",
                        style: Style(context).styleTextDataCell,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Center(
                      child: Badge2StatusSettingWidget(
                        status: nodeInstance["status"],
                      ),
                    ),
                  ),
                ],
              ),
          ],
        ),
        const SizedBox(
          height: 20,
        )
      ],
    );
  }

  Widget testRunInformation() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "name",
                  defaultValue: "Name",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(130, 130, 130, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
              Expanded(
                flex: 8,
                child: Text(
                  resultSysNode["name"] ?? "",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(49, 49, 49, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "type",
                  defaultValue: "Type",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(130, 130, 130, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
              Expanded(
                flex: 8,
                child: Text(
                  resultSysNode["nodeType"] ?? "",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(49, 49, 49, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "create_date",
                  defaultValue: "Create date",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(130, 130, 130, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
              Expanded(
                flex: 8,
                child: (resultSysNode["createDate"] != null)
                    ? Text(
                        "${DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultSysNode["createDate"]).toLocal())} ${DateFormat('HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultSysNode["createDate"]).toLocal())}",
                        softWrap: true,
                        style: TextStyle(
                          fontSize: context.fontSizeManager.fontSizeText16,
                          color: const Color.fromRGBO(49, 49, 49, 1),
                          fontWeight: FontWeight.w500,
                          letterSpacing: 1.5,
                        ),
                      )
                    : const SizedBox.shrink(),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "last_run",
                  defaultValue: "Last run",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(130, 130, 130, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
              Expanded(
                flex: 8,
                child: (resultSysNode["lastRunDate"] != null)
                    ? Text(
                        "${DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultSysNode["lastRunDate"]).toLocal())} ${DateFormat('HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(resultSysNode["lastRunDate"]).toLocal())}",
                        softWrap: true,
                        style: TextStyle(
                          fontSize: context.fontSizeManager.fontSizeText16,
                          color: const Color.fromRGBO(49, 49, 49, 1),
                          fontWeight: FontWeight.w500,
                          letterSpacing: 1.5,
                        ),
                      )
                    : const SizedBox.shrink(),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                flex: 2,
                child: MultiLanguageText(
                  name: "status",
                  defaultValue: "Status",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(130, 130, 130, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ),
              Expanded(
                flex: 8,
                child: Row(
                  children: [
                    IntrinsicWidth(
                      child: Badge2StatusSettingWidget(
                        status: resultSysNode["status"],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          if (resultSysNode["description"] != null)
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 10,
                ),
                MultiLanguageText(
                  name: "description",
                  defaultValue: "Description",
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(49, 49, 49, 1),
                    fontWeight: FontWeight.w600,
                    letterSpacing: 1.5,
                  ),
                ),
                Text(
                  resultSysNode["description"],
                  style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText16,
                    color: const Color.fromRGBO(49, 49, 49, 1),
                    fontWeight: FontWeight.w500,
                    letterSpacing: 1.5,
                  ),
                ),
              ],
            ),
        ],
      ),
    );
  }

  Widget header(NavigationModel navigationModel) {
    return Row(
      children: [
        IconButton(
          onPressed: () {
            Navigator.pop(context);
            // navigationModel.pop("/node");
          },
          icon: const Icon(Icons.arrow_back),
          padding: EdgeInsets.zero,
          splashRadius: 16,
        ),
        Expanded(
          child: Text(
            resultSysNode["name"],
            style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
          ),
        ),
      ],
    );
  }
}
