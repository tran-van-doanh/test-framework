import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/common/dynamic_text_field.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/setting_project/sys_node/sys_node_detail.dart';

import '../../../common/custom_state.dart';
import '../../../common/dynamic_dropdown_button.dart';
import '../../../type.dart';

class SysNodeWidget extends StatefulWidget {
  const SysNodeWidget({Key? key}) : super(key: key);

  @override
  State<SysNodeWidget> createState() => _SysNodeWidgetState();
}

class _SysNodeWidgetState extends CustomState<SysNodeWidget> {
  late Future futuresysNodeList;
  var sysNodeSearchRequest = {};
  var sysNodeRowCount = 0;
  var sysNodeCurrentPage = 1;
  var sysNodeResultList = [];
  var sysNodeRowPerPage = 10;
  var sysNodeStatusMap = {1: "Active", 0: "Inactive"};
  TextEditingController nameSearchController = TextEditingController();
  SingleValueDropDownController statusController = SingleValueDropDownController();
  @override
  void initState() {
    futuresysNodeList = sysNodePageChange(sysNodeCurrentPage);
    super.initState();
  }

  sysNodeSearch() {
    sysNodeSearchRequest = {"status": statusController.dropDownValue?.value ?? "", "nameLike": nameSearchController.text};
    futuresysNodeList = sysNodePageChange(sysNodeCurrentPage);
  }

  sysNodePageChange(page) async {
    if ((page - 1) * sysNodeRowPerPage > sysNodeRowCount) {
      page = (1.0 * sysNodeRowCount / sysNodeRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * sysNodeRowPerPage, "queryLimit": sysNodeRowPerPage};
    if (sysNodeSearchRequest["nameLike"] != null && sysNodeSearchRequest["nameLike"].isNotEmpty) {
      findRequest["nameLike"] = "%${sysNodeSearchRequest["nameLike"]}%";
    }
    if (sysNodeSearchRequest["status"] != null && sysNodeSearchRequest["status"].isNotEmpty) {
      findRequest["status"] = sysNodeSearchRequest["status"];
    }
    var response = await httpPost("/test-framework-api/user/sys/sys-node/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        sysNodeCurrentPage = page;
        sysNodeRowCount = response["body"]["rowCount"];
        sysNodeResultList = response["body"]["resultList"];
      });
    }
    return 0;
  }

  @override
  void dispose() {
    statusController.dispose();
    nameSearchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<NavigationModel>(builder: (context, navigationModel, child) {
      return FutureBuilder(
        future: futuresysNodeList,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Scaffold(
              body: Column(
                children: [
                  Expanded(
                    child: ListView(
                      children: [
                        OrganizationNavMenuTab(
                          margin: const EdgeInsets.fromLTRB(45, 20, 30, 0),
                          navigationList: context.organizationSettingsMenuList,
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
                          child: Column(
                            children: [
                              searchForm(),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  MultiLanguageText(
                                    name: "sys_agent_count",
                                    defaultValue: "Sys Agent ( \$0 )",
                                    variables: ["$sysNodeRowCount"],
                                    style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
                                  ),
                                ],
                              ),
                              tableWidget(context),
                              DynamicTablePagging(
                                sysNodeRowCount,
                                sysNodeCurrentPage,
                                sysNodeRowPerPage,
                                pageChangeHandler: (page) {
                                  setState(() {
                                    futuresysNodeList = sysNodePageChange(page);
                                  });
                                },
                                rowPerPageChangeHandler: (rowPerPage) {
                                  setState(() {
                                    sysNodeRowPerPage = rowPerPage!;
                                    futuresysNodeList = sysNodePageChange(1);
                                  });
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        },
      );
    });
  }

  Container tableWidget(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 30),
      child: CustomDataTableWidget(
        columns: [
          Expanded(
            child: MultiLanguageText(
              name: "type",
              defaultValue: "Type",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            flex: 2,
            child: MultiLanguageText(
              name: "name",
              defaultValue: "Name",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            child: MultiLanguageText(
              name: "owner",
              defaultValue: "Owner",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            child: MultiLanguageText(
              name: "last_run",
              defaultValue: "Last run",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            child: MultiLanguageText(
              name: "status",
              defaultValue: "Status",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
        ],
        rows: [
          for (var sysNode in sysNodeResultList)
            CustomDataRow(
              cells: [
                Expanded(
                  child: Text(
                    sysNode["nodeType"] ?? "",
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.all(5),
                    child: Row(
                      children: [
                        const Icon(Icons.description, color: Color.fromRGBO(61, 148, 254, 1)),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Text(
                            sysNode["name"] ?? "",
                            overflow: TextOverflow.ellipsis,
                            style: Style(context).styleTextDataCell,
                            maxLines: 3,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Text(
                    sysNode["sysUser"]["fullname"] ?? "",
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                Expanded(
                  child: Text(
                    sysNode["lastRunDate"] != null
                        ? "${DateFormat('dd-MM-yyyy').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(sysNode["lastRunDate"]).toLocal())} ${DateFormat('HH:mm:ss').format(DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(sysNode["lastRunDate"]).toLocal())}"
                        : "",
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                Expanded(
                  child: Text(
                    sysNodeStatusMap[sysNode["status"]] ?? "Not defined",
                    style: Style(context).styleTextDataCell,
                  ),
                ),
              ],
              onSelectChanged: () {
                Navigator.of(context).push(
                  createRoute(
                    SysNodeDetailWidget(id: sysNode["id"]),
                  ),
                );
                // navigationModel.navigate(
                //     "node/${sysNode["id"]}");
              },
            ),
        ],
      ),
    );
  }

  Widget searchForm() {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                child: SizedBox(
                  height: 50,
                  child: DynamicTextField(
                    controller: nameSearchController,
                    onComplete: () => sysNodeSearch(),
                    onChanged: (value) {
                      setState(() {
                        nameSearchController.text;
                      });
                    },
                    labelText: multiLanguageString(name: "name", defaultValue: "Name", context: context),
                    hintText: multiLanguageString(name: "by_name", defaultValue: "By name", context: context),
                    suffixIcon: (nameSearchController.text != "")
                        ? IconButton(
                            onPressed: () {
                              setState(() {
                                nameSearchController.clear();
                              });
                            },
                            icon: const Icon(Icons.clear),
                            splashRadius: 20,
                          )
                        : null,
                  ),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 8, 8, 8),
                child: DynamicDropdownSearchClearOption(
                  labelText: multiLanguageString(name: "search_status", defaultValue: "Search status", context: context),
                  controller: statusController,
                  hint: multiLanguageString(name: "search_status", defaultValue: "Search status", context: context),
                  items: {
                    multiLanguageString(name: "inactive", defaultValue: "Inactive", context: context): "0",
                    multiLanguageString(name: "active", defaultValue: "Active", context: context): "1"
                  }.entries.map<DropDownValueModel>((result) {
                    return DropDownValueModel(
                      value: result.value,
                      name: result.key,
                    );
                  }).toList(),
                  maxHeight: 200,
                  onChanged: (dynamic newValue) {
                    // if (newValue == "") {
                    //   sysNodeSearch();
                    // }
                  },
                ),
              ),
            ),
          ],
        ),
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 20),
              child: ElevatedButton(
                  onPressed: () {
                    sysNodeSearch();
                  },
                  child: const MultiLanguageText(name: "search", defaultValue: "Search", isLowerCase: false)),
            ),
            ElevatedButton(
                onPressed: () {
                  setState(() {
                    nameSearchController.clear();
                    statusController.clearDropDown();
                    sysNodeSearch();
                  });
                },
                child: const MultiLanguageText(name: "reset", defaultValue: "Reset", isLowerCase: false))
          ],
        ),
      ],
    );
  }
}
