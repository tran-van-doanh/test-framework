import 'dart:convert';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/common/file_download.dart';
import 'package:test_framework_view/components/dialog/dialog_delete.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/setting_project/project_type/option_project_type.dart';
import 'package:test_framework_view/type.dart';

import '../../../common/custom_state.dart';
import '../../../config.dart';
import '../find_element_template/setting_find_element_template.dart';

class SettingProjectTypeWidget extends StatefulWidget {
  const SettingProjectTypeWidget({Key? key}) : super(key: key);

  @override
  State<SettingProjectTypeWidget> createState() => _SettingProjectTypeWidgetState();
}

class _SettingProjectTypeWidgetState extends CustomState<SettingProjectTypeWidget> {
  late Future futureprjProjectTypeList;
  var prjProjectTypeSearchRequest = {};
  var prjProjectTypeRowCount = 0;
  var prjProjectTypeCurrentPage = 1;
  var prjProjectTypeResultList = [];
  var prjProjectTypeRowPerPage = 10;
  var prjProjectTypeStatusMap = {1: "Active", 0: "Inactive"};
  PlatformFile? fileContent;

  @override
  void initState() {
    prjProjectTypeSearch({});
    super.initState();
  }

  uploadFile(PlatformFile? file, String url) async {
    final request = http.MultipartRequest(
      "POST",
      Uri.parse(
        "$baseUrl$url",
      ),
    );
    request.headers.addAll({"Authorization": Provider.of<SecurityModel>(context, listen: false).authorization!});
    request.files.add(http.MultipartFile.fromBytes('file', await readFile(file!), filename: file.name));
    var resp = await request.send();
    var responseDecode = jsonDecode(await resp.stream.bytesToString());
    if (responseDecode.containsKey("errorMessage") && mounted) {
      showToast(context: context, msg: responseDecode["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
    } else if (mounted) {
      showToast(
          context: context,
          msg: multiLanguageString(name: "upload_success", defaultValue: "Upload success", context: context),
          color: Colors.greenAccent,
          icon: const Icon(Icons.done));
    }
  }

  downloadFile(String id) async {
    File? downloadedFile = await httpDownloadFile(
        context,
        "/test-framework-api/user/test-framework-file/prj-project-type/$id/tf-find-element-template/export",
        Provider.of<SecurityModel>(context, listen: false).authorization,
        'Output.zip');
    if (downloadedFile != null && mounted) {
      var snackBar = SnackBar(
        content: MultiLanguageText(name: "file_downloaded_to", defaultValue: "File downloaded to \$0 .", variables: ["$downloadedFile"]),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  prjProjectTypeSearch(prjProjectTypeSearchRequest) {
    this.prjProjectTypeSearchRequest = prjProjectTypeSearchRequest;
    futureprjProjectTypeList = prjProjectTypePageChange(prjProjectTypeCurrentPage);
  }

  prjProjectTypePageChange(page) async {
    if ((page - 1) * prjProjectTypeRowPerPage > prjProjectTypeRowCount) {
      page = (1.0 * prjProjectTypeRowCount / prjProjectTypeRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * prjProjectTypeRowPerPage, "queryLimit": prjProjectTypeRowPerPage};
    if (prjProjectTypeSearchRequest["nameLike"] != null && prjProjectTypeSearchRequest["nameLike"].isNotEmpty) {
      findRequest["nameLike"] = "%${prjProjectTypeSearchRequest["nameLike"]}%";
    }
    var response = await httpPost("/test-framework-api/user/project/prj-project-type/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        prjProjectTypeCurrentPage = page;
        prjProjectTypeRowCount = response["body"]["rowCount"];
        prjProjectTypeResultList = response["body"]["resultList"];
      });
    }
    return 0;
  }

  handleDeleteProjectType(prjProjectType) async {
    var response = await httpDelete("/test-framework-api/user/project/prj-project-type/${prjProjectType["id"]}", context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        await prjProjectTypePageChange(prjProjectTypeCurrentPage);
      }
    }
  }

  handleDeleteAllProjectType(id) async {
    var response = await httpDelete("/test-framework-api/user/test-framework/prj-project-type/$id/tf-find-element-template", context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
      }
    }
  }

  handleAddProjectType(result) async {
    var response = await httpPut("/test-framework-api/user/project/prj-project-type", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await prjProjectTypePageChange(prjProjectTypeCurrentPage);
      }
    }
  }

  handleEditProjectType(result, prjProjectType) async {
    var response = await httpPost("/test-framework-api/user/project/prj-project-type/${prjProjectType["id"]}", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await prjProjectTypePageChange(prjProjectTypeCurrentPage);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<NavigationModel>(
      builder: (context, navigationModel, child) {
        return FutureBuilder(
          future: futureprjProjectTypeList,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Scaffold(
                body: Column(
                  children: [
                    Expanded(
                      child: ListView(
                        children: [
                          OrganizationNavMenuTab(
                            margin: const EdgeInsets.fromLTRB(45, 20, 30, 0),
                            navigationList: context.organizationSettingsMenuList,
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    MultiLanguageText(
                                      name: "project_type_count",
                                      defaultValue: "Project Type ( \$0)",
                                      variables: ["$prjProjectTypeRowCount"],
                                      style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
                                    ),
                                  ],
                                ),
                                tableWidget(),
                                DynamicTablePagging(
                                  prjProjectTypeRowCount,
                                  prjProjectTypeCurrentPage,
                                  prjProjectTypeRowPerPage,
                                  pageChangeHandler: (page) {
                                    setState(() {
                                      futureprjProjectTypeList = prjProjectTypePageChange(page);
                                    });
                                  },
                                  rowPerPageChangeHandler: (rowPerPage) {
                                    setState(() {
                                      prjProjectTypeRowPerPage = rowPerPage!;
                                      futureprjProjectTypeList = prjProjectTypePageChange(1);
                                    });
                                  },
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    btnWidget(context),
                  ],
                ),
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            return const Center(child: CircularProgressIndicator());
          },
        );
      },
    );
  }

  Padding btnWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 40, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            margin: const EdgeInsets.only(right: 10),
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              icon: const IconTheme(
                data: IconThemeData(color: Color.fromRGBO(49, 49, 49, 1)),
                child: Icon(Icons.add),
              ),
              label: MultiLanguageText(
                name: "new_project_type",
                defaultValue: "New Project Type",
                isLowerCase: false,
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
              onPressed: () {
                Navigator.of(context).push(
                  createRoute(
                    DialogOptionProjectTypeWidget(
                        type: "create",
                        title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                        callbackOptionTest: (result) {
                          handleAddProjectType(result);
                        }),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Container tableWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 30),
      child: CustomDataTableWidget(
        columns: [
          Expanded(
            flex: 2,
            child: MultiLanguageText(
              name: "code",
              defaultValue: "Code",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            flex: 2,
            child: MultiLanguageText(
              name: "name",
              defaultValue: "Name",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            flex: 2,
            child: MultiLanguageText(
              name: "description",
              defaultValue: "Description",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            child: MultiLanguageText(
              name: "status",
              defaultValue: "Status",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          const SizedBox(
            width: 38,
          ),
        ],
        rows: [
          for (var prjProjectType in prjProjectTypeResultList)
            CustomDataRow(
              cells: [
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.all(5),
                    child: Row(
                      children: [
                        const Icon(Icons.description, color: Color.fromRGBO(61, 148, 254, 1)),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Text(
                            prjProjectType["name"] ?? "",
                            maxLines: 3,
                            overflow: TextOverflow.ellipsis,
                            style: Style(context).styleTextDataCell,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.all(5),
                    child: Text(
                      prjProjectType["title"] ?? "",
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: Style(context).styleTextDataCell,
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text(
                    prjProjectType["description"] ?? "",
                    style: Style(context).styleTextDataCell,
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                Expanded(
                  child: Text(
                    prjProjectTypeStatusMap[prjProjectType["status"]] ?? "Not defined",
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                PopupMenuButton(
                  offset: const Offset(5, 50),
                  tooltip: '',
                  splashRadius: 10,
                  icon: const Icon(
                    Icons.more_vert,
                  ),
                  itemBuilder: (context) => [
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.of(context).push(
                            createRoute(
                              DialogOptionProjectTypeWidget(
                                  type: "edit",
                                  title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                  id: prjProjectType["id"],
                                  callbackOptionTest: (result) {
                                    handleEditProjectType(result, prjProjectType);
                                  }),
                            ),
                          );
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                        leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.of(context).push(
                            createRoute(
                              SettingFindElementTemplateWidget(prjProjectTypeId: prjProjectType["id"]),
                            ),
                          );
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "element_template", defaultValue: "Element template"),
                        leading: const Icon(Icons.settings, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          dialogDeleteProjectType(prjProjectType, "delete");
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "delete", defaultValue: "Delete"),
                        leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          dialogDeleteProjectType(prjProjectType, "delete_all");
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "delete_all_value", defaultValue: "Delete all value"),
                        leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () async {
                          fileContent = await selectFile(['json']);
                          if (fileContent != null) {
                            await uploadFile(fileContent,
                                "/test-framework-api/user/test-framework-file/prj-project-type/${prjProjectType["id"]}/tf-find-element-template/import");
                          }
                          if (mounted) {
                            Navigator.pop(this.context);
                          }
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "import", defaultValue: "Import"),
                        leading: const Icon(Icons.upload_file, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () async {
                          await downloadFile(prjProjectType["id"]);
                          if (mounted) {
                            Navigator.pop(this.context);
                          }
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "export", defaultValue: "Export"),
                        leading: const Icon(Icons.cloud_download, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                  ],
                ),
              ],
            ),
        ],
      ),
    );
  }

  dialogDeleteProjectType(prjProjectType, type) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogDelete(
          selectedItem: prjProjectType,
          callback: () {
            if (type == "delete") {
              handleDeleteProjectType(prjProjectType);
            } else if (type == "delete_all") {
              handleDeleteAllProjectType(prjProjectType["id"]);
            }
          },
          type: 'project_type',
        );
      },
    );
  }
}
