import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import 'package:test_framework_view/api.dart';
import 'package:test_framework_view/common/dynamic_table.dart';
import 'package:test_framework_view/components/dialog/dialog_delete.dart';
import 'package:test_framework_view/components/menu_tab.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/components/toast.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/setting_project/value_type/option_value_type.dart';
import 'package:test_framework_view/page/setting_project/value_type/value_template/setting_value_template.dart';
import 'package:test_framework_view/type.dart';

import '../../../common/custom_state.dart';

class SettingValueTypeWidget extends StatefulWidget {
  const SettingValueTypeWidget({Key? key}) : super(key: key);

  @override
  State<SettingValueTypeWidget> createState() => _SettingValueTypeWidgetState();
}

class _SettingValueTypeWidgetState extends CustomState<SettingValueTypeWidget> {
  late Future futuretfValueTypeList;
  var tfValueTypeSearchRequest = {};
  var tfValueTypeRowCount = 0;
  var tfValueTypeCurrentPage = 1;
  var tfValueTypeResultList = [];
  var tfValueTypeRowPerPage = 10;
  var tfValueTypeStatusMap = {1: "Active", 0: "Inactive"};

  @override
  void initState() {
    tfValueTypeSearch({});
    super.initState();
  }

  tfValueTypeSearch(tfValueTypeSearchRequest) {
    this.tfValueTypeSearchRequest = tfValueTypeSearchRequest;
    futuretfValueTypeList = tfValueTypePageChange(tfValueTypeCurrentPage);
  }

  tfValueTypePageChange(page) async {
    if ((page - 1) * tfValueTypeRowPerPage > tfValueTypeRowCount) {
      page = (1.0 * tfValueTypeRowCount / tfValueTypeRowPerPage).ceil();
    }
    if (page < 1) {
      page = 1;
    }
    var findRequest = {"queryOffset": (page - 1) * tfValueTypeRowPerPage, "queryLimit": tfValueTypeRowPerPage};
    if (tfValueTypeSearchRequest["nameLike"] != null && tfValueTypeSearchRequest["nameLike"].isNotEmpty) {
      findRequest["nameLike"] = "%${tfValueTypeSearchRequest["nameLike"]}%";
    }
    var response = await httpPost("/test-framework-api/user/test-framework/tf-value-type/search", findRequest, context);
    if (response.containsKey("body") && response["body"] is String == false) {
      setState(() {
        tfValueTypeCurrentPage = page;
        tfValueTypeRowCount = response["body"]["rowCount"];
        tfValueTypeResultList = response["body"]["resultList"];
      });
    }
    return 0;
  }

  handleDeleteValueType(tfValueType) async {
    var response = await httpDelete("/test-framework-api/user/test-framework/tf-value-type/${tfValueType["id"]}", context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        await tfValueTypePageChange(tfValueTypeCurrentPage);
      }
    }
  }

  handleDeleteAllValueType(id) async {
    var response = await httpDelete("/test-framework-api/user/test-framework/tf-value-type/$id/tf-value-template", context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "delete_successful", defaultValue: "Delete successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
      }
    }
  }

  handleAddValueType(result) async {
    var response = await httpPut("/test-framework-api/user/test-framework/tf-value-type", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "create_successful", defaultValue: "Create successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await tfValueTypePageChange(tfValueTypeCurrentPage);
      }
    }
  }

  handleEditValueType(result, tfValueType) async {
    var response = await httpPost("/test-framework-api/user/test-framework/tf-value-type/${tfValueType["id"]}", result, context);
    if (response.containsKey("body") && response["body"] is String == false && mounted) {
      if (response["body"].containsKey("errorMessage")) {
        showToast(context: context, msg: response["body"]["errorMessage"], color: Colors.red, icon: const Icon(Icons.error));
      } else {
        showToast(
            context: context,
            msg: multiLanguageString(name: "update_successful", defaultValue: "Update successful", context: context),
            color: Colors.greenAccent,
            icon: const Icon(Icons.done));
        Navigator.of(context).pop();
        await tfValueTypePageChange(tfValueTypeCurrentPage);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<NavigationModel>(builder: (context, navigationModel, child) {
      return FutureBuilder(
        future: futuretfValueTypeList,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Scaffold(
              body: Column(
                children: [
                  Expanded(
                    child: ListView(
                      children: [
                        OrganizationNavMenuTab(
                          margin: const EdgeInsets.fromLTRB(45, 20, 30, 0),
                          navigationList: context.organizationSettingsMenuList,
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(50, 20, 50, 20),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  MultiLanguageText(
                                    name: "value_type_count",
                                    defaultValue: "Value Type ( \$0 )",
                                    variables: ["$tfValueTypeRowCount"],
                                    style: TextStyle(fontSize: context.fontSizeManager.fontSizeText30, fontWeight: FontWeight.w500),
                                  ),
                                ],
                              ),
                              tableWidget(),
                              DynamicTablePagging(
                                tfValueTypeRowCount,
                                tfValueTypeCurrentPage,
                                tfValueTypeRowPerPage,
                                pageChangeHandler: (page) {
                                  setState(() {
                                    futuretfValueTypeList = tfValueTypePageChange(page);
                                  });
                                },
                                rowPerPageChangeHandler: (rowPerPage) {
                                  setState(() {
                                    tfValueTypeRowPerPage = rowPerPage!;
                                    futuretfValueTypeList = tfValueTypePageChange(1);
                                  });
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  btnWidget(context),
                ],
              ),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        },
      );
    });
  }

  Padding btnWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 40, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            margin: const EdgeInsets.only(right: 10),
            height: 40,
            child: FloatingActionButton.extended(
              heroTag: null,
              icon: const IconTheme(
                data: IconThemeData(color: Color.fromRGBO(49, 49, 49, 1)),
                child: Icon(Icons.add),
              ),
              label: MultiLanguageText(
                name: "new_value_type",
                defaultValue: "New Value Type",
                isLowerCase: false,
                style: TextStyle(
                    fontSize: context.fontSizeManager.fontSizeText13, fontWeight: FontWeight.w600, color: const Color.fromRGBO(49, 49, 49, 1)),
              ),
              backgroundColor: const Color.fromARGB(175, 8, 196, 80),
              elevation: 0,
              hoverElevation: 0,
              onPressed: () {
                Navigator.of(context).push(
                  createRoute(DialogOptionValueTypeWidget(
                      type: "create",
                      title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
                      callbackOptionTest: (result) {
                        handleAddValueType(result);
                      })),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Container tableWidget() {
    return Container(
      margin: const EdgeInsets.only(top: 30),
      child: CustomDataTableWidget(
        columns: [
          Expanded(
            flex: 3,
            child: MultiLanguageText(
              name: "code",
              defaultValue: "Code",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            flex: 3,
            child: MultiLanguageText(
              name: "description",
              defaultValue: "Description",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          Expanded(
            child: MultiLanguageText(
              name: "status",
              defaultValue: "Status",
              isLowerCase: false,
              style: Style(context).styleTextDataColumn,
            ),
          ),
          const SizedBox(
            width: 38,
          ),
        ],
        rows: [
          for (var tfValueType in tfValueTypeResultList)
            CustomDataRow(
              cells: [
                Expanded(
                  flex: 3,
                  child: Padding(
                    padding: const EdgeInsets.all(5),
                    child: Row(
                      children: [
                        const Icon(Icons.description, color: Color.fromRGBO(61, 148, 254, 1)),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Text(
                            tfValueType["name"] ?? "",
                            overflow: TextOverflow.ellipsis,
                            style: Style(context).styleTextDataCell,
                            maxLines: 3,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: Text(
                    tfValueType["description"] ?? "",
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                Expanded(
                  child: Text(
                    tfValueTypeStatusMap[tfValueType["status"]] ?? "Not defined",
                    style: Style(context).styleTextDataCell,
                  ),
                ),
                PopupMenuButton(
                  offset: const Offset(5, 50),
                  tooltip: '',
                  splashRadius: 10,
                  icon: const Icon(
                    Icons.more_vert,
                  ),
                  itemBuilder: (context) => [
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.of(context).push(
                            createRoute(DialogOptionValueTypeWidget(
                                type: "edit",
                                title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
                                id: tfValueType["id"],
                                callbackOptionTest: (result) {
                                  handleEditValueType(result, tfValueType);
                                })),
                          );
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "edit", defaultValue: "Edit"),
                        leading: const Icon(Icons.edit, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.of(context).push(
                            createRoute(
                              SettingTemplateWidget(tfValueTypeId: tfValueType["id"]),
                            ),
                          );
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "value_template", defaultValue: "Value template"),
                        leading: const Icon(Icons.settings, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          dialogDeleteValueType(tfValueType, "delete");
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "delete", defaultValue: "Delete"),
                        leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                    PopupMenuItem(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                      child: ListTile(
                        onTap: () {
                          Navigator.pop(context);
                          dialogDeleteValueType(tfValueType, "delete_all");
                        },
                        contentPadding: const EdgeInsets.all(0),
                        hoverColor: Colors.transparent,
                        title: const MultiLanguageText(name: "delete_all_template", defaultValue: "Delete all template"),
                        leading: const Icon(Icons.delete, color: Color.fromARGB(255, 112, 114, 119)),
                      ),
                    ),
                  ],
                ),
              ],
            ),
        ],
      ),
    );
  }

  dialogCreateNewValueType() {
    showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          return DialogOptionValueTypeWidget(
              type: "create",
              title: multiLanguageString(name: "create", defaultValue: "Create", context: context),
              callbackOptionTest: (result) {
                handleAddValueType(result);
              });
        });
  }

  dialogEditValueType(tfValueType) {
    showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          return DialogOptionValueTypeWidget(
              type: "edit",
              title: multiLanguageString(name: "edit", defaultValue: "Edit", context: context),
              id: tfValueType["id"],
              callbackOptionTest: (result) {
                handleEditValueType(result, tfValueType);
              });
        });
  }

  dialogDeleteValueType(tfValueType, type) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return DialogDelete(
          selectedItem: tfValueType,
          callback: () {
            if (type == "delete") {
              handleDeleteValueType(tfValueType);
            } else if (type == "delete_all") {
              handleDeleteAllValueType(tfValueType["id"]);
            }
          },
          type: 'value_type',
        );
      },
    );
  }
}
