import 'package:flutter/material.dart';

class FontSizeManager {
  static final FontSizeManager _instance = FontSizeManager._internal();

  factory FontSizeManager() {
    return _instance;
  }

  FontSizeManager._internal();

  double fontSizeText = 14.0;
  double get fontSizeText12 => fontSizeText - 2;
  double get fontSizeText13 => fontSizeText - 1;
  double get fontSizeText14 => fontSizeText;
  double get fontSizeText15 => fontSizeText + 1;
  double get fontSizeText16 => fontSizeText + 2;
  double get fontSizeText18 => fontSizeText + 4;
  double get fontSizeText20 => fontSizeText + 6;
  double get fontSizeText22 => fontSizeText + 8;
  double get fontSizeText24 => fontSizeText + 10;
  double get fontSizeText30 => fontSizeText + 16;
  double get fontSizeText36 => fontSizeText + 22;
  double get fontSizeText40 => fontSizeText + 26;
  void setFontSizeText(double value) {
    fontSizeText = value;
  }

  void loadFontSizeTextFromStorage(double valueChange) {
    fontSizeText = 14 + valueChange;
  }
}

extension FontSizeExtension on BuildContext {
  FontSizeManager get fontSizeManager => FontSizeManager();
}
