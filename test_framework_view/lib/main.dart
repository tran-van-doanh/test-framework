import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/common/custom_state.dart';
import 'package:test_framework_view/common/theme.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';
import 'package:test_framework_view/l10n/l10n.dart';
import 'package:test_framework_view/l10n/locale_provider.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/router.dart';
import 'package:localstorage/localstorage.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

void main() {
  GoogleFonts.config.allowRuntimeFetching = false;
  runApp(
    const TestFrameworkApp(),
  );
}

class MyCustomScrollBehavior extends MaterialScrollBehavior {
  // Override behavior methods and getters like dragDevices
  @override
  Set<PointerDeviceKind> get dragDevices => {
        PointerDeviceKind.touch,
        PointerDeviceKind.mouse,
        PointerDeviceKind.trackpad,
        // etc.
      };
}

class TestFrameworkApp extends StatefulWidget {
  const TestFrameworkApp({Key? key}) : super(key: key);
  @override
  State<StatefulWidget> createState() => _TestFrameworkAppState();
}

class _TestFrameworkAppState extends CustomState<TestFrameworkApp> {
  final AppRouterDelegate _routerDelegate = AppRouterDelegate();
  final AppRouteInformationParser _routeInformationParser = AppRouteInformationParser();
  late LocalStorage localStorage;
  late SecurityModel securityModel;
  late NavigationModel navigationModel;
  late ScaleModel scaleModel;
  late LanguageModel languageModel;
  late NotificationModel notificationModel;
  _TestFrameworkAppState() {
    localStorage = LocalStorage('storage');
    securityModel = SecurityModel(localStorage);
    navigationModel = NavigationModel(localStorage);
    scaleModel = ScaleModel(localStorage);
    languageModel = LanguageModel(localStorage);
    notificationModel = NotificationModel(localStorage);
  }

  @override
  void initState() {
    super.initState();
    languageModel.storage.ready.then((value) {
      languageModel.initialize();
    });
    scaleModel.storage.ready.then((value) {
      scaleModel.reload();
    });
    notificationModel.storage.ready.then((value) {
      notificationModel.reload();
    });
    navigationModel.addListener(() async {
      _routerDelegate.updateNavigation(navigationModel);
    });
    securityModel.storage.ready.then((value) {
      securityModel.reload();
      navigationModel.reload();
    });
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => securityModel),
        ChangeNotifierProvider(create: (context) => navigationModel),
        ChangeNotifierProvider(create: (context) => scaleModel),
        ChangeNotifierProvider(create: (context) => languageModel),
        ChangeNotifierProvider(create: (context) => notificationModel),
        ChangeNotifierProvider(create: (context) => LocaleProvider()),
        ChangeNotifierProvider(create: (context) => WebElementModel()),
        ChangeNotifierProvider(create: (context) => ElementRepositoryModel()),
        ChangeNotifierProvider(create: (context) => TestCaseConfigModel()),
        ChangeNotifierProvider(create: (context) => DebugStatusModel()),
        ChangeNotifierProvider(create: (context) => TestCaseDirectoryModel()),
        ChangeNotifierProvider(create: (context) => SideBarDocumentModel()),
        ChangeNotifierProvider(create: (context) => PlayTestCaseModel()),
        ChangeNotifierProvider(create: (context) => ListElementPlayIdModel()),
        ChangeNotifierProvider(create: (context) => CopyWidgetModel()),
        ChangeNotifierProvider(create: (context) => ModeViewEditorModel()),
        ChangeNotifierProvider(create: (context) => EditorNotificationModel()),
      ],
      child: FutureBuilder(
        future: securityModel.storage.ready,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          final provider = Provider.of<LocaleProvider>(context);
          final scaleModel = Provider.of<ScaleModel>(context);
          final navigationModel = Provider.of<NavigationModel>(context);
          context.fontSizeManager.loadFontSizeTextFromStorage(navigationModel.differencefontSizeText);
          return MaterialApp.router(
            title: navigationModel.product == "qa-platform" ? 'QA PLATFORM' : 'WORK MANAGER',
            localizationsDelegates: const [
              AppLocalizations.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
            ],
            locale: provider.locale,
            supportedLocales: L10n.all,
            scrollBehavior: MyCustomScrollBehavior(),
            routerDelegate: _routerDelegate,
            routeInformationParser: _routeInformationParser,
            debugShowCheckedModeBanner: false,
            builder: (context, widget) {
              return MediaQuery(
                data: MediaQuery.of(context).copyWith(textScaler: TextScaler.linear(scaleModel.textScaleFactor)),
                child: widget ?? Container(),
              );
            },
            theme: CommonThemeMethod().themedata,
          );
        },
      ),
    );
  }
}
