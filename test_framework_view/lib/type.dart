// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';
import 'dart:io' show Platform, File;

import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:test_framework_view/page/editor/template_list_widget/action_template.dart';
import 'package:test_framework_view/page/editor/template_list_widget/assert_template.dart';
import 'package:test_framework_view/page/editor/template_list_widget/condition_variable_template.dart';
import 'package:test_framework_view/page/editor/template_list_widget/control_template.dart';

class ChartData {
  String x;
  int y;
  Color color;
  ChartData(this.x, this.y, this.color);

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'x': x,
      'y': y,
      'color': color.value,
    };
  }

  factory ChartData.fromMap(Map<String, dynamic> map) {
    return ChartData(
      map['x'] as String,
      map['y'] as int,
      Color(map['color'] as int),
    );
  }

  String toJson() => json.encode(toMap());

  factory ChartData.fromJson(String source) => ChartData.fromMap(json.decode(source) as Map<String, dynamic>);
}

class Dragable {}

class Template extends Dragable {
  String type;
  String category;
  String description;
  Widget widget;
  String title;
  List platformsList;
  Map<String, dynamic>? componentConfig;
  Template({
    required this.title,
    required this.description,
    required this.type,
    required this.category,
    this.widget = const SizedBox.shrink(),
    required this.platformsList,
    this.componentConfig,
  });
}

class Item extends Dragable {
  String? id;
  String type;
  String category;
  String key = "NONE";
  String mouseActionType = "NONE";
  String? endKey;
  List<Item> thenBodyList = [];
  List<Item> elseBodyList = [];
  List<Item> onErrorBodyList = [];
  List<Item> onFinishBodyList = [];
  List<Item> bodyList = [];
  List<Item> variablePath = [];
  List<Item> conditionList = [];
  List<Item> operatorList = [];
  dynamic conditionGroup;
  String? name;
  String? platform;
  String? clientName;
  List clientList = [];
  String? description;
  String? value;
  String? dataType;
  String? actionName;
  String? actionId;
  String? testCaseName;
  String? apiName;
  String? objectName;
  String? operatorType;
  String? message;
  String? tfTestElementId;
  String? tfTestElementRepositoryId;
  String? tfValueTemplateId;
  bool? isList;
  bool isSelected = true;
  bool isExpand = true;
  var returnType = {};
  Item? o1;
  Item? o2;
  var parameterList = [];
  var parameterFieldList = [];
  bool? isError;
  GlobalKey<State<StatefulWidget>>? globalKey;
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'type': type,
      'key': key,
      'mouseActionType': mouseActionType,
      'category': category,
      'thenBodyList': thenBodyList,
      'elseBodyList': elseBodyList,
      'bodyList': bodyList,
      'onErrorBodyList': onErrorBodyList,
      'onFinishBodyList': onFinishBodyList,
      'conditionList': conditionList,
      'operatorList': operatorList,
      'operatorType': operatorType,
      'actionName': actionName,
      'testCaseName': testCaseName,
      'apiName': apiName,
      'objectName': objectName,
      'name': name,
      'platform': platform,
      'clientName': clientName,
      'clientList': clientList,
      'isList': isList,
      'isSelected': isSelected,
      'isExpand': isExpand,
      'description': description,
      'value': value,
      'dataType': dataType,
      'message': message,
      'tfTestElementId': tfTestElementId,
      'tfTestElementRepositoryId': tfTestElementRepositoryId,
      'tfValueTemplateId': tfValueTemplateId,
      'parameterList': parameterList,
      'parameterFieldList': parameterFieldList,
      'variablePath': variablePath,
      'conditionGroup': conditionGroup,
      'returnType': returnType,
      'o1': o1,
      'o2': o2,
      'isError': isError,
      // 'globalKey': globalKey,
    };
  }

  Item(this.type, this.category) {
    globalKey = GlobalKey();
    if (type == "condition" || type == "while" || type == "assertTrue" || type == "assertFalse") {
      conditionGroup = Item("AND", "conditionGroup");
    } else if (type == "forEach" || type == "newVariable" || type == "assignValue" || type == "sleep" || type == "error") {
      variablePath = [Item("variableGroup_default", "variableGroup_default")];
    } else if (type == "add" || type == "sub" || type == "mul" || type == "div") {
      operatorList = [Item("variable_default", "variable_default"), Item("variable_default", "variable_default")];
    } else if (type == "contains" ||
        type == "matches" ||
        type == "gt" ||
        type == "gte" ||
        type == "lt" ||
        type == "lte" ||
        type == "eq" ||
        type == "ne") {
      o1 = Item("variable_default", "variable_default");
      o2 = Item("variable_default", "variable_default");
    } else if (type == "AND" || type == "OR") {
      conditionList = [Item("condition_default", "condition_default"), Item("condition_default", "condition_default")];
    } else if (type == "NOT") {
      conditionList = [
        Item("condition_default", "condition_default"),
      ];
    }
  }
}

class Task extends Dragable {
  String id;
  String name;
  String title;
  String? description;
  // String priority;
  String? wfStatusId;
  String? assigneeSysUserId;
  User? assigneeSysUser;
  String? scheduledStartDate;
  String? scheduledEndDate;
  String? actualStartDate;
  String? actualEndDate;
  List? tagsList;
  int status;
  String? parentId;
  String? taskType;
  // dynamic testCase;
  Task({
    required this.id,
    required this.name,
    required this.title,
    this.description,
    // required this.priority,
    this.wfStatusId,
    this.assigneeSysUserId,
    this.scheduledStartDate,
    this.scheduledEndDate,
    this.actualStartDate,
    this.actualEndDate,
    this.tagsList,
    required this.status,
    this.parentId,
    this.taskType,
    this.assigneeSysUser,
    // this.testCase,
  });
  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'name': name,
      'title': title,
      'description': description,
      'wfStatusId': wfStatusId,
      'assigneeSysUserId': assigneeSysUserId,
      'assigneeSysUser': assigneeSysUser?.toMap(),
      'scheduledStartDate': scheduledStartDate,
      'scheduledEndDate': scheduledEndDate,
      'actualStartDate': actualStartDate,
      'actualEndDate': actualEndDate,
      if (tagsList != null) 'tagsList': tagsList!.map((task) => task.toJson()).toList(),
      'status': status,
      'parentId': parentId,
      'taskType': taskType,
    };
  }

  factory Task.fromMap(Map<String, dynamic> map) {
    return Task(
      id: map['id'] as String,
      name: map['name'] as String,
      title: map['title'] as String,
      description: map['description'] != null ? map['description'] as String : null,
      wfStatusId: map['wfStatusId'] != null ? map['wfStatusId'] as String : null,
      assigneeSysUserId: map['assigneeSysUserId'] != null ? map['assigneeSysUserId'] as String : null,
      assigneeSysUser: map['assigneeSysUser'] != null ? User.fromMap(map['assigneeSysUser'] as Map<String, dynamic>) : null,
      scheduledStartDate: map['scheduledStartDate'] != null ? map['scheduledStartDate'] as String : null,
      scheduledEndDate: map['scheduledEndDate'] != null ? map['scheduledEndDate'] as String : null,
      actualStartDate: map['actualStartDate'] != null ? map['actualStartDate'] as String : null,
      actualEndDate: map['actualEndDate'] != null ? map['actualEndDate'] as String : null,
      tagsList: map['tagsList'],
      status: map['status'] as int,
      parentId: map['parentId'] != null ? map['parentId'] as String : null,
      taskType: map['taskType'] != null ? map['taskType'] as String : null,
    );
  }

  // String toJson() => json.encode(toMap());

  // factory Task.fromJson(String source) => Task.fromMap(json.decode(source) as Map<String, dynamic>);
}

class TaskStatus extends Dragable {
  String id;
  String name;
  String title;
  String statusColor;
  String? description;
  int? status;
  int? statusIndex;
  List? flowTypesList;
  TaskStatus({
    required this.id,
    required this.name,
    required this.title,
    required this.statusColor,
    this.description,
    required this.status,
    this.statusIndex,
    this.flowTypesList,
  });
  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'name': name,
      'title': title,
      'statusColor': statusColor,
      'description': description,
      'status': status,
      'statusIndex': statusIndex,
      'flowTypesList': flowTypesList,
    };
  }

  factory TaskStatus.fromMap(Map<String, dynamic> map) {
    return TaskStatus(
      id: map['id'] as String,
      name: map['name'] as String,
      title: map['title'] as String,
      statusColor: map['statusColor'] as String,
      description: map['description'] != null ? map['description'] as String : null,
      status: map['status'] != null ? map['status'] as int : null,
      statusIndex: map['statusIndex'] != null ? map['statusIndex'] as int : null,
      flowTypesList: map['flowTypesList'],
    );
  }

  // String toJson() => json.encode(toMap());

  // factory TaskStatus.fromJson(String source) => TaskStatus.fromMap(json.decode(source) as Map<String, dynamic>);
}

class Sprint extends Dragable {
  String id;
  String name;
  String title;
  String? description;
  String startDate;
  String endDate;
  int status;
  Sprint({
    required this.id,
    required this.name,
    required this.title,
    this.description,
    required this.startDate,
    required this.endDate,
    required this.status,
  });
  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'name': name,
      'title': title,
      'description': description,
      'startDate': startDate,
      'endDate': endDate,
      'status': status,
    };
  }

  factory Sprint.fromMap(Map<String, dynamic> map) {
    return Sprint(
      id: map['id'] as String,
      name: map['name'] as String,
      title: map['title'] as String,
      description: map['description'] != null ? map['description'] as String : null,
      startDate: map['startDate'] as String,
      endDate: map['endDate'] as String,
      status: map['status'] as int,
    );
  }

  // String toJson() => json.encode(toMap());

  // factory Sprint.fromJson(String source) => Sprint.fromMap(json.decode(source) as Map<String, dynamic>);
}

class User extends Dragable {
  String id;
  String nickname;
  String fullname;
  String email;
  User({
    required this.id,
    required this.nickname,
    required this.fullname,
    required this.email,
  });
  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'nickname': nickname,
      'fullname': fullname,
      'email': email,
    };
  }

  factory User.fromMap(Map<String, dynamic> map) {
    return User(
      id: map['id'] as String,
      nickname: map['nickname'] as String,
      fullname: map['fullname'] as String,
      email: map['email'] as String,
    );
  }

  // String toJson() => json.encode(toMap());

  // factory User.fromJson(String source) => User.fromMap(json.decode(source) as Map<String, dynamic>);
}

class Tag extends Dragable {
  String id;
  String tagColor;
  String name;
  String title;
  String? description;
  int status;
  Tag({
    required this.id,
    required this.tagColor,
    required this.name,
    required this.title,
    this.description,
    required this.status,
  });
  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'tagColor': tagColor,
      'name': name,
      'title': title,
      'description': description,
      'status': status,
    };
  }

  factory Tag.fromMap(Map<String, dynamic> map) {
    return Tag(
      id: map['id'] as String,
      tagColor: map['tagColor'] as String,
      name: map['name'] as String,
      title: map['title'] as String,
      description: map['description'] != null ? map['description'] as String : null,
      status: map['status'] as int,
    );
  }

  // String toJson() => json.encode(toMap());

  // factory Tag.fromJson(String source) => Tag.fromMap(json.decode(source) as Map<String, dynamic>);
}

class SprintTask extends Dragable {
  String id;
  String prjSprintId;
  Sprint prjSprint;
  String prjTaskId;
  Task prjTask;
  String wfStatusId;
  TaskStatus wfStatus;
  int taskIndex;
  SprintTask({
    required this.id,
    required this.prjSprintId,
    required this.prjSprint,
    required this.prjTaskId,
    required this.prjTask,
    required this.wfStatusId,
    required this.wfStatus,
    required this.taskIndex,
  });
  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'prjSprintId': prjSprintId,
      'prjSprint': prjSprint.toMap(),
      'prjTaskId': prjTaskId,
      'prjTask': prjTask.toMap(),
      'wfStatusId': wfStatusId,
      'wfStatus': wfStatus.toMap(),
      'taskIndex': taskIndex,
    };
  }

  factory SprintTask.fromMap(Map<String, dynamic> map) {
    return SprintTask(
      id: map['id'] as String,
      prjSprintId: map['prjSprintId'] as String,
      prjSprint: Sprint.fromMap(map['prjSprint'] as Map<String, dynamic>),
      prjTaskId: map['prjTaskId'] as String,
      prjTask: Task.fromMap(map['prjTask'] as Map<String, dynamic>),
      wfStatusId: map['wfStatusId'] as String,
      wfStatus: TaskStatus.fromMap(map['wfStatus'] as Map<String, dynamic>),
      taskIndex: map['taskIndex'] as int,
    );
  }

  // String toJson() => json.encode(toMap());

  // factory SprintTask.fromJson(String source) => SprintTask.fromMap(json.decode(source) as Map<String, dynamic>);

  String toJson() => json.encode(toMap());

  factory SprintTask.fromJson(String source) => SprintTask.fromMap(json.decode(source) as Map<String, dynamic>);
}

class WorkFlowTemplate extends Dragable {
  String type;
  Widget widget;
  WorkFlowTemplate({
    required this.type,
    required this.widget,
  });
}

class WorkFlowStep extends Dragable {
  String? id;
  String type;
  String? content;
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'type': type,
      'content': content,
    };
  }

  WorkFlowStep({
    this.id,
    required this.type,
    this.content,
  });
}

class WorkFlowRole extends Dragable {
  String id;
  String name;
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
    };
  }

  WorkFlowRole({
    required this.id,
    required this.name,
  });
}

Widget mapTemplateWidget(String type, String title, List platformsList, {bool hasBodyWidget = true, dynamic componentConfig}) {
  if (type == "startApplication" ||
      type == "installApplication" ||
      type == "apiCall" ||
      type == "actionCall" ||
      type == "testCaseCall" ||
      type == "sendKeys" ||
      type == "childElementSendKeys" ||
      type == "clearAndSendKeys" ||
      type == "childElementClearAndSendKeys" ||
      type == "sendKeyFromElement" ||
      type == "childElementSendKeyFromElement" ||
      type == "sendKeyWithoutElement" ||
      type == "sendKeysWithoutElement" ||
      type == "clearAndSendFile" ||
      type == "childElementClearAndSendFile" ||
      type == "mouseAction" ||
      type == "mouseActionWithOffset" ||
      type == "click" ||
      type == "switchToFrame" ||
      type == "clickByPosition" ||
      type == "childElementClick" ||
      type == "selectTableRow" ||
      type == "selectByValue" ||
      type == "childElementSelectByValue" ||
      type == "selectByVisibleText" ||
      type == "getSelectedValue" ||
      type == "getSelectOptionsValue" ||
      type == "getSelectVisibleOptionsValue" ||
      type == "getSelectedVisibleValue" ||
      type == "childElementSelectByVisibleText" ||
      type == "findElements" ||
      type == "childElementFindElements" ||
      type == "findElementByPosition" ||
      type == "sendKeysByPosition" ||
      type == "clearAndSendKeysByPosition" ||
      type == "selectByValueByPosition" ||
      type == "selectByVisibleTextByPosition" ||
      type == "findElementsByPosition" ||
      type == "findElement" ||
      type == "childElementFindElement" ||
      type == "getPageSource" ||
      type == "getText" ||
      type == "childElementGetText" ||
      type == "getAttribute" ||
      type == "childElementGetAttribute" ||
      type == "goTo" ||
      type == "getUrl" ||
      type == "executeScript" ||
      type == "executeScriptOnElement" ||
      type == "waitUrl" ||
      type == "waitElementExists" ||
      type == "waitElementDisappear" ||
      type == "waitRepositoryElementDisappear" ||
      type == "waitRepositoryElementExists" ||
      type == "childElementWaitElement" ||
      type == "clickWithOffset" ||
      type == "childElementClickWithOffset" ||
      type == "getAlertText" ||
      type == "acceptAlert" ||
      type == "switchToDefaultContent" ||
      type == "sendKeysAlert" ||
      type == "dismissAlert" ||
      type == "getWindowHandle" ||
      type == "getWindowHandles" ||
      type == "switchToWindow" ||
      type == "selectWindow" ||
      type == "switchToNextWindow" ||
      type == "switchToPreviousWindow" ||
      type == "replaceAll" ||
      type == "substring" ||
      type == "split" ||
      type == "format" ||
      type == "datePlus" ||
      type == "dateMinus" ||
      type == "parseInteger" ||
      type == "parseLong" ||
      type == "parseDouble" ||
      type == "parseFloat" ||
      type == "parseDate" ||
      type == "parseDateTime" ||
      type == "newVariable" ||
      type == "assignValue" ||
      type == "applicationRefresh" ||
      type == "closeWindow" ||
      type == "bringForeground" ||
      type == "break" ||
      type == "continue" ||
      type == "error" ||
      type == "finish" ||
      type == "sleep" ||
      type == "comment" ||
      type == "ftpUpload" ||
      type == "ftpDownload" ||
      type == "ftpWaitFile" ||
      type == "sftpUpload" ||
      type == "sftpDownload" ||
      type == "sftpWaitFile" ||
      type == "textFileRead" ||
      type == "textFileWrite" ||
      type == "excelFileRead" ||
      type == "excelFileWrite" ||
      type == "jsonFileRead" ||
      type == "jsonFileWrite") {
    return ActionTemplate(type, title: title, platformsList: platformsList, componentConfig: componentConfig);
  }
  if (type == "assertEquals" || type == "assertNull" || type == "assertNotNull" || type == "assertTrue" || type == "assertFalse") {
    return AssertTemplate(type, title: title, platformsList: platformsList, componentConfig: componentConfig);
  }
  if (type == "condition" || type == "try" || type == "forEach" || type == "while" || type == "group" || type == "client" || type == "step") {
    return ControlTemplate(type, platformsList: platformsList, hasBodyWidget: hasBodyWidget, componentConfig: componentConfig);
  }
  if (type == "add") {
    return ConditionVariableTemplate(category: "variable", operatorType: "+", childrenType: "variable_default", componentConfig: componentConfig);
  }
  if (type == "sub") {
    return ConditionVariableTemplate(category: "variable", operatorType: "-", childrenType: "variable_default", componentConfig: componentConfig);
  }
  if (type == "mul") {
    return ConditionVariableTemplate(category: "variable", operatorType: "x", childrenType: "variable_default", componentConfig: componentConfig);
  }
  if (type == "div") {
    return ConditionVariableTemplate(category: "variable", operatorType: "/", childrenType: "variable_default", componentConfig: componentConfig);
  }
  if (type == "contains") {
    return ConditionVariableTemplate(
        category: "condition", operatorType: "contains", childrenType: "variable_default", componentConfig: componentConfig);
  }
  if (type == "matches") {
    return ConditionVariableTemplate(
        category: "condition", operatorType: "matches", childrenType: "variable_default", componentConfig: componentConfig);
  }
  if (type == "checkWindowExists") {
    return ConditionVariableTemplate(category: "condition", operatorType: "Check Window Exists", componentConfig: componentConfig);
  }
  if (type == "checkElementExists") {
    return ConditionVariableTemplate(category: "condition", operatorType: "Check Element Exists", componentConfig: componentConfig);
  }
  if (type == "checkAlertExists") {
    return ConditionVariableTemplate(category: "condition", operatorType: "Check Alert Exists", componentConfig: componentConfig);
  }
  if (type == "childElementCheckElementExists") {
    return ConditionVariableTemplate(category: "condition", operatorType: "Child Element Check Element Exists", componentConfig: componentConfig);
  }
  if (type == "checkElementEnabled") {
    return ConditionVariableTemplate(category: "condition", operatorType: "Check Element Enabled", componentConfig: componentConfig);
  }
  if (type == "childElementCheckElementEnabled") {
    return ConditionVariableTemplate(category: "condition", operatorType: "Child Element Check Element Enabled", componentConfig: componentConfig);
  }
  if (type == "isNull") {
    return ConditionVariableTemplate(category: "condition", operatorType: "Is Null", componentConfig: componentConfig);
  }
  if (type == "isNotNull") {
    return ConditionVariableTemplate(category: "condition", operatorType: "Is Not Null", componentConfig: componentConfig);
  }
  if (type == "isEmpty") {
    return ConditionVariableTemplate(category: "condition", operatorType: "Is Empty", componentConfig: componentConfig);
  }
  if (type == "isNotEmpty") {
    return ConditionVariableTemplate(category: "condition", operatorType: "Is Not Empty", componentConfig: componentConfig);
  }
  if (type == "gt") {
    return ConditionVariableTemplate(category: "condition", operatorType: ">", childrenType: "variable_default", componentConfig: componentConfig);
  }
  if (type == "gte") {
    return ConditionVariableTemplate(category: "condition", operatorType: ">=", childrenType: "variable_default", componentConfig: componentConfig);
  }
  if (type == "lt") {
    return ConditionVariableTemplate(category: "condition", operatorType: "<", childrenType: "variable_default", componentConfig: componentConfig);
  }
  if (type == "lte") {
    return ConditionVariableTemplate(category: "condition", operatorType: "<=", childrenType: "variable_default", componentConfig: componentConfig);
  }
  if (type == "eq") {
    return ConditionVariableTemplate(category: "condition", operatorType: "==", childrenType: "variable_default", componentConfig: componentConfig);
  }
  if (type == "ne") {
    return ConditionVariableTemplate(category: "condition", operatorType: "!=", childrenType: "variable_default", componentConfig: componentConfig);
  }
  if (type == "AND") {
    return ConditionVariableTemplate(
        category: "conditionGroup", operatorType: "AND", childrenType: "condition_default", componentConfig: componentConfig);
  }
  if (type == "OR") {
    return ConditionVariableTemplate(
        category: "conditionGroup", operatorType: "OR", childrenType: "condition_default", componentConfig: componentConfig);
  }
  if (type == "NOT") {
    return ConditionVariableTemplate(
        category: "conditionGroup", operatorType: "NOT", childrenType: "condition_default", componentConfig: componentConfig);
  }
  return const SizedBox.shrink();
}

var operatorTypeList = [
  {"operatorType": "contains", "type": "contains"},
  {"operatorType": "matches", "type": "matches"},
  {"operatorType": "+", "type": "add"},
  {"operatorType": "-", "type": "sub"},
  {"operatorType": "x", "type": "mul"},
  {"operatorType": "/", "type": "div"},
  {"operatorType": ">", "type": "gt"},
  {"operatorType": ">=", "type": "gte"},
  {"operatorType": "<", "type": "lt"},
  {"operatorType": "<=", "type": "lte"},
  {"operatorType": "==", "type": "eq"},
  {"operatorType": "!=", "type": "ne"},
  {"operatorType": "AND", "type": "AND"},
  {"operatorType": "OR", "type": "OR"},
  {"operatorType": "NOT", "type": "NOT"},
];
List<Map<String, String>> dataTypeList = [
  {"value": "CallOutput", "name": "Call Output"},
  {"value": "String", "name": "String"},
  {"value": "Integer", "name": "Integer"},
  {"value": "Long", "name": "Long"},
  {"value": "Double", "name": "Double"},
  {"value": "Float", "name": "Float"},
  {"value": "Date", "name": "Date"},
  {"value": "DateTime", "name": "DateTime"},
  {"value": "WebElement", "name": "Elements"},
  {"value": "RepositoryElement", "name": "Repository Element"},
  {"value": "Password", "name": "Password"},
  {"value": "Null", "name": "Null"},
  {"value": "EmptyString", "name": "Empty String"},
  {"value": "RandomValue", "name": "RandomValue"},
  {"value": "Json", "name": "Json"},
  {"value": "ExcelWorkbook", "name": "Excel Workbook"},
  {"value": "ExcelSheet", "name": "Excel Sheet"},
  {"value": "ExcelRow", "name": "Excel Row"},
  {"value": "ExcelCell", "name": "Excel Cell"},
];
List mouseActionTypeValueList = [
  {"value": "NONE", "name": "None"},
  {"value": "moveToElement", "name": "Move to Element"},
  {"value": "doubleClick", "name": "Double click"}
];
List<String> keyValueList = [
  // "NULL",
  // "CANCEL",
  // "HELP",
  // "BACK_SPACE",
  "NONE",
  "TAB",
  // "CLEAR",
  // "RETURN",
  "ENTER",
  // "SHIFT",
  // "LEFT_SHIFT",
  // "CONTROL",
  // "LEFT_CONTROL",
  // "ALT",
  // "LEFT_ALT",
  // "PAUSE",
  "ESCAPE",
  // "SPACE",
  // "PAGE_UP",
  // "PAGE_DOWN",
  // "END",
  // "HOME",
  // "LEFT",
  // "ARROW_LEFT",
  // "UP",
  // "ARROW_UP",
  // "RIGHT",
  // "ARROW_RIGHT",
  // "DOWN",
  // "ARROW_DOWN",
  // "INSERT",
  // "DELETE",
  // "SEMICOLON",
  // "EQUALS",
  // "NUMPAD0",
  // "NUMPAD1",
  // "NUMPAD2",
  // "NUMPAD3",
  // "NUMPAD4",
  // "NUMPAD5",
  // "NUMPAD6",
  // "NUMPAD7",
  // "NUMPAD8",
  // "NUMPAD9",
  // "MULTIPLY",
  // "ADD",
  // "SEPARATOR",
  // "SUBTRACT",
  // "DECIMAL",
  // "DIVIDE",
  // "F1",
  // "F2",
  // "F3",
  // "F4",
  // "F5",
  // "F6",
  // "F7",
  // "F8",
  // "F9",
  // "F10",
  // "F11",
  // "F12",
];
handleValidateText(String text) {
  return RegExp(r'^[a-z][a-z0-9_]+$').hasMatch(text);
}

Route createRoute(Widget widget) {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => widget,
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      const begin = Offset(0.0, 1);
      const end = Offset.zero;
      const curve = Curves.linear;
      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    },
  );
}

String convertToAgo(String dateTime) {
  DateTime input = DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(dateTime).toLocal();
  Duration diff = DateTime.now().difference(input);
  if (diff.inDays >= 1) {
    return '${diff.inDays} day${diff.inDays == 1 ? '' : 's'} ago';
  } else if (diff.inHours >= 1) {
    return '${diff.inHours} hour${diff.inHours == 1 ? '' : 's'} ago';
  } else if (diff.inMinutes >= 1) {
    return '${diff.inMinutes} minute${diff.inMinutes == 1 ? '' : 's'} ago';
  } else if (diff.inSeconds >= 1) {
    return '${diff.inSeconds} second${diff.inSeconds == 1 ? '' : 's'} ago';
  } else {
    return 'just now';
  }
}

selectFile(List<String>? allowedExtensions) async {
  FilePickerResult? result = await FilePicker.platform.pickFiles(
    type: FileType.custom,
    allowedExtensions: allowedExtensions,
  );
  if (result == null) {
    return null;
  }
  return result.files.first;
}

readFile(PlatformFile file) async {
  if (kIsWeb == false) {
    if (Platform.isWindows) {
      return await File(file.path!).readAsBytes();
    }
  } else {
    return file.bytes!;
  }
}

List scaleOptionList = [
  {"label": "50%", "value": 0.5},
  {"label": "75%", "value": 0.75},
  {"label": "100%", "value": 1.0},
  {"label": "125%", "value": 1.25},
  {"label": "150%", "value": 1.5},
  {"label": "175%", "value": 1.74},
  {"label": "200%", "value": 2.0},
];
bool checkCategoryBodyListItem(String category) {
  if (category == "control" || category == "action" || category == "assert" || category == "file" || category == "other" || category == "client") {
    return true;
  }
  return false;
}

bool checkStepWidget(String parentType, String itemType) {
  if (itemType == "step" && parentType != "step" && parentType != "rootDefault") {
    return false;
  }
  return true;
}

bool checkPlatformTemplateContainBodyListItem(Item? parentItem, Dragable item) {
  if (item is Template) {
    if (parentItem != null && parentItem.platform != null && !item.platformsList.contains(parentItem.platform)) {
      return false;
    }
    return true;
  }
  if (item is Item) {
    if (parentItem != null && parentItem.platform != null && !(item.platform == parentItem.platform)) {
      return false;
    }
    return true;
  }
  return false;
}

bool checkPlatformTemplateContainFooterItem(String? platform, Dragable item) {
  if (item is Template) {
    if (platform != null && !item.platformsList.contains(platform)) {
      return false;
    }
    return true;
  }
  if (item is Item) {
    if (platform != null && !(item.platform == platform)) {
      return false;
    }
    return true;
  }
  return false;
}

bool conditionCheck(String typeCheck, String categoryCheck, String type) {
  if (type == "config_action") {
    return categoryCheck == "action" &&
        typeCheck != "break" &&
        typeCheck != "continue" &&
        typeCheck != "error" &&
        typeCheck != "finish" &&
        typeCheck != "sleep" &&
        typeCheck != "newVariable" &&
        typeCheck != "assignValue" &&
        typeCheck != "replaceAll" &&
        typeCheck != "substring" &&
        typeCheck != "split" &&
        typeCheck != "format" &&
        typeCheck != "datePlus" &&
        typeCheck != "dateMinus" &&
        typeCheck != "parseInteger" &&
        typeCheck != "parseLong" &&
        typeCheck != "parseDouble" &&
        typeCheck != "parseFloat" &&
        typeCheck != "parseDate" &&
        typeCheck != "parseDateTime";
  }
  if (type == "config_control") {
    return categoryCheck == "control" ||
        typeCheck == "break" ||
        typeCheck == "continue" ||
        typeCheck == "error" ||
        typeCheck == "finish" ||
        typeCheck == "sleep";
  }
  if (type == "config_variable") {
    return categoryCheck == "variable" ||
        typeCheck == "newVariable" ||
        typeCheck == "assignValue" ||
        typeCheck == "replaceAll" ||
        typeCheck == "substring" ||
        typeCheck == "split" ||
        typeCheck == "format" ||
        typeCheck == "datePlus" ||
        typeCheck == "dateMinus" ||
        typeCheck == "parseInteger" ||
        typeCheck == "parseLong" ||
        typeCheck == "parseDouble" ||
        typeCheck == "parseFloat" ||
        typeCheck == "parseDate" ||
        typeCheck == "parseDateTime";
  }
  if (type == "config_operator") {
    return categoryCheck == "condition" || categoryCheck == "conditionGroup";
  }
  if (type == "config_other") {
    return categoryCheck == "other";
  }
  if (type == "config_assert") {
    return categoryCheck == "assert";
  }
  if (type == "config_client") {
    return categoryCheck == "client";
  }
  if (type == "config_file") {
    return categoryCheck == "file";
  }
  if (type == "" && categoryCheck != "category") return true;
  return false;
}

String dateFormatTimeZone(DateFormat dateFormat, String inputString) {
  DateTime date = dateFormat.parse(inputString);
  String timeZone =
      '${date.timeZoneOffset.inHours.toString().padLeft(2, '0')}:${date.timeZoneOffset.inMinutes.remainder(60).toString().padLeft(2, '0')}';
  return "${date.toIso8601String()}+$timeZone";
}
