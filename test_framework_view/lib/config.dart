library config;

import 'package:flutter/foundation.dart' show kIsWeb;

var baseUrl = kIsWeb && Uri.base.origin.contains("localhost") == false ? Uri.base.origin : "https://dev.helptotest.com";
setBaseUrl(var newBaseUrl) {
  baseUrl = newBaseUrl;
}
