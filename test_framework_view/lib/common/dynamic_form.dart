import 'package:flutter/material.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/services.dart';
import 'package:test_framework_view/common/painter.dart';
import 'package:test_framework_view/common/tree.dart';
import 'package:test_framework_view/components/multi_language_text.dart';

import 'custom_state.dart';

class DynamicForm extends StatefulWidget {
  final Map formConfig;
  final Map<dynamic, dynamic> formData;
  final Map<dynamic, dynamic> optionListMap;
  final Function? actionHandler;
  final Function? onChanged;
  const DynamicForm({required this.formConfig, required this.optionListMap, required this.formData, this.actionHandler, this.onChanged, Key? key})
      : super(key: key);

  @override
  State<DynamicForm> createState() => _DynamicFormState();
}

class _DynamicFormState extends CustomState<DynamicForm> {
  Widget buildConfig(Map config) {
    String? type = config["type"];
    if (type == null) {
      return Container();
    }
    if (type == "row") {
      var children = config["children"];
      if (children != null) {
        return Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            for (var child in children)
              child["expanded"] ?? false
                  ? Expanded(
                      flex: (child["flex"] ?? 1),
                      child: buildConfig(child),
                    )
                  : buildConfig(child)
          ],
        );
      }
    }
    if (type == "column") {
      var children = config["children"];
      if (children != null) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: [for (var child in children) buildConfig(child)],
        );
      }
    }
    if (type == "text") {
      return Text("${config["value"] ?? ""}", style: TextStyle(fontWeight: config["fontWeight"], fontSize: config["fontSize"]));
    }
    if (type == "icon") {
      return IconButton(icon: config["value"] ?? "", onPressed: config["onPressed"]);
    }
    if (type == "inputText") {
      var fieldName = config["fieldName"];
      return Padding(
        padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
        child: TextField(
          maxLines: config["maxLines"] ?? 1,
          enabled: !(config["disabled"] ?? false),
          obscureText: config["obscureText"] ?? false,
          controller: TextEditingController(text: widget.formData[fieldName] ?? ""),
          onChanged: (value) {
            widget.formData[fieldName] = value;
            if (widget.onChanged != null) {
              widget.onChanged!(fieldName, value);
            }
          },
          decoration: InputDecoration(
            label: Text(config["label"] ?? ""),
            hintText: config["hintText"],
            border: const OutlineInputBorder(),
          ),
        ),
      );
    }
    if (type == "inputNumber") {
      var fieldName = config["fieldName"];
      return Padding(
        padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
        child: TextField(
          enabled: !(config["disabled"] ?? false),
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.allow(RegExp(r'[0-9.]')),
          ],
          keyboardType: TextInputType.number,
          controller: TextEditingController(text: "${widget.formData[fieldName] ?? ""}"),
          onChanged: (value) {
            try {
              widget.formData[fieldName] = double.parse(value);
              if (widget.onChanged != null) {
                widget.onChanged!(fieldName, value);
              }
            } on FormatException {
              widget.formData[fieldName] = null;
              if (widget.onChanged != null) {
                widget.onChanged!(fieldName, null);
              }
            }
          },
          decoration: InputDecoration(
            label: Text(config["label"] ?? ""),
            hintText: config["hintText"],
            border: const OutlineInputBorder(),
          ),
        ),
      );
    }
    if (type == "switches") {
      var fieldName = config["fieldName"];
      return MySwitches(
          label: config["label"],
          value: widget.formData[fieldName] ?? false,
          onChanged: (value) {
            widget.formData[fieldName] = value;
            if (widget.onChanged != null) {
              widget.onChanged!(fieldName, value);
            }
          });
    }
    if (type == "treeMultipleSelect") {
      var fieldName = config["fieldName"];
      return (config["treeData"] != null && config["getTreeNodeLabel"] != null)
          ? MyTreeMultipleSelect(
              label: config["label"],
              selectedValue: widget.formData[fieldName] ?? [],
              treeData: config["treeData"],
              getTreeNodeLabel: config["getTreeNodeLabel"],
              onChanged: (value) {
                widget.formData[fieldName] = value;
                if (widget.onChanged != null) {
                  widget.onChanged!(fieldName, value);
                }
              })
          : Container();
    }
    if (type == "multipleSelect") {
      var fieldName = config["fieldName"];
      var optionList = widget.optionListMap[fieldName] ?? [];
      return MyMultipleSelect(
          label: config["label"],
          selectedValue: widget.formData[fieldName] ?? [],
          optionList: optionList,
          onChanged: (value) {
            widget.formData[fieldName] = value;
            if (widget.onChanged != null) {
              widget.onChanged!(fieldName, value);
            }
          });
    }
    if (type == "select") {
      var fieldName = config["fieldName"];
      return MyDropdown(
        label: config["label"],
        hintText: config["hintText"],
        selectedValue: widget.formData[fieldName],
        optionList: widget.optionListMap[fieldName] ?? [],
        onChanged: (value) {
          widget.formData[fieldName] = value;
          if (widget.onChanged != null) {
            widget.onChanged!(fieldName, value);
          }
        },
        isValidator: config["isValidator"],
      );
    }

    if (type == "action") {
      var actionName = config["actionName"];
      var actionLabel = config["actionLabel"];
      var actionType = config["actionType"] ?? "primary";
      var width = config["width"];
      var height = config["height"];
      ButtonStyle? buttonStyle;
      MaterialStateProperty<Size>? fixedSize;
      if (width != null && height != null) {
        fixedSize = MaterialStateProperty.all(Size(width, height));
      }
      if (actionType == "danger") {
        buttonStyle = ButtonStyle(fixedSize: fixedSize, backgroundColor: MaterialStateProperty.all<Color>(Colors.red));
      }
      if (actionType == "secondary") {
        buttonStyle = ButtonStyle(
          fixedSize: fixedSize,
          backgroundColor: MaterialStateProperty.all<Color>(Colors.grey),
        );
      }

      return ElevatedButton(
        style: buttonStyle,
        onPressed: () {
          if (widget.actionHandler != null) {
            widget.actionHandler!(actionName, widget.formData);
          }
        },
        child: Text(actionLabel),
      );
    }
    if (type == "sizeBox") {
      var width = config["width"];
      var height = config["height"];
      var child = config["child"];

      return SizedBox(
        width: width,
        height: height,
        child: child,
      );
    }
    if (type == "container") {
      var width = config["width"];
      var height = config["height"];
      var child = config["child"];
      var color = child["color"];
      return Container(
        color: color,
        width: width,
        height: height,
        child: child['type'] == 'inputText'
            ? Container(
                color: child["color"] ?? Colors.red,
                width: child["width"] ?? 100,
                height: child["height"] ?? 100,
              )
            : Container(),
      );
    }

    return MultiLanguageText(
      name: "not_implement_type",
      defaultValue: "Not implement \$0",
      variables: [type],
    );
  }

  @override
  Widget build(BuildContext context) {
    return buildConfig(widget.formConfig);
  }
}

class MySwitches extends StatefulWidget {
  final String? label;
  final bool value;
  final Function? onChanged;
  const MySwitches({required this.value, this.label, this.onChanged, Key? key}) : super(key: key);

  @override
  State<MySwitches> createState() => _MySwitchesState();
}

class _MySwitchesState extends CustomState<MySwitches> {
  bool selectedValue = false;
  @override
  void initState() {
    super.initState();
    selectedValue = widget.value;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
        child: Row(
          children: [
            Switch(
              value: selectedValue,
              onChanged: (value) {
                setState(() {
                  selectedValue = value;
                });
                if (widget.onChanged != null) {
                  widget.onChanged!(value);
                }
              },
              activeTrackColor: Colors.lightGreenAccent,
              activeColor: Colors.green,
            ),
            Expanded(child: Text(widget.label ?? "")),
          ],
        ));
  }
}

class MyTreeMultipleSelect extends StatefulWidget {
  final String? label;
  final List<dynamic> selectedValue;
  final TreeData treeData;
  final Function getTreeNodeLabel;
  final Function? onChanged;
  const MyTreeMultipleSelect(
      {this.label, required this.selectedValue, required this.treeData, required this.getTreeNodeLabel, this.onChanged, Key? key})
      : super(key: key);

  @override
  State<MyTreeMultipleSelect> createState() => _MyTreeMultipleSelectState();
}

class _MyTreeMultipleSelectState extends CustomState<MyTreeMultipleSelect> {
  @override
  Widget build(BuildContext context) {
    var treeNodeList = widget.treeData.getAllNode();
    return Padding(
        padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
        child: Column(
          children: [
            if (widget.label != null)
              Row(
                children: [Expanded(child: Text(widget.label!))],
              ),
            Column(
              children: [
                for (var treeNode in treeNodeList)
                  Padding(
                      padding: EdgeInsets.fromLTRB(treeNode.level * 20, 0, 0, 0),
                      child: Row(
                        children: [
                          Checkbox(
                            value: widget.selectedValue.contains(treeNode.key),
                            onChanged: (bool? value) {
                              setState(() {
                                if (value ?? false) {
                                  widget.selectedValue.add(treeNode.key);
                                } else {
                                  widget.selectedValue.remove(treeNode.key);
                                }
                              });
                              if (widget.onChanged != null) {
                                widget.onChanged!(widget.selectedValue);
                              }
                            },
                          ),
                          Text("${widget.getTreeNodeLabel(treeNode)}"),
                        ],
                      ))
              ],
            )
          ],
        ));
  }
}

class MyMultipleSelect extends StatefulWidget {
  final String? label;
  final List<dynamic> selectedValue;
  final List<dynamic> optionList;
  final Function? onChanged;
  const MyMultipleSelect({this.label, required this.selectedValue, required this.optionList, this.onChanged, Key? key}) : super(key: key);
  @override
  State<MyMultipleSelect> createState() => _MyMultipleSelectState();
}

class _MyMultipleSelectState extends CustomState<MyMultipleSelect> {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
        child: Column(
          children: [
            Row(
              children: [Expanded(child: Text(widget.label ?? ""))],
            ),
            Column(
              children: [
                for (var option in widget.optionList)
                  Padding(
                      padding: EdgeInsets.fromLTRB((option["level"] ?? 0) * 1.0 * 20, 0, 0, 0),
                      child: Row(
                        children: [
                          Checkbox(
                            value: widget.selectedValue.contains(option["value"]),
                            onChanged: (bool? value) {
                              setState(() {
                                if (value ?? false) {
                                  widget.selectedValue.add(option["value"]);
                                } else {
                                  widget.selectedValue.remove(option["value"]);
                                }
                              });
                              if (widget.onChanged != null) {
                                widget.onChanged!(widget.selectedValue);
                              }
                            },
                          ),
                          Text(option["label"] ?? ""),
                        ],
                      ))
              ],
            )
          ],
        ));
  }
}

class MyDropdown extends StatefulWidget {
  final String? label;
  final String? hintText;
  final dynamic selectedValue;
  final List<dynamic> optionList;
  final Function? onChanged;
  final bool isValidator;
  const MyDropdown({this.selectedValue, this.label, this.hintText, this.onChanged, required this.optionList, Key? key, this.isValidator = false})
      : super(key: key);

  @override
  State<MyDropdown> createState() => _MyDropdownState();
}

class _MyDropdownState extends CustomState<MyDropdown> {
  dynamic selectedValue;
  @override
  void initState() {
    super.initState();
    selectedValue = widget.selectedValue;
  }

  @override
  Widget build(BuildContext context) {
    var optionValueList = widget.optionList.map((dynamic option) => option["value"]).toList();
    return Container(
      padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
      child: CustomPaint(
        painter: FormDefaultPainter(label: widget.label),
        child: Row(
          children: [
            Expanded(
              child: DropdownButtonHideUnderline(
                  child: DropdownButtonFormField2(
                hint: Text(
                  widget.hintText ?? "",
                ),
                items: [
                  for (var option in widget.optionList)
                    DropdownMenuItem<dynamic>(
                      enabled: !(option["disabled"] ?? false),
                      value: option["value"],
                      child: Text("${option['label']}"),
                    )
                ],
                value: selectedValue != null && optionValueList.contains(selectedValue) == false ? null : selectedValue,
                onChanged: (dynamic value) {
                  setState(() {
                    selectedValue = value;
                  });
                  if (widget.onChanged != null) {
                    widget.onChanged!(value);
                  }
                },
                validator: (dynamic value) {
                  if ((value == null || value.isEmpty) && widget.isValidator) {
                    return multiLanguageString(name: "please_select", defaultValue: "Please select", context: context);
                  }

                  return null;
                },
              )),
            )
          ],
        ),
      ),
    );
  }
}
