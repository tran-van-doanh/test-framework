import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/type.dart';

class DynamicTextField extends StatefulWidget {
  final TextEditingController? controller;
  final String? hintText;
  final String? labelText;
  final Function? onChanged;
  final bool isRequiredNotEmpty;
  final bool isRequiredRegex;
  final bool? isDuplicate;
  final Color? fillColor;
  final InputBorder? enabledBorder;
  final int? maxline;
  final int? minline;
  final TextInputType? keyboardType;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final Function()? onComplete;
  final bool autofocus;
  final bool readOnly;
  final bool? enabled;
  final bool obscureText;
  final Function(String)? onFieldSubmitted;
  final int? maxLength;
  final List<TextInputFormatter>? inputFormatters;
  final Function()? onTap;
  const DynamicTextField({
    this.controller,
    this.hintText,
    this.labelText,
    this.onChanged,
    this.isRequiredNotEmpty = false,
    this.isRequiredRegex = false,
    Key? key,
    this.fillColor,
    this.enabledBorder,
    this.maxline = 1,
    this.keyboardType,
    this.minline,
    this.isDuplicate,
    this.onComplete,
    this.prefixIcon,
    this.autofocus = false,
    this.readOnly = false,
    this.suffixIcon,
    this.obscureText = false,
    this.enabled,
    this.onFieldSubmitted,
    this.maxLength,
    this.inputFormatters,
    this.onTap,
  }) : super(key: key);

  @override
  State<DynamicTextField> createState() => _DynamicTextFieldState();
}

class _DynamicTextFieldState extends State<DynamicTextField> {
  bool obscureText = true;
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        disabledColor: const Color.fromARGB(255, 83, 85, 88),
      ),
      child: TextFormField(
        maxLength: widget.maxLength,
        obscureText: (widget.maxline == 1 && widget.obscureText) ? obscureText : false,
        readOnly: widget.readOnly,
        enabled: widget.enabled,
        autofocus: widget.autofocus,
        onEditingComplete: widget.onComplete,
        keyboardType: widget.keyboardType,
        inputFormatters:
            (widget.keyboardType == TextInputType.number) ? <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly] : widget.inputFormatters,
        maxLines: widget.maxline,
        minLines: widget.minline,
        controller: widget.controller,
        decoration: InputDecoration(
          contentPadding: const EdgeInsets.fromLTRB(12, 12, 12, 12),
          hintText: widget.hintText,
          fillColor: widget.fillColor,
          hoverColor: Colors.transparent,
          labelText: widget.labelText,
          prefixIcon: widget.prefixIcon,
          suffixIcon: (widget.maxline == 1 && widget.obscureText)
              ? GestureDetector(
                  onTap: () {
                    setState(() {
                      obscureText = !obscureText;
                    });
                  },
                  child: Icon(obscureText ? Icons.visibility : Icons.visibility_off),
                )
              : widget.suffixIcon,
          enabledBorder: widget.enabledBorder ??
              const OutlineInputBorder(
                  borderSide: BorderSide(
                color: Color.fromRGBO(196, 200, 223, 1),
              )),
          border: const OutlineInputBorder(
              borderSide: BorderSide(
            color: Color.fromARGB(255, 199, 199, 199),
          )),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.blue,
            ),
          ),
        ),
        onChanged: widget.onChanged != null ? (text) => widget.onChanged!(text) : null,
        onTap: widget.onTap != null ? () => widget.onTap!() : null,
        onFieldSubmitted: widget.onFieldSubmitted != null ? (value) => widget.onFieldSubmitted!(value) : null,
        validator: (value) {
          if (value != null && widget.isDuplicate == true) {
            return multiLanguageString(name: "data_has_been_duplicated", defaultValue: "Data has been duplicated", context: context);
          } else {
            if (value != null && widget.isDuplicate == false) {
              return null;
            }
          }
          if ((value == null || value.isEmpty) && widget.isRequiredNotEmpty) {
            if (widget.keyboardType == TextInputType.number) {
              return multiLanguageString(name: "please_enter_a_number", defaultValue: "Please enter a number", context: context);
            } else {
              return multiLanguageString(name: "this_field_is_required", defaultValue: "This field is required!", context: context);
            }
          } else {
            if (!handleValidateText(value!) && widget.isRequiredRegex) {
              return multiLanguageString(
                  name: "text_must_be_start_with_letter",
                  defaultValue: "Text must be start with letter and contain lowerCases, numbers, '_' !!!",
                  context: context);
            }
          }
          return null;
        },
      ),
    );
  }
}
