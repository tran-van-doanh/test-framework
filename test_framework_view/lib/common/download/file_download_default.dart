import 'dart:convert';
import 'dart:io';

import 'package:disks_desktop/disks_desktop.dart';
import 'package:filesystem_picker/filesystem_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:test_framework_view/config.dart';
import 'package:permission_handler/permission_handler.dart';

getRootDirectory() async {
  if (Platform.isWindows) {
    return Directory("C:\\");
  }
  Directory? rootPath;
  if (Platform.isAndroid) {
    if (await Permission.manageExternalStorage.request().isGranted) {
      rootPath = await getExternalStorageDirectory();
    }
  }
  rootPath ??= await getApplicationDocumentsDirectory();
  return rootPath;
}

getShortcuts() async {
  var repository = DisksRepository();
  var disks = await repository.query;
  List<FilesystemPickerShortcut> shortcuts = [];
  for (var disk in disks) {
    for (var mountpoint in disk.mountpoints) {
      shortcuts.add(FilesystemPickerShortcut(name: mountpoint.path, path: Directory(mountpoint.path)));
    }
  }
  return shortcuts;
}

selectDirectory(context) async {
  if (Platform.isWindows) {
    return await FilesystemPicker.open(
      title: 'Save to folder',
      context: context,
      showGoUp: true,
      fsType: FilesystemType.folder,
      pickText: 'Save file to this folder',
      shortcuts: await getShortcuts(),
      contextActions: [
        FilesystemPickerNewFolderContextAction(),
      ],
    );
  } else {
    return await FilesystemPicker.open(
      title: 'Save to folder',
      context: context,
      showGoUp: true,
      rootDirectory: await getRootDirectory(),
      fsType: FilesystemType.folder,
      pickText: 'Save file to this folder',
      contextActions: [
        FilesystemPickerNewFolderContextAction(),
      ],
    );
  }
}

Future<File?> httpDownloadFile(BuildContext context, String url, String? authorization, String filename) async {
  String? path = await selectDirectory(context);
  if (path != null) {
    Map<String, String> headers = {};
    if (authorization != null) {
      headers["Authorization"] = authorization;
    }
    var res = await http.get(Uri.parse("$baseUrl$url"), headers: headers);
    if (res.statusCode == 200) {
      String downloadFileName = filename;
      if (res.headers.containsKey("content-disposition")) {
        String contentDisposition = res.headers["content-disposition"] ?? "";
        if (contentDisposition.contains("filename")) {
          downloadFileName = contentDisposition.substring(contentDisposition.indexOf("filename=") + 9);
          while (downloadFileName.startsWith("\"")) {
            downloadFileName = downloadFileName.substring(1);
          }
          if (downloadFileName.contains("\"")) {
            downloadFileName = downloadFileName.substring(0, downloadFileName.indexOf("\""));
          }
        }
      }
      File file = File(Platform.isWindows ? '$path\\$downloadFileName' : '$path/$downloadFileName');
      file.writeAsBytesSync(res.bodyBytes, mode: FileMode.write);
      return file;
    }
  }
  return null;
}

Future<File?> httpDownloadFileWithBodyRequest(BuildContext context, String url, String? authorization, String filename, bodyRequest) async {
  String? path = await selectDirectory(context);
  if (path != null) {
    Map<String, String> headers = {'content-type': 'application/json'};
    var finalRequestBody = json.encode(bodyRequest);
    if (authorization != null) {
      headers["Authorization"] = authorization;
    }
    var res = await http.post(Uri.parse("$baseUrl$url"), headers: headers, body: finalRequestBody);
    if (res.statusCode == 200) {
      String downloadFileName = filename;
      if (res.headers.containsKey("content-disposition")) {
        String contentDisposition = res.headers["content-disposition"] ?? "";
        if (contentDisposition.contains("filename")) {
          downloadFileName = contentDisposition.substring(contentDisposition.indexOf("filename=") + 9);
          while (downloadFileName.startsWith("\"")) {
            downloadFileName = downloadFileName.substring(1);
          }
          if (downloadFileName.contains("\"")) {
            downloadFileName = downloadFileName.substring(0, downloadFileName.indexOf("\""));
          }
        }
      }
      File file = File('$path/$downloadFileName');
      await file.writeAsBytes(res.bodyBytes);
      return file;
    }
  }
  return null;
}
