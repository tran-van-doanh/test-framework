import 'dart:convert';
import 'dart:io';

import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:test_framework_view/config.dart';
// ignore: avoid_web_libraries_in_flutter
import 'dart:html' as html;

Future<File?> httpDownloadFile(BuildContext context,String url, String? authorization, String filename) async {
  Map<String, String> headers = {};
  if (authorization != null) {
    headers["Authorization"] = authorization;
  }
  var res = await http.get(Uri.parse("$baseUrl$url"), headers: headers);

  if (res.statusCode == 200) {
    String downloadFileName=filename;
    if(res.headers.containsKey("content-disposition")){
      String contentDisposition=res.headers["content-disposition"]??"";
      if(contentDisposition.contains("filename")){
        downloadFileName=contentDisposition.substring(contentDisposition.indexOf("filename=")+9);
        while(downloadFileName.startsWith("\"")){
          downloadFileName=downloadFileName.substring(1);
        }
        if(downloadFileName.contains("\"")){
          downloadFileName=downloadFileName.substring(0,downloadFileName.indexOf("\""));
        }
      }
    }
    final blob = html.Blob([res.bodyBytes]);
    final url = html.Url.createObjectUrlFromBlob(blob);
    final anchor = html.document.createElement('a') as html.AnchorElement
      ..href = url
      ..style.display = 'none'
      ..download = downloadFileName;
    html.document.body!.children.add(anchor);
    anchor.click();
    html.document.body!.children.remove(anchor);
    html.Url.revokeObjectUrl(url);
  }
  return null;
}

Future<File?> httpDownloadFileWithBodyRequest(BuildContext context,String url, String? authorization, String filename, bodyRequest) async {
  Map<String, String> headers = {'content-type': 'application/json'};
  var finalRequestBody = json.encode(bodyRequest);
  if (authorization != null) {
    headers["Authorization"] = authorization;
  }

  var res = await http.post(Uri.parse("$baseUrl$url"), headers: headers, body: finalRequestBody);

  if (res.statusCode == 200) {
    String downloadFileName=filename;
    if(res.headers.containsKey("content-disposition")){
      String contentDisposition=res.headers["content-disposition"]??"";
      if(contentDisposition.contains("filename")){
        downloadFileName=contentDisposition.substring(contentDisposition.indexOf("filename=")+9);
        while(downloadFileName.startsWith("\"")){
          downloadFileName=downloadFileName.substring(1);
        }
        if(downloadFileName.contains("\"")){
          downloadFileName=downloadFileName.substring(0,downloadFileName.indexOf("\""));
        }
      }
    }
    final blob = html.Blob([res.bodyBytes]);
    final url = html.Url.createObjectUrlFromBlob(blob);
    final anchor = html.document.createElement('a') as html.AnchorElement
      ..href = url
      ..style.display = 'none'
      ..download = downloadFileName;
    html.document.body!.children.add(anchor);
    anchor.click();
    html.document.body!.children.remove(anchor);
    html.Url.revokeObjectUrl(url);
  }
  return null;
}
