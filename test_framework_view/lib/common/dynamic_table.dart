import 'package:flutter/material.dart';
import 'package:test_framework_view/components/multi_language_text.dart';

class DynamicTablePagging extends StatelessWidget {
  final int rowCount;
  final int currentPage;
  final int rowPerPage;
  final Function? pageChangeHandler;
  final Function? rowPerPageChangeHandler;
  const DynamicTablePagging(this.rowCount, this.currentPage, this.rowPerPage, {Key? key, this.pageChangeHandler, this.rowPerPageChangeHandler})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (rowCount > 0) {
      var firstRow = (currentPage - 1) * rowPerPage + 1;
      var lastRow = currentPage * rowPerPage;
      if (lastRow > rowCount) {
        lastRow = rowCount;
      }
      return Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          const MultiLanguageText(name: "row_per_page", defaultValue: "Row per page", suffiText: " : "),
          DropdownButton<int>(
            value: rowPerPage,
            icon: const Icon(Icons.keyboard_arrow_down),
            elevation: 16,
            style: const TextStyle(color: Colors.deepPurple),
            underline: Container(
              height: 2,
              color: Colors.deepPurpleAccent,
            ),
            onChanged: (int? newValue) {
              if (rowPerPageChangeHandler != null) {
                rowPerPageChangeHandler!(newValue);
              }
            },
            items: <int>[2, 5, 10, 25, 50, 100].map<DropdownMenuItem<int>>((int value) {
              return DropdownMenuItem<int>(
                value: value,
                child: Text("$value"),
              );
            }).toList(),
          ),
          MultiLanguageText(name: "row_of", defaultValue: "Row \$0 - \$1 of \$2", variables: ["$firstRow", "$lastRow", "$rowCount"]),
          IconButton(
              onPressed: (pageChangeHandler != null && firstRow != 1)
                  ? () {
                      pageChangeHandler!(currentPage - 1);
                    }
                  : null,
              icon: const Icon(Icons.chevron_left)),
          IconButton(
              onPressed: (pageChangeHandler != null && lastRow < rowCount)
                  ? () {
                      pageChangeHandler!(currentPage + 1);
                    }
                  : null,
              icon: const Icon(Icons.chevron_right)),
        ],
      );
    }
    return Container();
  }
}

class CustomDataTableWidget extends StatefulWidget {
  final List<Widget>? columns;
  final List<CustomDataRow> rows;
  final double minWidth;
  final int lengthScroll;
  final double widthColumnScroll;
  const CustomDataTableWidget({
    Key? key,
    this.columns,
    required this.rows,
    this.minWidth = 1200,
    this.lengthScroll = 10,
    this.widthColumnScroll = 250,
  }) : super(key: key);

  @override
  State<CustomDataTableWidget> createState() => _CustomDataTableWidgetState();
}

class _CustomDataTableWidgetState extends State<CustomDataTableWidget> {
  final _scrollController = ScrollController();
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      if (constraints.maxWidth < widget.minWidth) {
        return Scrollbar(
          thumbVisibility: true,
          controller: _scrollController,
          child: SingleChildScrollView(
            controller: _scrollController,
            scrollDirection: Axis.horizontal,
            child: SizedBox(
              width: widget.minWidth,
              child: tableWidget(),
            ),
          ),
        );
      } else {
        if (widget.columns != null && widget.columns!.length >= widget.lengthScroll) {
          return Scrollbar(
            thumbVisibility: true,
            controller: _scrollController,
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              controller: _scrollController,
              child: SizedBox(
                width: widget.columns!.length * widget.widthColumnScroll,
                child: tableWidget(),
              ),
            ),
          );
        }
        return tableWidget();
      }
    });
  }

  Column tableWidget() {
    return Column(
      children: [
        if (widget.columns != null)
          Container(
            padding: const EdgeInsets.all(13),
            decoration: BoxDecoration(
              border: Border.all(
                color: const Color.fromRGBO(212, 212, 212, 1),
              ),
              color: Colors.grey[100],
            ),
            child: Row(
              children: [
                for (Widget column in widget.columns!) column,
              ],
            ),
          ),
        Column(
          children: [
            for (Widget row in widget.rows) row,
          ],
        )
      ],
    );
  }
}

class CustomDataRow extends StatefulWidget {
  final List<Widget> cells;
  final List<Widget>? cell2;
  final Function? onSelectChanged;
  final EdgeInsetsGeometry? padding;
  const CustomDataRow({Key? key, this.onSelectChanged, required this.cells, this.cell2, this.padding}) : super(key: key);

  @override
  State<CustomDataRow> createState() => _CustomDataRowState();
}

class _CustomDataRowState extends State<CustomDataRow> {
  bool isHovered = false;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onSelectChanged != null
          ? () {
              widget.onSelectChanged!();
            }
          : null,
      child: MouseRegion(
          onEnter: (_) {
            setState(() {
              isHovered = true;
            });
          },
          onExit: (_) {
            setState(() {
              isHovered = false;
            });
          },
          child: Container(
            padding: widget.padding ?? const EdgeInsets.all(5),
            decoration: BoxDecoration(
              border: const Border(
                left: BorderSide(
                  color: Color.fromRGBO(212, 212, 212, 1),
                ),
                bottom: BorderSide(
                  color: Color.fromRGBO(212, 212, 212, 1),
                ),
                right: BorderSide(
                  color: Color.fromRGBO(212, 212, 212, 1),
                ),
              ),
              color: isHovered ? Colors.blue[50] : Colors.white,
            ),
            child: (widget.cell2 != null)
                ? Column(
                    children: [
                      Row(
                        children: [
                          for (Widget cell in widget.cells) cell,
                        ],
                      ),
                      Row(
                        children: [
                          for (Widget cell in widget.cell2!) cell,
                        ],
                      ),
                    ],
                  )
                : Row(
                    children: [
                      for (Widget cell in widget.cells) cell,
                    ],
                  ),
          )),
    );
  }
}
