import 'package:flutter/material.dart';

abstract class CustomState<T extends StatefulWidget> extends State<T> {
  var disopsed = false;

  @override
  void initState() {
    super.initState();
    disopsed = false;
  }

  @override
  void dispose() {
    disopsed = true;
    super.dispose();
  }

  @override
  setState(VoidCallback fn) {
    if (disopsed) {
      return;
    }
    super.setState(fn);
  }
}
