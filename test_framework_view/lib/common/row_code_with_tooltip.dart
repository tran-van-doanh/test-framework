import 'package:flutter/material.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';

class RowCodeWithTooltipWidget extends StatelessWidget {
  const RowCodeWithTooltipWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        MultiLanguageText(name: "code", defaultValue: "Code", style: Style(context).styleTitleDialog),
        const SizedBox(
          width: 5,
        ),
        Tooltip(
          message: multiLanguageString(
              name: "the_input_requires_with_ex",
              defaultValue: 'Input must be start with the letters a-z, which must only contain a-z, numbers & "_".\nEx: tc_001_xx',
              context: context),
          verticalOffset: 15,
          margin: const EdgeInsets.only(left: 70),
          child: const Icon(
            Icons.info,
            color: Colors.grey,
            size: 18,
          ),
        ),
      ],
    );
  }
}
