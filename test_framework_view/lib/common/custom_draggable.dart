import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'dart:io' show Platform;

class CustomDraggable<T extends Object> extends StatelessWidget {
  final Widget child;
  final Widget feedback;
  final Widget? childWhenDragging;
  final T? data;
  final void Function()? onDragStarted;
  final void Function()? onDragCompleted;
  final void Function(Velocity, Offset)? onDraggableCanceled;
  final void Function(DraggableDetails)? onDragEnd;
  const CustomDraggable(
      {required this.child,
      required this.feedback,
      this.childWhenDragging,
      this.onDragStarted,
      this.onDragCompleted,
      this.onDraggableCanceled,
      this.onDragEnd,
      this.data,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (kIsWeb == false && (Platform.isAndroid || Platform.isIOS)) {
      return LongPressDraggable<T>(
          feedback: feedback,
          data: data,
          childWhenDragging: childWhenDragging,
          onDragStarted: onDragStarted,
          onDragCompleted: onDragCompleted,
          onDraggableCanceled: onDraggableCanceled,
          child: child);
    } else {
      return Draggable<T>(
          feedback: feedback,
          data: data,
          childWhenDragging: childWhenDragging,
          onDragStarted: onDragStarted,
          onDragCompleted: onDragCompleted,
          onDraggableCanceled: onDraggableCanceled,
          onDragEnd: onDragEnd,
          child: child);
    }
  }
}
