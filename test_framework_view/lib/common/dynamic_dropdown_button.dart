import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/components/style.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';

class DynamicDropdownButton extends StatelessWidget {
  final GlobalKey<FormFieldState>? keyReset;
  final dynamic value;
  final String? hint;
  final List<DropdownMenuItem>? items;
  final Function? onChanged;
  final bool isRequiredNotEmpty;
  final double? borderRadius;
  final String? labelText;
  final bool enabled;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  const DynamicDropdownButton({
    this.value,
    this.hint,
    this.items,
    this.onChanged,
    this.isRequiredNotEmpty = false,
    Key? key,
    this.borderRadius,
    this.labelText,
    this.enabled = true,
    this.keyReset,
    this.prefixIcon,
    this.suffixIcon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      ignoring: !enabled,
      child: DropdownButtonFormField2<dynamic>(
        isExpanded: true,
        decoration: InputDecoration(
          prefixIcon: prefixIcon,
          suffixIcon: suffixIcon,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(borderRadius ?? 0),
          ),
          contentPadding: const EdgeInsets.fromLTRB(0, 5, 5, 5),
          labelText: labelText,
          labelStyle: Style(context).styleTitleDialog,
          enabledBorder: const OutlineInputBorder(
            borderSide: BorderSide(
              color: Color.fromRGBO(196, 200, 223, 1),
            ),
          ),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.blue,
            ),
          ),
        ),
        value: value,
        onChanged: onChanged != null
            ? (dynamic newValue) {
                onChanged!(newValue);
              }
            : null,
        items: items,
        hint: Text(
          value != null ? value.toString() : hint ?? "",
          style: const TextStyle(fontWeight: FontWeight.bold, overflow: TextOverflow.ellipsis),
        ),
        validator: (value) {
          if ((value == null) && isRequiredNotEmpty) {
            return multiLanguageString(name: "this_field_is_required", defaultValue: "This field is required!", context: context);
          }
          return null;
        },
      ),
    );
  }
}

class DynamicDropdownSearchClearOption extends StatelessWidget {
  final GlobalKey<FormFieldState>? keyReset;
  final SingleValueDropDownController? controller;
  final String? hint;
  final List<DropDownValueModel>? items;
  final Function(dynamic)? onChanged;
  final bool isRequiredNotEmpty;
  final double? borderRadius;
  final double? maxHeight;
  final String? labelText;
  final bool enabled;
  const DynamicDropdownSearchClearOption({
    this.controller,
    this.hint,
    this.items,
    this.onChanged,
    this.isRequiredNotEmpty = false,
    Key? key,
    this.borderRadius,
    this.maxHeight,
    this.labelText,
    this.keyReset,
    this.enabled = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      ignoring: !enabled,
      child: DropDownTextField(
        key: key,
        isEnabled: true,
        textFieldDecoration: InputDecoration(
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.blue,
            ),
          ),
          border: const OutlineInputBorder(),
          filled: true,
          fillColor: Colors.white,
          disabledBorder: InputBorder.none,
          enabledBorder: const OutlineInputBorder(
            borderSide: BorderSide(
              color: Color.fromRGBO(196, 200, 223, 1),
            ),
          ),
          labelText: labelText,
          contentPadding: const EdgeInsets.symmetric(horizontal: 15),
        ),
        listSpace: 0,
        searchAutofocus: true,
        searchShowCursor: true,
        dropdownRadius: 0,
        controller: controller,
        clearOption: true,
        enableSearch: true,
        clearIconProperty: IconProperty(color: Colors.grey),
        dropdownColor: Colors.white,
        searchDecoration: InputDecoration(
          hintText: hint,
          contentPadding: const EdgeInsets.symmetric(horizontal: 15),
          border: const OutlineInputBorder(),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.blue,
            ),
          ),
        ),
        validator: (value) {
          if ((value == null || value.isEmpty) && isRequiredNotEmpty) {
            return multiLanguageString(name: "this_field_is_required", defaultValue: "This field is required!", context: context);
          }
          return null;
        },
        dropDownItemCount: 4,
        dropDownList: items!,
        onChanged: (dynamic newValue) {
          onChanged!(newValue);
        },
      ),
    );
  }
}

class DynamicDropdownSearch extends StatelessWidget {
  final String? value;
  final String? hint;
  final List<String>? items;
  final Function? onChanged;
  final String? selectedItem;
  final double? maxHeight;
  final String? labelText;
  final Color? fillColor;
  const DynamicDropdownSearch({
    Key? key,
    this.value,
    this.hint,
    this.items,
    this.onChanged,
    this.selectedItem,
    this.maxHeight,
    this.labelText,
    this.fillColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DropdownSearch<String>(
      popupProps: PopupProps.menu(
        isFilterOnline: true,
        showSelectedItems: true,
        emptyBuilder: (context, searchEntry) => const Padding(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: Align(alignment: Alignment.center, child: MultiLanguageText(name: "no_data", defaultValue: "No data")),
        ),
        showSearchBox: true,
        constraints: BoxConstraints(maxHeight: maxHeight!),
        searchFieldProps: TextFieldProps(
          decoration: InputDecoration(
            border: const OutlineInputBorder(),
            hintText: multiLanguageString(name: "enter_a_name", defaultValue: "Enter a name", context: context),
            focusedBorder: const OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.blue,
              ),
            ),
          ),
        ),
      ),
      items: items!,
      dropdownDecoratorProps: DropDownDecoratorProps(
        dropdownSearchDecoration: InputDecoration(
          border: const OutlineInputBorder(),
          filled: true,
          fillColor: fillColor ?? Colors.white,
          disabledBorder: InputBorder.none,
          labelText: labelText,
          contentPadding: const EdgeInsets.symmetric(horizontal: 15),
          suffixIcon: const Icon(Icons.add),
          hintText: hint,
          hintStyle: TextStyle(
            color: Colors.black,
            fontSize: context.fontSizeManager.fontSizeText12,
          ),
        ),
      ),
      onChanged: (String? newValue) {
        onChanged!(newValue);
      },
      selectedItem: selectedItem,
    );
  }
}
