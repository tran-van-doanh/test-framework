class TreeData {
  List<TreeNode> rootList = [];
  TreeData(List<TreeNode> treeNodeList) {
    Map<dynamic, TreeNode> treeNodeMap = {};
    for (TreeNode treeNode in treeNodeList) {
      treeNodeMap[treeNode.key] = treeNode;
    }
    for (TreeNode treeNode in treeNodeList) {
      if (treeNode.parentKey == null) {
        rootList.add(treeNode);
      } else {
        if (treeNodeMap.containsKey(treeNode.parentKey)) {
          TreeNode parentTreeNode = treeNodeMap[treeNode.parentKey]!;
          treeNode.parent = parentTreeNode;
          parentTreeNode.hasChildren = true;
          parentTreeNode.children.add(treeNode);
        }
      }
    }
    List<TreeNode> processingNodeList = [];
    processingNodeList.addAll(rootList);
    while (processingNodeList.isNotEmpty) {
      TreeNode processingNode = processingNodeList.removeAt(0);
      if (processingNode.children.isNotEmpty) {
        for (TreeNode childNode in processingNode.children) {
          childNode.level = processingNode.level + 1;
        }
        processingNodeList.insertAll(0, processingNode.children);
      }
    }
  }

  expandNode(TreeNode treeNode) {
    treeNode.isExpanded = true;
  }

  expandAllNode() {
    for (TreeNode treeNode in getAllNode()) {
      if (treeNode.children.isNotEmpty) {
        treeNode.isExpanded = true;
      }
    }
  }

  collapseNode(TreeNode treeNode) {
    treeNode.isExpanded = false;
  }

  collapseAllNode() {
    for (TreeNode treeNode in getAllNode()) {
      if (treeNode.children.isNotEmpty) {
        treeNode.isExpanded = false;
      }
    }
  }

  List<TreeNode> getExpandedNode() {
    List<TreeNode> resultList = [];
    List<TreeNode> processingNodeList = [];
    processingNodeList.addAll(rootList);
    while (processingNodeList.isNotEmpty) {
      TreeNode processingNode = processingNodeList.removeAt(0);
      resultList.add(processingNode);
      if (processingNode.isExpanded && processingNode.children.isNotEmpty) {
        for (TreeNode childNode in processingNode.children) {
          childNode.level = processingNode.level + 1;
        }
        processingNodeList.insertAll(0, processingNode.children);
      }
    }
    return resultList;
  }

  List<TreeNode> getAllNode() {
    List<TreeNode> resultList = [];
    List<TreeNode> processingNodeList = [];
    processingNodeList.addAll(rootList);
    while (processingNodeList.isNotEmpty) {
      TreeNode processingNode = processingNodeList.removeAt(0);
      resultList.add(processingNode);
      if (processingNode.children.isNotEmpty) {
        for (TreeNode childNode in processingNode.children) {
          childNode.level = processingNode.level + 1;
        }
        processingNodeList.insertAll(0, processingNode.children);
      }
    }
    return resultList;
  }
}

class TreeNode {
  dynamic key;
  dynamic parentKey;
  dynamic item;
  int level = 1;
  bool isExpanded = false;
  bool hasChildren = false;
  TreeNode? parent;
  List<TreeNode> children = [];

  TreeNode({this.key, this.parentKey, this.item});
}