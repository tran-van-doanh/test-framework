import 'package:flutter/material.dart';
import 'package:test_framework_view/components/multi_language_text.dart';
import 'package:test_framework_view/extension/font_size_manager.dart';

class DynamicPagging extends StatelessWidget {
  final int rowCount;
  final int currentItem;
  final Function? pageChangeHandler;
  const DynamicPagging(this.rowCount, this.currentItem, {Key? key, this.pageChangeHandler}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (rowCount > 0) {
      return Wrap(
        crossAxisAlignment: WrapCrossAlignment.center,
        spacing: 20,
        runSpacing: 10,
        children: [
          GestureDetector(
              onTap: (pageChangeHandler != null && currentItem != 1)
                  ? () {
                      pageChangeHandler!(currentItem - 1);
                    }
                  : null,
              child: Wrap(
                crossAxisAlignment: WrapCrossAlignment.center,
                children: [
                  Icon(
                    Icons.chevron_left,
                    color: (pageChangeHandler != null && currentItem != 1) ? Colors.blueAccent : Colors.grey,
                  ),
                  MultiLanguageText(
                    name: "previous",
                    defaultValue: "Previous",
                    style: TextStyle(
                      color: (pageChangeHandler != null && currentItem != 1) ? Colors.blueAccent : Colors.grey,
                      fontSize: context.fontSizeManager.fontSizeText14,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 2,
                    ),
                  ),
                ],
              )),
          MultiLanguageText(
            name: "show_of",
            defaultValue: "Show \$0 of \$1",
            variables: ["$currentItem", "$rowCount"],
            style: TextStyle(
              color: const Color.fromRGBO(0, 0, 0, 0.8),
              fontSize: context.fontSizeManager.fontSizeText14,
              fontWeight: FontWeight.bold,
              letterSpacing: 2,
            ),
          ),
          GestureDetector(
              onTap: (pageChangeHandler != null && currentItem < rowCount)
                  ? () {
                      pageChangeHandler!(currentItem + 1);
                    }
                  : null,
              child: Wrap(
                crossAxisAlignment: WrapCrossAlignment.center,
                children: [
                  MultiLanguageText(
                    name: "next",
                    defaultValue: "Next",
                    style: TextStyle(
                      color: (pageChangeHandler != null && currentItem < rowCount) ? Colors.blueAccent : Colors.grey,
                      fontSize: context.fontSizeManager.fontSizeText14,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 2,
                    ),
                  ),
                  Icon(
                    Icons.chevron_right,
                    color: (pageChangeHandler != null && currentItem < rowCount) ? Colors.blueAccent : Colors.grey,
                  ),
                ],
              )),
        ],
      );
    }
    return Container();
  }
}
