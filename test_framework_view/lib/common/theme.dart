import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CommonThemeMethod {
  ThemeData themedata = ThemeData(
    useMaterial3: false,
    fontFamily: 'Poppins',
    fontFamilyFallback: const ["Poppins"],
    textTheme: GoogleFonts.poppinsTextTheme(),
    appBarTheme: const AppBarTheme(color: Colors.white),
    // checkboxTheme: CheckboxThemeData(
    //   fillColor: MaterialStateColor.resolveWith((states) {
    //     if (states.contains(MaterialState.selected)) {
    //       return Colors.blue; // Màu nền khi checkbox được chọn
    //     } else {
    //       return Colors.grey; // Màu nền khi checkbox không được chọn
    //     }
    //   }),
    // ),
    // floatingActionButtonTheme: FloatingActionButtonThemeData(
    //   sizeConstraints: const BoxConstraints(minHeight: 45, minWidth: 40),
    //   extendedSizeConstraints: const BoxConstraints(minHeight: 50, minWidth: 40),
    //   foregroundColor: Colors.white,
    //   backgroundColor: Colors.blue,
    //   shape: RoundedRectangleBorder(
    //     borderRadius: BorderRadius.circular(25), // Đặt bán kính cong của góc
    //   ),
    //   // Các thuộc tính khác tùy chỉnh cho FAB
    // ),
    // popupMenuTheme: PopupMenuThemeData(
    //   shape: RoundedRectangleBorder(
    //     borderRadius: BorderRadius.circular(8),
    //   ),
    //   color: Colors.white,
    //   textStyle: const TextStyle(fontSize: 16),
    // ),
    // dialogBackgroundColor: const Color.fromARGB(255, 255, 255, 255),
    // elevatedButtonTheme: ElevatedButtonThemeData(
    //   style: ElevatedButton.styleFrom(
    //     foregroundColor: Colors.white,
    //     backgroundColor: Colors.blue,
    //     minimumSize: const Size(64, 36),
    //     textStyle: const TextStyle(fontWeight: FontWeight.bold, letterSpacing: 1.2),
    //     shape: RoundedRectangleBorder(
    //       borderRadius: BorderRadius.circular(4),
    //     ),
    //   ),
    // ),
    // scaffoldBackgroundColor: Colors.white,
  );
}
