import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:test_framework_view/components/multi_language_text.dart';

import 'dynamic_text_field.dart';

class ColorPickerWidget extends StatelessWidget {
  final Color pickerColor;
  final void Function(Color) onColorChanged;
  final TextEditingController? hexInputController;
  final Function? onCopyToClipboard;
  final Function onSubmit;
  final bool isColorIcon;
  const ColorPickerWidget(
      {Key? key,
      required this.pickerColor,
      required this.onColorChanged,
      this.hexInputController,
      this.onCopyToClipboard,
      required this.onSubmit,
      this.isColorIcon = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              scrollable: true,
              titlePadding: const EdgeInsets.all(0),
              contentPadding: const EdgeInsets.all(0),
              content: Column(
                children: [
                  ColorPicker(
                    pickerColor: pickerColor,
                    onColorChanged: onColorChanged,
                    colorPickerWidth: 300,
                    pickerAreaHeightPercent: 0.7,
                    enableAlpha: true,
                    displayThumbColor: true,
                    paletteType: PaletteType.hsvWithHue,
                    labelTypes: const [],
                    pickerAreaBorderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(2),
                      topRight: Radius.circular(2),
                    ),
                    hexInputController: hexInputController,
                    portraitOnly: true,
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
                    child: DynamicTextField(
                      controller: hexInputController,
                      prefixIcon: const Padding(padding: EdgeInsets.only(left: 8), child: Icon(Icons.tag)),
                      suffixIcon: onCopyToClipboard != null
                          ? IconButton(
                              icon: const Icon(Icons.content_paste_rounded),
                              onPressed: () => onCopyToClipboard!(pickerColor.value.toString()),
                            )
                          : null,
                      autofocus: true,
                      maxLength: 10,
                      inputFormatters: [
                        UpperCaseTextFormatter(),
                        FilteringTextInputFormatter.allow(RegExp(kValidHexPattern)),
                      ],
                    ),
                  ),
                  ElevatedButton(
                    child: const MultiLanguageText(name: "got_it", defaultValue: "Got it"),
                    onPressed: () {
                      Navigator.of(context).pop();
                      onSubmit();
                    },
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                ],
              ),
            );
          },
        );
      },
      child: Icon(
        Icons.palette,
        color: isColorIcon ? pickerColor : null,
      ),
    );
  }
}
