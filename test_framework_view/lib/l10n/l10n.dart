import 'package:flutter/cupertino.dart';

class L10n {
  static const List<Locale> all = [
    Locale('en'),
    Locale('vi'),
  ];
}
