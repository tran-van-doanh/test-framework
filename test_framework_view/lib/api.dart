library api;

import 'dart:convert';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'package:test_framework_view/config.dart';
import 'package:test_framework_view/model/model.dart';

checkLogin(securityModel) async {
  Map<String, String> headers = {'content-type': 'application/json'};
  if (securityModel.authorization != null) {
    headers["Authorization"] = securityModel.authorization!;
  }
  var response = await http.get(Uri.parse('$baseUrl/test-framework-api/auth/user-detail'), headers: headers);
  return response.statusCode == 200;
}

httpGet(url, context) async {
  var securityModel = Provider.of<SecurityModel>(context, listen: false);
  var languageModel = Provider.of<LanguageModel>(context, listen: false);
  Map<String, String> headers = {'content-type': 'application/json'};
  if (securityModel.authorization != null) {
    headers["Authorization"] = securityModel.authorization!;
  }
  headers["language"] = languageModel.currentLanguage??"en";
  var response = await http.get(Uri.parse('$baseUrl$url'), headers: headers);
  if (response.statusCode == 403) {
    if (await checkLogin(securityModel) == false) {
      securityModel.clearProvider();
      return {
        "headers": response.headers,
        "body": {"errorCode": "NOT_AUTHENTICATED", "errorMessage": "NOT_AUTHENTICATED"}
      };
    }
  }
  if (response.headers["content-type"] == 'application/json') {
    try {
      return {"headers": response.headers, "body": json.decode(utf8.decode(response.bodyBytes))};
    } on FormatException {
      //bypass
    }
  }
  return {"headers": response.headers, "body": utf8.decode(response.bodyBytes)};
}

httpPost(url, requestBody, context) async {
  var securityModel = Provider.of<SecurityModel>(context, listen: false);
  var languageModel = Provider.of<LanguageModel>(context, listen: false);
  Map<String, String> headers = {'content-type': 'application/json'};
  if (securityModel.authorization != null) {
    headers["Authorization"] = securityModel.authorization!;
  }
  headers["language"] = languageModel.currentLanguage??"en";
  var finalRequestBody = json.encode(requestBody);
  var response = await http.post(Uri.parse('$baseUrl$url'), headers: headers, body: finalRequestBody);
  if (response.statusCode == 403) {
    if (await checkLogin(securityModel) == false) {
      securityModel.clearProvider();
      return {
        "headers": response.headers,
        "body": {"errorCode": "NOT_AUTHENTICATED", "errorMessage": "NOT_AUTHENTICATED"}
      };
    }
  }
  if (response.headers["content-type"] == 'application/json') {
    try {
      return {"headers": response.headers, "body": json.decode(utf8.decode(response.bodyBytes))};
    } on FormatException {
      //bypass
    }
  }
  return {"headers": response.headers, "body": utf8.decode(response.bodyBytes)};
}

httpPatch(url, requestBody, context) async {
  var securityModel = Provider.of<SecurityModel>(context, listen: false);
  var languageModel = Provider.of<LanguageModel>(context, listen: false);
  Map<String, String> headers = {'content-type': 'application/json'};
  if (securityModel.authorization != null) {
    headers["Authorization"] = securityModel.authorization!;
  }
  headers["language"] = languageModel.currentLanguage??"en";
  var finalRequestBody = json.encode(requestBody);
  var response = await http.patch(Uri.parse('$baseUrl$url'), headers: headers, body: finalRequestBody);
  if (response.statusCode == 403) {
    if (await checkLogin(securityModel) == false) {
      securityModel.clearProvider();
      return {
        "headers": response.headers,
        "body": {"errorCode": "NOT_AUTHENTICATED", "errorMessage": "NOT_AUTHENTICATED"}
      };
    }
  }
  if (response.headers["content-type"] == 'application/json') {
    try {
      return {"headers": response.headers, "body": json.decode(utf8.decode(response.bodyBytes))};
    } on FormatException {
      //bypass
    }
  }
  return {"headers": response.headers, "body": utf8.decode(response.bodyBytes)};
}

httpDelete(url, context) async {
  var securityModel = Provider.of<SecurityModel>(context, listen: false);
  var languageModel = Provider.of<LanguageModel>(context, listen: false);
  Map<String, String> headers = {'content-type': 'application/json'};
  if (securityModel.authorization != null) {
    headers["Authorization"] = securityModel.authorization!;
  }
  headers["language"] = languageModel.currentLanguage??"en";
  try {
    var response = await http.delete(Uri.parse('$baseUrl$url'), headers: headers);
    if (response.statusCode == 403) {
      if (await checkLogin(securityModel) == false) {
        securityModel.clearProvider();
        return {
          "headers": response.headers,
          "body": {"errorCode": "NOT_AUTHENTICATED", "errorMessage": "NOT_AUTHENTICATED"}
        };
      }
    }
    if (response.headers["content-type"] == 'application/json') {
      try {
        return {"headers": response.headers, "body": json.decode(utf8.decode(response.bodyBytes))};
      } on FormatException {
        //bypass
      }
    }
    return {"headers": response.headers, "body": utf8.decode(response.bodyBytes)};
  } catch (error) {
    return {
      "headers": {},
      "body": {"errorMessage": "Connection error!"}
    };
  }
}

httpPut(url, requestBody, context) async {
  var securityModel = Provider.of<SecurityModel>(context, listen: false);
  var languageModel = Provider.of<LanguageModel>(context, listen: false);
  Map<String, String> headers = {'content-type': 'application/json'};
  if (securityModel.authorization != null) {
    headers["Authorization"] = securityModel.authorization!;
  }
  headers["language"] = languageModel.currentLanguage??"en";
  var finalRequestBody = json.encode(requestBody);
  var response = await http.put(Uri.parse('$baseUrl$url'), headers: headers, body: finalRequestBody);
  if (response.statusCode == 403) {
    if (await checkLogin(securityModel) == false) {
      securityModel.clearProvider();
      return {
        "headers": response.headers,
        "body": {"errorCode": "NOT_AUTHENTICATED", "errorMessage": "NOT_AUTHENTICATED"}
      };
    }
  }
  if (response.headers["content-type"] == 'application/json') {
    try {
      return {"headers": response.headers, "body": json.decode(utf8.decode(response.bodyBytes))};
    } on FormatException {
      //bypass
    }
  }
  return {"headers": response.headers, "body": utf8.decode(response.bodyBytes)};
}
