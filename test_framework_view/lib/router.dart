import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_framework_view/components/dynamic_play_form.dart';
import 'package:test_framework_view/page/administrator/admin_config.dart';
import 'package:test_framework_view/page/file/list_file_directory.dart';
import 'package:test_framework_view/page/insights/dashboard/qa_platform_dashboard.dart';
import 'package:test_framework_view/page/runs/test_run/test_run_data_detail.dart';
import 'package:test_framework_view/page/setting_navigation/setting_project_info/setting_project_info.dart';
import 'package:test_framework_view/page/work_mgt/task_type/task_type_widget.dart';
import 'package:test_framework_view/page/list_test/tests/dashboard/test_dashboard.dart';
import 'package:test_framework_view/page/prj-360/prj360_dashboard.dart';
import 'package:test_framework_view/page/prj-360/prj360_qa_platform.dart';
import 'package:test_framework_view/page/prj-360/prj360_resource_pool.dart';
import 'package:test_framework_view/page/prj-360/prj360_work_mgt.dart';
import 'package:test_framework_view/page/product_list/product_list_widget.dart';
import 'package:test_framework_view/page/404_page/not_found_page_widget.dart';
import 'package:test_framework_view/page/delete_data/delete_test_batch_run.dart';
import 'package:test_framework_view/page/guide/sidebar_guide.dart';
import 'package:test_framework_view/page/insights/report/dynamic_report_page.dart';
import 'package:test_framework_view/page/work_mgt/board/agile_task_widget.dart';
import 'package:test_framework_view/page/work_mgt/sprint/active_sprint_widget.dart';
import 'package:test_framework_view/page/work_mgt/sprint/sprint_widget.dart';
import 'package:test_framework_view/page/work_mgt/summary/kanban_summary_widget.dart';
import 'package:test_framework_view/page/work_mgt/summary/total_summary_widget.dart';
import 'package:test_framework_view/page/work_mgt/tag/tags_widget.dart';
import 'package:test_framework_view/page/work_mgt/task/task_timeline_widget.dart';
import 'package:test_framework_view/page/document/document.dart';
import 'package:test_framework_view/page/element_repository/element_repository.dart';
import 'package:test_framework_view/page/list_test/import-export-test/import_export_multi_test.dart';
import 'package:test_framework_view/page/list_test/tests/test/list_all_test.dart';
import 'package:test_framework_view/page/list_test/tests/test/list_test_directory_test_case.dart';
import 'package:test_framework_view/page/loading.dart';
import 'package:test_framework_view/page/noti/notification.dart';
import 'package:test_framework_view/page/profile/password.dart';
import 'package:test_framework_view/page/profile/profile_view.dart';
import 'package:test_framework_view/page/profile/token_widget.dart';
import 'package:test_framework_view/page/resource-pool/device/device/rp_device_widget.dart';
import 'package:test_framework_view/page/resource-pool/device/device_type/rp_device_type_widget.dart';
import 'package:test_framework_view/components/dashboard/dynamic_dashboard_page.dart';
import 'package:test_framework_view/page/resource-pool/insights/rp_dashboard.dart';
import 'package:test_framework_view/page/runs/batch_run/test_batch_run.dart';
import 'package:test_framework_view/page/runs/batch_run/test_batch_run_detail.dart';
import 'package:test_framework_view/page/runs/bug_tracker/bug_tracker.dart';
import 'package:test_framework_view/page/runs/dashboard/test_run_dashboard.dart';
import 'package:test_framework_view/page/runs/test_run/test_run_detail.dart';
import 'package:test_framework_view/page/set_up/set_up_widget.dart';
import 'package:test_framework_view/page/setting_navigation/setting_component/setting_component.dart';
import 'package:test_framework_view/page/setting_navigation/setting_data_directory/setting_data_directory.dart';
import 'package:test_framework_view/page/setting_navigation/setting_data_directory/setting_data_directory_detail.dart';
import 'package:test_framework_view/page/setting_navigation/setting_environment/setting_environment.dart';
import 'package:test_framework_view/page/setting_navigation/setting_environment/version_environment.dart';
import 'package:test_framework_view/page/setting_navigation/setting_label/setting_label.dart';
import 'package:test_framework_view/page/setting_navigation/setting_parameter/setting_parameter.dart';
import 'package:test_framework_view/page/setting_navigation/setting_profile/setting_profile.dart';
import 'package:test_framework_view/page/setting_navigation/setting_version/setting_version.dart';
import 'package:test_framework_view/page/setting_navigation/setting_version/version_variable.dart';
import 'package:test_framework_view/page/list_test/batch/run_batch/batch_run_widget.dart';
import 'package:test_framework_view/page/list_test/batch/batch_step/batch_step_detail.dart';
import 'package:test_framework_view/page/list_test/batch/batch_step/batch_step_widget.dart';
import 'package:test_framework_view/page/setting_project/project/setting_project.dart';
import 'package:test_framework_view/page/settings_application/bug_tracker_account/bug_tracker_account.dart';
import 'package:test_framework_view/page/settings_application/email_account/setting_email_account.dart';
import 'package:test_framework_view/page/settings_application/email_template/setting_email_template.dart';
import 'package:test_framework_view/page/settings_application/host-name/host_name_.dart';
import 'package:test_framework_view/page/settings_application/notification_template/setting_notification_template.dart';
import 'package:test_framework_view/page/settings_application/organization/setting_organization.dart';
import 'package:test_framework_view/page/settings_application/organization_role/setting_organization_role.dart';
import 'package:test_framework_view/page/setting_project/project_type/setting_project_type.dart';
import 'package:test_framework_view/page/setting_project/random_type/setting_random_type.dart';
import 'package:test_framework_view/page/setting_project/sys_node/sys_node.dart';
import 'package:test_framework_view/page/setting_project/sys_node/sys_node_detail.dart';
import 'package:test_framework_view/page/setting_project/value_type/setting_value_type.dart';
import 'package:test_framework_view/page/settings_application/organization_user/setting_organization_user.dart';
import 'package:test_framework_view/page/setting_navigation/setting_user/setting_user.dart';
import 'package:test_framework_view/model/model.dart';
import 'package:test_framework_view/page/list_test/batch/test_batch/test_batch_detail_widget.dart';
import 'package:test_framework_view/page/list_test/batch/test_batch/test_batch_widget.dart';
import 'package:test_framework_view/page/workflow/wf-flow/wf_flow_widget.dart';
import 'package:test_framework_view/page/workflow/wf-status/wf_status_widget.dart';
import 'page/delete_data/delete_test_case_run.dart';
import 'page/work_mgt/task/task_list_widget.dart';
import 'page/login/login.dart';
import 'page/runs/test_run/test_scenario_run.dart';
import 'package:flutter/foundation.dart';

import 'page/settings_application/scale/scale.dart';

class AppRoutePath {
  String location;
  AppRoutePath(this.location);
}

class AppRouteInformationParser extends RouteInformationParser<AppRoutePath> {
  @override
  Future<AppRoutePath> parseRouteInformation(RouteInformation routeInformation) async {
    return AppRoutePath(routeInformation.location);
  }

  @override
  RouteInformation? restoreRouteInformation(AppRoutePath configuration) {
    if (configuration.location != "") {
      return RouteInformation(location: configuration.location);
    }
    return const RouteInformation(location: "/");
  }
}

class AppRouterDelegate extends RouterDelegate<AppRoutePath> with ChangeNotifier, PopNavigatorRouterDelegateMixin<AppRoutePath> {
  @override
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
  RouterItem? _nextRouterItem;
  RouterItem? _lastRouterItem;

  AppRouterDelegate() {
    _nextRouterItem = RouterItem(key: UniqueKey(), url: "/");
    _lastRouterItem = _nextRouterItem;
  }

  @override
  AppRoutePath get currentConfiguration => AppRoutePath(_lastRouterItem != null ? _lastRouterItem!.url : "/");

  Widget? getUrlWidget(RouterItem routerItem, NavigationModel navigationModel) {
    Widget? currentWidget;
    final uri = Uri.parse(routerItem.url);
    if (uri.pathSegments.isNotEmpty) {
      if (uri.pathSegments[0] == "select") {
        currentWidget = const ProductListWidget();
      } else if (uri.pathSegments[0] == "notification") {
        currentWidget = const MyNotification();
      } else if (uri.pathSegments[0] == "qa-platform") {
        if (navigationModel.product != null && navigationModel.product != uri.pathSegments[0]) {
          navigationModel.setProductWithoutNotify(uri.pathSegments[0]);
        }
        if (uri.pathSegments[1] == "docs") {
          if (uri.pathSegments.length >= 3) {
            currentWidget = DocumentWidget(nameSideBarItem: uri.pathSegments[2]);
          } else {
            currentWidget = const DocumentWidget();
          }
        } else if (uri.pathSegments[1] == "setting") {
          if (uri.pathSegments[2] == "project") {
            currentWidget = const SettingProjectWidget(
              type: "qa-platform",
            );
          } else if (uri.pathSegments[2] == "project-type") {
            currentWidget = const SettingProjectTypeWidget();
          } else if (uri.pathSegments[2] == "value-type") {
            currentWidget = const SettingValueTypeWidget();
          } else if (uri.pathSegments[2] == "random-type") {
            currentWidget = const SettingRandomTypeWidget();
          } else {
            currentWidget = const NotFoundPageWidget();
          }
        } else if (uri.pathSegments[1] == "agent") {
          if (uri.pathSegments.length > 2) {
            currentWidget = SysNodeDetailWidget(id: uri.pathSegments[2]);
          } else {
            currentWidget = const SysNodeWidget();
          }
        } else if (uri.pathSegments[1] == "project") {
          if (uri.pathSegments.length >= 3) {
            if (navigationModel.prjProjectId != null && navigationModel.prjProjectId != uri.pathSegments[2]) {
              navigationModel.setPrjProjectIdWithoutNotify(uri.pathSegments[2]);
            }
          }
          if (uri.pathSegments.length >= 5 && uri.pathSegments[3] == "insights") {
            if (uri.pathSegments[4] == "dashboard") {
              currentWidget = const QAPlatformDashBoardPageWidget();
            } else if (uri.pathSegments[4] == "reports") {
              currentWidget = const DynamicReportPageWidget(reportPageType: "qa_platform");
            } else {
              currentWidget = const NotFoundPageWidget();
            }
          } else if (uri.pathSegments.length >= 5 && uri.pathSegments[3] == "test-list") {
            if (uri.pathSegments.length >= 5 && uri.pathSegments[4] == "dashboard") {
              currentWidget = const TestDashBoardPageWidget();
            } else if (uri.pathSegments.length >= 5 && uri.pathSegments[4] == "all") {
              currentWidget = const ListTestAllWidget();
            } else if (uri.pathSegments.length >= 5 && uri.pathSegments[4] == "test_case") {
              if (uri.pathSegments.length >= 7) {
                if (uri.pathSegments[6] == "run") {
                  currentWidget = Material(
                    child: DynamicPlayFormBody(
                      testCaseId: uri.pathSegments[5],
                      type: uri.pathSegments[4],
                      parentId: uri.pathSegments[7],
                    ),
                  );
                } else if (uri.pathSegments[6] == "re-run") {
                  currentWidget = Material(
                      child: DynamicPlayFormBody(
                    testCaseId: uri.pathSegments[5],
                    type: uri.pathSegments[4],
                    testRunid: uri.pathSegments[7],
                    parentId: uri.pathSegments[8],
                  ));
                } else {
                  currentWidget = const NotFoundPageWidget();
                }
              } else {
                currentWidget = const ListTestDirectoryTestCase(
                  type: "test_case",
                );
              }
            } else if (uri.pathSegments.length >= 5 &&
                (uri.pathSegments[4] == "test_scenario" || uri.pathSegments[4] == "test_suite" || uri.pathSegments[4] == "manual_test")) {
              if (uri.pathSegments.length >= 7) {
                if (uri.pathSegments[6] == "run") {
                  currentWidget = Material(child: DynamicPlayFormBody(testCaseId: uri.pathSegments[5], type: uri.pathSegments[4]));
                } else if (uri.pathSegments[6] == "re-run") {
                  currentWidget = Material(
                      child: DynamicPlayFormBody(testCaseId: uri.pathSegments[5], type: uri.pathSegments[4], testRunid: uri.pathSegments[7]));
                } else {
                  currentWidget = const NotFoundPageWidget();
                }
              } else {
                currentWidget = ListTestDirectoryTestCase(
                  type: uri.pathSegments[4],
                );
              }
            } else if (uri.pathSegments[4] == "test_batch") {
              if (uri.pathSegments.length == 6) {
                currentWidget = SettingTestBatchDetailWidget(
                  id: uri.pathSegments[5],
                  prjProjectId: uri.pathSegments[2],
                );
              } else if (uri.pathSegments.length >= 7 && uri.pathSegments[6] == "batch-step") {
                if (uri.pathSegments.length == 8) {
                  currentWidget = SettingBatchStepDetailWidget(
                    id: uri.pathSegments[7],
                    prjProjectId: uri.pathSegments[2],
                  );
                } else {
                  currentWidget = BatchStepWidget(
                    testBatchId: uri.pathSegments[5],
                  );
                }
              } else if (uri.pathSegments.length == 7 && uri.pathSegments[6] == "batch-run") {
                currentWidget = RunBatchWidget(
                  id: uri.pathSegments[5],
                  prjProjectId: uri.pathSegments[2],
                );
              } else if (uri.pathSegments.length == 8 && uri.pathSegments[6] == "re-run") {
                currentWidget = RunBatchWidget(
                  id: uri.pathSegments[5],
                  prjProjectId: uri.pathSegments[2],
                  testRunid: uri.pathSegments[7],
                );
              } else {
                currentWidget = const TestBatchWidget();
              }
            } else if (uri.pathSegments.length >= 5 &&
                (uri.pathSegments[4] == "test_api" || uri.pathSegments[4] == "test_jdbc" || uri.pathSegments[4] == "action")) {
              currentWidget = ListTestDirectoryTestCase(
                type: uri.pathSegments[4],
              );
            } else if (uri.pathSegments.length >= 5 && uri.pathSegments[4] == "import-export") {
              currentWidget = const ImportExportMultiTestBodyWidget();
            } else {
              currentWidget = const NotFoundPageWidget();
            }
          } else if (uri.pathSegments.length >= 5 && uri.pathSegments[3] == "runs") {
            if (uri.pathSegments[4] == "dashboard") {
              currentWidget = const TestRunDashBoardPageWidget();
            } else if (uri.pathSegments[4] == "test_case" ||
                uri.pathSegments[4] == "test_scenario" ||
                uri.pathSegments[4] == "test_suite" ||
                uri.pathSegments[4] == "manual_test") {
              if (uri.pathSegments.length >= 7) {
                if (uri.pathSegments[5] == "test-run") {
                  currentWidget = TestRunDetailWidget(
                    id: uri.pathSegments[6],
                    prjProjectId: uri.pathSegments[2],
                    type: uri.pathSegments[4],
                  );
                } else if (uri.pathSegments[5] == "test-run-data") {
                  currentWidget = TestRunDataDetailWidget(
                    id: uri.pathSegments[6],
                    prjProjectId: uri.pathSegments[2],
                    type: uri.pathSegments[4],
                  );
                }
              } else if (uri.pathSegments.length >= 6) {
                currentWidget = TestRun(
                  testCaseId: uri.pathSegments[5],
                  type: uri.pathSegments[4],
                );
              } else {
                currentWidget = TestRun(
                  type: uri.pathSegments[4],
                );
              }
            } else if (uri.pathSegments[4] == "test_batch") {
              if (uri.pathSegments.length >= 7 && uri.pathSegments[5] == "test-run") {
                currentWidget = TestBatchRunDetailWidget(
                  id: uri.pathSegments[6],
                  prjProjectId: uri.pathSegments[2],
                );
              } else if (uri.pathSegments.length >= 6) {
                currentWidget = BatchRun(
                  tfTestBatchId: uri.pathSegments[5],
                );
              } else {
                currentWidget = const BatchRun();
              }
            } else if (uri.pathSegments[4] == "bug-tracker") {
              currentWidget = const BugTrackerWidget();
            } else {
              currentWidget = const NotFoundPageWidget();
            }
          } else if (uri.pathSegments.length >= 5 && uri.pathSegments[3] == "delete-data") {
            if (uri.pathSegments[4] == "test_case" ||
                uri.pathSegments[4] == "test_scenario" ||
                uri.pathSegments[4] == "test_suite" ||
                uri.pathSegments[4] == "manual_test") {
              if (uri.pathSegments.length >= 6) {
                currentWidget = DeleteTestRun(
                  testCaseId: uri.pathSegments[5],
                  type: uri.pathSegments[4],
                );
              } else {
                currentWidget = DeleteTestRun(
                  type: uri.pathSegments[4],
                );
              }
            } else if (uri.pathSegments[4] == "test_batch") {
              currentWidget = const DeleteTestBatchRun();
            }
          } else if (uri.pathSegments.length >= 4 && uri.pathSegments[3] == "file") {
            currentWidget = const ListDirectoryFile(
              type: "file",
            );
          } else if (uri.pathSegments[3] == "data-directory") {
            if (uri.pathSegments.length >= 5) {
              currentWidget = SettingDataDirectoryDetailWidget(
                id: uri.pathSegments[4],
                prjProjectId: uri.pathSegments[2],
              );
            } else {
              currentWidget = const SettingDataDirectoryWidget();
            }
          } else if (uri.pathSegments[3] == "setting") {
            if (uri.pathSegments[4] == "set-up") {
              currentWidget = const SetUpWidget();
            } else if (uri.pathSegments[4] == "teammate") {
              currentWidget = const SettingUserWidget();
            } else if (uri.pathSegments[4] == "parameter") {
              currentWidget = SettingParameterWidget(
                prjProjectId: uri.pathSegments[2],
              );
            } else if (uri.pathSegments[4] == "module") {
              currentWidget = const SettingComponentWidget();
            } else if (uri.pathSegments[4] == "version") {
              if (uri.pathSegments.length >= 7 && uri.pathSegments[6] == "edit") {
                currentWidget = VersionVariableBody(versionId: uri.pathSegments[5]);
              } else {
                currentWidget = const SettingVersionWidget();
              }
            } else if (uri.pathSegments[4] == "environment") {
              if (uri.pathSegments.length >= 7 && uri.pathSegments[6] == "edit") {
                currentWidget = EnvironmentVariableBody(environmentId: uri.pathSegments[5]);
              } else {
                currentWidget = const SettingEnvironmentWidget();
              }
            } else if (uri.pathSegments[4] == "profile") {
              currentWidget = const SettingProfileWidget();
            } else if (uri.pathSegments[4] == "label") {
              currentWidget = const SettingLabelWidget();
            } else if (uri.pathSegments[4] == "project-info") {
              currentWidget = SettingProjectInfoWidget(
                id: uri.pathSegments[2],
              );
            }
            //  else if (uri.pathSegments[4] == "testcase-status") {
            //   currentWidget = const SettingTestCaseStatus();
            // }
            else {
              currentWidget = const NotFoundPageWidget();
            }
          } else if (uri.pathSegments[3] == "element-repository") {
            if (uri.pathSegments.length >= 5) {
              currentWidget = ListElementRepository(
                parentId: uri.pathSegments[4],
              );
            } else {
              currentWidget = const ListElementRepository();
            }
          } else {
            currentWidget = const NotFoundPageWidget();
          }
          return TestFrameworkRootPageWidget(child: currentWidget ?? const NotFoundPageWidget());
        } else if (uri.pathSegments[1] == "404-page-not-found") {
          currentWidget = const NotFoundPageWidget();
        } else {
          currentWidget = const NotFoundPageWidget();
        }
      } else if (uri.pathSegments[0] == "work-mgt") {
        if (navigationModel.product != null && navigationModel.product != uri.pathSegments[0]) {
          navigationModel.setProductWithoutNotify(uri.pathSegments[0]);
        }
        if (uri.pathSegments[1] == "setting") {
          if (uri.pathSegments[2] == "project") {
            currentWidget = const SettingProjectWidget(
              type: "work-mgt",
            );
          } else {
            currentWidget = const NotFoundPageWidget();
          }
        } else if (uri.pathSegments[1] == "summary") {
          currentWidget = const Scaffold(
            body: TotalKanbanSummaryWidget(),
          );
        } else if (uri.pathSegments[1] == "project") {
          if (uri.pathSegments.length >= 3) {
            if (navigationModel.prjProjectId != null && navigationModel.prjProjectId != uri.pathSegments[2]) {
              navigationModel.setPrjProjectIdWithoutNotify(uri.pathSegments[2]);
            }
          }
          if (uri.pathSegments.length >= 4 && uri.pathSegments[3] == "summary") {
            currentWidget = const KanbanSummaryWidget();
          } else if (uri.pathSegments.length >= 4 && uri.pathSegments[3] == "board") {
            currentWidget = const AgileTaskWidget();
          } else if (uri.pathSegments.length >= 4 && uri.pathSegments[3] == "task-list") {
            currentWidget = const TaskListBodyWidget();
          } else if (uri.pathSegments.length >= 4 && uri.pathSegments[3] == "timeline") {
            currentWidget = const TaskTimelineBodyWidget();
          } else if (uri.pathSegments.length >= 4 && uri.pathSegments[3] == "active-sprint") {
            currentWidget = const ActiveSprintWidget();
          } else if (uri.pathSegments.length >= 4 && uri.pathSegments[3] == "settings") {
            if (uri.pathSegments.length >= 5 && uri.pathSegments[4] == "tag") {
              currentWidget = const TagsWidget();
            } else if (uri.pathSegments.length >= 5 && uri.pathSegments[4] == "sprint-list") {
              currentWidget = const SprintWidget();
            } else if (uri.pathSegments.length >= 5 && uri.pathSegments[4] == "task-type") {
              currentWidget = const TaskTypeWidget();
            } else {
              currentWidget = const NotFoundPageWidget();
            }
          } else {
            currentWidget = const NotFoundPageWidget();
          }
          return TestFrameworkRootPageWidget(child: currentWidget);
        } else {
          currentWidget = const NotFoundPageWidget();
        }
      } else if (uri.pathSegments[0] == "setting") {
        if (uri.pathSegments[1] == "role") {
          currentWidget = const SettingOrganizationRoleWidget(
            typeNav: "settingRole",
          );
        } else if (uri.pathSegments[1] == "user") {
          currentWidget = const SettingOrganizationUserWidget(
            typeNav: "settingUser",
          );
        } else if (uri.pathSegments[1] == "organization") {
          currentWidget = const SettingOrganizationWidget();
        } else if (uri.pathSegments[1] == "host-name") {
          currentWidget = const SettingHostNameWidget();
        } else if (uri.pathSegments[1] == "notification-template") {
          currentWidget = const SettingNotificationTemplateWidget();
        } else if (uri.pathSegments[1] == "bug-tracker-account") {
          currentWidget = const SettingBugTrackerAccountWidget();
        } else if (uri.pathSegments[1] == "email-account") {
          currentWidget = const SettingEmailAccountWidget();
        } else if (uri.pathSegments[1] == "email-template") {
          currentWidget = const SettingEmailTemplateWidget();
        } else if (uri.pathSegments[1] == "scale") {
          currentWidget = const SettingScaleWidget();
        } else {
          currentWidget = const NotFoundPageWidget();
        }
      } else if (uri.pathSegments[0] == "prj-360") {
        if (navigationModel.product != null && navigationModel.product != uri.pathSegments[0]) {
          navigationModel.setProductWithoutNotify(uri.pathSegments[0]);
        }
        if (uri.pathSegments[1] == "setting") {
          if (uri.pathSegments[2] == "project") {
            currentWidget = const ProductListWidget();
          } else {
            currentWidget = const NotFoundPageWidget();
          }
        } else {
          if (uri.pathSegments.length >= 2 && uri.pathSegments[1] == "dashboard") {
            currentWidget = const Prj360DashBoardWidget();
          } else if (uri.pathSegments.length >= 2 && uri.pathSegments[1] == "resource-pool") {
            currentWidget = const Prj360ResourcePoolWidget();
          } else if (uri.pathSegments.length >= 2 && uri.pathSegments[1] == "qa-platform") {
            currentWidget = const Prj360QAPlatformWidget();
          } else if (uri.pathSegments.length >= 2 && uri.pathSegments[1] == "work-mgt") {
            currentWidget = const Prj360WorkMgtWidget();
          } else {
            currentWidget = const NotFoundPageWidget();
          }
          return TestFrameworkRootPageWidget(child: currentWidget);
        }
      } else if (uri.pathSegments[0] == "workflow") {
        if (navigationModel.product != null && navigationModel.product != uri.pathSegments[0]) {
          navigationModel.setProductWithoutNotify(uri.pathSegments[0]);
        }
        if (uri.pathSegments.length >= 2 && uri.pathSegments[1] == "setting") {
          if (uri.pathSegments[2] == "project") {
            currentWidget = const ProductListWidget();
          } else {
            currentWidget = const NotFoundPageWidget();
          }
        } else {
          if (uri.pathSegments.length >= 2 && uri.pathSegments[1] == "status") {
            currentWidget = const WFStatusWidget();
          } else if (uri.pathSegments.length >= 2 && uri.pathSegments[1] == "flow") {
            currentWidget = const WFFlowWidget();
          } else {
            currentWidget = const NotFoundPageWidget();
          }
          return TestFrameworkRootPageWidget(child: currentWidget);
        }
      } else if (uri.pathSegments[0] == "profile") {
        if (uri.pathSegments[1] == "view") {
          currentWidget = const ProfileView();
        } else if (uri.pathSegments[1] == "password") {
          currentWidget = const Password();
        } else if (uri.pathSegments[1] == "token") {
          currentWidget = const TokenWidget();
        } else {
          currentWidget = const NotFoundPageWidget();
        }
      } else if (uri.pathSegments[0] == "resource-pool") {
        if (navigationModel.product != null && navigationModel.product != uri.pathSegments[0]) {
          navigationModel.setProductWithoutNotify(uri.pathSegments[0]);
        }
        if (uri.pathSegments[1] == "setting") {
          if (uri.pathSegments[2] == "project") {
            currentWidget = const ProductListWidget();
          } else {
            currentWidget = const NotFoundPageWidget();
          }
        } else {
          if (uri.pathSegments[1] == "insights") {
            currentWidget = const RPDashBoardPageWidget();
          } else if (uri.pathSegments[1] == "user") {
            if (uri.pathSegments.length >= 3 && uri.pathSegments[2] == "role") {
              currentWidget = const SettingOrganizationRoleWidget(
                typeNav: "resource-pool",
              );
            } else if (uri.pathSegments.length >= 3 && uri.pathSegments[2] == "user") {
              currentWidget = const SettingOrganizationUserWidget(
                typeNav: "resource-pool",
              );
            } else {
              currentWidget = const NotFoundPageWidget();
            }
          } else if (uri.pathSegments[1] == "device") {
            if (uri.pathSegments.length >= 3 && uri.pathSegments[2] == "device") {
              currentWidget = const RpDeviceWidget();
            } else if (uri.pathSegments.length >= 3 && uri.pathSegments[2] == "device-type") {
              currentWidget = const RpDeviceTypeWidget();
            } else {
              currentWidget = const NotFoundPageWidget();
            }
          } else if (uri.pathSegments[1] == "project") {
            currentWidget = const SettingProjectWidget(
              type: "resource-pool",
            );
          } else {
            currentWidget = const NotFoundPageWidget();
          }
          return TestFrameworkRootPageWidget(child: currentWidget);
        }
      } else if (uri.pathSegments[0] == "admin") {
        if (navigationModel.product != null && navigationModel.product != uri.pathSegments[0]) {
          navigationModel.setProductWithoutNotify(uri.pathSegments[0]);
        }
        if (uri.pathSegments[1] == "setting") {
          if (uri.pathSegments[2] == "project") {
            currentWidget = const ProductListWidget();
          } else {
            currentWidget = const NotFoundPageWidget();
          }
        } else {
          if (uri.pathSegments[1] == "dashboard") {
            currentWidget = const AdminDashBoardPageWidget();
          } else if (uri.pathSegments[1] == "chart") {
            currentWidget = const DynamicReportPageWidget();
          } else if (uri.pathSegments[1] == "config") {
            currentWidget = const AdminConfigWidget();
          } else {
            currentWidget = const NotFoundPageWidget();
          }
          return TestFrameworkRootPageWidget(child: currentWidget);
        }
      } else {
        currentWidget = const NotFoundPageWidget();
      }
    }
    return currentWidget;
  }

  @override
  Widget build(BuildContext context) {
    SecurityModel securityModel = Provider.of<SecurityModel>(context, listen: true);
    NavigationModel navigationModel = Provider.of<NavigationModel>(context, listen: false);
    if (_nextRouterItem != null) {
      navigationModel.routerItemHistory.add(_nextRouterItem!);
      _nextRouterItem = null;
    }
    List<Page<dynamic>> pages = [];
    if (securityModel.inited == false) {
      pages = [const MaterialPage(child: LoadingScreen())];
    } else if (securityModel.authenticated == false) {
      pages = [
        const MaterialPage(
          child: LoginScreen(),
        )
      ];
    } else {
      pages = [
        MaterialPage(
          key: navigationModel.defaultKey,
          child: const ProductListWidget(),
        )
      ];

      for (var routerItem in navigationModel.routerItemHistory) {
        Widget? currentWidget = getUrlWidget(routerItem, navigationModel);
        if (currentWidget != null) {
          pages.add(MaterialPage(key: routerItem.key, child: currentWidget));
        }
      }
    }
    if (kReleaseMode) {
      return Navigator(
        key: navigatorKey,
        transitionDelegate: NoAnimationTransitionDelegate(),
        pages: pages,
        onPopPage: (route, result) {
          if (!route.didPop(result)) {
            return false;
          }
          Provider.of<NavigationModel>(context, listen: false).pop("/");
          return true;
        },
      );
    } else {
      return Navigator(
        key: navigatorKey,
        pages: pages,
        onPopPage: (route, result) {
          if (!route.didPop(result)) {
            return false;
          }
          Provider.of<NavigationModel>(context, listen: false).pop("/");
          return true;
        },
      );
    }
  }

  @override
  Future<void> setNewRoutePath(AppRoutePath configuration) async {
    if (configuration.location != "/") {
      _nextRouterItem = RouterItem(key: UniqueKey(), url: configuration.location);
      _lastRouterItem = _nextRouterItem;
    } else {
      _nextRouterItem = RouterItem(key: UniqueKey(), url: "/");
      _lastRouterItem = _nextRouterItem;
    }
  }

  void updateNavigation(NavigationModel navigationModel) {
    if (navigationModel.routerItemHistory.isNotEmpty) {
      _lastRouterItem = navigationModel.routerItemHistory.last;
      notifyListeners();
    } else {
      _nextRouterItem = RouterItem(key: UniqueKey(), url: "/");
      _lastRouterItem = _nextRouterItem;
      notifyListeners();
    }
  }
}

class NoAnimationTransitionDelegate extends TransitionDelegate<void> {
  @override
  Iterable<RouteTransitionRecord> resolve({
    required List<RouteTransitionRecord> newPageRouteHistory,
    required Map<RouteTransitionRecord?, RouteTransitionRecord> locationToExitingPageRoute,
    required Map<RouteTransitionRecord?, List<RouteTransitionRecord>> pageRouteToPagelessRoutes,
  }) {
    final results = <RouteTransitionRecord>[];

    for (final pageRoute in newPageRouteHistory) {
      if (pageRoute.isWaitingForEnteringDecision) {
        pageRoute.markForAdd();
      }
      results.add(pageRoute);
    }
    for (final exitingPageRoute in locationToExitingPageRoute.values) {
      if (exitingPageRoute.isWaitingForExitingDecision) {
        exitingPageRoute.markForRemove();
        final pagelessRoutes = pageRouteToPagelessRoutes[exitingPageRoute];
        if (pagelessRoutes != null) {
          for (final pagelessRoute in pagelessRoutes) {
            pagelessRoute.markForRemove();
          }
        }
      }
      results.add(exitingPageRoute);
    }
    return results;
  }
}
