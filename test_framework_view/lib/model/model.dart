import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';

import 'package:test_framework_view/config.dart';
import 'package:test_framework_view/type.dart';

class SecurityModel extends ChangeNotifier {
  LocalStorage storage;
  bool authenticated = false;
  bool inited = false;
  String? authorization;
  dynamic userLoginCurren;
  List? userPermList;
  List<String>? sysOrganizationModulesList;
  List<String>? sysOrganizationPlatformsList;
  dynamic projectPermMap;
  SecurityModel(this.storage);

  reload() async {
    authorization = storage.getItem("authorization");
    if (storage.getItem('server') != null) {
      var server = storage.getItem('server');
      setBaseUrl(server);
    }
    await reloadUserInfo();
    inited = true;
    notifyListeners();
  }

  reloadUserInfo() async {
    if (storage.getItem('server') != null) {
      var server = storage.getItem('server');
      setBaseUrl(server);
    }
    if (authorization != null) {
      authenticated = true;
      try {
        Map<String, String> headers = {'content-type': 'application/json', 'Authorization': authorization!};
        var getUserPerm = await http.get(Uri.parse('$baseUrl/test-framework-api/auth/user-perm'), headers: headers);
        if (getUserPerm.statusCode == 200 && getUserPerm.headers["content-type"] == 'application/json') {
          var userPermGetResponse = json.decode(utf8.decode(getUserPerm.bodyBytes));
          userPermList = userPermGetResponse["resultList"];
        } else if (getUserPerm.statusCode == 401) {
          authorization = null;
          authenticated = false;
          userPermList = null;
          projectPermMap = null;
          sysOrganizationModulesList = null;
          sysOrganizationPlatformsList = null;
        }
        var getProjectPerm = await http.get(Uri.parse('$baseUrl/test-framework-api/auth/project-perm'), headers: headers);
        if (getProjectPerm.statusCode == 200 && getProjectPerm.headers["content-type"] == 'application/json') {
          var projectPermGetResponse = json.decode(utf8.decode(getProjectPerm.bodyBytes));
          projectPermMap = projectPermGetResponse["result"];
        } else if (getProjectPerm.statusCode == 401) {
          authorization = null;
          authenticated = false;
          userPermList = null;
          projectPermMap = null;
          sysOrganizationModulesList = null;
          sysOrganizationPlatformsList = null;
        }

        var getUserDetail = await http.get(Uri.parse('$baseUrl/test-framework-api/auth/user-detail'), headers: headers);
        if (getUserDetail.statusCode == 200 && getUserDetail.headers["content-type"] == 'application/json') {
          var userLoginGetResponse = json.decode(utf8.decode(getUserDetail.bodyBytes));
          userLoginCurren = userLoginGetResponse["result"];
        } else if (getUserDetail.statusCode == 401) {
          authorization = null;
          authenticated = false;
          userPermList = null;
          projectPermMap = null;
          sysOrganizationModulesList = null;
          sysOrganizationPlatformsList = null;
        }
        var getSysOrganizationModules = await http.get(Uri.parse('$baseUrl/test-framework-api/user/sys/sys-organization/modules'), headers: headers);
        if (getSysOrganizationModules.statusCode == 200 && getSysOrganizationModules.headers["content-type"] == 'application/json') {
          var getSysOrganizationModulesResponse = json.decode(utf8.decode(getSysOrganizationModules.bodyBytes));
          List<String> sysOrganizationModulesList = [];
          for (String sysOrganizationModules in getSysOrganizationModulesResponse["resultList"]) {
            sysOrganizationModulesList.add(sysOrganizationModules);
          }
          this.sysOrganizationModulesList = sysOrganizationModulesList;
        } else if (getSysOrganizationModules.statusCode == 401) {
          authorization = null;
          authenticated = false;
          userPermList = null;
          projectPermMap = null;
          sysOrganizationModulesList = null;
          sysOrganizationPlatformsList = null;
        }
        var getSysOrganizationPlatforms =
            await http.get(Uri.parse('$baseUrl/test-framework-api/user/sys/sys-organization/platforms'), headers: headers);
        if (getSysOrganizationPlatforms.statusCode == 200 && getSysOrganizationPlatforms.headers["content-type"] == 'application/json') {
          var getSysOrganizationPlatformsResponse = json.decode(utf8.decode(getSysOrganizationPlatforms.bodyBytes));
          List<String> sysOrganizationPlatformsList = [];
          for (String sysOrganizationPlatforms in getSysOrganizationPlatformsResponse["resultList"]) {
            sysOrganizationPlatformsList.add(sysOrganizationPlatforms);
          }
          this.sysOrganizationPlatformsList = sysOrganizationPlatformsList;
        } else if (getSysOrganizationPlatforms.statusCode == 401) {
          authorization = null;
          authenticated = false;
          userPermList = null;
          projectPermMap = null;
          sysOrganizationModulesList = null;
          sysOrganizationPlatformsList = null;
        }
      } catch (error) {
        authorization = null;
        authenticated = false;
        userPermList = null;
        projectPermMap = null;
        sysOrganizationModulesList = null;
        sysOrganizationPlatformsList = null;
      }
    }
  }

  bool hasSysOrganizationModules(String? module) {
    if (module != null) {
      return (sysOrganizationModulesList ?? []).any((sysOrganizationModule) => sysOrganizationModule == module);
    }
    return true;
  }

  bool hasSysOrganizationPlatforms(String platform) {
    return (sysOrganizationPlatformsList ?? []).any((sysOrganizationPlatforms) => sysOrganizationPlatforms == platform);
  }

  bool hasUserPermission(String? permCode) {
    if (permCode != null) {
      return (userPermList ?? []).any((userPerm) => userPerm == permCode);
    }
    return true;
  }

  bool hasProjectPermission(String prjProjectId, String? permCode) {
    if (permCode != null) {
      return ((projectPermMap ?? {})[prjProjectId] ?? []).any((projectPerm) => projectPerm == permCode);
    }
    return true;
  }

  clearProvider() async {
    authorization = null;
    authenticated = false;
    userPermList = null;
    projectPermMap = null;
    await storage.setItem("authorization", null);
    notifyListeners();
  }

  setAuthorization({String? authorization}) async {
    this.authorization = authorization;
    authenticated = authorization != null;
    await storage.setItem("authorization", authorization);
    await reloadUserInfo();
    notifyListeners();
  }
}

class RouterItem {
  final LocalKey key;
  final String url;
  RouterItem({required this.key, required this.url});
}

class NavigationModel extends ChangeNotifier {
  LocalStorage storage;
  final List<RouterItem> routerItemHistory = [];
  var defaultKey = UniqueKey();
  String? prjProjectId;
  String? product;
  double differencefontSizeText = 0.0;
  dynamic prjProject;
  List _prjProjectList = [];
  NavigationModel(this.storage);
  bool isRequiredGetListProject = false;
  clearProvider() async {
    await storage.setItem("prjProjectId", null);
    await storage.setItem("product", null);
    await storage.setItem("differencefontSizeText", 0.0);
    routerItemHistory.clear();
    prjProjectId = null;
    product = null;
    differencefontSizeText = 0.0;
    prjProject = null;
    _prjProjectList.clear();
    notifyListeners();
  }

  reload() async {
    prjProjectId = storage.getItem("prjProjectId");
    product = storage.getItem("product");
    differencefontSizeText = storage.getItem("differencefontSizeText") ?? 0.0;
    notifyListeners();
  }

  navigate(String url) {
    add(RouterItem(key: UniqueKey(), url: url));
  }

  getPrjProject(authorization) async {
    if (prjProjectId != null && (prjProject == null || prjProject["id"] != prjProjectId)) {
      Map<String, String> headers = {'content-type': 'application/json', 'Authorization': authorization!};
      var getUserPerm = await http.get(Uri.parse('$baseUrl/test-framework-api/user/project/prj-project/$prjProjectId'), headers: headers);
      if (getUserPerm.statusCode == 200 && getUserPerm.headers["content-type"] == 'application/json') {
        var userPermGetResponse = json.decode(utf8.decode(getUserPerm.bodyBytes));
        prjProject = userPermGetResponse["result"];
      }
    }
    return prjProject;
  }

  getPrjProjectList(authorization) async {
    if (_prjProjectList.isEmpty || isRequiredGetListProject) {
      Map<String, String> headers = {'content-type': 'application/json', 'Authorization': authorization!};
      var finalRequestBody = json.encode({});
      var getUserPerm =
          await http.post(Uri.parse('$baseUrl/test-framework-api/user/project/prj-project/search'), body: finalRequestBody, headers: headers);
      if (getUserPerm.statusCode == 200 && getUserPerm.headers["content-type"] == 'application/json') {
        var userPermGetResponse = json.decode(utf8.decode(getUserPerm.bodyBytes));
        _prjProjectList = userPermGetResponse["resultList"];
      }
      if (isRequiredGetListProject) {
        isRequiredGetListProject = false;
      }
    }
    return _prjProjectList;
  }

  void setIsRequiredGetListProject(isRequiredGetListProject) {
    this.isRequiredGetListProject = isRequiredGetListProject;
    notifyListeners();
  }

  void add(RouterItem routerItem) async {
    final uri = Uri.parse(routerItem.url);
    if (uri.pathSegments.isNotEmpty && uri.pathSegments.length >= 3) {
      if (uri.pathSegments[1] == "project") {
        if (prjProjectId != null && prjProjectId != uri.pathSegments[2]) {
          prjProjectId = uri.pathSegments[2];
          await storage.setItem("prjProjectId", prjProjectId);
        }
        if (product != null && product != uri.pathSegments[0]) {
          product = uri.pathSegments[0];
          await storage.setItem("product", product);
        }
      }
    }
    routerItemHistory.add(routerItem);
    while (routerItemHistory.length > 15) {
      routerItemHistory.removeRange(0, routerItemHistory.length - 10);
    }
    notifyListeners();
  }

  void pop(defaultUrl) {
    if (routerItemHistory.isNotEmpty) {
      routerItemHistory.removeLast();
    }
    if (routerItemHistory.isEmpty) {
      navigate(defaultUrl);
    } else {
      notifyListeners();
    }
  }

  void clear() {
    routerItemHistory.clear();
    notifyListeners();
  }

  void setPrjProjectId(prjProjectId) async {
    this.prjProjectId = prjProjectId;
    await storage.setItem("prjProjectId", prjProjectId);
    notifyListeners();
  }

  setFontSizeText(double differencefontSizeText) async {
    this.differencefontSizeText += differencefontSizeText;
    await storage.setItem("differencefontSizeText", this.differencefontSizeText);
    notifyListeners();
  }

  resetFontSizeText() async {
    differencefontSizeText = 0.0;
    await storage.setItem("differencefontSizeText", differencefontSizeText);
    notifyListeners();
  }

  setProduct(product) async {
    this.product = product;
    await storage.setItem("product", product);
    notifyListeners();
  }

  void setPrjProject(prjProject) {
    this.prjProject = prjProject;
    notifyListeners();
  }

  void setPrjProjectIdWithoutNotify(prjProjectId) async {
    this.prjProjectId = prjProjectId;
    await storage.setItem("prjProjectId", prjProjectId);
  }

  void setProductWithoutNotify(product) async {
    this.product = product;
    await storage.setItem("product", product);
  }
}

class ScaleModel extends ChangeNotifier {
  LocalStorage storage;
  double textScaleFactor = 1.0;
  ScaleModel(this.storage);
  void clearProvider() async {
    await storage.setItem("textScaleFactor", 1);
    notifyListeners();
  }

  reload() async {
    if (storage.getItem("textScaleFactor") == null) {
      textScaleFactor = 1;
      await storage.setItem("textScaleFactor", 1);
      notifyListeners();
    }
  }

  void setTextScaleFactor(double textScaleFactor) async {
    this.textScaleFactor = textScaleFactor;
    await storage.setItem("textScaleFactor", textScaleFactor);
    notifyListeners();
  }
}

class WebElementModel extends ChangeNotifier {
  String? selectElementId;
  String? selectElementName;
  int currentTimeMicrosecondsSinceEpoch = 0;
  var resultAttributeList = [];
  var listFindElementsResult = [];
  void setCurrentTimeMicrosecondsSinceEpoch(currentTimeMicrosecondsSinceEpoch) {
    this.currentTimeMicrosecondsSinceEpoch = currentTimeMicrosecondsSinceEpoch;
    notifyListeners();
  }

  clearProvider() {
    selectElementId = null;
    selectElementName = null;
    currentTimeMicrosecondsSinceEpoch = 0;
    resultAttributeList.clear();
    listFindElementsResult.clear();
    notifyListeners();
  }
}

class ElementRepositoryModel extends ChangeNotifier {
  String? selectElementId;
  String? selectRepositoryId;

  clearProvider() {
    selectElementId = null;
    selectRepositoryId = null;
    notifyListeners();
  }
}

class DebugStatusModel extends ChangeNotifier {
  bool debugStatus = false;
  dynamic versionStartDebug;
  dynamic environmentStartDebug;
  dynamic profileStartDebug;

  void setDebugStatus(debugStatus) {
    this.debugStatus = debugStatus;
    notifyListeners();
  }

  void setVersionStartDebug(versionStartDebug) {
    this.versionStartDebug = versionStartDebug;
  }

  void setEnvironmentStartDebug(environmentStartDebug) {
    this.environmentStartDebug = environmentStartDebug;
  }

  void setProfileStartDebug(profileStartDebug) {
    this.profileStartDebug = profileStartDebug;
  }

  clearProvider() {
    debugStatus = false;
    versionStartDebug = null;
    environmentStartDebug = null;
    profileStartDebug = null;
    notifyListeners();
  }
}

class SideBarDocumentModel extends ChangeNotifier {
  String? selectedDocumentId;
  String? selectedDocumentContent;

  void setSelectedDocumentId(selectedDocumentId) {
    this.selectedDocumentId = selectedDocumentId;
    notifyListeners();
  }

  void setSelectedDocumentContent(selectedDocumentContent) {
    this.selectedDocumentContent = selectedDocumentContent;
  }

  clearProvider() {
    selectedDocumentId = null;
    selectedDocumentContent = null;
    notifyListeners();
  }
}

class TestCaseConfigModel extends ChangeNotifier {
  List editorComponentList = [];
  List<String> testCaseIdList = ["null"];
  List<String> testCaseNameList = [];
  Map<String, dynamic> testCaseConfigMap = {
    "null": {
      "initialIndex": 0,
      "parameterFieldList": [],
      "outputFieldList": [],
      "newVariableList": [],
      "clientList": [],
      "typeOptionEditor": false,
      "setParameterHistory": false,
      "setClientListHistory": false,
    }
  };
  clearProvider() {
    testCaseConfigMap = {
      "null": {
        "initialIndex": 0,
        "parameterFieldList": [],
        "outputFieldList": [],
        "newVariableList": [],
        "clientList": [],
        "typeOptionEditor": false,
        "setParameterHistory": false,
        "setClientListHistory": false,
      }
    };
    testCaseIdList = ["null"];
    testCaseNameList = [];
    editorComponentList = [];
    notifyListeners();
  }

  void addTestCaseIdList(testCaseId) {
    testCaseIdList.add(testCaseId);
    testCaseConfigMap[testCaseId] = {
      "initialIndex": 0,
      "parameterFieldList": [],
      "outputFieldList": [],
      "newVariableList": [],
      "clientList": [],
      "typeOptionEditor": false,
      "setParameterHistory": false,
      "setClientListHistory": false,
    };
    notifyListeners();
  }

  void removeTestCaseIdList(testCaseId) {
    testCaseIdList.remove(testCaseId);
    testCaseConfigMap.remove(testCaseId);
    notifyListeners();
  }

  void addTestCaseNameList(testCaseName) {
    testCaseNameList.add(testCaseName);
    notifyListeners();
  }

  void removeTestCaseNameList(testCaseName) {
    testCaseNameList.remove(testCaseName);
    notifyListeners();
  }

  void updateTestCaseNameLast(testCaseName) {
    testCaseNameList.last = testCaseName;
    notifyListeners();
  }

  void setInitialIndex(initialIndex) {
    if (testCaseConfigMap.containsKey(testCaseIdList.last)) {
      testCaseConfigMap[testCaseIdList.last]["initialIndex"] = initialIndex;
    } else {
      testCaseConfigMap[testCaseIdList.last] = {"initialIndex": initialIndex};
    }
    notifyListeners();
  }

  void setParameterFieldList(parameterFieldList) {
    if (testCaseConfigMap.containsKey(testCaseIdList.last)) {
      testCaseConfigMap[testCaseIdList.last]["parameterFieldList"] = parameterFieldList;
    } else {
      testCaseConfigMap[testCaseIdList.last] = {"parameterFieldList": parameterFieldList};
    }
    notifyListeners();
  }

  void setOutputFieldList(outputFieldList) {
    if (testCaseConfigMap.containsKey(testCaseIdList.last)) {
      testCaseConfigMap[testCaseIdList.last]["outputFieldList"] = outputFieldList;
    } else {
      testCaseConfigMap[testCaseIdList.last] = {"outputFieldList": outputFieldList};
    }
    notifyListeners();
  }

  void addNewVariableList(newVariable) {
    if (testCaseConfigMap.containsKey(testCaseIdList.last)) {
      testCaseConfigMap[testCaseIdList.last]["newVariableList"].add(newVariable);
    } else {
      testCaseConfigMap[testCaseIdList.last] = {"newVariableList": newVariable};
    }
    notifyListeners();
  }

  void addClientList(clientList) {
    if (testCaseConfigMap.containsKey(testCaseIdList.last)) {
      testCaseConfigMap[testCaseIdList.last]["clientList"].add(clientList);
    } else {
      testCaseConfigMap[testCaseIdList.last] = {"clientList": clientList};
    }
    notifyListeners();
  }

  void setClientList(clientList) {
    if (testCaseConfigMap.containsKey(testCaseIdList.last)) {
      testCaseConfigMap[testCaseIdList.last]["clientList"] = clientList;
    } else {
      testCaseConfigMap[testCaseIdList.last] = {"clientList": clientList};
    }
    notifyListeners();
  }

  void setEditorComponentList(editorComponentList) {
    this.editorComponentList = editorComponentList;
    notifyListeners();
  }

  void setTypeOptionEditor(typeOptionEditor) {
    if (testCaseConfigMap.containsKey(testCaseIdList.last)) {
      testCaseConfigMap[testCaseIdList.last]["typeOptionEditor"] = typeOptionEditor;
    } else {
      testCaseConfigMap[testCaseIdList.last] = {"typeOptionEditor": typeOptionEditor};
    }
  }

  void setParameterHistory(setParameterHistory) {
    if (testCaseConfigMap.containsKey(testCaseIdList.last)) {
      testCaseConfigMap[testCaseIdList.last]["setParameterHistory"] = setParameterHistory;
    } else {
      testCaseConfigMap[testCaseIdList.last] = {"setParameterHistory": setParameterHistory};
    }
  }

  void setClientListHistory(setClientListHistory) {
    if (testCaseConfigMap.containsKey(testCaseIdList.last)) {
      testCaseConfigMap[testCaseIdList.last]["setClientListHistory"] = setClientListHistory;
    } else {
      testCaseConfigMap[testCaseIdList.last] = {"setClientListHistory": setClientListHistory};
    }
  }
}

class TestCaseDirectoryModel extends ChangeNotifier {
  dynamic testCaseDirectory;
  clearProvider() {
    testCaseDirectory = null;
    notifyListeners();
  }

  void setTestCaseDirectory(testCaseDirectory) {
    this.testCaseDirectory = testCaseDirectory;
    notifyListeners();
  }
}

class PlayTestCaseModel extends ChangeNotifier {
  List playDataList = [];
  clearProvider() {
    playDataList.clear();
    notifyListeners();
  }

  void addPlayDataList(playData) {
    playDataList.insert(0, playData);
    notifyListeners();
  }
}

class CopyWidgetModel extends ChangeNotifier {
  Item? copyWidget;
  clearProvider() {
    copyWidget = null;
    notifyListeners();
  }

  void setCopyWidget(Item? widget) {
    Item newItem = checkItemWidget(widget!);
    setExpand(newItem);
    copyWidget = newItem;
    notifyListeners();
  }

  void setExpand(Item newItem) {
    if (newItem.bodyList.length > 1) {
      newItem.bodyList[0].isExpand = false;
    }
    if (newItem.elseBodyList.length > 1) {
      newItem.elseBodyList[0].isExpand = false;
    }
    if (newItem.thenBodyList.length > 1) {
      newItem.thenBodyList[0].isExpand = false;
    }
  }

  Item checkItemWidget(Item listCheckType) {
    if (listCheckType.type == "waitElement") {
      listCheckType.type = "waitElementExists";
    }
    if (listCheckType.type == "checkElement") {
      listCheckType.type = "checkElementExists";
    }
    if (listCheckType.type == "childElementCheckElement") {
      listCheckType.type = "childElementCheckElementExists";
    }
    if (listCheckType.type == "sendKey") {
      listCheckType.type = "sendKeyFromElement";
    }
    if (listCheckType.type == "childElementSendKey") {
      listCheckType.type = "childElementSendKeyFromElement";
    }
    Item itemBodyList = Item(listCheckType.type, listCheckType.category);
    if (listCheckType.type == "value" ||
        listCheckType.type == "variable" ||
        listCheckType.type == "version_variable" ||
        listCheckType.type == "environment_variable" ||
        listCheckType.type == "node_variable" ||
        listCheckType.type == "get") {
      itemBodyList = Item(listCheckType.type, "variablePath");
    }
    if (listCheckType.type == "value") {
      if (listCheckType.tfTestElementId != null) {
        itemBodyList.tfTestElementId = listCheckType.tfTestElementId;
      }
      if (listCheckType.tfTestElementRepositoryId != null) {
        itemBodyList.tfTestElementRepositoryId = listCheckType.tfTestElementRepositoryId;
      }
      if (listCheckType.tfValueTemplateId != null) {
        itemBodyList.tfValueTemplateId = listCheckType.tfValueTemplateId;
      }
    }
    if (listCheckType.type == "variable_default") {
      itemBodyList = Item(listCheckType.type, "variable_default");
      if (listCheckType.variablePath.isNotEmpty) {
        itemBodyList.variablePath = [];
        for (var list in listCheckType.variablePath) {
          itemBodyList.variablePath.add(checkItemWidget(list));
        }
      }
    }
    itemBodyList.key = listCheckType.key;
    itemBodyList.mouseActionType = listCheckType.mouseActionType;
    if (listCheckType.endKey != null) itemBodyList.endKey = listCheckType.endKey;
    if (listCheckType.thenBodyList.isNotEmpty) {
      for (var thenBody in listCheckType.thenBodyList) {
        itemBodyList.thenBodyList.add(checkItemWidget(thenBody));
      }
    }
    if (listCheckType.elseBodyList.isNotEmpty) {
      for (var elseBody in listCheckType.elseBodyList) {
        itemBodyList.elseBodyList.add(checkItemWidget(elseBody));
      }
    }
    if (listCheckType.onErrorBodyList.isNotEmpty) {
      for (var errorBody in listCheckType.onErrorBodyList) {
        itemBodyList.onErrorBodyList.add(checkItemWidget(errorBody));
      }
    }
    if (listCheckType.onFinishBodyList.isNotEmpty) {
      for (var finalBody in listCheckType.onFinishBodyList) {
        itemBodyList.onFinishBodyList.add(checkItemWidget(finalBody));
      }
    }
    if (listCheckType.bodyList.isNotEmpty) {
      for (var body in listCheckType.bodyList) {
        itemBodyList.bodyList.add(checkItemWidget(body));
      }
    }
    if (listCheckType.variablePath.isNotEmpty) {
      itemBodyList.variablePath = [];
      for (var variable in listCheckType.variablePath) {
        itemBodyList.variablePath.add(checkItemWidget(variable));
      }
    }
    if (listCheckType.conditionList.isNotEmpty) {
      itemBodyList.conditionList = [];
      for (var condition in listCheckType.conditionList) {
        itemBodyList.conditionList.add(checkItemWidget(condition));
      }
    }
    if (listCheckType.operatorList.isNotEmpty) {
      itemBodyList.operatorList = [];
      for (var operator in listCheckType.operatorList) {
        itemBodyList.operatorList.add(checkItemWidget(operator));
      }
    }
    if (listCheckType.conditionGroup != null) {
      itemBodyList.conditionGroup = checkItemWidget(listCheckType.conditionGroup);
    }
    if (listCheckType.name != null) {
      itemBodyList.name = listCheckType.name;
    }
    if (listCheckType.platform != null) {
      itemBodyList.platform = listCheckType.platform;
    }
    if (listCheckType.clientName != null) {
      itemBodyList.clientName = listCheckType.clientName;
    }
    if (listCheckType.clientList.isNotEmpty) {
      itemBodyList.clientList = listCheckType.clientList;
    }
    if (listCheckType.description != null) {
      itemBodyList.description = listCheckType.description;
    }
    if (listCheckType.dataType != null) {
      itemBodyList.dataType = listCheckType.dataType;
    }
    if (listCheckType.type == "actionCall" && listCheckType.actionName != null) {
      itemBodyList.actionName = listCheckType.actionName;
    }
    if (listCheckType.type == "testCaseCall" && listCheckType.testCaseName != null) {
      itemBodyList.testCaseName = listCheckType.testCaseName;
    }
    if (listCheckType.type == "apiCall" && listCheckType.apiName != null) {
      itemBodyList.apiName = listCheckType.apiName;
    }
    if (listCheckType.objectName != null) {
      itemBodyList.objectName = listCheckType.objectName;
    }
    if (listCheckType.operatorType != null) {
      itemBodyList.operatorType = listCheckType.operatorType;
    }
    if (listCheckType.message != null) {
      itemBodyList.message = listCheckType.message;
    }
    if (listCheckType.isList != null) {
      itemBodyList.isList = listCheckType.isList;
    }
    if (listCheckType.returnType.isNotEmpty) {
      var returnTypeNew = Map.from(listCheckType.returnType);
      itemBodyList.returnType = returnTypeNew;
    }
    if (listCheckType.o1 != null) {
      itemBodyList.o1 = checkItemWidget(listCheckType.o1!);
    }
    if (listCheckType.o2 != null) {
      itemBodyList.o2 = checkItemWidget(listCheckType.o2!);
    }
    if (listCheckType.parameterList.isNotEmpty) {
      for (var parameter in listCheckType.parameterList) {
        var parameterNew = Map.from(parameter);
        if (parameterNew["variablePath"] != null && parameterNew["variablePath"].length > 0) {
          var variablePathList = [];
          for (var variablePath in parameterNew["variablePath"]) {
            var newVariablePath = Map.from(variablePath);
            variablePathList.add(newVariablePath);
          }
          parameterNew["variablePath"] = variablePathList;
        }
        itemBodyList.parameterList.add(parameterNew);
      }
    }
    return itemBodyList;
  }
}

class ListElementPlayIdModel extends ChangeNotifier {
  List<String> listId = [];
  bool? isSelectedAll = true;
  List<String> listAllId = [];
  clearProvider() {
    listId.clear();
    listAllId.clear();
    isSelectedAll = true;
    notifyListeners();
  }

  void setIsSelectedAll(selected) {
    isSelectedAll = selected;
    notifyListeners();
  }

  void setListId(listId) {
    this.listId = listId;
    notifyListeners();
  }

  void setListAllId(listAllId) {
    this.listAllId = listAllId;
    notifyListeners();
  }

  void addAllIdList(String? id) {
    if (id != null) {
      listAllId.add(id);
    }
    notifyListeners();
  }

  void addListId(String id) {
    listAllId.add(id);
    notifyListeners();
  }

  void addElementPlayIdList(String? id, bool hasUnique) {
    if (id != null) {
      if (hasUnique) {
        if (!listId.contains(id)) {
          listId.add(id);
        }
      } else {
        listId.add(id);
      }
    }
    notifyListeners();
  }

  void removeElementPlayIdList(id) {
    listId.remove(id);
    notifyListeners();
  }
}

class ModeViewEditorModel extends ChangeNotifier {
  bool isStepMode = false;
  clearProvider() {
    isStepMode = false;
    notifyListeners();
  }

  setIsStepMode(isStepMode) {
    this.isStepMode = isStepMode;
    notifyListeners();
  }
}

class NotificationModel extends ChangeNotifier {
  LocalStorage storage;
  Timer? timer;
  NotificationModel(this.storage);
  List<dynamic> sysNotificationList = [];
  int lastNotificationRowCount = 0;
  bool hasNewNotification = false;
  reload() async {
    reloadData();
    if (timer != null) {
      timer!.cancel();
    }
    timer = Timer.periodic(const Duration(seconds: 30), (Timer timer) => reloadData());
  }

  reloadData() async {
    var authorization = storage.getItem("authorization");
    if (authorization == null) {
      return;
    }
    Map<String, String> headers = {'content-type': 'application/json', 'Authorization': storage.getItem("authorization") ?? ""};
    var searchNotiNotificationParameters = {"queryLimit": "10", "status": "1"};
    var searchNotiNotificationResponse = await http.get(
      Uri.parse('$baseUrl/test-framework-api/user/noti/noti-notification').replace(queryParameters: searchNotiNotificationParameters),
      headers: headers,
    );
    if (searchNotiNotificationResponse.statusCode == 200 && searchNotiNotificationResponse.headers["content-type"] == 'application/json') {
      var notiNotificationResult = json.decode(utf8.decode(searchNotiNotificationResponse.bodyBytes));
      var sysNotificationRowCount = notiNotificationResult["rowCount"];
      if (lastNotificationRowCount != sysNotificationRowCount) {
        lastNotificationRowCount = sysNotificationRowCount;
        hasNewNotification = true;
      }
      sysNotificationList = notiNotificationResult["resultList"];
      notifyListeners();
    }
  }

  unmarkedNewNotification() {
    hasNewNotification = false;
    notifyListeners();
  }
}

class EditorNotificationModel extends ChangeNotifier {
  updateNotification() {
    notifyListeners();
  }
}

class LanguageModel extends ChangeNotifier {
  String _currentLanguage = "en";
  final Map<String, dynamic> _localizedStrings = {};
  String? get currentLanguage => _currentLanguage;
  bool modelInit = false;
  dynamic lastAuthorization = "";
  dynamic lastAuthorizationHasAdminMultilingualPermission = false;
  // List get localizedStrings => _localizedStrings;

  LocalStorage storage;
  LanguageModel(this.storage);

  String translate(String name, String defaultValue, {List<String>? variables}) {
    Map<String, dynamic>? translations = _localizedStrings[name];
    if (translations == null) {
      translations = {
        "name": name,
        "defaultValue": defaultValue,
        "status": 1,
      };
      _localizedStrings.addAll({
        for (var sysMultilingual in [translations]) sysMultilingual["name"]: sysMultilingual
      });
      addLocalizedString(translations);
    }
    if (translations.isNotEmpty) {
      var translatedValue = '';
      var fieldName = "defaultValue";
      switch (_currentLanguage) {
        case 'vi':
          fieldName = "valueVi";
          break;
        case 'jp':
          fieldName = "valueJp";
          break;
        default:
          fieldName = "valueEn";
          break;
      }
      translatedValue = translations[fieldName] ?? translations['defaultValue'] ?? defaultValue;
      if (variables != null) {
        for (var index = 0; index < variables.length; index++) {
          translatedValue = translatedValue.replaceAll('\$$index', variables[index]);
        }
      }
      return translatedValue;
    }
    return defaultValue;
  }

  Future<void> initialize() async {
    if (storage.getItem("language") == null) {
      await storage.setItem("language", 'en');
    }
    _currentLanguage = await storage.getItem("language");
    await loadLocalizedStrings();
  }

  Future loadLocalizedStrings() async {
    Map<String, String> headers = {'content-type': 'application/json', 'Authorization': storage.getItem("authorization") ?? ""};
    var localizedStrings = await http.get(Uri.parse('$baseUrl/test-framework-api/guest/sys/sys-multilingual'), headers: headers);
    if (localizedStrings.statusCode == 200 && localizedStrings.headers["content-type"] == 'application/json') {
      var localizedStringsResponse = json.decode(utf8.decode(localizedStrings.bodyBytes));
      _localizedStrings.addAll({for (var sysMultilingual in localizedStringsResponse["resultList"]) sysMultilingual["name"]: sysMultilingual});
    }
    modelInit = true;
  }

  void addLocalizedString(result) async {
    if (modelInit) {
      var authorization = storage.getItem("authorization");
      if (authorization != null && lastAuthorization != authorization) {
        lastAuthorizationHasAdminMultilingualPermission = false;
        var permCode = "ADMIN_MULTILINGUAL";
        Map<String, String> headers = {'content-type': 'application/json', 'Authorization': authorization ?? ""};
        var getUserPerm = await http.get(Uri.parse('$baseUrl/test-framework-api/auth/user-perm'), headers: headers);
        if (getUserPerm.statusCode == 200 && getUserPerm.headers["content-type"] == 'application/json') {
          var userPermGetResponse = json.decode(utf8.decode(getUserPerm.bodyBytes));
          lastAuthorizationHasAdminMultilingualPermission = (userPermGetResponse["resultList"] ?? []).any((userPerm) => userPerm == permCode);
        }
      }
      if (lastAuthorizationHasAdminMultilingualPermission) {
        Map<String, String> headers = {'content-type': 'application/json', 'Authorization': authorization};
        var localizedStrings =
            await http.put(Uri.parse("$baseUrl/test-framework-api/admin/sys/sys-multilingual"), body: json.encode(result), headers: headers);
        if (localizedStrings.statusCode == 200 && localizedStrings.headers["content-type"] == 'application/json') {
          await loadLocalizedStrings();
        }
        notifyListeners();
      }
    }
  }

  Future<void> setLanguage(String locale) async {
    if (_currentLanguage != locale) {
      _currentLanguage = locale;
      await storage.setItem("language", locale);
      await loadLocalizedStrings();
      notifyListeners();
    }
  }
}
